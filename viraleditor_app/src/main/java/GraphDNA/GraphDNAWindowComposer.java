package GraphDNA;

import java.io.File;
import java.io.IOException;

import javax.imageio.ImageIO;

import Shared.interchange.Family;
import javafx.embed.swing.SwingFXUtils;
import javafx.scene.control.MenuBar;
import javafx.scene.image.WritableImage;
import javafx.stage.Stage;

public class GraphDNAWindowComposer {

    private static final String HELP_SITE = "https://4virology.net/virology-ca-tools/graphdna/";
    Family database;

    /**
     * Opens the Graph DNA help site in a new web browser
     */
    public void getOnlineHelp() {
        Shared.Functions.getOnlineHelp(HELP_SITE);
    }

    /**
     * Closes the GraphDNA window from menu
     * 
     * @param graphDNAWindowMenu the menu attached to the window
     */
    public void closeFromMenu(MenuBar graphDNAWindowMenu) {
        Stage stage = (Stage) graphDNAWindowMenu.getScene().getWindow();
        System.out.println("Closing Graph DNA.  Goodbye :)");
        stage.close();
    }

    /**
     * Saves image of chart to file specified on user's machine.
     * 
     * @param image - image of chart to be saved
     * @param file  - file image will be saved to (location specified by user)
     */
    public void saveAsPng(WritableImage image, File file) {
        try {
            ImageIO.write(SwingFXUtils.fromFXImage(image, null), "png", file);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

}