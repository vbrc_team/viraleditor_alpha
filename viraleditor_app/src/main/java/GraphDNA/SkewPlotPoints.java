package GraphDNA;

import org.biojava.nbio.core.sequence.DNASequence;

/**
 * Holds the x and y points for a DNA Graph. Also requires the type of skew to
 * be calculating...
 */
public class SkewPlotPoints extends PlotPoints {

    /** keyword for purine skew graph. */
    public static final String SKEWTYPE_PURINE = "PURINE";

    /** keyword for keto skew graph. */
    public static final String SKEWTYPE_KETO = "KETO";

    /** keyword for AT skew graph. */
    public static final String SKEWTYPE_AT = "AT";

    /** keyword for GC skew graph. */
    public static final String SKEWTYPE_GC = "GC";

    /** keyword for AC skew graph. */
    public static final String SKEWTYPE_AC = "AC";

    /** keyword for GA skew graph. */
    public static final String SKEWTYPE_GA = "GA";

    /** keyword for GT skew graph. */
    public static final String SKEWTYPE_GT = "GT";

    /** keyword for TC skew graph. */
    public static final String SKEWTYPE_TC = "TC";

    /** keyword for DNA skew graph. */
    public static final String SKEWTYPE_DNA = "DNA";

    String type;

    // strings for individual nucleotides
    public static final String THYMINE = "T";
    public static final String CYTOSINE = "C";
    public static final String ADENINE = "A";
    public static final String GUANINE = "G";

    /**
     * Creates a new SkewPlotPoints object.
     *
     * @param seq  The main sequence (field inherited from PlotPoints)
     * @param type Assigns specific type to class
     */
    public SkewPlotPoints(DNASequence seq, String type) {
        super(seq);
        this.type = type;
    }

    /**
     * calculate the plot
     *
     * NOTE: start and end are inclusive and numbering starts at 1 e.g. if start = 2
     * and end = 5, the data will be plotted for x={2, 3, 4, 5} constraints: start
     * >= 1 end > start special values for end: if end == -1 use ALL data
     *
     * @param start start range of the data to plot
     * @param end   end range of the data to plot
     */
    public void calculate(int start, int end) {

        if ((end == -1) || ((end >= sequence.getLength()) && (start == 0))) {
            xVal = new int[sequence.getLength() + 1];
            yVal = new int[sequence.getLength() + 1];
        } else if (start > sequence.getLength()) {
            xVal = new int[1];
            yVal = new int[1];
        } else if (((start > 0) && (end < sequence.getLength())) || ((start == 0) && (end < sequence.getLength()))) {
            xVal = new int[end - start + 1];
            yVal = new int[end - start + 1];
        } else if ((start > 0) && (end >= sequence.getLength())) {
            xVal = new int[sequence.getLength() - start + 1];
            yVal = new int[sequence.getLength() - start + 1];
        }

        if (type.equals(SKEWTYPE_PURINE)) {
            calculatePurine(start);
        } else if (type.equals(SKEWTYPE_KETO)) {
            calculateKeto(start);
        } else if (type.equals(SKEWTYPE_AT)) {
            calculateAT(start);
        } else if (type.equals(SKEWTYPE_GC)) {
            calculateGC(start);
        } else if (type.equals(SKEWTYPE_AC)) {
            calculateAC(start);
        } else if (type.equals(SKEWTYPE_GA)) {
            calculateGA(start);
        } else if (type.equals(SKEWTYPE_GT)) {
            calculateGT(start);
        } else if (type.equals(SKEWTYPE_TC)) {
            calculateTC(start);
        } else if (type.equals(SKEWTYPE_DNA)) {
            calculateDNA(start);
        } else {
            System.out.println("Unknown Skew FeatureType Selected!");
        }
    }

    /**
     * calculate and set our purine skew values
     * 
     * @param start The start of the sequence to calculate
     */
    private void calculatePurine(int start) {

        // set xVals
        for (int i = 0; i < xVal.length; i++) {
            xVal[i] = start + i;
        }

        yVal[0] = 0;

        for (int i = 1; i < yVal.length; i++) {

            String s = sequence.getCompoundAt(start + i).getBase();
            if (s.equals(ADENINE) || s.equals(GUANINE)) {
                yVal[i] = yVal[i - 1] + 1;
            } else if (s.equals(CYTOSINE) || s.equals(THYMINE)) {
                yVal[i] = yVal[i - 1] - 1;
            } else {
                yVal[i] = yVal[i - 1];
            }
        }
    }

    /**
     * calculate and set our keto skew values
     * 
     * @param start The start of the sequence to calculate
     */
    private void calculateKeto(int start) {

        // set xVals
        for (int i = 0; i < xVal.length; i++) {
            xVal[i] = start + i;
        }

        yVal[0] = 0;

        for (int i = 1; i < yVal.length; i++) {
            String s = sequence.getCompoundAt(start + i).getBase();
            if (s.equals(GUANINE) || s.equals(THYMINE)) {
                yVal[i] = yVal[i - 1] + 1;
            } else if (s.equals(CYTOSINE) || s.equals(ADENINE)) {
                yVal[i] = yVal[i - 1] - 1;
            } else {
                yVal[i] = yVal[i - 1];
            }
        }
    }

    /**
     * calculate the AT skew
     * 
     * @param start The start of the sequence to calculate
     */
    private void calculateAT(int start) {

        // set xVals
        for (int i = 0; i < xVal.length; i++) {
            xVal[i] = start + i;
        }

        yVal[0] = 0;

        for (int i = 1; i < yVal.length; i++) {
            String s = sequence.getCompoundAt(start + i).getBase();
            if (s.equals(ADENINE)) {
                yVal[i] = yVal[i - 1] + 1;
            } else if (s.equals(THYMINE)) {
                yVal[i] = yVal[i - 1] - 1;
            } else {
                yVal[i] = yVal[i - 1];
            }
        }
    }

    /**
     * calculate the GC skew
     * 
     * @param start The start of the sequence to calculate
     */
    private void calculateGC(int start) {

        // set xVals
        for (int i = 0; i < xVal.length; i++) {
            xVal[i] = start + i;
        }

        yVal[0] = 0;

        for (int i = 1; i < yVal.length; i++) {
            String s = sequence.getCompoundAt(start + i).getBase();
            if (s.equals(GUANINE)) {
                yVal[i] = yVal[i - 1] + 1;
            } else if (s.equals(CYTOSINE)) {
                yVal[i] = yVal[i - 1] - 1;
            } else {
                yVal[i] = yVal[i - 1];
            }
        }
    }

    /**
     * calculate the AC skew
     * 
     * @param start The start of the sequence to calculate
     */
    private void calculateAC(int start) {

        // set xVals
        for (int i = 0; i < xVal.length; i++) {
            xVal[i] = start + i;
        }

        yVal[0] = 0;

        for (int i = 1; i < yVal.length; i++) {
            String s = sequence.getCompoundAt(start + i).getBase();
            if (s.equals(ADENINE)) {
                yVal[i] = yVal[i - 1] + 1;
            } else if (s.equals(CYTOSINE)) {
                yVal[i] = yVal[i - 1] - 1;
            } else {
                yVal[i] = yVal[i - 1];
            }
        }
    }

    /**
     * calculate the GA skew
     * 
     * @param start The start of the sequence to calculate
     */
    private void calculateGA(int start) {

        // set xVals
        for (int i = 0; i < xVal.length; i++) {
            xVal[i] = start + i;
        }

        yVal[0] = 0;

        for (int i = 1; i < yVal.length; i++) {
            String s = sequence.getCompoundAt(start + i).getBase();

            if (s.equals(GUANINE)) {
                yVal[i] = yVal[i - 1] + 1;
            } else if (s.equals(ADENINE)) {
                yVal[i] = yVal[i - 1] - 1;
            } else {
                yVal[i] = yVal[i - 1];
            }
        }
    }

    /**
     * calculate the GT skew
     * 
     * @param start The start of the sequence to calculate
     */
    private void calculateGT(int start) {

        // set xVals
        for (int i = 0; i < xVal.length; i++) {
            xVal[i] = start + i;
        }

        yVal[0] = 0;

        for (int i = 1; i < yVal.length; i++) {
            String s = sequence.getCompoundAt(start + i).getBase();

            if (s.equals(GUANINE)) {
                yVal[i] = yVal[i - 1] + 1;
            } else if (s.equals(THYMINE)) {
                yVal[i] = yVal[i - 1] - 1;
            } else {
                yVal[i] = yVal[i - 1];
            }
        }
    }

    /**
     * calculate the TC skew
     * 
     * @param start The start of the sequence to calculate
     */
    private void calculateTC(int start) {

        // set xVals
        for (int i = 0; i < xVal.length; i++) {
            xVal[i] = start + i;
        }

        yVal[0] = 0;

        for (int i = 1; i < yVal.length; i++) {
            String s = sequence.getCompoundAt(start + i).getBase();

            if (s.equals(THYMINE)) {
                yVal[i] = yVal[i - 1] + 1;
            } else if (s.equals(CYTOSINE)) {
                yVal[i] = yVal[i - 1] - 1;
            } else {
                yVal[i] = yVal[i - 1];
            }
        }
    }

    /**
     * Calculates the x and y points for DNA Walker.
     *
     * @param start The start of the sequence to calculate
     */
    public void calculateDNA(int start) {

        xVal[0] = 0;

        for (int i = 1; i < xVal.length; i++) {
            String s = sequence.getCompoundAt(start + i).getBase();
            if (s.equals(THYMINE)) {
                xVal[i] = xVal[i - 1] + 1;
            } else if (s.equals(ADENINE)) {
                xVal[i] = xVal[i - 1] - 1;
            } else {
                xVal[i] = xVal[i - 1];
            }
        }

        yVal[0] = 0;

        for (int i = 1; i < yVal.length; i++) {
            String s = sequence.getCompoundAt(start + i).getBase();
            if (s.equals(CYTOSINE)) {
                yVal[i] = yVal[i - 1] + 1;
            } else if (s.equals(GUANINE)) {
                yVal[i] = yVal[i - 1] - 1;
            } else {
                yVal[i] = yVal[i - 1];
            }
        }
    }
}
