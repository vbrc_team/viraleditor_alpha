package GraphDNA;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.biojava.nbio.core.exceptions.CompoundNotFoundException;
import org.biojava.nbio.core.sequence.DNASequence;
import org.biojava.nbio.core.sequence.io.GenbankReaderHelper;

import Shared.Windows.DatabasePopupDialog;
import Shared.interchange.*;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.geometry.Point2D;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.SnapshotParameters;
import javafx.scene.chart.LineChart;
import javafx.scene.chart.NumberAxis;
import javafx.scene.chart.XYChart;
import javafx.scene.chart.XYChart.Data;
import javafx.scene.chart.XYChart.Series;
import javafx.scene.control.MenuBar;
import javafx.scene.control.TextField;
import javafx.scene.image.WritableImage;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.AnchorPane;
import javafx.scene.paint.Color;
import javafx.scene.shape.Rectangle;
import javafx.stage.FileChooser;
import javafx.stage.FileChooser.ExtensionFilter;
import javafx.stage.Stage;

/**
 * @author Christian
 *
 */
public class GraphDNAWindowDialog {

    /** keyword for purine skew graph. */
    public static final String SKEWTYPE_PURINE = "PURINE";

    /** keyword for keto skew graph. */
    public static final String SKEWTYPE_KETO = "KETO";

    /** keyword for AT skew graph. */
    public static final String SKEWTYPE_AT = "AT";

    /** keyword for GC skew graph. */
    public static final String SKEWTYPE_GC = "GC";

    /** keyword for AC skew graph. */
    public static final String SKEWTYPE_AC = "AC";

    /** keyword for GA skew graph. */
    public static final String SKEWTYPE_GA = "GA";

    /** keyword for GT skew graph. */
    public static final String SKEWTYPE_GT = "GT";

    /** keyword for TC skew graph. */
    public static final String SKEWTYPE_TC = "TC";

    /** keyword for DNA skew graph. */
    public static final String SKEWTYPE_DNA = "DNA";

    private GraphDNAWindowComposer composer;

    private static List<Reference> genomes;

    /** stores which skew is visible. */
    private String display;

    // plot points for the mouse
    @FXML
    private TextField xMousePos;
    @FXML
    private TextField yMousePos;

    // plot points for zoom rectangle
    private double startYPosition;
    private double startXPosition;

    /**
     * Zoom rectangle X and Y positions relative to layout.
     */
    private double rectangleX;
    private double rectangleY;

    @FXML
    private AnchorPane graphPane;

    @FXML
    private Rectangle draggedRectangle;

    @FXML
    MenuBar graphDNAWindowMenu;

    /**
     * Initializes the GraphDNAWindow composer
     * 
     * @param composer
     */
    public void initComposer(GraphDNAWindowComposer composer) {
        this.composer = composer;
    }

    @FXML
    LineChart<Number, Number> dnaGraph;
    @FXML
    NumberAxis xAxis;
    @FXML
    NumberAxis yAxis;

    /**
     * Initializes the FXML file and its components.
     */
    @FXML
    public void initialize() {
        xMousePos.setEditable(false);
        yMousePos.setEditable(false);
        dnaGraph.setCreateSymbols(false);
        genomes = new ArrayList<>();
        display = SKEWTYPE_PURINE;
    }

    /**
     * Imports a single fasta file
     */
    @FXML
    public void importFasta() {
        importFile('f');
    }

    /**
     * Imports a single genbank file
     */
    @FXML
    public void importGenBank() {
        importFile('g');
    }

    /**
     * Imports a file directly.
     *
     * @param key indicates which type of file and how the data is handled
     */
    private void importFile(char key) {
        FileChooser fc = new FileChooser();
        fc.setTitle("Open File");

        if (key == 'f') {
            fc.getExtensionFilters().addAll(new FileChooser.ExtensionFilter("FASTA", "*.fasta", "*.fa"));
        } else if (key == 'g') {
            fc.getExtensionFilters().addAll(new FileChooser.ExtensionFilter("GenBank", "*.gbk", "*.gb"));
        } else {
            throw new IllegalArgumentException(String.format("Unsupported key: %c", key));
        }

        // open window
        File file = fc.showOpenDialog(null);
        if (file != null) {
            if (key == 'f') {
                InterchangeToFASTA fromFASTA = new InterchangeToFASTA();
                String fasta;
                try {
                    fasta = String.join(System.lineSeparator(), Files.readAllLines(file.toPath()));
                    genomes.addAll(fromFASTA.from(fasta).getReferences());
                } catch (IOException e) {
                    return;
                }
            } else if (key == 'g') {
                try {
                    Map<String, DNASequence> genBank = GenbankReaderHelper.readGenbankDNASequence(file);
                    InterchangeToNTGenBank fromGenBank = new InterchangeToNTGenBank();
                    genomes.addAll(fromGenBank.from(genBank).getReferences());
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }

            loadGraph(display);
        }
    }

    /**
     * Loads all the data into the graph with the Purine skew.
     */
    @FXML
    public void loadPurine() {
        display = SKEWTYPE_PURINE;
        loadGraph(display);
    }

    /**
     * Loads all the data into the graph with the Keto skew.
     */
    @FXML
    public void loadKeto() {
        display = SKEWTYPE_KETO;
        loadGraph(SKEWTYPE_KETO);
    }

    /**
     * Loads all the data into the graph with the AT skew.
     */
    @FXML
    public void loadAT() {
        display = SKEWTYPE_AT;
        loadGraph(display);
    }

    /**
     * Loads all the data into the graph with the GC skew.
     */
    @FXML
    public void loadGC() {
        display = SKEWTYPE_GC;
        loadGraph(display);
    }

    /**
     * Loads all the data into the graph with the AC skew.
     */
    @FXML
    public void loadAC() {
        display = SKEWTYPE_AC;
        loadGraph(display);
    }

    /**
     * Loads all the data into the graph with the GA skew.
     */
    @FXML
    public void loadGA() {
        display = SKEWTYPE_GA;
        loadGraph(display);
    }

    /**
     * Loads all the data into the graph with the GT skew.
     */
    @FXML
    public void loadGT() {
        display = SKEWTYPE_GT;
        loadGraph(display);
    }

    /**
     * Loads all the data into the graph with the TC skew.
     */
    @FXML
    public void loadTC() {
        display = SKEWTYPE_TC;
        loadGraph(display);
    }

    /**
     * Loads all the data into the graph with the DNA Walker skew.
     */
    @FXML
    public void loadDNA() {
        display = SKEWTYPE_DNA;
        loadGraph(display);
    }

    /**
     * Loads all the data into the graph with a specified skew type
     * 
     * @param graphType
     */
    private void loadGraph(String graphType) {
        dnaGraph.getData().clear();
        xAxis.setLabel("Length of Genome");

        switch (graphType) {
        case SKEWTYPE_PURINE:
            dnaGraph.setTitle("Purine Skew");
            yAxis.setLabel("CT-AG");
            break;
        case SKEWTYPE_KETO:
            dnaGraph.setTitle("Keto Skew");
            yAxis.setLabel("AC-GT");
            break;
        case SKEWTYPE_AT:
            dnaGraph.setTitle("AT Skew");
            yAxis.setLabel("T-A");
            break;
        case SKEWTYPE_GC:
            dnaGraph.setTitle("GC Skew");
            yAxis.setLabel("C-G");
            break;
        case SKEWTYPE_AC:
            dnaGraph.setTitle("AC Skew");
            yAxis.setLabel("C-A");
            break;
        case SKEWTYPE_GA:
            dnaGraph.setTitle("GA Skew");
            yAxis.setLabel("A-G");
            break;
        case SKEWTYPE_GT:
            dnaGraph.setTitle("GT Skew");
            yAxis.setLabel("T-G");
            break;
        case SKEWTYPE_TC:
            dnaGraph.setTitle("TC Skew");
            yAxis.setLabel("C-T");
            break;
        case SKEWTYPE_DNA:
            dnaGraph.setTitle("DNA Walker");
            xAxis.setLabel("A-T");
            yAxis.setLabel("G-C");
            break;
        default:
            System.out.println("Unknown Skew FeatureType Selected!");
            break;
        }

        for (Reference reference : genomes) {
            if (reference.getOrigin().getSequence().equals("")) {
                continue;
            }

            DNASequence sequence;
            try {
                sequence = new DNASequence(reference.getOrigin().getSequence());
            } catch (CompoundNotFoundException e) {
                continue;
            }

            SkewPlotPoints testPlot = new SkewPlotPoints(sequence, graphType);
            testPlot.calculate(0, sequence.getLength());
            int[] xPlot = testPlot.getX();
            int[] yPlot = testPlot.getY();

            XYChart.Series<Number, Number> series = new Series<>();

            if (!reference.getFeatures(Genome.class).isEmpty()) {
                series.setName(reference.getFeatures(Genome.class).get(0).getOrganismName());
            } else if (!reference.getFeatures(Source.class).isEmpty()) {
                series.setName(reference.getFeatures(Source.class).get(0).getOrganism());
            }

            // series.getData() returns ObservableList that will cause the GUI to update
            // every time an element is added. Add data in batch to quickly load data.
            List<Data<Number, Number>> data = new ArrayList<>();
            for (int j = 0; j < sequence.getLength(); j = j + 10) {
                data.add(new Data<>(xPlot[j], yPlot[j]));
            }
            series.getData().addAll(data);

            dnaGraph.getData().add(series);
        }

        xAxis.setAutoRanging(true);
        yAxis.setAutoRanging(true);
        xAxis.autosize();
        yAxis.autosize();
    }

    /**
     * Saves an image of the graph to the location specified by the user
     */
    @FXML
    public void saveImage() {
        FileChooser fc = new FileChooser();
        fc.getExtensionFilters().add(new ExtensionFilter("PNG Files", "*.png"));
        fc.setTitle("Save Image");
        File file = fc.showSaveDialog(null);

        WritableImage image = dnaGraph.snapshot(new SnapshotParameters(), null);
        composer.saveAsPng(image, file);
    }

    /**
     * Creates a rectangle user can drag over chart. When done, the chart becomes
     * the area inside the rectangle.
     * 
     * @param event - mouse event from user, press, drag, or release.
     */
    @FXML
    public void zoomIn(MouseEvent event) {
        if (event.getEventType() == MouseEvent.MOUSE_PRESSED) {
            draggedRectangle = new Rectangle();
            draggedRectangle.setFill(Color.web("blue", 0.1));
            draggedRectangle.setStroke(Color.BLUE);
            draggedRectangle.setStrokeDashOffset(50);
            graphPane.getChildren().add(draggedRectangle);
            draggedRectangle.setTranslateX(event.getSceneX());
            draggedRectangle.setTranslateY(event.getSceneY());
            rectangleX = event.getSceneX();
            rectangleY = event.getSceneY();

            Point2D mouseSceneCoords = new Point2D(event.getSceneX(), event.getSceneY());

            startXPosition = (double) xAxis.getValueForDisplay(xAxis.sceneToLocal(mouseSceneCoords).getX());
            startYPosition = (double) yAxis.getValueForDisplay(yAxis.sceneToLocal(mouseSceneCoords).getY());
        }

        if (event.getEventType() == MouseEvent.MOUSE_DRAGGED) {
            draggedRectangle.setWidth(Math.abs(event.getSceneX() - rectangleX));
            draggedRectangle.setHeight(Math.abs(event.getSceneY() - rectangleY));

            if (event.getSceneX() < rectangleX) {
                draggedRectangle.setTranslateX(event.getSceneX());
            }

            if (event.getSceneY() < rectangleY) {
                draggedRectangle.setTranslateY(event.getSceneY());
            }
        }

        if (event.getEventType() == MouseEvent.MOUSE_RELEASED) {
            Point2D mouseSceneCoords = new Point2D(event.getSceneX(), event.getSceneY());
            double endXPosition = (double) xAxis.getValueForDisplay(xAxis.sceneToLocal(mouseSceneCoords).getX());
            double endYPosition = (double) yAxis.getValueForDisplay(yAxis.sceneToLocal(mouseSceneCoords).getY());

            setYBounds(endYPosition);
            setXBounds(endXPosition);
            graphPane.getChildren().remove(draggedRectangle);
        }
    }
    
    /**
     * Opens the UI preferences window
     * 
     * @param actionEvent
     */
    @FXML
    public void openPrefs() {
        double[] bounds = new GraphDNAPrefsDialog().openPrefs(xAxis.getLowerBound(), xAxis.getUpperBound(),
                yAxis.getLowerBound(), yAxis.getUpperBound());
        if(bounds.length == 0) {
            return;
        }

        xAxis.setAutoRanging(false);
        xAxis.setLowerBound(bounds[0]);
        xAxis.setUpperBound(bounds[1]);
        yAxis.setAutoRanging(false);
        yAxis.setLowerBound(bounds[2]);
        yAxis.setUpperBound(bounds[3]);
    }

    /**
     * Updates the x and y fields above the graph based on the position of the mouse
     * on the graph
     * 
     * @param event
     */
    @FXML
    public void updateMouseCoords(MouseEvent event) {
        Point2D mouseSceneCoords = new Point2D(event.getSceneX(), event.getSceneY());

        double xPos = Math.round((double) xAxis.getValueForDisplay(xAxis.sceneToLocal(mouseSceneCoords).getX()) * 1000d)
                / 1000d;
        double yPos = Math.round((double) yAxis.getValueForDisplay(yAxis.sceneToLocal(mouseSceneCoords).getY()) * 1000d)
                / 1000d;

        xMousePos.setText(String.valueOf(xPos));
        yMousePos.setText(String.valueOf(yPos));
    }

    /**
     * Sets the bounds of the zoomed in chart for the y values.
     * 
     * @param endYPosition - position where user dragged mouse until.
     */
    private void setYBounds(double endYPosition) {
        double lowerBound = Math.min(endYPosition, startYPosition);
        double upperBound = Math.max(endYPosition, startYPosition);

        if (Math.abs((upperBound - lowerBound)) > 0.0005) {
            yAxis.setAutoRanging(false);
            yAxis.setUpperBound(upperBound);
            yAxis.setLowerBound(lowerBound);
            yAxis.setTickUnit(Math.abs((upperBound - lowerBound)) / 10);
        }
    }

    /**
     * Sets the bounds of the zoomed in chart for the x values.
     * 
     * @param endXPosition - position where user dragged mouse until.
     */
    private void setXBounds(double endXPosition) {
        double lowerBound = Math.min(endXPosition, startXPosition);
        double upperBound = Math.max(endXPosition, startXPosition);

        if (Math.abs((upperBound - lowerBound)) > 0.0005) {
            xAxis.setAutoRanging(false);
            xAxis.setUpperBound(upperBound);
            if (lowerBound >= 0) {
                xAxis.setLowerBound(lowerBound);
            } else {
                xAxis.setLowerBound(0);
            }
            xAxis.setTickUnit(Math.abs((upperBound - lowerBound)) / 10);
        }
    }

    /**
     * Resets the view to original values. If a plot has been added, the view will
     * be reset including it.
     */
    @FXML
    public void resetView() {
        xAxis.setAutoRanging(true);
        yAxis.setAutoRanging(true);
    }

    /**
     * Clears all data in the chart.
     */
    @FXML
    public void clearAll() {
        genomes = new ArrayList<>();
        loadGraph(display);
    }

    /**
     * Opens the graph DNA window with the given sequences loaded
     * 
     * @param data - genomes to be mapped
     * @return - the controller
     * @throws IOException
     */
    public static GraphDNAWindowDialog openWindow(List<Reference> data) throws IOException {
        FXMLLoader loader = new FXMLLoader(
                GraphDNAWindowDialog.class.getClassLoader().getResource("GraphDNAWindow.fxml"));
        Parent root = loader.load();
        GraphDNAWindowDialog controller = loader.getController();
        controller.initComposer(new GraphDNAWindowComposer());

        if (!data.isEmpty()) {
            genomes.addAll(data);
            controller.loadPurine();
        }

        Stage popup = new Stage();
        popup.setTitle("Graph DNA");
        popup.setScene(new Scene(root));
        popup.show();

        return controller;
    }

    /**
     * Opens a new window so the user can select viruses to add, then Graph DNA is updated with the selection
     */
    @FXML
    void loadSequencesFromDatabase() {
        List<Reference> selectedViruses = DatabasePopupDialog.OpenPopUp(composer.database);
        //ToDo: when we have real a database, add the viruses to the window

    }

    public void setDatabase(Family database) {
        composer.database = database;
    }


    /**
     * Opens GraphDNA help page in browser
     * 
     * @param actionEvent
     */
    @FXML
    private void helpMenuAction(ActionEvent actionEvent) {
        composer.getOnlineHelp();
    }

    /**
     * Closes the GraphDNA window from the menu.
     * 
     * @param actionEvent
     */
    @FXML
    private void quitMenuAction(ActionEvent actionEvent) {
        composer.closeFromMenu(graphDNAWindowMenu);
    }
}