package GraphDNA;

import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.Node;
import javafx.stage.Stage;

public class GraphDNAPrefsComposer {
    
    /**
     * Closes the Preferences window
     * 
     * @param actionEvent
     */
    @FXML
    public void close(ActionEvent actionEvent) {
        final Node source = (Node) actionEvent.getSource();
        final Stage stage = (Stage) source.getScene().getWindow();
        stage.close();
    }

}
