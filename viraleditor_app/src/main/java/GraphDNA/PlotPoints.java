package GraphDNA;

import org.biojava.nbio.core.sequence.DNASequence;

/**
 * Holds all the x and y points for a graph...
 */
public abstract class PlotPoints {

    /**
     * Offset for Sequences, since they start counting at 1 and most strings start
     * counting at zero.
     */
    public static final int BIOSEQUENCEOFFSET = 1;

    /** the main sequence. */
    protected DNASequence sequence;

    /** x coordinate points. */
    protected int[] xVal;

    /** y coordinate points. */
    protected int[] yVal;

    /**
     * Creates a new PlotPoints object.
     *
     * @param sequence Assigned to the sequence field for use in the class
     */
    public PlotPoints(DNASequence sequence) {
        this.sequence = sequence;
    }

    /**
     * Returns the X points to be plotted.
     * 
     * @return All the x points to be plotted
     */
    public int[] getX() {
        return (xVal);
    }

    /**
     * Returns the Y points to be potted.
     * 
     * @return All the y points to be plotted
     */
    public int[] getY() {
        return (yVal);
    }

    /**
     * Returns how many points there are.
     * 
     * @return The length of the x and y points (which is also just the sequence
     *         length...)
     */
    public int getLength() {
        return (sequence.getLength());
    }

    /**
     * performs the calculations specific to what type of PlotPoints we are using...
     * 
     * @param start the start position in the sequence
     * @param end   the end position in the sequence
     */
    public abstract void calculate(int start, int end);
}
