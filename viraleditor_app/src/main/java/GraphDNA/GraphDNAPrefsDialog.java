package GraphDNA;

import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.TextField;
import javafx.stage.Stage;

public class GraphDNAPrefsDialog {
    
 // plot points for preferences
    @FXML
    private TextField xLow;
    @FXML
    private TextField xHigh;
    @FXML
    private TextField yLow;
    @FXML
    private TextField yHigh;
    
    private GraphDNAPrefsComposer composer;
    
    private boolean updateFlag;

    /**
     * Initializes the GraphDNAPrefs composer
     * 
     * @param composer
     */
    public void initComposer(GraphDNAPrefsComposer composer) {
        this.composer = composer;
    }
    
    @FXML
    public void close(ActionEvent event) {
        updateFlag = false;
        composer.close(event);
    }
    
    @FXML
    public void setBoundaries(ActionEvent event) {
        updateFlag = true;
        composer.close(event);
    }
    
    /**
     * Opens a new window for the user to change boundaries for x and y axis
     * 
     * @param xLow  lower bound for the x axis
     * @param xHigh upper bound for the x axis
     * @param yLow  lower bound for the y axis
     * @param yHigh upper bound for the y axis
     * @return the x and y coordinates set by the user in the preferences window.
     */
    public double[] openPrefs(double xLow, double xHigh, double yLow, double yHigh) {
        double[] bounds = new double[4];
        // Open window for selection
        try {
            FXMLLoader loader = new FXMLLoader(
                    GraphDNAPrefsDialog.class.getClassLoader().getResource("GraphDNAPrefs.fxml"));
            Parent root = loader.load();
            GraphDNAPrefsDialog controller = loader.getController();
            controller.initComposer(new GraphDNAPrefsComposer());
            controller.xLow.setText(Double.toString(xLow));
            controller.xHigh.setText(Double.toString(xHigh));
            controller.yLow.setText(Double.toString(yLow));
            controller.yHigh.setText(Double.toString(yHigh));

            Stage prefsWindow = new Stage();
            prefsWindow.setTitle("Axes Options");
            prefsWindow.setScene(new Scene(root));
            prefsWindow.showAndWait();
            
            bounds[0] = Double.parseDouble(controller.xLow.getText());
            bounds[1] = Double.parseDouble(controller.xHigh.getText());
            bounds[2] = Double.parseDouble(controller.yLow.getText());
            bounds[3] = Double.parseDouble(controller.yHigh.getText());
        } catch (Exception e) {
            e.printStackTrace();
        }
        if(updateFlag) {
            return bounds;
        } else {
            return new double[0];
        }
    }
}
