package VirusInvestigator;

import java.util.List;

import Shared.TreeNode;
import Shared.interchange.Family;
import javafx.scene.control.TreeItem;

public interface VirusInvestigatorView {
	
	/**
     * Sets the right panel of the investigator dialog to a FXML UI and sets the controller so the panel elements
     * have access to variables of the scene
     * 
     * @param path -  The path to an FXML file
     * @param controller - The controller of the current dialog, aka the parent of the upper panel
     */
    public void setPanel(String path, VirusInvestigatorDialog controller);
    
    /**
     * Sets the top right panel of the investigator dialog to a FXML UI and sets the controller so the panel elements
     * have access to variables of the scene
     * @param path -  The path to an FXML file
     * @param controller - The controller of the current dialog, aka the parent of the upper panel
     */
    public void setUpper(String path, VirusInvestigatorDialog controller);
    
    /**
     * Sets the bottom right panel of the investigator dialog to a FXML UI and sets the controller so the panel elements
     * have access to variables of the scene
     * @param path -  The path to an FXML file
     * @param controller - The controller of the current dialog, aka the parent of the upper panel
     */
    public void setLower(String path, VirusInvestigatorDialog controller);
    
    /**
     * Sets the values for the database drop down with 'current' as selected
     * 
     * @param databases - List of database names
     * @param current   - the currently selected database (from ViralEditor)
     */
    public void setDatabaseList(List<Family> databases, Family current);
    
    /**
     * Sets the values for the gene display preference drop down with show
     * Sequences and Genes as the default.
     */
    
    public void setGenePreference();
    
    /**
     * Fetches viruses for the database and adds them to the UI, sets up preferences for interaction
     * @param database - name of database to be used
     */
    public void setUpDatabase(TreeItem<TreeNode> dbRootNode);
    
    public void setInvestigatorTips(String toolTip);
    
}
