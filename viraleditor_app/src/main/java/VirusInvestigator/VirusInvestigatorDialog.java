package VirusInvestigator;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.LinkedHashSet;
import java.util.List;

import Alignment.AlignmentComposer;
import Alignment.AlignmentDialog;
import Annotator.AnnotatorDialog;
import CodonStats.CodonStatsDialog;
import DatabaseAnalysis.*;
import DatabaseAnalysis.TableView;
import DotPlot.DotPlotSequenceDialog;
import DotPlot.DotPlotSessionDialog;
import GenomeMap.GenomeMapWindowDialog;
import GraphDNA.GraphDNAWindowDialog;
import Shared.FamilyItem;
import Shared.Performance;
import Shared.TreeNode;
import Shared.interchange.*;
import ViralEditor.*;
import VirusInvestigator.Menu.DrawGenomeMap;
import VirusInvestigator.Menu.DrawGenomeMapComposer;
import VirusInvestigator.Menu.DrawGenomeMapDialog;
import VirusInvestigator.Menu.DrawGenomeMapView;
import javafx.collections.FXCollections;
import javafx.collections.ListChangeListener;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.scene.input.DragEvent;
import javafx.scene.input.Dragboard;
import javafx.scene.input.TransferMode;
import javafx.scene.layout.AnchorPane;
import javafx.stage.FileChooser;
import javafx.stage.Stage;
import javafx.stage.Window;
import javafx.util.Pair;


/**
 * Handles the UI for the investigator (where files and viruses are selected), design is based on the tool selected
 * when the composer is initialized
 */
public class VirusInvestigatorDialog implements VirusInvestigatorView {
    private VirusInvestigatorComposer composer;

    
    //main UI elements
    @FXML
    private AnchorPane investigatorRightPane;
    @FXML
    private AnchorPane investigatorUpperSplit;
    @FXML
    private AnchorPane investigatorLowerSplit;
    @FXML
    private ChoiceBox<FamilyItem> databaseList;
    @FXML
    private ChoiceBox<String> genePreference;
    @FXML
    private Label investigatorTips;
    @FXML
    private Button investigatorHome;
    @FXML
    private Button investigatorLaunch;
    @FXML
    private TreeView<TreeNode> investigatorVirusList;
    
    //selected database viruses UI element
    @FXML
    private Button saveSelectedButton;
    @FXML
    private ListView<TreeNode> selectedList;
    
    //Database analysis UI element
    @FXML
    private Label investigatorCount;
    
    

    //upload buttons
    @FXML
    private Button Multiple_LocalUpload;
    @FXML
    private Button Annotation_uploadRef;
    @FXML
    private Button Annotation_uploadTarget;
    @FXML
    private Button DotPlot_uploadLeft;
    @FXML
    private Button DotPlot_uploadTop;
    @FXML
    private Button Codon_LocalUpload;

    @FXML
    private CheckBox checkLocalA;
    @FXML
    private CheckBox checkDBA;
    @FXML
    private CheckBox checkLocalB;
    @FXML
    private CheckBox checkDBB;


    //upload - file names
    @FXML
    private Label LocalAName;
    @FXML
    private Label LocalBName;

    @FXML
    private Label DBAName;
    @FXML
    private Label DBBName;

    @FXML
    private ListView<String> localList;
    
    
    ObservableList <String> localFiles;
    ObservableList<TreeNode> dbSelected;
    TreeItem<TreeNode> dbRootNode; // ***
    TreeItem<TreeNode> lastSelected;

    private Tool tool; // ***
    private Family database;// ***
    private List<TreeNode> databaseSelection = new ArrayList<>();;
    private List<File> localUploads = new ArrayList<>();
    private File localFileA;                        // Reference genome or left axis
    private File localFileB;                        // target genome or  top axis
    public Boolean databaseForA = true;
    public Boolean databaseForB = true;
    public String selectedPreference = "Sequences and Genes Separate";
    
    
    
    /**
     * Sets up the investigatorDialog
     * 
     * @param composer - The Composer associated with this dialog
     * @param toolName - The name of the tool that the investigator will display
     */
    public void initComposer(VirusInvestigatorComposer composer, Tool tool) {
        this.composer = composer;
        this.tool = tool;
    }

    
    /**
     * Sets the values for the database drop down with 'current' as selected
     * @param databases - List of database names
     * @param current - Name of the currently selected database (from ViralEditor)
     */
    @Override
    public void setDatabaseList(List<Family> databases, Family current) {
    	database = current;
        List<FamilyItem> items = new ArrayList<>();
        for (Family family : databases) {
            items.add(new FamilyItem(family));
        }
        FamilyItem selected = new FamilyItem(current);

        databaseList.setItems(FXCollections.observableList(items));
        databaseList.setValue(selected);
        databaseList.getSelectionModel().selectedItemProperty().addListener((observable, oldValue, newValue) -> {
            if (newValue != null) {
                composer.setCurrentDatabase(newValue.getFamily());
            }
        });
    }
    
    
    /**
     * sets the gene preference choicebox to display the 4 implemented options
     * Display genes and sequences separate
     * annotate genes onto sequence
     * genes only as nucleotides
     * genes only as A.As.
     */
    public void setGenePreference() {
        
        List<String> geneOptions = new ArrayList<String>();
        geneOptions.add("Sequences and Genes separate");
        geneOptions.add("Selected Genes annotated onto sequences");
        geneOptions.add("Display only Genes as Nucleotides");
        geneOptions.add("Display only Genes as Amino Acids");
        
        genePreference.setItems(FXCollections.observableList(geneOptions));
        genePreference.setValue(geneOptions.get(0));
        genePreference.getSelectionModel().selectedItemProperty().addListener((observable, oldValue, newValue) -> {
        	if (newValue != null) {
        		selectedPreference = newValue;
        	}
        
        });
        
    }
    
    @Override
    public void setInvestigatorTips(String toolTip) {
        investigatorTips.setText(toolTip);
    }
    
    /**
     * Fetches viruses for the database and adds them to the UI, sets up preferences for interaction
     * @param database - name of database to be used
     */
    @Override
    public void setUpDatabase(TreeItem<TreeNode> dbRootNode) {
    	this.dbRootNode = dbRootNode;
        investigatorVirusList.setRoot(dbRootNode);
        dbRootNode.setExpanded(true);
        investigatorVirusList.setShowRoot(false);
        investigatorVirusList.getSelectionModel().getSelectedItems().addListener(dbChange);

        if(!isDotplot() ){
            investigatorVirusList.getSelectionModel().setSelectionMode(SelectionMode.MULTIPLE);
            dbSelected =  FXCollections.observableArrayList();
            selectedList.setItems(dbSelected);
            selectedList.setMouseTransparent(true);
            selectedList.setFocusTraversable(false);
        }
        if (needSingular()){
            investigatorVirusList.getSelectionModel().setSelectionMode(SelectionMode.SINGLE);
            expandList();
        }

    }

    //called when a new selection is made in the database tree
    // Do not move, this does not work as a lambda
    ListChangeListener<TreeItem<TreeNode>> dbChange = new ListChangeListener<TreeItem<TreeNode>>() {
        @Override
        public void onChanged(Change c) {
            investigatorVirusList.getSelectionModel().getSelectedItems().removeListener(this);

            if(needSingular()){
                handleSingleTreeCellSelection();
            }else{
                handleTreeCellSelction();
            }
            investigatorVirusList.getSelectionModel().getSelectedItems().addListener(this);
        }
    };
    /**
     * Called when Db virus is selected
     * Adds valid selected items to the local list and updates composer
     */
    private void handleTreeCellSelction(){
        List<TreeNode> viruses = new ArrayList<>();
        List<TreeItem<TreeNode>> nodes = new ArrayList<>();
        nodes.addAll(investigatorVirusList.getSelectionModel().getSelectedItems());
       
        while (!nodes.isEmpty()) {
            TreeItem<TreeNode> node = nodes.remove(0);
            Reference reference = node.getValue().getReference();
            
            if (!(reference.getAccession().equals(""))){
            	viruses.add(node.getValue());
            }
            
            if ((reference.getFeatures(Genome.class).isEmpty() && reference.getFeatures(Annotation.class).isEmpty())) {
            		if (node.getValue().getReference().getFeatures(Annotation.class).size() == 0) {
            			nodes.addAll(node.getChildren());
            		}
                
            }

            else {
                viruses.add(node.getValue());
            }
        }
        //remove duplicate objects (things can be selected twice)
        viruses = new ArrayList<>(new LinkedHashSet<>(viruses));
        //store new selection in the composer
        this.databaseSelection = viruses;
        dbSelected.clear();
        dbSelected.addAll(viruses);
    }
    
    public List<Reference> buildHandler(List<Reference> virus) throws IOException {
    	FileHandler handle = new FileHandler(virus, localUploads, localFileA, localFileB, databaseForA,
                databaseForB, selectedPreference);
    	Interchange tempInter = handle.readyAlignmentReadyInterchange();
    	return tempInter.getReferences();
    }

    /**
     * Called when Db virus is selected
     * updates composer, ignores parents
     */
    private void handleSingleTreeCellSelection(){
        TreeItem<TreeNode> selectedItem = investigatorVirusList.getSelectionModel().getSelectedItem();
        boolean update = false;

        if( selectedItem != null && !selectedItem.isLeaf()){
            investigatorVirusList.getSelectionModel().clearSelection();
            if(lastSelected != null){
                investigatorVirusList.getSelectionModel().select(lastSelected);
            }else{
                investigatorVirusList.getSelectionModel().select(selectedItem.getChildren().get(0));
                lastSelected = selectedItem.getChildren().get(0);
                update = true;
            }
        }else{
            lastSelected = selectedItem;
            update =true;
        }

        //since dot plot stores viruses when user updates selection
        if(!isDotplot() && update){
            ArrayList<TreeNode> viruses = new ArrayList<>();
            dbSelected.clear();
            if(lastSelected != null){
                viruses.add(lastSelected.getValue());
                this.databaseSelection = viruses;
                dbSelected.addAll(viruses);
            }
        }

    }

    
    
    /**
     * Sets the right panel of the investigator dialog to a FXML UI and sets the controller so the panel elements
     * have access to variables of the scene
     * 
     * @param path -  The path to an FXML file
     * @param controller - The controller of the current dialog, aka the parent of the upper panel
     */
    @Override
    public void setPanel(String path, VirusInvestigatorDialog controller) {
        try{
            FXMLLoader loader = new FXMLLoader(getClass().getClassLoader().getResource(path));
            loader.setController(controller);
            Parent node = loader.load();

            investigatorRightPane.getChildren().setAll(node);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    /**
     * Sets the top right panel of the investigator dialog to a FXML UI and sets the controller so the panel elements
     * have access to variables of the scene
     * @param path -  The path to an FXML file
     * @param controller - The controller of the current dialog, aka the parent of the upper panel
     */
    @Override
    public void setUpper(String path, VirusInvestigatorDialog controller) {
        try{
            FXMLLoader loader = new FXMLLoader(getClass().getClassLoader().getResource(path));
            loader.setController(controller);
            Parent node = loader.load();

            investigatorUpperSplit.getChildren().setAll(node);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    /**
     * Sets the bottom right panel of the investigator dialog to a FXML UI and sets the controller so the panel elements
     * have access to variables of the scene
     * @param path -  The path to an FXML file
     * @param controller - The controller of the current dialog, aka the parent of the upper panel
     */
    @Override
    public void setLower(String path, VirusInvestigatorDialog controller) {
        try {
            FXMLLoader loader = new FXMLLoader(getClass().getClassLoader().getResource(path));
            loader.setController(controller);
            Parent node = loader.load();

            investigatorLowerSplit.getChildren().setAll(node);
        } catch (IOException e) {
            e.printStackTrace();
        }

    }
    

    // implement menu bar 
    @FXML
    public void openDBInfo() {
    	try {
            FXMLLoader loader = new FXMLLoader(getClass().getClassLoader().getResource("DBInfoWindow.fxml"));
            Parent root = loader.load();      
            DBInfoView controller = loader.getController();
            controller.setDBInfo(composer);
            Stage popup = new Stage();
            popup.setTitle("Database Information");
            popup.setScene(new Scene(root));
            popup.show();

        } catch (Exception e) {
            e.printStackTrace();
        }
    }
    

    @FXML
    public void openUpstreamSequence() {
    	List<String> colName = new ArrayList<String>();
    	colName.addAll(Arrays.asList("Gene Number", "Ortholog Group Name", "ORF start", "ORF stop", "ORF strandedness", "Upstream Sequence"));
    	List<String> colFactory = new ArrayList<String>();
		colFactory.addAll(Arrays.asList("gene_num", "ortholog_name", "ORF_start", "ORF_stop", "ORF_strand", "upstream_seq"));
    	try {
            FXMLLoader loader = new FXMLLoader(getClass().getClassLoader().getResource("TableViewWindow.fxml"));
            Parent root = loader.load();      
            TableViewView controller = loader.getController();
            TableViewComposer composer = new TableView(controller);
            ((TableViewDialog)controller).initComposer(composer);
            controller.setTableColumnName(colName);
            controller.setTableFactory(colFactory);
            composer.setTable("Upstream");
         
            Stage popup = new Stage();
            popup.setTitle("Gene Upstream Sequences");
            popup.setScene(new Scene(root));
            popup.show();

        } catch (Exception e) {
            e.printStackTrace();
        }
    }
    
    @FXML
    public void openChangeLog() {
    	try {
            FXMLLoader loader = new FXMLLoader(getClass().getClassLoader().getResource("ChangeLogWindow.fxml"));
            Parent root = loader.load();      
            ChangeLogView controller = loader.getController();
            controller.initial();
            Stage popup = new Stage();
            popup.setTitle("Change Log");
            popup.setScene(new Scene(root));
            popup.show();

        } catch (Exception e) {
            e.printStackTrace();
        }
    }
    
    @FXML
    public void openNewGenomeInserted() {
    	try {
            FXMLLoader loader = new FXMLLoader(getClass().getClassLoader().getResource("NewGenomeWindow.fxml"));
            Parent root = loader.load();      
            NewGenomeView controller = loader.getController();
            controller.initial();
            Stage popup = new Stage();
            popup.setTitle("New Genome Inserted");
            popup.setScene(new Scene(root));
            popup.show();

        } catch (Exception e) {
            e.printStackTrace();
        }
    }
    
    @FXML
    public void openGenomeMap(){
    	try {
            FXMLLoader loader = new FXMLLoader(getClass().getClassLoader().getResource("DrawGenomeMap.fxml"));
            Parent root = loader.load();      
            DrawGenomeMapView controller = loader.getController();
            DrawGenomeMapComposer composer = new DrawGenomeMap(controller);
            ((DrawGenomeMapDialog)controller).initial(composer);

            List<Reference> selectedViruses = new ArrayList<>();
            for (TreeNode node : databaseSelection) {
                selectedViruses.add(node.getReference());
            }
            FileHandler fileCheck = new FileHandler(selectedViruses, localUploads, localFileA, localFileB, databaseForA,
                    databaseForB, selectedPreference);
            selectedViruses = fileCheck.readyGenomemapInterchange().getReferences();
            if (selectedViruses.size()!=1) {
            	// warning cannot map more than one viruses
            	return;
            }

            composer.setGenome(selectedViruses.get(0));

            Stage popup = new Stage();
            popup.setTitle("Draw Genome Map");
            popup.setScene(new Scene(root));
            popup.show();

        } catch (Exception e) {
            e.printStackTrace();
        }
    }
    
    
    @FXML
    private void goHome() {
        ((Stage) investigatorHome.getScene().getWindow()).close();
        List<FamilyItem> list = new ArrayList<>(FXCollections.observableArrayList(databaseList.getItems()));
        
        try {
            FXMLLoader loader = new FXMLLoader(getClass().getClassLoader().getResource("ViralEditorDialog.fxml"));
            Parent root = loader.load();

            ViralEditorView controller = loader.getController();
            ViralEditorComposer viralEditorComposer = new ViralEditor();
            ((ViralEditorDialog)controller).initComposer(viralEditorComposer);
            ((ViralEditor)viralEditorComposer).bind(controller);
            
            List<Family> families = new ArrayList<>();
            for (FamilyItem item : list) {
                families.add(item.getFamily());
            }
            viralEditorComposer.setDatabaseList(families);
            
            Stage popup = new Stage();
            popup.setTitle("Viral Editor");
            popup.setScene(new Scene(root));
            popup.show();
        } catch (IOException e) {
            e.printStackTrace();
        }
        
    }
    
    @FXML
    private void handleLaunch() {
        openApplication();
    }
    
    private List<Reference> checkForAnnotations(List<Reference> selectedViruses){
    	if (selectedPreference.equals("Selected Genes annotated onto sequences")) {
        	List<Reference> refList = new ArrayList<Reference>();
        	for (Reference virus: selectedViruses) {
        		if (!(virus.getAccession().isEmpty())) {
        			refList.add(virus);
        		}
        	}
        	return refList;
        }
        else {
        	List<Reference> refList = new ArrayList<Reference>();
        	return refList;
        }
    	
    	
    }
    
    /**
     * Opens the an application in a new window, based on the application selected when viral investigator was opened
     */
    private void openApplication() {
        List<Reference> selectedViruses = new ArrayList<>();
        for (TreeNode node : databaseSelection) {
            selectedViruses.add(node.getReference());
            
        }
        
        FileHandler fileCheck = new FileHandler(selectedViruses, localUploads, localFileA, localFileB, databaseForA,
                databaseForB, selectedPreference);

        try {
            switch (tool) {
            case ALIGNMENT:
                Interchange alignmentData = fileCheck.readyAlignmentReadyInterchange();
                List<Reference> annotationList = checkForAnnotations(selectedViruses);
                AlignmentComposer alignmentComposer = new AlignmentComposer(alignmentData, database, annotationList, selectedPreference);
                Performance<AlignmentDialog, AlignmentComposer> p = new Performance<>(
                        "Alignment_main", new Stage(), alignmentComposer, "Alignment/Alignment.fxml");
                p.render();
                break;
            case ANNOTATION:
                Pair<Interchange, Interchange> annotationData = fileCheck.readyAnnotationInterchange();
                AnnotatorDialog.openWindow(annotationData.getKey(), annotationData.getValue());
                break;
            case CODONSTATS:
                CodonStatsDialog.openWindow(fileCheck.readyCodonInterchange().getReferences());
                break;
            case DOTPLOT:
                Pair<Interchange, Interchange> dotplotData = fileCheck.readyDotPlotInterchange();
                DotPlotSessionDialog.openSessionWindow(dotplotData.getKey(), dotplotData.getValue());
                break;
            case GENOMEMAP:
                GenomeMapWindowDialog.openWindow(fileCheck.readyGenomeMapInterchange());
                break;
            case GRAPHDNA:
                GraphDNAWindowDialog controller = GraphDNAWindowDialog.openWindow(
                        fileCheck.readyGraphDNAInterchange().getReferences());
                controller.setDatabase(database);
                break;
            default:
                throw new IllegalArgumentException(String.format("Missing case for %s tool", tool.name));
            }
        } catch (FileHandler.NotReadyException e) {
            System.out.println("Missing or incorrect files given, try again");
            FileHandler.showErrorWindow(e.getMessage());
        } catch (Exception e) {
            System.out.println("Can't load window: " + tool.name);
            e.printStackTrace();
        }
    }
    
    
    //======= START: selected database viruses panel ===========
    /**
     * Opens a window for the user to choose where the database selection should be saved
     */
    @FXML
    void saveSelectedSetToFile() {
        try {
            Window window = saveSelectedButton.getScene().getWindow();
            List<Reference> references = new ArrayList<>();
            for (TreeNode node : databaseSelection) {
                references.add(node.getReference());
            }
            FileHandler.saveSubsetFile(references, database, window);
        }catch (IOException e){
            e.printStackTrace();
        }
    }
    
    /**
     * Opens a file chooser then if the subset file is valid the viruses will make up the selected list
     */
    @FXML
    void openSet() {
        FileChooser fc = new FileChooser();
        List<TreeItem<TreeNode>> toSelect;
        Pair<Family, List<Integer>> data;

        //open window and wait
        fc.getExtensionFilters().addAll(new FileChooser.ExtensionFilter("Text", "*.txt"));
        File file = fc.showOpenDialog(null);
        if (file == null){
            return;
        }

        try {
             data = FileHandler.readSubsetFile(file);
        }catch (Exception e){
            FileHandler.showErrorWindow(e.getMessage());
            return;
        }

        //set database and selection to match
        composer.setCurrentDatabase(data.getKey());

        toSelect = findAndSetViruses(data.getValue(), new ArrayList<>(this.dbRootNode.getChildren()));

        investigatorVirusList.getSelectionModel().clearSelection();
        for (TreeItem<TreeNode> virus : toSelect) {
            investigatorVirusList.getSelectionModel().select(virus);
        }
    }
    
    @FXML
    void clearSelection() {
        investigatorVirusList.getSelectionModel().clearSelection();
        this.databaseSelection.clear();
    }
    //======= END: selected database viruses ===========
    
    
    
    
    //======== START ========= Database analysis tool panel =======
    @FXML
    private void viewGeneCount() {
        investigatorCount.setText("12");
    }

    @FXML
    private void viewGenomeCount() {
    	investigatorCount.setText("13");
    }
    
    @FXML
    private void viewOrthologCount() {
    	investigatorCount.setText("13");
    }
    
    @FXML
    private void openGeneView() {
		try {
			FXMLLoader loader = new FXMLLoader(getClass().getClassLoader().getResource("GeneView.fxml"));
			Parent root = loader.load();
			
			GeneViewView controller = loader.getController();            
            GeneViewComposer composer = new GeneView(controller);
            ((GeneViewDialog)controller).initComposer(composer);
            composer.setGenes();
            
			Stage popup = new Stage();
			popup.setTitle("Gene View");
			popup.setScene(new Scene(root));
			popup.show();
	
	    } catch (Exception e) {
	        e.printStackTrace();
	    }
    }

    @FXML
    private void openGenomeView() {
    	try {
            FXMLLoader loader = new FXMLLoader(getClass().getClassLoader().getResource("GenomeView.fxml"));
            Parent root = loader.load();
            
            GenomeViewView controller = loader.getController();            
            GenomeViewComposer composer = new GenomeView(controller);
            ((GenomeViewDialog)controller).initComposer(composer);
            composer.setGenomes();
            
            Stage popup = new Stage();
            popup.setTitle("Genome View");
            popup.setScene(new Scene(root));
            popup.show();

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @FXML
    private void openOrthologView() {
    	try {
            FXMLLoader loader = new FXMLLoader(getClass().getClassLoader().getResource("OrthologView.fxml"));
            Parent root = loader.load();
            
            OrthologViewView controller = loader.getController();            
            OrthologViewComposer composer = new OrthologView(controller);
            ((OrthologViewDialog)controller).initComposer(composer);
            composer.setOrtholog();
            
            Stage popup = new Stage();
            popup.setTitle("Ortholog Group View");
            popup.setScene(new Scene(root));
            popup.show();

        } catch (Exception e) {
            e.printStackTrace();
        }
    }
    //======== END ========= Database analysis tool =======
    
    
    //===== for dotplot tool =======
    @FXML
    void openEmptyDotPlot() {
        DotPlotSequenceDialog.openWindow();
    }
    
     
  
    //===== methods for using local files ======

    /**
     * Opens file explorer and allows the user to upload a file
     * the selection is given to the composer and the UI is updated
     * @param event - Action event
     */
    @FXML
    private void upload(ActionEvent event) {
        //set up file chooser
        FileChooser fc = new FileChooser();

        if ( event.getSource() == Multiple_LocalUpload || event.getSource() == Annotation_uploadRef || event.getSource() == Codon_LocalUpload) {
            fc.getExtensionFilters().addAll(
                    new FileChooser.ExtensionFilter("All Sequences", "*.gbk", "*.gb", "*.fasta"),
                    new FileChooser.ExtensionFilter("GBK", "*.gbk", "*.gb"),
                    new FileChooser.ExtensionFilter("FASTA", "*.fasta")
            );

        } else if (event.getSource() == Annotation_uploadTarget || event.getSource() == DotPlot_uploadLeft || event.getSource() == DotPlot_uploadTop){
            fc.getExtensionFilters().addAll(
            new FileChooser.ExtensionFilter("FASTA", "*.fasta")
            );
        }

        //open window and wait
        File file = fc.showOpenDialog(null);
        if (file == null){
            return;
        }

        //What happens to the chosen file depends on the window; and update UI
        if (event.getSource() == Multiple_LocalUpload) {
            addFile(file);
            addLocalFile(file);
        } else if(event.getSource() == Annotation_uploadRef || event.getSource() == DotPlot_uploadLeft ) {
            setLocalFileA(file);
            LocalAName.setText(file.getName());
        } else if(event.getSource() == Annotation_uploadTarget || event.getSource() == DotPlot_uploadTop ) {
            setLocalFileB(file);
            LocalBName.setText(file.getName());
        } if (event.getSource() == Codon_LocalUpload) {
            setLocalFileA(file);
            LocalAName.setText(file.getName());
        }

    }

    /**
     * Checks each file of the dragged object to see if it is an accepted file (fasta or gnb)
     * If this is true, drop is allowed
     * @param event -
     */
    @FXML
    void allowDragAndDrop(DragEvent event) {
        Dragboard db = event.getDragboard();
        boolean allowed = true;
        if (event.getGestureSource() != localList && db.hasFiles()) {
            for(File file : db.getFiles()) {
                if (!FileHandler.isFastaOrGBK(file)) {
                    allowed = false;
                }
            }
        }
        if(allowed){
            event.acceptTransferModes(TransferMode.COPY);
        }
        event.consume();
    }

    /**
     * Adds each dropped file to UI and composer
     * @param event -
     */
    @FXML
    void handleFileDropped(DragEvent event) {
            Dragboard db = event.getDragboard();
            if (db.hasFiles()) {
                for (File file : db.getFiles()) {
                    addLocalFile(file);
                    addFile(file);
                }
            }
        event.setDropCompleted(true);
        event.consume();
    }


    //adds a file name to the local file list UI
    private void addLocalFile(File file){
        if(localFiles == null){
            localFiles =  FXCollections.observableArrayList();
            localList.setItems(localFiles);
        }
        localFiles.add(file.getName());
    }

    @FXML
    //removes selected local file from the UI and updates composer
    private void deleteAction(){
        int selectedItem = localList.getSelectionModel().getSelectedIndex();
        localFiles.remove(selectedItem);
        removeLocalFile(selectedItem);
    }




    /**
     * Toggles if the database is used instead of the local file A
     * (updates composer)
     */
    @FXML
    void toggleDatabaseForA() {
        this.databaseForA = !this.databaseForA;
    }

    /**
     * Toggles if the database is used instead of the local file B
     * (updates composer)
     */
    @FXML
    void toggleDatabaseForB() {
        this.databaseForB = !this.databaseForB;
    }

    /**
     * Sets label with name and updates composer
     */
    @FXML
    void setDatabaseA() {
        TreeItem<TreeNode> selectedItem = investigatorVirusList.getSelectionModel().getSelectedItem();
        if(selectedItem != null){
            DBAName.setText(selectedItem.getValue().toString());
            this.databaseSelection.set(0,selectedItem.getValue());
        }
    }

    /**
     * Sets label with name and updates composer
     */
    @FXML
    void setDatabaseB() {
        TreeItem<TreeNode> selectedItem = investigatorVirusList.getSelectionModel().getSelectedItem();
        if(selectedItem != null){
            DBBName.setText(selectedItem.getValue().toString());
            this.databaseSelection.set(1,selectedItem.getValue());
        }

    }

    @FXML
    void checkfileA() {
        this.databaseForA = toggleFile(checkLocalA, checkDBA, false);
    }
    @FXML
    void checkDBA() {
        this.databaseForA = toggleFile(checkLocalA, checkDBA, true);
    }

    @FXML
    void checkfileB() {
        this.databaseForB = toggleFile(checkLocalB, checkDBB, false);
    }

    @FXML
    void checkDBB() {
        this.databaseForB = toggleFile(checkLocalB, checkDBB, true);
    }


    private Boolean toggleFile(CheckBox local, CheckBox db, Boolean bool) {
        local.selectedProperty().setValue(!bool);
        db.selectedProperty().setValue(bool);
        return bool;
    }
    
    /**
     * Checks if the current tool is dot plot
     * @return true if the current application is dotplot
     */
    private boolean isDotplot(){
        return tool == Tool.DOTPLOT;
    }

    /**
     * Checks if the current tool needs single selection for database viruses
     * @return true if the current application is dotplot, annotation or condon stats
     */
    private boolean needSingular() {
        return tool == Tool.DOTPLOT || tool == Tool.ANNOTATION || tool == Tool.CODONSTATS;
    }
    
    /**
     * Finds and returns tree items that match the given virus IDS. If an ID is not
     * found it is ignored.
     * 
     * @param IDs   - lists of virus IDs that match database items
     * @param items - tree items for database items
     * @return - the tree items for viruses with the given IDs
     */
    public List<TreeItem<TreeNode>> findAndSetViruses(List<Integer> IDs, List<TreeItem<TreeNode>> items) {

        // Find all viruses in tree
        List<TreeItem<TreeNode>> viruses = new ArrayList<>();
        List<TreeItem<TreeNode>> nodes = new ArrayList<>();
        for (TreeItem<TreeNode> item : items) {
            Reference reference = item.getValue().getReference();

            if (reference.getFeatures(Genome.class).isEmpty()) {
                nodes.add(item);
            } else {
                viruses.add(item);
            }
        }
        
        // Find all matching viruses
        List<TreeItem<TreeNode>> toSelect = new ArrayList<>();
        databaseSelection.clear();
        for (Integer id : IDs) {
            for (TreeItem<TreeNode> virus : viruses) {
                Genome genome = virus.getValue().getReference().getFeatures(Genome.class).get(0);
                if (genome.getGenomeID() == id) {
                    toSelect.add(virus);
                    databaseSelection.add(virus.getValue());
                    break;
                }
            }
        }
        
        return toSelect;
    }


    /**
     * expands the database selection list so that dotplot can store in index 0/1
     */
    public void expandList(){
        databaseSelection.add(0,null);
        databaseSelection.add(1,null);
    }
    
    //getters
    public List<File> getLocalUploads(){
        return new ArrayList<>(localUploads);
    }
    
    /**
     * Adds a file to the list of local uploads
     * @param sequence - Uploaded file
     */
    public void addFile(File sequence) {
        localUploads.add(sequence);
    }

    /**
     * Removes the local file at the given index
     * @param index - position of file in localUploads list
     */
    public void removeLocalFile(int index) {
        localUploads.remove(index);
    }
    

    //setters
    public void setLocalFileA(File sequence) {
        localFileA = sequence;
    }

    public void setLocalFileB(File sequence) {
        localFileB = sequence;
    }
}
