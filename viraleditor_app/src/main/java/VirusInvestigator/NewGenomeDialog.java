package VirusInvestigator;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import DatabaseAnalysis.TableView;
import DatabaseAnalysis.TableViewComposer;
import DatabaseAnalysis.TableViewDialog;
import DatabaseAnalysis.TableViewView;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.DatePicker;
import javafx.scene.control.TextField;
import javafx.stage.Stage;


public class NewGenomeDialog implements NewGenomeView {
	
	 @FXML
	private DatePicker date;
	 
    @FXML
    private Button cancel;

    @FXML
    void onCancelClicked() {
    	((Stage) cancel.getScene().getWindow()).close();
    }

    @FXML
    void onSearchClicked() {
    	List<String> colName = new ArrayList<String>();
    	colName.addAll(Arrays.asList("Inserted On", "Genome Name", "Strain", "Genbank Accession"));
    	List<String> colFactory = new ArrayList<String>();
		colFactory.addAll(Arrays.asList("inserted_on","genome_name","strain","genbank_acc"));
		try {
            FXMLLoader loader = new FXMLLoader(getClass().getClassLoader().getResource("TableViewWindow.fxml"));
            Parent root = loader.load();      
            TableViewView controller = loader.getController();
            TableViewComposer composer = new TableView(controller);
            ((TableViewDialog)controller).initComposer(composer);
            controller.setTableColumnName(colName);
            controller.setTableFactory(colFactory);
            composer.setTable("NewGenome");
            Stage popup = new Stage();
            popup.setTitle("New Genome Inserted");
            popup.setScene(new Scene(root));
            popup.show();

        } catch (Exception e) {
            e.printStackTrace();
        }
    }
    
    public void initial() {
    	LocalDate myDate = LocalDate.now();
        date.setValue(myDate.plusYears(-4));
    }
}
