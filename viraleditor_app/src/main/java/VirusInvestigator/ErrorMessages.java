package VirusInvestigator;

public class ErrorMessages {

    public enum Warnings {
        ALIGNMENT("..."),
        ANNOTATIONDatabase("Exactly one database virus must be selected for reference"),
        ANNOTATIONNoTarget("A target file must be uploaded"),
        ANNOTATIONNoFiles("A target and reference file must be uploaded"),
        DBANALYSIS("Please select at least one virus from the database"),
        DOTNoSelection("Please select and update your virus of choice from the database"),
        DOTNoA("Please upload a file for the left axis"),
        DOTNoB("Please upload a file for the top axis"),
        ANNOTATION("A file for both the reference genome and target genome must be uploaded before launching"),
        GENOMEMAP("Please select or upload at least one virus"),
        CODONSTATSDatabase("Please select exactly one virus from the database"),
        CODONSTATSNoTarget("If 'Use local' is selected file must be uploaded"),;

        public final String message;

        Warnings (String message) {
            this.message = message;
        }
    }
}
