package VirusInvestigator.Menu;

public class Gene {
	private final boolean complement;
    private final int low;
    private final int high;
	private final String name;
	private final int familySize;
	
	public Gene(String name, boolean complement, int low, int high, int familySize) {
		this.name = name;
		this.low = low;
		this.high = high;
		this.complement = complement;
		this.familySize = familySize;
	}

	public boolean isComplement() {
		return complement;
	}

	public int getLow() {
		return low;
	}

	public int getHigh() {
		return high;
	}

	public String getName() {
		return name;
	}
	
	public int getFamilySize() {
		return familySize;
	}
	
}
