package VirusInvestigator.Menu;

import java.util.ArrayList;
import java.util.List;

import Shared.interchange.Genome;
import Shared.interchange.Interchange;
import Shared.interchange.Interval;
import Shared.interchange.Reference;

public class DrawGenomeMap implements DrawGenomeMapComposer{

	private DrawGenomeMapView bindView;
	
	public DrawGenomeMap(DrawGenomeMapView view) {
		this.bindView = view;
	}
	
	@Override
	public void setGenome(Reference data) {
		System.out.println(data.getFeatures(Shared.interchange.Gene.class).size());	
		bindView.setGeneList(geneListInGenome(),100000, data.getFeatures(Genome.class).get(0).getOrganismName());
	}
	
	private List<GenomePlot> geneListInGenome() {
		List<GenomePlot> geneList = new ArrayList<GenomePlot>();
		
		
		//TODO: connect to database
		
		geneList.add(new GenomePlot("ASFV-001",false, 1744, 2004, 40));
		geneList.add(new GenomePlot("ASFV-062",true, 1871, 3152, 30));
		geneList.add(new GenomePlot("ASFV-010",false, 4608, 7147, 20));
		geneList.add(new GenomePlot("ASFV-011",true, 8631, 9005, 50));
		geneList.add(new GenomePlot("ASFV-051",false, 39194, 43511, 50));
		geneList.add(new GenomePlot("ASFV-061",false, 57194, 67511, 15));
		geneList.add(new GenomePlot("ASFV-062",true, 67910, 72683, 50));
		geneList.add(new GenomePlot("ASFV-051",false, 79194, 83511, 20));
		geneList.add(new GenomePlot("ASFV-061",false, 87194, 92511, 15));
		geneList.add(new GenomePlot("ASFV-062",true, 97910, 98683, 20));
		geneList.add(new GenomePlot("ASFV-071",false, 98094, 99511, 60));
		geneList.add(new GenomePlot("ASFV-071",true, 100094, 100511, 40));
		return geneList;
	}
}
