package VirusInvestigator.Menu;

import java.util.List;

public interface DrawGenomeMapView {

	public void setGeneList(List<GenomePlot> geneList, int largestOrf, String genome);
}
