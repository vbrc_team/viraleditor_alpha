package VirusInvestigator.Menu;

import javafx.scene.paint.Color;

public class GenomePlot {
	private final boolean complement;
    private final int low;
    private final int high;
	private final String name;
	private final int familySize;
	private boolean changeLine;
	private double[] positionX;
	private double[] positionY;
	private Color color;
	
	public GenomePlot(String name, boolean complement, int low, int high, int familySize) {
		this.name = name;
		this.low = low;
		this.high = high;
		this.complement = complement;
		this.familySize = familySize;
		this.changeLine = false;
		this.color = getDefaultColour();
	}

	public boolean isComplement() {
		return complement;
	}

	public int getLow() {
		return low;
	}

	public int getHigh() {
		return high;
	}

	public String getName() {
		return name;
	}
	
	public int getFamilySize() {
		return familySize;
	}
	
	public boolean isChangeLine() {
		return changeLine;
	}
	
	public void setChangeLine() {
		this.changeLine = true;
	}

	public double[] getPositionX() {
		return positionX;
	}
	
	public double[] getPositionY() {
		return positionY;
	}

	public Color getColor() {
		return color;
	}

	public void setPositionX(double[] pos) {
		this.positionX = pos;
	}

	public void setPositionY(double[] pos) {
		this.positionY = pos;
	}

	public void setColor(Color color) {
		this.color = color;
	}
	
	
	private Color getDefaultColour() {
		if (this.familySize<=20) {
			return Color.YELLOW;
		} else if (this.familySize>20 && this.familySize <=40) {
			return Color.GREEN;
		} else {
			return Color.BLUE;
		}
	}
	
}
