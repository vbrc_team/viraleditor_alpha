package VirusInvestigator.Menu;


import java.util.Arrays;
import java.util.Collections;
import java.util.List;

import Shared.Windows.WarningPopupDialog;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.canvas.Canvas;
import javafx.scene.canvas.GraphicsContext;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.AnchorPane;
import javafx.scene.paint.Color;
import javafx.scene.paint.CycleMethod;
import javafx.scene.paint.LinearGradient;
import javafx.scene.paint.Stop;
import javafx.scene.text.Text;
import javafx.stage.Stage;

public class DrawGenomeMapDialog implements DrawGenomeMapView{
	

	@FXML
    private Canvas canvas1;
    @FXML
    private Canvas canvas2;
	
    @FXML
    private Text GenomeNameField;

    @FXML
    private Canvas LegendCanvas;
    
    
	private DrawGenomeMapComposer composer;
	private List<GenomePlot> geneList;
	
	private GraphicsContext gc;
	private GraphicsContext label;
	
	
	
	
	public void initial(DrawGenomeMapComposer composer) {	
		
		
	    this.gc = canvas1.getGraphicsContext2D(); 
        this.label = canvas2.getGraphicsContext2D();
//        gc.setFont(new Font(fontName, fontSize));
        gc.setFill(Color.BLUE);
        label.setFill(Color.BLACK);
        this.composer = composer;
//        canvas1.addEventHandler(MouseEvent.MOUSE_CLICKED, 
//                new EventHandler<MouseEvent>() {
//                    @Override
//                    public void handle(MouseEvent t) {
//                    	System.out.print(t.getClickCount());
//                    	System.out.print(t.getSceneX() + " "+ t.getSceneY());
//                        if (t.getClickCount() >1) {
//                            reset(canvas1, Color.BLUE);
//                            System.out.print(t.getSceneX() + " "+ t.getSceneY());
//                        }  
//                    }
//                });
    }
	
	@Override
	public void setGeneList(List<GenomePlot> geneList, int largestOrf, String genome) {
		
		GenomeNameField.setText(genome);
		this.geneList = geneList;
		drawBaseLine(largestOrf);
		for(GenomePlot gene: geneList){
			drawGene(gene);
		}
		drawLegend(Color.YELLOW, 10, 10, 0, 20);
		drawLegend(Color.GREEN, 100, 10, 21, 40);
		drawLegend(Color.BLUE, 200, 10, 41, 60);
	     
	}
	


    @FXML
    void onMouseClicked(MouseEvent t) {
    	System.out.print(t.getClickCount());
    	System.out.print(t.getSceneX() + " "+ t.getSceneY());
    	GenomePlot g = getSelectedGene(t.getSceneX(), t.getSceneY());
    	if (g == null) {
    		return;
    	}
    	try {
	        FXMLLoader loader = new FXMLLoader(getClass().getClassLoader().getResource("GenomePlotInfo.fxml"));
	        Parent root = loader.load();      
	        GenomePlotInfoDialog controller = loader.getController();
	        controller.initial(g, this);
	        Stage popup = new Stage();
	        popup.setTitle("Gene Info");
	        popup.setScene(new Scene(root));
	        popup.show();

	    } catch (Exception e) {
	        e.printStackTrace();
	    }
    }
	
	private void drawBaseLine(int largestOrf) {
 
        gc.setStroke(Color.BLACK);
        gc.setLineWidth(1);
        int start = 10;
        int end = 410;
        int pos = 0;
        int interval = (end-start)/8;
        int labelNum = 0;
        for (int i = 0; i < largestOrf/40000; i++) {
        	pos = 50 + 100*i;
        	gc.strokeLine(start, pos, end, pos);
            for(int j = 0; j < 9; j++) {
            	gc.strokeLine(start+(j*interval), pos-1, start+(j*interval), pos+1);
            }
            labelNum = labelNum + 40;
            label.fillText(labelNum+".0 kbp", end+10, pos);
        }
        pos = pos+100;
        end = start+interval*(largestOrf%40000/5000+1);
    	gc.strokeLine(start, pos, end, pos);
        for(int j = 0; j < (largestOrf%40000/5000+2); j++) {
        	gc.strokeLine(start+(j*interval), pos-1, start+(j*interval), pos+1);
        }
        label.fillText(labelNum+(end-start)/10+".0 kbp", end+10, pos);
        
    }
	
	public void drawGene(GenomePlot gene) {
		gc.setFill(gene.getColor());
		if (gene.getLow()/40000 == gene.getHigh()/40000) {
			int multi = gene.getLow()/40000;
			int pos = 50 + 100*multi;
			int start = 10 + (gene.getLow()-40000*multi)/100;
	        int end = 10 + (gene.getHigh()-40000*multi)/100;
	        int interval = 10;
	        if (end-start < interval) {
	        	interval = end-start;
	        }
	        if (! gene.isComplement()) {
	        	gene.setPositionX(new double[]{start, end-interval, end, end-interval, start});
	        	gene.setPositionY(new double[]{pos-15, pos-15, pos-10, pos-5, pos-5});
	            gc.fillPolygon(gene.getPositionX(),gene.getPositionY(), 5);
	            label.fillText(gene.getName(), start, pos-20);
	        } else {
	        	gene.setPositionX(new double[]{start, start+interval, end, end, start+interval});
	        	gene.setPositionY(new double[]{pos+10, pos+5, pos+5, pos+15, pos+15});
	        	gc.fillPolygon(gene.getPositionX(),gene.getPositionY(), 5);
	        	label.fillText(gene.getName(), start, pos+30);
	        }
		} else {
			int multi = gene.getLow()/40000;
			int pos = 50 + 100*multi;
			int start = 10 + (gene.getLow()-40000*multi)/100;
	        int end = 410;
	        int interval = 10;
	        if (end-start < interval) {
	        	interval = end-start;
	        }
	        if (! gene.isComplement()) {
	            gc.fillPolygon(new double[]{start, end, end, start},new double[]{pos-15, pos-15, pos-5, pos-5}, 4);
	            label.fillText(gene.getName(), start, pos-20);
	        } else {
	        	gc.fillPolygon(new double[]{start, start+interval, end, end, start+interval},new double[]{pos+10, pos+5, pos+5, pos+15, pos+15}, 5);
	        	label.fillText(gene.getName(), start, pos+30);
	        }
	        multi = gene.getHigh()/40000;
			pos = 50 + 100*multi;
			start = 10;
	        end = 10 + (gene.getHigh()-40000*multi)/100;
	        if (end-start < interval) {
	        	interval = end-start;
	        }
	        if (! gene.isComplement()) {
	            gc.fillPolygon(new double[]{start, end-interval, end, end-interval, start},
	                    new double[]{pos-15, pos-15, pos-10, pos-5, pos-5}, 5);
	        } else {
	        	gc.fillPolygon(new double[]{start, end, end, start},
	             	   new double[]{pos+5, pos+5, pos+15, pos+15}, 4);
	        }
		}
        

    }
	

	private void drawLegend(Color col, int posX, int posY,int begin, int end) {
		GraphicsContext legend = LegendCanvas.getGraphicsContext2D();
		legend.setStroke(Color.BLACK);
		legend.setFill(col);
		legend.fillRect(posX, posY, 10, 10);
		legend.strokeText(begin + "-" + end, posX+15, posY+10);
	}
	
	
	private GenomePlot getSelectedGene(double x, double y) {
		
		System.out.println(x+","+y);
		
		for(GenomePlot gene: geneList) {
			if (gene.getPositionX() == null) {
				continue;
			}
			System.out.println(Arrays.stream(gene.getPositionX()).min().getAsDouble()+","+Arrays.stream(gene.getPositionY()).min().getAsDouble());
			System.out.println(Arrays.stream(gene.getPositionX()).max().getAsDouble()+","+Arrays.stream(gene.getPositionY()).max().getAsDouble());
			
			if (x-15 >= Arrays.stream(gene.getPositionX()).min().getAsDouble() && x-15 <= Arrays.stream(gene.getPositionX()).max().getAsDouble()
			&& y-30 >= Arrays.stream(gene.getPositionY()).min().getAsDouble() && y-30 <= Arrays.stream(gene.getPositionY()).max().getAsDouble()) {
				System.out.print("Gene");
				return gene;
			}
		} 
		return null;
	}
	
	
//	private Color getGeneColour(GenomePlot gene) {
//		int size = gene.getFamilySize();
//		if (size<=20) {
//			return Color.YELLOW;
//		} else if (size>20 && size <=40) {
//			return Color.GREEN;
//		} else {
//			return Color.BLUE;
//		}
//	}
//	private void reset(Canvas canvas, Color color) {
//	    GraphicsContext gc = canvas.getGraphicsContext2D();
//	    gc.setFill(color);
//	    gc.fillRect(0, 0, canvas.getWidth(), canvas.getHeight());
//	}

}
