package VirusInvestigator.Menu;


import javafx.event.Event;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.scene.canvas.Canvas;
import javafx.scene.control.Button;
import javafx.scene.control.ColorPicker;
import javafx.scene.text.Text;
import javafx.stage.Stage;

public class GenomePlotInfoDialog implements GenomePlotInfoView{

    @FXML
    private Text infoText;

    @FXML
    private Canvas canvas;
    
    @FXML
    private ColorPicker colorPicker;
    
    @FXML
    private Button cancelButton;
    
    private DrawGenomeMapDialog controller;
    private GenomePlot gene;

    @FXML
    void cancel() {
    	((Stage) cancelButton.getScene().getWindow()).close();
    }

    @FXML
    void save() {
    	gene.setColor(colorPicker.getValue());  
    	controller.drawGene(gene);
    	((Stage) cancelButton.getScene().getWindow()).close();
    }

    @FXML
    void viewFamily() {

    }
    
    @Override
    public void initial(GenomePlot gene, DrawGenomeMapDialog controller) {
    	this.controller = controller;
    	this.gene = gene;
    	colorPicker.setValue(gene.getColor());
    	infoText.setText(gene.getName()+": "+gene.getLow()+" - "+gene.getHigh() + "\n"
    					+"Family: 274 Ankyrin (SPPV-N-14)" + "\n"
    					+"Family Size: #Genomes=208 #Genes=1538 #Genera=11" + "\n"
    					+"Genebank Name: CNPV011 ");
//    	colorPicker.setOnAction(new EventHandler() {
//            public void handle(Event t) {
//                gene.setColor(colorPicker.getValue());       
//            }
//        });
    }

}