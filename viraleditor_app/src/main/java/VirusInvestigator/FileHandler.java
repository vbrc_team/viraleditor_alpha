package VirusInvestigator;

import java.io.*;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.StandardCopyOption;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.apache.commons.io.FilenameUtils;
import org.biojava.nbio.core.sequence.DNASequence;
import org.biojava.nbio.core.sequence.io.GenbankReaderHelper;

import Shared.Handling.Database;
import Shared.interchange.*;
import javafx.scene.control.Alert;
import javafx.scene.control.ButtonType;
import javafx.stage.FileChooser;
import javafx.stage.Window;
import javafx.util.Pair;

public class FileHandler {
    private static final StringToNucleotides toNucleotides = new StringToNucleotides();

    private List<Reference> databaseSelection;
    private List<File> localUploads;
    private File localFileA;                        // Reference genome or left axis
    private File localFileB;                        // target genome or other axis
    private Boolean databaseForA;
    private Boolean databaseForB;
    private Database database = new Database();
    private String preference;


    public FileHandler(List<Reference> databaseSelection, List<File> localUploads, File localFileA, File localFileB,
            Boolean databaseForA, Boolean databaseForB, String preference) {
        this.databaseSelection = databaseSelection;
        this.localUploads = localUploads;
        this.localFileA = localFileA;
        this.localFileB = localFileB;
        this.databaseForA = databaseForA;
        this.databaseForB = databaseForB;
        this.preference = preference;
    }
    
    

    /**
     * Checks if Annotation had the necessary files to run, throws exception if not
     * @return interchanges for the reference and target genome
     */
    public Pair<Interchange, Interchange> readyAnnotationInterchange()
            throws NotReadyException {
        Interchange ref;
        Interchange target;
        if (databaseForA && databaseSelection.size() != 1) {
            throw new NotReadyException(ErrorMessages.Warnings.ANNOTATIONDatabase.message);

        } else if (databaseForA && (localFileB == null || !localFileB.exists())) {
            throw new NotReadyException(ErrorMessages.Warnings.ANNOTATIONNoTarget.message);

        } else if ((!databaseForA && (localFileA == null || !localFileA.exists())
                || (localFileB == null || !localFileB.exists()))) {
            throw new NotReadyException(ErrorMessages.Warnings.ANNOTATIONNoFiles.message);

        } else if (databaseForA) {
            throw new NotReadyException("Database data selection not implemented");
            // ref should be set from database and target from localFileB
        } else {
            ref = readFile(localFileA.toPath());
            target = readFile(localFileB.toPath());
        }

        return new Pair<>(ref, target);
    }

    
    /**
     * Checks if Genome Map had the necessary files to run, throws exception if not
     * @return interchanges of the local and database viruses
     */
    public Interchange readyGenomeMapInterchange() {
        List<Reference> references = new ArrayList<>();

        if (localUploads.isEmpty() && databaseSelection.isEmpty()) {
            throw new NotReadyException(ErrorMessages.Warnings.GENOMEMAP.message);
        }
        // check if any files have been moved since uploaded else create an interchange
        for (File file : localUploads) {
            if (!file.exists()) {
                throw new NotReadyException("File " + file.getName() + "has been moved or deleted, please reupload");
            }
            references.addAll(readFile(file.toPath()).getReferences());
        }

        // add interchange for database and return
        if (!databaseSelection.isEmpty()) {
            throw new NotReadyException("Database data selection not implemented");
        }

        return Interchange.builder().withReferences(references).build();
    }

    
    /**
     * Checks if graph DNA had the necessary files to run.
     * 
     * @return interchanges the local and database viruses
     * @throws IOException when querying the database for genome sequences fails
     */
    public Interchange readyGraphDNAInterchange() throws IOException {
        List<Reference> references = new ArrayList<>();

        // check if any files have been moved since uploaded else create an interchange
        for (File file : localUploads) {
            references.addAll(readFile(file.toPath()).getReferences());
        }

        // add interchange for database and return
        if (!databaseSelection.isEmpty()) {
            references.addAll(getGenomes(databaseSelection));
        }

        return Interchange.builder().withReferences(references).build();
    }

    
    /**
     * Checks if codon had the necessary files to run, throws exception if not
     *
     * @return interchanges for the local upload or database virus, as chosen by the
     *         user
     * @throws IOException when querying the database for genome sequences fails
     */
    public Interchange readyCodonInterchange() throws IOException {
        Interchange interchange;

        if (localFileA == null && !databaseForA) {
            throw new NotReadyException(ErrorMessages.Warnings.CODONSTATSNoTarget.message);

        } else if (!databaseForA) {
            if (localFileA.exists()) {
                interchange = readFile(localFileA.toPath());
                // Convert CDS features to Annotation
                List<Reference> references = new ArrayList<>();
                references.add(Reference.builder().withFeatures(readGenes(interchange)).build());
                interchange = Interchange.builder().withReferences(references).build();
            } else {
                throw new NotReadyException("File " + localFileA.getName() + "has been moved or deleted, please reupload");
            }

        } else if (databaseSelection.size() != 1) {
            throw new NotReadyException(ErrorMessages.Warnings.CODONSTATSDatabase.message);
        } else {
            interchange = Interchange.builder().withReferences(getGenes(getGenomes(databaseSelection))).build();
        }

        return interchange;
    }


    /**
     * Checks if alignments uploaded files still exist and creates interchange.
     * 
     * @return interchange with one reference for each selected database and local
     *         sequence
     * @throws IOException when querying the database for genome sequences fails
     */
    public Interchange readyAlignmentReadyInterchange() throws IOException {
        List<Reference> references = new ArrayList<>();
        // check if any files have been moved since uploaded else create an interchange
        for (File file : localUploads) {
            if (!file.exists()) {
                throw new NotReadyException("File " + file.getName() + "has been moved or deleted, please reupload");
            }

            references.addAll(readFile(file.toPath()).getReferences());
        }

        // add interchange for database and return
        if (!databaseSelection.isEmpty()) {

        	//adding genes to the reference list, if any were selected
        	List annotationNames = new ArrayList();
        	for (Reference ref: databaseSelection) {
        		
        		if (ref.getFeatures(Annotation.class).size() != 0) {
        			annotationNames.add(ref);
        		}
        		
        	}
        	references.addAll(getGenomes(references));
        }
        

        return Interchange.builder().withReferences(references).build();
    }


    /**
     * Checks if dot plot had the necessary files to run, throws exception if not
     * @return interchanges for the left and top axis stored in a pair
     */
    public Pair<Interchange, Interchange> readyDotPlotInterchange() {
        Interchange A;
        Interchange B;
        // check database selection
        if ((databaseForA && databaseSelection.get(0) == null) || (databaseForB && databaseSelection.get(1) == null)) {
            System.out.println(databaseSelection.get(0));
            System.out.println(databaseSelection.get(1));
            throw new NotReadyException(ErrorMessages.Warnings.DOTNoSelection.message);
        }

        // check file A
        if (!databaseForA && localFileA == null) {
            throw new NotReadyException(ErrorMessages.Warnings.DOTNoA.message);
        } else if (!databaseForA) {
            if (localFileA.exists()) {
                A = readFile(localFileA.toPath());
            } else {
                throw new NotReadyException(
                        "File " + localFileA.getName() + "has been moved or deleted, please reupload");
            }
        } else {
            A = Interchange.builder().build();
        }

        // check file B
        if (!databaseForB && localFileB == null) {
            throw new NotReadyException(ErrorMessages.Warnings.DOTNoB.message);
        } else if (!databaseForB) {
            if (localFileB.exists()) {
                B = readFile(localFileB.toPath());
            } else {
                throw new NotReadyException(
                        "File " + localFileB.getName() + "has been moved or deleted, please reupload");
            }
        } else {
            B = Interchange.builder().build();
        }
        return new Pair<>(A, B);
    }

    
    /**
     * Checks if genome map had the necessary files to run.
     * 
     * @return interchanges the local and database viruses
     * @throws IOException when querying the database for genome sequences fails
     */
    public Interchange readyGenomemapInterchange() throws IOException {
        List<Reference> references = new ArrayList<>();

        // check if any files have been moved since uploaded else create an interchange
        for (File file : localUploads) {
            references.addAll(readFile(file.toPath()).getReferences());
        }

        // add interchange for database and return
        if (!databaseSelection.isEmpty()) {
            references.addAll(getGenomes(databaseSelection));
        }

        return Interchange.builder().withReferences(references).build();
    }
    
    
    public static void showErrorWindow(String message){
        Alert alert = new Alert(Alert.AlertType.NONE, message, ButtonType.OK);
        alert.setTitle("File Warning");
        alert.showAndWait();
    }

    /**
     * Checks if the file is accepted by the current tool
     * @param file - file
     * @return true if the file can be accepted
     */
    public static boolean isFastaOrGBK(File file){
        String extension = FilenameUtils.getExtension(file.getName());
        switch (extension){
            case "fasta":
            case "gb":
            case "gbk":
                return true;
            default:
                return false;
        }
    }


    /**
     * Purpose: Checks to see if the file follows the following structure: int -
     * String
     * 
     * @param file - path to the subset file
     * @return - database and list of virus ids or exception if file is invalid
     *         Imported and modified from VOCS 2015/2020
     */
    public static Pair<Family, List<Integer>> readSubsetFile(File file) throws Exception {
        List<Integer> IDList = new ArrayList<>();
        String[] tokens;

        if(file.exists()){
            String dir = file.getAbsolutePath();
            ArrayList<String> openFile = readFile(dir);
            if (openFile == null) {
                throw new Exception("File can not be read");
            }

            //get database name
            String idName = openFile.remove(0);
            Pair<Family, List<Integer>> data;
            Family database;
            if (idName.contains("|")) {
                String[] pair = idName.split("\\|");
                List<Interval> intervals = new ArrayList<>();
                intervals.add(new Interval(false, 1, 2));
                database = Family.builder()
                                 .withIntervals(intervals)
                                 .withFamilyName(pair[0])
                                 .withFamilyID(Integer.parseInt(pair[1]))
                                 .build();
                data = new Pair<>(database, IDList);
            } else {
                throw new Exception("First line of file violates format");
            }

            // Viewing information stored in the file
            for (String s : openFile) {
                tokens = s.split(" - ");
                if (tokens.length != 2 || !tokens[0].matches("\\d+")) {
                    throw new Exception("Invalid subset file format");
                }
                IDList.add(Integer.parseInt(tokens[0]));
            }
            return data;
        }
        throw new Exception("File does not exist");
    }

    /**
     * For each reference get genes and their sequences from the database.
     * <p>
     * Genes are searched based on the Genome feature in each reference.
     *
     * @param references references to look for genes
     * @return references with genes added from database
     * @throws IOException when querying the database for genome sequences fails
     */
    private List<Reference> getGenes(List<Reference> references) throws IOException {
        List<Integer> genomeIDs = new ArrayList<>();
        for (Reference reference : references) {
            if (!reference.getFeatures(Genome.class).isEmpty()) {
                genomeIDs.add(reference.getFeatures(Genome.class).get(0).getGenomeID());
            }
        }

        return database.genomes(genomeIDs).withSequences().queryGenes().getReferences();
    }

    /**
     * Query database for Genomes, their sequences, and {@code Annotation} features. As well 
     * as any genes that were selected.
     * <p>
     * 
     *
     * @param genomes references with a genome feature
     * @return references with sequence data, genome features, and annotation
     *         features
     * @throws IOException when querying the database for genome sequences fails
     */
    private List<Reference> getGenomes(List<Reference> genomes) throws IOException {

    	List<Integer> genomeIDs = new ArrayList<>();
        boolean Annotation = false;
        for (Reference reference : databaseSelection) {
        	if (reference.getFeatures(Genome.class).size() >0) {
            genomeIDs.add(reference.getFeatures(Genome.class).get(0).getGenomeID());
        	}
        	else if (reference.getFeatures(Annotation.class).size() > 0) {
        		genomeIDs.add(reference.getFeatures(Annotation.class).get(0).getGenomeID());
        	}
        }
        List<Reference> sequences = database.genomes(genomeIDs).withSequences().queryGenomes().getReferences();
        List<Reference> genes = database.genomes(genomeIDs).queryGenes().getReferences();
        List<Reference> genomesWithGenes = new ArrayList<>();
        if (sequences.size() != genes.size()) {
            //throw new IllegalStateException();
        }
        
        List<Reference> seqsAndGenes = new ArrayList<Reference>();
        seqsAndGenes.addAll(sequences);
        seqsAndGenes.addAll(databaseSelection);
        for (int i = 0; i < seqsAndGenes.size(); i++) {
            List<Feature> features = new ArrayList<>();
            features.addAll(seqsAndGenes.get(i).getFeatures(Feature.class));
            if (!(seqsAndGenes.get(i).getOrigin().getSequence().equals(""))){
            	features.addAll(genes.get(i).getFeatures(Feature.class));
            	if (!(preference.equals("Display only Genes as Nucleotides") || preference.equals("Display only Genes as Amino Acids"))) {
            	genomesWithGenes.add(Reference.builder().withReference(seqsAndGenes.get(i)).withFeatures(features).build());
            	}
        }
            else {
            	if (!(seqsAndGenes.get(i).getVersion().equals(""))){
            		int Low = Integer.valueOf(seqsAndGenes.get(i).getVersion().split("-")[0]);
            		int High = Integer.valueOf(seqsAndGenes.get(i).getVersion().split("-")[1]);
            		String geneName = (seqsAndGenes.get(i).getVersion().split("-")[2]);
            		for (Reference ref: seqsAndGenes) {
            			if (ref.getFeatures(Genome.class).get(0).getOrganismName().equals(geneName) && !(preference.equals("Selected Genes annotated onto sequences"))){
            				genomesWithGenes.add(Reference.builder().withReference(ref).withFeatures(features).withOrigin(ref.getOrigin().subSequence(Low-1, High)).withAccession(seqsAndGenes.get(i).getAccession()).build());
            				break;
            			}
            		}
            		
            	}
            }
        }
        return genomesWithGenes;
    }

    private Interchange readFile(Path file) {
        int lastIndex = file.toString().lastIndexOf(".");
        
        String fileExtension;
        if (lastIndex >= 0 && lastIndex + 1 < file.toString().length()) {
            fileExtension = file.toString().substring(lastIndex + 1);
        } else {
            fileExtension = ""; 
        }
        
        Interchange interchange;
        switch (fileExtension) {
        case "gb":
        case "gbk":
            try {
                Map<String, DNASequence> genBank;
                genBank = GenbankReaderHelper.readGenbankDNASequence(Files.newInputStream(file));

                InterchangeToNTGenBank fromGenBank = new InterchangeToNTGenBank();
                interchange = fromGenBank.from(genBank);
            } catch (Exception e) {
                throw new NotReadyException(String.format("Unable to read %s as GenBank file", file.toString()));
            }
            break;
        case "fasta":
            InterchangeToFASTA fromFASTA = new InterchangeToFASTA();

            try {
                List<String> lines = Files.readAllLines(file);
                interchange = fromFASTA.from(String.join(System.lineSeparator(), lines));
            } catch (IOException e) {
                throw new NotReadyException(String.format("Unable to read %s as FASTA file", file.toString()));
            }
            break;
        default:
            throw new NotReadyException(String.format("Unsupported file extension: %s", fileExtension));
        }

        return interchange;
    }

    /**
     * 
     * @param interchange
     * @return annotations created from references with GenBank features
     */
    private List<Feature> readGenes(Interchange interchange) {
        List<Feature> annotations = new ArrayList<>();
        for (Reference reference : interchange.getReferences()) {
            if (reference.getOrigin().getSequence().equals("")) {
                continue;
            }
            
            String origin = reference.getOrigin().getSequence();
            
            for (CDS cds : reference.getFeatures(CDS.class)) {
                StringBuilder builder = new StringBuilder();
                for (Interval interval : cds.getIntervals()) {
                    if (interval.isComplement()) {
                        String reversed = origin.substring(interval.getLow() - 1, interval.getHigh());
                        reversed = new StringBuilder(reversed).reverse().toString();
                        builder.append(reversed);
                    } else {
                        builder.append(origin.substring(interval.getLow() - 1, interval.getHigh()));
                    }
                }
                
                annotations.add(Annotation.builder()
                                          .withIntervals(cds.getIntervals())
                                          .withGeneAbbreviation(cds.getGene())
                                          .withNTSequence(new NucleotideSequence(toNucleotides.to(builder.toString())))
                                          .build());
            }

            for (Description description : reference.getFeatures(Description.class)) {
                annotations.add(Annotation.builder()
                                          .withIntervals(description.getIntervals())
                                          .withGeneAbbreviation(description.getDescription())
                                          .withNTSequence(new NucleotideSequence(
                                                  toNucleotides.to(reference.getOrigin().getSequence())))
                                          .build());
            }

        }

        return annotations;
    }

    /**
     * Saves a file containing a set of viruses
     * format: ID - Name
     * @param dbItems - Selected DB items
     * @param database - the current database
     * @param window - current stage
     * @throws IOException
     * Imported and modified from VOCS, 2015/2020
     */
    public static void saveSubsetFile(List<Reference> dbItems, Family database, Window window) throws IOException {
        File tempFile = new File("temp.txt");
        FileChooser fileChooser = new FileChooser();

        if (dbItems.isEmpty()) {
            showErrorWindow("No viruses have been selected");
            return;
        }

        // Store all virus names into 'content' for saving
        StringBuilder content = new StringBuilder();
        content.append(database.getFamilyName());
        content.append("|");
        content.append(database.getFamilyID());
        content.append("\n");
        for (Reference virus : dbItems) {
            if (!virus.getFeatures(Genome.class).isEmpty()) {
                Genome genome = virus.getFeatures(Genome.class).get(0);
                content.append(genome.getGenomeID());
                content.append(" - ");
                content.append(genome.getOrganismName());
                content.append("\n");
            }
        }

        FileWriter fw = new FileWriter(tempFile.getAbsoluteFile());
        try (BufferedWriter bw = new BufferedWriter(fw)) {
            bw.write(content.toString());
        }

        //copy file to where the user chooses
        fileChooser.setTitle("Save Virus Subset");
        fileChooser.getExtensionFilters().add( new FileChooser.ExtensionFilter("Text", "*.txt"));
        fileChooser.setInitialFileName("Virus_Set");
        File destination = fileChooser.showSaveDialog(window);
        if (destination != null) {
            try {
                Files.copy(tempFile.toPath(), destination.toPath(), StandardCopyOption.REPLACE_EXISTING);
            }catch(IOException e){
                System.out.println("Problem with copying file");
                e.printStackTrace();
            }
        }
        tempFile.delete();
    }


    /**
     *  Open and read a file, and return the lines in the file as a list
     * @param fileDir - path to the file
     * @return - the lines in the file
     * Imported and modified from VOCS 2015/2020
     */
    private static ArrayList<String> readFile(String fileDir) {
        ArrayList<String> records = new ArrayList<>();

        try (BufferedReader reader = new BufferedReader(new FileReader(fileDir))) {
            String line;
            while ((line = reader.readLine()) != null) {
                records.add(line);
            }

            return records;
        } catch (Exception e) {
            // Error with the file. Most likely it does not exist
            showErrorWindow("Error Opening file");
            e.printStackTrace();
            return null;
        }
    }

    
    
    public static class NotReadyException extends RuntimeException {
        public NotReadyException(String message) {
            super(message);
        }
    }


}
