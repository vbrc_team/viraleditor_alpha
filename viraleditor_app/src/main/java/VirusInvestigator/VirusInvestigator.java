package VirusInvestigator;

import java.io.IOException;
import java.io.File;
import java.util.*;

import Shared.TreeNode;
import Shared.Handling.Database;
import Shared.interchange.*;
import javafx.scene.control.TreeItem;

public class VirusInvestigator implements VirusInvestigatorComposer {

    private VirusInvestigatorView boundView;
    private TreeItem<TreeNode> dbRootNode;
    private Family selectedDatabase;
    
    public void bind(VirusInvestigatorView view) {
    	this.boundView = view;
    }

    @Override
    public void setDatabaseList(List<Family> databases, Family current) {
    	boundView.setDatabaseList(databases, current);
    	boundView.setGenePreference();
    	setCurrentDatabase(current);
    }
    
    @Override
    public void setCurrentDatabase(Family current) {
        this.dbRootNode = getTree(current);
    	boundView.setUpDatabase(dbRootNode);
        selectedDatabase = current;
    }
    
    @Override
    public String getDatabaseName() {
        return selectedDatabase.getFamilyName();
    }

    /**
     * Return a TreeItem that serves as the root of a taxonomy tree for the virus
     * family. Leaf nodes will contain {@link Reference} with a Genome feature for
     * each virus.
     * <p>
     * The database will be queried to obtain viruses. A TreeItem with no children
     * will be returned when the query fails.
     *
     * @param family
     * @return all nodes under the family with leaf nodes being viruses
     */
    private TreeItem<TreeNode> getTree(Family family) {
        List<Interval> intervals = new ArrayList<>();
        intervals.add(new Interval(false, 1, 2));
        List<Feature> features = new ArrayList<>();
        features.add(Family.builder().withIntervals(intervals).withFamilyName("Root").build());
        Reference root = Reference.builder().withFeatures(features).build();
        dbRootNode = new TreeItem<>(new TreeNode("Root", root));

        List<Reference> viruses;
        List<Reference> taxonomyNodes;
        HashMap<String, List<Reference>> genes;
        try {
            viruses = getViruses(family);
            taxonomyNodes = getTaxonomyNodes(family);

           
        } catch (IOException e) {
        	e.printStackTrace();
            return dbRootNode;
        }
        try {
            genes = getGenesFromViruses(viruses);
        } catch(IOException e) {
        	HashMap<String, List<Reference>> genesError = new HashMap<String, List<Reference>>();
        	List<Reference> geneError = new ArrayList<Reference>();
        	genesError.put("error", geneError);
        	return constructTree(family, viruses, taxonomyNodes, genesError);
        }
        

        return constructTree(family, viruses, taxonomyNodes, genes);
    }

    /**
     * Get viruses for a specific family.
     *
     * @param family virus family
     * @return references with one {@code Genome} for virus
     * @throws IOException when families cannot be obtained
     */
    private List<Reference> getViruses(Family family) throws IOException {
        Database database = new Database();
        List<Integer> familyIDs = new ArrayList<>();
        familyIDs.add(family.getFamilyID());
        return database.families(familyIDs).queryGenomes().getReferences();
    }
    
    /**
     * Fetches gene information from the virus information, returns a Hashmap containing the name of the virus
     * and a list of genes that virus has within the database
     */

    private HashMap<String, List<Reference>> getGenesFromViruses(List<Reference> viruses) throws IOException {
    	Database database = new Database();
    	HashMap<String, List<Reference>> geneMap = new HashMap<String, List<Reference>>();
    	List<Integer> genomeIDs = new ArrayList<>();
    	List<Class<? extends Feature>> classes = new ArrayList<>();
        classes.add(Annotation.class);
        classes.add(Gene.class);
        
        
    	for (Reference reference: viruses) {
    		
    		List<Reference> tempList = new ArrayList<Reference>();
    		tempList.add(reference);
    		Interchange tempInt = Interchange.builder().withReferences(tempList).build();
    		FileHandler handle = new FileHandler(tempList, new ArrayList<>(), new File("ao"), new File("ee"), true,
                true, "Sequences and Genes separate");
    		tempInt = handle.readyAlignmentReadyInterchange();
    		for (Reference ref: tempInt.getReferences()) {
    			geneMap.put(reference.getFeatures(Genome.class).get(0).getOrganismName()  , tempInt.getReferences());
    		}
    		
    	}

    	return geneMap;
    }
    
    /**
     * Get taxonomy nodes for a specific family.
     *
     * @param family virus family
     * @return references with one {@code TaxonomyNode} for virus
     * @throws IOException when taxonomy nodes cannot be obtained
     */
    private List<Reference> getTaxonomyNodes(Family family) throws IOException {
        List<Reference> taxonomyNodes = new ArrayList<>();
        for (Reference reference : new Database().queryTaxnodes().getReferences()) {
            if (reference.getFeatures(TaxonomyNode.class).get(0).getFamilyID() == family.getFamilyID()) {
                taxonomyNodes.add(reference);
            }
        }

        return taxonomyNodes;
    }

    /**
     * Construct a taxonomy tree rooted at a family. Includes genes if information is available.
     * <p>
     *
     * @param root          family to server as root of taxonomy tree
     * @param viruses       references with a single {@code Genome} for each leaf
     *                      node
     * @param taxonomyNodes references with a single {@code TaxonomyNode} for each
     *                      internal node
     * @return
     */
    private TreeItem<TreeNode> constructTree(Family family, List<Reference> viruses, List<Reference> taxonomyNodes, HashMap<String, List<Reference>> genes) {
        TreeItem<TreeNode> rootNode = findRoot(family, taxonomyNodes);
        Integer rootNodeID = rootNode.getValue().getReference().getFeatures(TaxonomyNode.class).get(0).getNodeID();

        boolean change = true;
        Map<Integer, TreeItem<TreeNode>> internalNodes = new HashMap<>();
        internalNodes.put(rootNodeID, rootNode);
        while (change) {
            change = false;
            ListIterator<Reference> iterator = taxonomyNodes.listIterator();
            while (iterator.hasNext()) {
                Reference reference = iterator.next();
                TaxonomyNode node = reference.getFeatures(TaxonomyNode.class).get(0);
                if (internalNodes.containsKey(node.getParentNodeID())) {
                    TreeItem<TreeNode> internalNode = new TreeItem<>(new TreeNode(node.getName(), reference));
                    internalNodes.get(node.getParentNodeID()).getChildren().add(internalNode);
                    internalNodes.put(node.getNodeID(), internalNode);

                    iterator.remove();
                    change = true;
                }
            }
        }

        for (Reference virus : viruses) {
            Genome genome = virus.getFeatures(Genome.class).get(0);
            if (internalNodes.containsKey(genome.getTaxnodeID())) {
            	TreeItem temp = new TreeItem<>(new TreeNode(genome.getOrganismName(), virus));
                internalNodes.get(genome.getTaxnodeID())
                             .getChildren()
                             .add(temp);
                             
                int tempIndex = internalNodes.get(genome.getTaxnodeID())
                			 .getChildren()
                			 .indexOf(temp);
                
                
                             
                
                int x = 0;
                int count = 0;
                if (genes.get(genome.getOrganismName()) != null) {
                while (x<genes.get(genome.getOrganismName()).get(0).getFeatures(Annotation.class).size()) {
                	int High = genes.get(virus.getFeatures(Genome.class).get(0)
            				 .getOrganismName()).get(0).getFeatures(Annotation.class).get(x).getIntervals().get(0).getHigh();
                	int Low = genes.get(virus.getFeatures(Genome.class).get(0)
           				 .getOrganismName()).get(0).getFeatures(Annotation.class).get(x).getIntervals().get(0).getLow();
                	String geneName = virus.getFeatures(Genome.class).get(0).getOrganismName();
                	Reference tempRef = Reference.builder().withReference(virus).withAccession(genes.get(virus.getFeatures(Genome.class).get(0)
           				 .getOrganismName()).get(0).getFeatures(Annotation.class).get(x).getGeneAbbreviation()).withVersion(Integer.toString(Low)+"-"+Integer.toString(High)+"-"+geneName).build();
                	internalNodes.get(genome.getTaxnodeID())
                     		 .getChildren()
                     		 .get(tempIndex)
                     		 .getChildren()
                     		 
                     		 .add(new TreeItem<>(new TreeNode(genes.get(virus.getFeatures(Genome.class).get(0)
                     				 .getOrganismName()).get(0).getFeatures(Annotation.class).get(x).getGeneAbbreviation(), 
                     				 tempRef)));
                	 x++;
                 	if (internalNodes.get(genome.getTaxnodeID()).getChildren().size() < count) {
                		count++;
                 	}
                 	else {
                 		count = 0;
                 	}
                     		 
                }
                }
                
                			
            }
        }
        for (TreeItem<TreeNode> internalNode : internalNodes.values()) {
            if (internalNode.getChildren().isEmpty()) {
                internalNode.getParent().getChildren().remove(internalNode);
            }
        }

        return rootNode;
    }

    /**
     * Find the root node for family in a list of taxonomy nodes.
     * <p>
     * The taxonomy nodes must contain the root node.
     *
     * @param family        family of root node
     * @param taxonomyNodes nodes with root node for family
     * @return
     */
    private TreeItem<TreeNode> findRoot(Family family, List<Reference> taxonomyNodes) {
        for (Reference reference : taxonomyNodes) {
            TaxonomyNode node = reference.getFeatures(TaxonomyNode.class).get(0);
            if (node.getName().equals(family.getFamilyName())) {
                return new TreeItem<>(new TreeNode(family.getFamilyName(), reference));
            }
        }

        throw new IllegalArgumentException("Taxonomy nodes missing data for root node");
    }
}
