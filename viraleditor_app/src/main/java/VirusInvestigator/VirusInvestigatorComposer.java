package VirusInvestigator;

import java.util.List;

import Shared.interchange.Family;

public interface VirusInvestigatorComposer {

    public void setDatabaseList(List<Family> databases, Family current);
	
    public void setCurrentDatabase(Family current);
	
	public String getDatabaseName();
}
