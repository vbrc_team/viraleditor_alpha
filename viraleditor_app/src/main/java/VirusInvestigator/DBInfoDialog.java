package VirusInvestigator;

import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.TextField;
import javafx.stage.Stage;

public class DBInfoDialog implements DBInfoView{

    @FXML
    private TextField databaseName;

    @FXML
    private TextField databaseServerSystem;

    @FXML
    private TextField genomesNum;

    @FXML
    private TextField genesNum;

    @FXML
    private TextField OrthlogNum;

    @FXML
    private TextField OrtSize1;

    @FXML
    private TextField OrtSize2;

    @FXML
    private TextField OrtSize3;

    @FXML
    private TextField OrtSize4;

    @FXML
    private TextField OrtSize5;

    @FXML
    private TextField OrtSize6;

    @FXML
    private TextField OrtSize7;
    
    @FXML
    private Button ok;
    
    @Override
    public void setDBInfo(VirusInvestigatorComposer composer) {
    	databaseName.setText(composer.getDatabaseName());
    	databaseServerSystem.setText("52.35.121.80");
    	genomesNum.setText("9");
    	genesNum.setText("91");
    	OrthlogNum.setText("23");
    	OrtSize1.setText("4");
    	OrtSize2.setText("14");
    	OrtSize3.setText("54");
    	OrtSize4.setText("210");
    	OrtSize5.setText("115");
    	OrtSize6.setText("45");
    	OrtSize7.setText("4");
    	
    }
    
    @FXML
    void close() {
    	((Stage) ok.getScene().getWindow()).close();
    }
}
