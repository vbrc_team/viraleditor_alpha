package VirusInvestigator;

import javafx.application.Platform;
import javafx.collections.FXCollections;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.ChoiceBox;
import javafx.scene.control.DatePicker;
import javafx.scene.control.Label;
import javafx.scene.control.MenuButton;
import javafx.scene.control.SkinBase;
import javafx.scene.control.TextField;
import javafx.stage.Stage;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import DatabaseAnalysis.GenomeView;
import DatabaseAnalysis.GenomeViewComposer;
import DatabaseAnalysis.GenomeViewDialog;
import DatabaseAnalysis.TableView;
import DatabaseAnalysis.TableViewComposer;
import DatabaseAnalysis.TableViewDialog;
import DatabaseAnalysis.TableViewView; 

public class ChangeLogDialog implements ChangeLogView{
	
	@FXML
	private ChoiceBox<String> tableChoice;

	@FXML
	private ChoiceBox<String> actionChoice;

    @FXML
    private DatePicker startDate;

    @FXML
    private DatePicker endDate;
    
    @FXML
    private Button cancel;

    @FXML
    public void onCancelClicked() {
    	((Stage) cancel.getScene().getWindow()).close();
    }

    @FXML
    public void onSearchClicked() {
    	List<String> colName = new ArrayList<String>();
    	colName.addAll(Arrays.asList("Date", "Subject/Table", "Action", "Curator", "Discription of change applied"));
    	List<String> colFactory = new ArrayList<String>();
		colFactory.addAll(Arrays.asList("date","subject","action","curator","description"));
    	
    	try {
            FXMLLoader loader = new FXMLLoader(getClass().getClassLoader().getResource("TableViewWindow.fxml"));
            Parent root = loader.load();      
            TableViewView controller = loader.getController();
            TableViewComposer composer = new TableView(controller);
            ((TableViewDialog)controller).initComposer(composer);
            controller.setTableColumnName(colName);
            controller.setTableFactory(colFactory);
            composer.setTable("ChangeLog");
         
            Stage popup = new Stage();
            popup.setTitle("Change Log");
            popup.setScene(new Scene(root));
            popup.show();

        } catch (Exception e) {
            e.printStackTrace();
        }
    }
    
    public void initial() {
    	LocalDate myDate = LocalDate.now();
        startDate.setValue(myDate.plusYears(-4));
    	endDate.setValue(myDate);
    	
    	tableChoice.getItems().addAll("genome","ortholog group","seq_center");
    	setChoiceBoxLabel(tableChoice,"Tables (e.g. gene)");
    	actionChoice.getItems().addAll("Insert","Update");
    	setChoiceBoxLabel(actionChoice,"Action (e.g. Insert)");
    }
    
    private void setChoiceBoxLabel(ChoiceBox<String> box,String title) {
    	Platform.runLater(() -> {
    	    SkinBase<ChoiceBox<String>> skin = (SkinBase<ChoiceBox<String>>) box.getSkin();
    	    for (Node child : skin.getChildren()) {
    	        if (child instanceof Label) {
    	            Label label = (Label) child;
    	            if (label.getText().isEmpty()) {
    	                label.setText(title);
    	            }
    	            return;
    	        }
    	    }
    	});
    }
}
