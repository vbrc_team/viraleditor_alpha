package GenomeMap;

import Shared.interchange.Interchange;

public class GenomeMapWindow implements GenomeMapWindowComposer {

	private GenomeMapWindowView genomeMapWindowView;
	
	
    public void bindGenomeMapWindowView(GenomeMapWindowView view) {
    	genomeMapWindowView = view;
    }
    
    @Override
    public void setGenes(Interchange genes) {
    	genomeMapWindowView.setGenes(genes);
    }

    
}
