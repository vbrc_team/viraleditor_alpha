package GenomeMap;

import Shared.interchange.Interchange;

public interface GenomeMapWindowComposer {

    public void setGenes(Interchange genes);
}
