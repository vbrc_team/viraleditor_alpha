package GenomeMap;


import java.io.IOException;

import Shared.interchange.Interchange;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.layout.VBox;
import javafx.stage.Stage;

public class GenomeMapWindowDialog implements GenomeMapWindowView{

	/**
     * Opens the genome map window with the given sequences loaded
     * 
     * @param data - genomes to be mapped
	 * @return 
     * @return - the controller
     * @throws IOException
     */
    public static GenomeMapWindowDialog openWindow(Interchange data) throws IOException {
        FXMLLoader loader = new FXMLLoader(GenomeMapWindowDialog.class.getClassLoader().getResource("GenomeMapWindow.fxml"));
        Parent root = loader.load();
        GenomeMapWindowDialog controller = loader.getController();
        controller.setGenes(data);

        Stage popup = new Stage();
        popup.setTitle("Genome Map");
        popup.setScene(new Scene(root));
        popup.show();

        return controller;
    }
	
    @Override
    public void setGenes(Interchange genes) {
		System.out.print(genes);
		
	}
	

    @FXML
    private VBox mapFrame;

    
    

    @FXML
    public void exportToJPG() {
    	System.out.print("TODO: exportToJPG");
    }
    
    @FXML
    public void closeOrganism() {
    	System.out.print("TODO: closeOrganism");
    }
    
    @FXML
    public void addCompositionGraph() {
    	System.out.print("TODO: addCompositionGraph");
    }
    
    @FXML
    public void importAnalysis() {
    	System.out.print("TODO: importAnalysis");
    }
    
    @FXML
    public void mapSettings() {
    	System.out.print("TODO: mapSettings");
    }
    
    @FXML
    public void sequenceInfo() {
    	System.out.print("TODO: sequenceInfo");		
    }
    
    @FXML
    public void framedTranslation() {
    	System.out.print("TODO: framedTranslation");
    }
    

    

    private void setUp(){
        addMap();addMap();addMap();
    }


    /**
     * Given a genome, adds a corresponding map to the window
     */
    public void addMap(){
        try {
            Node node = FXMLLoader.load(getClass().getClassLoader().getResource("GenomeMap.fxml"));
            mapFrame.getChildren().add(node);
        }catch ( IOException e){
            System.out.println("failed to add map");
            e.printStackTrace();
        }
    }
}
