package GenomeMap;

import Shared.interchange.Interchange;

public interface GenomeMapWindowView {
	
    public void setGenes(Interchange genes);
}
