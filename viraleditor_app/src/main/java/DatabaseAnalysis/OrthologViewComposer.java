package DatabaseAnalysis;

import Shared.interchange.Reference;
import javafx.scene.control.ListView;

public interface OrthologViewComposer {
	
	/**
	 * Set the ortholog used by the ortholog view composer
	 * 
	 */
	public void setOrtholog();
	
	/**
	 * Get the missing viruses data used by the missing viruses window
	 * 
	 * @param ortholog
	 */
    public ListView<Reference> getMissingViruses();
	
	/**
	 * set the search sequences list to sequence search window
	 * @param controller
	 */
	public void setSearchSequencesList(SequenceSearchView controller, String sequenceType);

}
