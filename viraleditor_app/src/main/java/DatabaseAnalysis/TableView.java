package DatabaseAnalysis;

import java.util.ArrayList;
import java.util.List;

import DatabaseAnalysis.structures.ChangeLog;
import DatabaseAnalysis.structures.GeneTable;



public class TableView implements TableViewComposer{
	
	TableViewView boundView;
	
	
	public TableView(TableViewView view) {
		this.boundView = view;
	}
	
	@Override
	public void setTable(String tableName) {
		
		List<Object> rows = new ArrayList<Object>();
		
		
		switch(tableName) {
		case "Upstream":
			GeneTable gene = new GeneTable();
			gene.UpstreamTable();
			rows.add(gene);
			rows.add(gene);
			this.boundView.setTableRows(rows);
			break;
		case "ChangeLog":
			ChangeLog change = new ChangeLog();
			rows.add(change);
			rows.add(change);
			this.boundView.setTableRows(rows);
			break;
		case "NewGenome":
			ChangeLog genome = new ChangeLog();
			genome.NewGenome();
			rows.add(genome);
			rows.add(genome);
			this.boundView.setTableRows(rows);
			break;
		default:
			break;
		}
	}

}
