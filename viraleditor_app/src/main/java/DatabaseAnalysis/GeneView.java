package DatabaseAnalysis;

import java.util.ArrayList;
import java.util.List;

import DatabaseAnalysis.structures.GeneTable;
import DatabaseAnalysis.structures.Sequence;

public class GeneView implements GeneViewComposer{
	GeneViewView boundView;
	
	public GeneView(GeneViewView view) {
		this.boundView = view;
	}
	
	
	@Override
	public void setGenes() {
		List<GeneTable> genes = new ArrayList<GeneTable>();
		genes.add(new GeneTable());
		genes.add(new GeneTable());
		boundView.setGeneTable(genes);
	}
	
	@Override
	public List<String> getGeneDetail(String geneName) {
		
		//TODO: fake data, modify when database is accessible
		List<String> geneDetail = new ArrayList<String> ();
		return geneDetail;
	}
	
	@Override
	public List<String> getGenomeSelectionList(){
		
		//TODO: fake data, modify when database is accessible
		List<String> genomes = new ArrayList<String> ();
		genomes.add("genomeA");
		genomes.add("genomeB");
		genomes.add("genomeC");
		return genomes;
	}
	
	@Override
	public void setSearchSequencesList(SequenceSearchView controller, String sequenceType) {
		
		//TODO: fake data, modify when database is accessible
		List<Sequence> searchSeq = new ArrayList<Sequence> ();
		searchSeq.add(new Sequence("CoV2-WuhanHu1_19_12_Chn-006", 
				"ATGGCAGATTCCAACGGTACTATTACCGTTGAAGAGCTTAAAAAGCTCCTTGAACAATGGAACCTAGTAATAGGTTTCCTATTCCTTACATGGATTTGTCTTCTACAATTTGCCTATGCCAACAGGAATAGGTTTTTGTATATAATTAAGTTAATTTTCCTCTGGCTGTTATGGCCAGTAACTTTAGCTTGTTTTGTGCTTGCTGCTGTTTACAGAATAAATTGGATCACCGGTGGAATTGCTATCGCAATGGCTTGTCTTGTAGGCTTGATGTGGCTCAGCTACTTCATTGCTTCTTTCAGACTGTTTGCGCGTACGCGTTCCATGTGGTCATTCAATCCAGAAACTAACATTCTTCTCAACGTGCCACTCCATGGCACTATTCTGACCAGACCGCTTCTAGAAAGTGAACTCGTAATCGGAGCTGTGATCCTTCGTGGACATCTTCGTATTGCTGGACACCATCTAGGACGCTGTGACATCAAGGACCTGCCTAAAGAAATCACTGTTGCTACATCACGAACGCTTTCTTATTACAAATTGGGAGCTTCGCAGCGTGTAGCAGGTGACTCAGGTTTTGCTGCATACAGTCGCTACAGGATTGGCAACTATAAATTAAACACAGACCATTCCAGTAGCAGTGACAATATTGCTTTGCTTGTACAGTAA",
				sequenceType));
		controller.setSearchSeqList(searchSeq);
	}
}
