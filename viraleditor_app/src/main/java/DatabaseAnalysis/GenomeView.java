package DatabaseAnalysis;

import java.util.ArrayList;
import java.util.List;

import DatabaseAnalysis.structures.GenomeTable;
import DatabaseAnalysis.structures.Sequence;

public class GenomeView implements GenomeViewComposer{
	
	GenomeViewView boundView;
	
	public GenomeView(GenomeViewView view) {
		this.boundView = view;
	}
	
	@Override
	public void setGenomes() {
    	List<GenomeTable> genomes = new ArrayList<GenomeTable>();
    	genomes.add(new GenomeTable());
    	genomes.add(new GenomeTable());
		boundView.setGenomeTable(genomes);
	}
	
	
	@Override
	public void setSearchSequencesList(SequenceSearchView controller, String sequenceType) {
		
		//TODO: fake data, modify when database is accessible
		List<Sequence> searchSeq = new ArrayList<Sequence> ();
		searchSeq.add(new Sequence("CoV2-WuhanHu1_19_12_Chn-006", 
				"ATGGCAGATTCCAACGGTACTATTACCGTTGAAGAGCTTAAAAAGCTCCTTGAACAATGGAACCTAGTAATAGGTTTCCTATTCCTTACATGGATTTGTCTTCTACAATTTGCCTATGCCAACAGGAATAGGTTTTTGTATATAATTAAGTTAATTTTCCTCTGGCTGTTATGGCCAGTAACTTTAGCTTGTTTTGTGCTTGCTGCTGTTTACAGAATAAATTGGATCACCGGTGGAATTGCTATCGCAATGGCTTGTCTTGTAGGCTTGATGTGGCTCAGCTACTTCATTGCTTCTTTCAGACTGTTTGCGCGTACGCGTTCCATGTGGTCATTCAATCCAGAAACTAACATTCTTCTCAACGTGCCACTCCATGGCACTATTCTGACCAGACCGCTTCTAGAAAGTGAACTCGTAATCGGAGCTGTGATCCTTCGTGGACATCTTCGTATTGCTGGACACCATCTAGGACGCTGTGACATCAAGGACCTGCCTAAAGAAATCACTGTTGCTACATCACGAACGCTTTCTTATTACAAATTGGGAGCTTCGCAGCGTGTAGCAGGTGACTCAGGTTTTGCTGCATACAGTCGCTACAGGATTGGCAACTATAAATTAAACACAGACCATTCCAGTAGCAGTGACAATATTGCTTTGCTTGTACAGTAA",
				sequenceType));
		controller.setSearchSeqList(searchSeq);
	}
}
