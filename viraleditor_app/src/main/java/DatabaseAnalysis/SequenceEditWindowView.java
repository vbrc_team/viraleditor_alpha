package DatabaseAnalysis;

import DatabaseAnalysis.structures.Sequence;

public interface SequenceEditWindowView {
	
	/**
	 * initial the window comments depends on window type
	 * 
	 * @param composer
	 * @param winType
	 * @param sequence
	 */
	public void initial(SequenceSearchView parentView, String winType, Sequence sequence);
}
