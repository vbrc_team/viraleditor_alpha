package DatabaseAnalysis;


import java.util.List;

import DatabaseAnalysis.structures.ColumnsHeader;
import javafx.beans.property.BooleanProperty;
import javafx.beans.value.ObservableValue;
import javafx.fxml.FXML;
import javafx.scene.control.ListView;
import javafx.scene.control.TableView;
import javafx.scene.control.cell.CheckBoxListCell;
import javafx.util.Callback;
import javafx.util.StringConverter;

public class ColumnSelectionDialog implements ColumnSelectionView{

	@FXML
    private ListView<ColumnsHeader> columnList;
	
	@Override
	public void setColumnList(TableView<?> table, List<ColumnsHeader> columnsName) {
		columnList.getItems().addAll(columnsName);
		StringConverter<ColumnsHeader> converter = new StringConverter<ColumnsHeader>() {
            @Override
            public String toString(ColumnsHeader header) {
                return header.getHeader();
            }
			@Override
			public ColumnsHeader fromString(String string) {
				return null;
			}
        };
        Callback<ColumnsHeader, ObservableValue<Boolean>> listener =  new Callback<ColumnsHeader, ObservableValue<Boolean>>() {
            @Override
            public ObservableValue<Boolean> call(ColumnsHeader header) {
                BooleanProperty observable = header.selectedProperty();
                observable.addListener((obs, wasSelected, isNowSelected) -> {
                    if (isNowSelected) {
                    	table.getColumns().get(columnsName.indexOf(header)).setVisible(true);
                    } else {
                    	table.getColumns().get(columnsName.indexOf(header)).setVisible(false);
                    }
                    columnList.getSelectionModel().select(header);
                });
                return observable;
            }
        };
		columnList.setCellFactory(CheckBoxListCell.forListView(listener, converter));
	}

}
