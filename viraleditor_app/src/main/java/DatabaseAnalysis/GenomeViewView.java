package DatabaseAnalysis;

import java.util.List;

import DatabaseAnalysis.structures.GenomeTable;

public interface GenomeViewView {
	
	/**
	 * set the genome data to the table in genome view window
	 * @param genomeTable
	 */
	public void setGenomeTable(List<GenomeTable> genomes);
}
