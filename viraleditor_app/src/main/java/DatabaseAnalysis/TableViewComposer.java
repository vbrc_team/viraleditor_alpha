package DatabaseAnalysis;


public interface TableViewComposer {
	/**
	 * set table data to tableview depends on table name
	 */
	public void setTable(String tableName);
}
