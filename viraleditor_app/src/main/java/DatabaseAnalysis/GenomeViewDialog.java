package DatabaseAnalysis;

import java.awt.Toolkit;
import java.awt.datatransfer.Clipboard;
import java.awt.datatransfer.StringSelection;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileWriter;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardCopyOption;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;


import DatabaseAnalysis.structures.ColumnsHeader;
import DatabaseAnalysis.structures.GenomeTable;
import Shared.Windows.WarningPopupDialog;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.print.Printer;
import javafx.print.PrinterJob;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.SelectionMode;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.stage.FileChooser;
import javafx.stage.Stage;
import javafx.stage.Window;

 
public class GenomeViewDialog implements GenomeViewView{

    private GenomeViewComposer composer;

    @FXML
    private TableView<GenomeTable> genomeTable;
    
    @FXML
    private TableColumn<GenomeTable, Integer> GenomeId;

    @FXML
    private TableColumn<GenomeTable, String> GenomeName;

    @FXML
    private TableColumn<GenomeTable, String> GenomeAbbr;

    @FXML
    private TableColumn<GenomeTable, Integer> BasepairsNum;

    @FXML
    private TableColumn<GenomeTable, Integer> GenesNum;

    @FXML
    private TableColumn<GenomeTable, Integer> GenbankGI;

    @FXML
    private TableColumn<GenomeTable, String> GenbankAccession;

    @FXML
    private TableColumn<GenomeTable, Integer> GenbankVersion;

    @FXML
    private TableColumn<GenomeTable, Integer> GenbankTid;

    @FXML
    private TableColumn<GenomeTable, String> GenbankDate;
    
    @FXML
    private TableColumn<GenomeTable, String> GenbankDescription;

    @FXML
    private TableColumn<GenomeTable, String> IsComplete;

    @FXML
    private TableColumn<GenomeTable, String> IsPublished;

    @FXML
    private TableColumn<GenomeTable, String> Strain;

    @FXML
    private TableColumn<GenomeTable, String> Species;

    @FXML
    private TableColumn<GenomeTable, String> MoleculeType;

    @FXML
    private TableColumn<GenomeTable, String> Genus;

    @FXML
    private TableColumn<GenomeTable, String> LastModified;

    
    public void initComposer(GenomeViewComposer composer) {
        this.composer = composer;
    }
    
    @Override
    public void setGenomeTable(List<GenomeTable> genomes) {
    	GenomeId.setCellValueFactory(new PropertyValueFactory<>("genome_id"));
    	GenomeName.setCellValueFactory(new PropertyValueFactory<>("genome_name"));
    	GenomeAbbr.setCellValueFactory(new PropertyValueFactory<>("genome_abbr"));
    	BasepairsNum.setCellValueFactory(new PropertyValueFactory<>("basepairs_num"));
    	GenesNum.setCellValueFactory(new PropertyValueFactory<>("genes_num"));
    	GenbankGI.setCellValueFactory(new PropertyValueFactory<>("genbank_gi"));
    	GenbankAccession.setCellValueFactory(new PropertyValueFactory<>("genbank_accession"));
    	GenbankVersion.setCellValueFactory(new PropertyValueFactory<>("genbank_version"));
    	GenbankTid.setCellValueFactory(new PropertyValueFactory<>("genbank_tid"));
    	GenbankDate.setCellValueFactory(new PropertyValueFactory<>("genbank_date"));
    	GenbankDescription.setCellValueFactory(new PropertyValueFactory<>("genbank_description"));
    	IsComplete.setCellValueFactory(new PropertyValueFactory<>("is_complete"));
    	IsPublished.setCellValueFactory(new PropertyValueFactory<>("is_published"));
    	Strain.setCellValueFactory(new PropertyValueFactory<>("strain"));
    	Species.setCellValueFactory(new PropertyValueFactory<>("species"));
    	MoleculeType.setCellValueFactory(new PropertyValueFactory<>("molecule_type"));
    	Genus.setCellValueFactory(new PropertyValueFactory<>("genus"));
    	LastModified.setCellValueFactory(new PropertyValueFactory<>("last_modified"));
    	
    	ObservableList<GenomeTable> value = FXCollections.observableList(genomes);
    	genomeTable.setItems(value);
    	genomeTable.getSelectionModel().setSelectionMode(SelectionMode.MULTIPLE);

    }
    
   
    /** 
     * Write whole table to a txt file
     * @throws IOException
     */
    @FXML
    private void writeToFile() throws IOException {
    	writeTxtFile(getHeader() + getAllItems(),"genomes");
    }
    
    /**
     * Write selected rows in table to a txt file
     * @throws IOException
     */
    @FXML
    private void writeSelectToFile() throws IOException {
    	if (genomeTable.getSelectionModel().getSelectedItems().isEmpty()) {
    		warning("Please select the rows you need");
    		return;
    	}
    	writeTxtFile(getHeader() + getSelectedItems(),"genomes");
    	
    }
    
    /** 
     * export whole table to an html file
     * @throws IOException
     */
    @FXML
    private void exportToHtml() throws IOException {
    	writeHtml(getAllItemsHtml());
    }
    
    /** 
     * export selected rows in table to an html file
     * @throws IOException
     */
    @FXML
    private void exportSelectToHtml() throws IOException {
    	if (genomeTable.getSelectionModel().getSelectedItems().isEmpty()) {
    		warning("Please select the rows you need");
    		return;
    	}
    	writeHtml(getSelectedItemsHtml());
    }
    
    /** 
     * print whole table view
     */
    @FXML
    private void printAll() {
    	print(genomeTable);
    }
    
    /** 
     * print selected row in table view
     */
    @FXML
    private void printSelectedItems() {
    	//TODO: print selected rows
    	print(genomeTable);
    }
    
    @FXML
	public void close() {
		((Stage) genomeTable.getScene().getWindow()).close();
	}
    
    @FXML
    private void selectAll() {
    	genomeTable.getSelectionModel().selectAll();
    }
    
    @FXML
    private void deselectAll() {
    	genomeTable.getSelectionModel().clearSelection();
    }
    
    /**
     * put selected table rows into clipboard
     */
    @FXML
    private void copySelectedItem() {
    	if (genomeTable.getSelectionModel().getSelectedItems().isEmpty()) {
    		warning("Please select the rows you need");
    		return;
    	}
    	StringSelection stringSelection = new StringSelection(getSelectedItems());
    	Clipboard clipboard = Toolkit.getDefaultToolkit().getSystemClipboard();
    	clipboard.setContents(stringSelection, null);
    }
    
    /**
     * Open the column selection window 
     * transmit the orthalog table to column selection
     */
    @FXML
    private void openColumnSelection() {
    	try {
            FXMLLoader loader = new FXMLLoader(getClass().getClassLoader().getResource("ColumnSelection.fxml"));
            Parent root = loader.load();
                   
            ColumnSelectionView controller = loader.getController();
            controller.setColumnList(genomeTable, getColumnsName());
            
            Stage popup = new Stage();
            popup.setTitle("Column Select");
            popup.setScene(new Scene(root));
            popup.show();

        } catch (Exception e) {
            e.printStackTrace();
        }
    }
    
    /**
     * save table column setting into a text file
     * @throws IOException
     */
    @FXML
    private void saveSettingAs() throws IOException {
    	String copied = "#genomes\n";
    	int index = 0;
		for (TableColumn<GenomeTable, ?> tableColumn: genomeTable.getColumns()) {
			copied += index + "," + tableColumn.getText() + "=" + tableColumn.isVisible() + "|" + tableColumn.getWidth() + "\n";
			index++;
		}
		copied = copied.substring(0, copied.length() - 1);
    	writeTxtFile(copied,"genomesColumnSetting");
    }
    
    /**
     * read the default column setting file
     */
    @FXML
    private void restoreDefaultSetting() {
    	Path currentRelativePath = Paths.get("");
    	String s = currentRelativePath.toAbsolutePath().toString() + "/src/main/resources/DatabaseAnalysisColumns";
    	File columns = new File(s + "/GenomesColumnSetting.txt");
    	getColumnSettingFromFile(columns);
    }
    
    /**
     * read the column setting file from client side
     */
    @FXML
    private void restoreSettingFrom() {
    	FileChooser fc = new FileChooser();
        //open window and wait
        fc.getExtensionFilters().addAll(new FileChooser.ExtensionFilter("Text", "*.txt"));
        File columns = fc.showOpenDialog(null);
        if (columns == null){
            return;
        }
        getColumnSettingFromFile(columns);
    }
    
    @FXML
    private void onDnaSearchClicked() {
    	openSequenceSearchWindow("DNA");
    }
    
    @FXML
    private void onUpstreamSearchClicked() {
    	openSequenceSearchWindow("Upstream");
    }
    
    @FXML
    private void onProteinSearchClicked() {
    	openSequenceSearchWindow("Protein");
    }
    
    /**
     * open the sequence search window depends on search type
     * @param searchType
     */
    private void openSequenceSearchWindow(String sequenceType) {
    	try {
            FXMLLoader loader = new FXMLLoader(getClass().getClassLoader().getResource("SequenceSearcherWindow.fxml"));
            Parent root = loader.load();      
            SequenceSearchView controller = loader.getController();
            SequenceSearchComposer searchComposer = new SequenceSearch(controller);
            controller.initial(sequenceType, searchComposer);
            composer.setSearchSequencesList(controller, sequenceType);
            Stage popup = new Stage();
            popup.setTitle("Sequence Searcher");
            popup.setScene(new Scene(root));
            popup.show();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
    
    /**
	 * get columns name from genome table view
	 * 
	 * @param columns
	 * @return
	 */
	private List<ColumnsHeader> getColumnsName() {
		List<ColumnsHeader> columnsName = new ArrayList<>();
		for (TableColumn<GenomeTable,?> column: genomeTable.getColumns()) {
			columnsName.add(new ColumnsHeader(column.getText(), column.isVisible()));
		}
		return columnsName;
	}
	
	/**
	 * get table header with comma between each
	 */
	private String getHeader() {
		String copied = "";
		for (TableColumn<GenomeTable, ?> tableColumn: genomeTable.getVisibleLeafColumns()) {
			copied += tableColumn.getText()  + ",";
		}
		copied += "\n";
		return copied;
	}
	
	/**
	 * get selected table items with comma between each
	 */
	private String getSelectedItems() {
    	String copied = "";
    	for(GenomeTable row:genomeTable.getSelectionModel().getSelectedItems()) {
    		for (TableColumn<GenomeTable, ?> column: genomeTable.getVisibleLeafColumns()) {
    			copied += column.getCellData(row) + ",";
    		}
    		copied += "\n";
    	}
    	return copied;
	}
	
	/**
	 * get all table items with comma between each
	 */
	private String getAllItems() {
    	String copied = "";
    	for(GenomeTable row:genomeTable.getItems()) {
    		for (TableColumn<GenomeTable, ?> column: genomeTable.getVisibleLeafColumns()) {
    			copied += column.getCellData(row) + ",";
    		}
    		copied += "\n";
    	}
    	return copied;
	}
	
	/**
	 * write text file to a selected place with popup window
	 * @param text
	 * @param initName
	 * @throws IOException
	 */
	private void writeTxtFile(String text, String initName) throws IOException {
		Window window = genomeTable.getScene().getWindow();
        
        File tempFile = new File("temp.txt");
        FileChooser fileChooser = new FileChooser();

        FileWriter fw = new FileWriter(tempFile.getAbsoluteFile());
        BufferedWriter bw = new BufferedWriter(fw);
        bw.write(text);
        bw.close();

        //copy file to where the user chooses
        fileChooser.setTitle("Save");
        fileChooser.getExtensionFilters().add( new FileChooser.ExtensionFilter("Text", "*.txt"));
        fileChooser.setInitialFileName(initName);
        File destination = fileChooser.showSaveDialog(window);
        if (destination != null) {
            try {
                Files.copy(tempFile.toPath(), destination.toPath(), StandardCopyOption.REPLACE_EXISTING);
            }catch(IOException e){
                System.out.println("Problem with copying file");
                e.printStackTrace();
            }
        }
        tempFile.delete();
	}
	
	/**
	 * get table header in html style
	 * @return
	 */
	private String getHeaderHtml() {
		String content = "";
		for (TableColumn<GenomeTable, ?> tableColumn : genomeTable.getVisibleLeafColumns()) {
			content += "    <TD><B>" + tableColumn.getText()  + "</B></TD>" + "\n";
		}
		return content;
	}
	
	/**
	 * get selected table text in html style
	 * @return
	 */
	private String getSelectedItemsHtml() {
		String content = "";
		for(GenomeTable row:genomeTable.getSelectionModel().getSelectedItems()) {
			content += "  <TR>" + "\n";
    		for (TableColumn<GenomeTable, ?> column: genomeTable.getVisibleLeafColumns()) {
    			String align = "right";
    			content += "    <TD><DIV ALIGN=" + align + ">" + column.getCellData(row) + "</DIV></TD>" + "\n"; 
    		}
    		content += "  </TR>" + "\n";
    	}
    	return content;
	}
	
	/**
	 * get all table text in html style
	 * @return
	 */
	private String getAllItemsHtml() {
		String content = "";
		for(GenomeTable row:genomeTable.getItems()) {
			content += "  <TR>" + "\n";
    		for (TableColumn<GenomeTable, ?> column: genomeTable.getVisibleLeafColumns()) {
    			String align = "right";
    			content += "    <TD><DIV ALIGN=" + align + ">" + column.getCellData(row) + "</DIV></TD>" + "\n"; 
    		}
    		content += "  </TR>" + "\n";
    	}
    	return content;
	}
	
	/**
	 * put table text into html structure, and write it to a new file.
	 * @param text
	 * @throws IOException
	 */
	private void writeHtml(String text) throws IOException {
		String projectTitle = "Genome";
		String m_tableTitle = "name";
		String content = "<HTML>" + "\n"
					   + "<HEAD><TITLE>" + projectTitle + " - " + m_tableTitle + "</TITLE></HEAD>" + "\n"
					   + "<BODY>" + "\n"
					   + "<H1>" + projectTitle + "</H1>" + "\n"
					   + "<H2>" + m_tableTitle + "</H2>" + "\n"
					   + "<TABLE BORDER=2>" + "\n"
					   + "  <TR>" + "\n"
					   + getHeaderHtml()
					   + "  </TR>" + "\n"
					   + text
					   + "</TABLE>" + "\n"
					   + "</BODY>" + "\n"
					   + "</HTML>";
		Window window = genomeTable.getScene().getWindow();
        
        File tempFile = new File("temp.html");
        FileChooser fileChooser = new FileChooser();

        FileWriter fw = new FileWriter(tempFile.getAbsoluteFile());
        BufferedWriter bw = new BufferedWriter(fw);
        bw.write(content);
        bw.close();

        //copy file to where the user chooses
        fileChooser.setTitle("Save");
        fileChooser.getExtensionFilters().add( new FileChooser.ExtensionFilter("HTML", "*.html"));
        fileChooser.setInitialFileName("genomes");
        File destination = fileChooser.showSaveDialog(window);
        if (destination != null) {
            try {
                Files.copy(tempFile.toPath(), destination.toPath(), StandardCopyOption.REPLACE_EXISTING);
            }catch(IOException e){
                System.out.println("Problem with copying file");
                e.printStackTrace();
            }
        }
        tempFile.delete();
	}
	
	/**
	 * Dealing with printer popup window and printer settings
	 * @param node - here is table view
	 */
	private void print(Node node) {
		List<Printer> printerList = new ArrayList<Printer>(Printer.getAllPrinters());
        Printer printer = printerList.get(0);
        PrinterJob job = PrinterJob.createPrinterJob(printer);
        if (job != null  && job.showPrintDialog(genomeTable.getScene().getWindow()) ) { 
            boolean success = job.printPage(node);                   
            if (success) {
                job.endJob();
            }
        }
	}
	
	/**
	 * read column information from columns file and implement them in the table
	 * @param columns
	 */
	private void getColumnSettingFromFile(File columns) {
		Scanner myReader;
		try {
			myReader = new Scanner(columns);
			if (!myReader.nextLine().equals("#genomes")){
				warning("Please check your column setting file has correct structure.");
				myReader.close();
				return;
			}
	        while (myReader.hasNextLine()) {
	          String[] data = myReader.nextLine().split(",|=|\\|");
	          genomeTable.getColumns().get(Integer. parseInt(data[0])).setVisible(Boolean.parseBoolean(data[2]));
	          genomeTable.getColumns().get(Integer. parseInt(data[0])).setPrefWidth(Double.parseDouble(data[3]));
	        }
	        myReader.close();
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		}
	}
	
	/**
	 * Open warning window and show warning sentence
	 * @param warning
	 */
	private void warning(String warning) {
		try {
	        FXMLLoader loader = new FXMLLoader(getClass().getClassLoader().getResource("WarningPopup.fxml"));
	        Parent root = loader.load();      
	        WarningPopupDialog controller = loader.getController();
	        controller.initial(warning);
	        Stage popup = new Stage();
	        popup.setTitle("Warning");
	        popup.setScene(new Scene(root));
	        popup.show();

	    } catch (Exception e) {
	        e.printStackTrace();
	    }
	}
	
}
