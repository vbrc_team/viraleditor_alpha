package DatabaseAnalysis;

import DatabaseAnalysis.structures.Sequence;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.TextArea;
import javafx.scene.text.Text;
import javafx.stage.Stage;

public class SequenceEditWindowDialog implements SequenceEditWindowView{
	
	@FXML
    private Text textLabel;

    @FXML
    private TextArea editableArea;

    @FXML
    private Button cancelButton;
    
    private SequenceSearchView parentView;
    private String winType;
    private Sequence sequence;
    
    @Override
    public void initial(SequenceSearchView parentView, String winType, Sequence sequence) {
    	this.parentView = parentView;
    	this.winType = winType;
    	this.sequence = sequence;
    	setDefault();
    	
    }
    
    @FXML
    private void onCancelClicked() {
    	((Stage) cancelButton.getScene().getWindow()).close();
    }
    
    /**
     * save the sequence to sequence list and close the edit window
     */
    @FXML
    private void onSaveClicked() {
    	if (winType.equals("View")) {
    		sequence.setSequence(editableArea.getText().split("\n")[1]);
    	} else {
    		parentView.addSequence(editableArea.getText());
    	}
    	((Stage) cancelButton.getScene().getWindow()).close();
    }
    	
    /**
     * set the window shown text
     */
    private void setDefault() {
    	if (winType.equals("View")) {
    		textLabel.setText("Here you can edit the sequence or the definition. \nWhen you are done, click Save to commit the changes.");
    		editableArea.setText(writeSequenceToTxt(sequence));
    	} else {
    		textLabel.setText("FeatureType or paste here one or more nucleotide or amino acid sequences in FASTA format. Only one sequence type (nuvleic or amino acid) can be present at a time. If provided, the definitions will denote the sequences.");
    	} 
    }
    
    /**
     * convert sequence to text type and show it in text view
     * @param seq
     * @return
     */
    private String writeSequenceToTxt(Sequence seq) {
    	String content = ">";
    	content += seq.getName() + "\n";
    	content += seq.getSequence();
    	return content;
    }
}
