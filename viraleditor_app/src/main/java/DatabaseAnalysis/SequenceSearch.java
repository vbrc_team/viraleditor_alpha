package DatabaseAnalysis;

import java.util.ArrayList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import DatabaseAnalysis.structures.FuzzyPattern;
import DatabaseAnalysis.structures.FuzzySearch;
import DatabaseAnalysis.structures.SearchResultTable;
import DatabaseAnalysis.structures.Sequence;
import Shared.SequenceUtility;

public class SequenceSearch implements SequenceSearchComposer{
	
	SequenceSearchView boundView;
	
	public SequenceSearch(SequenceSearchView boundView) {
		this.boundView = boundView;
	}
	
	
	
	@Override
	public List<SearchResultTable> search(List<Sequence> sequences, String searchType, String sequenceType, String pattern, int missMatch) {
		List<SearchResultTable> result = new ArrayList<SearchResultTable>();
		switch (searchType) {
		case "Regular Expression":
			for (Sequence s : sequences) {
				result.addAll(regexSearch(s.getName(), s.getSequence(), pattern.toUpperCase(), "+"));
				if (isNucleicAcids(sequenceType)) {
					result.addAll(regexSearch(s.getName(), SequenceUtility.make_complement(s.getSequence()), pattern.toUpperCase(), "-"));
				}
			}
			break;
		case "Fuzzy":
			for (Sequence s : sequences) {
				result.addAll(fuzzySearch(s.getName(), s.getSequence(), pattern.toUpperCase(), missMatch, "+"));
				if (isNucleicAcids(sequenceType)) {
					result.addAll(fuzzySearch(s.getName(), SequenceUtility.make_complement(s.getSequence()), pattern.toUpperCase(), missMatch, "-"));
				}
			}
			break;
		}
        return result;
		
	}
	

	/**
	 * realize the regular expression search
	 * 
	 * @param seqName
	 * @param target
	 * @param pattern
	 * @param strand
	 * @return
	 */
	private  List<SearchResultTable> regexSearch(String seqName, String target, String pattern, String strand){
		
		Pattern patternTool = Pattern.compile(pattern);
	    Matcher matcher = patternTool.matcher(target);
	    
	    List<SearchResultTable> regResult = new ArrayList<SearchResultTable>();
	    while (matcher.find()) {
			regResult.add(new SearchResultTable(seqName, matcher.group(), matcher.start()+1, matcher.end(), 100.0, strand));
		}
		return regResult;
	}

	/**
	 * realize the fuzzy search 
	 * 
	 * @param seqName
	 * @param target
	 * @param pattern
	 * @param missMatch
	 * @param strand
	 * @return
	 */
	private List<SearchResultTable> fuzzySearch(String seqName, String target, String pattern, int missMatch, String strand){
		FuzzyPattern fpattern = new FuzzyPattern(pattern);

		if (!fpattern.validate()) {
			System.out.print("Invalid Fuzzy Search Pattern (See help): " + fpattern.toString());
		}

		FuzzySearch search = new FuzzySearch(fpattern, target, missMatch);
		search.search();
		List<FuzzySearch.Match> matches = search.getMatches();
		
		List<SearchResultTable> fuzzyResult = new ArrayList<SearchResultTable>();
		for (FuzzySearch.Match m: matches) {
			SearchResultTable row = new SearchResultTable(seqName, m.toString(),m.position+1, m.position + m.value.length(), (1.0 -((double) m.mismatches / (double) fpattern.countTokens()))*100,strand);
			row.setDisplay(pattern);
			fuzzyResult.add(row);
		}
		return fuzzyResult;
	}
	
	/**
	 * decide whether a sequence is NuclaicAcids or not
	 * 
	 * @param sequenceType
	 * @return
	 */
	private boolean isNucleicAcids(String sequenceType) {
		switch(sequenceType) {
		case "DNA":
		case "Upstream":
			return true;
		case "Protein":
			return false;
		default:
			return false;
		}
	}
}
