package DatabaseAnalysis.structures;


public class GenomeTable {
    private int genome_id;
    private String genome_name;
    private String genome_abbr;
    private int basepairs_num;
    private int genes_num;
    private int genbank_gi;
    private String genbank_accession;
    private int genbank_version;
    private int genbank_tid;
    private String genbank_date;
    private String genbank_description;
    private String is_complete;
    private String is_published;
    private String strain;
    private String species;
    private String molecule_type;
    private String genus;
    private String last_modified;
  
	
	public GenomeTable() {
		this.genome_id = 228093;
		this.genome_name = "ASFV";
		this.genome_abbr = "ASFC";
		this.basepairs_num = 0;
		this.genes_num = 0;
		this.genbank_gi = 0;
		this.genbank_accession = "KX";
		this.genbank_version = 35;
		this.genbank_tid = 0;
		this.genbank_date = "";
		this.genbank_description = "";
		this.is_complete = "";
		this.is_published = "";
		this.strain = "";
		this.species = "";
		this.molecule_type = "";
		this.genus = "";
		this.last_modified = "";
	}
	
	
	public GenomeTable(int genome_id, String genome_name, String genome_abbr, int basepairs_num, int genes_num, int genbank_gi, 
			String genbank_accession, int genbank_version, int genbank_tid, String genbank_date, String genbank_description, String is_complete, 
			String is_published, String strain, String species, String molecule_type, String genus, String last_modified) {
		this.genome_id = genome_id;
		this.genome_name = genome_name;
		this.genome_abbr = genome_abbr;
		this.basepairs_num = basepairs_num;
		this.genes_num = genes_num;
		this.genbank_gi = genbank_gi;
		this.genbank_accession = genbank_accession;
		this.genbank_version = genbank_version;
		this.genbank_date = genbank_date;
		this.genbank_description = genbank_description;
		this.is_complete = is_complete;
		this.is_published = is_published;
		this.strain = strain;
		this.species = species;
		this.molecule_type = molecule_type;
		this.genus = genus;
		this.last_modified = last_modified;
	}

	public int getGenome_id() {
		return genome_id;
	}

	public String getGenome_name() {
		return genome_name;
	}

	public String getGenome_abbr() {
		return genome_abbr;
	}

	public int getBasepairs_num() {
		return basepairs_num;
	}

	public int getGenes_num() {
		return genes_num;
	}

	public int getGenbank_gi() {
		return genbank_gi;
	}

	public String getGenbank_accession() {
		return genbank_accession;
	}

	public int getGenbank_version() {
		return genbank_version;
	}

	public int genbank_version() {
		return genbank_tid;
	}

	public String getGenbank_date() {
		return genbank_date;
	}

	public String getGenbank_description() {
		return genbank_description;
	}

	public String getIs_complete() {
		return is_complete;
	}

	public String getIs_published() {
		return is_published;
	}

	public String getStrain() {
		return strain;
	}

	public String getSpecies() {
		return species;
	}

	public String getMolecule_type() {
		return molecule_type;
	}

	public String getGenus() {
		return genus;
	}

	public String getLast_modified() {
		return last_modified;
	}
	

}
