package DatabaseAnalysis.structures;



import javafx.beans.property.SimpleBooleanProperty;
import javafx.scene.Group;
import javafx.scene.layout.Pane;
import javafx.scene.paint.Color;
import javafx.scene.shape.Rectangle;

public class SearchResultTable {
	private String sequence;
	private String match;
	private int start;
	private int stop;
	private Pane display;
	private Double confidence;
	private String strand;
	private final SimpleBooleanProperty active = new SimpleBooleanProperty(true);
	private final int WIDTH = 60;
	
	public SearchResultTable(String sequenceName, String match, int start, int stop, Double confidence, String strand) {
		this.sequence = sequenceName;
		this.match = match;
		this.start = start;
		this.stop = stop;
		this.confidence = confidence;
		this.strand =strand;
	}
	
	public void setDisplay(String pattern) {
		int width = WIDTH/pattern.length();
		Group group = new Group();
		System.out.println(pattern);
		System.out.println(this.match);
		for (int i=0; i<pattern.length();i++) {
			Rectangle ch = new Rectangle(i*width,6,width,4);
			if (this.match.charAt(i) == pattern.charAt(i)){
				ch.setFill(Color.GREEN);
			} else {
				ch.setFill(Color.RED);
			}
			group.getChildren().add(ch);
		}
		this.display = new Pane(group);
		
	}

	public String getSequence() {
		return sequence;
	}

	public String getMatch() {
		return match;
	}

	public int getStart() {
		return start;
	}

	public int getStop() {
		return stop;
	}
	
	public Pane getDisplay() {
		return display;
	}

	public Double getConfidence() {
		return confidence;
	}

	public String getStrand() {
		return strand;
	}
	
	public SimpleBooleanProperty activeProperty() {
        return active;
    }

    public void setActive(boolean active) {
        this.active.set(active);
    }

    public boolean isActive() {
        return active.get();
    }
	
    
}
