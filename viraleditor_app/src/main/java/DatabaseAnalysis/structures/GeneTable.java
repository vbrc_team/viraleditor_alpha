package DatabaseAnalysis.structures;

public class GeneTable {
	private int gene_id;
	private String gene_num;
	private int genbank_id;
	private String genebank_name;
	private int ortholog_id;
	private String ortholog_name;
	private int genome_id;
	private String genome_addr;
	private String genome_name;
	private String genus;
	private int ORF_start;
	private int ORF_stop;
	private String ORF_strand;
	private String upstream_seq;
	
	public GeneTable() {
		this.gene_id = 1111;
		this.gene_num = "ASFV-001";
		this.genbank_id = 333;
		this.genebank_name = "Hello";
		this.ortholog_id = 1234;
		this.ortholog_name = "World";
		this.genome_id = 5678;
		this.genome_addr = "ABAC";
		this.genome_name = "CACM";
		this.genus = "Cor";
		this.ORF_start = 3344;
		this.ORF_stop = 5566;
	}
	
	public GeneTable(int gene_id, String gene_num, int genbank_id, String genebank_name, int ortholog_id, String ortholog_name, 
					int genome_id, String genome_addr, String genome_name, String genus, int ORF_start, int ORF_stop) {
		this.gene_id = gene_id;
		this.gene_num = gene_num;
		this.genbank_id = genbank_id;
		this.genebank_name = genebank_name;
		this.ortholog_id = ortholog_id;
		this.ortholog_name = ortholog_name;
		this.genome_id = genome_id;
		this.genome_addr = genome_addr;
		this.genome_name = genome_name;
		this.genus = genus;
		this.ORF_start = ORF_start;
		this.ORF_stop = ORF_stop;
	}
	
	public void UpstreamTable(String gene_num, String ortholog_name, int ORF_start, int ORF_stop, String ORF_strand, String seq) {
		this.gene_num = gene_num;
		this.ortholog_name = ortholog_name;
		this.ORF_start = ORF_start;
		this.ORF_stop = ORF_stop;
		this.ORF_strand = ORF_strand;
		this.upstream_seq = seq;
	}

	public void UpstreamTable() {
		this.gene_num = "ASPF-002";
		this.ortholog_name = "MGF 505-3R";
		this.ORF_start = 25832;
		this.ORF_stop = 26677;
		this.ORF_strand = "+";
		this.upstream_seq = "CCTATGGTGCAAGATAATCATCTAGCGCGTGAAACATGTCCTCTT";
	}
	
	public int getGene_id() {
		return gene_id;
	}

	public String getGene_num() {
		return gene_num;
	}

	public int getGenbank_id() {
		return genbank_id;
	}

	public String getGenebank_name() {
		return genebank_name;
	}

	public int getOrtholog_id() {
		return ortholog_id;
	}

	public String getOrtholog_name() {
		return ortholog_name;
	}

	public int getGenome_id() {
		return genome_id;
	}

	public String getGenome_addr() {
		return genome_addr;
	}

	public String getGenome_name() {
		return genome_name;
	}

	public String getGenus() {
		return genus;
	}

	public int getORF_start() {
		return ORF_start;
	}

	public int getORF_stop() {
		return ORF_stop;
	}
	
	public String getORF_strand() {
		return ORF_strand;
	}
	
	public String getUpstream_seq() {
		return upstream_seq;
	}
}
