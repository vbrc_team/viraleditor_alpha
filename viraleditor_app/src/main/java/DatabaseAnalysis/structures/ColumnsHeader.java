package DatabaseAnalysis.structures;

import javafx.beans.property.BooleanProperty;
import javafx.beans.property.SimpleBooleanProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;

public class ColumnsHeader {
	private StringProperty header = new SimpleStringProperty();
    private BooleanProperty selected = new SimpleBooleanProperty();

    public ColumnsHeader(String header, boolean selected) {
        setHeader(header);
        setCompleted(selected);
    }

    public final StringProperty headerProperty() {
        return this.header;
    }


    public final String getHeader() {
        return this.headerProperty().get();
    }


    public final void setHeader(final String header) {
        this.headerProperty().set(header);
    }


    public final BooleanProperty selectedProperty() {
        return this.selected;
    }


    public final boolean isSelected() {
        return this.selectedProperty().get();
    }


    public final void setCompleted(boolean selected) {
        this.selectedProperty().set(selected);
    }
}
