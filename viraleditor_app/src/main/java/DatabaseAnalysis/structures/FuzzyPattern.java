package DatabaseAnalysis.structures;

import java.util.ArrayList;
import java.util.List;

public class FuzzyPattern {
	private StringBuffer m_pattern;


    /**
     * Overloaded constructor for backwards compatibility.
     * The isDna variable is set to false because all amino
     * acid codes are included in the dna codes.
     * False errors should thus not be raised.
     * 
     * @param pattern
     */
    public FuzzyPattern(String pattern) {
    	m_pattern = new StringBuffer(pattern);
  
    }

    /**
     * returns a list <CODE>FuzzyPattern.Token</CODE> objects
     * @return DOCUMENT ME!
     */
    public List<Token> getTokens() {
        String pattern = m_pattern.toString().toUpperCase();

    	if (!validatePattern(pattern)) {
            return null;
        }
    	
        int          units = 0;
        boolean      bracket = false;
        boolean      not = false;

        int          curmax = 1;
        int          curmin = 1;
        StringBuffer curval = new StringBuffer();

        List<Token> tokens = new ArrayList<Token>();

        for (int i = 0; i < pattern.length(); ++i) {
            char c = pattern.charAt(i);

            switch (c) {
                case '(':

                    StringBuffer min = new StringBuffer();
                    StringBuffer max = new StringBuffer();
                    boolean      comma = false;

                    for (int j = i; j < pattern.length(); ++j) {
                        if (pattern.charAt(j) == ')') {
                            i = j;
                            curmin = Integer.parseInt(min.toString());
                            curmax = Integer.parseInt(max.toString());

                            Token t =
                                new Token(
                                    curval.toString(),
                                    curmin,
                                    curmax,
                                    not);
                            tokens.add(t);

                            curval = new StringBuffer();
                            not = false;
                            curmax = 1;
                            curmin = 1;

                            break;
                        } else if (pattern.charAt(j) == ',') {
                            comma = true;
                        } else if (Character.isDigit(pattern.charAt(j))) {
                            if (comma) {
                                max.append(pattern.charAt(j));
                            } else {
                                min.append(pattern.charAt(j));
                            }
                        }
                    }

                    break;

                case '[':
                    bracket = true;

                    if (!curval.toString()
                                   .equals("")) {
                        Token t =
                            new Token(
                                curval.toString(),
                                curmin,
                                curmax,
                                not);
                        curval = new StringBuffer();
                        curmax = 1;
                        curmin = 1;
                        not = false;
                        tokens.add(t);
                    }

                    break;

                case '{':
                    bracket = true;

                    if (!curval.toString()
                                   .equals("")) {
                        Token t =
                            new Token(
                                curval.toString(),
                                curmin,
                                curmax,
                                not);
                        curval = new StringBuffer();
                        curmax = 1;
                        curmin = 1;
                        not = false;
                        tokens.add(t);
                    }

                    not = true;

                    break;

                case ']':
                    bracket = false;

                    break;

                case '}':
                    bracket = false;

                    break;

                default:

                    if (!bracket) {
                        if (curval.toString()
                                      .equals("")) {
                            curval.append(c);
                        } else {
                            Token t =
                                new Token(
                                    curval.toString(),
                                    curmin,
                                    curmax,
                                    not);
                            curval = new StringBuffer(c + "");
                            curmax = 1;
                            curmin = 1;
                            not = false;
                            tokens.add(t);
                        }
                    } else {
                        curval.append(c);
                    }
            }
        }

        if (!curval.toString()
                       .equals("")) {
            Token t = new Token(
                    curval.toString(),
                    curmin,
                    curmax,
                    not);
            curval = new StringBuffer();
            curmax = 1;
            curmin = 1;
            tokens.add(t);
        }

        return tokens;
    }

    /**
     * DOCUMENT ME!
     *
     * @return DOCUMENT ME!
     */
    public int countTokens()
    {
        String pattern = m_pattern.toString();

        if (!validatePattern(pattern)) {
            return -1;
        }

        int     units = 0;
        boolean bracket = false;

        for (int i = 0; i < pattern.length(); ++i) {
            char c = pattern.charAt(i);

            switch (c) {
                case '(':

                    for (int j = i; j < pattern.length(); ++j) {
                        if (pattern.charAt(j) == ')') {
                            i = j;

                            break;
                        }
                    }

                    break;

                case '[':
                    bracket = true;

                    break;

                case '{':
                    bracket = true;

                    break;

                case ']':
                    bracket = false;
                    ++units;

                    break;

                case '}':
                    bracket = false;
                    ++units;

                    break;

                default:

                    if (!bracket) {
                        ++units;
                    }
            }
        }

        return units;
    }

    /**
     * DOCUMENT ME!
     *
     * @return DOCUMENT ME!
     */
    public boolean validate()
    {
        return validatePattern(m_pattern.toString());
    }

    /**
     * DOCUMENT ME!
     *
     * @param pattern DOCUMENT ME!
     *
     * @return DOCUMENT ME!
     */
    private boolean validatePattern(String pattern)
    {
        int     sb = 0;
        int     cb = 0;
        int     rb = 0;
        boolean empty = false;

        pattern = pattern.toUpperCase();
  
        for (int i = 0; i < pattern.length(); ++i) {
            char c = pattern.charAt(i);

            switch (c) {
                case '(':

                    if ((cb != 0) || (sb != 0) ||
                            (i == (pattern.length() - 1))) {
                        return false;
                    }

                    boolean comma = false;
                    boolean digit = false;

                    for (int j = i + 1; j < pattern.length(); ++j) {
                        char c2 = pattern.charAt(j);

                        if (Character.isDigit(c2)) {
                            digit = true;
                        } else if (c2 == ',') {
                            if (!digit) {
                                return false;
                            }

                            digit = false;
                            comma = true;
                        } else if (c2 == ')') {
                            if (!digit || !comma) {
                                return false;
                            } else {
                                i = j;

                                break;
                            }
                        } else {
                            return false;
                        }
                    }

                    break;

                case '[':

                    if ((cb != 0) || (rb != 0) || (sb != 0)) {
                        return false;
                    }

                    empty = true;
                    ++sb;

                    break;

                case '{':

                    if ((sb != 0) || (rb != 0) || (cb != 0)) {
                        return false;
                    }

                    empty = true;
                    ++cb;

                    break;

                case ']':
                    --sb;

                    if ((sb < 0) || empty) {
                        return false;
                    }

                    empty = false;

                    break;

                case '}':
                    --cb;

                    if ((cb < 0) || empty) {
                        return false;
                    }

                    empty = false;

                    break;

                default:

                    if (!validChar(c)) {
                        return false;
                    } else {
                        empty = false;
                    }
            }
        }

        if ((sb == 0) && (cb == 0) && (rb == 0)) {
            return true;
        } else {
            return false;
        }
    }



    /**
     * DOCUMENT ME!
     *
     * @param c DOCUMENT ME!
     *
     * @return DOCUMENT ME!
     */
    private boolean validChar(char c)
    {
    	boolean m_isdna = false;
    	if (m_isdna) {
    		switch (c) {
    			case 'A':
    			case 'C':
    			case 'T':
    			case 'G':
    			case 'M':
    			case 'R':
    			case 'W':
    			case 'S':
    			case 'Y':
    			case 'K':
    			case 'V':
    			case 'H':
    			case 'D':
    			case 'B':
    			case 'X':
    			case 'N':
    			case '.':
                case '\\':
        			return true;
    		}
    	}
    	else {
    		switch (c) {
    			case 'A':
    			case 'B':
    			case 'C':
    			case 'D':
    			case 'E':
    			case 'F':
    			case 'G':
    			case 'H':
    			case 'I':
    			case 'J':
    			case 'K':
    			case 'L':
    			case 'M':
    			case 'N':
    			case 'P':
    			case 'Q':
    			case 'R':
    			case 'S':
    			case 'T':
    			case 'U':
    			case 'V':
    			case 'W':
    			case 'Y':
    			case 'Z':
    			case 'X':
                case '\\':
    				return true;
    		}
    	}

    	return false;
    }

    //~ Inner Classes //////////////////////////////////////////////////////////

    public static class Token
    {
        public final String  value;
        public final int     min;
        public final int     max;
        public final boolean not;

        public Token(
            String value,
            int min,
            int max,
            boolean not)
        {
            this.value = value;
            this.min = min;
            this.max = max;
            this.not = not;
        }

        public String toString()
        {
            return (not ? "NOT" : "") + "'" + value + "'(" + min + "->" + max +
            ")";
        }
    }
}
