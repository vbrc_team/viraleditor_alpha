package DatabaseAnalysis.structures;

public class Sequence {
	private String name;
	private String sequence;
	private String sequenceType;
	
	
	public Sequence(String name, String sequence, String sequenceType) {
		this.name = name;
		this.sequence = sequence;
		this.sequenceType = sequenceType;

	}

	public String getName() {
		return name;
	}

	public String getSequence() {
		return sequence;
	}

	public void setSequence(String sequence) {
		this.sequence = sequence;
	}

	public String getSequenceType() {
		return sequenceType;
	}

	public String toString() {
		return this.getName();
	}
}
