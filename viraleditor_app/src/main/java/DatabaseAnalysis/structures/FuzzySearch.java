package DatabaseAnalysis.structures;

import java.util.ArrayList;
import java.util.List;

public class FuzzySearch {
	protected String m_sequence;
	protected FuzzyPattern m_pattern;
	protected int m_misses;
	protected List<Match> m_matches;

	/**
	 * Creates a new FuzzySearch object.
	 *
	 * @param pattern DOCUMENT ME!
	 * @param searchSequence DOCUMENT ME!
	 * @param mismatches DOCUMENT ME!
	 */
	public FuzzySearch(FuzzyPattern pattern, String searchSequence, int mismatches)
	{
		m_sequence = searchSequence.toUpperCase();
		m_pattern = pattern;
		m_matches = new ArrayList<Match>();
		m_misses = mismatches;

		if (mismatches >= m_pattern.countTokens()) {
			throw new IllegalArgumentException("# of mismatches must not equal or exceed the number of tokens in the pattern");
		}
	}

	//~ Methods ////////////////////////////////////////////////////////////////

	/**
	 * returns a list of FuzzySearch.Match objects
	 *
	 * @return DOCUMENT ME!
	 */
	public List<Match> getMatches()
	{
		return m_matches;
	}


	/**
	 * DOCUMENT ME!
	 */
	public void search()
	{
		List<FuzzyPattern.Token> l = m_pattern.getTokens();
		
		FuzzyPattern.Token curTok = null;
		int k = 0;
		int hits = 0;
		int miss = 0;

		for (int i = 0; i < m_sequence.length(); ++i) {
			 System.out.println("Processing from "+i);
			k = 0;
			hits = 0;
			miss = 0;

			boolean end = false;
			boolean nomatch = false;
			curTok = l.get(k);
			
			for (int j = i; j < m_sequence.length(); ++j) {
				char c = m_sequence.charAt(j);
				if (((curTok.value.indexOf(c) >= 0) && !curTok.not) ||
						((curTok.value.indexOf(c) < 0) && curTok.not)) {
					if (++hits == curTok.max) {
						 
						if (++k < l.size()) {
							hits = 0;
							curTok = (FuzzyPattern.Token) l.get(k);
						} else {
							end = true;
						}
					}
				}
				else {
					 
					if (hits >= curTok.min) {
						 
						if (++k < l.size()) {
							hits = 0;
							curTok = (FuzzyPattern.Token) l.get(k);
						} else {
							 
							end = true;
						}
					} else {
						
						if (++miss > m_misses) {
							 
							nomatch = true;
						} else {
							if (++k < l.size()) {
								hits = 0;
								curTok = (FuzzyPattern.Token) l.get(k);
							} else {
								 
								end = true;
							}
						}
					}

					if (!end) {
						curTok = (FuzzyPattern.Token) l.get(k);
					}
				}

				if (nomatch) {
					 
					break;
				} else if (end) {
					Match m = new Match(m_sequence.substring(i, j + 1), i, miss);
					m_matches.add(m);
					end = false;

					break;
				}
			}
		}
	}

	//~ Inner Classes //////////////////////////////////////////////////////////
	public static class Match
	{
		public final String value;
		public final int    position;
		public final int    mismatches;

		public Match(
				String value,
				int position,
				int mismatches)
		{
			this.value = value;
			this.position = position;
			this.mismatches = mismatches;
		}

		public String toString()
		{
			return value;
		}
	}
}
