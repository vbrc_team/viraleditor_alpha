package DatabaseAnalysis.structures;

import java.util.Date;

public class ChangeLog {
	private Date date;
	private String subject;
	private String action;
	private String curator;
	private String description;
	
	private Date inserted_on;
	private String genome_name;
	private String strain;
	private String genbank_acc;
	
	public ChangeLog() {
		this.date = new Date();
		this.subject = "genome";
		this.action = "update";
		this.curator = "root";
		this.description = "Import genome from genbank";
	}
	
	public void NewGenome() {
		this.inserted_on = new Date();
		this.genome_name = "ASPF PATENT";
		this.strain = "Patent WO20152";
		this.genbank_acc = "LP643842";
	}

	public Date getDate() {
		return date;
	}

	public String getSubject() {
		return subject;
	}

	public String getAction() {
		return action;
	}

	public String getCurator() {
		return curator;
	}

	public String getDescription() {
		return description;
	}

	public Date getInserted_on() {
		return inserted_on;
	}

	public String getGenome_name() {
		return genome_name;
	}

	public String getStrain() {
		return strain;
	}

	public String getGenbank_acc() {
		return genbank_acc;
	}
	
}
