package DatabaseAnalysis.structures;

public class OrthologTable {
	private int ortholog_id;
	private String ortholog_name;
	private int genes_num;
	private int viruses_num;
	private String note;
	
	public OrthologTable() {
		this.ortholog_id = 1234;
		this.ortholog_name = "Coron";
		this.genes_num = 50;
		this.viruses_num = 15;
		this.note = "";
	}
	
	public OrthologTable(int ortholog_id, String ortholog_name, int genes_num, int viruses_num, String note) {
		this.ortholog_id = ortholog_id;
		this.ortholog_name = ortholog_name;
		this.genes_num = genes_num;
		this.viruses_num = viruses_num;
		this.note = note;
	}

	public int getOrtholog_id() {
		return ortholog_id;
	}

	public String getOrtholog_name() {
		return ortholog_name;
	}

	public int getGenes_num() {
		return genes_num;
	}

	public int getViruses_num() {
		return viruses_num;
	}

	public String getNote() {
		return note;
	}
	
	
}
