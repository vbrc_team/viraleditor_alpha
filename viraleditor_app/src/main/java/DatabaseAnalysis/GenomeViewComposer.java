package DatabaseAnalysis;


public interface GenomeViewComposer {
	/**
	 * set the genome database 
	 */
	public void setGenomes();
	

	/**
	 * set the search sequences list to sequence search window
	 * 
	 * @param controller
	 * @param sequenceType
	 */
	public void setSearchSequencesList(SequenceSearchView controller, String sequenceType);
}
