package DatabaseAnalysis;

import java.util.List;

import DatabaseAnalysis.structures.OrthologTable;

public interface OrthologViewView {
	
	/**
	 * Set the ortholog used by the ortholog view composer
	 * 
	 * @param ortholog
	 */
	public void setOrtholog(List<OrthologTable> ortholog);

}
