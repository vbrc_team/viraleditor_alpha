package DatabaseAnalysis;

import java.util.ArrayList;
import java.util.List;

import DatabaseAnalysis.structures.OrthologTable;
import DatabaseAnalysis.structures.Sequence;
import Shared.interchange.Reference;
import javafx.scene.control.ListView;

public class OrthologView implements OrthologViewComposer{
	
	OrthologViewView boundView;
	
	public OrthologView(OrthologViewView view) {
		this.boundView = view;
	}
	
	@Override
	public void setOrtholog() {
		List<OrthologTable> ortholog = new ArrayList<OrthologTable>();
		ortholog.add(new OrthologTable());
    	ortholog.add(new OrthologTable());
		boundView.setOrtholog(ortholog);
	}
	
	
	@Override
    public ListView<Reference> getMissingViruses() {
        ListView<Reference> result = new ListView<>();
		return result;
	}
	
	@Override
	public void setSearchSequencesList(SequenceSearchView controller, String sequenceType) {
		
		//TODO: fake data, modify when database is accessible
		List<Sequence> searchSeq = new ArrayList<Sequence> ();
		searchSeq.add(new Sequence("CoV2-WuhanHu1_19_12_Chn-006", 
				"ATGGCAGATTCCAACGGTACTATTACCGTTGAAGAGCTTAAAAAGCTCCTTGAACAATGGAACCTAGTAATAGGTTTCCTATTCCTTACATGGATTTGTCTTCTACAATTTGCCTATGCCAACAGGAATAGGTTTTTGTATATAATTAAGTTAATTTTCCTCTGGCTGTTATGGCCAGTAACTTTAGCTTGTTTTGTGCTTGCTGCTGTTTACAGAATAAATTGGATCACCGGTGGAATTGCTATCGCAATGGCTTGTCTTGTAGGCTTGATGTGGCTCAGCTACTTCATTGCTTCTTTCAGACTGTTTGCGCGTACGCGTTCCATGTGGTCATTCAATCCAGAAACTAACATTCTTCTCAACGTGCCACTCCATGGCACTATTCTGACCAGACCGCTTCTAGAAAGTGAACTCGTAATCGGAGCTGTGATCCTTCGTGGACATCTTCGTATTGCTGGACACCATCTAGGACGCTGTGACATCAAGGACCTGCCTAAAGAAATCACTGTTGCTACATCACGAACGCTTTCTTATTACAAATTGGGAGCTTCGCAGCGTGTAGCAGGTGACTCAGGTTTTGCTGCATACAGTCGCTACAGGATTGGCAACTATAAATTAAACACAGACCATTCCAGTAGCAGTGACAATATTGCTTTGCTTGTACAGTAA",
				sequenceType));
		controller.setSearchSeqList(searchSeq);
	}
}
