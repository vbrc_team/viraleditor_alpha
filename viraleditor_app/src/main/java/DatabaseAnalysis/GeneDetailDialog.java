package DatabaseAnalysis;


import javafx.scene.control.TextField;
import javafx.fxml.FXML;

public class GeneDetailDialog implements GeneDetailView {
	
	@FXML
    private TextField dataBaseIdField1;

    @FXML
    private TextField organismField1;

    @FXML
    private TextField geneNameField1;

    @FXML
    private TextField moleculeTypeField1;

    @FXML
    private TextField orthologGroupField1;

    @FXML
    private TextField molecularWeightField;

    @FXML
    private TextField genbankIdField1;

    @FXML
    private TextField genbankAccessionNumField1;

    @FXML
    private TextField aminoAcidCountField1;

    @FXML
    private TextField plField1;
    
    @FXML
    private TextField orfBeginField;

    @FXML
    private TextField orfEndField;

    @FXML
    private TextField orfPosField;
	
    @Override
	public void setGeneDetail(GeneViewComposer composer) {
	
	}
}
