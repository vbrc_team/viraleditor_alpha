package DatabaseAnalysis;


import java.io.BufferedWriter;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileWriter;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.StandardCopyOption;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Scanner;


import DatabaseAnalysis.structures.SearchResultTable;
import DatabaseAnalysis.structures.Sequence;
import Shared.Windows.WarningPopupDialog;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.ChoiceBox;
import javafx.scene.control.ListView;
import javafx.scene.control.SelectionMode;
import javafx.scene.control.TabPane;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.stage.FileChooser;
import javafx.stage.Stage;
import javafx.stage.Window;


public class SequenceSearchDialog implements SequenceSearchView{

	@FXML
	private TabPane tabPane;
	
	// input Tab Items
	@FXML
    private Button viewButton;

    @FXML
    private Button removeButton;
    
    @FXML
    private ListView<Sequence> sequencesList;
	
    @FXML
    private TextField PatternField;

    @FXML
    private TextField MismatchesField;

    @FXML
    private ChoiceBox<String> SearchType;

    @FXML
    private TextField SequenceType;

    //result tub items
    @FXML
    private ChoiceBox<String> SequenceChioceBox;

    @FXML
    private ChoiceBox<String> StrandTypeChoiceBox;

    @FXML
    private TextField SearchTypeField;

    @FXML
    private TextField PatternResultField;

    @FXML
    private TextField SequenceTypeField;

    @FXML
    private TextField MismatchesResultField;

    @FXML
    private TextField TotalMatchesField;
    
    @FXML
    private TableView<SearchResultTable> resultTable;

    @FXML
    private TableColumn<SearchResultTable, String> sequence;

    @FXML
    private TableColumn<SearchResultTable, String> match;

    @FXML
    private TableColumn<SearchResultTable, String> display;
    
    @FXML
    private TableColumn<SearchResultTable, Integer> start;

    @FXML
    private TableColumn<SearchResultTable, Integer> stop;

    @FXML
    private TableColumn<SearchResultTable, Integer> confidence;

    @FXML
    private TableColumn<SearchResultTable, String> strand;
    
    @FXML
    private Button closeButton;
    
    
    private String currentSearchType; // Regular expression/Fuzzy
    private String currentSequenceType;// DNA/Upstream/protein
   
    private String chosenResultSequence;
    private String chosenstrandsType; // Both/Top/Button
    
    private ObservableList<Sequence> seqList;
    
    private SequenceSearchComposer composer;
    
    public void initial(String sequenceType, SequenceSearchComposer composer) {
    	this.composer = composer;
    	this.currentSequenceType = sequenceType;
    	setDefaultItem();
    }
    
    @Override
    public void setSearchSeqList(List<Sequence> seqList) {
    	this.seqList = FXCollections.observableList(seqList);
    	sequencesList.setItems(this.seqList);
    	sequencesList.getSelectionModel().setSelectionMode(SelectionMode.SINGLE);
    	sequencesList.getSelectionModel().selectedItemProperty().addListener((observable, oldValue, newValue) -> {
            if (newValue != null) {
            	viewButton.setDisable(false);
            	removeButton.setDisable(false);
            }
        });
    }
    
   

    @Override
    public void addSequence(String txt) {
    	List<String> items = Arrays.asList(txt.split("\\s*\n\\s*"));
    	this.seqList.add(getSequenceFromFile(items));
    }
    
    /**
     * Opens file explorer and allows the user to upload a file
     * the selection is given to the composer and the UI is updated
     * 
     */
    @FXML
    private void onImportClicked() {
        //set up file chooser
        FileChooser fc = new FileChooser();
        fc.getExtensionFilters().addAll(
                new FileChooser.ExtensionFilter("All Sequences", "*", "*.txt", "*.fasta"));
        //open window and wait
        File file = fc.showOpenDialog(null);
        if (file == null){
            return;
        } 
        List<String> fileLine = new ArrayList<String>();
        // read file
        Scanner myReader;
		try {
			myReader = new Scanner(file);
			while (myReader.hasNextLine()) {
				fileLine.add(myReader.nextLine());
			}
	        myReader.close();
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		}
		this.seqList.add(getSequenceFromFile(fileLine));
    }

    
    @FXML
    private void onAddManuallyClicked() {
    	openEditableSequenceWindow("Add Manually", null);
    }
    
    @FXML
    private void onViewEditClicked() {
    	openEditableSequenceWindow("View", sequencesList.getSelectionModel().getSelectedItem());
    	
    }
    
    @FXML
    private void onRemoveClicked() {
    	seqList.removeAll(sequencesList.getSelectionModel().getSelectedItems());
    }
    
    @FXML
    private void onSearchClicked() {
    	//check Pattern and seq type filled
    	if (PatternField.getText().isEmpty()) {
    		openWarningPopWindow("Please fill in Pattern and Sequence FeatureType to process search.");
    		return;
    	}
    	tabPane.getSelectionModel().select(1);
    	
    	// get search result
    	List<SearchResultTable> result = composer.search(seqList,SearchType.getValue(), this.currentSequenceType, PatternField.getText(), Integer.parseInt(MismatchesField.getText()));
    	setResultPage(result);
    	
    }
    
    @FXML
    private void onSaveClicked() throws IOException {
    	if (resultTable.getSelectionModel().getSelectedItems().isEmpty()) {
    		System.out.print("no select");
    		warning("Please select the rows you want to save.");
    		return;
    	}
    	writeTxtFile(getHeader() + getSelectedItems(), "result");
    }
    
    @FXML
    private void onSaveAllClicked() throws IOException {
    	writeTxtFile(getHeader() + getAllItems(), "result");
    }
    
    @FXML
    private void onCloseClicked() {
    	((Stage) closeButton.getScene().getWindow()).close();
    }
    
    
    /**
     * set the default item used in sequence search dialog
     */
    private void setDefaultItem() {
    	List<String> searchTypeList = new ArrayList<String>();
    	searchTypeList.add("Regular Expression");
    	searchTypeList.add("Fuzzy");
    	SearchType.setItems(FXCollections.observableList(searchTypeList));
    	SearchType.getSelectionModel().selectFirst();
    	setMismatches("Regular Expression");
    	SearchType.getSelectionModel().selectedItemProperty().addListener((observable, oldValue, newValue) -> {
    		setMismatches(newValue);
        });
    		
    	SequenceType.setText(this.currentSequenceType);
    
    	
    }
    
    /**
     * set mismathches disable depends on the search type
     * set display column visible or not
     * @param newValue
     */
    private void setMismatches(String newValue) {
    	if (newValue == "Fuzzy") {
    		MismatchesField.setDisable(false);
    		display.setVisible(true);
        } else {
        	MismatchesField.setDisable(true);
        	display.setVisible(false);
        }
    }
    
    /**
     * set the search result to the table view
     * @param result
     */
    private void setResultPage(List<SearchResultTable> result) {
    	//set sequence choice box
    	List<String> sequencesResultList = new ArrayList<String>();
    	sequencesResultList.add("All sequences");
    	for (Sequence s : seqList) {
    		sequencesResultList.add(s.getName());
		}
    	SequenceChioceBox.setItems(FXCollections.observableList(sequencesResultList));
    	SequenceChioceBox.getSelectionModel().selectFirst();
    	this.chosenResultSequence = "All sequences";
    	
    	List<SearchResultTable> shownResult = new ArrayList<SearchResultTable>();
    	shownResult.addAll(result);
    	
    	SequenceChioceBox.getSelectionModel().selectedItemProperty().addListener((observable, oldValue, newValue) -> {
    		if (newValue != null && !newValue.equals("All sequences")) {
    			shownResult.removeAll(shownResult);
    			for (SearchResultTable item: result) {
	    			if (newValue.equals(item.getSequence()))
	    				shownResult.add(item);	
                }
    		} else {
    			shownResult.removeAll(shownResult);
    			shownResult.addAll(result);
    		}   		
        });
    	
    	//set strand choice box
    	List<String> strandsTypeList = new ArrayList<String>();
    	strandsTypeList.add("Both strands");
    	strandsTypeList.add("Top strand");
    	strandsTypeList.add("Botton strand");
    	StrandTypeChoiceBox.setItems(FXCollections.observableList(strandsTypeList));
    	StrandTypeChoiceBox.getSelectionModel().selectFirst();
    	chosenstrandsType = "Both strands";
    	StrandTypeChoiceBox.getSelectionModel().selectedItemProperty().addListener((observable, oldValue, newValue) -> {
    		if (newValue != null && !newValue.equals("Both strands")) {
    			shownResult.removeAll(shownResult);
    			for (SearchResultTable item: result) {
	    			if (newValue.equals("Top strand") && item.getStrand().equals("+"))
	    				shownResult.add(item);	
	    			if (newValue.equals("Button strand") && item.getStrand().equals("-"))
	    				shownResult.add(item);	
                }
    		} else {
    			shownResult.removeAll(shownResult);
    			shownResult.addAll(result);
    		}
        });
    	
    	// set result page fields
    	SearchTypeField.setText(SearchType.getSelectionModel().getSelectedItem());
    	PatternResultField.setText(PatternField.getText());
    	SequenceTypeField.setText(this.currentSequenceType);
    	MismatchesResultField.setText(MismatchesField.getText());
    	TotalMatchesField.setText(Integer.toString(result.size())); //length of table rows
    	
    	//set result table
    	sequence.setCellValueFactory(new PropertyValueFactory<>("sequence"));
    	match.setCellValueFactory(new PropertyValueFactory<>("match"));
    	display.setCellValueFactory(new PropertyValueFactory<>("display"));
    	start.setCellValueFactory(new PropertyValueFactory<>("start"));
    	stop.setCellValueFactory(new PropertyValueFactory<>("stop"));
        confidence.setCellValueFactory(new PropertyValueFactory<>("confidence"));
        strand.setCellValueFactory(new PropertyValueFactory<>("strand"));
        
        ObservableList<SearchResultTable> value = FXCollections.observableList(shownResult);
    	resultTable.setItems(value);
    	resultTable.getSelectionModel().setSelectionMode(SelectionMode.MULTIPLE); 	
    }
    
    
    /**
     * open the Editable sequence window
     * 
     * @param winType
     * @param seq
     */
    private void openEditableSequenceWindow(String winType, Sequence seq) {
    	try {
            FXMLLoader loader = new FXMLLoader(getClass().getClassLoader().getResource("SequenceEditWindow.fxml"));
            Parent root = loader.load();      
            SequenceEditWindowView controller = loader.getController();
            controller.initial(this, winType, seq);
            Stage popup = new Stage();
            popup.setTitle(winType);
            popup.setScene(new Scene(root));
            popup.show();

        } catch (Exception e) {
            e.printStackTrace();
        }
    }
    
    /**
     * convert the file text into sequence type
     * 
     * @param fileLine
     * @return
     */
    private Sequence getSequenceFromFile(List<String> fileLine) {
    	String seqName = "";
    	String seq = "";
    	for (String line : fileLine) {
    		if (line.equals("")){
    			continue;
    		}
			if (line.substring(0, 1).equals(">")){
				seqName = line.substring(1).split(";")[0];
			} else {
				seq = line;
			}
		}
    	return (new Sequence(seqName, seq, this.currentSequenceType));
    }
    
   
    /**
     * open the warning popup window
     * 
     * @param warning
     */
    private void openWarningPopWindow(String warning) {
    	try {
            FXMLLoader loader = new FXMLLoader(getClass().getClassLoader().getResource("WarningPopup.fxml"));
            Parent root = loader.load();      
            WarningPopupDialog controller = loader.getController();
            controller.initial(warning);
            Stage popup = new Stage();
            popup.setTitle("Warning");
            popup.setScene(new Scene(root));
            popup.show();

        } catch (Exception e) {
            e.printStackTrace();
        }
    }
    
    /**
	 * get table header with comma between each
	 */
	private String getHeader() {
		String copied = "";
		for (TableColumn<SearchResultTable, ?> tableColumn: resultTable.getVisibleLeafColumns()) {
			copied += tableColumn.getText()  + ",";
		}
		copied += "\n";
		return copied;
	}
    
	/**
	 * get selected table items with comma between each
	 */
	private String getSelectedItems() {
    	String copied = "";
    	for(SearchResultTable row: resultTable.getSelectionModel().getSelectedItems()) {
    		for (TableColumn<SearchResultTable, ?> column: resultTable.getVisibleLeafColumns()) {
    			copied += column.getCellData(row) + ",";
    		}
    		copied += "\n";
    	}
    	return copied;
	}
	
	/**
	 * get all table items with comma between each
	 */
	private String getAllItems() {
    	String copied = "";
    	for(SearchResultTable row: resultTable.getItems()) {
    		for (TableColumn<SearchResultTable, ?> column: resultTable.getVisibleLeafColumns()) {
    			copied += column.getCellData(row) + ",";
    		}
    		copied += "\n";
    	}
    	return copied;
	}
	
    /**
	 * write text file to a selected place with popup window
	 * @param text
	 * @param initName
	 * @throws IOException
	 */
	private void writeTxtFile(String text, String initName) throws IOException {
		Window window = resultTable.getScene().getWindow();
        
        File tempFile = new File("temp.txt");
        FileChooser fileChooser = new FileChooser();

        FileWriter fw = new FileWriter(tempFile.getAbsoluteFile());
        BufferedWriter bw = new BufferedWriter(fw);
        bw.write(text);
        bw.close();

        //copy file to where the user chooses
        fileChooser.setTitle("Save");
        fileChooser.getExtensionFilters().add( new FileChooser.ExtensionFilter("Text", "*.txt"));
        fileChooser.setInitialFileName(initName);
        File destination = fileChooser.showSaveDialog(window);
        if (destination != null) {
            try {
                Files.copy(tempFile.toPath(), destination.toPath(), StandardCopyOption.REPLACE_EXISTING);
            }catch(IOException e){
                System.out.println("Problem with copying file");
                e.printStackTrace();
            }
        }
        tempFile.delete();
	}
	
	/**
	 * Open warning window and show warning sentence
	 * 
	 * @param warning
	 */
	private void warning(String warning) {
		try {
	        FXMLLoader loader = new FXMLLoader(getClass().getClassLoader().getResource("WarningPopup.fxml"));
	        Parent root = loader.load();      
	        WarningPopupDialog controller = loader.getController();
	        controller.initial(warning);
	        Stage popup = new Stage();
	        popup.setTitle("Warning");
	        popup.setScene(new Scene(root));
	        popup.show();

	    } catch (Exception e) {
	        e.printStackTrace();
	    }
	}
}
