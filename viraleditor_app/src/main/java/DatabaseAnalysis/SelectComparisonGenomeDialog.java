package DatabaseAnalysis;


import java.util.List;

import javafx.collections.FXCollections;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.ComboBox;
import javafx.scene.control.Label;
import javafx.scene.control.RadioButton;
import javafx.scene.control.ToggleGroup;

public class SelectComparisonGenomeDialog implements SelectComparisonGenomeView{
	
	@FXML
    private Label referenceGenome;

    @FXML
    private ComboBox<String> genomeSelection;

    @FXML
    private RadioButton GenBankButton;

    @FXML
    private RadioButton geneNumButton;

    @FXML
    private Button selectButton;
    
    @Override
    public void setGenomeSelection(GeneViewComposer composer) {
    	List<String> genomesList = composer.getGenomeSelectionList();
    	genomeSelection.setItems(FXCollections.observableArrayList(composer.getGenomeSelectionList()));
    	setDefaultSelection(genomesList);
    	genomeSelection.getSelectionModel().selectedItemProperty().addListener((observable, oldValue, newValue) -> {
            if (newValue != null) {
            	referenceGenome.setText(newValue);
            }
        });
    	
    }
    
    
    /**
     * set the default selection view when open the window
     * 
     * @param genomesList
     */
    private void setDefaultSelection(List<String> genomesList) {
    	referenceGenome.setText(genomesList.get(0));
    	genomeSelection.getSelectionModel().selectFirst();
    	ToggleGroup toggleGroup = new ToggleGroup();

    	GenBankButton.setToggleGroup(toggleGroup);
    	geneNumButton.setToggleGroup(toggleGroup);

    	toggleGroup.selectedToggleProperty().addListener((observable, oldValue, newValue) ->  {
            if (newValue != null) {
            	//TODO
            	System.out.print(toggleGroup.getSelectedToggle());
            }
        });
    }
    
    
    @FXML
    private void onSelectButtonSelected() {
    	//TODO: add a new column: Compared Genome's gene Info
    	//TODO: get column data from database
    }
}
