package DatabaseAnalysis;

public interface GeneDetailView {
	/**
	 * set gene detail information for gene composer
	 * 
	 * @param composer
	 */
	public void setGeneDetail(GeneViewComposer composer);
}
