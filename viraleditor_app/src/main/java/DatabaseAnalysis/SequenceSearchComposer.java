package DatabaseAnalysis;

import java.util.List;

import DatabaseAnalysis.structures.SearchResultTable;
import DatabaseAnalysis.structures.Sequence;

public interface SequenceSearchComposer {
	
	/**
	 * search for the pattern
	 * 
	 * @param sequences
	 * @param searchType
	 * @param sequenceType
	 * @param pattern
	 * @param missMatch
	 * @return
	 */
	public List<SearchResultTable> search(List<Sequence> sequences, String searchType, String sequenceType, String pattern, int missMatch);
}
