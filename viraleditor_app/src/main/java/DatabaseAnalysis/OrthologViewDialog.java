package DatabaseAnalysis;

import java.awt.Toolkit;
import java.awt.datatransfer.Clipboard;
import java.awt.datatransfer.StringSelection;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileWriter;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardCopyOption;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

import DatabaseAnalysis.structures.ColumnsHeader;
import DatabaseAnalysis.structures.OrthologTable;
import Shared.Windows.WarningPopupDialog;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.print.Printer;
import javafx.print.PrinterJob;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.SelectionMode;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.stage.FileChooser;
import javafx.stage.Stage;
import javafx.stage.Window;

public class OrthologViewDialog implements OrthologViewView {

    private OrthologViewComposer composer;
    
    @FXML
    private TableView<OrthologTable> orthologTable;
    
    @FXML
    private TableColumn<OrthologTable, Integer> orthologId;

    @FXML
    private TableColumn<OrthologTable, String> orthologName;

    @FXML
    private TableColumn<OrthologTable, Integer> genesNum;

    @FXML
    private TableColumn<OrthologTable, Integer> virusesNum;

    @FXML
    private TableColumn<OrthologTable, Integer> notes;
    
    /**
     * Constructor
     * initial the ortholog composer
     * 
     * @param orthoComposer
     */
    public void initComposer(OrthologViewComposer orthoComposer) {
    	this.composer = orthoComposer;
	}
    

    @Override
    public void setOrtholog(List<OrthologTable> ortholog) {
    	
    	orthologId.setCellValueFactory(new PropertyValueFactory<>("ortholog_id"));
    	orthologName.setCellValueFactory(new PropertyValueFactory<>("ortholog_name"));
    	genesNum.setCellValueFactory(new PropertyValueFactory<>("genes_num"));
    	virusesNum.setCellValueFactory(new PropertyValueFactory<>("viruses_num"));
    	notes.setCellValueFactory(new PropertyValueFactory<>("note"));
    
    	ObservableList<OrthologTable> value = FXCollections.observableList(ortholog);
    	orthologTable.setItems(value);
    	orthologTable.getSelectionModel().setSelectionMode(SelectionMode.MULTIPLE);
    }

    /** 
     * Write whole table to a txt file
     * @throws IOException
     */
    @FXML
    private void writeToFile() throws IOException {
    	writeTxtFile(getHeader() + getAllItems(), "ortholog_group");
    }
    
    /**
     * Write selected rows in table to a txt file
     * @throws IOException
     */
    @FXML
    private void writeSelectToFile() throws IOException {
    	if (orthologTable.getSelectionModel().getSelectedItems().isEmpty()) {
    		warning("Please select the rows you need");
    		return;
    	}
    	writeTxtFile(getHeader() + getSelectedItems(), "ortholog_group");
    	
    }
    
    /** 
     * export whole table to an html file
     * @throws IOException
     */
    @FXML
    private void exportToHtml() throws IOException {
    	writeHtml(getAllItemsHtml());
    }
    
    /** 
     * export selected rows in table to an html file
     * @throws IOException
     */
    @FXML
    private void exportSelectToHtml() throws IOException {
    	if (orthologTable.getSelectionModel().getSelectedItems().isEmpty()) {
    		warning("Please select the rows you need");
    		return;
    	}
    	writeHtml(getSelectedItemsHtml());
    }
    
    /** 
     * print whole table view
     */
    @FXML
    private void printAll() {
    	print(orthologTable);
    }
    
    /** 
     * print selected row in table view
     */
    @FXML
    private void printSelectedItems() {
    	//TODO: print selected rows
    	print(orthologTable);
    }
    
    @FXML
	public void close() {
		((Stage) orthologTable.getScene().getWindow()).close();
	}
    
    @FXML
    private void selectAll() {
    	orthologTable.getSelectionModel().selectAll();
    }
    
    @FXML
    private void deselectAll() {
    	orthologTable.getSelectionModel().clearSelection();
    }
    
    /**
     * put selected table rows into clipboard
     */
    @FXML
    private void copySelectedItem() {
    	if (orthologTable.getSelectionModel().getSelectedItems().isEmpty()) {
    		warning("Please select the rows you need");
    		return;
    	}
    	StringSelection stringSelection = new StringSelection(getSelectedItems());
    	Clipboard clipboard = Toolkit.getDefaultToolkit().getSystemClipboard();
    	clipboard.setContents(stringSelection, null);
    }
    
    /**
     * Open the column selection window 
     * transmit the orthalog table to column selection
     */
    @FXML
    private void openColumnSelection() {
    	try {
            FXMLLoader loader = new FXMLLoader(getClass().getClassLoader().getResource("ColumnSelection.fxml"));
            Parent root = loader.load();
                   
            ColumnSelectionView controller = loader.getController();
            controller.setColumnList(orthologTable, getColumnsName());
            
            Stage popup = new Stage();
            popup.setTitle("Column Select");
            popup.setScene(new Scene(root));
            popup.show();

        } catch (Exception e) {
            e.printStackTrace();
        }
    }
    
    /**
     * save table column setting into a text file
     * @throws IOException
     */
    @FXML
    private void saveSettingAs() throws IOException {
    	String copied = "#ortholog\n";
    	int index = 0;
		for (TableColumn<OrthologTable, ?> tableColumn: orthologTable.getColumns()) {
			copied += index + "," + tableColumn.getText() + "=" + tableColumn.isVisible() + "|" + tableColumn.getWidth() + "\n";
			index++;
		}
		copied = copied.substring(0, copied.length() - 1);
    	writeTxtFile(copied,"orthologColumnSetting");
    }
    
    /**
     * read the default column setting file
     */
    @FXML
    private void restoreDefaultSetting() {
    	Path currentRelativePath = Paths.get("");
    	String s = currentRelativePath.toAbsolutePath().toString() + "/src/main/resources/DatabaseAnalysisColumns";
    	File columns = new File(s + "/OrthologColumnSetting.txt");
    	getColumnSettingFromFile(columns);
    }
    
    /**
     * read the column setting file from client side
     */
    @FXML
    private void restoreSettingFrom() {
    	FileChooser fc = new FileChooser();
        //open window and wait
        fc.getExtensionFilters().addAll(new FileChooser.ExtensionFilter("Text", "*.txt"));
        File columns = fc.showOpenDialog(null);
        if (columns == null){
            return;
        }
        getColumnSettingFromFile(columns);
    }
    
    /**
     * Qpen the missing viruses window
     * transmit composer to missingViruses
     */
    @FXML
    private void openMissingViruses() {
    	try {
            FXMLLoader loader = new FXMLLoader(getClass().getClassLoader().getResource("MissingViruses.fxml"));
            Parent root = loader.load();
                   
            MissingVirusesView controller = loader.getController();
            controller.initMissingViruses(composer);
            
            Stage popup = new Stage();
            popup.setTitle("View Missing Viruses");
            popup.setScene(new Scene(root));
            popup.show();

        } catch (Exception e) {
            e.printStackTrace();
        }	
    }
    
    @FXML
    private void onDnaSearchClicked() {
    	openSequenceSearchWindow("DNA");
    }
    
    @FXML
    private void onUpstreamSearchClicked() {
    	openSequenceSearchWindow("Upstream");
    }
    
    @FXML
    private void onProteinSearchClicked() {
    	openSequenceSearchWindow("Protein");
    }
    
    /**
     * open the sequence search window depends on search type
     * @param searchType
     */
    private void openSequenceSearchWindow(String sequenceType) {
    	try {
            FXMLLoader loader = new FXMLLoader(getClass().getClassLoader().getResource("SequenceSearcherWindow.fxml"));
            Parent root = loader.load();      
            SequenceSearchView controller = loader.getController();
            SequenceSearchComposer searchComposer = new SequenceSearch(controller);
            controller.initial(sequenceType, searchComposer);
            composer.setSearchSequencesList(controller, sequenceType);
            Stage popup = new Stage();
            popup.setTitle("Sequence Searcher");
            popup.setScene(new Scene(root));
            popup.show();

        } catch (Exception e) {
            e.printStackTrace();
        }
    }
    
    /**
     * open the analysis plot window
     */
    @FXML
    private void openAnalysisPlotGenes() {
    	try {
            FXMLLoader loader = new FXMLLoader(getClass().getClassLoader().getResource("AnalysisPlot.fxml"));
            Parent root = loader.load();
                   
            AnalysisPlotDialog controller = loader.getController();
            controller.setPointChartData();
            
            Stage popup = new Stage();
            popup.setTitle("Orthlog View Genes Plot");
            popup.setScene(new Scene(root));
            popup.show();

        } catch (Exception e) {
            e.printStackTrace();
        }	
    }
    
    /**
     * open the family window for the selected orthlog group
     */
    @FXML
    private void openFamilyWindow() {
		try {
			FXMLLoader loader = new FXMLLoader(getClass().getClassLoader().getResource("GeneView.fxml"));
		
			Parent root = loader.load();
			GeneViewView familyWinController = loader.getController();            
            GeneViewComposer composer = new GeneView(familyWinController);
            ((GeneViewDialog)familyWinController).initComposer(composer);
            ((GeneViewDialog)familyWinController).familyButton.setDisable(true);
            composer.setGenes();
			
			Stage popup = new Stage();
			popup.setTitle("Genes in Family View");
			popup.setScene(new Scene(root));
			popup.show();
	    } catch (Exception e) {
	        e.printStackTrace();
	    }
    }
    
    /**
	 * get columns name from genome table view
	 * 
	 * @param columns
	 * @return
	 */
	private List<ColumnsHeader> getColumnsName() {
		List<ColumnsHeader> columnsName = new ArrayList<>();
		for (TableColumn<OrthologTable,?> column: orthologTable.getColumns()) {
			columnsName.add(new ColumnsHeader(column.getText(), column.isVisible()));
		}
		return columnsName;
	}
	
	/**
	 * get table header with comma between each
	 */
    private String getHeader() {
		String copied = "";
		for (TableColumn<OrthologTable, ?> tableColumn: orthologTable.getVisibleLeafColumns()) {
			copied += tableColumn.getText()  + ",";
		}
		copied += "\n";
		return copied;
	}
	
    /**
	 * get selected table items with comma between each
	 */
	private String getSelectedItems() {
    	String copied = "";
    	for(OrthologTable row:orthologTable.getSelectionModel().getSelectedItems()) {
    		for (TableColumn<OrthologTable, ?> column: orthologTable.getVisibleLeafColumns()) {
    			copied += column.getCellData(row) + ",";
    		}
    		copied += "\n";
    	}
    	return copied;
	}
	
	/**
	 * get all table items with comma between each
	 */
	private String getAllItems() {
    	String copied = "";
    	for(OrthologTable row:orthologTable.getItems()) {
    		for (TableColumn<OrthologTable, ?> column: orthologTable.getVisibleLeafColumns()) {
    			copied += column.getCellData(row) + ",";
    		}
    		copied += "\n";
    	}
    	return copied;
	}
	
	/**
	 * write text file to a selected place with popup window
	 * @param text
	 * @param initName
	 * @throws IOException
	 */
	private void writeTxtFile(String text, String initName) throws IOException {
		Window window = orthologTable.getScene().getWindow();
        
        File tempFile = new File("temp.txt");
        FileChooser fileChooser = new FileChooser();

        FileWriter fw = new FileWriter(tempFile.getAbsoluteFile());
        BufferedWriter bw = new BufferedWriter(fw);
        bw.write(text);
        bw.close();

        //copy file to where the user chooses
        fileChooser.setTitle("Save");
        fileChooser.getExtensionFilters().add( new FileChooser.ExtensionFilter("Text", "*.txt"));
        fileChooser.setInitialFileName(initName);
        File destination = fileChooser.showSaveDialog(window);
        if (destination != null) {
            try {
                Files.copy(tempFile.toPath(), destination.toPath(), StandardCopyOption.REPLACE_EXISTING);
            }catch(IOException e){
                System.out.println("Problem with copying file");
                e.printStackTrace();
            }
        }
        tempFile.delete();
	}
	
	/**
	 * get table header in html style
	 * @return
	 */
	private String getHeaderHtml() {
		String content = "";
		for (TableColumn<OrthologTable, ?> tableColumn: orthologTable.getVisibleLeafColumns()) {
			content += "    <TD><B>" + tableColumn.getText()  + "</B></TD>" + "\n";
		}
		return content;
	}
	
	/**
	 * get selected table text in html style
	 * @return
	 */
	private String getSelectedItemsHtml() {
		String content = "";
		for(OrthologTable row: orthologTable.getSelectionModel().getSelectedItems()) {
			content += "  <TR>" + "\n";
    		for (TableColumn<OrthologTable, ?> column: orthologTable.getVisibleLeafColumns()) {
    			String align = "right";
    			content += "    <TD><DIV ALIGN=" + align + ">" + column.getCellData(row) + "</DIV></TD>" + "\n"; 
    		}
    		content += "  </TR>" + "\n";
    	}
    	return content;
	}
	
	/**
	 * get all table text in html style
	 * @return
	 */
	private String getAllItemsHtml() {
		String content = "";
		for(OrthologTable row: orthologTable.getItems()) {
			content += "  <TR>" + "\n";
    		for (TableColumn<OrthologTable, ?> column: orthologTable.getVisibleLeafColumns()) {
    			String align = "right";
    			content += "    <TD><DIV ALIGN=" + align + ">" + column.getCellData(row) + "</DIV></TD>" + "\n"; 
    		}
    		content += "  </TR>" + "\n";
    	}
    	return content;
	}
	
	/**
	 * put table text into html structure, and write it to a new file.
	 * @param text
	 * @throws IOException
	 */
	private void writeHtml(String text) throws IOException {
		String projectTitle = "Ortholog Group";
		String m_tableTitle = "name";
		String content = "<HTML>" + "\n"
					   + "<HEAD><TITLE>" + projectTitle + " - " + m_tableTitle + "</TITLE></HEAD>" + "\n"
					   + "<BODY>" + "\n"
					   + "<H1>" + projectTitle + "</H1>" + "\n"
					   + "<H2>" + m_tableTitle + "</H2>" + "\n"
					   + "<TABLE BORDER=2>" + "\n"
					   + "  <TR>" + "\n"
					   + getHeaderHtml()
					   + "  </TR>" + "\n"
					   + text
					   + "</TABLE>" + "\n"
					   + "</BODY>" + "\n"
					   + "</HTML>";
		Window window = orthologTable.getScene().getWindow();
        
        File tempFile = new File("temp.html");
        FileChooser fileChooser = new FileChooser();

        FileWriter fw = new FileWriter(tempFile.getAbsoluteFile());
        BufferedWriter bw = new BufferedWriter(fw);
        bw.write(content);
        bw.close();

        //copy file to where the user chooses
        fileChooser.setTitle("Save");
        fileChooser.getExtensionFilters().add( new FileChooser.ExtensionFilter("HTML", "*.html"));
        fileChooser.setInitialFileName("ortholog_group");
        File destination = fileChooser.showSaveDialog(window);
        if (destination != null) {
            try {
                Files.copy(tempFile.toPath(), destination.toPath(), StandardCopyOption.REPLACE_EXISTING);
            }catch(IOException e){
                System.out.println("Problem with copying file");
                e.printStackTrace();
            }
        }
        tempFile.delete();
	}
	
	/**
	 * Dealing with printer popup window and printer settings
	 * @param node - here is table view
	 */
	private void getColumnSettingFromFile(File columns) {
		Scanner myReader;
		try {
			myReader = new Scanner(columns);
			if (!myReader.nextLine().equals("#ortholog")){
				warning("Please check your column setting file has correct structure.");
				myReader.close();
				return;
			}
	        while (myReader.hasNextLine()) {
	          String[] data = myReader.nextLine().split(",|=|\\|");
	          orthologTable.getColumns().get(Integer. parseInt(data[0])).setVisible(Boolean.parseBoolean(data[2]));
	          orthologTable.getColumns().get(Integer. parseInt(data[0])).setPrefWidth(Double.parseDouble(data[3]));
	        }
	        myReader.close();
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		}
	}
	
	/**
	 * Open warning window and show warning sentence
	 * @param warning
	 */
	private void print(Node node) {
		List<Printer> printerList = new ArrayList<Printer>(Printer.getAllPrinters());
        Printer printer = printerList.get(0);
        PrinterJob job = PrinterJob.createPrinterJob(printer);
        if (job != null  && job.showPrintDialog(orthologTable.getScene().getWindow()) ) { 
            boolean success = job.printPage(node);                   
            if (success) {
                job.endJob();
            }
        }
	}
	
	/**
	 * read column information from columns file and implement them in the table
	 * @param columns
	 */
	private void warning(String warning) {
		try {
        FXMLLoader loader = new FXMLLoader(getClass().getClassLoader().getResource("WarningPopup.fxml"));
        Parent root = loader.load();      
        WarningPopupDialog controller = loader.getController();
        controller.initial(warning);
        Stage popup = new Stage();
        popup.setTitle("Warning");
        popup.setScene(new Scene(root));
        popup.show();

	    } catch (Exception e) {
	        e.printStackTrace();
	    }
	}
}
