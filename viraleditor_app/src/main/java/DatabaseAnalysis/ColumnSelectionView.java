package DatabaseAnalysis;

import java.util.List;

import DatabaseAnalysis.structures.ColumnsHeader;
import javafx.scene.control.TableView;

public interface ColumnSelectionView {

	/**
	 * set the column list used in column selection window
	 * 
	 * @param table
	 * @param columnsName
	 */
	public void setColumnList(TableView<?> table, List<ColumnsHeader> columnsName);
}
