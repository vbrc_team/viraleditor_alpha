package DatabaseAnalysis;

public interface MissingVirusesView {

	/**
     * initial the missing viruses data
     * 
     * @param composer
     */
	public void initMissingViruses(OrthologViewComposer composer);
	
}
