package DatabaseAnalysis;

import java.util.List;


public interface GeneViewComposer {
	/**
	 * set genes used in gene view table
	 * 
	 */
	public void setGenes();
	
	/**
	 * get gene detail information
	 * @param geneName
	 * @return
	 */
	public List<String> getGeneDetail(String geneName);
	
	/**
	 * get comparison genomes selection list
	 * @return
	 */
	public List<String> getGenomeSelectionList();
	
	/**
	 * set the search sequence list for the search window
	 * @param controller
	 */
	public void setSearchSequencesList(SequenceSearchView controller, String sequenceType);
}
