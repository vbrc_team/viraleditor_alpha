package DatabaseAnalysis;

import java.util.List;

import DatabaseAnalysis.structures.Sequence;

public interface SequenceSearchView {
	
	/**
	 * initial the search type of the sequence search window
	 * @param searchType
	 */
	public void initial(String sequenceType, SequenceSearchComposer composer);
	 
	/**
	 * set the sequence list used in search window
	 * @param seqList
	 */
	public void setSearchSeqList(List<Sequence> seqList);
	
	/**
	 * add the sequence to the sequence list
	 * @param txt
	 */
	public void addSequence(String txt);
}

