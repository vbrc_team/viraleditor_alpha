package DatabaseAnalysis;

public interface SelectComparisonGenomeView {
	/**
	 * set comparison genome list with gene view composer
	 * 
	 * @param composer
	 */
	public void setGenomeSelection(GeneViewComposer composer);
}
