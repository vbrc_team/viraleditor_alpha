package DatabaseAnalysis;

import java.util.List;

public interface TableViewView {
	public void setTableColumnName(List<String> colNames);
	public void setTableRows(List<Object> rows);
	public void setTableFactory(List<String> cols);
}
