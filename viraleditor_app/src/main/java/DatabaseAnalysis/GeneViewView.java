package DatabaseAnalysis;

import java.util.List;

import DatabaseAnalysis.structures.GeneTable;


public interface GeneViewView {
	/**
	 * set the gene data for gene view table
	 * 
	 * @param geneTable
	 */
	public void setGeneTable(List<GeneTable> genes);
}
