package DatabaseAnalysis;

import Shared.interchange.Reference;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.ListView;
import javafx.stage.Stage;

public class MissingVirusesDialog implements MissingVirusesView{

	@FXML
    private Button OKButton;
    @FXML
    private Label missingViruses;
    @FXML
    private ListView<Reference> missingVirusesList; // TODO: type may not work
    
    /**
     * Handle the OK button
     */
    @FXML
	public void handleOK() {
		((Stage) OKButton.getScene().getWindow()).close();
	}
    
    
    /**
     * initial the missing viruses data
     * 
     * @param composer
     */
    @Override
	public void initMissingViruses(OrthologViewComposer composer) {
		missingVirusesList = composer.getMissingViruses();
	}
	
	

}
