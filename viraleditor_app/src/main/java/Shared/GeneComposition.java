package Shared;

import java.util.HashMap;
import java.util.Iterator;

public class GeneComposition {
    private HashMap<String, Integer> amino_acid_counts;
    private HashMap<String, Integer> nucleotide_counts;
    private HashMap<String, Integer> dinucleotide_counts;
    private HashMap<String, Integer> codon_counts;

    /**
     * GeneComposition class constructor
     */
    public GeneComposition() {
        initVariables();
    }

    /**
     * GeneComposition class constructor
     * 
     * @param aa_cnt      the amino acid counts
     * @param nucl_cnt    the nucleotide counts
     * @param di_nucl_cnt the di-nucleotide counts
     * @param codon_cnt   the codon counts
     */
    public GeneComposition(HashMap<String, Integer> aa_cnt, HashMap<String, Integer> nucl_cnt,
            HashMap<String, Integer> di_nucl_cnt, HashMap<String, Integer> codon_cnt) {
        amino_acid_counts = aa_cnt;
        nucleotide_counts = nucl_cnt;
        dinucleotide_counts = di_nucl_cnt;
        codon_counts = codon_cnt;
    }

    /**
     * initialize variables
     */
    private void initVariables() {
        amino_acid_counts = new HashMap<>();
        for (int i = 0; i < SequenceConstants.aminoAcids.length; i++) {
            amino_acid_counts.put(SequenceConstants.aminoAcids[i], 0);
        }
        nucleotide_counts = new HashMap<>();
        for (int i = 0; i < SequenceConstants.nucleotides.length; i++) {
            nucleotide_counts.put(SequenceConstants.nucleotides[i], 0);
        }
        dinucleotide_counts = new HashMap<>();
        for (int i = 0; i < SequenceConstants.dinucleotides.length; i++) {
            dinucleotide_counts.put(SequenceConstants.dinucleotides[i], 0);
        }
        codon_counts = new HashMap<>();
        for (int i = 0; i < SequenceConstants.codons.length; i++) {
            codon_counts.put(SequenceConstants.codons[i], 0);
        }
    }

    /**
     * Get the composition counts for a,c,g, and t
     * 
     * @return the occurrences of a,c,g, and t
     */
    public HashMap<String, Integer> getNucleotideCounts() {
        return nucleotide_counts;
    }

    /**
     * Get the composition counts for aa,ac,ag,at,ca,cc,cg,ct, etc.
     * 
     * @return the occurrences of aa,ac,ag,at,ca,cc,cg,ct, ga,gc,gg,gt,ta,tc,tg, and
     *         tt in the gene dna
     */
    public HashMap<String, Integer> getDinucleotideCounts() {
        return dinucleotide_counts;
    }

    /**
     * Get the composition counts for aaa,aac,aag,aat,aca,acc,acg,act, etc.
     * 
     * @return the occurrences of aaa,aac,aag,aat,aca,acc,acg,act,
     *         aga,agc,agg,agt,ata,atc,atg,att,caa,cac,cag,cat,cca,ccc,ccg,cct,
     *         cga,cgc,cgg,cgt,cta,ctc,ctg,ctt,gaa,gac,gag,gat,gca,gcc,gcg,gct,
     *         gga,ggc,ggg,ggt,gta,gtc,gtg,gtt,taa,tac,tag,tat,tca,tcc,tcg,tct,
     *         tga,tgc,tgg,tgt,tta,ttc,ttg, and ttt
     */
    public HashMap<String, Integer> getCodonCounts() {
        return codon_counts;
    }

    /**
     * Get the amino acid counts
     * 
     * @return table with all amino acid counts
     */
    public HashMap<String, Integer> getAminoAcidCounts() {
        return amino_acid_counts;
    }

    /**
     * Set the counts for the occurrences of the amino acids
     * 
     * @param aa_cnt the amino acid counts
     */
    public void setAminoAcidCounts(HashMap<String, Integer> aa_cnt) {
        amino_acid_counts = aa_cnt;
    }

    /**
     * Count the occurrences of the amino acids and calculate the frequency of the
     * amino acids
     * 
     * @param protein_seq the protein sequence of the gene
     */
    public void setAminoAcidCounts(String protein_seq) {
        String c;
        Integer cnt;
        int cntVal;
        double mysize;

        amino_acid_counts = new HashMap<>();
        for (int i = 0; i < SequenceConstants.aminoAcids.length; i++) {
            amino_acid_counts.put(SequenceConstants.aminoAcids[i], 0);
        }
        mysize = protein_seq.length();
        for (int k = 0; k < mysize; k++) {
            c = "" + protein_seq.charAt(k);
            c = c.toUpperCase();
            cnt = amino_acid_counts.get(c);
            if (cnt == null)
                cntVal = 0;
            else
                cntVal = cnt;
            cntVal++;
            amino_acid_counts.put(c, cntVal);
        }
    }

    /**
     * Set the counts for the occurrences of the nucleotides
     * 
     * @param nucl_cnt the nucleotide counts
     */
    public void setNucleotideCounts(HashMap<String, Integer> nucl_cnt) {
        nucleotide_counts = nucl_cnt;
    }

    /**
     * Count the occurrences of a,c,g, and t in the gene dna sequence and calculate
     * their frequency
     * 
     * @param dna_seq the nucleotide sequence of the gene
     */
    public void setNucleotideCounts(String dna_seq) {
        String c;
        Integer cnt;
        int cntVal;
        double mysize;

        nucleotide_counts = new HashMap<>();
        for (int i = 0; i < SequenceConstants.nucleotides.length; i++) {
            nucleotide_counts.put(SequenceConstants.nucleotides[i], 0);
        }
        mysize = dna_seq.length();
        for (int k = 0; k < mysize; k++) {
            c = "" + dna_seq.charAt(k);
            c = c.toUpperCase();
            cnt = nucleotide_counts.get(c);
            if (cnt == null)
                cntVal = 0;
            else
                cntVal = cnt;
            cntVal++;
            nucleotide_counts.put(c, cntVal);
        }
    }

    /**
     * Set the counts for the occurrences of the di-nucleotides
     * 
     * @param di_nucl_cnt the di-nucleotide counts
     */
    public void setDinucleotideCounts(HashMap<String, Integer> di_nucl_cnt) {
        dinucleotide_counts = di_nucl_cnt;
    }

    /**
     * Count the occurrences of aa,ac,ag,at,ca,cc,cg,ct, ga,gc,gg,gt,ta,tc,tg, and
     * tt in the gene dna sequence and calculate their frequency
     * 
     * @param dna_seq the nucleotide sequence of the gene
     */
    public void setDinucleotideCounts(String dna_seq) {
        Integer cnt;
        String c;
        int cntVal;
        double mysize;

        dinucleotide_counts = new HashMap<>();
        for (int i = 0; i < SequenceConstants.dinucleotides.length; i++) {
            dinucleotide_counts.put(SequenceConstants.dinucleotides[i], 0);
        }
        mysize = dna_seq.length();
        for (int k = 0; k < (mysize - 1); k++) {
            c = "" + dna_seq.charAt(k) + dna_seq.charAt(k + 1);
            c = c.toUpperCase();
            cnt = dinucleotide_counts.get(c);
            if (cnt == null)
                cntVal = 0;
            else
                cntVal = cnt;
            cntVal++;
            dinucleotide_counts.put(c, cntVal);
        }
    }

    /**
     * Set the counts for the occurrences of the codons
     * 
     * @param codon_cnt the codon counts
     */
    public void setCodonCounts(HashMap<String, Integer> codon_cnt) {
        codon_counts = codon_cnt;
    }

    /**
     * Count the occurrences of aaa,aac,aag,aat,aca,acc,acg,act,
     * aga,agc,agg,agt,ata,atc,atg,att,caa,cac,cag,cat,cca,ccc,ccg,cct,
     * cga,cgc,cgg,cgt,cta,ctc,ctg,ctt,gaa,gac,gag,gat,gca,gcc,gcg,gct,
     * gga,ggc,ggg,ggt,gta,gtc,gtg,gtt,taa,tac,tag,tat,tca,tcc,tcg,tct,
     * tga,tgc,tgg,tgt,tta,ttc,ttg, and ttt in the gene dna sequence and calculate
     * their frequency
     * 
     * @param dna_seq the nucleotide sequence of the gene
     */
    public void setCodonCounts(String dna_seq) {
        String c;
        Integer cnt;
        int cntVal;
        double mysize;

        codon_counts = new HashMap<>();
        for (int i = 0; i < SequenceConstants.codons.length; i++) {
            codon_counts.put(SequenceConstants.codons[i], 0);
        }
        mysize = dna_seq.length();
        for (int k = 0; k < (mysize - 2); k += 3) {
            c = "" + dna_seq.charAt(k) + dna_seq.charAt(k + 1) + dna_seq.charAt(k + 2);
            c = c.toUpperCase();
            cnt = codon_counts.get(c);
            if (cnt == null)
                cntVal = 0;
            else
                cntVal = cnt;
            cntVal++;
            codon_counts.put(c, cntVal);
        }
    }

    /**
     * Compare protein properties of genes
     * 
     * @param g the gene compositions to compare with
     * @return if equal
     */
    public boolean isEqual(GeneComposition g) {
        boolean geqs = true;
        String key;
        Integer val1, val2;
        Iterator<String> keys = amino_acid_counts.keySet().iterator();
        while (keys.hasNext()) {
            key = keys.next();
            val1 = amino_acid_counts.get(key);
            val2 = g.amino_acid_counts.get(key);
            if (val1 == null) {
                if (val2 != null)
                    geqs = false;
            } else {
                if (val2 == null) {
                    geqs = false;
                } else {
                    if (val1.intValue() != val2.intValue())
                        geqs = false;
                }
                if (!geqs)
                    break;
            }
        }
        if (geqs) {
            keys = nucleotide_counts.keySet().iterator();
            while (keys.hasNext()) {
                key = keys.next();
                val1 = nucleotide_counts.get(key);
                val2 = g.nucleotide_counts.get(key);
                if (val1 == null) {
                    if (val2 != null)
                        geqs = false;
                } else {
                    if (val2 == null) {
                        geqs = false;
                    } else {
                        if (val1.intValue() != val2.intValue())
                            geqs = false;
                    }
                }
                if (!geqs)
                    break;
            }
        }
        if (geqs) {
            keys = dinucleotide_counts.keySet().iterator();
            while (keys.hasNext()) {
                key = keys.next();
                val1 = dinucleotide_counts.get(key);
                val2 = g.dinucleotide_counts.get(key);
                if (val1 == null) {
                    if (val2 != null)
                        geqs = false;
                } else {
                    if (val2 == null) {
                        geqs = false;
                    } else {
                        if (val1.intValue() != val2.intValue())
                            geqs = false;
                    }
                }
                if (!geqs)
                    break;
            }
        }
        if (geqs) {
            keys = codon_counts.keySet().iterator();
            while (keys.hasNext()) {
                key = keys.next();
                val1 = codon_counts.get(key);
                val2 = g.codon_counts.get(key);
                if (val1 == null) {
                    if (val2 != null)
                        geqs = false;
                } else {
                    if (val2 == null) {
                        geqs = false;
                    } else {
                        if (val1.intValue() != val2.intValue())
                            geqs = false;
                    }
                }
                if (!geqs)
                    break;
            }
        }
        return geqs;
    }
}
