package Shared;

import java.awt.Desktop;
import java.io.IOException;
import java.net.URI;

public class Functions {
    public static void getOnlineHelp(String site) {
        if (Desktop.isDesktopSupported()) {
            Desktop desktop = Desktop.getDesktop();
            try {
                desktop.browse(new URI(site));
            } catch (Exception e) {
                e.printStackTrace();
            }
        } else {
            Runtime runtime = Runtime.getRuntime();
            try {
                runtime.exec("xdg-open " + site);
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }
}
