package Shared.Handling;

import java.util.ArrayList;
import java.util.List;

import Shared.interchange.Feature;
import Shared.interchange.Genome;
import Shared.interchange.Interval;
import Shared.interchange.Reference;

//would be for eventually talking to the database, perhaps should be moved to shared
public class DatabaseHandler {

    // database //family //virus
    private List<Reference> dummydata;

    public DatabaseHandler() {
        dummydata = new ArrayList<>();
    }

    // temporary, should probably be fetched by database in reality
    public List<Reference> getDummyData() {
        if (dummydata.isEmpty()) {
            dummydata.add(createReference("thing 1", 1));
            dummydata.add(createReference("thing 2", 2));
            dummydata.add(createReference("thing 3", 3));
        }

        return dummydata;
    }

    /**
     * Create reference with a {@code Genome} feature.
     * <p>
     * Genome feature has organism name set to name and genome id set to id.
     * 
     * @param name the organism name to use
     * @param id   the genome id to use
     * @return
     */
    private Reference createReference(String name, int id) {
        List<Interval> intervals = new ArrayList<>();
        intervals.add(new Interval(false, 1, 2));

        Genome genome = Genome.builder().withIntervals(intervals).withOrganismName(name).withGenomeID(id).build();
        List<Feature> features = new ArrayList<>();
        features.add(genome);

        return Reference.builder().withFeatures(features).build();
    }

}
