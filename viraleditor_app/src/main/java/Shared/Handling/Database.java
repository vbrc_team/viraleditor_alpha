package Shared.Handling;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URI;
import java.net.URISyntaxException;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;
import java.util.StringJoiner;

import com.fasterxml.jackson.databind.ObjectMapper;

import Shared.interchange.Interchange;

// TODO: Docstrings
// TODO: get config settings from file or something

public class Database {
	
	private List<Integer> family_ids = new ArrayList<> ();
	private List<Integer> genome_ids = new ArrayList<> ();
	private List<Integer> gene_ids = new ArrayList<> ();
	private List<Integer> ortho_group_ids = new ArrayList<> ();
	private boolean get_sequences = false;

	// URI components
	private String protocol;
	private String auth = null; // unused for now
	private String host;
	private int port;
	private String db_path; // Must be combined with endpoint
	private String fragment = null; // Unused for now
	
	// Endpoints
	private String family_endpoint;
	private String genome_endpoint;
	private String gene_endpoint;
	private String ortholog_endpoint;
	private String taxnode_endpoint;
	
		
	public Database() {
		// Setup URL and endpoints
		// These should be fetched from config settings
        protocol = "http";
        host = "4virology.net";
        port = 80;
        db_path = "/proxy";
		
		family_endpoint = "/families";
		genome_endpoint = "/genomes";
		gene_endpoint = "/genes";
		ortholog_endpoint = "/ortholog-groups";
		taxnode_endpoint = "/taxnodes";
	}
	
	private void clearIDs() {
		family_ids = new ArrayList<> ();
		genome_ids = new ArrayList<> ();
		gene_ids = new ArrayList<> ();
		ortho_group_ids = new ArrayList<> ();
		get_sequences = false;
	}
	
	// FAMILY
	
	/**
	 * Fetch virus family information from the database server, with family
	 * IDs optionally specified by {@link #families(List)}.
	 * <p>
	 * Returns an interchange object containing specified families, if IDs were
	 * provided, or all families (if IDs are not provided). 
	 * <p>
	 * Will clear Database object ID lists upon being called.
	 * 
	 * @return Interchange object containing references, each with an interchange
	 * Family feature.
	 * @throws IOException if query URI creation or database connection fails.
	 */
	public Shared.interchange.Interchange queryFamilies() throws IOException {
		
		URI query_uri;
		try {
			query_uri = buildFamilyQuery();
		} catch (URISyntaxException e) {
			throw new IOException("Failed to build family query URI", e);
		}
		
		return doQuery(query_uri);
	}
	
	// For ease of testing
	URI buildFamilyQuery() throws URISyntaxException {
		
		String query = null;
		String path = db_path + family_endpoint;
		
		if (!family_ids.isEmpty()) {
			String ids = listToCSV(family_ids);
			query = "ids=" + ids;
		}

		clearIDs();
		return new URI(protocol, auth, host, port, path, query, fragment);
	}
	
	// GENOME
	/**
	 * Fetch genomes from the database server, with genome IDs or family IDs
	 * optionally specified by {@link #genomes(List)} and {@link #families(List)},
	 * respectively. By default, does not include genome sequences, which must be
	 * explicitly requested with {@link #withSequences()}. If no IDs are provided,
	 * will return ALL genomes (in this case, sequences may not be requested, in 
	 * order to prevent unreasonable server load).
	 * <p>
	 * Genome IDs will take precedence over family IDs- i.e. if provided with both,
	 * query will be made with genome IDs only.
	 * <p>
	 * Will clear Database object ID lists upon being called.
	 * 
	 * @return An Interchange object containing one reference per genome. Note that
	 * genomes are represented as features within reference objects.
	 * @throws IOException if query URI creation or database connection fails.
	 */
	public Shared.interchange.Interchange queryGenomes() throws IOException {
		
		URI query_uri;
		try {
			query_uri = buildGenomeQuery();
		} catch (URISyntaxException e) {
			throw new IOException("Failed to build genome query URI", e);
		}
		
		return doQuery(query_uri);
	}
	
	URI buildGenomeQuery() throws URISyntaxException {
		
		String query = null;
		String path = db_path + genome_endpoint;
		
		if (!genome_ids.isEmpty()) {
			// query by genome id
			
			String ids = listToCSV(genome_ids);
			query = "ids=" + ids;
			if (get_sequences) {
				query += "&sequence=true";
			}
			
		} else if (!family_ids.isEmpty()) {
			// query by family id
			
			String ids = listToCSV(family_ids);
			query = "family-ids=" + ids;
			if (get_sequences) {
				query += "&sequence=true";
			}
		}
		
		clearIDs();
		return new URI(protocol, auth, host, port, path, query, fragment);
	}
	
	// GENE
	
	
	/**
	 * Fetch genes from the database server, with gene IDs or genome IDs 
	 * specified by {@link #genes(List)} and {@link #genomes(List)} respectively.
	 * By default, does not include gene sequences, which must be explicitly 
	 * requested with {@link #withSequences()}. If no IDs are provided, nothing
	 * is returned.
	 * <p>
	 * Note that, when querying by gene IDs, each gene feature will be placed in
	 * its own reference. When querying by genome IDs, however, all genes belonging
	 * to the same genome will be grouped in a reference. For instance, querying 
	 * for genes with IDs 1, 2, 3 will return an Interchange with 3 references,
	 * while querying for genes belonging to genomes with IDs 12, 34 will return an
	 * Interchange with two references- one containing all genes belonging to
	 * genome 12, and another containing all genes beloning to genome 34.
	 * <p>
	 * Gene IDs will take precedence over genome IDs- i.e. if provided with both, 
	 * query will be made with gene IDs only.
	 * <p>
	 * Will clear Database object ID lists upon being called.
	 * 
	 * @return An Interchange object containing either one reference per gene,
	 * or one reference per genome, as detailed above.
	 * @throws IOException if query URI creation or database connection fails.
	 */
	public Shared.interchange.Interchange queryGenes() throws IOException {
		
		URI query_uri;
		try {
			query_uri = buildGeneQuery();
		} catch (URISyntaxException e) {
			throw new IOException("Failed to build gene query URI", e);
		}
		return doQuery(query_uri);
	}
	
	URI buildGeneQuery() throws URISyntaxException {
		
		String path = db_path + gene_endpoint;
		String query = null;
		
		if (!gene_ids.isEmpty()) {
			// query by gene id
			
			String ids = listToCSV(gene_ids);
			
			// build URL + parameters
			query = "ids=" + ids;
			if (get_sequences) {
				query += "&sequence=true";
			}
			
		} else if (!genome_ids.isEmpty()) {
			// query by genome id
			
			String ids = listToCSV(genome_ids);
			
			// build URL + parameters
			query = "genome-ids=" + ids;
			if (get_sequences) {
				query += "&sequence=true";
			}
			
		}
		
		clearIDs();
		return new URI(protocol, auth, host, port, path, query, fragment);
	}

	// TAXNODE
	
	
	/**
	 * Fetch taxonomy node information from the database server. Does not take
	 * any IDs, simply returns all taxonomy nodes. 
	 * 
	 * @return An interchange object containing taxonomy nodes (WHEN I DO IT WHOOPS)
	 * @throws IOException if query URI creation or database connection fails.
	 */
	// TODO: THE INTERCHANGE CONVERSION FOR TAXNODES ISN'T DONE!!!
	public Shared.interchange.Interchange queryTaxnodes() throws IOException {
		
		URI query_uri;
		try {
			query_uri = buildTaxnodeQuery();
		} catch (URISyntaxException e) {
			throw new IOException("Failed to build taxonomy node query URI", e);
		}
		return doQuery(query_uri);
	}
	
	URI buildTaxnodeQuery() throws URISyntaxException  {
		
		String path = db_path + taxnode_endpoint;
		String query = null;
		
		clearIDs();
		return new URI(protocol, auth, host, port, path, query, fragment);
	}
	
	// ORTHOLOG GROUPS
	
	/**
	 * Fetch ortholog groups from the database server, optionally by ortholog group
	 * IDs specified by {@link #orthologGroups(List)}. If no IDs are provided, 
	 * returns all ortholog groups.
	 * <p>
	 * Ortholog groups are each placed in their own reference within an 
	 * interchange object. 
	 *  
	 * @return An interchange object containing ortholog group features, each 
	 * within its own reference.
	 * @throws IOException if query URI creation or database connection fails.
	 */
	public Shared.interchange.Interchange queryOrthologGroups() throws IOException {
		URI query_uri;
		try {
			query_uri = buildOrthologGroupQuery();
		} catch (URISyntaxException e) {
			throw new IOException("Failed to build ortholog group query URI", e);
		}
		
		return doQuery(query_uri);
	}
	
	URI buildOrthologGroupQuery() throws URISyntaxException {
		
		String path = db_path + ortholog_endpoint;
		String query = null;
		
		if (!ortho_group_ids.isEmpty()) {
			String ids = listToCSV(ortho_group_ids);
			query = "ids=" + ids;
		}
		
		clearIDs();
		return new URI(protocol, auth, host, port, path, query, fragment);
	}
	
	// helper method to actually do the queries
	// TODO: probably best to handle some of these exceptions here
	private Interchange doQuery(URI query_uri) throws IOException {
		
		URL url = query_uri.toURL();
		HttpURLConnection con = (HttpURLConnection) url.openConnection();
		con.setRequestMethod("GET");
		
		String inputLine;
		StringBuffer content = new StringBuffer();
		
		try (BufferedReader in = new BufferedReader(new InputStreamReader(con.getInputStream()))) {
			while ((inputLine = in.readLine()) != null) {
				content.append(inputLine);
			}	
		}

		con.disconnect();
		String json = content.toString();
		Shared.interchange.Interchange interchange;
		ObjectMapper mapper = new ObjectMapper();
		
		interchange = mapper.reader().readValue(json, Interchange.class);
		return interchange;
	}
	
	private String listToCSV(List<Integer> ids) {
		StringJoiner joiner = new StringJoiner(",");
		for (Integer i : ids) {
			joiner.add(i.toString());
		}
		return joiner.toString();
	}
	
	// Setters

	public Database families(List<Integer> family_ids) {
		this.family_ids = family_ids;
		return this;
	}

	public Database genomes(List<Integer> genome_ids) {
		this.genome_ids = genome_ids;
		return this;
	}

	public Database genes(List<Integer> gene_ids) {
		this.gene_ids = gene_ids;
		return this;
	}

	public Database orthologGroups(List<Integer> ortho_group_ids) {
		this.ortho_group_ids = ortho_group_ids;
		return this;
	}

	public Database withSequences() {
		this.get_sequences = true;
		return this;
	}
}
