package Shared;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

//Reference Gene.java in VRBC for original legacy code
public class Gene {
    public String geneName;
    public int virusID;

    // on the fly calculated values
    public HashMap<String, Double> amino_acid_freq;
    public HashMap<String, Double> nucleotide_freq;
    public HashMap<String, Double> dinucleotide_freq;
    public HashMap<String, Double> codon_freq;
    public int size;
    public int num_aa;
    public int orf_start;
    public int orf_stop;
    public double aComp;
    public double cComp;
    public double gComp;
    public double tComp;

    // gene table
    public int gfid;
    public int id;
    public String name; // gene abbrev
    public String strand;
    public String annotator;
    public String anno_source;
    public String fragment; // molecule_type
    public String inserted_on;
    public String last_modified;
    public String function_name;
    public String feature_name;
    public String time_of_expression; // regulation
    public String ec_num;
    public String description;
    public String notes;
    public String note_desc;
    public String genbank_name;
    public int genbank_id; // gb_gene_gi
    public String proteinId; // gb_gene_accession
    public int gb_gene_version;
    public int parent_gene_id;
    public int family_id;
    public String manual_family; // evid_name
    public String upstream_seq;
    public String dnaSeq;
    public String protein_seq;
    public int gene_seq_version;

    // protein_properties table
    public ProteinProperties protein_properties;
    // gene_composition table
    public GeneComposition gene_compositions;

    public List<Orf> orfs;

    // genome characteristics
    public boolean circular = false;
    public boolean single = false;

    public boolean debug = false;

    public Gene() {
        virusID = 0;
        gfid = 0;
        id = 0;
        name = "";
        geneName = "";
        strand = "+";
        feature_name = "Protein (+ strand)";
        annotator = "";
        anno_source = "manual";
        fragment = "protein";
        inserted_on = "";
        last_modified = "";
        function_name = "";
        time_of_expression = "";
        ec_num = "";
        description = "";
        notes = "";
        note_desc = "";
        genbank_name = "";
        genbank_id = 0;
        proteinId = "";
        gb_gene_version = 0;
        parent_gene_id = 0;
        family_id = 0;
        manual_family = "manual";
        dnaSeq = "";
        protein_seq = "";
        upstream_seq = "";
        gene_seq_version = 1;

        initVariables();
    }

    /**
     * initialize variables for calculated values
     */
    private void initVariables() {
        protein_properties = new ProteinProperties();
        gene_compositions = new GeneComposition();
        amino_acid_freq = new HashMap<>();
        for (int i = 0; i < SequenceConstants.aminoAcids.length; i++) {
            amino_acid_freq.put(SequenceConstants.aminoAcids[i], 0.0);
        }
        nucleotide_freq = new HashMap<>();
        for (int i = 0; i < SequenceConstants.nucleotides.length; i++) {
            nucleotide_freq.put(SequenceConstants.nucleotides[i], 0.0);
        }
        dinucleotide_freq = new HashMap<>();
        for (int i = 0; i < SequenceConstants.dinucleotides.length; i++) {
            dinucleotide_freq.put(SequenceConstants.dinucleotides[i], 0.0);
        }
        codon_freq = new HashMap<>();
        for (int i = 0; i < SequenceConstants.codons.length; i++) {
            codon_freq.put(SequenceConstants.codons[i], 0.0);
        }
        size = 0;
        num_aa = 0;
        orf_start = 0;
        orf_stop = 0;
        orfs = new ArrayList<>();
        circular = false;
        single = false;
        debug = false;
    }

    // get functions are used by cell value factory to load into tables.

    public String getGeneName() {
        if (geneName.equals("")) {
            geneName = name;
        }
        return this.geneName;
    }

    public int getVirusID() {
        if (id != 0) {
            virusID = id;
        }
        return this.virusID;
    }

    public double getAComp() {
        BigDecimal bd = BigDecimal.valueOf(aComp).setScale(4, RoundingMode.HALF_UP);
        return bd.doubleValue();
    }

    public double getCComp() {
        BigDecimal bd = BigDecimal.valueOf(cComp).setScale(4, RoundingMode.HALF_UP);
        return bd.doubleValue();
    }

    public double getGComp() {
        BigDecimal bd = BigDecimal.valueOf(gComp).setScale(4, RoundingMode.HALF_UP);
        return bd.doubleValue();
    }

    public double getTComp() {
        BigDecimal bd = BigDecimal.valueOf(tComp).setScale(4, RoundingMode.HALF_UP);
        return bd.doubleValue();
    }

    public int getSize() {
        return this.size;
    }

    /**
     * Get a composition count
     * 
     * @return count for a specific composition
     */
    public double getCompostition(String key) {
        Double comp;
        double cnt;

        comp = nucleotide_freq.get(key);
        if (comp == null)
            comp = dinucleotide_freq.get(key);
        if (comp == null)
            comp = codon_freq.get(key);
        if (comp == null)
            cnt = -1;
        else
            cnt = comp;
        return cnt;
    }

    /**
     * Get a composition count for all a, c, g, t of nucleotide sequence
     */
    public void setCompositions() {
        int[] counts = new int[] { 0, 0, 0, 0 };
        double[] compositions = new double[] { 0.0, 0.0, 0.0, 0.0 };
        for (int i = 0; i < dnaSeq.length(); i++) {
            if ((dnaSeq.charAt(i) == 'a') || (dnaSeq.charAt(i) == 'A'))
                counts[0]++;
            else if ((dnaSeq.charAt(i) == 'c') || (dnaSeq.charAt(i) == 'C'))
                counts[1]++;
            else if ((dnaSeq.charAt(i) == 'g') || (dnaSeq.charAt(i) == 'G'))
                counts[2]++;
            else if ((dnaSeq.charAt(i) == 't') || (dnaSeq.charAt(i) == 'T'))
                counts[3]++;
        }
        for (int i = 0; i < 4; i++) {
            compositions[i] = ((double) counts[i]) / dnaSeq.length();
        }
        setAComp(compositions[0]);
        setCComp(compositions[1]);
        setGComp(compositions[2]);
        setTComp(compositions[3]);
    }

    /**
     * Set the gene size
     */
    public void setSize() {
        size = dnaSeq.length();
        if (dnaSeq.endsWith("TAA") || dnaSeq.endsWith("taa")) {
            size = dnaSeq.length() - 3;
        } else {
            if (dnaSeq.endsWith("TAG") || dnaSeq.endsWith("tag")) {
                size = dnaSeq.length() - 3;
            } else {
                if (dnaSeq.endsWith("TGA") || dnaSeq.endsWith("tga")) {
                    size = dnaSeq.length() - 3;
                } else {
                    size = dnaSeq.length();
                }
            }
        }
        if (size < 0)
            size = 0;
    }

    /**
     * Set the gene size
     * 
     * @param s the size of the gene
     */
    public void setSize(int s) {
        size = s;
    }

    /**
     * Set the dna sequence of the gene
     * 
     * @param dna the DNA sequence of the gene
     */
    public void setDNA(String dna) {
        dnaSeq = dna.toUpperCase();
        setSize();
    }

    public void setVirusID(int id) {
        this.virusID = id;
    }

    public void setGeneName(String name) {
        this.geneName = name;
    }

    public void setAComp(double comp) {
        this.aComp = comp;
    }

    public void setCComp(double comp) {
        this.cComp = comp;
    }

    public void setGComp(double comp) {
        this.gComp = comp;
    }

    public void setTComp(double comp) {
        this.tComp = comp;
    }

    public String getDnaSeq() {
        return this.dnaSeq;
    }

    public int getOrf_start() {
        return orf_start;
    }

    public int getOrf_stop() {
        return orf_stop;
    }

    /**
     * Set the feature_name of the gene
     * 
     * @param fn the gene
     */
    public void setFeature(String fn) {
        feature_name = fn;
    }

    /**
     * Set the feature_name of the gene based on the strand
     */
    public void setFeature() {
        if (strand.equals("+")) {
            feature_name = "Protein (+ strand)";
        } else {
            feature_name = "Protein (- strand)";
        }
    }

    /**
     * Set the protein sequence of this gene
     * 
     * @param seq the gene protein sequence
     */
    public void setProteinSeq(String seq) {
        protein_seq = seq.toUpperCase();
        setNumAA();
    }

    /**
     * Set the protein sequence of this gene based on the genes DNA seq Used in (for
     * example) Gene Panel - Calculate
     */
    public void setProteinSeq() {
        protein_seq = "";
        setSize();
        protein_seq = SequenceUtility.setProteinSeq(dnaSeq);
        setNumAA();
    }

    /**
     * Set the number of amino acids in the gene
     */
    public void setNumAA() {
        num_aa = protein_seq.length();
        if (num_aa < 0)
            num_aa = 0;
    }

    /**
     * Set the number of amino acids in the gene
     * 
     * @param naa the number of amini acids in the gene
     */
    public void setNumAA(int naa) {
        num_aa = naa;
    }

    /**
     * Calculates the molecular weight of a protein given by the inputted string and
     * a hashtable consisting of a String mapping one letter amino acid symbols to
     * their weight in Daltons Note: U = selenocysteine which is the 21st amino
     * acid, X is unknown and has been given a weight of 135 daltons which is close
     * to the "average" weight of an amino acid
     */
    public void calculateMW() {
        protein_properties.setMW(protein_seq);
    }

    /**
     * This function calculcates the best isolelectric point from a string of amino
     * acids: The formula used is: Q = Sum( Nt / (1+10^(h-pKt) ) ) where Q = charge
     * of peptide or protein, Nt (number of amino acids of type t, h = ph, and pkT =
     * Ka of amino acid A transcendal equation is solved, ie) ph is changed until it
     * is very close to zero.
     */
    public void calculatePI() {
        protein_properties.setPI(protein_seq);
    }

    /**
     * Get the isoelectric point of the protein
     * 
     * @return isoelectric point
     */
    public double getPi() {
        return protein_properties.getPI();
    }

    /**
     * Define genome of gene to be circular
     * 
     * @param circle true if the genome is circular
     */
    public void isCircular(boolean circle) {
        circular = circle;
    }

    /**
     * Count the occurrences of a,c,g, and t in the gene dna sequence and calculate
     * their frequency
     */
    public void calculateXcnt() {
        Integer cnt;
        int cntVal;
        double mysize, freqVal;
        HashMap<String, Integer> nucleotide_counts;

        nucleotide_freq = new HashMap<>();
        for (int i = 0; i < SequenceConstants.nucleotides.length; i++) {
            nucleotide_freq.put(SequenceConstants.nucleotides[i], 0.0);
        }
        gene_compositions.setNucleotideCounts(dnaSeq);
        nucleotide_counts = gene_compositions.getNucleotideCounts();
        mysize = dnaSeq.length();
        for (int i = 0; i < SequenceConstants.nucleotides.length; i++) {
            cnt = nucleotide_counts.get(SequenceConstants.nucleotides[i]);
            if (cnt == null) {
                freqVal = 0.0;
            } else {
                cntVal = cnt;
                freqVal = cntVal / mysize;
            }
            nucleotide_freq.put(SequenceConstants.nucleotides[i], freqVal);
        }
    }

    /**
     * Count the occurrences of aa,ac,ag,at,ca,cc,cg,ct, ga,gc,gg,gt,ta,tc,tg, and
     * tt in the gene dna sequence and calculate their frequency
     */
    public void calculateXXcnt() {
        Integer cnt;
        int cntVal;
        double mysize, freqVal;
        HashMap<String, Integer> dinucleotide_counts;

        dinucleotide_freq = new HashMap<>();
        for (int i = 0; i < SequenceConstants.dinucleotides.length; i++) {
            dinucleotide_freq.put(SequenceConstants.dinucleotides[i], 0.0);
        }
        gene_compositions.setDinucleotideCounts(dnaSeq);
        dinucleotide_counts = gene_compositions.getDinucleotideCounts();
        mysize = dnaSeq.length();
        for (int i = 0; i < SequenceConstants.dinucleotides.length; i++) {
            cnt = dinucleotide_counts.get(SequenceConstants.dinucleotides[i]);
            if (cnt == null) {
                freqVal = 0.0;
            } else {
                cntVal = cnt;
                freqVal = cntVal / mysize;
            }
            dinucleotide_freq.put(SequenceConstants.dinucleotides[i], freqVal);
        }
    }

    /**
     * Count the occurrences of aaa,aac,aag,aat,aca,acc,acg,act,
     * aga,agc,agg,agt,ata,atc,atg,att,caa,cac,cag,cat,cca,ccc,ccg,cct,
     * cga,cgc,cgg,cgt,cta,ctc,ctg,ctt,gaa,gac,gag,gat,gca,gcc,gcg,gct,
     * gga,ggc,ggg,ggt,gta,gtc,gtg,gtt,taa,tac,tag,tat,tca,tcc,tcg,tct,
     * tga,tgc,tgg,tgt,tta,ttc,ttg, and ttt in the gene dna sequence and calculate
     * their frequency
     */
    public void calculateXXXcnt() {
        Integer cnt;
        int cntVal;
        double mysize, freqVal;
        HashMap<String, Integer> codon_counts;

        codon_freq = new HashMap<>();
        for (int i = 0; i < SequenceConstants.codons.length; i++) {
            codon_freq.put(SequenceConstants.codons[i], 0.0);
        }
        gene_compositions.setCodonCounts(dnaSeq);
        codon_counts = gene_compositions.getCodonCounts();
        mysize = dnaSeq.length() / 3;
        for (int i = 0; i < SequenceConstants.codons.length; i++) {
            cnt = codon_counts.get(SequenceConstants.codons[i]);
            if (cnt == null) {
                freqVal = 0.0;
            } else {
                cntVal = cnt;
                freqVal = cntVal / mysize;
            }
            codon_freq.put(SequenceConstants.codons[i], freqVal);
        }
    }

    /**
     * Count the occurrences of the amino acids and calculate the frequency of the
     * amino acids
     */
    public void calculateXaacCnt() {
        Integer cnt;
        int cntVal;
        double mysize, freqVal;
        HashMap<String, Integer> amino_acid_counts;

        amino_acid_freq = new HashMap<>();
        for (int i = 0; i < SequenceConstants.aminoAcids.length; i++) {
            amino_acid_freq.put(SequenceConstants.aminoAcids[i], 0.0);
        }
        gene_compositions.setAminoAcidCounts(protein_seq);
        amino_acid_counts = gene_compositions.getAminoAcidCounts();
        mysize = protein_seq.length();
        for (int i = 0; i < SequenceConstants.aminoAcids.length; i++) {
            cnt = amino_acid_counts.get(SequenceConstants.aminoAcids[i]);
            if (cnt == null) {
                freqVal = 0.0;
            } else {
                cntVal = cnt;
                freqVal = cntVal / mysize;
            }
            amino_acid_freq.put(SequenceConstants.aminoAcids[i], freqVal);
        }
    }

    /**
     * Sets the start and stop  of the open reading frame,
     * in absolute genomic terms
     */
    private void setOrfStartStop() { 
      if (orfs == null) return;
      for (int k=0; k< orfs.size(); k++) {
        Orf co = orfs.get(k);
        if (co.getPosition() == 1) {
            orf_start = co.getStart();
            orf_stop = co.getStop();
          break;
        }
      }
    }
    
    /** 
     * Add an orf to the gene orfs 
     * @param s the orf start location of the orf to add to the gene orfs
     * @param e the orf stop location of the orf to add to the gene orfs
     */ 
    public void addOrf(int s, int e) {
      int n = 0;
      Orf o;
   
      if (orfs == null) orfs = new ArrayList<Orf>();
      n = orfs.size() + 1;
      o = new Orf(id, s, e, n);
      orfs.add(o);
      setOrfStartStop();
    }
    
    /**
     * Convert object to string
     * 
     * @return string representation of object
     */
    @Override
    public String toString() {
        StringBuilder str;
        Orf o;

        str = new StringBuilder("Virus=" + virusID + "  Gene: " + id + " " + name + " " + size + "bp " + num_aa + "aa "
                + " family=" + family_id + " location=" + orf_start + "-" + orf_stop + " " + strand + " fragment="
                + fragment);
        if (orfs != null) {
            if (orfs.size() > 0) {
                str.append(" ORFs:");
                for (Orf orf : orfs) {
                    o = orf;
                    str.append(" [").append(o.getPosition()).append("]").append(o.getStart()).append("-")
                            .append(o.getStop());
                }
            }
        }
        return str.toString();
    }

}
