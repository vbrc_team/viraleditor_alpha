package Shared;

import javafx.stage.Stage;

public abstract class AbstractDialog<T> {
	protected Stage stage;
	protected T composer;
	protected Performance<?, ?> performance;
	public abstract void initializeDialog();
	public abstract void mountChildPerformances();
}
