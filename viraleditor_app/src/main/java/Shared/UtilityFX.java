package Shared;

import java.io.File;
import java.io.IOException;

import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.scene.layout.Pane;
import javafx.stage.Stage;

public class UtilityFX {

	public abstract class DialogWrapper<T> {
		
		protected Stage stage;
		protected T composer;
		//SubsceneMountPair[] MOUNT_LIST;
		
		public abstract void initializeDialog();
		public abstract void constructMounts();
		
		class SubsceneMountPair {
			Pane mount;
			String subsceneFXML;
			
			protected SubsceneMountPair(Pane mount, String subsceneFXML) {
				this.mount = mount;
				this.subsceneFXML = subsceneFXML;
			}
		}		
	}
	
	interface ComposerWrapper<T> {}
	
	public abstract class SceneWrapper<T extends DialogWrapper<C>, C extends ComposerWrapper<T>> {
		T dialog;
		C composer;
		Stage stage;
				
		public SceneWrapper(Stage stage, String FXMLFile, C composer) throws IOException {
			//T.MOUNT_LIST = null;
			FXMLLoader loader = new FXMLLoader(new File("src/main/resources/" + FXMLFile).toURI().toURL());
	    	stage.setScene(new Scene( (new FXMLLoader(new File("src/main/resources/" + FXMLFile).toURI().toURL())).load()));
	    	T controller = loader.getController();
	    	
	    	this.composer = composer;
	    	this.dialog = controller;
	
	    	this.stage = stage;
		}
	}
}
