package Shared;

/**
 * This class represents an inverted terminal repeat (ITR)
 * 
 * @author Angelika Ehlers
 * @version 1.0
 */

public class Itr {
    private int gfid;
    private int vid;
    private int no;
    private int loc;
    private int start;
    private int stop;
    private String sequence;

    /**
     * Constructor
     * 
     * @param virid    the virus id
     * @param itrno    the itr id/number
     * @param pos      the location of the itr (0=left, 1=right)
     * @param itrstart the start position of the itr
     * @param itrstop  the stop position of the itr
     * @param seq      the itr sequence
     */
    public Itr(int virid, int itrno, int pos, int itrstart, int itrstop, String seq) {
        vid = virid;
        no = itrno;
        loc = pos;
        start = itrstart;
        stop = itrstop;
        sequence = seq;
    }

    /**
     * Get the genome family id
     * 
     * @return the genome family id
     */
    public int getGenomeFamilyId() {
        return gfid;
    }

    /**
     * Get the virus id
     * 
     * @return the virus id
     */
    public int getVirusId() {
        return vid;
    }

    /**
     * Get the itr id/number
     * 
     * @return the itr id
     */
    public int getItrNo() {
        return no;
    }

    /**
     * Get the itr location type (0=left; 1=right)
     * 
     * @return the itr location
     */
    public int getLocation() {
        return loc;
    }

    /**
     * Get the itr start location
     * 
     * @return the itr start location
     */
    public int getStart() {
        return start;
    }

    /**
     * Get the itr stop location
     * 
     * @return the itr stop location
     */
    public int getStop() {
        return stop;
    }

    /**
     * Get the itr sequence
     * 
     * @return the itr sequence
     */
    public String getSequence() {
        return sequence;
    }
}
