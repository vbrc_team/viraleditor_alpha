package Shared;

import java.text.DecimalFormat;
import java.util.HashMap;
import java.util.Iterator;

/**
 * This class represents a row in the genes table
 * 
 * @version 1.3
 */

public class ProteinProperties {
    private double mol_weight;
    private double pi; // isoelectric point
    private String has_coil;
    private String has_signal_peptide;
    private int num_trans_membrane;
    private String prosite_motifs;
    private String coiled_coil;
    private String signal_peptide;
    private String trans_mem;

    /**
     * ProteinProperties class constructor
     * 
     * @param fid the genome family id
     * @param gid the gene id
     */
    public ProteinProperties() {
        has_coil = "N";
        has_signal_peptide = "N";
        prosite_motifs = "";
        coiled_coil = "";
        signal_peptide = "";
        trans_mem = "";
    }

    /**
     * Get the moleculeor weight of the protein
     * 
     * @return molecular weight
     */
    public double getMW() {
        return mol_weight;
    }

    /**
     * Get the isoelectric point of the protein
     * 
     * @return isoelectric point
     */
    public double getPI() {
        return pi;
    }

    /**
     * Get if gene has coil
     * 
     * @return if gene has coil
     */
    public String getHasCoil() {
        return has_coil;
    }

    /**
     * Get if gene has signal peptide
     * 
     * @return if gene has signal peptide
     */
    public String getHasSignalPeptide() {
        return has_signal_peptide;
    }

    /**
     * Get number of trans membrane of gene
     * 
     * @return the number of trns membrane
     */
    public int getNumTransMem() {
        return num_trans_membrane;
    }

    /**
     * Get the prosite motifs of gene
     * 
     * @return the prosite motifs
     */
    public String getPrositeMotifs() {
        return prosite_motifs;
    }

    /**
     * Get the coiled coil of gene
     * 
     * @return the coiled coil
     */
    public String getCoiledCoil() {
        return coiled_coil;
    }

    /**
     * Get the signal peptide of gene
     * 
     * @return the signal peptide
     */
    public String getSignalPeptide() {
        return signal_peptide;
    }

    /**
     * Get the trans membrane of gene
     * 
     * @return the trans membrane
     */
    public String getTransMem() {
        return trans_mem;
    }

    /**
     * Set the molecular weight of the gene
     * 
     * @param mw the molecular weight of the gene
     */
    public void setMW(float mw) {
        mol_weight = mw;
    }

    /**
     * Set the molecular weight of the gene
     * 
     * @param mw the molecular weight of the gene
     */
    public void setMW(double mw) {
        mol_weight = mw;
    }

    /**
     * Calculates the molecular weight of a protein given by the input string and a
     * hashtable consisting of a String mapping one letter amino acid symbols to
     * their weight in Daltons. Note: U = selenocysteine which is the 21st amino
     * acid, X is unknown and has been given a weight of 135 Daltons which is close
     * to the "average" weight of an amino acid
     * 
     * @param protein_seq the protein sequence of the gene
     */
    public void setMW(String protein_seq) {
        int i;
        Float temp = null;
        float water = (float) 18.0152; // Molecular weight of H20
        float sum = 0;
        HashMap<String, Float> aa_weights;
        String upper_sequence;
        int len;

        aa_weights = new HashMap<>();
        for (i = 0; i < SequenceConstants.aminoAcidWeights.length; i = i + 2) {
            temp = new Float(SequenceConstants.aminoAcidWeights[i + 1]);
            aa_weights.put(SequenceConstants.aminoAcidWeights[i], temp);
        }
        if (protein_seq == null)
            return;
        if (protein_seq.length() < 1)
            return;
        try {
            upper_sequence = protein_seq.toUpperCase();
            len = upper_sequence.length();
            for (i = 0; i < len; i++) {
                char c = upper_sequence.charAt(i);
                temp = aa_weights.get("" + c);
                if (temp == null) {
                    if (c != '*')
                        System.out.println("Unknown character in sequence: " + c); // mjw Hack for STAR char
                    temp = (float) 0;
                }
                sum = sum + temp;
            }
            // One H20 is lost per peptide bond
            mol_weight = sum - ((upper_sequence.length() - 1) * water);
            DecimalFormat formatter = new DecimalFormat("#######.####");
            String nstr = formatter.format(mol_weight);
            nstr.replace(",", ".");
            try {
                mol_weight = new Double(nstr);
            } catch (NumberFormatException e) {
                e.printStackTrace();
            }
        } catch (Exception e) {
            System.out.println("Molecular weight not calculated correctly for " + protein_seq);
            System.out.println("HashMap was: " + aa_weights.toString());
            System.out.println("Error is: " + e.getMessage());
            e.printStackTrace();
            System.exit(0);
        }
    }

    /**
     * Set isoelectric point of the gene
     * 
     * @param p the isoelectric point of the gene
     */
    public void setPI(float p) {
        pi = p;
    }

    /**
     * Set isoelectric point of the gene
     * 
     * @param p the isoelectric point of the gene
     */
    public void setPI(double p) {
        pi = p;
    }

    /**
     * This function calculates the best isoelectric point from a string of amino
     * acids: The formula used is: Q = Sum( Nt / (1+10^(h-pKt) ) ) where Q = charge
     * of peptide or protein, Nt (number of amino acids of type t, h = ph, and pkT =
     * Ka of amino acid A transcendental equation is solved, ie) ph is changed until
     * it is very close to zero.
     * 
     * @param protein_seq the protein sequence of the gene
     */
    public void setPI(String protein_seq) {
        Float temp;
        int i;
        HashMap<String, Float> pka_table = new HashMap<>();
        for (i = 0; i < SequenceConstants.pka_pkbs.length; i = i + 2) {
            temp = new Float(SequenceConstants.pka_pkbs[i + 1]);
            pka_table.put(SequenceConstants.pka_pkbs[i], temp);
        }
        HashMap<String, Float> aminoterm = new HashMap<>();
        for (i = 0; i < SequenceConstants.nt_terminus_pi.length; i = i + 2) {
            temp = new Float(SequenceConstants.nt_terminus_pi[i + 1]);
            aminoterm.put(SequenceConstants.nt_terminus_pi[i], temp);
        }
        HashMap<String, Float> carboxyterm = new HashMap<>();
        for (i = 0; i < SequenceConstants.ct_terminus_pi.length; i = i + 2) {
            temp = new Float(SequenceConstants.ct_terminus_pi[i + 1]);
            carboxyterm.put(SequenceConstants.ct_terminus_pi[i], temp);
        }
        HashMap<String, Integer> count = new HashMap<>();
        int old;
        char put_char, first, last;
        String put_string;
        Float at_value, ct_value;
        Integer old_num, new_num; // number of occurrence of a particular aa type
        float h, old_h, low, high;

        if ((protein_seq == null) || (protein_seq.length() < 1)) {
            System.out.println("Error calculating PI, wrong protein seq (" + protein_seq + ")");
            return;
        }
        // Set up pka's for terminal amino acids
        first = protein_seq.charAt(0);
        put_string = "" + first;
        at_value = aminoterm.get(put_string.trim());
        if (at_value == null)
            at_value = new Float("7.50");
        last = protein_seq.charAt(protein_seq.length() - 1);
        put_string = "" + last;
        ct_value = carboxyterm.get(put_string.trim());
        if (ct_value == null)
            ct_value = new Float("3.55");
        pka_table.put("<", at_value);
        pka_table.put(">", ct_value);
        StringBuilder modified = new StringBuilder(protein_seq);
        modified.replace(0, 1, "<");
        modified.replace(modified.length() - 1, modified.length(), ">");

        /*
         * Sum up the tye of each amino acid, calculate Nt Put it into a hash table
         */
        try {
            for (i = 0; i < modified.length(); i++) {
                put_char = modified.charAt(i);
                put_string = "" + put_char;
                old_num = count.get(put_string);
                if (old_num == null) {
                    new_num = 1;
                } else {
                    old = old_num;
                    new_num = old + 1;
                }
                count.put(put_string, new_num);
            }
        } catch (Exception e) {
            System.out.println("Failed to set up count hashtable in pi calc " + e.getMessage());
            e.printStackTrace();
        }

        /* Look through each individual amino acid */
        h = (float) 7.0; // Initial starting Ph
        old_h = 0; // Previous Ph
        low = 0;
        high = 14;
        float result;
        Float temp_acid, temp_base;
        String temp_string;
        Iterator<String> keys;

        while (Math.abs(h - old_h) > 0.001) {
            keys = count.keySet().iterator();
            result = (float) 0.0;
            try {
                while (keys.hasNext()) {
                    temp_string = keys.next();
                    if (temp_string.equals("C") || temp_string.equals("D") || temp_string.equals("E")
                            || temp_string.equals(">") || temp_string.equals("Y")) {
                        // Acid found
                        temp_acid = pka_table.get(temp_string);
                        result += count.get(temp_string) / (float) (1 + Math.pow(10, (temp_acid.doubleValue() - h)));
                    }
                    if (temp_string.equals("H") || temp_string.equals("K") || temp_string.equals("<")
                            || temp_string.equals("R")) {
                        // Base found
                        temp_base = pka_table.get(temp_string);
                        result -= count.get(temp_string) / (float) (1 + Math.pow(10, (h - temp_base.doubleValue())));
                    }
                }
            } catch (Exception e) {
                System.out.println("Trouble during transcendental equation " + e.getMessage());
                e.printStackTrace();
            }
            // Recalibrate parameters for next iteration
            try {
                old_h = h;
                if (result > 0) {
                    h = (h + low) / 2;
                    high = old_h;
                } else {
                    h = (h + high) / 2;
                    low = old_h;
                }
            } catch (Exception e) {
                System.out.println("Exception in calculating ph... h: " + h + " low: " + low + "high: " + high
                        + "message: " + e.getMessage());
                e.printStackTrace();
            }
        } // end while
        pi = h;
        DecimalFormat formatter = new DecimalFormat("#######.####");
        String nstr = formatter.format(pi);
        nstr.replace(",", ".");
        try {
            pi = new Double(nstr);
        } catch (NumberFormatException e) {
            e.printStackTrace();
        }
    }

    /**
     * Set if gene has signal peptide
     * 
     * @param hsp if gene has signal peptide
     */
    public void setHasSignalPeptide(String hsp) {
        has_signal_peptide = hsp;
    }

    /**
     * Set if gene has coil
     * 
     * @param hc if gene has coil
     */
    public void setHasCoil(String hc) {
        has_coil = hc;
    }

    /**
     * Set number of trans membrane of gene
     * 
     * @param ntm the number of trans membrane
     */
    public void setNumTransMem(int ntm) {
        num_trans_membrane = ntm;
    }

    /**
     * Set the prosite motifs of gene
     * 
     * @param pm the prosite motifs
     */
    public void setPrositeMotifs(String pm) {
        prosite_motifs = pm;
    }

    /**
     * Set the coiled coil of gene
     * 
     * @param cc the coiled coil
     */
    public void setCoiledCoil(String cc) {
        coiled_coil = cc;
    }

    /**
     * Set the signal peptide of gene
     * 
     * @param sp the signal peptide
     */
    public void setSignalPeptide(String sp) {
        signal_peptide = sp;
    }

    /**
     * Set the trans membrane of gene
     * 
     * @param tm the trans membrane
     */
    public void setTransMem(String tm) {
        trans_mem = tm;
    }

    /**
     * Compare protein properties of genes
     * 
     * @param g the protein properties to compare with
     * @return if equal
     */
    public boolean isEqual(ProteinProperties g) {
        boolean geqs = true;
        if (has_coil == null) {
            if (g.has_coil != null)
                geqs = false;
        } else {
            if (g.has_coil == null)
                geqs = false;
            else if (!has_coil.equals(g.has_coil))
                geqs = false;
        }
        if (has_signal_peptide == null) {
            if (g.has_signal_peptide != null)
                geqs = false;
        } else {
            if (g.has_signal_peptide == null)
                geqs = false;
            else if (!has_signal_peptide.equals(g.has_signal_peptide))
                geqs = false;
        }
        if (num_trans_membrane != g.num_trans_membrane)
            geqs = false;
        if (prosite_motifs == null) {
            if (g.prosite_motifs != null)
                geqs = false;
        } else {
            if (g.prosite_motifs == null)
                geqs = false;
            else if (!prosite_motifs.equals(g.prosite_motifs))
                geqs = false;
        }
        if (coiled_coil == null) {
            if (g.coiled_coil != null)
                geqs = false;
        } else {
            if (g.coiled_coil == null)
                geqs = false;
            else if (!coiled_coil.equals(g.coiled_coil))
                geqs = false;
        }
        if (signal_peptide == null) {
            if (g.signal_peptide != null)
                geqs = false;
        } else {
            if (g.signal_peptide == null)
                geqs = false;
            else if (!signal_peptide.equals(g.signal_peptide))
                geqs = false;
        }
        if (trans_mem == null) {
            if (g.trans_mem != null)
                geqs = false;
        } else {
            if (g.trans_mem == null)
                geqs = false;
            else if (!trans_mem.equals(g.trans_mem))
                geqs = false;
        }
        return geqs;
    }
}
