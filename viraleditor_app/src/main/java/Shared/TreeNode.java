package Shared;

import Shared.interchange.Reference;
import javafx.scene.control.TreeView;

/**
 * A TreeNode stores a name and a {@link Reference} to be used by a
 * {@link TreeView}.
 * <p>
 * The name is used as the node display name and the {@code Reference} to store
 * data.
 */
public class TreeNode {
    private final String name;
    private final Reference reference;

    /**
     * Construct a TreeNode with a name and reference.
     *
     * @param name      name to display when calling {@code #toString()}
     * @param reference reference to store
     */
    public TreeNode(String name, Reference reference) {
        this.name = name;
        this.reference = reference;
    }

    /**
     * Return the stored reference.
     *
     * @return the stored reference
     */
    public Reference getReference() {
        return reference;
    }

    // Used by TreeView as Display name
    @Override
    public String toString() {
        return name;
    }
}
