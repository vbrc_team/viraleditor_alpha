package Shared;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;

import org.fxmisc.richtext.StyleClassedTextArea;

import javafx.fxml.FXMLLoader;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.layout.Pane;
import javafx.stage.Stage;

/**
 * The performance class is a container responsible for handling the mounting and rendering of nested dialog-composer pairs (known as performances). 
 * <p> The specific loading order of JavaFX and CSS components is handled by the "mount" and "render" methods,
 * provided that generic classes T and C extend AbstractDialog[C] and AbstractComposer[T] respectively.
 * 
 * <p> The construction of a performance instance is inherently recursive, 
 * and child performances which are to be mounted within the desired parent performance
 * will be fully instantiated first (automatically) by invoking the "mountChildPerformances" method in the corresponding dialog (controller). 
 * <p> Note that the dialog (controller) will always have this method, as by the definition of the performance class, the dialog must extend
 * AbstractDialog, which defines the method "mountChildPerformances".
 */
public class Performance<T extends AbstractDialog<C>, C extends AbstractComposer<T>> {
	
	/**
     * Unique identifier for a performance instance
     */	
	private String tag;
	
	/**
     * Dialog (controller) associated with the performance instance
     */	
	private T controller;
	
	/**
     * Root node of the JavaFX scene graph
     */	
	private Node root;
	
	/**
     * List of mounted child performances (i.e., within descendants of the parent performance)
     */	
	private ArrayList<Performance<?, ?>> children = new ArrayList<Performance<?, ?>>();
	
	/**
     * Directory of all performances
     */	
	private HashMap<String, Performance<?, ?>> performanceDirectory = new HashMap<String, Performance<?, ?>>();
	
	/**
     * Constructor for a performance instance.
     *
     * @param tag - unique string identifier for the performance instance
     * @param stage - the stage which hosts this performance instance and all its children
     * @param composer - the composer associated with the dialog (controller) of this performance
     * @param FXMLFile - the url of the fxml file for the dialog (controller) associated with this performance
     */	
	public Performance(String tag, Stage stage, C composer, String FXMLFile) throws IOException {
		
		this.tag = tag;
        FXMLLoader loader = new FXMLLoader(getClass().getResource("/" + FXMLFile));
    	root = loader.load();
    	T controller = loader.getController();
    	
    	controller.composer = composer;
    	composer.dialog = controller;
    	
    	controller.performance = this;
    	controller.stage = stage;
    	this.controller = controller;
    	
    	performanceDirectory.put(tag, this);
    	
    	controller.mountChildPerformances();
	}
	
	/**
     * Method invoked by a dialog (controller) instance within "mountChildPerformances" (a necessary method inherited from AbstractDialog)
     * which binds another, arbitrarily constructed performance instance to a pane within the calling dialog (controller) instance.
     * Note that each dialog (controller) instance has exactly one associated performance instance
     *
     * @param mountPane - JavaFX pane object to mount/bind performance to
     * @param childPerformance - the child performance instance to be bound
     */	
	
	public void mount(Pane mountPane, Performance<?, ?> childPerformance) {
		mountPane.getChildren().setAll(childPerformance.root);
		children.add(childPerformance);
		childPerformance.performanceDirectory = performanceDirectory;
		performanceDirectory.put(childPerformance.tag, childPerformance);
	}
	
	/**
     * Retrieves a desired performance instance corresponding to the provided tag
     * @param tag - unique string identifier for the performance instance
     * @return returns performance instance with associated tag
     */	
	@SuppressWarnings("unchecked")
	public <A extends AbstractDialog<B>, B extends AbstractComposer<A>> Performance<A, B> getPerformance(String tag) {
		return (Performance<A, B>)performanceDirectory.get(tag);
	}
	
	/**
     * Retrieves the unique dialog (controller) associated with this performance instance
     * @return returns unique dialog (controller) associated with this performance instance
     */	
	public T getController() { return controller; }
	
	/**
     * Renders the entire performance tree with all its mounted child performances in JavaFX and shows the stage.
     * 
     * Once a performance (or performance tree) has been constructed, this method can then simply be called to display the contents.
     */	
	public void render() {
		Scene scene = new Scene((Parent)root);
        scene.getStylesheets().add(getClass().getResource("/alignment-styles.css").toExternalForm());
        
		controller.stage.setScene(scene);
		controller.stage.show();
		renderChildren();
	}
	
	/**
     * Recursively iterates through all children of the performance instance, initializes their dialogs, and renders them. 
     */		
	private void renderChildren() {
		controller.initializeDialog();
		for(Performance<?, ?> child : children) {
			child.renderChildren();
		}
	}
	
}
