package Shared;

import Shared.interchange.Family;

public class FamilyItem {
    private final Family family;

    public FamilyItem(Family family) {
        this.family = family;
    }

    public Family getFamily() {
        return family;
    }

    @Override
    public String toString() {
        return family.getFamilyName();
    }
}
