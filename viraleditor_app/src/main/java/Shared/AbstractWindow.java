package Shared;

import java.io.File;
import java.io.IOException;

import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;
import javafx.stage.Window;

public class AbstractWindow {
	
	public static <T extends AbstractDialog<C>, C extends AbstractComposer<T>> Window newWindow(Stage stage, C composer, String FXMLFile) throws IOException {
    	
    	FXMLLoader loader = new FXMLLoader(new File("src/main/resources/" + FXMLFile).toURI().toURL());
    	stage.setScene(new Scene(loader.load()));
    	T controller = loader.getController();
    	
    	controller.composer = composer;
    	composer.dialog = controller;

    	controller.stage = stage;
    	stage.show();
    	controller.initializeDialog();
    	    	
    	return stage;
    }
}
