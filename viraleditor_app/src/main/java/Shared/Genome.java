package Shared;

/**
 * This class represents a genome.
 * 
 * @author Angelika Ehlers
 * @version 1.0
 */

public class Genome {
    private static final String NCBI_LINK_PREFIX = "http://www.ncbi.nlm.nih.gov/entrez/viewer.fcgi?val=";
    // genome table
    public int gfid;
    public int id;
    public String genome_abbr;
    public String molecule_type;
    public String strain_abbr;
    public String name;
    public String strain_name;
    public String segment_name;
    public String inserted_on;
    public String last_modified;
    public String gb_accession;
    public int gb_version;
    public int gb_gi;
    public int gb_taxid;
    public String gb_date;
    public String gb_record;
    public String gb_record_fixed;
    public String description;
    public String is_complete;
    public String is_published;
    public String notes;
    public String genome_seq;
    public int genome_seq_version;
    public int center_id;

    // on the fly calculated values
    public String genePrefix;
    public String ncbiLink;
    public String order;
    public int bpSize;
    public int a_cnt;
    public int c_cnt;
    public int g_cnt;
    public int t_cnt;

    public int taxnode_id;
    public long geneCount;

    public String isolate;
    public String strain;

    /**
     * Default constructor
     */
    public Genome() {
        genome_abbr = "";
        molecule_type = "dsDNA";
        strain_abbr = "";
        name = "";
        gb_accession = "";
        strain_name = "";
        segment_name = "single";
        inserted_on = "";
        last_modified = "";
        gb_date = "";
        gb_record = "";
        gb_record_fixed = "";
        description = "";
        is_complete = "Y";
        is_published = "Y";
        notes = "";
        genome_seq = "";
        genome_seq_version = 1;
        genePrefix = "";
        ncbiLink = "";
        order = "";
    }

    /**
     * Set genome family id
     * 
     * @param gid genome family id
     */
    public void setGenomeFamily(int gid) {
        gfid = gid;
    }

    /**
     * Set virus id
     * 
     * @param vid virus id
     */
    public void setID(int vid) {
        id = vid;
    }

    /**
     * Set genome/virus name
     * 
     * @param vname genome/virus name
     */
    public void setName(String vname) {
        name = vname;
    }

    /**
     * Set genome/virus abbreviation name
     * 
     * @param vgenome_abbr genome/virus abbreviation
     */
    public void setAbbrev(String vgenome_abbr) {
        genome_abbr = vgenome_abbr;
    }

    /**
     * Set genome/virus strain_abbr name
     * 
     * @param vstrain_abbr genome/virus strain_abbr name
     */
    public void setStrain(String vstrain_abbr) {
        strain_abbr = vstrain_abbr;
    }

    /**
     * Set genome/virus strain_name
     * 
     * @param vstrain genome/virus strain_name
     */
    public void setStrainName(String vstrain) {
        strain_name = vstrain;
    }

    /**
     * Set genome/virus molecule type
     * 
     * @param moltyp genome/virus molecule type
     */
    public void setMoleculeType(String moltyp) {
        molecule_type = moltyp;
    }

    /**
     * Set genome/virus inserted on date
     * 
     * @param idate genome/virus inserted on date
     */
    public void setInsertedOn(String idate) {
        inserted_on = idate;
    }

    /**
     * Set genome/virus modification date
     * 
     * @param mdate genome/virus modification date
     */
    public void setLastModified(String mdate) {
        last_modified = mdate;
    }

    /**
     * Set genome/virus genbank date
     * 
     * @param gbdate genome/virus genbank date
     */
    public void setGenbankDate(String gbdate) {
        gb_date = gbdate;
    }

    /**
     * Set genome/virus genbank record
     * 
     * @param gbr genome/virus genbank record
     */
    public void setGenbankRecord(String gbr) {
        gb_record = gbr;
    }

    /**
     * Set genome/virus genbank record (modified version)
     * 
     * @param gbr genome/virus genbank record
     */
    public void setGenbankRecordFixed(String gbr) {
        gb_record_fixed = gbr;
    }

    /**
     * Set genome/virus GenBank taxonomy number
     * 
     * @param taxid genome/virus GenBank taxonomy number
     */
    public void setTaxId(int taxid) {
        gb_taxid = taxid;
    }

    /**
     * Set genome/virus GenBank version number
     * 
     * @param version genome/virus GenBank version number
     */
    public void setVersion(int version) {
        gb_version = version;
    }

    /**
     * Set genome/virus GenBank GI number
     * 
     * @param gbGINo genome/virus GenBank GI number
     */
    public void setGI(int gbGINo) {
        gb_gi = gbGINo;
    }

    /**
     * Set genome/virus GenBank GI number
     * 
     * @param gbGINo genome/virus GenBank GI number
     */
    public void setGenBankGI(int gbGINo) {
        gb_gi = gbGINo;
    }

    /**
     * Set genome/virus GenBank accession number
     * 
     * @param gbAccNo genome/virus GenBank accession number
     */
    public void setAccession(String gbAccNo) {
        gb_accession = gbAccNo;
    }

    /**
     * Set genome/virus GenBank accession number
     * 
     * @param gbAccNo genome/virus GenBank accession number
     */
    public void setGenBankAccessionNo(String gbAccNo) {
        gb_accession = gbAccNo;
    }

    /**
     * Set virus size in nuber of basepairs
     * 
     * @param size virus size in no of basepairs
     */
    public void setSize(int size) {
        bpSize = size;
    }

    /**
     * Set virus size in nuber of basepairs
     */
    public void setSize() {
        if (genome_seq == null)
            bpSize = 0;
        else
            bpSize = genome_seq.length();
    }

    /**
     * Set virus dna sequence
     * 
     * @param dna virus dna sequence
     */
    public void setDNA(String dna) {
        genome_seq = dna;
        setSize();
        countNucl();
    }

    /**
     * Set the genome/virus dna sequence version number
     * 
     * @param sv the genome/virus dna sequence version number
     */
    public void setSequenceVersion(int sv) {
        genome_seq_version = sv;
    }

    /**
     * Set the genome/virus sequencing center id
     * 
     * @param cid the genome/virus sequencing center id
     */
    public void setCenterId(int cid) {
        center_id = cid;
    }

    /**
     * Set the segment name
     * 
     * @param sn the segment name
     */
    public void setSegmentName(String sn) {
        segment_name = sn;
    }

    /**
     * Set the genome/virus notes
     * 
     * @param n the notes
     */
    public void setNotes(String n) {
        notes = n;
    }

    /**
     * Set if genome is published
     * 
     * @param isp Y if true, N otherwise
     */
    public void setIsPublished(boolean isp) {
        if (isp)
            is_published = "Y";
        else
            is_published = "N";
    }

    /**
     * Set if genome is complete
     * 
     * @param isc Y if true, N otherwise
     */
    public void setIsComplete(boolean isc) {
        if (isc)
            is_complete = "Y";
        else
            is_complete = "N";
    }

    /**
     * Set the genome/virus description
     * 
     * @param descr the description
     */
    public void setDescription(String descr) {
        description = descr;
    }

    /**
     * Set the virus gene prefix
     * 
     * @param prefix the gene prefix
     */
    public void setPrefix(String prefix) {
        genePrefix = prefix;
    }

    /**
     * Set the virus gene prefix
     */
    public void setPrefix() {
        if (genome_abbr == null)
            genePrefix = "";
        else
            genePrefix = genome_abbr + "-";
    }

    /**
     * Set the ncbi link
     * 
     * @param link the ncbi link
     */
    public void setNCBILink(String link) {
        ncbiLink = link;
    }

    /**
     * Set the ncbi link
     */
    public void setNCBILink() {
        if (gb_accession == null)
            ncbiLink = "";
        else
            ncbiLink = NCBI_LINK_PREFIX + gb_accession;
    }

    /**
     * Set the order
     */
    public void setOrder(String newOrder) {
        order = newOrder;
    }

    /**
     * Get genome family id
     * 
     * @return genome family id
     */
    public int getGenomeFamily() {
        return gfid;
    }

    /**
     * Get genome/virus id
     * 
     * @return genome/virus id
     */
    public int getID() {
        return id;
    }

    /**
     * Get genome/virus name
     * 
     * @return genome/virus name
     */
    public String getName() {
        return name;
    }

    /**
     * Get genome/virus genome_abbriation name
     * 
     * @return genome/virus genome_abbriation
     */
    public String getAbbrev() {
        return genome_abbr;
    }

    /**
     * Get genome/virus strain_abbr name
     * 
     * @return genome/virus strain_abbr name
     */
    public String getStrain() {
        return strain_abbr;
    }

    /**
     * Get genome/virus strain_abbr name
     * 
     * @return genome/virus strain_abbr name
     */
    public String getStrainAbbrev() {
        return strain_abbr;
    }

    /**
     * Get genome/virus strain name
     * 
     * @return genome/virus strain name
     */
    public String getStrainName() {
        return strain_name;
    }

    /**
     * Get genome/virus GenBank GI number
     * 
     * @return genome/virus GenBank BI number
     */
    public int getGI() {
        return gb_gi;
    }

    /**
     * Get genome/virus GenBank GI number
     * 
     * @return genome/virus GenBank BI number
     */
    public int getGenBankGI() {
        return gb_gi;
    }

    /**
     * Get genome/virus GenBank accession number
     * 
     * @return genome/virus GenBank accession number
     */
    public String getAccession() {
        return gb_accession;
    }

    /**
     * Get genome/virus GenBank accession number
     * 
     * @return virus GenBank accession number
     */
    public String getGenBankAccessionNo() {
        return gb_accession;
    }

    /**
     * Get genome/virus notes
     * 
     * @return genome/virus notes
     */
    public String getNotes() {
        return notes;
    }

    /**
     * Get genome/virus description
     * 
     * @return genome/virus description
     */
    public String getDescription() {
        return description;
    }

    /**
     * Get genome/virus genbank file date
     * 
     * @return genome/virus genbank file date
     */
    public String getGBDate() {
        return gb_date;
    }

    /**
     * Get genome/virus genbank file date
     * 
     * @return genome/virus genbank file date
     */
    public String getDate() {
        return gb_date;
    }

    /**
     * Get genome/virus inserted on date
     * 
     * @return genome/virus inserted on date
     */
    public String getInsertedOn() {
        return inserted_on;
    }

    /**
     * Get genome/virus last modified date
     * 
     * @return genome/virus last modified date
     */
    public String getLastModified() {
        return last_modified;
    }

    /**
     * Get genome/virus segment name
     * 
     * @return genome/virus segment name
     */
    public String getSegmentName() {
        return segment_name;
    }

    /**
     * Get genome/virus molecule type
     * 
     * @return genome/virus molecule type
     */
    public String getMoleculeType() {
        return molecule_type;
    }

    /**
     * Get genome/virus genbank version
     * 
     * @return genome/virus genbank version
     */
    public int getVersion() {
        return gb_version;
    }

    /**
     * Get genome/virus genbank version
     * 
     * @return genome/virus genbank version
     */
    public int getGBVersion() {
        return gb_version;
    }

    /**
     * Get genome/virus genbank taxonomy id
     * 
     * @return genome/virus genbank taxonomy id
     */
    public int getTaxId() {
        return gb_taxid;
    }

    /**
     * Get if genome/virus is published
     * 
     * @return if genome/virus is published
     */
    public boolean isPublished() {
        return is_published.equalsIgnoreCase("Y");
    }

    /**
     * Get if genome/virus is published
     * 
     * @return if genome/virus is published
     */
    public String getIsPublished() {
        return is_published;
    }

    /**
     * Get if genome/virus is complete
     * 
     * @return if genome/virus is complete
     */
    public boolean isComplete() {
        return is_published.equalsIgnoreCase("Y");
    }

    /**
     * Get if genome/virus is complete
     * 
     * @return if genome/virus is complete
     */
    public String getIsComplete() {
        return is_complete;
    }

    /**
     * Get genome/virus size in nuber of basepairs
     * 
     * @return genome/virus size in no of basepairs
     */
    public int getSize() {
        return bpSize;
    }

    /**
     * Get genome/virus order name
     * 
     * @return genome/virus order name
     */
    public String getOrder() {
        return order;
    }

    /**
     * Get genome/virus dna sequence
     * 
     * @return genome/virus dna sequence
     */
    public String getSequence() {
        return genome_seq;
    }

    /**
     * Get genome/virus dna sequence version number
     * 
     * @return genome/virus dna sequence version number
     */
    public int getSequenceVersion() {
        return genome_seq_version;
    }

    /**
     * Get the genome/virus sequencing center id
     * 
     * @return the genome/virus sequencing center id
     */
    public int getCenterId() {
        return center_id;
    }

    /**
     * Get genome/virus dna sequence
     * 
     * @return genome/virus dna sequence
     */
    public String getDNA() {
        return genome_seq;
    }

    /**
     * Get the gene prefix
     * 
     * @return the gene prefix
     */
    public String getPrefix() {
        return genePrefix;
    }

    /**
     * Get the ncbi link
     * 
     * @return the ncbi link
     */
    public String getNCBILink() {
        return ncbiLink;
    }

    /**
     * Get the number of occurrences of the 'a' nucleotide
     * 
     * @return the number of occurrences of the 'a' nucleotide
     */
    public int getAcnt() {
        return a_cnt;
    }

    /**
     * Get the percent of occurrences of the 'a' nucleotide
     * 
     * @return the percent of occurrences of the 'a' nucleotide
     */
    public double getApercent() {
        return (double) a_cnt / bpSize;
    }

    /**
     * Get the number of occurrences of the 'c' nucleotide
     * 
     * @return the number of occurrences of the 'c' nucleotide
     */
    public int getCcnt() {
        return c_cnt;
    }

    /**
     * Get the percent of occurrences of the 'c' nucleotide
     * 
     * @return the percent of occurrences of the 'c' nucleotide
     */
    public double getCpercent() {
        return (double) c_cnt / bpSize;
    }

    /**
     * Get the number of occurrences of the 'g' nucleotide
     * 
     * @return the number of occurrences of the 'g' nucleotide
     */
    public int getGcnt() {
        return g_cnt;
    }

    /**
     * Get the percent of occurrences of the 'g' nucleotide
     * 
     * @return the percent of occurrences of the 'g' nucleotide
     */
    public double getGpercent() {
        return (double) g_cnt / bpSize;
    }

    /**
     * Get the number of occurrences of the 't' nucleotide
     * 
     * @return the number of occurrences of the 't' nucleotide
     */
    public int getTcnt() {
        return t_cnt;
    }

    /**
     * Get the percent of occurrences of the 't' nucleotide
     * 
     * @return the percent of occurrences of the 't' nucleotide
     */
    public double getTpercent() {
        return (double) t_cnt / bpSize;
    }

    /**
     * Get the number of A and T nucleotides
     * 
     * @return sum of A's and T's
     */
    public int getATcnt() {
        return a_cnt + t_cnt;
    }

    /**
     * Get the percentage of A and T nucleotides
     * 
     * @return percentage of A's and T's
     */
    public double getATpercent() {
        return (double) getATcnt() / bpSize;
    }

    /**
     * Get the number of G and C nucleotides
     * 
     * @return sum of G's and C's
     */
    public int getGCcnt() {
        return c_cnt + g_cnt;
    }

    /**
     * Get the percentage of G and C nucleotides
     * 
     * @return percentage of G's and C's
     */
    public double getGCpercent() {
        return (double) getGCcnt() / bpSize;
    }

    /**
     * Count the occurrences of the nucleotides in the dna sequence of the virus
     */
    public void countNucl() {
        char c;
        int len = genome_seq.length();

        a_cnt = 0;
        c_cnt = 0;
        g_cnt = 0;
        t_cnt = 0;
        for (int i = 0; i < len; i++) {
            c = genome_seq.charAt(i);
            if ((c == 'a') || (c == 'A'))
                a_cnt++;
            if ((c == 'c') || (c == 'C'))
                c_cnt++;
            if ((c == 'g') || (c == 'G'))
                g_cnt++;
            if ((c == 't') || (c == 'T'))
                t_cnt++;
        }
    }

    /**
     * Compare this genome with another genome
     * 
     * @param g the genome to compare with
     * @return if genome is diiferent (true/false)
     */
    public boolean isEqual(Genome g) {
        if (g.id != id)
            return false;
        if (!g.genome_abbr.equals(genome_abbr))
            return false;
        if (!g.molecule_type.equals(molecule_type))
            return false;
        if (!g.strain_abbr.equals(strain_abbr))
            return false;
        if (!g.name.equals(name))
            return false;
        if (!g.strain_name.equals(strain_name))
            return false;
        if (!g.segment_name.equals(segment_name))
            return false;
        if (g.gb_accession == null) {
            if (gb_accession != null)
                return false;
        } else {
            if (gb_accession == null)
                return false;
            else if (!g.gb_accession.equals(gb_accession))
                return false;
        }
        if (g.gb_version != gb_version)
            return false;
        if (g.taxnode_id != taxnode_id)
            return false;
        if (g.gb_gi != gb_gi)
            return false;
        if (g.gb_taxid != gb_taxid)
            return false;
        if (g.gb_date == null) {
            if (gb_date != null)
                return false;
        } else {
            if (gb_date == null)
                return false;
            else if (!g.gb_date.equals(gb_date))
                return false;
        }
        if (g.description == null) {
            if (description != null)
                return false;
        } else {
            if (description == null)
                return false;
            else if (!g.description.equals(description))
                return false;
        }
        if (!g.is_complete.equals(is_complete))
            return false;
        if (!g.is_published.equals(is_published))
            return false;
        if (g.notes == null) {
            if (notes != null)
                return false;
        } else {
            if (notes == null)
                return false;
            else if (!g.notes.equals(notes))
                return false;
        }
        if (!g.genome_seq.equals(genome_seq))
            return false;
        if (g.genome_seq_version != genome_seq_version)
            return false;
        if (g.center_id != center_id)
            return false;
        return true;
    }

    /**
     * Compare this virus with another virus and report differences If return string
     * has lenght of zero (empty string) thne no differences have been found.
     * Otherwise the string lists the properties which are different separated by a
     * space.
     * 
     * @param v the genome to compare to
     * @return the report of differences found
     */
    public String diff(Genome v) {
        String str = "";
        if (id != v.id)
            str += " id";
        if (!name.equals(v.name))
            str += " name";
        if (!genome_abbr.equals(v.genome_abbr))
            str += " genome_abbr";
        if (!strain_abbr.equals(v.strain_abbr))
            str += " strain_abbr";
        if (gb_gi != v.gb_gi)
            str += " gi";
        if (!gb_accession.equals(v.gb_accession))
            str += " accession";
        if (bpSize != v.bpSize)
            str += " size";
        if (!genome_seq.equals(v.genome_seq))
            str += " dna";
        if (!genePrefix.equals(v.genePrefix))
            str += " geneprefix";
        return str;
    }

    /**
     * Print the virus data
     */
    public void print() {
        System.out.println("ID: " + id);
        System.out.println("Name: " + name);
        System.out.println("Abbreviation: " + genome_abbr);
        System.out.println("Strain: " + strain_abbr);
        System.out.println("GenBank GI number: " + gb_gi);
        System.out.println("GenBank accession number: " + gb_accession);
        System.out.println("Size: " + bpSize);
        System.out.println("DNA Sequence: " + genome_seq.substring(0, 50) + " ...");
        System.out.println("Gene prefix: " + genePrefix);
        System.out.println("#a: " + a_cnt);
        System.out.println("#c: " + c_cnt);
        System.out.println("#g: " + g_cnt);
        System.out.println("#t: " + t_cnt);
    }

    /**
     * Convert to a textual representation. Eventually, let's complete this and make
     * print() use it. (mjw 20090204)
     */
    @Override
    public String toString() {
        String r = "";
        r += "ID: " + id;
        r += "\nName: " + name;
        r += "\nAbbreviation: " + genome_abbr;
        r += "\nStrain name: " + strain_name;
        r += "\nStrain abbr: " + strain_abbr;
        r += "\nGI: " + gb_gi;
        r += "\nAccession: " + gb_accession;
        r += "\nVersion: " + gb_version;
        r += "\nTaxid: " + gb_taxid;
        r += "\nSize: " + bpSize;
        r += "\nGene prefix: " + genePrefix;
        r += "\nCount(acgt): " + (a_cnt + c_cnt + g_cnt + t_cnt);
        r += "\n";
        return r;
    }

    public void setTaxNodeId(int intValue) {
        taxnode_id = intValue;
    }

    public Integer getTaxNodeId() {
        return taxnode_id;
    }

}
