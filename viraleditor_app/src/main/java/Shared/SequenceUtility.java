package Shared;

import java.util.LinkedList;
import java.util.List;

public class SequenceUtility {

    /**
     * Make the simple complement of a piece of DNA (ie do not reverse string)
     * 
     * @param minusStr String to make complement of
     * @return the complement String
     */
    public static String make_simple_complement(String minusStr) {
        StringBuffer temp;
        int len;

        len = minusStr.length();
        temp = new StringBuffer(len);
        for (int i = 0; i < len; i++) {
            switch (minusStr.charAt(i)) {
            case 'a':
            case 'A':
                temp.append('T');
                break;
            case 'c':
            case 'C':
                temp.append('G');
                break;
            case 'g':
            case 'G':
                temp.append('C');
                break;
            case 't':
            case 'T':
            case 'u':
            case 'U':
                temp.append('A');
                break;
            case 'm':
            case 'M':
                temp.append('K');
                break;
            case 'r':
            case 'R':
                temp.append('Y');
                break;
            case 'w':
            case 'W':
                temp.append('W');
                break;
            case 's':
            case 'S':
                temp.append('S');
                break;
            case 'y':
            case 'Y':
                temp.append('R');
                break;
            case 'k':
            case 'K':
                temp.append('M');
                break;
            case 'v':
            case 'V':
                temp.append('B');
                break;
            case 'h':
            case 'H':
                temp.append('D');
                break;
            case 'd':
            case 'D':
                temp.append('H');
                break;
            case 'b':
            case 'B':
                temp.append('V');
                break;
            case 'x':
            case 'X':
                temp.append('X');
                break;
            case 'n':
            case 'N':
                temp.append('N');
                break;
            default:
                temp.append(Character.toUpperCase(minusStr.charAt(i)));
                break;
            }
        }
        return temp.toString();
    }

    /**
     * Make the complement of a piece of DNA
     * 
     * @param plusStr String to make complement of
     * @return the complement String
     */
    public static String make_complement(String plusStr) {
        StringBuffer temp;
        int len;

        len = plusStr.length();
        temp = new StringBuffer(len);
        len--;
        for (int i = len; i >= 0; i--) {
            switch (plusStr.charAt(i)) {
            case 'a':
            case 'A':
                temp.append('T');
                break;
            case 'c':
            case 'C':
                temp.append('G');
                break;
            case 'g':
            case 'G':
                temp.append('C');
                break;
            case 't':
            case 'T':
            case 'u':
            case 'U':
                temp.append('A');
                break;
            case 'm':
            case 'M':
                temp.append('K');
                break;
            case 'r':
            case 'R':
                temp.append('Y');
                break;
            case 'w':
            case 'W':
                temp.append('W');
                break;
            case 's':
            case 'S':
                temp.append('S');
                break;
            case 'y':
            case 'Y':
                temp.append('R');
                break;
            case 'k':
            case 'K':
                temp.append('M');
                break;
            case 'v':
            case 'V':
                temp.append('B');
                break;
            case 'h':
            case 'H':
                temp.append('D');
                break;
            case 'd':
            case 'D':
                temp.append('H');
                break;
            case 'b':
            case 'B':
                temp.append('V');
                break;
            case 'x':
            case 'X':
                temp.append('X');
                break;
            case 'n':
            case 'N':
                temp.append('N');
                break;
            default:
                temp.append(Character.toUpperCase(plusStr.charAt(i)));
                break;
            }
        }
        return temp.toString();
    }

    /**
     * Define the dna sequence of the gene
     * 
     * @param vir_dna the DNA sequence of the genome
     * @param strand  the strand the gene is on
     * @param orfs    the genes start/stop positions
     * @return dna_seq the DNA sequence of the gene
     */
    public static String setDNASeq(String vir_dna, String strand, List<Orf> orfs) {
        Orf o;
        String temp;
        boolean plus;
        StringBuilder dna_seq;
        int start, stop;
        List<Integer> positions = new LinkedList<>();
        
        if (strand.equals("-") | strand.equals("bottom"))
        	plus = false;
        else
        	plus = true;
        
        //plus = !strand.equals("-") || !strand.equals("bottom") || !strand.equals("bottom");
        dna_seq = new StringBuilder();
        
        if (orfs != null) {
            for (Orf orf : orfs) {
                o = orf;
                positions.add(o.getPosition());
            }
            for (int i = 1; i < positions.size() + 1; i++) {
                int j = positions.indexOf(i);
                if ((j >= 0) && (j < orfs.size())) {
                    o = orfs.get(j);
                    start = o.getStart();
                    stop = o.getStop();
                    if (start <= stop) {
                        // case start <= stop
                        if (stop > vir_dna.length())
                            stop = vir_dna.length();
                        temp = vir_dna.substring(start - 1, stop);
                        dna_seq.append(temp);
                    } else {
                        // case start > stop
                        if (start <= vir_dna.length()) {
                            temp = vir_dna.substring(start - 1, vir_dna.length());
                            dna_seq.append(temp);
                        }
                        if (stop <= vir_dna.length()) {
                            temp = vir_dna.substring(0, stop);
                            dna_seq.append(temp);
                        }
                    }
                }
            }
        }
        if (!plus)
            dna_seq = new StringBuilder(make_complement(dna_seq.toString())); // take reverse complement
        else
            dna_seq = new StringBuilder(dna_seq.toString().toUpperCase());
        return dna_seq.toString();
    }

    /**
     * Set the protein sequence of this gene based on the genes DNA seq
     *
     * Nucleotides Description Abbreviation
     * ----------------------------------------------- Adenosine A Thymidine T
     * Cytosine C Guanosine G Uridine U Any nucleotide (A, T, C or G) N G or A R A
     * or T W C or T Y A or C M G or T K G or C S Not G (A or C or T) H Not A (C or
     * G or T) B Not T (A or C or G) V Not C (A or G or T) D
     *
     * Amino Acids Description Abbreviation
     * ----------------------------------------------- Alanine (Ala) A Arginine
     * (Arg) R Asparagine (Asn) N Aspartic acid (Asp) D Cysteine (Cys) C Glutamic
     * acid (Glu) E Glutamine (Gln) Q Glycine (Gly) G Histidine (His) H Isoleucine
     * (Ile) I Leucine (Leu) L Lysine (Lys) K Methionine (Met) M Phenylalanine (Phe)
     * F Proline (Pro) P Serine (Ser) S Threonine (Thr) T Tryptophan (Trp) W
     * Tyrosine (Tyr) Y Valine (Val) V
     *
     * D or N B E or Q Z L or I J U (TGA) U any X
     *
     * @param nuclSequence the nucleotide sequence
     * @return the protein sequence
     */
    public static String setProteinSeq(String nuclSequence) {
        String str;
        StringBuilder protein_seq = new StringBuilder();
        int size;

        if (nuclSequence == null)
            size = 0;
        else
            size = nuclSequence.length();
        for (int i = 0; i < size; i += 3) {
            if ((i + 3) <= size) {
                str = nuclSequence.substring(i, i + 3);
                str = str.toUpperCase();

                if (str.equals("AAA")) {
                    protein_seq.append("K");
                    continue;
                }
                if (str.equals("AAG")) {
                    protein_seq.append("K");
                    continue;
                }
                if (str.equals("AAR")) {
                    protein_seq.append("K");
                    continue;
                }

                if (str.equals("AAT")) {
                    protein_seq.append("N");
                    continue;
                }
                if (str.equals("AAC")) {
                    protein_seq.append("N");
                    continue;
                }
                if (str.equals("AAY")) {
                    protein_seq.append("N");
                    continue;
                }

                if (str.equals("ACA")) {
                    protein_seq.append("T");
                    continue;
                }
                if (str.equals("ACC")) {
                    protein_seq.append("T");
                    continue;
                }
                if (str.equals("ACG")) {
                    protein_seq.append("T");
                    continue;
                }
                if (str.equals("ACT")) {
                    protein_seq.append("T");
                    continue;
                }
                if (str.equals("ACN")) {
                    protein_seq.append("T");
                    continue;
                }
                if (str.equals("ACR")) {
                    protein_seq.append("T");
                    continue;
                }
                if (str.equals("ACW")) {
                    protein_seq.append("T");
                    continue;
                }
                if (str.equals("ACY")) {
                    protein_seq.append("T");
                    continue;
                }
                if (str.equals("ACM")) {
                    protein_seq.append("T");
                    continue;
                }
                if (str.equals("ACK")) {
                    protein_seq.append("T");
                    continue;
                }
                if (str.equals("ACS")) {
                    protein_seq.append("T");
                    continue;
                }
                if (str.equals("ACH")) {
                    protein_seq.append("T");
                    continue;
                }
                if (str.equals("ACB")) {
                    protein_seq.append("T");
                    continue;
                }
                if (str.equals("ACV")) {
                    protein_seq.append("T");
                    continue;
                }
                if (str.equals("ACD")) {
                    protein_seq.append("T");
                    continue;
                }

                if (str.equals("AGA")) {
                    protein_seq.append("R");
                    continue;
                }
                if (str.equals("AGG")) {
                    protein_seq.append("R");
                    continue;
                }
                if (str.equals("AGR")) {
                    protein_seq.append("R");
                    continue;
                }

                if (str.equals("AGC")) {
                    protein_seq.append("S");
                    continue;
                }
                if (str.equals("AGT")) {
                    protein_seq.append("S");
                    continue;
                }
                if (str.equals("AGY")) {
                    protein_seq.append("S");
                    continue;
                }

                if (str.equals("ATG")) {
                    protein_seq.append("M");
                    continue;
                }

                if (str.equals("CAA")) {
                    protein_seq.append("Q");
                    continue;
                }
                if (str.equals("CAG")) {
                    protein_seq.append("Q");
                    continue;
                }
                if (str.equals("CAR")) {
                    protein_seq.append("Q");
                    continue;
                }

                if (str.equals("CAC")) {
                    protein_seq.append("H");
                    continue;
                }
                if (str.equals("CAT")) {
                    protein_seq.append("H");
                    continue;
                }
                if (str.equals("CAY")) {
                    protein_seq.append("H");
                    continue;
                }

                if (str.equals("CCA")) {
                    protein_seq.append("P");
                    continue;
                }
                if (str.equals("CCC")) {
                    protein_seq.append("P");
                    continue;
                }
                if (str.equals("CCG")) {
                    protein_seq.append("P");
                    continue;
                }
                if (str.equals("CCT")) {
                    protein_seq.append("P");
                    continue;
                }
                if (str.equals("CCN")) {
                    protein_seq.append("P");
                    continue;
                }
                if (str.equals("CCR")) {
                    protein_seq.append("P");
                    continue;
                }
                if (str.equals("CCW")) {
                    protein_seq.append("P");
                    continue;
                }
                if (str.equals("CCY")) {
                    protein_seq.append("P");
                    continue;
                }
                if (str.equals("CCM")) {
                    protein_seq.append("P");
                    continue;
                }
                if (str.equals("CCK")) {
                    protein_seq.append("P");
                    continue;
                }
                if (str.equals("CCS")) {
                    protein_seq.append("P");
                    continue;
                }
                if (str.equals("CCH")) {
                    protein_seq.append("P");
                    continue;
                }
                if (str.equals("CCB")) {
                    protein_seq.append("P");
                    continue;
                }
                if (str.equals("CCV")) {
                    protein_seq.append("P");
                    continue;
                }
                if (str.equals("CCD")) {
                    protein_seq.append("P");
                    continue;
                }

                if (str.equals("CGA")) {
                    protein_seq.append("R");
                    continue;
                }
                if (str.equals("CGC")) {
                    protein_seq.append("R");
                    continue;
                }
                if (str.equals("CGG")) {
                    protein_seq.append("R");
                    continue;
                }
                if (str.equals("CGT")) {
                    protein_seq.append("R");
                    continue;
                }
                if (str.equals("CGN")) {
                    protein_seq.append("R");
                    continue;
                }
                if (str.equals("CGR")) {
                    protein_seq.append("R");
                    continue;
                }
                if (str.equals("CGW")) {
                    protein_seq.append("R");
                    continue;
                }
                if (str.equals("CGY")) {
                    protein_seq.append("R");
                    continue;
                }
                if (str.equals("CGM")) {
                    protein_seq.append("R");
                    continue;
                }
                if (str.equals("CGK")) {
                    protein_seq.append("R");
                    continue;
                }
                if (str.equals("CGS")) {
                    protein_seq.append("R");
                    continue;
                }
                if (str.equals("CGH")) {
                    protein_seq.append("R");
                    continue;
                }
                if (str.equals("CGB")) {
                    protein_seq.append("R");
                    continue;
                }
                if (str.equals("CGV")) {
                    protein_seq.append("R");
                    continue;
                }
                if (str.equals("CGD")) {
                    protein_seq.append("R");
                    continue;
                }
                if (str.equals("MGA")) {
                    protein_seq.append("R");
                    continue;
                }
                if (str.equals("MGG")) {
                    protein_seq.append("R");
                    continue;
                }

                if (str.equals("ATA")) {
                    protein_seq.append("I");
                    continue;
                }
                if (str.equals("ATC")) {
                    protein_seq.append("I");
                    continue;
                }
                if (str.equals("ATT")) {
                    protein_seq.append("I");
                    continue;
                }
                if (str.equals("ATM")) {
                    protein_seq.append("I");
                    continue;
                }
                if (str.equals("ATW")) {
                    protein_seq.append("I");
                    continue;
                }
                if (str.equals("ATY")) {
                    protein_seq.append("I");
                    continue;
                }
                if (str.equals("ATH")) {
                    protein_seq.append("I");
                    continue;
                }

                if (str.equals("CTA")) {
                    protein_seq.append("L");
                    continue;
                }
                if (str.equals("CTC")) {
                    protein_seq.append("L");
                    continue;
                }
                if (str.equals("CTG")) {
                    protein_seq.append("L");
                    continue;
                }
                if (str.equals("CTT")) {
                    protein_seq.append("L");
                    continue;
                }
                if (str.equals("CTN")) {
                    protein_seq.append("L");
                    continue;
                }
                if (str.equals("CTR")) {
                    protein_seq.append("L");
                    continue;
                }
                if (str.equals("CTW")) {
                    protein_seq.append("L");
                    continue;
                }
                if (str.equals("CTY")) {
                    protein_seq.append("L");
                    continue;
                }
                if (str.equals("CTM")) {
                    protein_seq.append("L");
                    continue;
                }
                if (str.equals("CTK")) {
                    protein_seq.append("L");
                    continue;
                }
                if (str.equals("CTS")) {
                    protein_seq.append("L");
                    continue;
                }
                if (str.equals("CTH")) {
                    protein_seq.append("L");
                    continue;
                }
                if (str.equals("CTB")) {
                    protein_seq.append("L");
                    continue;
                }
                if (str.equals("CTV")) {
                    protein_seq.append("L");
                    continue;
                }
                if (str.equals("CTD")) {
                    protein_seq.append("L");
                    continue;
                }

                if (str.equals("YTG")) {
                    protein_seq.append("L");
                    continue;
                }
                if (str.equals("YTA")) {
                    protein_seq.append("L");
                    continue;
                }

                if (str.equals("WTA")) {
                    protein_seq.append("J");
                    continue;
                }
                if (str.equals("HTA")) {
                    protein_seq.append("J");
                    continue;
                }
                if (str.equals("YTA")) {
                    protein_seq.append("J");
                    continue;
                }
                if (str.equals("YTG")) {
                    protein_seq.append("J");
                    continue;
                }
                if (str.equals("YTR")) {
                    protein_seq.append("J");
                    continue;
                }

                if (str.equals("GAA")) {
                    protein_seq.append("E");
                    continue;
                }
                if (str.equals("GAG")) {
                    protein_seq.append("E");
                    continue;
                }
                if (str.equals("GAR")) {
                    protein_seq.append("E");
                    continue;
                }

                if (str.equals("GAT")) {
                    protein_seq.append("D");
                    continue;
                }
                if (str.equals("GAC")) {
                    protein_seq.append("D");
                    continue;
                }
                if (str.equals("GAY")) {
                    protein_seq.append("D");
                    continue;
                }

                if (str.equals("GCA")) {
                    protein_seq.append("A");
                    continue;
                }
                if (str.equals("GCC")) {
                    protein_seq.append("A");
                    continue;
                }
                if (str.equals("GCG")) {
                    protein_seq.append("A");
                    continue;
                }
                if (str.equals("GCT")) {
                    protein_seq.append("A");
                    continue;
                }
                if (str.equals("GCN")) {
                    protein_seq.append("A");
                    continue;
                }
                if (str.equals("GCR")) {
                    protein_seq.append("A");
                    continue;
                }
                if (str.equals("GCW")) {
                    protein_seq.append("A");
                    continue;
                }
                if (str.equals("GCY")) {
                    protein_seq.append("A");
                    continue;
                }
                if (str.equals("GCM")) {
                    protein_seq.append("A");
                    continue;
                }
                if (str.equals("GCK")) {
                    protein_seq.append("A");
                    continue;
                }
                if (str.equals("GCS")) {
                    protein_seq.append("A");
                    continue;
                }
                if (str.equals("GCH")) {
                    protein_seq.append("A");
                    continue;
                }
                if (str.equals("GCB")) {
                    protein_seq.append("A");
                    continue;
                }
                if (str.equals("GCV")) {
                    protein_seq.append("A");
                    continue;
                }
                if (str.equals("GCD")) {
                    protein_seq.append("A");
                    continue;
                }

                if (str.equals("GGA")) {
                    protein_seq.append("G");
                    continue;
                }
                if (str.equals("GGC")) {
                    protein_seq.append("G");
                    continue;
                }
                if (str.equals("GGG")) {
                    protein_seq.append("G");
                    continue;
                }
                if (str.equals("GGT")) {
                    protein_seq.append("G");
                    continue;
                }
                if (str.equals("GGN")) {
                    protein_seq.append("G");
                    continue;
                }
                if (str.equals("GGR")) {
                    protein_seq.append("G");
                    continue;
                }
                if (str.equals("GGW")) {
                    protein_seq.append("G");
                    continue;
                }
                if (str.equals("GGY")) {
                    protein_seq.append("G");
                    continue;
                }
                if (str.equals("GGM")) {
                    protein_seq.append("G");
                    continue;
                }
                if (str.equals("GGK")) {
                    protein_seq.append("G");
                    continue;
                }
                if (str.equals("GGS")) {
                    protein_seq.append("G");
                    continue;
                }
                if (str.equals("GGH")) {
                    protein_seq.append("G");
                    continue;
                }
                if (str.equals("GGB")) {
                    protein_seq.append("G");
                    continue;
                }
                if (str.equals("GGV")) {
                    protein_seq.append("G");
                    continue;
                }
                if (str.equals("GGD")) {
                    protein_seq.append("G");
                    continue;
                }

                if (str.equals("GTA")) {
                    protein_seq.append("V");
                    continue;
                }
                if (str.equals("GTC")) {
                    protein_seq.append("V");
                    continue;
                }
                if (str.equals("GTG")) {
                    protein_seq.append("V");
                    continue;
                }
                if (str.equals("GTT")) {
                    protein_seq.append("V");
                    continue;
                }
                if (str.equals("GTN")) {
                    protein_seq.append("V");
                    continue;
                }
                if (str.equals("GTR")) {
                    protein_seq.append("V");
                    continue;
                }
                if (str.equals("GTW")) {
                    protein_seq.append("V");
                    continue;
                }
                if (str.equals("GTY")) {
                    protein_seq.append("V");
                    continue;
                }
                if (str.equals("GTM")) {
                    protein_seq.append("V");
                    continue;
                }
                if (str.equals("GTK")) {
                    protein_seq.append("V");
                    continue;
                }
                if (str.equals("GTS")) {
                    protein_seq.append("V");
                    continue;
                }
                if (str.equals("GTH")) {
                    protein_seq.append("V");
                    continue;
                }
                if (str.equals("GTB")) {
                    protein_seq.append("V");
                    continue;
                }
                if (str.equals("GTV")) {
                    protein_seq.append("V");
                    continue;
                }
                if (str.equals("GTD")) {
                    protein_seq.append("V");
                    continue;
                }

                if (str.equals("TAA")) {
                    protein_seq.append("*");
                    continue;
                } // used to be X
                if (str.equals("TAG")) {
                    protein_seq.append("*");
                    continue;
                } // used to be X
                if (str.equals("TAR")) {
                    protein_seq.append("*");
                    continue;
                } // used to be X

                if (str.equals("TAC")) {
                    protein_seq.append("Y");
                    continue;
                }
                if (str.equals("TAT")) {
                    protein_seq.append("Y");
                    continue;
                }
                if (str.equals("TAY")) {
                    protein_seq.append("Y");
                    continue;
                }

                if (str.equals("TCA")) {
                    protein_seq.append("S");
                    continue;
                }
                if (str.equals("TCC")) {
                    protein_seq.append("S");
                    continue;
                }
                if (str.equals("TCG")) {
                    protein_seq.append("S");
                    continue;
                }
                if (str.equals("TCT")) {
                    protein_seq.append("S");
                    continue;
                }
                if (str.equals("TCN")) {
                    protein_seq.append("S");
                    continue;
                }
                if (str.equals("TCR")) {
                    protein_seq.append("S");
                    continue;
                }
                if (str.equals("TCW")) {
                    protein_seq.append("S");
                    continue;
                }
                if (str.equals("TCY")) {
                    protein_seq.append("S");
                    continue;
                }
                if (str.equals("TCM")) {
                    protein_seq.append("S");
                    continue;
                }
                if (str.equals("TCK")) {
                    protein_seq.append("S");
                    continue;
                }
                if (str.equals("TCS")) {
                    protein_seq.append("S");
                    continue;
                }
                if (str.equals("TCH")) {
                    protein_seq.append("S");
                    continue;
                }
                if (str.equals("TCB")) {
                    protein_seq.append("S");
                    continue;
                }
                if (str.equals("TCV")) {
                    protein_seq.append("S");
                    continue;
                }
                if (str.equals("TCD")) {
                    protein_seq.append("S");
                    continue;
                }

                if (str.equals("TGA")) {
                    protein_seq.append("*");
                    continue;
                } // used to be U

                if (str.equals("TGC")) {
                    protein_seq.append("C");
                    continue;
                }
                if (str.equals("TGT")) {
                    protein_seq.append("C");
                    continue;
                }
                if (str.equals("TGY")) {
                    protein_seq.append("C");
                    continue;
                }

                if (str.equals("TGG")) {
                    protein_seq.append("W");
                    continue;
                }

                if (str.equals("TTA")) {
                    protein_seq.append("L");
                    continue;
                }
                if (str.equals("TTG")) {
                    protein_seq.append("L");
                    continue;
                }
                if (str.equals("TTR")) {
                    protein_seq.append("L");
                    continue;
                }

                if (str.equals("TTC")) {
                    protein_seq.append("F");
                    continue;
                }
                if (str.equals("TTT")) {
                    protein_seq.append("F");
                    continue;
                }
                if (str.equals("TTY")) {
                    protein_seq.append("F");
                    continue;
                }

                if (str.equals("RAC")) {
                    protein_seq.append("B");
                    continue;
                }
                if (str.equals("RAT")) {
                    protein_seq.append("B");
                    continue;
                }
                if (str.equals("RAY")) {
                    protein_seq.append("B");
                    continue;
                }

                if (str.equals("SAA")) {
                    protein_seq.append("Z");
                    continue;
                }
                if (str.equals("SAG")) {
                    protein_seq.append("Z");
                    continue;
                }
                if (str.equals("SAR")) {
                    protein_seq.append("Z");
                    continue;
                }

                protein_seq.append("X");
            }
        }
        return protein_seq.toString();
    }

    /**
     * Set the upstream sequence of the gene to the 100 base pairs upstream of the
     * AGT sequence in a protein
     * 
     * @param vir_dna  the DNA sequence of the virus
     * @param ulen     the length of the upstream sequence
     * @param strand   the strand the gene is on
     * @param orfs     the genes start/stop positions
     * @param circular the genome characteristics
     * @return the upstream seq of the gene
     */
    public static String makeUpstreamSeq(String vir_dna, int ulen, String strand, List<Orf> orfs,
            boolean circular) {
        int[] ups_interval;
        int begin, end, cbegin, cend;
        String temp;
        String upstream_seq;
        ups_interval = calculateUpstreamInterval(vir_dna.length(), ulen, strand, orfs, circular);
        begin = ups_interval[0];
        end = ups_interval[1];
        cbegin = ups_interval[2];
        cend = ups_interval[3];
        if (begin < end) {
            upstream_seq = vir_dna.substring(begin - 1, end);
            if (cbegin > -1) {
                upstream_seq = vir_dna.substring(cbegin - 1, cend) + upstream_seq;
            }
        } else {
            temp = vir_dna.substring(end - 1, begin);
            upstream_seq = make_complement(temp);
            if (cbegin > -1) {
                temp = vir_dna.substring(cend - 1, cbegin);
                upstream_seq = upstream_seq + make_complement(temp);
            }
        }
        upstream_seq = upstream_seq.toUpperCase();
        return upstream_seq;
    }

    /**
     * calcultate the start/stops for the upstream sequence
     * 
     * @param genomeSize the length of the genome (#bp)
     * @param ulen       the length of the upstream sequence
     * @param strand     the strand the gene is on
     * @param orfs       the genes start/stop positions
     * @param circular   the genome characteristics
     * @return the start/stops for the upstream sequence
     */
    private static int[] calculateUpstreamInterval(int genomeSize, int ulen, String strand, List<Orf> orfs,
            boolean circular) {
        int[] interval;
        int begin, end, cbegin, cend;
        int start, stop;
        Orf o;
        boolean plus;
        List<Integer> positions = new LinkedList<>();
        interval = new int[4];
        for (int i = 0; i < 4; i++)
            interval[i] = -1;
        cbegin = -1;
        cend = -1;
        plus = !strand.equals("-");
        if (strand.equals("bottom"))
            plus = false;
        if (strand.equals("minus"))
            plus = false;
        if (orfs != null) {
            for (Orf orf : orfs) {
                o = orf;
                positions.add(o.getPosition());
            }
            int j = positions.indexOf(1);
            o = orfs.get(j);
            start = o.getStart();
            stop = o.getStop();
            if (start > stop) {
                start = o.getStop();
                stop = o.getStart();
            }
            if (!plus) {
                begin = stop + ulen;
                end = stop - 9;
                if (circular) {
                    if (begin > genomeSize) {
                        cend = 1;
                        cbegin = begin - genomeSize;
                        begin = genomeSize;
                    }
                }
            } else {
                begin = start - ulen;
                end = start + 9;
                if (circular) {
                    if (begin < 1) {
                        cend = genomeSize;
                        cbegin = genomeSize + begin;
                        begin = 1;
                    }
                }
            }
            if (begin < 1)
                begin = 1;
            if (end < 1)
                end = 1;
            if (end > genomeSize)
                end = genomeSize;
            if (begin > genomeSize)
                begin = genomeSize;
            interval[0] = begin;
            interval[1] = end;
            interval[2] = cbegin;
            interval[3] = cend;
        }
        return interval;
    }

    /**
     * isDNA detects whether a sequence is DNA or amino acids, returning true if it
     * is DNA.
     * 
     * @param s a string to test
     * @return true if the string appears to be DNA; false otherwise.
     */
    public static boolean isDNA(String s) {
        // Note: we only require 90% ACGT, to allow for a few
        // N's or other ambiguity codes
        String nucleotides = "aAcCgGtT";
        double threshold = 0.9;
        int count = 0;
        int slen = s.length();
        for (int i = 0; i < slen; i++) {
            if (nucleotides.indexOf(s.codePointAt(i)) != -1) {
                count += 1;
            }
        }
        return (double) count / (double) slen > threshold;
    }
}
