package Shared;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

public class IntervalTree<E> {
    private Node root;

    /**
     * Constructs an empty interval tree.
     */
    public IntervalTree() {
        root = null;
    }

    /**
     * Insert element into tree with interval from {@code low} (inclusive) to
     * {@code high} (exclusive).
     * 
     * @param element element to insert
     * @param low
     * @param high
     * @throws IllegalArgumentException if low >= high
     */
    public void insert(E element, int low, int high) {
        Interval interval = new Interval(element, low, high);
        if (root == null) {
            root = new Node(interval);
        } else {
            root.add(interval);
        }
    }

    /**
     * Returns all elements intersecting with {@code point}.
     * 
     * @return all elements intersecting with {@code point}
     */
    public List<E> find(int point) {
        if (root == null) {
            return Collections.emptyList();
        } else {
            return root.find(point);
        }
    }

    private class Interval {
        /**
         * Element stored by interval.
         */
        private final E element;

        /**
         * Inclusive interval start value.
         */
        private final int low;

        /**
         * Exclusive interval end value.
         */
        private final int high;

        /**
         * Constructs an interval over range [{@code low}, {@code high}).
         * 
         * @param element element to store
         * @param low     inclusive interval start value
         * @param high    exclusive interval end value
         * @throws IllegalArgumentException if low >= high
         */
        public Interval(E element, int low, int high) {
            if (low >= high) {
                throw new IllegalArgumentException("low >= high");
            }
            this.element = element;
            this.low = low;
            this.high = high;
        }

        /**
         * Returns the stored element.
         * 
         * @return the stored element
         */
        public E getElement() {
            return element;
        }

        public int getLow() {
            return low;
        }

        public int getHigh() {
            return high;
        }
    }

    private class Node {
        private Node leftNode;
        private Node rightNode;
        private final List<Interval> sortedIncreasing;
        private final List<Interval> sortedDecreaseing;

        /**
         * Used to sort by increasing left end point.
         */
        private final Comparator<Interval> leftSort = (l, r) -> l.getLow() - r.getLow();

        /**
         * Used to sort by decreasing right end point.
         */
        private final Comparator<Interval> rightSort = (l, r) -> r.getHigh() - l.getHigh();

        /**
         * Constructs a node with a single interval.
         * 
         * @param interval interval to store within node
         */
        public Node(Interval interval) {
            sortedIncreasing = new ArrayList<>();
            sortedDecreaseing = new ArrayList<>();
            sortedIncreasing.add(interval);
            sortedDecreaseing.add(interval);
            leftNode = null;
            rightNode = null;
        }

        /**
         * Add an interval to the node.
         * 
         * @param interval interval to add to node
         */
        public void add(Interval interval) {
            if (intersects(interval)) {
                insert(Collections.binarySearch(sortedIncreasing, interval, leftSort), interval, sortedIncreasing);
                insert(Collections.binarySearch(sortedDecreaseing, interval, rightSort), interval, sortedDecreaseing);
            } else if (interval.getHigh() < getMidPoint()) {
                if (leftNode == null) {
                    leftNode = new Node(interval);
                } else {
                    leftNode.add(interval);
                }
            } else {
                if (rightNode == null) {
                    rightNode = new Node(interval);
                } else {
                    rightNode.add(interval);
                }
            }
        }

        private void insert(int index, Interval interval, List<Interval> list) {
            if (index < 0) {
                index *= -1;
                index -= 1;
            }
            list.add(index, interval);
        }

        /**
         * Returns the left node.
         * 
         * @return the left node or null when no node exists
         */
        public Node getLeft() {
            return leftNode;
        }

        /**
         * Returns the right node.
         * 
         * @return the right node or null when no node exists
         */
        public Node getRight() {
            return rightNode;
        }

        public int getMidPoint() {
            return (sortedIncreasing.get(0).getLow() + sortedDecreaseing.get(0).getHigh()) / 2;
        }

        /**
         * Find all elements stored by intervals containing {@code point}.
         * 
         * @param point
         * @return all elements stored by intervals containing {@code point}.
         */
        public List<E> find(int point) {
            List<E> found = new ArrayList<>();
            find(point, found);

            if (point < getMidPoint() && getLeft() != null) {
                found.addAll(getLeft().find(point));
            } else if (point >= getMidPoint() && getRight() != null) {
                found.addAll(getRight().find(point));
            }

            return found;
        }

        private void find(int point, List<E> found) {
            if (point < getMidPoint()) {
                for (Interval interval : sortedIncreasing) {
                    if (interval.getLow() <= point) {
                        found.add(interval.getElement());
                    }
                }
            } else {
                for (Interval interval : sortedDecreaseing) {
                    if (interval.getHigh() >= point) {
                        found.add(interval.getElement());
                    }
                }
            }
        }

        /**
         * Returns true when interval intersects with midpoint and false otherwise.
         * 
         * @param interval interval to check for intersection with mid point
         * @return true when interval intersects with midpoint and false otherwise
         */
        private boolean intersects(Interval interval) {
            return interval.getLow() <= getMidPoint() && getMidPoint() <= interval.getHigh();
        }
    }
}
