package Shared;

/**
 * This class represents a open reading frame for a specific gene.
 * 
 * @author Angelika Ehlers
 * @version 1.0
 */

public class Orf {
    private int gfid;
    private int geneId;
    private int start;
    private int stop;
    private int position;

    /**
     * Default constructor
     */
    public Orf() {
        position = 1;
    }

    /**
     * Constructor
     * 
     * @param gid the gene id
     * @param s   the orf start location
     * @param e   the orf stop location
     * @param p   the orf position
     */
    public Orf(int gid, int s, int e, int p) {
        geneId = gid;
        start = s;
        stop = e;
        position = p;
    }

    /**
     * Get the genome family id
     * 
     * @return the genome family id
     */
    public int getGenomeFamily() {
        return gfid;
    }

    /**
     * Get the gene id
     * 
     * @return the gene id
     */
    public int getID() {
        return geneId;
    }

    /**
     * Get the start location of the orf
     * 
     * @return the start location of the gene's orf
     */
    public int getStart() {
        return start;
    }

    /**
     * Get the stop location of the orf
     * 
     * @return the stop location of the gene's orf
     */
    public int getStop() {
        return stop;
    }

    /**
     * Get the position value of the orf
     * 
     * @return the position value of the gene's orf
     */
    public int getPosition() {
        return position;
    }

    /**
     * Check if ORF is equal to
     * 
     * @param o the orf to compare to
     * @return if orfs are equal
     */
    public boolean isEqual(Orf o) {
        boolean iseq = true;
        if (start != o.start)
            iseq = false;
        if (stop != o.stop)
            iseq = false;
        if (position != o.position)
            iseq = false;
        return iseq;
    }

    /**
     * Check if ORF is equal to
     * 
     * @param o the orf to compare to
     * @return if orfs are equal
     */
    public boolean isEqualLocation(Orf o) {
        boolean iseq = true;
        if (start != o.start)
            iseq = false;
        if (stop != o.stop)
            iseq = false;
        return iseq;
    }
}
