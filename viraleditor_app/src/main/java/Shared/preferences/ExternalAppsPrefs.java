package Shared.preferences;

/*
 * ExternalAppsPrefs: Retrieves client prefs to send to application server
 * Copyright (C) 2004  Dr. Chris Upton University of Victoria
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */

import java.util.prefs.*;

/**
 * This class of methods is used to provide client applications with access to
 * preferences/configurations to programs that make requests to the application
 * server
 * 
 * @author Sameer Girn, modified Anup Misra
 * @version 1.1
 */
public class ExternalAppsPrefs {

    private static final String EXTERNAL_ROOT = "/ca/virology/externalapps";
    public static final String JDOTTER = "/ca/virology/externalapps/JDotter";
    public static final String GFS = "/ca/virology/externalapps/GFS";
    public static final String MUSCLE = "/ca/virology/externalapps/MUSCLE";
    public static final String CLUSTALW = "/ca/virology/externalapps/CLUSTALW";
    public static final String CLUSTALO = "/ca/virology/externalapps/CLUSTALO";
    public static final String TCOFFEE = "/ca/virology/externalapps/TCOFFEE";
    public static final String BLAST = "/ca/virology/externalapps/BLAST";
    public static final String NOPTALIGN = "/ca/virology/externalapps/NOPTALIGN";
    public static final String NAP = "/ca/virology/externalapps/NAP";
    public static final String MVIEW = "/ca/virology/externalapps/MVIEW";
    public static final String NEEDLE = "/ca/virology/externalapps/NEEDLE";
    public static final String GENEMARK = "/ca/virology/externalapps/GENEMARK";
    public static final String RPSBLAST = "/ca/virology/externalapps/RPSBLAST";
    public static final String MAFFT = "ca.virology/xml/externalapps.properties";

    protected static ExternalAppsPrefs myinstance = null;

    /*
     * Constructor which sets the xml file location and imports the preferences
     * 
     * @param xmlFileLoc - the location of the xml file containing all the
     * preferences
     */
    public ExternalAppsPrefs(String xmlFileLoc) {

        final String XML_LOC = xmlFileLoc;
        myinstance = this;

        try {
            // import new preferences if they don't already exist
            // note: client preferences should never be overwritten
            PrefsTools pt = new PrefsTools();
            System.out.print("Searching for existing preferences...");
            if (Preferences.userRoot().nodeExists(EXTERNAL_ROOT)) {
                System.out.println(EXTERNAL_ROOT + " already exists");
                // check for additions to preferences (if new keys were added)
                pt.checkForNewPrefs(xmlFileLoc, EXTERNAL_ROOT, "version");
            } else {
                System.out.println("Importing new preferences: " + EXTERNAL_ROOT);
                pt.importPrefs(XML_LOC);
                System.out.println("Preferences imported");
            }

        } catch (BackingStoreException e) {
            System.out.println("BackingStoreException");
            System.out.println("Error: " + e.getMessage());
            e.printStackTrace();
        }
    }

    /**
     * Gets the instance of the attribute of the ExternalAppsPrefs class
     * 
     * @return The instance value
     */
    public static ExternalAppsPrefs getInstance() {
        return myinstance;
    }

    /**
     * return the nag flag for a specific application
     * 
     * @param nagNode the application nag flag to get
     * @return true/false
     */
    public boolean getnag(String nagNode) {
        Preferences serviceNode = Preferences.userRoot().node(nagNode);
        try {
            return serviceNode.getBoolean("nag", true);
        } catch (Exception e) {
            System.out.println("Error: " + e.getMessage());
            e.printStackTrace();
        }
        return true;
    }

    /**
     * set the nag flag for a specific application
     * 
     * @param nagNode the application nag flag to get
     * @param value   the nag value to set
     */
    public void setnag(String nagNode, boolean value) {
        Preferences serviceNode = Preferences.userRoot().node(nagNode);
        try {
            serviceNode.putBoolean("nag", value);
            serviceNode.flush();
        } catch (Exception e) {
            System.out.println("Error: " + e.getMessage());
            e.printStackTrace();
        }
    }

    /**
     * return the gfs window size
     * 
     * @return window size
     */
    public int get_gfs_windowsize() {
        Preferences serviceNode = Preferences.userRoot().node(GFS);
        int win = 500;
        try {
            win = serviceNode.getInt("windowSize", 500);
        } catch (Exception e) {
            System.out.println("ca.virology.lib.prefs.ExternalAppsPrefs Error: " + e.getMessage());
            e.printStackTrace();
        }
        return win;
    }

    /**
     * return the gfs isotopic mode
     * 
     * @return the isotopic mode
     */
    public String get_gfs_isotopicmode() {
        Preferences serviceNode = Preferences.userRoot().node(GFS);
        String mode = "m";
        try {
            mode = serviceNode.get("isotopicMode", "m");
        } catch (Exception e) {
            System.out.println("ca.virology.lib.prefs.ExternalAppsPrefs Error: " + e.getMessage());
            e.printStackTrace();
        }
        return mode;
    }

    /**
     * return the gfs max. missed cleavage
     * 
     * @return the max. missed cleavage
     */
    public int get_gfs_cleavage() {
        Preferences serviceNode = Preferences.userRoot().node(GFS);
        int size = 2;
        try {
            size = serviceNode.getInt("cleavage", 2);
        } catch (Exception e) {
            System.out.println("ca.virology.lib.prefs.ExternalAppsPrefs Error: " + e.getMessage());
            e.printStackTrace();
        }
        return size;
    }

    /**
     * return the gfs max. missed cleavage
     * 
     * @return the max. missed cleavage
     */
    public double get_gfs_tolerance() {
        Preferences serviceNode = Preferences.userRoot().node(GFS);
        double size = 0.01;
        try {
            size = serviceNode.getDouble("tolerance", 0.01);
        } catch (Exception e) {
            System.out.println("ca.virology.lib.prefs.ExternalAppsPrefs Error: " + e.getMessage());
            e.printStackTrace();
        }
        return size;
    }

    /**
     * set the gfs window size
     * 
     * @param size window size to set
     */
    public void set_gfs_windowsize(int size) {
        Preferences serviceNode = Preferences.userRoot().node(GFS);
        try {
            serviceNode.putInt("windowSize", size);
            serviceNode.flush();
        } catch (Exception e) {
            System.out.println("ca.virology.lib.prefs.ExternalAppsPrefs Error: " + e.getMessage());
            e.printStackTrace();
        }
    }

    /**
     * set the gfs isotopic mode
     * 
     * @param mode isotopic mode to set
     */
    public void set_gfs_isotopicmode(String mode) {
        Preferences serviceNode = Preferences.userRoot().node(GFS);
        try {
            serviceNode.put("isotopicMode", mode);
            serviceNode.flush();
        } catch (Exception e) {
            System.out.println("ca.virology.lib.prefs.ExternalAppsPrefs Error: " + e.getMessage());
            e.printStackTrace();
        }
    }

    /*
     * set the gfs max. missed cleavage
     * 
     * @param size max. missed cleavage to set
     */
    public void set_gfs_cleavage(int size) {
        Preferences serviceNode = Preferences.userRoot().node(GFS);
        try {
            serviceNode.putInt("cleavage", size);
            serviceNode.flush();
        } catch (Exception e) {
            System.out.println("ca.virology.lib.prefs.ExternalAppsPrefs Error: " + e.getMessage());
            e.printStackTrace();
        }
    }

    /*
     * set the gfs delta percent match tolerance
     * 
     * @param size delta percent match tolerance to set
     */
    public void set_gfs_tolerance(double size) {
        Preferences serviceNode = Preferences.userRoot().node(GFS);
        try {
            serviceNode.putDouble("tolerance", size);
            serviceNode.flush();
        } catch (Exception e) {
            System.out.println("ca.virology.lib.prefs.ExternalAppsPrefs Error: " + e.getMessage());
            e.printStackTrace();
        }
    }

    /**
     * return the window size for dot plots
     * 
     * @return window size
     */
    public int getwinsize() {
        Preferences serviceNode = Preferences.userRoot().node(JDOTTER);
        try {
            return serviceNode.getInt("winsize", 50);
        } catch (Exception e) {
            System.out.println("Error: " + e.getMessage());
            e.printStackTrace();
        }
        return 50;
    }

    /**
     * set the window size for dot plots
     * 
     * @param window size to set
     */
    public void setwinsize(int size) {
        Preferences serviceNode = Preferences.userRoot().node(JDOTTER);
        try {
            serviceNode.putInt("winsize", size);
            serviceNode.flush();
        } catch (Exception e) {
            System.out.println("Error: " + e.getMessage());
            e.printStackTrace();
        }
    }

    /**
     * return the plot size for dot plots
     * 
     * @return plot size
     */
    public int getplotsize() {
        Preferences serviceNode = Preferences.userRoot().node(JDOTTER);
        try {
            return serviceNode.getInt("plotsize", 700);
        } catch (Exception e) {
            System.out.println("Error: " + e.getMessage());
            e.printStackTrace();
        }
        return 700;
    }

    /**
     * set the plot size for dot plots
     * 
     * @param plot size to set
     */
    public void setplotsize(int size) {
        Preferences serviceNode = Preferences.userRoot().node(JDOTTER);
        try {
            serviceNode.putInt("plotsize", size);
            serviceNode.flush();
        } catch (Exception e) {
            System.out.println("Error: " + e.getMessage());
            e.printStackTrace();
        }
    }

    /**
     * @return Returns the default sge behavior.
     */
    public boolean getJDotterSGE() {
        Preferences serviceNode = Preferences.userRoot().node(JDOTTER);
        try {
            return serviceNode.getBoolean("sge", false);
        } catch (Exception e) {
            System.out.println("Error: " + e.getMessage());
        }
        return false;
    }

    /**
     * @param Set sge flag
     */
    public void setJDotterSGE(boolean b) {
        Preferences serviceNode = Preferences.userRoot().node(JDOTTER);
        try {
            serviceNode.putBoolean("sge", b);
        } catch (Exception e) {
            System.out.println("Error: " + e.getMessage());
        }
    }

    /**
     * @return Returns the default client side execution behavior.
     */
    public boolean getJDotterClientLocal() {
        Preferences serviceNode = Preferences.userRoot().node(JDOTTER);
        try {
            return serviceNode.getBoolean("clientLocal", false);
        } catch (Exception e) {
            System.out.println("Error: " + e.getMessage());
        }
        return false;
    }

    /**
     * @param Set client side execution flag
     */
    public void setJDotterClientLocal(boolean b) {
        Preferences serviceNode = Preferences.userRoot().node(JDOTTER);
        try {
            serviceNode.putBoolean("clientLocal", b);
        } catch (Exception e) {
            System.out.println("Error: " + e.getMessage());
        }
    }

    /**
     * Get the path to client side muscle
     * 
     * @return
     */
    public String getJDotterLocalPath() {
        Preferences serviceNode = Preferences.userRoot().node(MUSCLE);
        try {
            return serviceNode.get("localPath", "");
        } catch (Exception e) {
            System.out.println("Error: " + e.getMessage());
            e.printStackTrace();
        }
        return "";
    }

    public void setJDotterLocalPath(String path) {
        Preferences serviceNode = Preferences.userRoot().node(MUSCLE);
        try {
            serviceNode.put("localPath", path);
        } catch (Exception e) {
            System.out.println("Error: " + e.getMessage());
        }

    }

    // *****-------------------------- Muscle ------------------------------****

    /**
     * set the maximum iterations for Muscle
     * 
     * @param max iterations to set
     */
    public void setMuscleMaximumIterations(int intSize) {
        // TODO modify preference/XML file to include MUSCLE as a node with
        // maxIterations as a field.
        Preferences serviceNode = Preferences.userRoot().node(MUSCLE);
        try {
            serviceNode.putInt("maximumIterations", intSize);
            serviceNode.flush();
        } catch (Exception e) {
            System.out.println("Error: " + e.getMessage());
            e.printStackTrace();
        }
    }

    /**
     * get the maximum iterations for Muscle
     * 
     * @return max iterations
     */
    public int getMuscleMaximumIterations() {
        Preferences serviceNode = Preferences.userRoot().node(MUSCLE);
        try {
            // note default is 16
            return serviceNode.getInt("maximumIterations", 16);
        } catch (Exception e) {
            System.out.println("Error: " + e.getMessage());
            e.printStackTrace();
        }
        return 16;
    }

    /**
     * set the maximum hours for Muscle
     * 
     * @param max hours to set
     */
    public void setMuscleMaximumHours(float fltHours) {
        Preferences serviceNode = Preferences.userRoot().node(MUSCLE);
        try {
            serviceNode.putFloat("maximumHours", fltHours);
            serviceNode.flush();
        } catch (Exception e) {
            System.out.println("Error: " + e.getMessage());
            e.printStackTrace();
        }
    }

    /**
     * get the maximum hours which Muscle is set to execute
     * 
     * @return max hours
     */
    public float getMuscleMaximumHours() {
        Preferences serviceNode = Preferences.userRoot().node(MUSCLE);
        try {
            // note default is none, hence 99
            return serviceNode.getFloat("maximumHours", 0.0f);
        } catch (Exception e) {
            System.out.println("Error: " + e.getMessage());
            e.printStackTrace();
        }
        return 99.9f;
    }

    /**
     * @return Returns the default sge behavior.
     */
    public boolean getMuscleSGE() {
        Preferences serviceNode = Preferences.userRoot().node(MUSCLE);
        try {
            return serviceNode.getBoolean("sge", false);
        } catch (Exception e) {
            System.out.println("Error: " + e.getMessage());
        }
        return false;
    }

    /**
     * @param Set sge flag
     */
    public void setMuscleSGE(boolean b) {
        Preferences serviceNode = Preferences.userRoot().node(MUSCLE);
        try {
            serviceNode.putBoolean("sge", b);
        } catch (Exception e) {
            System.out.println("Error: " + e.getMessage());
        }
    }

    /**
     * @return Returns the default client side execution behavior.
     */
    public boolean getMuscleClientLocal() {
        Preferences serviceNode = Preferences.userRoot().node(MUSCLE);
        try {
            return serviceNode.getBoolean("clientLocal", false);
        } catch (Exception e) {
            System.out.println("Error: " + e.getMessage());
        }
        return false;
    }

    /**
     * @param Set client side execution flag
     */
    public void setMuscleClientLocal(boolean b) {
        Preferences serviceNode = Preferences.userRoot().node(MUSCLE);
        try {
            serviceNode.putBoolean("clientLocal", b);
        } catch (Exception e) {
            System.out.println("Error: " + e.getMessage());
        }
    }

    /**
     * Get the path to client side muscle
     * 
     * @return
     */
    public String getMuscleLocalPath() {
        Preferences serviceNode = Preferences.userRoot().node(MUSCLE);
        try {
            return serviceNode.get("localPath", "");
        } catch (Exception e) {
            System.out.println("Error: " + e.getMessage());
            e.printStackTrace();
        }
        return "";
    }

    public void setMuscleLocalPath(String path) {
        Preferences serviceNode = Preferences.userRoot().node(MUSCLE);
        try {
            serviceNode.put("localPath", path);
        } catch (Exception e) {
            System.out.println("Error: " + e.getMessage());
        }

    }

    /**
     * Get the path to client side temp directory
     * 
     * @return
     */
    public String getLocalTempPath() {
        Preferences serviceNode = Preferences.userRoot().node(MUSCLE);
        try {
            return serviceNode.get("clientTempPath", "");
        } catch (Exception e) {
            System.out.println("Error: " + e.getMessage());
            e.printStackTrace();
        }
        return "";
    }

    public void setLocalTempPath(String path) {
        Preferences serviceNode = Preferences.userRoot().node(MUSCLE);
        try {
            serviceNode.put("clientTempPath", path);
        } catch (Exception e) {
            System.out.println("Error: " + e.getMessage());
        }

    }

    // *** ---------------- Blast ----------------------------------------

    /**
     * Set for generate blast types (when type is not known)
     * 
     * @deprecated Use setBlastDescriptions(String strType), where strType is one of
     *             blastp, blastn, tblastn, psiblast, blastx
     * @param in
     */
    @Deprecated
    public void setBlastDescriptions(int in) {
        Preferences serviceNode = Preferences.userRoot().node(BLAST);
        try {
            serviceNode.putInt("descriptions", in);
            serviceNode.flush();
        } catch (Exception e) {
            System.out.println("Error: " + e.getMessage());
            e.printStackTrace();
        }
    }

    /**
     * Get number of descriptiosn for blast
     * 
     * @param in
     * @param strBlastType one of blastp, blastn, tblastn, psiblast, blastx
     */
    public void setBlastDescriptions(int in, String strBlastType) {
        Preferences serviceNode = Preferences.userRoot().node(BLAST);
        try {
            serviceNode.putInt(strBlastType.toLowerCase() + "Descriptions", in);
            serviceNode.flush();
        } catch (Exception e) {
            System.out.println("Error: " + e.getMessage());
            e.printStackTrace();
        }
    }

    /**
     * Set for generate blast alignments
     * 
     * @deprecated Use setBlastAlignments(int in, String strType), where strType is
     *             one of blastp, blastn, tblastn, psiblast, blastx
     * @param in number of database sequence to show alignments for.
     */
    @Deprecated
    public void setBlastAlignments(int in) {
        Preferences serviceNode = Preferences.userRoot().node(BLAST);
        try {
            serviceNode.putInt("alignments", in);
            serviceNode.flush();
        } catch (Exception e) {
            System.out.println("Error: " + e.getMessage());
            e.printStackTrace();
        }
    }

    /**
     * Set the number of database sequence to show alignments for.
     * 
     * @param in
     * @param strBlastType one of blastp, blastn, tblastn, psiblast, blastx
     */
    public void setBlastAlignments(int in, String strBlastType) {
        Preferences serviceNode = Preferences.userRoot().node(BLAST);
        try {
            serviceNode.putInt(strBlastType.toLowerCase() + "Alignments", in);
            serviceNode.flush();
        } catch (Exception e) {
            System.out.println("Error: " + e.getMessage());
            e.printStackTrace();
        }
    }

    /**
     * For "generic blast" - when one does not know the blast type
     * 
     * @deprecated Use setBlastDescriptions(String strType), where strType is one of
     *             blastp, blastn, tblastn, psiblast, blastx
     * @return Number of database sequences to show one-line alignments for.
     */
    @Deprecated
    public int getBlastDescriptions() {
        Preferences serviceNode = Preferences.userRoot().node(BLAST);
        // different paths since blastn's default value is different from the rest in
        // the blast family.

        try {
            return serviceNode.getInt("descriptions", 500);
        } catch (Exception e) {
            System.out.println("Error: " + e.getMessage());
        }
        return 500;
    }

    /**
     * Returns the number of database sequences to show alignments for.
     * 
     * @param strBlastType type of blast; one of blastp, blastn, tblastn, psiblast,
     *                     blastx
     * @return number of database sequences to show alignments for.
     */
    public int getBlastDescriptions(String strBlastType) {
        Preferences serviceNode = Preferences.userRoot().node(BLAST);
        // different paths since blastn's default value is different from the rest in
        // the blast family.

        try {
            return serviceNode.getInt(strBlastType.toLowerCase() + "Descriptions", 500);
        } catch (Exception e) {
            System.out.println("Error: " + e.getMessage());
        }
        return 500;
    }

    /**
     * For "generic blast" - when one does not know the blast type
     * 
     * @deprecated Use getBlastAlignments(String strType), where strType is one of
     *             blastp, blastn, tblastn, psiblast, blastx
     * @return number of alignments to show descriptions for.
     */
    @Deprecated
    public int getBlastAlignments() {
        Preferences serviceNode = Preferences.userRoot().node(BLAST);
        // different paths since blastn's default value is different from the rest in
        // the blast family.
        try {
            return serviceNode.getInt("alignments", 500);
        } catch (Exception e) {
            System.out.println("Error: " + e.getMessage());
        }
        return 500;
    }

    /**
     * get the number of database sequence to show alignments for
     * 
     * @param strBlastType type of blast; one of blastp, blastn, tblastn, psiblast,
     *                     blastx
     * @return number of database sequence to show alignemnts for, for a paricular
     *         blast varient.
     */
    public int getBlastAlignments(String strBlastType) {
        Preferences serviceNode = Preferences.userRoot().node(BLAST);
        // different paths since blastn's default value is different from the rest in
        // the blast family.
        try {
            return serviceNode.getInt(strBlastType.toLowerCase() + "Alignments", 500);
        } catch (Exception e) {
            System.out.println("Error: " + e.getMessage());
        }
        return 500;
    }

    /**
     * Set the e-value for psiblast
     * 
     * @param evalue the e-value to set
     */

    public void setBlastEValue(double evalue) {
        Preferences serviceNode = Preferences.userRoot().node(BLAST);
        try {
            serviceNode.putDouble("psiblastEValue", evalue);
            serviceNode.flush();
        } catch (Exception e) {
            System.out.println("Error: " + e.getMessage());
            e.printStackTrace();
        }
    }

    /**
     * Get the e-value for psiblast
     *
     */
    public double getBlastEValue() {
        Preferences serviceNode = Preferences.userRoot().node(BLAST);
        try {
            // default is 0.005
            return serviceNode.getDouble("psiblastEValue", 0.005);
        } catch (Exception e) {
            System.out.println("Error: " + e.getMessage());
        }
        return 0.005;
    }

    /**
     * Set the maximum number of passes for psiblast
     * 
     * @param maxPasses max passes to set
     */
    public void setBlastMaxPasses(int maxPasses) {
        Preferences serviceNode = Preferences.userRoot().node(BLAST);
        try {
            serviceNode.putInt("psiblastMaxPasses", maxPasses);
            serviceNode.flush();
        } catch (Exception e) {
            System.out.println("Error: " + e.getMessage());
            e.printStackTrace();
        }
    }

    /**
     * Get the maximum numbner of passes for psiblast
     * 
     * @return max maximum number of passes.
     */
    public int getBlastMaxPasses() {
        Preferences serviceNode = Preferences.userRoot().node(BLAST);
        try {
            // default is 0
            return serviceNode.getInt("psiblastMaxPasses", 1);
        } catch (Exception e) {
            System.out.println("Error: " + e.getMessage());
        }
        return 1;
    }

    /**
     * Set the word size for Blast
     * 
     * @param strBlastType one of blastp, blastn, tblastn, psiblast, blastx
     * @param wordSize     the word size to set
     */
    public void setBlastWordSize(int wordSize, String strBlastType) {
        Preferences serviceNode = Preferences.userRoot().node(BLAST);
        try {
            serviceNode.putInt(strBlastType.toLowerCase() + "WordSize", wordSize);
            serviceNode.flush();
        } catch (Exception e) {
            System.out.println("Error: " + e.getMessage());
            e.printStackTrace();
        }
    }

    /**
     * Used when type unknown; for backwards compatibility
     * 
     * @deprecated Use setBlastWordSize(String strType), where strType is one of
     *             blastp, blastn, tblastn, psiblast, blastx
     * @param wordSize
     */
    @Deprecated
    public void setBlastWordSize(int wordSize) {
        Preferences serviceNode = Preferences.userRoot().node(BLAST);
        try {
            serviceNode.putInt("wordSize", wordSize);
            serviceNode.flush();
        } catch (Exception e) {
            System.out.println("Error: " + e.getMessage());
            e.printStackTrace();
        }
    }

    /**
     * Get the word size for Blast: strBlastType must be either: blastp, tblastn,
     * psiblast, blastx, etc. in lower case
     * 
     * @param strBlastType one of blastp, blastn, tblastn, psiblast, blastx
     * @return word size
     */
    public int getBlastWordSize(String strBlastType) {
        Preferences serviceNode = Preferences.userRoot().node(BLAST);
        // different paths since blastn's default value is different from the rest in
        // the blast family.
        if (strBlastType.equals("blastn")) {
            try {
                return serviceNode.getInt(strBlastType.toLowerCase() + "WordSize", 11);
            } catch (Exception e) {
                System.out.println("Error: " + e.getMessage());
            }
            return 3;
        } else {
            // for blastp, psiblast, etc.
            try {
                return serviceNode.getInt(strBlastType.toLowerCase() + "WordSize", 3);
            } catch (Exception e) {
                System.out.println("Error: " + e.getMessage());
            }
            return 2;
        }
    }

    /**
     * Used for when blast type is unknown; for backwards compatibility. Returns
     * default wordsize for nucleotide blast
     * 
     * @deprecated Use getBlastwordSize(String strType), where strType is one of
     *             blastp, blastn, tblastn, psiblast, blastx
     * @param value
     */
    @Deprecated
    public int getBlastWordSize() {
        Preferences serviceNode = Preferences.userRoot().node(BLAST);
        // different paths since blastn's default value is different from the rest in
        // the blast family.
        try {
            return serviceNode.getInt("wordSize", 11);
        } catch (Exception e) {
            System.out.println("Error: " + e.getMessage());
        }
        return 3;
    }

    /**
     * set the low complexity flag for Blast
     * 
     * @param value        the low complexity value (true/false)
     * @param strBlastType one of blastp, blastn, tblastn, psiblast, blastx
     */

    public void setBlastLowComplexity(boolean value, String strBlastType) {
        Preferences serviceNode = Preferences.userRoot().node(BLAST);
        try {
            serviceNode.putBoolean(strBlastType.toLowerCase() + "LowComplexity", value);
        } catch (Exception e) {
            System.out.println("Error: " + e.getMessage());
            e.printStackTrace();
        }
    }

    /**
     * Used for when blast type is unknown; for backwards compatibility.
     * 
     * @deprecated Use setBlastLowComplexity(String strBlastType), where
     *             strBlastType is one of blastp, blastn, tblastn, psiblast, blastx
     * @param value true for low complexity; false otherwise
     */
    @Deprecated
    public void setBlastLowComplexity(boolean value) {
        Preferences serviceNode = Preferences.userRoot().node(BLAST);
        try {
            serviceNode.putBoolean("lowComplexity", value);
        } catch (Exception e) {
            System.out.println("Error: " + e.getMessage());
            e.printStackTrace();
        }
    }

    /**
     * get the value of the low complexity flag for Blast
     * 
     * @param strBlastType one of blastp, blastn, tblastn, psiblast, blastx
     * @return true/false
     */
    public boolean getBlastLowComplexity(String strBlastType) {
        Preferences serviceNode = Preferences.userRoot().node(BLAST);
        // true for all amino acids.
        try {
            return serviceNode.getBoolean(strBlastType.toLowerCase() + "LowComplexity", true);
        } catch (Exception e) {
            System.out.println("Error: " + e.getMessage());
        }
        return true;
    }

    /**
     * Used for when blast type is unknown; for backwards compatibility.
     * 
     * @deprecated Use getBlastLowComplexity(String strBlastType), where
     *             strBlastType is one of blastp, blastn, tblastn, psiblast, blastx
     * @param value true for low complexity; false otherwise
     */
    @Deprecated
    public boolean getBlastLowComplexity() {
        Preferences serviceNode = Preferences.userRoot().node(BLAST);
        // true for all amino acids.
        try {
            return serviceNode.getBoolean("lowComplexity", true);
        } catch (Exception e) {
            System.out.println("Error: " + e.getMessage());
        }
        return true;
    }

    /**
     * set expectation value for Blast
     * 
     * @param value        the expection value
     * @param strBlastType one of blastp, blastn, tblastn, psiblast, blastx
     */
    public void setBlastExpectation(double dblValue, String strBlastType) {
        Preferences serviceNode = Preferences.userRoot().node(BLAST);
        try {
            serviceNode.putDouble(strBlastType.toLowerCase() + "Expectation", dblValue);
        } catch (Exception e) {
            System.out.println("Error: " + e.getMessage());
            e.printStackTrace();
        }
    }

    /**
     * Used for when blast type is unknown; for backwards compatibility.
     * 
     * @deprecated Use setBlastExpectation(double dblValue,String strBlastType),
     *             where strBlastType is one of blastp, blastn, tblastn, psiblast,
     *             blastx
     */
    @Deprecated
    public void setBlastExpectation(double dblValue) {
        Preferences serviceNode = Preferences.userRoot().node(BLAST);
        try {
            serviceNode.putDouble("expectation", dblValue);
        } catch (Exception e) {
            System.out.println("Error: " + e.getMessage());
            e.printStackTrace();
        }
    }

    /**
     * get the expectation value for Blast
     * 
     * @param strBlastType one of blastp, blastn, tblastn, psiblast, blastx
     * @return expect expectation value
     */
    public double getBlastExpectation(String strBlastType) {
        Preferences serviceNode = Preferences.userRoot().node(BLAST);
        try {
            // default is 10.0
            return serviceNode.getDouble(strBlastType.toLowerCase() + "Expectation", 10.0);
        } catch (Exception e) {
            System.out.println("Error: " + e.getMessage());
        }
        return 10.0;
    }

    /**
     * Used for when blast type is unknown; for backwards compatibility.
     * 
     * @deprecated Use getBlastExpectation(String strBlastType), where strBlastType
     *             is one of blastp, blastn, tblastn, psiblast, blastx
     * @param true for low expectation; false otherwise
     */
    @Deprecated
    public double getBlastExpectation() {
        Preferences serviceNode = Preferences.userRoot().node(BLAST);
        try {
            // default is 10.0
            return serviceNode.getDouble("expectation", 10.0);
        } catch (Exception e) {
            System.out.println("Error: " + e.getMessage());
        }
        return 10.0;
    }

    /**
     * set the matrix value for Blast
     * 
     * @param matrix       the matrix selection.
     * @param strBlastType one of blastp, blastn, tblastn, psiblast, blastx
     */
    public void setBlastMatrix(String strMatrix, String strBlastType) {
        Preferences serviceNode = Preferences.userRoot().node(BLAST);
        try {
            serviceNode.put(strBlastType.toLowerCase() + "Matrix", strMatrix);
        } catch (Exception e) {
            System.out.println("Error: " + e.getMessage());
            e.printStackTrace();
        }
    }

    /**
     * Used for when blast type is unknown; for backwards compatibility.
     * 
     * @deprecated Use setBlastMatrix(String strMatrix, String strBlastType), where
     *             strBlastType is one of blastp, blastn, tblastn, psiblast, blastx
     * @param true for low expectation; false otherwise
     */
    @Deprecated
    public void setBlastMatrix(String strMatrix) {
        Preferences serviceNode = Preferences.userRoot().node(BLAST);
        try {
            serviceNode.put("matrix", strMatrix);
        } catch (Exception e) {
            System.out.println("Error: " + e.getMessage());
            e.printStackTrace();
        }
    }

    /**
     * get the matrix value for Blast
     * 
     * @param strBlastType one of blastp, blastn, tblastn, psiblast, blastx
     * @return matrix
     */
    public String getBlastMatrix(String strBlastType) {
        Preferences serviceNode = Preferences.userRoot().node(BLAST);
        try {
            // default BLOSUM62
            return serviceNode.get(strBlastType.toLowerCase() + "Matrix", "BLOSUM62");
        } catch (Exception e) {
            System.out.println("Error: " + e.getMessage());
        }
        return "BLOSUM62";
    }

    /**
     * Used for when blast type is unknown; for backwards compatibility.
     * 
     * @deprecated Use getBlastMatrix(String strBlastType), where strBlastType is
     *             one of blastp, blastn, tblastn, psiblast, blastx
     */
    @Deprecated
    public String getBlastMatrix() {
        Preferences serviceNode = Preferences.userRoot().node(BLAST);
        try {
            // default BLOSUM62
            return serviceNode.get("matrix", "BLOSUM62");
        } catch (Exception e) {
            System.out.println("Error: " + e.getMessage());
        }
        return "BLOSUM62";
    }

    /**
     * set the cost to open a gap for Blast
     * 
     * @param cost         the cost
     * @param strBlastType one of blastp, blastn, tblastn, psiblast, blastx
     */
    public void setBlastCostOpenGap(int intCost, String strBlastType) {
        Preferences serviceNode = Preferences.userRoot().node(BLAST);
        try {
            serviceNode.putInt(strBlastType.toLowerCase() + "CostOpenGap", intCost);
        } catch (Exception e) {
            System.out.println("Error: " + e.getMessage());
            e.printStackTrace();
        }
    }

    public void setBlastDropoffGappedAlignment(int intCost, String strBlastType) {
        Preferences serviceNode = Preferences.userRoot().node(BLAST);
        try {
            serviceNode.putInt(strBlastType.toLowerCase() + "DropoffGappedAlignment", intCost);
        } catch (Exception e) {
            System.out.println("Error:" + e.getMessage());
            e.printStackTrace();
        }
    }

    public int getBlastDropoffGappedAlignment(String strBlastType) {
        Preferences serviceNode = Preferences.userRoot().node(BLAST);
        // default value for these is 20
        if (strBlastType.equals("blastn")) {
            try {
                return serviceNode.getInt(strBlastType.toLowerCase() + "DropoffGappedAlignment", 20);
            } catch (Exception e) {
                System.out.println("Error: " + e.getMessage());
            }
            return 20;
        } else if (strBlastType.equals("tblastx")) {

            try {
                return serviceNode.getInt(strBlastType.toLowerCase() + "DropoffGappedAlignment", 0);
            } catch (Exception e) {
                System.out.println("Error: " + e.getMessage());
            }
            return 0;
        } else {
            // else default value is 25
            try {
                return serviceNode.getInt(strBlastType.toLowerCase() + "DropoffGappedAlignment", 25);
            } catch (Exception e) {
                System.out.println("Error: " + e.getMessage());
            }
            return 25;
        }
    }

    /**
     * Used for when blast type is unknown; for backwards compatibility.
     * 
     * @deprecated Use setBlastCostOpen(int inCost, String strBlastType), where
     *             strBlastType is one of blastp, blastn, tblastn, psiblast, blastx
     */
    @Deprecated
    public void setBlastCostOpenGap(int intCost) {
        Preferences serviceNode = Preferences.userRoot().node(BLAST);
        try {
            serviceNode.putInt("costOpenGap", intCost);
        } catch (Exception e) {
            System.out.println("Error: " + e.getMessage());
            e.printStackTrace();
        }
    }

    /**
     * get the cost to open gap
     * 
     * @param strBlastType one of blastp, blastn, tblastn, psiblast, blastx
     * @return cost gap open value
     */
    public int getBlastCostOpenGap(String strBlastType) {
        Preferences serviceNode = Preferences.userRoot().node(BLAST);
        // nuclodtide and protein blasts have different default gap open values.
        if (strBlastType.equals("blastn")) {
            try {
                return serviceNode.getInt(strBlastType.toLowerCase() + "CostOpenGap", 5);
            } catch (Exception e) {
                System.out.println("Error: " + e.getMessage());
            }
            return 5;
        } else {
            try {
                return serviceNode.getInt(strBlastType.toLowerCase() + "CostOpenGap", 11);
            } catch (Exception e) {
                System.out.println("Error: " + e.getMessage());
            }
            return 11;
        }
    }

    /**
     * Used for when blast type is unknown; for backwards compatibility. Note:
     * Initial default value is that for nucleotide blast
     * 
     * @deprecated Use getBlastCostOpenGap(String strBlastType), where strBlastType
     *             is one of blastp, blastn, tblastn, psiblast, blastx
     */
    @Deprecated
    public int getBlastCostOpenGap() {
        Preferences serviceNode = Preferences.userRoot().node(BLAST);
        // nuclodtide and protein blasts have different default gap open values.
        try {
            return serviceNode.getInt("costOpenGap", 5);
        } catch (Exception e) {
            System.out.println("Error: " + e.getMessage());
        }

        return 5;
    }

    /**
     * set the cost to extend a gap for Blast
     * 
     * @param cost         the cost
     * @param strBlastType one of blastp, blastn, tblastn, psiblast, blastx
     **/
    public void setBlastCostExtendGap(int intCost, String strBlastType) {
        Preferences serviceNode = Preferences.userRoot().node(BLAST);
        try {
            serviceNode.putInt(strBlastType.toLowerCase() + "CostExtendGap", intCost);
        } catch (Exception e) {
            System.out.println("Error: " + e.getMessage());
            e.printStackTrace();
        }
    }

    /**
     * Used for when blast type is unknown; for backwards compatibility.
     * 
     * @deprecated Use setBlastCostExtendGap(int inCost, String strBlastType), where
     *             strBlastType is one of blastp, blastn, tblastn, psiblast, blastx
     */
    @Deprecated
    public void setBlastCostExtendGap(int intCost) {
        Preferences serviceNode = Preferences.userRoot().node(BLAST);
        try {
            serviceNode.putInt("costExtendGap", intCost);
        } catch (Exception e) {
            System.out.println("Error: " + e.getMessage());
            e.printStackTrace();
        }
    }

    /**
     * get the cost to extend gap for Blast
     * 
     * @param strBlastType one of blastp, blastn, tblastn, psiblast, blastx
     * @return
     */
    public int getBlastCostExtendGap(String strBlastType) {
        Preferences serviceNode = Preferences.userRoot().node(BLAST);
        if (strBlastType.equals("blastn")) {
            try {
                return serviceNode.getInt(strBlastType.toLowerCase() + "CostExtendGap", 2);
            } catch (Exception e) {
                System.out.println("Error: " + e.getMessage());
            }
            return 2;
        } else {
            try {
                return serviceNode.getInt(strBlastType.toLowerCase() + "CostExtendGap", 1);
            } catch (Exception e) {
                System.out.println("Error: " + e.getMessage());
            }
            return 1;

        }
    }

    /**
     * Used for when blast type is unknown; for backwards compatibility.
     * 
     * @deprecated Use getBlastCostOpenGap(String strBlastType), where strBlastType
     *             is one of blastp, blastn, tblastn, psiblast, blastx
     */
    @Deprecated
    public int getBlastCostExtendGap() {
        Preferences serviceNode = Preferences.userRoot().node(BLAST);
        try {
            return serviceNode.getInt("costExtendGap", 2);
        } catch (Exception e) {
            System.out.println("Error: " + e.getMessage());
        }
        return 2;
    }

    /**
     * Set whether to use composition based stats in Blast
     * 
     * @param value        true or false
     * @param strBlastType one of blastp, blastn, tblastn, psiblast, blastx
     */
    public void setBlastCompositionBasedStats(boolean value, String strBlastType) {
        Preferences serviceNode = Preferences.userRoot().node(BLAST);
        try {
            serviceNode.putBoolean(strBlastType + "CompositionBasedStats", value);
        } catch (Exception e) {
            System.out.println("Error: " + e.getMessage());
        }
    }

    /**
     * Set whether blast is to use the Mpi implementation
     * 
     * @param value        true or false
     * @param strBlastType type of blast; one of blastp, blastn, tblastn, or blastx
     *                     (no psiblast!)
     */
    public void setBlastMPI(boolean value, String strBlastType) {
        Preferences serviceNode = Preferences.userRoot().node(BLAST);
        try {
            serviceNode.putBoolean(strBlastType + "MPI", value);
        } catch (Exception e) {
            System.out.println("Error:" + e.getMessage());
        }
    }

    /**
     * Get whether blast is to use Mpi implementation
     * 
     * @param strBlastType one of blastp, blastn, tblastn, or blastx (no psiblast)
     * @return true or false.
     */
    public boolean getBlastMPI(String strBlastType) {
        Preferences serviceNode = Preferences.userRoot().node(BLAST);
        try {
            return serviceNode.getBoolean(strBlastType + "MPI", false); // default value is false/
        } catch (Exception e) {
            System.out.println("Error:" + e.getMessage());
        }
        return false;
    }

    /**
     * Used for when blast type is unknown; for backwards compatibility.
     * 
     * @deprecated Use setBlastCompositionBasedStats(boolean value, String
     *             strBlastType), where strBlastType is one of blastp, blastn,
     *             tblastn, psiblast, blastx
     */
    @Deprecated
    public void setBlastCompositionBasedStats(boolean value) {
        Preferences serviceNode = Preferences.userRoot().node(BLAST);
        try {
            serviceNode.putBoolean("compositionBasedStats", value);
        } catch (Exception e) {
            System.out.println("Error: " + e.getMessage());
        }

    }

    /**
     * Get whether Blast is to use composition based stats
     * 
     * @param strBlastType one of blastp, blastn, tblastn, psiblast, blastx
     * @return value true or false
     */
    public boolean getBlastCompositionBasedStats(String strBlastType) {
        Preferences serviceNode = Preferences.userRoot().node(BLAST);
        try {
            return serviceNode.getBoolean(strBlastType + "CompositionBasedStats", true);
        } catch (Exception e) {
            System.out.println("Error: " + e.getMessage());
        }
        return false;

    }

    /**
     * Used for when blast type is unknown; for backwards compatibility.
     * 
     * @deprecated Use getBlastCompositionBasedStats(String strBlastType), where
     *             strBlastType is one of blastp, blastn, tblastn, psiblast, blastx
     */
    @Deprecated
    public boolean getBlastCompositionBasedStats() {
        Preferences serviceNode = Preferences.userRoot().node(BLAST);
        try {
            return serviceNode.getBoolean("compositionBasedStats", true);
        } catch (Exception e) {
            System.out.println("Error: " + e.getMessage());
        }
        return false;

    }

    /**
     * @return Returns the default sge behavior.
     */
    public boolean getBlastSGE() {
        Preferences serviceNode = Preferences.userRoot().node(BLAST);
        try {
            return serviceNode.getBoolean("sge", false);
        } catch (Exception e) {
            System.out.println("Error: " + e.getMessage());
        }
        return false;
    }

    /**
     * @param Set sge flag
     */
    public void setBlastSGE(boolean b) {
        Preferences serviceNode = Preferences.userRoot().node(BLAST);
        try {
            serviceNode.putBoolean("sge", b);
        } catch (Exception e) {
            System.out.println("Error: " + e.getMessage());
        }
    }

    /**
     * @return Returns the default client side execution behavior.
     */
    public boolean getBlastClientLocal() {
        Preferences serviceNode = Preferences.userRoot().node(BLAST);
        try {
            return serviceNode.getBoolean("clientLocal", false);
        } catch (Exception e) {
            System.out.println("Error: " + e.getMessage());
        }
        return false;
    }

    /**
     * @param Set client side execution flag
     */
    public void setBlastClientLocal(boolean b) {
        Preferences serviceNode = Preferences.userRoot().node(BLAST);
        try {
            serviceNode.putBoolean("clientLocal", b);
        } catch (Exception e) {
            System.out.println("Error: " + e.getMessage());
        }
    }

    /**
     * get the Client side path of blastall
     * 
     * @return path String
     */
    public String getBlastall() {
        Preferences serviceNode = Preferences.userRoot().node(BLAST);
        try {
            return serviceNode.get("blastall", "");
        } catch (Exception e) {
            System.out.println("Error: " + e.getMessage());
            e.printStackTrace();
        }
        return "";

    }

    public void setBlastall(String path) {
        Preferences serviceNode = Preferences.userRoot().node(BLAST);
        try {
            serviceNode.put("blastall", path);
        } catch (Exception e) {
            System.out.println("Error: " + e.getMessage());
        }

    }

    /**
     * get the Client side path of blastpgp
     * 
     * @return path String
     */
    public String getBlastpgp() {
        Preferences serviceNode = Preferences.userRoot().node(BLAST);
        try {
            return serviceNode.get("blastpgp", "");
        } catch (Exception e) {
            System.out.println("Error: " + e.getMessage());
            e.printStackTrace();
        }
        return "";

    }

    public void setBlastpgp(String path) {
        Preferences serviceNode = Preferences.userRoot().node(BLAST);
        try {
            serviceNode.put("blastpgp", path);
        } catch (Exception e) {
            System.out.println("Error: " + e.getMessage());
        }

    }

    /**
     * get the Client side path of formatdb
     * 
     * @return path String
     */
    public String getBlastFormatdb() {
        Preferences serviceNode = Preferences.userRoot().node(BLAST);
        try {
            return serviceNode.get("formatdb", "");
        } catch (Exception e) {
            System.out.println("Error: " + e.getMessage());
            e.printStackTrace();
        }
        return "";

    }

    public void setBlastFormatdb(String path) {
        Preferences serviceNode = Preferences.userRoot().node(BLAST);
        try {
            serviceNode.put("formatdb", path);
        } catch (Exception e) {
            System.out.println("Error: " + e.getMessage());
        }

    }

    /**
     * get the Client side path of fastacmd
     * 
     * @return path String
     */
    public String getBlastFastacmd() {
        Preferences serviceNode = Preferences.userRoot().node(BLAST);
        try {
            return serviceNode.get("fastacmd", "");
        } catch (Exception e) {
            System.out.println("Error: " + e.getMessage());
            e.printStackTrace();
        }
        return "";

    }

    public void setBlastFastacmd(String path) {
        Preferences serviceNode = Preferences.userRoot().node(BLAST);
        try {
            serviceNode.put("fastacmd", path);
        } catch (Exception e) {
            System.out.println("Error: " + e.getMessage());
        }

    }

    /**
     * get the Client side path of blast db
     * 
     * @return path String
     */
    public String getBlastdb() {
        Preferences serviceNode = Preferences.userRoot().node(BLAST);
        try {
            return serviceNode.get("blastdb", "");
        } catch (Exception e) {
            System.out.println("Error: " + e.getMessage());
            e.printStackTrace();
        }
        return "";

    }

    public void setBlastdb(String path) {
        Preferences serviceNode = Preferences.userRoot().node(BLAST);
        try {
            serviceNode.put("blastdb", path);
        } catch (Exception e) {
            System.out.println("Error: " + e.getMessage());
        }

    }

    // ****--------------------- Clustalw -----------------------------------------
    /**
     * set the matrix for Clustalw
     * 
     * @param matrix  matrix to use
     * @param strType dna or protein
     */
    public void setClustalwMatrix(String matrix, String strType) {
        Preferences serviceNode = Preferences.userRoot().node(CLUSTALW);
        try {
            serviceNode.put(strType + "Matrix", matrix);
        } catch (Exception e) {
            System.out.println("Error: " + e.getMessage());
        }

    }

    /**
     * get the matrix for Clustalw
     * 
     * @return matrix matrix which is currently set for Clustalw
     */
    public String getClustalwMatrix(String strType) {
        Preferences serviceNode = Preferences.userRoot().node(CLUSTALW);
        try {
            return serviceNode.get(strType + "Matrix", "GONNET");
        } catch (Exception e) {
            System.out.println("Error: " + e.getMessage());
            e.printStackTrace();
        }
        return "GONNET";

    }

    /**
     * set the gap open setting for Clustalw
     * 
     * @param gapOpen gap open value
     * @param strType dna or protein
     */
    public void setClustalwGapOpen(float gapOpen, String strType) {
        Preferences serviceNode = Preferences.userRoot().node(CLUSTALW);
        try {
            serviceNode.putFloat(strType + "GapOpen", gapOpen);
        } catch (Exception e) {
            System.out.println("Error: " + e.getMessage());
        }

    }

    /**
     * get the pairwise gap open setting for clustalw
     * 
     * @param strType dna or protein * @return int gap open setting
     */
    public float getClustalwGapOpen(String strType) {
        Preferences serviceNode = Preferences.userRoot().node(CLUSTALW);
        try {
            return serviceNode.getFloat(strType + "GapOpen", 10.0f);
        } catch (Exception e) {
            System.out.println("Error: " + e.getMessage());
            e.printStackTrace();
        }
        return 10.0f;
    }

    /**
     * Set the gap extend value for Clustalw
     * 
     * @param gapExtend gap extend setting
     * @param strType   dna or protein
     */
    public void setClustalwGapExtend(float gapExtend, String strType) {
        Preferences serviceNode = Preferences.userRoot().node(CLUSTALW);
        try {
            serviceNode.putFloat(strType + "GapExtend", gapExtend);
        } catch (Exception e) {
            System.out.println("Error: " + e.getMessage());
            e.printStackTrace();
        }
    }

    /**
     * get the pairwise gap extend value for Clustalw
     * 
     * @param strType dna or protein
     * @return gapExted gap extend setting
     */
    public float getClustalwGapExtend(String strType) {
        Preferences serviceNode = Preferences.userRoot().node(CLUSTALW);
        try {
            float value = serviceNode.getFloat(strType + "GapExtend", 0.02f);
            return value;
        } catch (Exception e) {
            System.out.println("Error: " + e.getMessage());
            e.printStackTrace();
        }
        return 0.02f;
    }

    /**
     * sets whether Clustalw is to use negative matrix valuess
     * 
     * @param true    - for negative matrix values false - otherwise
     * @param strType dna or protein
     */
    public void setClustalwNegative(boolean value, String strType) {
        Preferences serviceNode = Preferences.userRoot().node(CLUSTALW);
        try {
            serviceNode.putBoolean(strType + "Negative", value);
        } catch (Exception e) {
            System.out.println("Error: " + e.getMessage());
            e.printStackTrace();
        }
    }

    /**
     * get whether Clustalw is to use negative matrices
     * 
     * @return true/false
     * @param strType dna or protein
     */
    public boolean getClustalwNegative(String strType) {
        Preferences serviceNode = Preferences.userRoot().node(CLUSTALW);
        try {
            return serviceNode.getBoolean(strType + "Negative", false);

        } catch (Exception e) {
            System.out.println("Error: " + e.getMessage());
            e.printStackTrace();
        }
        return false;
    }

    /**
     * sets whether Clustalw is to use negative matrix values Precondition: isDNA
     * must be false; negative only works with protein sequences
     * 
     * @param value   true or false
     * @param strType dna or protein
     */
    public void setClustalNegative(boolean value, String strType) {
        Preferences serviceNode = Preferences.userRoot().node(CLUSTALW);
        try {
            serviceNode.putBoolean(strType + "Negative", value);
        } catch (Exception e) {
            System.out.println("Error: " + e.getMessage());
            e.printStackTrace();
        }
    }

    /**
     * @return Returns the default sge behavior.
     */
    public boolean getClustalwSGE() {
        Preferences serviceNode = Preferences.userRoot().node(CLUSTALW);
        try {
            return serviceNode.getBoolean("sge", false);
        } catch (Exception e) {
            System.out.println("Error: " + e.getMessage());
        }
        return false;
    }

    /**
     * @param Set sge flag
     */
    public void setClustalwSGE(boolean b) {
        Preferences serviceNode = Preferences.userRoot().node(CLUSTALW);
        try {
            serviceNode.putBoolean("sge", b);
        } catch (Exception e) {
            System.out.println("Error: " + e.getMessage());
        }
    }

    // ***** ----------------------- CLUSTALO ---------------------- ******
    /**
     * @return Returns the default sge behavior.
     */
    public boolean getClustaloSGE() {
        Preferences serviceNode = Preferences.userRoot().node(CLUSTALO);
        try {
            return serviceNode.getBoolean("sge", false);
        } catch (Exception e) {
            System.out.println("Error: " + e.getMessage());
        }
        return false;
    }

    /**
     * @param Set sge flag
     */
    public void setClustaloSGE(boolean b) {
        Preferences serviceNode = Preferences.userRoot().node(CLUSTALO);
        try {
            serviceNode.putBoolean("sge", b);
        } catch (Exception e) {
            System.out.println("Error: " + e.getMessage());
        }
    }

    // ***** ----------------------- TCoffee ---------------------- ******

    /**
     * @return Returns the tCoffeeGapExtend.
     */
    public int getTCoffeeGapExtend() {
        Preferences serviceNode = Preferences.userRoot().node(CLUSTALW);
        try {
            return serviceNode.getInt("gapExtend", 0);
        } catch (Exception e) {
            System.out.println("Error: " + e.getMessage());
            e.printStackTrace();
        }
        return 0;

    }

    /**
     * @param coffeeGapExtend The tCoffeeGapExtend to set.
     */
    public void setTCoffeeGapExtend(int coffeeGapExtend) {
        Preferences serviceNode = Preferences.userRoot().node(TCOFFEE);
        try {
            serviceNode.putInt("gapExtend", coffeeGapExtend);
        } catch (Exception e) {
            System.out.println("Error: " + e.getMessage());
            e.printStackTrace();
        }
    }

    /**
     * @return Returns the tCoffeeGapOpen.
     */
    public int getTCoffeeGapOpen() {
        Preferences serviceNode = Preferences.userRoot().node(TCOFFEE);
        try {
            return serviceNode.getInt("gapOpen", 0);
        } catch (Exception e) {
            System.out.println("Error: " + e.getMessage());
            e.printStackTrace();
        }
        return 0;
    }

    /**
     * @param coffeeGapOpen The tCoffeeGapOpen to set.
     */
    public void setTCoffeeGapOpen(int coffeeGapOpen) {
        Preferences serviceNode = Preferences.userRoot().node(TCOFFEE);
        try {
            serviceNode.putInt("gapOpen", coffeeGapOpen);
        } catch (Exception e) {
            System.out.println("Error: " + e.getMessage());
            e.printStackTrace();
        }

    }

    /**
     * @return Returns the tCoffeeMatrix.
     */
    public String getTCoffeeMatrix() {
        Preferences serviceNode = Preferences.userRoot().node(TCOFFEE);
        try {
            return serviceNode.get("matrix", "idmat");
        } catch (Exception e) {
            System.out.println("Error: " + e.getMessage());
        }
        return "no matrix";
    }

    /**
     * @param coffeeMatrix The tCoffeeMatrix to set.
     */
    public void setTCoffeeMatrix(String coffeeMatrix) {
        Preferences serviceNode = Preferences.userRoot().node(TCOFFEE);
        try {
            serviceNode.put("matrix", coffeeMatrix);
        } catch (Exception e) {
            System.out.println("Error: " + e.getMessage());
        }
    }

    /**
     * @return Returns the default sge behavior.
     */
    public boolean getTCoffeeSGE() {
        Preferences serviceNode = Preferences.userRoot().node(TCOFFEE);
        try {
            return serviceNode.getBoolean("sge", false);
        } catch (Exception e) {
            System.out.println("Error: " + e.getMessage());
        }
        return false;
    }

    /**
     * @param Set sge flag
     */
    public void setTCoffeeSGE(boolean b) {
        Preferences serviceNode = Preferences.userRoot().node(TCOFFEE);
        try {
            serviceNode.putBoolean("sge", b);
        } catch (Exception e) {
            System.out.println("Error: " + e.getMessage());
        }
    }

    /**
     * @return Returns the default client side execution behavior.
     */
    public boolean getTCoffeeClientLocal() {
        Preferences serviceNode = Preferences.userRoot().node(TCOFFEE);
        try {
            return serviceNode.getBoolean("clientLocal", true);
        } catch (Exception e) {
            System.out.println("Error: " + e.getMessage());
        }
        return false;
    }

    /**
     * @param Set client side execution flag
     */
    public void setTCoffeeClientLocal(boolean b) {
        Preferences serviceNode = Preferences.userRoot().node(TCOFFEE);
        try {
            serviceNode.putBoolean("clientLocal", b);
        } catch (Exception e) {
            System.out.println("Error: " + e.getMessage());
        }
    }

    /**
     * Get the path to client side tcoffee
     * 
     * @return
     */
    public String getTCoffeeLocalPath() {
        Preferences serviceNode = Preferences.userRoot().node(TCOFFEE);
        try {
            return serviceNode.get("localPath", "");
        } catch (Exception e) {
            System.out.println("Error: " + e.getMessage());
            e.printStackTrace();
        }
        return "";
    }

    public void setTCoffeeLocalPath(String path) {
        Preferences serviceNode = Preferences.userRoot().node(TCOFFEE);
        try {
            serviceNode.put("localPath", path);
        } catch (Exception e) {
            System.out.println("Error: " + e.getMessage());
        }

    }

    // *** ------------------- Needle ---------------------------***

    /**
     *
     * @return Returns the needle matrix
     */
    public String getNeedleMatrix() {
        Preferences serviceNode = Preferences.userRoot().node(NEEDLE);
        try {
            return serviceNode.get("matrix", "EBLOSUM65");
        } catch (Exception e) {
            System.out.println("Error:" + e.getMessage());
        }
        return "EBLOSUM 65";
    }

    /**
     * Sets the needle matrix
     * 
     * @param inMatrix matrix to set
     */
    public void setNeedleMatrix(String inMatrix) {
        Preferences serviceNode = Preferences.userRoot().node(NEEDLE);
        try {
            serviceNode.put("matrix", inMatrix);
        } catch (Exception e) {
            System.out.println("Error:" + e.getMessage());
        }
    }

    /**
     * Gets the gap open value for needle
     * 
     * @return
     */
    public float getNeedleGapOpen() {
        Preferences serviceNode = Preferences.userRoot().node(NEEDLE);
        try {
            return serviceNode.getFloat("gapOpen", 10.0f);
        } catch (Exception e) {
            System.out.println("Error:" + e.getMessage());
        }
        return 10.0f; // the default
    }

    public void setNeedleGapOpen(float inGapOpen) {
        Preferences serviceNode = Preferences.userRoot().node(NEEDLE);
        try {
            serviceNode.putFloat("gapOpen", inGapOpen);
        } catch (Exception e) {
            System.out.println("Erorr:" + e.getMessage());
        }
    }

    /**
     * Gets the gap-extend value for needle
     * 
     * @return gap extend value for needle
     */
    public float getNeedleGapExtend() {
        Preferences serviceNode = Preferences.userRoot().node(NEEDLE);
        try {
            return serviceNode.getFloat("gapExtend", 0.05f);
        } catch (Exception e) {
            System.out.println("Error:" + e.getMessage());
        }
        return 0.05f;
    }

    /**
     * Sets the gap extend value for need
     * 
     * @param inGapExtend gap extend value to use
     */
    public void setNeedleGapExtend(float inGapExtend) {
        Preferences serviceNode = Preferences.userRoot().node(NEEDLE);
        try {
            serviceNode.putFloat("gapExtend", inGapExtend);
        } catch (Exception e) {
            System.out.println("Error:" + e.getMessage());
        }
    }

    /**
     * Gets whether to show brief identity and similarity
     * 
     * @return true or false
     */
    public boolean getNeedleShowSimilarity() {
        Preferences serviceNode = Preferences.userRoot().node(NEEDLE);
        try {
            return serviceNode.getBoolean("showSimilarity", true);
        } catch (Exception e) {
            System.out.println("Error:" + e.getMessage());
        }
        return true;
    }

    /**
     * Sets whether to show brief identity and similarity for needle
     * 
     * @param inShowSimilarity true or false
     */
    public void setNeedleShowSimilarity(boolean inShowSimilarity) {
        Preferences serviceNode = Preferences.userRoot().node(NEEDLE);
        try {
            serviceNode.putBoolean("showSimilarity", inShowSimilarity);
        } catch (Exception e) {
            System.out.println("Error:" + e.getMessage());
        }
    }

    /**
     * Gets the aligned format type for needle
     * 
     * @return the aligned format type
     */
    public String getNeedleAlignmentFormat() {
        Preferences serviceNode = Preferences.userRoot().node(NEEDLE);
        try {
            return serviceNode.get("alignmentFormat", "srspair");
        } catch (Exception e) {
            System.out.println("Error:" + e.getMessage());
        }
        return "srspair";
    }

    /**
     * Sets needle's format
     * 
     * @param inFormat format to use
     */
    public void setNeedleAlignmentFormat(String inFormat) {
        Preferences serviceNode = Preferences.userRoot().node(NEEDLE);
        try {
            serviceNode.put("alignmentFormat", inFormat);
        } catch (Exception e) {
            System.out.println("Error:" + e.getMessage());
        }

    }

    /**
     * @return Returns the default sge behavior.
     */
    public boolean getNeedleSGE() {
        Preferences serviceNode = Preferences.userRoot().node(NEEDLE);
        try {
            return serviceNode.getBoolean("sge", false);
        } catch (Exception e) {
            System.out.println("Error: " + e.getMessage());
        }
        return false;
    }

    /**
     * @param Set sge flag
     */
    public void setNeedleSGE(boolean b) {
        Preferences serviceNode = Preferences.userRoot().node(NEEDLE);
        try {
            serviceNode.putBoolean("sge", b);
        } catch (Exception e) {
            System.out.println("Error: " + e.getMessage());
        }
    }

    /**
     * @return Returns the default client side execution behavior.
     */
    public boolean getNeedleClientLocal() {
        Preferences serviceNode = Preferences.userRoot().node(NEEDLE);
        try {
            return serviceNode.getBoolean("clientLocal", false);
        } catch (Exception e) {
            System.out.println("Error: " + e.getMessage());
        }
        return false;
    }

    /**
     * @param Set client side execution flag
     */
    public void setNeedleClientLocal(boolean b) {
        Preferences serviceNode = Preferences.userRoot().node(NEEDLE);
        try {
            serviceNode.putBoolean("clientLocal", b);
        } catch (Exception e) {
            System.out.println("Error: " + e.getMessage());
        }
    }

    /**
     * Get the path to client side needle
     * 
     * @return
     */
    public String getNeedleLocalPath() {
        Preferences serviceNode = Preferences.userRoot().node(NEEDLE);
        try {
            return serviceNode.get("localPath", "");
        } catch (Exception e) {
            System.out.println("Error: " + e.getMessage());
            e.printStackTrace();
        }
        return "";
    }

    public void setNeedleLocalPath(String path) {
        Preferences serviceNode = Preferences.userRoot().node(NEEDLE);
        try {
            serviceNode.put("localPath", path);
        } catch (Exception e) {
            System.out.println("Error: " + e.getMessage());
        }

    }

    /**
     * Get the path to client side matrices
     * 
     * @return
     */
    public String getNeedleLocalMatricesPath() {
        Preferences serviceNode = Preferences.userRoot().node(NEEDLE);
        try {
            return serviceNode.get("localMatricesPath", "");
        } catch (Exception e) {
            System.out.println("Error: " + e.getMessage());
            e.printStackTrace();
        }
        return "";
    }

    public void setNeedleLocalMatricesPath(String path) {
        Preferences serviceNode = Preferences.userRoot().node(NEEDLE);
        try {
            serviceNode.put("localMatricesPath", path);
        } catch (Exception e) {
            System.out.println("Error: " + e.getMessage());
        }

    }

    // ****------------------- NoptAlign ---------------------------------

    /**
     *
     * @return Returns the generate all optical alignment setting (true/false).
     */
    public boolean isNoptAlignDNAAll() {
        Preferences serviceNode = Preferences.userRoot().node(NOPTALIGN);
        try {
            return serviceNode.getBoolean("DNAAll", false);
        } catch (Exception e) {
            System.out.println("Error: " + e.getMessage());
        }
        return false;
    }

    /**
     * @param NoptAlignAll The NoptAlignAll to set.
     */
    public void setNoptAlignDNAAll(boolean NoptAlignAll) {
        Preferences serviceNode = Preferences.userRoot().node(NOPTALIGN);
        try {
            serviceNode.putBoolean("DNAAll", NoptAlignAll);
        } catch (Exception e) {
            System.out.println("Error: " + e.getMessage());
        }

    }

    /**
     * @return Returns the NoptAlignGapExtend.
     */
    // default values extracted from noptalign pl file
    public int getNoptAlignDNAGapExtend() {
        Preferences serviceNode = Preferences.userRoot().node(NOPTALIGN);
        try {
            return serviceNode.getInt("DNAGapExtend", -2);
        } catch (Exception e) {
            System.out.println("Error: " + e.getMessage());
        }
        return -2;
    }

    /**
     * @param NoptAlignGapExtend The NoptAlignGapExtend to set.
     */
    public void setNoptAlignDNAGapExtend(int NoptAlignGapExtend) {
        Preferences serviceNode = Preferences.userRoot().node(NOPTALIGN);
        try {
            serviceNode.putInt("DNAGapExtend", NoptAlignGapExtend);
        } catch (Exception e) {
            System.out.println("Error: " + e.getMessage());
        }

    }

    /**
     * @return Returns the NoptAlignGapOpen.
     */
    public int getNoptAlignDNAGapOpen() {
        Preferences serviceNode = Preferences.userRoot().node(NOPTALIGN);
        try {
            return serviceNode.getInt("DNAGapOpen", -10);
        } catch (Exception e) {
            System.out.println("Error: " + e.getMessage());
        }
        return -10;
    }

    /**
     * @param NoptAlignGapOpen The NoptAlignGapOpen to set.
     */
    public void setNoptAlignDNAGapOpen(int NoptAlignGapOpen) {
        Preferences serviceNode = Preferences.userRoot().node(NOPTALIGN);
        try {
            serviceNode.putInt("DNAGapOpen", NoptAlignGapOpen);
        } catch (Exception e) {
            System.out.println("Error: " + e.getMessage());
        }

    }

    /**
     * @return Returns the NoptAlignLocal.
     */
    public boolean isNoptAlignDNALocal() {
        Preferences serviceNode = Preferences.userRoot().node(NOPTALIGN);
        try {
            return serviceNode.getBoolean("DNALocal", false);
        } catch (Exception e) {
            System.out.println("Error: " + e.getMessage());
        }
        return false;
    }

    /**
     * @param NoptAlignLocal The NoptAlignLocal to set.
     */
    public void setNoptAlignDNALocal(boolean NoptAlignLocal) {
        Preferences serviceNode = Preferences.userRoot().node(NOPTALIGN);
        try {
            serviceNode.putBoolean("DNALocal", NoptAlignLocal);
        } catch (Exception e) {
            System.out.println("Error: " + e.getMessage());
        }

    }

    /**
     * @return Returns the NoptAlignMatrix.
     */
    // default values extractd from noptlaign pl file.
    public String getNoptAlignDNAMatrix() {
        Preferences serviceNode = Preferences.userRoot().node(NOPTALIGN);
        try {
            return serviceNode.get("DNAMatrix", "blosum50").toLowerCase();
        } catch (Exception e) {
            System.out.println("Error: " + e.getMessage());
        }
        return "blosum50";
    }

    /**
     * @param NoptAlignMatrix The NoptAlignMatrix to set.
     */
    public void setNoptAlignDNAMatrix(String NoptAlignMatrix) {
        Preferences serviceNode = Preferences.userRoot().node(NOPTALIGN);
        try {
            serviceNode.put("DNAMatrix", NoptAlignMatrix);
        } catch (Exception e) {
            System.out.println("Error: " + e.getMessage());
        }
    }

    /**
     *
     * @return Returns the generate all optical alignment setting (true/false).
     */
    public boolean isNoptAlignProteinAll() {
        Preferences serviceNode = Preferences.userRoot().node(NOPTALIGN);
        try {
            return serviceNode.getBoolean("ProteinAll", false);
        } catch (Exception e) {
            System.out.println("Error: " + e.getMessage());
        }
        return false;
    }

    /**
     * @param NoptAlignAll The NoptAlignAll to set.
     */
    public void setNoptAlignProteinAll(boolean NoptAlignAll) {
        Preferences serviceNode = Preferences.userRoot().node(NOPTALIGN);
        try {
            serviceNode.putBoolean("ProteinAll", NoptAlignAll);
        } catch (Exception e) {
            System.out.println("Error: " + e.getMessage());
        }

    }

    /**
     * @return Returns the NoptAlignGapExtend.
     */
    public int getNoptAlignProteinGapExtend() {
        Preferences serviceNode = Preferences.userRoot().node(NOPTALIGN);
        try {
            return serviceNode.getInt("ProteinGapExtend", -2);
        } catch (Exception e) {
            System.out.println("Error: " + e.getMessage());
        }
        return -2;
    }

    /**
     * @param NoptAlignGapExtend The NoptAlignGapExtend to set.
     */
    public void setNoptAlignProteinGapExtend(int NoptAlignGapExtend) {
        Preferences serviceNode = Preferences.userRoot().node(NOPTALIGN);
        try {
            serviceNode.putInt("ProteinGapExtend", NoptAlignGapExtend);
        } catch (Exception e) {
            System.out.println("Error: " + e.getMessage());
        }

    }

    /**
     * @return Returns the NoptAlignGapOpen.
     */
    public int getNoptAlignProteinGapOpen() {
        Preferences serviceNode = Preferences.userRoot().node(NOPTALIGN);
        try {
            return serviceNode.getInt("ProteinGapOpen", -10);
        } catch (Exception e) {
            System.out.println("Error: " + e.getMessage());
        }
        return -10;
    }

    /**
     * @param NoptAlignGapOpen The NoptAlignGapOpen to set.
     */
    public void setNoptAlignProteinGapOpen(int NoptAlignGapOpen) {
        Preferences serviceNode = Preferences.userRoot().node(NOPTALIGN);
        try {
            serviceNode.putInt("ProteinGapOpen", NoptAlignGapOpen);
        } catch (Exception e) {
            System.out.println("Error: " + e.getMessage());
        }

    }

    /**
     * @return Returns the NoptAlignLocal.
     */
    public boolean isNoptAlignProteinLocal() {
        Preferences serviceNode = Preferences.userRoot().node(NOPTALIGN);
        try {
            return serviceNode.getBoolean("ProteinLocal", false);
        } catch (Exception e) {
            System.out.println("Error: " + e.getMessage());
        }
        return false;
    }

    /**
     * @param NoptAlignLocal The NoptAlignLocal to set.
     */
    public void setNoptAlignProteinLocal(boolean NoptAlignLocal) {
        Preferences serviceNode = Preferences.userRoot().node(NOPTALIGN);
        try {
            serviceNode.putBoolean("ProteinLocal", NoptAlignLocal);
        } catch (Exception e) {
            System.out.println("Error: " + e.getMessage());
        }

    }

    /**
     * @return Returns the NoptAlignMatrix.
     */
    public String getNoptAlignProteinMatrix() {
        Preferences serviceNode = Preferences.userRoot().node(NOPTALIGN);
        try {
            return serviceNode.get("ProteinMatrix", "blosum50").toLowerCase();
        } catch (Exception e) {
            System.out.println("Error: " + e.getMessage());
        }
        return "blosum50";
    }

    /**
     * @param NoptAlignMatrix The NoptAlignMatrix to set.
     */
    public void setNoptAlignProteinMatrix(String NoptAlignMatrix) {
        Preferences serviceNode = Preferences.userRoot().node(NOPTALIGN);
        try {
            serviceNode.put("ProteinMatrix", NoptAlignMatrix);
        } catch (Exception e) {
            System.out.println("Error: " + e.getMessage());
        }
    }

    /**
     *
     * @return Returns the nAPMismatch penalty
     */
    public int getNAPMismatch() {
        Preferences serviceNode = Preferences.userRoot().node(NAP);
        try {
            return serviceNode.getInt("mismatch", -1); // negative since mismatch penalty must be negative
        } catch (Exception e) {
            System.out.println("Error: " + e.getMessage());
        }
        return -1;
    }

    /**
     * @return Returns the nAPGapExtension.
     */
    public int getNAPGapExtension() {
        Preferences serviceNode = Preferences.userRoot().node(NAP);
        try {
            return serviceNode.getInt("gapExtension", 3);
        } catch (Exception e) {
            System.out.println("Error: " + e.getMessage());
        }
        return 3;
    }

    /**
     * @param gapExtension The nAPGapExtension to set.
     */
    public void setNAPGapExtension(int gapExtension) {
        Preferences serviceNode = Preferences.userRoot().node(NAP);
        try {
            serviceNode.putInt("gapExtension", gapExtension);
        } catch (Exception e) {
            System.out.println("Error: " + e.getMessage());
        }
    }

    /**
     * @param gapExtension The nAPGapExtension to set.
     */
    public void setNAPMismatch(int mismatch) {
        Preferences serviceNode = Preferences.userRoot().node(NAP);
        try {
            serviceNode.putInt("mismatch", mismatch);
        } catch (Exception e) {
            System.out.println("Error: " + e.getMessage());
        }
    }

    /**
     * @return Returns the nAPGapOpen.
     */
    public int getNAPGapOpen() {
        Preferences serviceNode = Preferences.userRoot().node(NAP);
        try {
            return serviceNode.getInt("gapOpen", 3);
        } catch (Exception e) {
            System.out.println("Error: " + e.getMessage());
        }
        return 3;
    }

    /**
     * @param gapOpen The nAPGapOpen to set.
     */
    public void setNAPGapOpen(int gapOpen) {
        Preferences serviceNode = Preferences.userRoot().node(NAP);
        try {
            serviceNode.putInt("gapOpen", gapOpen);
        } catch (Exception e) {
            System.out.println("Error: " + e.getMessage());
        }
    }

    /**
     * @return Returns the nAPGapSize.
     */
    public int getNAPGapSize() {
        Preferences serviceNode = Preferences.userRoot().node(NAP);
        try {
            int value = serviceNode.getInt("gapSize", 6);
            return value;
        } catch (Exception e) {
            System.out.println("Error: " + e.getMessage());
        }
        return 6;
    }

    /**
     * @param gapSize The NAPGapSize to set.
     */
    public void setNAPGapSize(int gapSize) {
        Preferences serviceNode = Preferences.userRoot().node(NAP);
        try {
            serviceNode.putInt("gapSize", gapSize);
        } catch (Exception e) {
            System.out.println("Error: " + e.getMessage());
        }
    }

    /**
     * @return Returns the NAPMatrix.
     */
    public String getNAPMatrix() {
        Preferences serviceNode = Preferences.userRoot().node(NAP);
        try {
            return serviceNode.get("matrix", "PAM250");
        } catch (Exception e) {
            System.out.println("Error: " + e.getMessage());
        }
        return "PAM250";
    }

    /**
     * @param matrix The nAPMatrix to set.
     */
    public void setNAPMatrix(String matrix) {
        Preferences serviceNode = Preferences.userRoot().node(NAP);
        try {
            serviceNode.put("wordSize", matrix);
        } catch (Exception e) {
            System.out.println("Error: " + e.getMessage());
        }
    }

    /**
     * @return Returns the default sge behavior.
     */
    public boolean getNAPSGE() {
        Preferences serviceNode = Preferences.userRoot().node(NAP);
        try {
            return serviceNode.getBoolean("sge", false);
        } catch (Exception e) {
            System.out.println("Error: " + e.getMessage());
        }
        return false;
    }

    /**
     * @param Set sge flag
     */
    public void setNAPSGE(boolean b) {
        Preferences serviceNode = Preferences.userRoot().node(NAP);
        try {
            serviceNode.putBoolean("sge", b);
        } catch (Exception e) {
            System.out.println("Error: " + e.getMessage());
        }
    }

    /**
     * @return Returns the default local execution behavior.
     */
    public boolean getNAPLocal() {
        Preferences serviceNode = Preferences.userRoot().node(NAP);
        try {
            return serviceNode.getBoolean("local", false);
        } catch (Exception e) {
            System.out.println("Error: " + e.getMessage());
        }
        return false;
    }

    /**
     * @param Set the default local execution behavior
     */
    public void setNAPLocal(boolean b) {
        Preferences serviceNode = Preferences.userRoot().node(NAP);
        try {
            serviceNode.putBoolean("local", b);
        } catch (Exception e) {
            System.out.println("Error: " + e.getMessage());
        }
    }

    /**
     * @return Returns the NAP local execution path
     */
    public String getNAPLocalExecution() {
        Preferences serviceNode = Preferences.userRoot().node(NAP);
        try {
            return serviceNode.get("localpath", "");
        } catch (Exception e) {
            System.out.println("Error: " + e.getMessage());
        }
        return "";
    }

    /**
     * @param path - the NAP local execution path
     */
    public void setNAPLocalExecution(String path) {
        Preferences serviceNode = Preferences.userRoot().node(NAP);
        try {
            serviceNode.put("localpath", path);
        } catch (Exception e) {
            System.out.println("Error: " + e.getMessage());
        }
    }

    /**
     * @return Returns the mViewBold.
     */
    public boolean isMViewBold() {
        Preferences serviceNode = Preferences.userRoot().node(MVIEW);
        try {
            return serviceNode.getBoolean("bold", false);
        } catch (Exception e) {
            System.out.println("Error: " + e.getMessage());
        }
        return false;
    }

    /**
     * @param viewBold The mViewBold to set.
     */
    public void setMViewBold(boolean viewBold) {
        Preferences serviceNode = Preferences.userRoot().node(MVIEW);
        try {
            serviceNode.putBoolean("bold", viewBold);
        } catch (Exception e) {
            System.out.println("Error: " + e.getMessage());
        }
    }

    /**
     * @return Returns the mViewColoring.
     */
    public String getMViewColoring() {
        Preferences serviceNode = Preferences.userRoot().node(MVIEW);
        try {
            return serviceNode.get("coloring", "none");
        } catch (Exception e) {
            System.out.println("Error: " + e.getMessage());
        }
        return "none";
    }

    /**
     * @param viewColoring The mViewColoring to set.
     */
    public void setMViewColoring(String viewColoring) {
        Preferences serviceNode = Preferences.userRoot().node(MVIEW);
        try {
            serviceNode.put("coloring", viewColoring);
        } catch (Exception e) {
            System.out.println("Error: " + e.getMessage());
        }
    }

    /**
     * @return Returns the mViewConGaps.
     */
    public boolean isMViewConGaps() {
        Preferences serviceNode = Preferences.userRoot().node(MVIEW);
        try {
            return serviceNode.getBoolean("conGaps", true);
        } catch (Exception e) {
            System.out.println("Error: " + e.getMessage());
        }
        return true;
    }

    /**
     * @param viewConGaps The mViewConGaps to set.
     */
    public void setMViewConGaps(boolean viewConGaps) {
        Preferences serviceNode = Preferences.userRoot().node(MVIEW);
        try {
            serviceNode.putBoolean("conGaps", viewConGaps);
        } catch (Exception e) {
            System.out.println("Error: " + e.getMessage());
        }
    }

    /**
     * @return Returns the mViewConsensus.
     */
    public boolean isMViewConsensus() {
        Preferences serviceNode = Preferences.userRoot().node(MVIEW);
        try {
            return serviceNode.getBoolean("consensus", false);
        } catch (Exception e) {
            System.out.println("Error: " + e.getMessage());
        }
        return false;
    }

    /**
     * @param viewConsensus The mViewConsensus to set.
     */
    public void setMViewConsensus(boolean viewConsensus) {
        Preferences serviceNode = Preferences.userRoot().node(MVIEW);
        try {
            serviceNode.putBoolean("consensus", viewConsensus);
        } catch (Exception e) {
            System.out.println("Error: " + e.getMessage());
        }
    }

    /**
     * @return Returns the mViewMaxIdent.
     */
    public float getMViewMaxIdent() {
        Preferences serviceNode = Preferences.userRoot().node(MVIEW);
        try {
            return serviceNode.getFloat("maxIdent", 100);
        } catch (Exception e) {
            System.out.println("Error: " + e.getMessage());
        }
        return 100;
    }

    /**
     * @param viewMaxIdent The mViewMaxIdent to set.
     */
    public void setMViewMaxIdent(float viewMaxIdent) {
        Preferences serviceNode = Preferences.userRoot().node(MVIEW);
        try {
            serviceNode.putFloat("maxIdent", viewMaxIdent);
        } catch (Exception e) {
            System.out.println("Error: " + e.getMessage());
        }
    }

    /**
     * @return Returns the mViewRuler.
     */
    public boolean isMViewRuler() {
        Preferences serviceNode = Preferences.userRoot().node(MVIEW);
        try {
            return serviceNode.getBoolean("ruler", false);
        } catch (Exception e) {
            System.out.println("Error: " + e.getMessage());
        }
        return false;
    }

    /**
     * @param viewRuler The mViewRuler to set.
     */
    public void setMViewRuler(boolean ruler) {
        Preferences serviceNode = Preferences.userRoot().node(MVIEW);
        try {
            serviceNode.putBoolean("ruler", ruler);
        } catch (Exception e) {
            System.out.println("Error: " + e.getMessage());
        }
    }

    /**
     * @return Returns the mViewTop.
     */
    public int getMViewTop() {
        Preferences serviceNode = Preferences.userRoot().node(MVIEW);
        try {
            return serviceNode.getInt("top", -1); // note minus one is to be interpreted as "all"
        } catch (Exception e) {
            System.out.println("Error: " + e.getMessage());
        }
        return -1;
    }

    /**
     * @param viewTop The mViewTop to set.
     */
    public void setMViewTop(int top) {
        Preferences serviceNode = Preferences.userRoot().node(MVIEW);
        try {
            serviceNode.putInt("top", top);
        } catch (Exception e) {
            System.out.println("Error: " + e.getMessage());
        }
    }

    // ***------------------- Genemark ---------------------------------
    /**
     * @param viewTop The Genemark model to set.
     */
    public void setGenemarkThreshold(double threshold) {
        Preferences serviceNode = Preferences.userRoot().node(GENEMARK);
        try {
            serviceNode.putDouble("threshold", threshold);
        } catch (Exception e) {
            System.out.println("Error: " + e.getMessage());
        }
    }

    /**
     * @return Returns the Genemark model.
     */
    public double getGenemarkThreshold() {
        Preferences serviceNode = Preferences.userRoot().node(GENEMARK);
        try {
            return serviceNode.getDouble("threshold", -1); // note minus one is to be interpreted as "all"
        } catch (Exception e) {
            System.out.println("Error: " + e.getMessage());
        }
        return -1;
    }

    /**
     * @return Returns the default sge behavior.
     */
    public boolean getGenemarkSGE() {
        Preferences serviceNode = Preferences.userRoot().node(GENEMARK);
        try {
            return serviceNode.getBoolean("sge", false);
        } catch (Exception e) {
            System.out.println("Error: " + e.getMessage());
        }
        return false;
    }

    /**
     * @param Set sge flag
     */
    public void setGenemarkSGE(boolean b) {
        Preferences serviceNode = Preferences.userRoot().node(GENEMARK);
        try {
            serviceNode.putBoolean("sge", b);
        } catch (Exception e) {
            System.out.println("Error: " + e.getMessage());
        }
    }

    // ****------------------- RPSBlast ---------------------------------

    /**
     * @param expect The RPSBlast Expect value
     */
    public void setRPSBlastExpect(double expect) {
        Preferences serviceNode = Preferences.userRoot().node(RPSBLAST);
        try {
            serviceNode.putDouble("expect", expect);
        } catch (Exception e) {
            System.out.println("Error: " + e.getMessage());
        }
    }

    /**
     * @return Returns the RPSBlast Expect value.
     */
    public double getRPSBlastExpect() {
        Preferences serviceNode = Preferences.userRoot().node(RPSBLAST);
        try {
            return serviceNode.getDouble("expect", -1); // note minus one is to be interpreted as "all"
        } catch (Exception e) {
            System.out.println("Error: " + e.getMessage());
        }
        return -1;
    }

    /**
     * @param database The RPSBlast Database to set.
     */
    public void setRPSBlastDatabase(String database) {
        Preferences serviceNode = Preferences.userRoot().node(RPSBLAST);
        try {
            serviceNode.put("database", database);
        } catch (Exception e) {
            System.out.println("Error: " + e.getMessage());
        }
    }

    /**
     * @return the RPSBlast Database.
     */
    public String getRPSBlastDatabase() {
        Preferences serviceNode = Preferences.userRoot().node(RPSBLAST);
        try {
            return serviceNode.get("database", "-1"); // note minus one is to be interpreted as "all"
        } catch (Exception e) {
            System.out.println("Error: " + e.getMessage());
        }
        return "-1";
    }

    /**
     * @param viewTop The RPSBlast Database path to set.
     */
    public void setRPSBlastDatabasePath(String dbpath) {
        Preferences serviceNode = Preferences.userRoot().node(RPSBLAST);
        try {
            serviceNode.put("dbpath", dbpath);
        } catch (Exception e) {
            System.out.println("Error: " + e.getMessage());
        }
    }

    /**
     * @return Returns the RPSBlast Database path.
     */
    public String getRPSBlastDatabasePath() {
        Preferences serviceNode = Preferences.userRoot().node(RPSBLAST);
        try {
            return serviceNode.get("dbpath", "-1"); // note minus one is to be interpreted as "all"
        } catch (Exception e) {
            System.out.println("Error: " + e.getMessage());
        }
        return "-1";
    }

    /**
     * @param viewTop The RPSBlast infile to set.
     */
    public void setRPSBlastInFile(String inFile) {
        Preferences serviceNode = Preferences.userRoot().node(RPSBLAST);
        try {
            serviceNode.put("infile", inFile);
        } catch (Exception e) {
            System.out.println("Error: " + e.getMessage());
        }
    }

    /**
     * @return Returns the RPSBlast infile.
     */
    public String getRPSBlastInFile() {
        Preferences serviceNode = Preferences.userRoot().node(RPSBLAST);
        try {
            return serviceNode.get("infile", "-1"); // note minus one is to be interpreted as "all"
        } catch (Exception e) {
            System.out.println("Error: " + e.getMessage());
        }
        return "-1";
    }

    /**
     * @param viewTop The RPSBlast outfile to set.
     */
    public void setRPSBlastOutFile(String outFile) {
        Preferences serviceNode = Preferences.userRoot().node(RPSBLAST);
        try {
            serviceNode.put("outfile", outFile);
        } catch (Exception e) {
            System.out.println("Error: " + e.getMessage());
        }
    }

    /**
     * @return Returns the RPSBlast outfile.
     */
    public String getRPSBlastOutFile() {
        Preferences serviceNode = Preferences.userRoot().node(RPSBLAST);
        try {
            return serviceNode.get("outfile", "-1"); // note minus one is to be interpreted as "all"
        } catch (Exception e) {
            System.out.println("Error: " + e.getMessage());
        }
        return "-1";
    }

    /**
     * @return Returns the default sge behavior.
     */
    public boolean getRPSBlastSGE() {
        Preferences serviceNode = Preferences.userRoot().node(RPSBLAST);
        try {
            return serviceNode.getBoolean("sge", false);
        } catch (Exception e) {
            System.out.println("Error: " + e.getMessage());
        }
        return false;
    }

    /**
     * @param Set sge flag
     */
    public void setRPSBlastSGE(boolean b) {
        Preferences serviceNode = Preferences.userRoot().node(RPSBLAST);
        try {
            serviceNode.putBoolean("sge", b);
        } catch (Exception e) {
            System.out.println("Error: " + e.getMessage());
        }
    }

}
