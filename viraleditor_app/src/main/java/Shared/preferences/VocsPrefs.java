package Shared.preferences;

/*
 * VocsPrefs: Retrieves client settings from a specified xml file
 * Copyright (C) 2004  Dr. Chris Upton University of Victoria
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */

import java.util.prefs.*;

/**
 * This class of methods is used to provide vocs client with access to
 * preferences/configurations
 * 
 * @author Sameer Girn
 */
public class VocsPrefs {

    private static final String VOCS_TABLE = "/ca/virology/vocs/vocs_table";
    private static final String VOCS_GENERAL = "/ca/virology/vocs/general";

    protected static VocsPrefs myinstance = null;

    /**
     * Constructor which sets the xml file location and imports the preferences
     * 
     * @param xmlFileLoc - the location of the xml file containing all the
     *                   preferences
     */
    public VocsPrefs(String xmlFileLoc) {

        final String XML_LOC = xmlFileLoc;
        myinstance = this;

        // import new preferences if they don't already exist
        // note: client preferences should never be overwritten
        PrefsTools pt = new PrefsTools();
        pt.importPrefs(XML_LOC);
    }

    /**
     * Gets the instance of the attribute of the VocsPrefs class
     * 
     * @return The instance value
     */
    public static VocsPrefs getInstance() {
        return myinstance;
    }

    /**
     * return vocs table preferences
     * 
     * @param node - the database key name
     * @param key  - choose from: display, width, name, or percent
     * @return preference
     */
    public String get_vocs_table(String node, String key) {
        Preferences vocsNodes = Preferences.userRoot().node(VOCS_TABLE);
        try {
            String children[] = vocsNodes.childrenNames();
            for (int i = 0; i < children.length; i++) {
                if (children[i].equals(node)) {
                    Preferences prefs = vocsNodes.node(children[i]);
                    return prefs.get(key, "");
                }
            }
        } catch (Exception e) {
            System.out.println("Error: " + e.getMessage());
            e.printStackTrace();
        }
        return "";
    }

    /**
     * set vocs table preferences
     * 
     * @param node  - the database key name
     * @param key   - choose from: display, width, name, or percent
     * @param value - the value to set
     */
    public void set_vocs_table(String node, String key, String value) {
        Preferences vocsNodes = Preferences.userRoot().node(VOCS_TABLE);
        try {
            String children[] = vocsNodes.childrenNames();
            for (int i = 0; i < children.length; i++) {
                if (children[i].equals(node)) {
                    Preferences prefs = vocsNodes.node(children[i]);
                    prefs.put(key, value);
                    prefs.flush();
                }
            }
        } catch (Exception e) {
            System.out.println("Error: " + e.getMessage());
            e.printStackTrace();
        }
    }

    /**
     * return general vocs preferences
     * 
     * @param key - get the value associated with this key
     * @return preference
     */
    public String get_general(String key) {
        Preferences vocsNode = Preferences.userRoot().node(VOCS_GENERAL);
        try {
            return vocsNode.get(key, "");
        } catch (Exception e) {
            System.out.println("Error: " + e.getMessage());
            e.printStackTrace();
        }
        return "";
    }

    /**
     * set general vocs preferences
     * 
     * @param key   - set the value associated with this key
     * @param value - the value to set
     * @return preference
     */
    public void set_general(String key, String value) {
        Preferences vocsNode = Preferences.userRoot().node(VOCS_GENERAL);
        try {
            vocsNode.put(key, value);
            vocsNode.flush();
        } catch (Exception e) {
            System.out.println("Error: " + e.getMessage());
            e.printStackTrace();
        }
    }

    /**
     * Create a new node under the "vocs_table" node
     * 
     * @param nodeName - the name of the node
     * @param display  - the display value to set
     * @param width    - the column width to set
     * @param name     - the custom column name
     * @param percent  - the factor value
     * @param column   - the column number
     * @return true, if successfully created a new node, false if the specified node
     *         already exists
     */
    public boolean create_node(String nodeName, boolean display, int width, String name, int percent, int column) {
        try {
            if (Preferences.userRoot().nodeExists(VOCS_TABLE + "/" + nodeName))
                return false;
            else {
                Preferences prefs = Preferences.userRoot().node(VOCS_TABLE + "/" + nodeName);
                prefs.putBoolean("display", display);
                prefs.putInt("width", width);
                prefs.put("name", name);
                prefs.putInt("percent", percent);
                prefs.putInt("column", column);
                Preferences.userRoot().node(VOCS_TABLE + "/" + nodeName).flush();
                return true;
            }
        } catch (Exception e) {
            System.out.println("Error: " + e.getMessage());
            e.printStackTrace();
        }
        return false;
    }

}
