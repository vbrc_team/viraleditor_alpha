package Shared.preferences;
/*
 * DBPrefs: Retrieves database and server settings from a specified xml file
 */
import java.io.*;
import java.net.*;
import java.util.*;
import java.util.prefs.InvalidPreferencesFormatException;
import java.util.prefs.Preferences;
 
/**
* This class of methods is used to provide applications 
* with db and server info from a specified xml file.
* @author Sameer Girn
*/
public class DBPrefs {
	 private static final String XML_ROOT = "/ca/virology/";
	 private static final String VOCS_DB = "/ca/virology/vocsdatabases";
	 private static String PREFIX; 
	 protected static DBPrefs myinstance = null;
	 
	 /**
	  * Constructor which sets the xml file location and imports the preferences
	  * @param xmlFileLoc - the location of the xml file containing the preferences
	  */
	 public DBPrefs(String xmlFileLoc) {
	   myinstance = this;
	   PREFIX = null;
	   loadPrefs(xmlFileLoc);
	 }
	
	 /**
	  * Constructor which sets the xml file location and imports the preferences
	  * @param xmlFileLoc - the location of the xml file containing the preferences
	  */
	 public DBPrefs(String xmlFileLoc, String prefix) {
	   myinstance = this;
	   if ((prefix == null) || (prefix.length() < 1)) PREFIX = null;
	   else PREFIX = prefix;
	   loadPrefs(xmlFileLoc);
	} 
	
	/**
	 * Set the xml file location and import the preferences
	 * @param xmlFileLoc - the location of the xml file containing the preferences
	 */
	private void loadPrefs(String xmlFileLoc) { 
	   final String XML_LOC = xmlFileLoc;
	
	   try {
	     URL url = new URL(XML_LOC);
	     Preferences.importPreferences(url.openStream());
	   } catch (MalformedURLException e) {
	     System.out.println("MalformedURLException");
	 System.out.println("Error: " + e.getMessage());
	     e.printStackTrace();
	   } catch (IOException e ) {
	     System.out.println("IOException");
	 System.out.println("Error: Could not find "+e.getMessage());
	     e.printStackTrace();
	   } catch (InvalidPreferencesFormatException e) {
	     System.out.println("InvalidPreferencesFormatException");
	 System.out.println("Error: " + e.getMessage());
	     e.printStackTrace();
	   }
	 }
	 
	 /**
	  * Gets the instance of the attribute of the DBPrefs class
	  * @return The instance value
	  */
	 public static DBPrefs getInstance() {
	   return myinstance;
	 }
	 
	 /**
	  * Given the database name, determine whether the virus is circular
	  * @param dbname - the database name
	  * @return isCircular
	  */
	 public boolean isCircular(String dbname) {
	   Preferences vocsNodes;
	
	   if (PREFIX == null) vocsNodes = Preferences.userRoot().node(VOCS_DB);
	   else vocsNodes = Preferences.userRoot().node("/" + PREFIX + "/" + VOCS_DB);
	   try {
	     String children[] = vocsNodes.childrenNames();
	     for (int i = 0; i < children.length; i++) {
	       Preferences prefs = vocsNodes.node(children[i]);
	       if (prefs.get("database", "").equals(dbname) ) {
	     return prefs.getBoolean("isCircular", false);
	       }
	     }
	   } catch (Exception e) {
	     System.out.println("Error: "+e.getMessage());
	     e.printStackTrace();
	   }
	   return false;  //default value is false
	 }
	
}
