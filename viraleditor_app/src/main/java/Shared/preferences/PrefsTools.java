package Shared.preferences;

import java.util.*;
import java.util.regex.*;
import java.io.*;
import java.net.*;
import java.util.prefs.*;
/*
 * PrefsTools: Provides common utility functions for the prefs classes
 * Copyright (C) 2004  Dr. Chris Upton University of Victoria
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */

/**
 * This class of methods is used to provide other prefs classes with common
 * utility functions
 * 
 * @author Sameer Girn
 */
public class PrefsTools {

    public PrefsTools() {
    }

    /**
     * Imports preferences from a specified xml file
     * 
     * @param xmlFileLoc - xml file location
     */
    public void importPrefs(String xmlFileLoc) {

        try {
            URL url = new URL(xmlFileLoc);
            Preferences.importPreferences(url.openStream());
        } catch (MalformedURLException e) {
            System.out.println("MalformedURLException");
            System.out.println("Error: " + e.getMessage());
            e.printStackTrace();

        } catch (IOException e) {
            System.out.println("IOException");
            System.out.println("Error: Could not find " + e.getMessage());
            e.printStackTrace();

        } catch (InvalidPreferencesFormatException e) {
            System.out.println("InvalidPreferencesFormatException");
            System.out.println("Error: " + e.getMessage());
            e.printStackTrace();
        }
    }

    /**
     * Compares the XML file with the client's prefs and adds any new prefs that the
     * client does not have
     * 
     * @param xmlFileLoc  - xml file location
     * @param versionNode - the node that contains the version number
     * @param appVersion  - the application version ie. vocsversion, jdotterversion,
     *                    etc
     */
    public void checkForNewPrefs(String xmlFileLoc, String versionNode, String appVersion) {
        // check version of the xml file first
        // if the version has changed, then add any new prefs
        try {
            URL url = new URL(xmlFileLoc);
            InputStream is = url.openStream();
            InputStreamReader isr = new InputStreamReader(is);
            BufferedReader br = new BufferedReader(isr);
            System.out.println("Checking " + xmlFileLoc + " for new preferences...");

            Pattern p = Pattern.compile(appVersion);
            String tempLine = br.readLine();
            while (tempLine != null) {
                Matcher m = p.matcher(tempLine);
                if (m.find()) {
                    double num;
                    // read tempLine and get value of vocsversion
                    Matcher matcher = Pattern.compile("(.*)value=\"(.*)\"(.*)").matcher(tempLine);
                    if (matcher.lookingAt()) {
                        num = Double.parseDouble(matcher.group(2));
                        System.out.println("Prefs XML file version is: " + num);
                        Preferences prefs = Preferences.userRoot().node(versionNode);
                        System.out.println("Client Prefs version is: " + prefs.getDouble(appVersion, 1.0));
                        // check if client has the latest version
                        if (num > prefs.getDouble(appVersion, 1.0)) {
                            setVersion(versionNode, appVersion, num); // update the client's version number
                            br.close();
                            isr.close();
                            is.close();
                            // start from the top of the xml file
                            is = url.openStream();
                            isr = new InputStreamReader(is);
                            br = new BufferedReader(isr);
                            System.out.println("Checking for new prefs...");
                            tempLine = br.readLine();

                            // get the root node and add it to the stack
                            Matcher km = Pattern.compile("(.*)node name=\"(.*)\">").matcher(tempLine);
                            while (!km.lookingAt()) {
                                tempLine = br.readLine(); // skip lines until it finds first node
                                km = Pattern.compile("(.*)node name=\"(.*)\">").matcher(tempLine);
                            }
                            Stack<String> mystack = new Stack<>();
                            Matcher keymatch1 = Pattern.compile("(.*)node name=\"(.*)\">").matcher(tempLine);
                            String rootnode;
                            if (keymatch1.lookingAt()) {
                                rootnode = "/" + keymatch1.group(2);
                                System.out.print("Checking node: " + rootnode + "...");
                                mystack.push(rootnode);
                                if (checkNode(rootnode))
                                    System.out.println("YES");
                                else {
                                    System.out.println("NO\nAdding node: " + rootnode);
                                    addNode(rootnode);
                                }
                            }
                            tempLine = br.readLine();
                            while (!mystack.empty()) {
                                // scan each line and look for the node name
                                Matcher nodematch = Pattern.compile("(.*)node name=\"(.*)\">").matcher(tempLine);
                                String nodename;
                                if (nodematch.lookingAt()) {
                                    nodename = nodematch.group(2);
                                    mystack.push(mystack.peek() + "/" + nodename);
                                    System.out.print("Checking node: " + mystack.peek() + "...");
                                    if (checkNode(mystack.peek().toString()))
                                        System.out.println("YES");
                                    else {
                                        System.out.println("NO\nAdding node: " + mystack.peek());
                                        addNode(mystack.peek().toString());
                                    }
                                }
                                // look for a key name and value
                                Matcher keymatch = Pattern.compile("(.*)key=\"(.*)\" value=\"(.*)\"").matcher(tempLine);
                                String keyname;
                                String valuename;
                                if (keymatch.lookingAt()) {
                                    keyname = keymatch.group(2); // get string after "key="
                                    valuename = keymatch.group(3); // get string after "value="
                                    System.out.print("\tChecking key: " + keyname + "...");
                                    if (checkKey(mystack.peek().toString(), keyname))
                                        System.out.println("YES");
                                    else {
                                        System.out.println("NO");
                                        addKey(mystack.peek().toString(), keyname, valuename);
                                    }
                                }

                                Matcher endmatch = Pattern.compile("(.*)</node>(.*)").matcher(tempLine);
                                if (endmatch.lookingAt()) {
                                    mystack.pop(); // if we see </node> pop a node name
                                }
                                tempLine = br.readLine();
                            }
                            System.out.println("Finished adding new prefs");
                        } else {
                            System.out.println("Client has the latest version of the prefs.");
                        }
                        break;
                    }
                }
                tempLine = br.readLine();
            }
            br.close();
            isr.close();
            is.close();
        } catch (Exception e) {
            System.out.println(e.getMessage());
            e.printStackTrace();
        }
    }

    /**
     * Sets the new preferences version for the client
     * 
     * @param versionNode - the location of the version number
     * @param appVersion  - the name of the key to update
     * @param version     - the version number to assign
     */
    private void setVersion(String versionNode, String appVersion, double version) {
        Preferences myNode = Preferences.userRoot().node(versionNode);
        myNode.putDouble(appVersion, version);
        try {
            myNode.flush();
        } catch (BackingStoreException e) {
            System.out.println(e.getMessage());
            e.printStackTrace();
        }
    }

    /**
     * Checks if the client has the node
     * 
     * @param nodePath - the node to check
     * @return true if the node exists, false otherwise
     */
    private boolean checkNode(String nodePath) {

        try {
            if (Preferences.userRoot().nodeExists(nodePath))
                return true;
            else
                return false;
        } catch (BackingStoreException e) {
        }
        return false;
    }

    /**
     * check if a key exists in the client's preferences
     * 
     * @param nodePath - check under this node
     * @param key      - check this key name
     * @return true if it exists, false otherwise
     */
    private boolean checkKey(String nodePath, String key) {
        String[] clientkeys;
        try {
            if (Preferences.userRoot().nodeExists(nodePath)) {
                Preferences mynode = Preferences.userRoot().node(nodePath);
                clientkeys = mynode.keys();
                if (clientkeys.length != 0) { // there are keys under the nodePath
                    for (int i = 0; i < clientkeys.length; i++) {
                        if (clientkeys[i].equals(key))
                            return true;
                    }
                }
            }
        } catch (BackingStoreException e) {
        }
        return false;
    }

    /**
     * Adds a key to the client's preferences
     * 
     * @param nodePath - the node to add it under
     * @param key      - the key name to add
     * @param value    - the value of the key
     */
    private void addKey(String nodePath, String key, String value) {
        Preferences mynode = Preferences.userRoot().node(nodePath);
        mynode.put(key, value);
        try {
            mynode.flush();
        } catch (BackingStoreException e) {
            System.out.println(e.getMessage());
            e.printStackTrace();
        }
        System.out.println("Added key: " + key + " value: " + value);
    }

    /**
     * Adds a new node to the client
     * 
     * @param nodePath - the node to add
     */
    private void addNode(String nodePath) {
        Preferences prefs = Preferences.userRoot().node(nodePath);
        try {
            prefs.flush();
        } catch (BackingStoreException e) {
            System.out.println(e.getMessage());
            e.printStackTrace();
        }
    }

    /**
     * Removes preferences from the client's machine (FOR TESTING ONLY)
     * 
     * @param nodePath - the node to remove (if it exists)
     */
    public void removePrefs(String nodePath) {

        try {
            if (Preferences.userRoot().nodeExists(nodePath)) {
                Preferences prefs = Preferences.userRoot().node(nodePath);
                prefs.removeNode();
                prefs.flush();
                System.out.println("Removed " + nodePath);
            } else
                System.out.println("Can't remove " + nodePath + " because it doesn't exist");
        } catch (BackingStoreException e) {
        }
    }

}
