package Shared.preferences;

import java.io.*;
import java.net.*;
import java.util.prefs.*;

/*
 * AppClientPrefs: Retrieves application client info
 * Copyright (C) 2004  Dr. Chris Upton University of Victoria
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */

/**
 * This class of methods is used to provide the application client configuration
 * info from a specified xml file.
 * 
 * @author Sameer Girn
 */
public class AppClientPrefs {

    private static final String APP_CLIENT = "/ca/virology/appserver/client";

    protected static AppClientPrefs myinstance = null;

    /**
     * Constructor which sets the xml file location and imports the preferences
     * 
     * @param xmlFileLoc - the location of the xml file containing all the
     *                   preferences appNode - the node path to the application's
     *                   preferences
     */
    public AppClientPrefs(String xmlFileLoc) {

        final String XML_LOC = xmlFileLoc;
        myinstance = this;

        try {
            // remove any existing preferences if they exist (on the client's machine)
            if (Preferences.userRoot().nodeExists(APP_CLIENT)) {
                Preferences.userRoot().node(APP_CLIENT).removeNode();
                Preferences.userRoot().node(APP_CLIENT).flush();
            }
        } catch (BackingStoreException e) {
            System.out.println("BackingStoreException");
            System.out.println("Error: " + e.getMessage());
            e.printStackTrace();
        }
        try {
            URL url = new URL(XML_LOC);
            Preferences.importPreferences(url.openStream());
        } catch (MalformedURLException e) {
            System.out.println("MalformedURLException");
            System.out.println("Error: " + e.getMessage());
            e.printStackTrace();
        } catch (IOException e) {
            System.out.println("IOException");
            System.out.println("Error: Could not find " + e.getMessage());
            e.printStackTrace();
        } catch (InvalidPreferencesFormatException e) {
            System.out.println("InvalidPreferencesFormatException");
            System.out.println("Error: " + e.getMessage());
            e.printStackTrace();
        }
    }

    /**
     * Gets the instance of the attribute of the AppClientPrefs class
     * 
     * @return The instance value
     */
    public static AppClientPrefs getInstance() {
        return myinstance;
    }

    /**
     * if true, then appserver will compress messages
     * 
     * @return compress value
     */
    public boolean get_client_compress() {
        Preferences serviceNode = Preferences.userRoot().node(APP_CLIENT);
        try {
            return serviceNode.getBoolean("compress", true);
        } catch (Exception e) {
            System.out.println("Error: " + e.getMessage());
            e.printStackTrace();
        }
        return true;
    }

    /**
     * gets the application server address
     * 
     * @return address
     */
    public String get_client_appserveraddress() {
        Preferences serviceNode = Preferences.userRoot().node(APP_CLIENT);
        try {
            return serviceNode.get("appserveraddress", "");
        } catch (Exception e) {
            System.out.println("Error: " + e.getMessage());
            e.printStackTrace();
        }
        return "";
    }

    /**
     * gets the application server port number
     * 
     * @return port number
     */
    public int get_client_appserverport() {
        Preferences serviceNode = Preferences.userRoot().node(APP_CLIENT);
        try {
            return serviceNode.getInt("appserverport", -1);
        } catch (Exception e) {
            System.out.println("Error: " + e.getMessage());
            e.printStackTrace();
        }
        return -1;
    }

    /**
     * gets the maximum jdotter query length
     * 
     * @return dottermaxlength
     */
    public int get_client_dottermaxlength() {
        Preferences serviceNode = Preferences.userRoot().node(APP_CLIENT);
        try {
            return serviceNode.getInt("dottermaxlength", 300000); // default is 300K
        } catch (Exception e) {
            System.out.println("Error: " + e.getMessage());
            e.printStackTrace();
        }
        return 300000; // default is 300K
    }
}
