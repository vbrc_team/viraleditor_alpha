package Shared.interchange;

import java.util.List;

import com.fasterxml.jackson.databind.util.StdConverter;

public class StringToAminoAcidConverter extends StdConverter<String, List<AminoAcid>> {
    private static final StringToAminoAcids toAminoAcids = new StringToAminoAcids();

    @Override
    public List<AminoAcid> convert(String value) {
        return toAminoAcids.to(value);
    }
}
