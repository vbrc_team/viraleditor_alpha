package Shared.interchange;

import java.util.Collections;
import java.util.EnumMap;
import java.util.List;
import java.util.Map;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonTypeInfo;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonPOJOBuilder;

@JsonDeserialize(builder = Source.Builder.class)
@JsonInclude(value = JsonInclude.Include.NON_DEFAULT)
@JsonTypeInfo(include = JsonTypeInfo.As.PROPERTY, use = JsonTypeInfo.Id.NAME, property = "type")
public class Source implements Feature {
    private List<Interval> intervals;
    private final Map<DbXRef, String> dbXref;
    private final String host;
    private final String isolate;
    private final String isolationSource;
    private final String labHost;
    private final String metagenomeSource;
    private final String molType;
    private final String note;
    private final String organism;
    private final String pcrPrimers;
    private final String segment;
    private final String serotype;
    private final String strain;
    private final String subSpecies;
    private final String subStrain;
    private final String tissueType;

    private Source(Builder builder) {
        if (builder.intervals.isEmpty()) {
            throw new IllegalArgumentException();
        }

        this.intervals = builder.intervals;
        this.dbXref = builder.dbXref;
        this.host = builder.host;
        this.isolate = builder.isolate;
        this.isolationSource = builder.isolationSource;
        this.labHost = builder.labHost;
        this.metagenomeSource = builder.metagenomeSource;
        this.molType = builder.molType;
        this.note = builder.note;
        this.organism = builder.organism;
        this.pcrPrimers = builder.pcrPrimers;
        this.segment = builder.segment;
        this.serotype = builder.serotype;
        this.strain = builder.strain;
        this.subSpecies = builder.subSpecies;
        this.subStrain = builder.subStrain;
        this.tissueType = builder.tissueType;
    }

    @Override
    @JsonIgnore
    public FeatureType getType() {
        return FeatureType.SOURCE;
    }

    @Override
    public List<Interval> getIntervals() {
        return intervals;
    }

    @Override
    public Source withIntervals(List<Interval> intervals) {
        return builder().copy(this).withIntervals(intervals).build();
    }

    public Map<DbXRef, String> getDbXref() {
        return Collections.unmodifiableMap(dbXref);
    }

    public String getHost() {
        return host;
    }

    public String getIsolate() {
        return isolate;
    }

    public String getIsolationSource() {
        return isolationSource;
    }

    public String getLabHost() {
        return labHost;
    }

    public String getMetagenomeSource() {
        return metagenomeSource;
    }

    public String getMolType() {
        return molType;
    }

    public String getNote() {
        return note;
    }

    public String getOrganism() {
        return organism;
    }

    public String getPCRPrimers() {
        return pcrPrimers;
    }

    public String getSegment() {
        return segment;
    }

    public String getSerotype() {
        return serotype;
    }

    public String getStrain() {
        return strain;
    }

    public String getSubSpecies() {
        return subSpecies;
    }

    public String getSubStrain() {
        return subStrain;
    }

    public String getTissueType() {
        return tissueType;
    }

    /**
     * Creates builder to build {@link Source}.
     * 
     * @return created builder
     */
    public static Builder builder() {
        return new Builder();
    }

    /**
     * Builder to build {@link Source}.
     */
    @JsonPOJOBuilder
    public static final class Builder {
        private static final String EMPTY = "";
        private List<Interval> intervals = Collections.emptyList();
        private Map<DbXRef, String> dbXref = Collections.emptyMap();
        private String host = EMPTY;
        private String isolate = EMPTY;
        private String isolationSource = EMPTY;
        private String labHost = EMPTY;
        private String metagenomeSource = EMPTY;
        private String molType = EMPTY;
        private String note = EMPTY;
        private String organism = EMPTY;
        private String pcrPrimers = EMPTY;
        private String segment = EMPTY;
        private String serotype = EMPTY;
        private String strain = EMPTY;
        private String subSpecies = EMPTY;
        private String subStrain = EMPTY;
        private String tissueType = EMPTY;

        private Builder() {
        }

        public Builder copy(Source source) {
            intervals = source.intervals;
            dbXref = source.dbXref;
            host = source.host;
            isolate = source.isolate;
            isolationSource = source.isolationSource;
            labHost = source.labHost;
            metagenomeSource = source.metagenomeSource;
            molType = source.molType;
            note = source.note;
            organism = source.organism;
            pcrPrimers = source.pcrPrimers;
            segment = source.segment;
            serotype = source.serotype;
            strain = source.strain;
            subSpecies = source.subSpecies;
            subStrain = source.subStrain;
            tissueType = source.tissueType;
            return this;
        }

        public Builder withIntervals(List<Interval> intervals) {
            this.intervals = intervals;
            return this;
        }

        public Builder withDbXref(Map<DbXRef, String> dbXref) {
            Map<DbXRef, String> map = new EnumMap<>(DbXRef.class);
            map.putAll(dbXref);
            this.dbXref = Collections.unmodifiableMap(map);
            return this;
        }

        public Builder withHost(String host) {
            this.host = host;
            return this;
        }

        public Builder withIsolate(String isolate) {
            this.isolate = isolate;
            return this;
        }

        public Builder withIsolationSource(String isolationSource) {
            this.isolationSource = isolationSource;
            return this;
        }

        public Builder withLabHost(String labHost) {
            this.labHost = labHost;
            return this;
        }

        public Builder withMetagenomeSource(String metagenomeSource) {
            this.metagenomeSource = metagenomeSource;
            return this;
        }

        public Builder withMolType(String molType) {
            this.molType = molType;
            return this;
        }

        public Builder withNote(String note) {
            this.note = note;
            return this;
        }

        public Builder withOrganism(String organism) {
            this.organism = organism;
            return this;
        }

        public Builder withPCRPrimers(String pcrPrimers) {
            this.pcrPrimers = pcrPrimers;
            return this;
        }

        public Builder withSegment(String segment) {
            this.segment = segment;
            return this;
        }

        public Builder withSerotype(String serotype) {
            this.serotype = serotype;
            return this;
        }

        public Builder withStrain(String strain) {
            this.strain = strain;
            return this;
        }

        public Builder withSubSpecies(String subSpecies) {
            this.subSpecies = subSpecies;
            return this;
        }

        public Builder withSubStrain(String subStrain) {
            this.subStrain = subStrain;
            return this;
        }

        public Builder withTissueType(String tissueType) {
            this.tissueType = tissueType;
            return this;
        }

        public Source build() {
            return new Source(this);
        }
    }

}
