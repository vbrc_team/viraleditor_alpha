package Shared.interchange;

import java.util.Collections;
import java.util.List;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonTypeInfo;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonPOJOBuilder;

@JsonDeserialize(builder = Description.Builder.class)
@JsonInclude(value = JsonInclude.Include.NON_DEFAULT)
@JsonTypeInfo(include = JsonTypeInfo.As.PROPERTY, use = JsonTypeInfo.Id.NAME, property = "type")
public class Description implements Feature {
    private final List<Interval> intervals;
    private final String description;

    private Description(Builder builder) {
        this.intervals = builder.intervals;
        this.description = builder.description;
    }

    @Override
    @JsonIgnore
    public FeatureType getType() {
        return FeatureType.DESCRIPTION;
    }

    /**
     * Returns a description.
     * <p>
     * 
     * @return a description
     */
    public String getDescription() {
        return description;
    }

    @Override
    public List<Interval> getIntervals() {
        return Collections.unmodifiableList(intervals);
    }

    @Override
    public Description withIntervals(List<Interval> intervals) {
        return builder().copy(this).withIntervals(intervals).build();
    }

    /**
     * Creates builder to build {@link Description}.
     * 
     * @return created builder
     */
    public static Builder builder() {
        return new Builder();
    }

    /**
     * Builder to build {@link Description}.
     */
    @JsonPOJOBuilder
    public static final class Builder {
        private List<Interval> intervals = Collections.emptyList();
        private String description;

        private Builder() {
        }

        public Builder copy(Description copy) {
            intervals = copy.intervals;
            description = copy.description;
            return this;
        }

        public Builder withIntervals(List<Interval> intervals) {
            if (intervals.isEmpty()) {
                throw new IllegalArgumentException("Empty intervals");
            }

            this.intervals = intervals;
            return this;
        }

        public Builder withDescription(String description) {
            this.description = description;
            return this;
        }

        public Description build() {
            return new Description(this);
        }
    }

}
