package Shared.interchange;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonTypeInfo;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonPOJOBuilder;

@JsonDeserialize(builder = Interchange.Builder.class)
@JsonInclude(value = JsonInclude.Include.NON_DEFAULT)
@JsonTypeInfo(include = JsonTypeInfo.As.PROPERTY, use = JsonTypeInfo.Id.NAME, property = "type")
public class Interchange {
    private final List<Reference> references;

    private Interchange(Builder builder) {
        this.references = builder.references;
    }

    public List<Reference> getReferences() {
        return this.references;
    }

    /**
     * Creates builder to build {@link Interchange}.
     * 
     * @return created builder
     */
    public static Builder builder() {
        return new Builder();
    }

    /**
     * Builder to build {@link Interchange}.
     */
    @JsonPOJOBuilder
    public static final class Builder {
        private List<Reference> references = Collections.emptyList();

        private Builder() {
        }

        public Builder withReferences(List<Reference> references) {
            List<Reference> refs = new ArrayList<>();
            refs.addAll(references);
            
            this.references = Collections.unmodifiableList(refs);
            return this;
        }

        public Interchange build() {
            return new Interchange(this);
        }
    }


}
