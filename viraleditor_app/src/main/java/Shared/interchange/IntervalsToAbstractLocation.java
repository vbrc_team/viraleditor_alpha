package Shared.interchange;

import java.util.ArrayList;
import java.util.List;

import org.biojava.nbio.core.sequence.Strand;
import org.biojava.nbio.core.sequence.location.SimpleLocation;
import org.biojava.nbio.core.sequence.location.template.AbstractLocation;
import org.biojava.nbio.core.sequence.location.template.Location;

/**
 * Convert between Intervals and AbstractLocation objects.
 * <p>
 * Converting from {@link AbstractLocation} to List<{@link Interval}> stores the
 * strand direction, start point, and end point. The conversion sets
 * {@link Interval#isComplement()} to {@code false} with a positive strand and
 * {@code true} with a negative strand. An {@code AbstractLocation} with
 * sub-locations creates one interval for each sub-location.
 * <p>
 * Converting from {@code List<Interval>} to {@code AbstractLocation} creates an
 * {@code AbstractLocation} with zero or more sub-locations. The conversion is
 * an inverse of the previous conversion with one exception. An
 * {@code AbstractLocation} with sub-locations has
 * {@code AbstractLocation#getStart()} set to {@code Interval#getLow()} of the
 * first interval and {@code AbstractLocation#getEnd()} set to
 * {@code Interval#getHigh()} of the last location.
 */
public class IntervalsToAbstractLocation implements Convert<List<Interval>, AbstractLocation> {

    @Override
    public AbstractLocation to(List<Interval> s) {
        if (s.size() == 1) {
            AbstractLocation location;
            Interval interval = s.get(0);
            if (interval.isComplement()) {
                location = new SimpleLocation(interval.getLow(), interval.getHigh(), Strand.NEGATIVE);
            } else {
                location = new SimpleLocation(interval.getLow(), interval.getHigh(), Strand.POSITIVE);
            }

            return location;
        } else if (s.size() > 1) {
            List<Location> locations = new ArrayList<>();
            for (Interval interval : s) {
                AbstractLocation location;
                if (interval.isComplement()) {
                    location = new SimpleLocation(interval.getLow(), interval.getHigh(), Strand.NEGATIVE);
                } else {
                    location = new SimpleLocation(interval.getLow(), interval.getHigh(), Strand.POSITIVE);
                }

                locations.add(location);
            }

            AbstractLocation location = new SimpleLocation(locations.get(0).getStart(),
                    locations.get(locations.size() - 1).getEnd());
            location.setSubLocations(locations);

            return location;
        } else {
            throw new IllegalArgumentException("Must have at least one interval");
        }
    }

    @Override
    public List<Interval> from(AbstractLocation d) {
        List<Interval> intervals = new ArrayList<>();
        List<Location> locations = new ArrayList<>();

        if (d.getRelevantSubLocations().isEmpty()) {
            locations.add(d);
        } else {
            locations = d.getRelevantSubLocations();
        }

        final boolean complement = true;
        for (Location loc : locations) {
            switch (loc.getStrand()) {
            case POSITIVE:
                intervals.add(new Interval(!complement, loc.getStart().getPosition(), loc.getEnd().getPosition()));
                break;
            case NEGATIVE:
                intervals.add(new Interval(complement, loc.getStart().getPosition(), loc.getEnd().getPosition()));
                break;
            default: // Unknown
                // Skip unknown strands
                break;
            }
        }

        return intervals;
    }

}
