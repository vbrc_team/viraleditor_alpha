package Shared.interchange;

import java.util.Collections;
import java.util.List;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonTypeInfo;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonPOJOBuilder;

@JsonDeserialize(builder = Family.Builder.class)
@JsonInclude(value = JsonInclude.Include.NON_DEFAULT)
@JsonTypeInfo(include = JsonTypeInfo.As.PROPERTY, use = JsonTypeInfo.Id.NAME, property = "type")
public class Family implements Feature {
    private List<Interval> intervals;
    private final int familyID;
    private final String familyName;

    private Family(Builder builder) {
        if (builder.intervals.isEmpty()) {
            throw new IllegalArgumentException();
        }

        this.intervals = builder.intervals;
        this.familyID = builder.familyID;
        this.familyName = builder.familyName;
    }

    @Override
    @JsonIgnore
    public FeatureType getType() {
        return FeatureType.FAMILY;
    }

    @Override
    public List<Interval> getIntervals() {
        return Collections.unmodifiableList(intervals);
    }

    @Override
    public Family withIntervals(List<Interval> intervals) {
        return builder().copy(this).withIntervals(intervals).build();
    }

    /**
     * Returns the family id.
     * <p>
     * A non-negative integer. The id uniquely identifies a family.
     *
     * @return the family id
     */
    public int getFamilyID() {
        return familyID;
    }

    /**
     * Returns the family name.
     *
     * @return the family name
     */
    public String getFamilyName() {
        return familyName;
    }

    /**
     * Creates builder to build {@link Family}.
     *
     * @return created builder
     */
    public static Builder builder() {
        return new Builder();
    }

    /**
     * Builder to build {@link Family}.
     */
    @JsonPOJOBuilder
    public static final class Builder {
        private List<Interval> intervals = Collections.emptyList();
        private int familyID;
        private String familyName;

        private Builder() {
            familyName = "";
        }

        public Builder copy(Family family) {
            intervals = family.intervals;
            familyID = family.familyID;
            familyName = family.familyName;
            return this;
        }

        public Builder withIntervals(List<Interval> intervals) {
            if (intervals.isEmpty()) {
                throw new IllegalArgumentException("Empty intervals");
            }

            this.intervals = intervals;
            return this;
        }

        public Builder withFamilyID(int familyID) {
            if (familyID < 0) {
                throw new IllegalArgumentException(String.format("Family id %d < 0", familyID));
            }

            this.familyID = familyID;
            return this;
        }

        public Builder withFamilyName(String familyName) {
            this.familyName = familyName;
            return this;
        }

        public Family build() {
            return new Family(this);
        }
    }

}
