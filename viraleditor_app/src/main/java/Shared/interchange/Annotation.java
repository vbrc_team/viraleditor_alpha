package Shared.interchange;

import java.time.LocalDateTime;
import java.time.format.DateTimeParseException;
import java.util.Collections;
import java.util.List;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonTypeInfo;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonPOJOBuilder;

@JsonDeserialize(builder = Annotation.Builder.class)
@JsonInclude(value = JsonInclude.Include.NON_DEFAULT)
@JsonTypeInfo(include = JsonTypeInfo.As.PROPERTY, use = JsonTypeInfo.Id.NAME, property = "type")
public class Annotation implements Feature {
    private List<Interval> intervals;

    private final int geneID;
    private final int familyID;
    private final int genomeID;
    private final String geneAbbreviation;
    private final char strand;
    private final String annotator;
    private final String annoSource;
    private final String moleculeType;
    private final String featureName;
    @JsonInclude(value = JsonInclude.Include.CUSTOM, valueFilter = DefaultEpoch.class)
    private final String lastModified;
    private final String description;
    private final String notes;
    private final String genBankName;
    private final int genBankGeneGI;
    @JsonInclude(value = JsonInclude.Include.CUSTOM, valueFilter = DefaultOne.class)
    private final int genBankGeneVersion;
    private final int orthologGroupID;
    private final String orthologEvidence;
    private final NucleotideSequence upstreamSequence;
    private final NucleotideSequence ntSequence;
    private final AminoAcidSequence aaSequence;
    @JsonInclude(value = JsonInclude.Include.CUSTOM, valueFilter = DefaultEpoch.class)
    private final String insertedOn;
    private static final String EPOCH_DATE = "1970-01-01T00:00";

    private Annotation(Builder builder) {
        if (builder.intervals.isEmpty()) {
            throw new IllegalArgumentException();
        }

        this.intervals = builder.intervals;
        this.geneID = builder.geneID;
        this.familyID = builder.familyID;
        this.genomeID = builder.genomeID;
        this.geneAbbreviation = builder.geneAbbreviation;
        this.strand = builder.strand;
        this.annotator = builder.annotator;
        this.annoSource = builder.annoSource;
        this.moleculeType = builder.moleculeType;
        this.featureName = builder.featureName;
        this.lastModified = builder.lastModified;
        this.description = builder.description;
        this.notes = builder.notes;
        this.genBankName = builder.genBankName;
        this.genBankGeneGI = builder.genBankGeneGI;
        this.genBankGeneVersion = builder.genBankGeneVersion;
        this.orthologGroupID = builder.orthologGroupID;
        this.orthologEvidence = builder.orthologEvidence;
        this.upstreamSequence = builder.upstreamSequence;
        this.ntSequence = builder.ntSequence;
        this.aaSequence = builder.aaSequence;
        this.insertedOn = builder.insertedOn;
    }

    @Override
    @JsonIgnore
    public FeatureType getType() {
        return FeatureType.ANNOTATION;
    }

    @Override
    public List<Interval> getIntervals() {
        return Collections.unmodifiableList(intervals);
    }

    @Override
    public Annotation withIntervals(List<Interval> intervals) {
        return builder().copy(this).withIntervals(intervals).build();
    }

    /**
     * Returns the id of the annotated gene.
     * <p>
     * Non-negative integer. The gene id uniquely identifies the annotation.
     *
     * @return the gene id
     */
    public int getGeneID() {
        return geneID;
    }

    /**
     * Returns the id of the family that the annotation belongs to.
     * <p>
     * Non-negative integer.
     *
     * @return the family id
     */
    public int getFamilyID() {
        return familyID;
    }

    /**
     * Returns the id of the genome that the annotation belongs to.
     * <p>
     * A non-negative integer.
     *
     * @return the genome id
     */
    public int getGenomeID() {
        return genomeID;
    }

    /**
     * Returns the abbreviation of the annotated gene.
     * <p>
     * The abbreviation is generally a genome abbreviation followed by a dash and a
     * number. An example would is 'AMEV-Moyer-001'.
     *
     * @return the abbreviation of the annotated gene
     */
    public String getGeneAbbreviation() {
        return geneAbbreviation;
    }

    /**
     * Returns + when the strand is in the 5' - 3' direction and - when the strand
     * is in the 3' - 5' direction.
     * <p>
     * Default value of '+'.
     *
     * @return the strand direction
     */
    public char getStrand() {
        return strand;
    }

    /**
     * Returns the name of who imported the annotation into the VBRC database.
     *
     * @return the name of the annotator
     */
    public String getAnnotator() {
        return annotator;
    }

    /**
     * Returns the source of the annotation.
     * <p>
     * A source is where the annotation originated. For example one source could be
     * a GenBank file.
     *
     * @return the annotation source
     */
    public String getAnnoSource() {
        return annoSource;
    }

    /**
     * Returns the molecule type of the the annotated gene.
     *
     * @return the type of molecule
     */
    public String getMoleculeType() {
        return moleculeType;
    }

    /**
     * Returns the name of the annotated feature.
     *
     * @return the name of the feature
     */
    public String getFeatureName() {
        return featureName;
    }

    /**
     * The date and time the annotation was last modified in the VBRC database.
     * <p>
     * Default value of 1970-01-01T00:00". Must be formatted like
     * 2007-12-03T10:15:30.
     *
     * @return the date the annotation was last modified
     */
    public String getLastModified() {
        return lastModified;
    }

    /**
     * Returns the description of the annotation.
     *
     * @return the description of the annotation
     */
    public String getDescription() {
        return description;
    }

    /**
     * Returns notes about the annotation.
     *
     * @return notes about the annotation
     */
    public String getNotes() {
        return notes;
    }

    /**
     *
     * @return the name assigned by GenBank to the annotation
     */
    public String getGenBankName() {
        return genBankName;
    }

    /**
     * Returns the annotation's gene id assigned by GenBank.
     *
     * @return the GenBank gene id
     */
    public int getGenBankGeneGI() {
        return genBankGeneGI;
    }

    /**
     * Returns the GenBank gene version.
     * <p>
     * Positive integer with a default value of 1. Generally the version number from
     * the GenBank protein_id qualifier.
     *
     * @return the GenBank gene version
     */
    public int getGenBankGeneVersion() {
        return genBankGeneVersion;
    }

    /**
     * Returns the ortholog group ID.
     * <p>
     * A non-negative integer with a default value of 0. The ortholog group ID is a
     * key in the VBRC database that groups genes into an ortholog group.
     *
     * @return the ortholog group ID.
     */
    public int getOrthologGroupID() {
        return orthologGroupID;
    }

    /**
     * Returns the type of evidence used to assign the ortholog group.
     *
     * @return the ortholog evidence type
     */
    public String getOrthologEvidence() {
        return orthologEvidence;
    }

    /**
     * Returns a sequence of characters upstream from the annotation.
     * <p>
     * The upstream sequence is typically the 100 characters before first interval.
     *
     * @return the upstream sequence
     */
    public NucleotideSequence getUpstreamSequence() {
        return upstreamSequence;
    }

    /**
     * Returns the annotated nucleotide sequence.
     * <p>
     * The sequence is made by combining the regions indicated by intervals in the
     * annotated genome.
     *
     * @return the annotated nucleotide sequence
     */
    public NucleotideSequence getNTSequence() {
        return ntSequence;
    }

    /**
     * Returns the amino acid sequence.
     * <p>
     * The same as the ntSequence translated into an amino acid sequence.
     *
     * @return the annotated amino acid sequence
     */
    public AminoAcidSequence getAASequence() {
        return aaSequence;
    }

    /**
     * The date and time the annotation was inserted into in the VBRC database.
     * <p>
     * Default value of 1970-01-01T00:00". Must be formatted like
     * 2007-12-03T10:15:30.
     *
     * @return the date the annotation was inserted into the VBRC database
     */
    public String getInsertedOn() {
        return insertedOn;
    }

    /**
     * Creates builder to build {@link Annotation}.
     *
     * @return created builder
     */
    public static Builder builder() {
        return new Builder();
    }

    /**
     * Creates builder to build {@link Annotation}.
     *
     * @return created builder
     */
    @JsonPOJOBuilder
    public static final class Builder {
        private List<Interval> intervals = Collections.emptyList();
        private int geneID;
        private int familyID;
        private int genomeID;
        private String geneAbbreviation;
        private char strand = '+';
        private String annotator;
        private String annoSource;
        private String moleculeType;
        private String featureName;
        private String lastModified;
        private String description;
        private String notes;
        private String genBankName;
        private int genBankGeneGI;
        private int genBankGeneVersion = 1;
        private int orthologGroupID;
        private String orthologEvidence;
        private NucleotideSequence upstreamSequence;
        private NucleotideSequence ntSequence;
        private AminoAcidSequence aaSequence;
        private String insertedOn;

        private Builder() {
            String emptyString = "";
            geneAbbreviation = emptyString;
            annotator = emptyString;
            annoSource = emptyString;
            moleculeType = emptyString;
            featureName = emptyString;
            lastModified = EPOCH_DATE;
            description = emptyString;
            notes = emptyString;
            genBankName = emptyString;
            orthologEvidence = emptyString;
            upstreamSequence = new NucleotideSequence(Collections.emptyList());
            insertedOn = EPOCH_DATE;
            ntSequence = new NucleotideSequence(Collections.emptyList());
            aaSequence = new AminoAcidSequence(Collections.emptyList());
        }

        public Builder copy(Annotation annotation) {
            intervals = annotation.intervals;
            geneAbbreviation = annotation.geneAbbreviation;
            annotator = annotation.annotator;
            annoSource = annotation.annoSource;
            moleculeType = annotation.moleculeType;
            featureName = annotation.featureName;
            lastModified = annotation.lastModified;
            description = annotation.description;
            notes = annotation.notes;
            genBankName = annotation.genBankName;
            orthologEvidence = annotation.orthologEvidence;
            upstreamSequence = annotation.upstreamSequence;
            insertedOn = annotation.insertedOn;
            ntSequence = annotation.ntSequence;
            aaSequence = annotation.aaSequence;
            return this;
        }

        public Builder withIntervals(List<Interval> intervals) {
            if (intervals.isEmpty()) {
                throw new IllegalArgumentException("Empty intervals");
            }

            this.intervals = intervals;
            return this;
        }

        public Builder withGeneID(int geneID) {
            if (geneID < 0) {
                throw new IllegalArgumentException(String.format("Gene id %d < 0", geneID));
            }

            this.geneID = geneID;
            return this;
        }

        public Builder withFamilyID(int familyID) {
            if (familyID < 0) {
                throw new IllegalArgumentException(String.format("Family id %d < 0", familyID));
            }

            this.familyID = familyID;
            return this;
        }

        public Builder withGenomeID(int genomeID) {
            if (genomeID < 0) {
                throw new IllegalArgumentException(String.format("Genome id %d < 0", genomeID));
            }

            this.genomeID = genomeID;
            return this;
        }

        public Builder withGeneAbbreviation(String geneAbbreviation) {
            this.geneAbbreviation = geneAbbreviation;
            return this;
        }

        public Builder withStrand(char strand) {
            if (strand != '+' && strand != '-') {
                throw new IllegalArgumentException(String.format("String '%c' must be + or -", strand));
            }

            this.strand = strand;
            return this;
        }

        public Builder withAnnotator(String annotator) {
            this.annotator = annotator;
            return this;
        }

        public Builder withAnnoSource(String annoSource) {
            this.annoSource = annoSource;
            return this;
        }

        public Builder withMoleculeType(String moleculeType) {
            this.moleculeType = moleculeType;
            return this;
        }

        public Builder withFeatureName(String featureName) {
            this.featureName = featureName;
            return this;
        }

        public Builder withLastModified(String lastModified) {
            try {
                LocalDateTime.parse(lastModified);
            } catch (DateTimeParseException exception) {
                throw new IllegalArgumentException(
                        String.format("%s must be formatted like 2007-12-03T10:15:30.", lastModified));
            }

            this.lastModified = lastModified;
            return this;
        }

        public Builder withDescription(String description) {
            this.description = description;
            return this;
        }

        public Builder withNotes(String notes) {
            this.notes = notes;
            return this;
        }

        public Builder withGenBankName(String genBankName) {
            this.genBankName = genBankName;
            return this;
        }

        public Builder withGenBankGeneGI(int genBankGeneGI) {
            if (genBankGeneGI < 0) {
                throw new IllegalArgumentException(String.format("Gene bank gene id %d < 0", genBankGeneGI));
            }

            this.genBankGeneGI = genBankGeneGI;
            return this;
        }

        public Builder withGenBankGeneVersion(int genBankGeneVersion) {
            if (genBankGeneVersion < 1) {
                throw new IllegalArgumentException(String.format("Gene bank gene version %d < 1", genBankGeneVersion));
            }

            this.genBankGeneVersion = genBankGeneVersion;
            return this;
        }

        public Builder withOrthologGroupID(int orthologGroupID) {
            if (orthologGroupID < 0) {
                throw new IllegalArgumentException(String.format("Ortholog group id %d < 0", orthologGroupID));
            }

            this.orthologGroupID = orthologGroupID;
            return this;
        }

        public Builder withOrthologEvidence(String orthologEvidence) {
            this.orthologEvidence = orthologEvidence;
            return this;
        }

        public Builder withUpstreamSequence(NucleotideSequence upstreamSequence) {
            this.upstreamSequence = upstreamSequence;
            return this;
        }

        public Builder withNTSequence(NucleotideSequence ntSequence) {
            this.ntSequence = ntSequence;
            return this;
        }

        public Builder withAASequence(AminoAcidSequence aaSequence) {
            this.aaSequence = aaSequence;
            return this;
        }

        public Builder withInsertedOn(String insertedOn) {
            try {
                LocalDateTime.parse(insertedOn);
            } catch (DateTimeParseException exception) {
                throw new IllegalArgumentException(
                        String.format("%s must be formatted like 2007-12-03T10:15:30.", insertedOn));
            }

            this.insertedOn = insertedOn;
            return this;
        }

        public Annotation build() {
            return new Annotation(this);
        }
    }

    private static final class DefaultOne {
        @Override
        public boolean equals(Object obj) {
            if (!(obj instanceof Integer)) {
                return false;
            }

            return (Integer) obj == 1;
        }
    }

    private static final class DefaultEpoch {
        @Override
        public boolean equals(Object obj) {
            if (!(obj instanceof String)) {
                return false;
            }

            return ((String) obj).equals(EPOCH_DATE);
        }
    }
}
