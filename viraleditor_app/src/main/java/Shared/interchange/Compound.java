package Shared.interchange;

public interface Compound<E extends Enum<E>> {
    /**
     * Returns the single character ambiguity code for the compound.
     * 
     * @return the single character ambiguity code for the compound
     */
    public String getCode();
}
