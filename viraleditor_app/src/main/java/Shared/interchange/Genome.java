package Shared.interchange;

import java.time.LocalDateTime;
import java.time.format.DateTimeParseException;
import java.util.Collections;
import java.util.List;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonTypeInfo;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonPOJOBuilder;

/**
 * Stores information from the VBRC database related to the genome of a virus
 * strain.
 */
@JsonDeserialize(builder = Genome.Builder.class)
@JsonInclude(value = JsonInclude.Include.NON_DEFAULT)
@JsonTypeInfo(include = JsonTypeInfo.As.PROPERTY, use = JsonTypeInfo.Id.NAME, property = "type")
public class Genome implements Feature {
    private List<Interval> intervals;
    private final int taxnodeID;
    private final int genomeID;
    private final int familyID;
    private final String organismName;
    private final String organismAbbrevation;
    private final String strainName;
    private final String strainAbbrevation;
    private final String moleculeType;
    private final String description;
    private final String notes;
    @JsonInclude(value = JsonInclude.Include.CUSTOM, valueFilter = DefaultEpoch.class)
    private final String lastModified;
    @JsonInclude(value = JsonInclude.Include.CUSTOM, valueFilter = DefaultEpoch.class)
    private final String insertedOn;
    private static final String EPOCH_DATE = "1970-01-01T00:00";

    private Genome(Builder builder) {
        if (builder.intervals.isEmpty()) {
            throw new IllegalArgumentException();
        }

        this.intervals = builder.intervals;
        this.taxnodeID = builder.taxnodeID;
        this.genomeID = builder.genomeID;
        this.familyID = builder.familyID;
        this.organismName = builder.organismName;
        this.organismAbbrevation = builder.organismAbbrevation;
        this.strainName = builder.strainName;
        this.strainAbbrevation = builder.strainAbbrevation;
        this.moleculeType = builder.moleculeType;
        this.description = builder.description;
        this.notes = builder.notes;
        this.lastModified = builder.lastModified;
        this.insertedOn = builder.insertedOn;
    }

    @Override
    @JsonIgnore
    public FeatureType getType() {
        return FeatureType.GENOME;
    }

    @Override
    public List<Interval> getIntervals() {
        return Collections.unmodifiableList(intervals);
    }

    @Override
    public Genome withIntervals(List<Interval> intervals) {
        return builder().copy(this).withIntervals(intervals).build();
    }

    /**
     * Returns the id of the taxonomy node associated with the genome.
     * <p>
     * A non-negative integer. The node ID determines where in the taxonomy tree the
     * genome is located.
     *
     * @return the id of the taxonomy node
     */
    public int getTaxnodeID() {
        return taxnodeID;
    }

    /**
     * Returns the genome id.
     * <p>
     * A non-negative integer. The id uniquely identifies a genome.
     *
     * @return the genome id
     */
    public int getGenomeID() {
        return genomeID;
    }

    /**
     * Returns the id of the family that the genome belongs to.
     * <p>
     * A non-negative integer.
     *
     * @return the family id
     */
    public int getFamilyID() {
        return familyID;
    }

    /**
     * Returns name of the organism the genome refers to.
     * <p>
     * Often the same as the ORGANISM field in the GenBank file with accession.
     *
     * @return the name of the organism or an empty string
     */
    public String getOrganismName() {
        return organismName;
    }

    /**
     * Returns the abbreviation of the organism name.
     *
     * @return the abbreviation of the organism name
     */
    public String getOrganismAbbrevation() {
        return organismAbbrevation;
    }

    /**
     * Returns the name of the virus strain.
     *
     * @return the name of the virus strain
     */
    public String getStrainName() {
        return strainName;
    }

    /**
     * Returns the abbreviation of the virus strain name.
     *
     * @return an abbreviation for the virus strain name
     */
    public String getStrainAbbrevation() {
        return strainAbbrevation;
    }

    /**
     * The molecule type based on GenBank feature keys.
     *
     * @return the name of the molecule type or an empty string
     */
    public String getMoleculeType() {
        return moleculeType;
    }

    /**
     * A description of the genome. Typically the same content as the DEFINITION
     * field in GenBank.
     *
     * @return genome description or an empty string
     */
    public String getDescription() {
        return description;
    }

    /**
     * Notes about the genome that may contain details such as publication ids,
     * corrections made to the sequence, where it was isolated first, and other
     * details.
     *
     * @return notes about the genome or an empty string
     */
    public String getNotes() {
        return notes;
    }

    /**
     * The date and time the genome was last modified in the VBRC database.
     * <p>
     * Default value of 1970-01-01T00:00". Must be formatted like
     * 2007-12-03T10:15:30.
     *
     * @return the date the genome was last modified
     */
    public String getLastModified() {
        return lastModified;
    }

    /**
     * The date and time the genome was inserted into in the VBRC database.
     * <p>
     * Default value of 1970-01-01T00:00". Must be formatted like
     * 2007-12-03T10:15:30.
     *
     * @return the date the genome was inserted into the VBRC database
     */
    public String getInsertedOn() {
        return insertedOn;
    }

    /**
     * Creates builder to build {@link Genome}.
     *
     * @return created builder
     */
    public static Builder builder() {
        return new Builder();
    }

    /**
     * Builder to build {@link Genome}.
     */
    @JsonPOJOBuilder
    public static final class Builder {
        private List<Interval> intervals = Collections.emptyList();
        private int taxnodeID;
        private int genomeID;
        private int familyID;
        private String organismName;
        private String organismAbbrevation;
        private String strainName;
        private String strainAbbrevation;
        private String moleculeType;
        private String description;
        private String notes;
        private String lastModified;
        private String insertedOn;

        private Builder() {
            String emptyString = "";
            organismName = emptyString;
            organismAbbrevation = emptyString;
            strainName = emptyString;
            strainAbbrevation = emptyString;
            moleculeType = emptyString;
            description = emptyString;
            notes = emptyString;
            lastModified = EPOCH_DATE;
            insertedOn = EPOCH_DATE;
        }

        public Builder copy(Genome genome) {
            intervals = genome.intervals;
            organismName = genome.organismName;
            organismAbbrevation = genome.organismAbbrevation;
            strainName = genome.strainName;
            strainAbbrevation = genome.strainAbbrevation;
            moleculeType = genome.moleculeType;
            description = genome.description;
            notes = genome.notes;
            lastModified = genome.lastModified;
            insertedOn = genome.insertedOn;
            return this;
        }

        public Builder withIntervals(List<Interval> intervals) {
            if (intervals.isEmpty()) {
                throw new IllegalArgumentException("Empty intervals");
            }

            this.intervals = intervals;
            return this;
        }

        public Builder withTaxnodeID(int taxnodeID) {
            if (taxnodeID < 0) {
                throw new IllegalArgumentException(String.format("Tax node id %d < 0", taxnodeID));
            }

            this.taxnodeID = taxnodeID;
            return this;
        }

        public Builder withGenomeID(int genomeID) {
            if (genomeID < 0) {
                throw new IllegalArgumentException(String.format("Genome id %d < 0", genomeID));
            }

            this.genomeID = genomeID;
            return this;
        }

        public Builder withFamilyID(int familyID) {
            if (familyID < 0) {
                throw new IllegalArgumentException(String.format("Family id %d < 0", familyID));
            }

            this.familyID = familyID;
            return this;
        }

        public Builder withOrganismName(String organismName) {
            this.organismName = organismName;
            return this;
        }

        public Builder withOrganismAbbrevation(String organismAbbrevation) {
            this.organismAbbrevation = organismAbbrevation;
            return this;
        }

        public Builder withStrainName(String strainName) {
            this.strainName = strainName;
            return this;
        }

        public Builder withStrainAbbrevation(String strainAbbrevation) {
            this.strainAbbrevation = strainAbbrevation;
            return this;
        }

        public Builder withMoleculeType(String moleculeType) {
            this.moleculeType = moleculeType;
            return this;
        }

        public Builder withDescription(String description) {
            this.description = description;
            return this;
        }

        public Builder withNotes(String notes) {
            this.notes = notes;
            return this;
        }

        public Builder withLastModified(String lastModified) {
            try {
                LocalDateTime.parse(lastModified);
            } catch (DateTimeParseException exception) {
                throw new IllegalArgumentException(
                        String.format("%s must be formatted like 2007-12-03T10:15:30.", lastModified));
            }
            this.lastModified = lastModified;
            return this;
        }

        public Builder withInsertedOn(String insertedOn) {
            try {
                LocalDateTime.parse(insertedOn);
            } catch (DateTimeParseException exception) {
                throw new IllegalArgumentException(
                        String.format("%s must be formatted like 2007-12-03T10:15:30.", insertedOn));
            }
            this.insertedOn = insertedOn;
            return this;
        }

        public Genome build() {
            return new Genome(this);
        }
    }

    private static final class DefaultEpoch {
        @Override
        public boolean equals(Object obj) {
            if (!(obj instanceof String)) {
                return false;
            }

            return ((String) obj).equals(EPOCH_DATE);
        }
    }
}
