package Shared.interchange;

/**
 * Convert between two types.
 * <p>
 * The conversion process may be lossy.
 * 
 * @param <S> the type to convert to
 * @param <D> the type to convert from
 */
public interface Convert<S, D> {
    /**
     * Convert from an object of type D to an object of type S.
     * 
     * @param s
     * @return s converted to type D
     */
    D to(S s);

    /**
     * Convert from an object of type S to an object of type D.
     * 
     * @param d
     * @return d converted to type S
     */
    S from(D d);
}
