package Shared.interchange;

import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import org.biojava.nbio.core.exceptions.CompoundNotFoundException;
import org.biojava.nbio.core.sequence.AccessionID;
import org.biojava.nbio.core.sequence.DNASequence;
import org.biojava.nbio.core.sequence.compound.NucleotideCompound;
import org.biojava.nbio.core.sequence.features.FeatureInterface;
import org.biojava.nbio.core.sequence.template.AbstractSequence;

import Shared.interchange.Reference.Builder;

/**
 * Convert between Interchange and BioJava's GenBank format.
 * <p>
 * Converting from GenBank to Interchange creates an Interchange object with one
 * Reference for every GenBank entry. The reference origin, accession, and
 * identifier are set from the GenBank file. These features are converted
 * <ul>
 * <li>CDS
 * <li>gene
 * <li>mat_peptide
 * <li>source
 * </ul>
 * <p>
 * Converting from Interchange to GenBank converts these objects to their
 * GenBank equivalent.
 * <ul>
 * <li>CDS
 * <li>Gene
 * <li>MatPeptide
 * <li>Source
 * </ul>
 * <p>
 * Other features are ignored. See the *ToFeature classes for details on what
 * Feature qualifiers are converted. Not all GenBank qualifiers are supported by
 * the Interchange.
 */
public class InterchangeToNTGenBank implements Convert<Interchange, Map<String, DNASequence>> {
    private static final String LOCATION = "%d..%d";
    private static final String COMPLEMENT_LOCATION = "complement(%d..%d)";
    private static final MatPeptideToFeature convertMatPeptide = new MatPeptideToFeature();
    private static final SourceToFeature convertSource = new SourceToFeature();
    private static final GeneToFeature convertGene = new GeneToFeature();
    private static final CDSToFeature convertCDS = new CDSToFeature();

    @Override
    public Map<String, DNASequence> to(Interchange s) {
        Map<String, DNASequence> sequences = new LinkedHashMap<>();
        int refName = 0;
        for (Reference reference : s.getReferences()) {
            if (reference.getOrigin().getType() != SequenceType.NUCLEOTIDE) {
                continue; // Only support nucleotide reference sequences
            }

            DNASequence seq;
            try {
                seq = new DNASequence(reference.getOrigin().getSequence());
            } catch (CompoundNotFoundException e) {
                throw new IllegalArgumentException("Origin is not a nucleotide sequence", e);
            }

            if (!reference.getAccession().equals("")) {
                AccessionID id = new AccessionID(reference.getAccession());
                id.setVersion(Integer.parseInt(reference.getVersion()));
                seq.setAccession(id);
            }

            for (Feature feature : reference.getFeatures(Feature.class)) {
                FeatureInterface<AbstractSequence<NucleotideCompound>, NucleotideCompound> converted;
                switch (feature.getType()) {
                case SOURCE:
                    converted = convertSource.to((Source) feature);
                    break;
                case GENE:
                    converted = convertGene.to((Gene) feature);
                    break;
                case MAT_PEPTIDE:
                    converted = convertMatPeptide.to((MatPeptide) feature);
                    break;
                case CDS:
                    converted = convertCDS.to((CDS) feature);
                    break;
                default:
                    continue;
                }

                converted.setSource(toSource(feature.getIntervals()));
                seq.addFeature(converted);
            }

            sequences.put(String.valueOf(refName), seq);
        }

        return sequences;
    }

    @Override
    public Interchange from(Map<String, DNASequence> d) {
        List<Reference> references = new ArrayList<>();
        for (DNASequence sequence : d.values()) {
            Builder builder = Reference.builder();
            List<FeatureInterface<AbstractSequence<NucleotideCompound>, NucleotideCompound>> features = sequence.getFeatures();

            AccessionID accession = sequence.getAccession();

            // BioJava uses nulls as default values.
            // Prevent nulls from propagating to rest of program.
            if (accession.getID() != null) {
                builder.withAccession(accession.getID());
            }

            if (accession.getVersion() != null) {
                builder.withVersion(accession.getVersion().toString());
            }

            if (accession.getIdentifier() != null) {
                builder.withIdentifier(accession.getIdentifier());
            }

            if (sequence.getSequenceAsString() != null) {
                builder.withOrigin(
                        new NucleotideSequence(new StringToNucleotides().to(sequence.getSequenceAsString())));
            }

            List<Feature> interchangeFeatures = new ArrayList<>();
            for (FeatureInterface<AbstractSequence<NucleotideCompound>, NucleotideCompound> feature : features) {
                switch (feature.getType()) {
                case "source":
                    interchangeFeatures.add(convertSource.from(feature));
                    break;
                case "gene":
                    interchangeFeatures.add(convertGene.from(feature));
                    break;
                case "CDS":
                    interchangeFeatures.add(convertCDS.from(feature));
                    break;
                case "mat_peptide":
                    interchangeFeatures.add(convertMatPeptide.from(feature));
                    break;
                default:
                    // Skip unsupported feature types
                    break;
                }
            }

            builder.withFeatures(interchangeFeatures);
            references.add(builder.build());
        }

        return Interchange.builder().withReferences(references).build();
    }

    private String toSource(List<Interval> intervals) {
        String source;
        if (intervals.size() > 1) {
            StringBuilder builder = new StringBuilder();
            builder.append("join(");

            for (int i = 0, end = intervals.size() - 1; i < end; ++i) {
                Interval interval = intervals.get(i);
                if (interval.isComplement()) {
                    builder.append(String.format(COMPLEMENT_LOCATION, interval.getHigh(), interval.getLow()));
                } else {
                    builder.append(String.format(LOCATION, interval.getLow(), interval.getHigh()));
                }
                builder.append(",");
            }

            Interval interval = intervals.get(intervals.size() - 1);
            if (interval.isComplement()) {
                builder.append(String.format(COMPLEMENT_LOCATION, interval.getHigh(), interval.getLow()));
            } else {
                builder.append(String.format(LOCATION, interval.getLow(), interval.getHigh()));
            }

            builder.append(")");
            source = builder.toString();
        } else if (intervals.size() == 1) {
            Interval interval = intervals.get(0);
            if (interval.isComplement()) {
                source = String.format(COMPLEMENT_LOCATION, interval.getHigh(), interval.getLow());
            } else {
                source = String.format(LOCATION, interval.getLow(), interval.getHigh());
            }
        } else {
            throw new IllegalArgumentException("intervals must not be empty");
        }

        return source;
    }

}
