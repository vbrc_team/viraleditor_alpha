package Shared.interchange;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonTypeInfo;

/**
 * An interval represents a continuous region in a reference sequence. One-based
 * indices are used based on the convention in GenBank.
 *
 */
@JsonTypeInfo(include = JsonTypeInfo.As.PROPERTY, use = JsonTypeInfo.Id.NAME, property = "type")
public class Interval {
    private final boolean complement;
    private final int low;
    private final int high;

    /**
     * Constructs an interval over the one-based interval with inclusive end points.
     * <p>
     * When complement is false the interval is read from low to high and when
     * complement is true the interval is read high to low.
     *
     * @param complement is the interval order complemented
     * @param low        the interval lower bound as positive integer
     * @param high       the interval upper bound as positive integer
     */
    @JsonCreator
    public Interval(@JsonProperty("complement") boolean complement, @JsonProperty("low") int low,
            @JsonProperty("high") int high) {
        if (low >= high || low <= 0 || high <= 0) {
            throw new IllegalArgumentException();
        }
        this.complement = complement;
        this.low = low;
        this.high = high;
    }

    /**
     * Returns true when the interval is read from high to low and false when read
     * from low to high.
     *
     * @return true when complemented and false when not
     */
    public boolean isComplement() {
        return complement;
    }

    /**
     *
     * @return the interval lower bound
     */
    public int getLow() {
        return low;
    }

    /**
     *
     * @return the interval upper bound
     */
    public int getHigh() {
        return high;
    }
}
