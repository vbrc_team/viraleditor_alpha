package Shared.interchange;

import java.util.Collections;
import java.util.List;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;

public class AminoAcidSequence implements Sequence {
    @JsonInclude(value = JsonInclude.Include.CUSTOM, valueFilter = DefaultString.class)
    @JsonDeserialize(converter = StringToAminoAcidConverter.class)
    private final String sequence;

    /**
     * Construct an amino acid sequence.
     *
     * @param seq an amino acid sequence
     */
    public AminoAcidSequence(@JsonProperty("sequence") List<AminoAcid> acids) {
        // Jackson can pass null value when deserializing
        if (acids == null) {
            acids = Collections.emptyList();
        }

        StringBuilder builder = new StringBuilder();
        for (AminoAcid acid : acids) {
            builder.append(acid.getCode());
        }
        sequence = builder.toString();
    }

    /**
     * Constructs a amino acid sequence without input validation.
     *
     * @param seq a amino acid sequence
     */
    public AminoAcidSequence(String seq) {
        sequence = seq;
    }

    /**
     * Returns the amino acid sequence with possible gaps.
     *
     * @return a string using only the characters
     */
    @Override
    public String getSequence() {
        return sequence;
    }

    @Override
    public SequenceType getType() {
        return SequenceType.AMINO_ACID;
    }
    
    @Override
    public Sequence subSequence(int start, int end) {
    	StringBuilder builder = new StringBuilder(sequence.substring(start, end));
    	return new AminoAcidSequence(builder.toString());
    }

    @Override
    public Sequence insertGaps(int index, int m) {
        StringBuilder builder = new StringBuilder(sequence.substring(0, index));
        for (int i = 0; i < m; i++) {
            builder.append("-");
        }
        builder.append(sequence.substring(index));
        return new AminoAcidSequence(builder.toString());
    }

    @Override
    public Sequence removeGaps(int start, int end) {
        StringBuilder builder = new StringBuilder(sequence.substring(0, start));
        for (int i = start; i < end; i++) {
            if (sequence.charAt(i) != '-') {
                builder.append(sequence.charAt(i));
            }
        }
        builder.append(sequence.substring(end));
        return new AminoAcidSequence(builder.toString());
    }

    private static final class DefaultString {
        @Override
        public boolean equals(Object obj) {
            if (!(obj instanceof String)) {
                return false;
            }

            return ((String) obj).equals("");
        }
    }
}
