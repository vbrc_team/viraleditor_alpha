package Shared.interchange;

import java.util.Collections;
import java.util.EnumMap;
import java.util.List;
import java.util.Map;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonTypeInfo;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonPOJOBuilder;

@JsonDeserialize(builder = MatPeptide.Builder.class)
@JsonInclude(value = JsonInclude.Include.NON_DEFAULT)
@JsonTypeInfo(include = JsonTypeInfo.As.PROPERTY, use = JsonTypeInfo.Id.NAME, property = "type")
public class MatPeptide implements Feature {
    private List<Interval> intervals;
    private final String allele;
    private final Map<DbXRef, String> dbXref;
    private final String gene;
    private final String geneSynonym;
    private final String locusTag;
    private final String note;
    private final String oldLocusTag;
    private final String standardName;

    private final String ecNumber;
    private final String product;
    private final String pseudogene;

    private MatPeptide(Builder builder) {
        if (builder.intervals.isEmpty()) {
            throw new IllegalArgumentException();
        }

        this.intervals = builder.intervals;
        this.allele = builder.allele;
        this.dbXref = builder.dbXref;
        this.gene = builder.gene;
        this.geneSynonym = builder.geneSynonym;
        this.locusTag = builder.locusTag;
        this.note = builder.note;
        this.oldLocusTag = builder.oldLocusTag;
        this.standardName = builder.standardName;
        this.ecNumber = builder.ecNumber;
        this.product = builder.product;
        this.pseudogene = builder.pseudogene;
    }

    @Override
    @JsonIgnore
    public FeatureType getType() {
        return FeatureType.MAT_PEPTIDE;
    }

    @Override
    public List<Interval> getIntervals() {
        return intervals;
    }

    @Override
    public MatPeptide withIntervals(List<Interval> intervals) {
        return builder().copy(this).withIntervals(intervals).build();
    }

    public String getAllele() {
        return allele;
    }

    public Map<DbXRef, String> getDbXref() {
        return dbXref;
    }

    public String getGene() {
        return gene;
    }

    public String getGeneSynonym() {
        return geneSynonym;
    }

    public String getLocusTag() {
        return locusTag;
    }

    public String getNote() {
        return note;
    }

    public String getOldLocusTag() {
        return oldLocusTag;
    }

    public String getStandardName() {
        return standardName;
    }

    public String getEcNumber() {
        return ecNumber;
    }

    public String getProduct() {
        return product;
    }

    public String getPseudogene() {
        return pseudogene;
    }

    /**
     * Creates builder to build {@link MatPeptide}.
     * 
     * @return created builder
     */
    public static Builder builder() {
        return new Builder();
    }

    /**
     * Builder to build {@link MatPeptide}.
     */
    @JsonPOJOBuilder
    public static final class Builder {
        private static final String EMPTY = "";
        private List<Interval> intervals = Collections.emptyList();
        private String allele = EMPTY;
        private Map<DbXRef, String> dbXref = Collections.emptyMap();
        private String gene = EMPTY;
        private String geneSynonym = EMPTY;
        private String locusTag = EMPTY;
        private String note = EMPTY;
        private String oldLocusTag = EMPTY;
        private String standardName = EMPTY;
        private String ecNumber = EMPTY;
        private String product = EMPTY;
        private String pseudogene = EMPTY;

        private Builder() {
        }

        public Builder copy(MatPeptide peptide) {
            intervals = peptide.intervals;
            allele = peptide.allele;
            dbXref = peptide.dbXref;
            gene = peptide.gene;
            geneSynonym = peptide.geneSynonym;
            locusTag = peptide.locusTag;
            note = peptide.note;
            oldLocusTag = peptide.oldLocusTag;
            standardName = peptide.standardName;
            ecNumber = peptide.ecNumber;
            product = peptide.product;
            pseudogene = peptide.pseudogene;
            return this;
        }

        public Builder withIntervals(List<Interval> intervals) {
            if (intervals.isEmpty()) {
                throw new IllegalArgumentException();
            }

            this.intervals = intervals;
            return this;
        }

        public Builder withAllele(String allele) {
            this.allele = allele;
            return this;
        }

        public Builder withDbXref(Map<DbXRef, String> dbXref) {
            Map<DbXRef, String> map = new EnumMap<>(DbXRef.class);
            map.putAll(dbXref);
            this.dbXref = Collections.unmodifiableMap(map);
            return this;
        }

        public Builder withGene(String gene) {
            this.gene = gene;
            return this;
        }

        public Builder withGeneSynonym(String geneSynonym) {
            this.geneSynonym = geneSynonym;
            return this;
        }

        public Builder withLocusTag(String locusTag) {
            this.locusTag = locusTag;
            return this;
        }

        public Builder withNote(String note) {
            this.note = note;
            return this;
        }

        public Builder withOldLocusTag(String oldLocusTag) {
            this.oldLocusTag = oldLocusTag;
            return this;
        }

        public Builder withStandardName(String standardName) {
            this.standardName = standardName;
            return this;
        }

        public Builder withEcNumber(String ecNumber) {
            this.ecNumber = ecNumber;
            return this;
        }

        public Builder withProduct(String product) {
            this.product = product;
            return this;
        }

        public Builder withPseudogene(String pseudogene) {
            this.pseudogene = pseudogene;
            return this;
        }

        public MatPeptide build() {
            return new MatPeptide(this);
        }
    }

}
