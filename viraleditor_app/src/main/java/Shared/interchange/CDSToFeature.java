package Shared.interchange;

import java.util.EnumMap;
import java.util.List;
import java.util.Map;

import org.biojava.nbio.core.sequence.compound.NucleotideCompound;
import org.biojava.nbio.core.sequence.features.DBReferenceInfo;
import org.biojava.nbio.core.sequence.features.FeatureInterface;
import org.biojava.nbio.core.sequence.features.Qualifier;
import org.biojava.nbio.core.sequence.features.TextFeature;
import org.biojava.nbio.core.sequence.template.AbstractSequence;

/**
 * 
 * Convert between CDS objects and GenBank CDS features
 * <p>
 * These GenBank qualifiers are converted.
 * <ul>
 * <li>allele
 * <li>db_xref
 * <li>gene
 * <li>gene_synonym
 * <li>locus_tag
 * <li>note
 * <li>old_locus_tag
 * <li>standard_name
 * <li>codon_start
 * <li>EC_number
 * <li>exception
 * <li>number
 * <li>operon
 * <li>product
 * <li>protein_id
 * <li>pseudogene
 * <li>transl_except
 * <li>transl_table
 * <li>translation
 * </ul>
 */
public class CDSToFeature
        implements Convert<CDS, FeatureInterface<AbstractSequence<NucleotideCompound>, NucleotideCompound>> {
    private static final IntervalsToAbstractLocation convertLocation = new IntervalsToAbstractLocation();

    @Override
    public FeatureInterface<AbstractSequence<NucleotideCompound>, NucleotideCompound> to(CDS s) {
        final boolean withQuotes = true;

        FeatureInterface<AbstractSequence<NucleotideCompound>, NucleotideCompound> feature = new TextFeature<>("CDS",
                "", "", "");
        feature.setLocation(convertLocation.to(s.getIntervals()));

        if (!s.getAllele().equals("")) {
            feature.addQualifier("allele", new Qualifier("allele", s.getAllele(), withQuotes));
        }

        for (DbXRef databaseName : s.getDbXref().keySet()) {
            String database;
            switch (databaseName) {
            case GI:
                database = "GI";
                break;
            case GENE_ID:
                database = "GeneID";
                break;
            case TAXON:
                database = "taxon";
                break;
            default:
                throw new IllegalArgumentException(String.format("Missing case statement for %s", databaseName));
            }

            feature.addQualifier("db_xref", new DBReferenceInfo(database, s.getDbXref().get(databaseName)));
        }

        if (!s.getCodonStart().equals("")) {
            feature.addQualifier("codon_start", new Qualifier("codon_start", s.getCodonStart()));
        }

        if (!s.getEcNumber().equals("")) {
            feature.addQualifier("EC_number", new Qualifier("EC_number", s.getEcNumber()));
        }

        if (!s.getException().equals("")) {
            feature.addQualifier("exception", new Qualifier("exception", s.getException()));
        }

        if (!s.getGene().equals("")) {
            feature.addQualifier("gene", new Qualifier("gene", s.getGene()));
        }

        if (!s.getGeneSynonym().equals("")) {
            feature.addQualifier("gene_synonym", new Qualifier("gene_synonym", s.getGeneSynonym()));
        }

        if (!s.getLocusTag().equals("")) {
            feature.addQualifier("locus_tag", new Qualifier("locus_tag", s.getLocusTag()));
        }

        if (!s.getNote().equals("")) {
            feature.addQualifier("note", new Qualifier("note", s.getNote()));
        }

        if (!s.getNumber().equals("")) {
            feature.addQualifier("number", new Qualifier("number", s.getNumber()));
        }

        if (!s.getOldLocusTag().equals("")) {
            feature.addQualifier("old_locus_tag", new Qualifier("old_locus_tag", s.getOldLocusTag()));
        }

        if (!s.getOperon().equals("")) {
            feature.addQualifier("operon", new Qualifier("operon", s.getOperon()));
        }

        if (!s.getProduct().equals("")) {
            feature.addQualifier("product", new Qualifier("product", s.getProduct()));
        }

        if (!s.getProteinId().equals("")) {
            feature.addQualifier("protein_id", new Qualifier("protein_id", s.getProteinId()));
        }

        if (!s.getPseudogene().equals("")) {
            feature.addQualifier("pseudogene", new Qualifier("pseudogene", s.getPseudogene()));
        }

        if (!s.getStandardName().equals("")) {
            feature.addQualifier("standard_name", new Qualifier("standard_name", s.getStandardName()));
        }

        if (!s.getTranslation().equals("")) {
            feature.addQualifier("translation", new Qualifier("translation", s.getTranslation()));
        }

        if (!s.getTranslExcept().equals("")) {
            feature.addQualifier("transl_except", new Qualifier("transl_except", s.getTranslExcept()));
        }

        if (!s.getTranslTable().equals("")) {
            feature.addQualifier("transl_table", new Qualifier("transl_table", s.getTranslTable()));
        }

        return feature;
    }

    @Override
    public CDS from(FeatureInterface<AbstractSequence<NucleotideCompound>, NucleotideCompound> d) {
        List<Interval> intervals = convertLocation.from(d.getLocations());

        CDS.Builder builder = CDS.builder().withIntervals(intervals);
        for (String name : d.getQualifiers().keySet()) {
            List<Qualifier> qualifiers = d.getQualifiers().get(name);
            for (Qualifier qualifier : qualifiers) {
                Map<DbXRef, String> dbXrefs = new EnumMap<>(DbXRef.class);
                String value = qualifier.getValue();
                switch (name) {
                case "db_xref":
                    DBReferenceInfo dbxref = (DBReferenceInfo) qualifier;

                    switch (dbxref.getDatabase()) {
                    case "GI":
                        dbXrefs.put(DbXRef.GI, dbxref.getId());
                        break;
                    case "GeneID":
                        dbXrefs.put(DbXRef.GENE_ID, dbxref.getId());
                        break;
                    case "taxon":
                        dbXrefs.put(DbXRef.TAXON, dbxref.getId());
                        break;
                    default:
                        break;
                    }
                    break;
                case "allele":
                    builder.withAllele(value);
                    break;
                case "codon_start":
                    builder.withCodonStart(value);
                    break;
                case "EC_number":
                    builder.withEcNumber(value);
                    break;
                case "exception":
                    builder.withException(value);
                    break;
                case "gene":
                    builder.withGene(value);
                    break;
                case "gene_synonym":
                    builder.withGeneSynonym(value);
                    break;
                case "locus_tag":
                    builder.withLocusTag(value);
                    break;
                case "note":
                    builder.withNote(value);
                    break;
                case "number":
                    builder.withNumber(value);
                    break;
                case "old_locus_tag":
                    builder.withOldLocusTag(value);
                    break;
                case "operon":
                    builder.withOperon(value);
                    break;
                case "product":
                    builder.withProduct(value);
                    break;
                case "protein_id":
                    builder.withProteinId(value);
                    break;
                case "pseudogene":
                    builder.withPseudogene(value);
                    break;
                case "standard_name":
                    builder.withStandardName(value);
                    break;
                case "translation":
                    builder.withTranslation(value);
                    break;
                case "transl_except":
                    builder.withTranslExcept(value);
                    break;
                case "transl_table":
                    builder.withTranslTable(value);
                    break;
                default:
                    continue;
                }

                if (!dbXrefs.isEmpty()) {
                    builder.withDbXref(dbXrefs);
                }
            }
        }

        return builder.build();
    }

}
