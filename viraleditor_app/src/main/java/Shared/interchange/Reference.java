package Shared.interchange;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonProperty.Access;
import com.fasterxml.jackson.annotation.JsonTypeInfo;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonPOJOBuilder;

@JsonDeserialize(builder = Reference.Builder.class)
@JsonInclude(value = JsonInclude.Include.NON_DEFAULT)
@JsonTypeInfo(include = JsonTypeInfo.As.PROPERTY, use = JsonTypeInfo.Id.NAME, property = "type")
public class Reference {
    // GenBank
    private final String accession;
    private final String version;
    private final String identifier;
    private String sequence;

    // Sequence associated with the reference
    private final Sequence origin;

    // Interchange features
    // Needed for Jackson to serialize features without no-parameter getter
    @JsonProperty(access = Access.READ_ONLY)
    private final List<Feature> features;

    private Reference(Builder builder) {
        accession = builder.accession;
        version = builder.version;
        origin = builder.origin;
        features = builder.features;
        identifier = builder.identifier;
        sequence = builder.origin.getSequence();
    }

    /**
     * Returns a list containing all features of a given class.
     *
     * @param clazz the class of features to return
     * @param <T>   the type of features the list will contain
     */
    public <C extends Feature> List<C> getFeatures(Class<C> clazz) {
        List<C> castFeatures = new ArrayList<>();

        for (Feature feature : features) {
            C castFeature;
            try {
                castFeature = clazz.cast(feature);
            } catch (ClassCastException exception) {
                // Some features may not cast to clazz and should not be added to castFeatures.
                continue;
            }
            castFeatures.add(castFeature);
        }

        return castFeatures;
    }

    /**
     * Returns all features that can be converted to one of the types in
     * {@code types}.
     *
     * @param types types of features to return
     * @return all features that can be converted to one of the types in
     *         {@code types}
     */
    public List<Feature> getFeatures(List<Class<? extends Feature>> clazzes) {
        List<Feature> castedFeatures = new ArrayList<>();
        for (Feature feature : features) {
            for (Class<? extends Feature> clazz : clazzes) {
                try {
                    castedFeatures.add(clazz.cast(feature));
                    break;
                } catch (ClassCastException exception) {
                    // Try next type if cast fails
                }
            }
        }

        return castedFeatures;
    }

    /**
     * Returns the GenBank accession for the reference.
     *
     * @return the GenBank accession ID for the reference or an empty string
     */
    public String getAccession() {
        return accession;
    }

    /**
     * Returns the NCBI version for the reference.
     * <p>
     * GenBank stores the version appended to the accession id. For example GenBAnk
     * version U49845.1 has an accession of U49845. This stores the portion after
     * the period. In this case "1".
     *
     * @return the GenBank version number for the reference or an empty string
     */
    public String getVersion() {
        return version;
    }

    /**
     * A nucleotide sequence containing only the characters a, g, c, t and n.
     *
     * @return reference nucleotide sequence or an empty string is there is none
     */
    public Sequence getOrigin() {
        return origin;
    }
    
    public String getSequence() {
    	return sequence;
    }
    

    /**
     * Creates builder to build {@link Reference}.
     *
     * @return created builder
     */
    public static Builder builder() {
        return new Builder();
    }

    /**
     * Builder to build {@link Reference}.
     */
    @JsonPOJOBuilder
    public static final class Builder {
        private String accession = "";
        private String version = "";
        private String identifier;
        private Sequence origin = new NucleotideSequence(Collections.emptyList());
        private List<Feature> features = Collections.emptyList();

        private Builder() {
        }

        public Builder withAccession(String accession) {
            this.accession = accession;
            return this;
        }

        public Builder withVersion(String version) {
            this.version = version;
            return this;
        }

        public Builder withIdentifier(String identifier) {
            this.identifier = identifier;
            return this;
        }
        

        public Builder withOrigin(Sequence origin) {
            this.origin = origin;
            return this;
        }

        public Builder withFeatures(List<Feature> features) {
            this.features = features;
            return this;
        }

        public Builder withReference(Reference reference) {
            accession = reference.accession;
            version = reference.version;
            identifier = reference.identifier;
            origin = reference.origin;
            features = reference.features;
            return this;
        }

        public Reference build() {
            return new Reference(this);
        }
    }
}
