package Shared.interchange;

import java.util.Collections;
import java.util.HashMap;
import java.util.Map;

public enum AminoAcid implements Compound<AminoAcid> {
    /**
     * The 20 amino acids, X for unknown, and - for a gap.
     */
    A("A"), C("C"), D("D"), E("E"), F("F"), G("G"), H("H"), I("I"), K("K"), L("L"), M("M"), N("N"), P("P"), Q("Q"),
    R("R"), S("S"), T("T"), V("V"), W("W"), Y("Y"), X("X"), GAP("-");

    private static final Map<String, AminoAcid> ENUM_MAP;

    static {
        Map<String, AminoAcid> map = new HashMap<>();
        for (AminoAcid acid : AminoAcid.values()) {
            map.put(acid.getCode(), acid);
        }
        ENUM_MAP = Collections.unmodifiableMap(map);
    }

    /**
     * Single character ambiguity code that uniquely identifies the amino acid.
     */
    private final String code;

    private AminoAcid(String code) {
        this.code = code;
    }

    @Override
    public String getCode() {
        return code;
    }

    /**
     * Returns an AminoAcid from the single character ambiguity code.
     *
     * @param code the single character ambiguity code
     * @return a AminoAcid from the single character ambiguity code
     * @throws IllegalArgumentException if code is not an ambiguity code for an
     *                                  AminoAcid
     */
    public static AminoAcid fromCode(String code) {
        AminoAcid acid = ENUM_MAP.get(code);
        if (acid == null) {
            throw new IllegalArgumentException(String.format("%s is not a supported amino acid.", code));
        }
        return acid;
    }
}
