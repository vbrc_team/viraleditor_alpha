package Shared.interchange;

import java.util.EnumMap;
import java.util.List;
import java.util.Map;

import org.biojava.nbio.core.sequence.compound.NucleotideCompound;
import org.biojava.nbio.core.sequence.features.DBReferenceInfo;
import org.biojava.nbio.core.sequence.features.FeatureInterface;
import org.biojava.nbio.core.sequence.features.Qualifier;
import org.biojava.nbio.core.sequence.features.TextFeature;
import org.biojava.nbio.core.sequence.template.AbstractSequence;

/**
 * Convert between Source objects and GenBank source features.
 * <p>
 * These GenBank qualifiers are converted
 * <ul>
 * <li>db_xref
 * <li>host
 * <li>isolate
 * <li>isolation_source
 * <li>lab_host
 * <li>metagenome_source
 * <li>mol_type
 * <li>note
 * <li>organism
 * <li>PCR_primers
 * <li>segment
 * <li>serotype
 * <li>strain
 * <li>sub_species
 * <li>sub_strain
 * <li>tissue_type
 * </ul>
 */
public class SourceToFeature
        implements Convert<Source, FeatureInterface<AbstractSequence<NucleotideCompound>, NucleotideCompound>> {
    private static final IntervalsToAbstractLocation convertLocation = new IntervalsToAbstractLocation();

    @Override
    public FeatureInterface<AbstractSequence<NucleotideCompound>, NucleotideCompound> to(Source s) {
        FeatureInterface<AbstractSequence<NucleotideCompound>, NucleotideCompound> feature;
        feature = new TextFeature<>("source", "", "", "");
        feature.setLocation(convertLocation.to(s.getIntervals()));

        for (DbXRef databaseName : s.getDbXref().keySet()) {
            String database;
            switch (databaseName) {
            case GI:
                database = "GI";
                break;
            case GENE_ID:
                database = "GeneID";
                break;
            case TAXON:
                database = "taxon";
                break;
            default:
                throw new IllegalArgumentException(String.format("Missing case statement for %s", databaseName));
            }

            feature.addQualifier("db_xref", new DBReferenceInfo(database, s.getDbXref().get(databaseName)));
        }

        if (!s.getHost().equals("")) {
            feature.addQualifier("host", new Qualifier("host", s.getHost()));
        }

        if (!s.getIsolate().equals("")) {
            feature.addQualifier("isolate", new Qualifier("isolate", s.getIsolate()));
        }

        if (!s.getIsolationSource().equals("")) {
            feature.addQualifier("isolation_source", new Qualifier("isolation_source", s.getIsolationSource()));
        }

        if (!s.getLabHost().equals("")) {
            feature.addQualifier("lab_host", new Qualifier("lab_host", s.getLabHost()));
        }

        if (!s.getMetagenomeSource().equals("")) {
            feature.addQualifier("metagenome_source", new Qualifier("metagenome_source", s.getMetagenomeSource()));
        }

        if (!s.getMolType().equals("")) {
            feature.addQualifier("mol_type", new Qualifier("mol_type", s.getMolType()));
        }

        if (!s.getNote().equals("")) {
            feature.addQualifier("note", new Qualifier("note", s.getNote()));
        }

        if (!s.getOrganism().equals("")) {
            feature.addQualifier("organism", new Qualifier("organism", s.getOrganism()));
        }

        if (!s.getPCRPrimers().equals("")) {
            feature.addQualifier("PCR_primers", new Qualifier("PCR_primers", s.getPCRPrimers()));
        }

        if (!s.getSegment().equals("")) {
            feature.addQualifier("segment", new Qualifier("segment", s.getSegment()));
        }

        if (!s.getSerotype().equals("")) {
            feature.addQualifier("serotype", new Qualifier("serotype", s.getSerotype()));
        }

        if (!s.getStrain().equals("")) {
            feature.addQualifier("strain", new Qualifier("strain", s.getStrain()));
        }

        if (!s.getSubSpecies().equals("")) {
            feature.addQualifier("sub_species", new Qualifier("sub_species", s.getSubSpecies()));
        }

        if (!s.getSubStrain().equals("")) {
            feature.addQualifier("sub_strain", new Qualifier("sub_strain", s.getSubStrain()));
        }

        if (!s.getTissueType().equals("")) {
            feature.addQualifier("tissue_type", new Qualifier("tissue_type", s.getTissueType()));
        }

        return feature;
    }

    @Override
    public Source from(FeatureInterface<AbstractSequence<NucleotideCompound>, NucleotideCompound> d) {
        List<Interval> intervals = convertLocation.from(d.getLocations());

        Source.Builder builder = Source.builder().withIntervals(intervals);
        for (String name : d.getQualifiers().keySet()) {
            List<Qualifier> qualifiers = d.getQualifiers().get(name);
            for (Qualifier qualifier : qualifiers) {
                Map<DbXRef, String> dbXrefs = new EnumMap<>(DbXRef.class);
                String value = (qualifier.getValue());
                switch (name) {
                case "db_xref":
                    DBReferenceInfo dbxref = (DBReferenceInfo) qualifier;

                    switch (dbxref.getDatabase()) {
                    case "GI":
                        dbXrefs.put(DbXRef.GI, dbxref.getId());
                        break;
                    case "GeneID":
                        dbXrefs.put(DbXRef.GENE_ID, dbxref.getId());
                        break;
                    case "taxon":
                        dbXrefs.put(DbXRef.TAXON, dbxref.getId());
                        break;
                    default:
                        break;
                    }
                    break;
                case "host":
                    builder.withHost(value);
                    break;
                case "isolate":
                    builder.withIsolate(value);
                    break;
                case "isolation_source":
                    builder.withIsolationSource(value);
                    break;
                case "lab_host":
                    builder.withLabHost(value);
                    break;
                case "metagenome_source":
                    builder.withMetagenomeSource(value);
                    break;
                case "mol_type":
                    builder.withMolType(value);
                    break;
                case "note":
                    builder.withNote(value);
                    break;
                case "organism":
                    builder.withOrganism(value);
                    break;
                case "PCR_primers":
                    builder.withPCRPrimers(value);
                    break;
                case "segment":
                    builder.withSegment(value);
                    break;
                case "serotype":
                    builder.withSerotype(value);
                    break;
                case "strain":
                    builder.withStrain(value);
                    break;
                case "sub_species":
                    builder.withSubSpecies(value);
                    break;
                case "sub_strain":
                    builder.withSubStrain(value);
                    break;
                case "tissue_type":
                    builder.withTissueType(value);
                    break;
                default:
                    break;
                }

                if (!dbXrefs.isEmpty()) {
                    builder.withDbXref(dbXrefs);
                }
            }
        }

        return builder.build();
    }

}
