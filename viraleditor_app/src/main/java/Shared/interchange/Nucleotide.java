package Shared.interchange;

import java.util.Collections;
import java.util.HashMap;
import java.util.Map;

public enum Nucleotide implements Compound<Nucleotide> {
    /**
     * The 4 nucleotide, N for unknown, and - for a gap.
     */
    A("A"), C("C"), G("G"), T("T"), N("N"), GAP("-");

    private static final Map<String, Nucleotide> ENUM_MAP;

    static {
        Map<String, Nucleotide> map = new HashMap<>();
        for (Nucleotide nucleotide : Nucleotide.values()) {
            map.put(nucleotide.getCode(), nucleotide);
        }
        ENUM_MAP = Collections.unmodifiableMap(map);
    }

    /**
     * Single character ambiguity code that uniquely identifies the nucleotide.
     */
    private final String code;

    private Nucleotide(String code) {
        this.code = code;
    }

    @Override
    public String getCode() {
        return code;
    }

    /**
     * Returns a Nucleotide from the single character ambiguity code.
     *
     * @param code the single character ambiguity code
     * @return a Nucleotide from the single character ambiguity code
     * @throws IllegalArgumentException if code is not an ambiguity code for a
     *                                  Nucleotide
     */
    public static Nucleotide fromCode(String code) {
        Nucleotide nucleotide = ENUM_MAP.get(code);
        if (nucleotide == null) {
            throw new IllegalArgumentException(String.format("%s is not a supported nucleotide.", code));
        }
        return nucleotide;
    }
}
