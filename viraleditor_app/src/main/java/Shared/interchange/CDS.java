package Shared.interchange;

import java.util.Collections;
import java.util.EnumMap;
import java.util.List;
import java.util.Map;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonTypeInfo;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonPOJOBuilder;

@JsonDeserialize(builder = CDS.Builder.class)
@JsonInclude(value = JsonInclude.Include.NON_DEFAULT)
@JsonTypeInfo(include = JsonTypeInfo.As.PROPERTY, use = JsonTypeInfo.Id.NAME, property = "type")
public class CDS implements Feature {
    private List<Interval> intervals;
    private final String allele;
    private final Map<DbXRef, String> dbXref;
    private final String gene;
    private final String geneSynonym;
    private final String locusTag;
    private final String note;
    private final String oldLocusTag;
    private final String standardName;

    private final String codonStart;
    private final String ecNumber;
    private final String exception;
    private final String number;
    private final String operon;
    private final String product;
    private final String proteinId;
    private final String pseudogene;
    private final String translExcept;
    private final String translTable;
    private final String translation;

    private CDS(Builder builder) {
        if (builder.intervals.isEmpty()) {
            throw new IllegalArgumentException();
        }

        this.intervals = builder.intervals;
        this.allele = builder.allele;
        this.dbXref = builder.dbXref;
        this.gene = builder.gene;
        this.geneSynonym = builder.geneSynonym;
        this.locusTag = builder.locusTag;
        this.note = builder.note;
        this.oldLocusTag = builder.oldLocusTag;
        this.standardName = builder.standardName;
        this.codonStart = builder.codonStart;
        this.ecNumber = builder.ecNumber;
        this.exception = builder.exception;
        this.number = builder.number;
        this.operon = builder.operon;
        this.product = builder.product;
        this.proteinId = builder.proteinId;
        this.pseudogene = builder.pseudogene;
        this.translExcept = builder.translExcept;
        this.translTable = builder.translTable;
        this.translation = builder.translation;
    }

    @Override
    @JsonIgnore
    public FeatureType getType() {
        return FeatureType.CDS;
    }

    @Override
    public List<Interval> getIntervals() {
        return intervals;
    }

    @Override
    public CDS withIntervals(List<Interval> intervals) {
        return builder().copy(this).withIntervals(intervals).build();
    }

    public String getAllele() {
        return allele;
    }

    public Map<DbXRef, String> getDbXref() {
        return dbXref;
    }

    public String getGene() {
        return gene;
    }

    public String getGeneSynonym() {
        return geneSynonym;
    }

    public String getLocusTag() {
        return locusTag;
    }

    public String getNote() {
        return note;
    }

    public String getOldLocusTag() {
        return oldLocusTag;
    }

    public String getStandardName() {
        return standardName;
    }

    public String getCodonStart() {
        return codonStart;
    }

    public String getEcNumber() {
        return ecNumber;
    }

    public String getException() {
        return exception;
    }

    public String getNumber() {
        return number;
    }

    public String getOperon() {
        return operon;
    }

    public String getProduct() {
        return product;
    }

    public String getProteinId() {
        return proteinId;
    }

    public String getPseudogene() {
        return pseudogene;
    }

    public String getTranslExcept() {
        return translExcept;
    }

    public String getTranslTable() {
        return translTable;
    }

    public String getTranslation() {
        return translation;
    }

    /**
     * Creates builder to build {@link CDS}.
     * 
     * @return created builder
     */
    public static Builder builder() {
        return new Builder();
    }

    /**
     * Builder to build {@link CDS}.
     */
    @JsonPOJOBuilder
    public static final class Builder {
        private static final String EMPTY = "";
        private List<Interval> intervals = Collections.emptyList();
        private String allele = EMPTY;
        private Map<DbXRef, String> dbXref = Collections.emptyMap();
        private String gene = EMPTY;
        private String geneSynonym = EMPTY;
        private String locusTag = EMPTY;
        private String note = EMPTY;
        private String oldLocusTag = EMPTY;
        private String standardName = EMPTY;
        private String codonStart = EMPTY;
        private String ecNumber = EMPTY;
        private String exception = EMPTY;
        private String number = EMPTY;
        private String operon = EMPTY;
        private String product = EMPTY;
        private String proteinId = EMPTY;
        private String pseudogene = EMPTY;
        private String translExcept = EMPTY;
        private String translTable = EMPTY;
        private String translation = EMPTY;

        private Builder() {
        }

        public Builder copy(CDS cds) {
            intervals = cds.intervals;
            allele = cds.allele;
            dbXref = cds.dbXref;
            gene = cds.gene;
            geneSynonym = cds.geneSynonym;
            locusTag = cds.locusTag;
            note = cds.note;
            oldLocusTag = cds.oldLocusTag;
            standardName = cds.standardName;
            codonStart = cds.codonStart;
            ecNumber = cds.ecNumber;
            exception = cds.exception;
            number = cds.number;
            operon = cds.operon;
            product = cds.product;
            proteinId = cds.proteinId;
            pseudogene = cds.pseudogene;
            translExcept = cds.translExcept;
            translTable = cds.translTable;
            translation = cds.translation;
            return this;
        }

        public Builder withIntervals(List<Interval> intervals) {
            if (intervals.isEmpty()) {
                throw new IllegalArgumentException();
            }

            this.intervals = intervals;
            return this;
        }

        public Builder withAllele(String allele) {
            this.allele = allele;
            return this;
        }

        public Builder withDbXref(Map<DbXRef, String> dbXref) {
            Map<DbXRef, String> map = new EnumMap<>(DbXRef.class);
            map.putAll(dbXref);
            this.dbXref = Collections.unmodifiableMap(map);
            this.dbXref = dbXref;
            return this;
        }

        public Builder withGene(String gene) {
            this.gene = gene;
            return this;
        }

        public Builder withGeneSynonym(String geneSynonym) {
            this.geneSynonym = geneSynonym;
            return this;
        }

        public Builder withLocusTag(String locusTag) {
            this.locusTag = locusTag;
            return this;
        }

        public Builder withNote(String note) {
            this.note = note;
            return this;
        }

        public Builder withOldLocusTag(String oldLocusTag) {
            this.oldLocusTag = oldLocusTag;
            return this;
        }

        public Builder withStandardName(String standardName) {
            this.standardName = standardName;
            return this;
        }

        public Builder withCodonStart(String codonStart) {
            this.codonStart = codonStart;
            return this;
        }

        public Builder withEcNumber(String ecNumber) {
            this.ecNumber = ecNumber;
            return this;
        }

        public Builder withException(String exception) {
            this.exception = exception;
            return this;
        }

        public Builder withNumber(String number) {
            this.number = number;
            return this;
        }

        public Builder withOperon(String operon) {
            this.operon = operon;
            return this;
        }

        public Builder withProduct(String product) {
            this.product = product;
            return this;
        }

        public Builder withProteinId(String proteinId) {
            this.proteinId = proteinId;
            return this;
        }

        public Builder withPseudogene(String pseudogene) {
            this.pseudogene = pseudogene;
            return this;
        }

        public Builder withTranslExcept(String translExcept) {
            this.translExcept = translExcept;
            return this;
        }

        public Builder withTranslTable(String translTable) {
            this.translTable = translTable;
            return this;
        }

        public Builder withTranslation(String translation) {
            this.translation = translation;
            return this;
        }

        public CDS build() {
            return new CDS(this);
        }
    }
}
