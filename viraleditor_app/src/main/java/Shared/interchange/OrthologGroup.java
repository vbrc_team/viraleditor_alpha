package Shared.interchange;

import java.util.Collections;
import java.util.List;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonTypeInfo;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonPOJOBuilder;

/**
 * Stores data from the VBRC database ortholog group table.
 */
@JsonDeserialize(builder = OrthologGroup.Builder.class)
@JsonInclude(value = JsonInclude.Include.NON_DEFAULT)
@JsonTypeInfo(include = JsonTypeInfo.As.PROPERTY, use = JsonTypeInfo.Id.NAME, property = "type")
public final class OrthologGroup implements Feature {
    private List<Interval> intervals;

    private final int familyID;
    private final int orthologGroupID;
    private final String vocAbbr;
    private final String vocFunction;
    private final String refAbbrs;
    private final String name;

    private OrthologGroup(Builder builder) {
        if (builder.intervals.isEmpty()) {
            throw new IllegalArgumentException();
        }

        this.intervals = builder.intervals;
        this.familyID = builder.familyID;
        this.orthologGroupID = builder.orthologGroupID;
        this.vocAbbr = builder.vocAbbr;
        this.vocFunction = builder.vocFunction;
        this.refAbbrs = builder.refAbbrs;
        this.name = builder.name;
    }

    @Override
    @JsonIgnore
    public FeatureType getType() {
        return FeatureType.ORTHOLOG_GROUP;
    }

    @Override
    public List<Interval> getIntervals() {
        return Collections.unmodifiableList(intervals);
    }

    @Override
    public OrthologGroup withIntervals(List<Interval> intervals) {
        return builder().copy(this).withIntervals(intervals).build();
    }

    /**
     * The id of the family that the ortholog group belongs to.
     * <p>
     * A non-negative integer.
     *
     * @return the family id
     */
    public int getFamilyID() {
        return familyID;
    }

    /**
     * The id for the ortholog group.
     * <p>
     * A non-negative integer.
     *
     * @return the ortholog id
     */
    public int getOrthologGroupID() {
        return orthologGroupID;
    }

    public String getVocAbbr() {
        return vocAbbr;
    }

    /**
     * A more detailed description of the ortholog group.
     *
     * @return the Voc function or an empty string
     */
    public String getVocFunction() {
        return vocFunction;
    }

    public String getRefAbbrs() {
        return refAbbrs;
    }

    /**
     * Returns the name of the ortholog group.
     *
     * @return the name of the ortholog group
     */
    public String getName() {
        return name;
    }

    /**
     * Creates builder to build {@link OrthologGroup}.
     *
     * @return created builder
     */
    public static Builder builder() {
        return new Builder();
    }

    /**
     * Builder to build {@link OrthologGroup}.
     */
    @JsonPOJOBuilder
    public static final class Builder {
        private List<Interval> intervals = Collections.emptyList();
        private int familyID;
        private int orthologGroupID;
        private String vocAbbr;
        private String vocFunction;
        private String refAbbrs;
        private String name;

        private Builder() {
            String emptryString = "";
            vocAbbr = emptryString;
            vocFunction = emptryString;
            refAbbrs = emptryString;
            name = emptryString;
        }

        public Builder copy(OrthologGroup group) {
            intervals = group.intervals;
            familyID = group.familyID;
            orthologGroupID = group.orthologGroupID;
            vocAbbr = group.vocAbbr;
            vocFunction = group.vocFunction;
            refAbbrs = group.refAbbrs;
            name = group.name;
            return this;
        }

        public Builder withIntervals(List<Interval> intervals) {
            if (intervals.isEmpty()) {
                throw new IllegalArgumentException("Empty intervals");
            }

            this.intervals = intervals;
            return this;
        }

        public Builder withFamilyID(int familyID) {
            if (familyID <= 0) {
                throw new IllegalArgumentException(String.format("Family id %d < 0", familyID));
            }

            this.familyID = familyID;
            return this;
        }

        public Builder withOrthologGroupID(int orthologGroupID) {
            if (orthologGroupID <= 0) {
                throw new IllegalArgumentException(String.format("Ortholog group id %d < 0", orthologGroupID));
            }

            this.orthologGroupID = orthologGroupID;
            return this;
        }

        public Builder withVocAbbr(String vocAbbr) {
            this.vocAbbr = vocAbbr;
            return this;
        }

        public Builder withVocFunction(String vocFunction) {
            this.vocFunction = vocFunction;
            return this;
        }

        public Builder withRefAbbrs(String refAbbrs) {
            this.refAbbrs = refAbbrs;
            return this;
        }

        public Builder withName(String name) {
            this.name = name;
            return this;
        }

        public OrthologGroup build() {
            return new OrthologGroup(this);
        }
    }

}
