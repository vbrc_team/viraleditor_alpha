package Shared.interchange;

import java.util.List;

import com.fasterxml.jackson.databind.util.StdConverter;

public class StringToNucleotidesConverter extends StdConverter<String, List<Nucleotide>> {
    private static final StringToNucleotides toNucleotides = new StringToNucleotides();

    @Override
    public List<Nucleotide> convert(String value) {
        return toNucleotides.to(value);
    }
}
