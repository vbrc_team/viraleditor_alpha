package Shared.interchange;

import java.util.Collections;
import java.util.List;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;

public class NucleotideSequence implements Sequence {
    @JsonInclude(value = JsonInclude.Include.CUSTOM, valueFilter = DefaultString.class)
    @JsonDeserialize(converter = StringToNucleotidesConverter.class)
    private final String sequence;

    /**
     * Constructs a nucleotide sequence.
     *
     * @param seq a nucleotide sequence
     */
    public NucleotideSequence(
            @JsonProperty("sequence") List<Nucleotide> nucleotides) {
        // Jackson can pass null value when deserializing
        if (nucleotides == null) {
            nucleotides = Collections.emptyList();
        }
        StringBuilder builder = new StringBuilder();
        for (Nucleotide nucleotide : nucleotides) {
            builder.append(nucleotide.getCode());
        }
        sequence = builder.toString();
    }

    /**
     * Constructs a nucleotide sequence without input validation.
     * 
     * @param seq a nucleotide sequence
     */
    protected NucleotideSequence(String seq) {
        sequence = seq;
    }

    @Override
    public String getSequence() {
        return sequence;
    }

    @Override
    public SequenceType getType() {
        return SequenceType.NUCLEOTIDE;
    }
    
    @Override
    public Sequence subSequence(int start, int end) {
    	StringBuilder builder = new StringBuilder(sequence.substring(start, end));
    	return new NucleotideSequence(builder.toString());
    }

    @Override
    public Sequence insertGaps(int index, int m) {
        StringBuilder builder = new StringBuilder(sequence.substring(0, index));
        for (int i = 0; i < m; i++) {
            builder.append("-");
        }
        builder.append(sequence.substring(index));
        return new NucleotideSequence(builder.toString());
    }
    

    @Override
    public Sequence removeGaps(int start, int end) {
        StringBuilder builder = new StringBuilder(sequence.substring(0, start));
        for (int i = start; i < end; i++) {
            if (!Nucleotide.GAP.getCode().equals("" + sequence.charAt(i))) {
                builder.append(sequence.charAt(i));
            }
        }
        builder.append(sequence.substring(end));
        return new NucleotideSequence(builder.toString());
    }

    private static final class DefaultString {
        @Override
        public boolean equals(Object obj) {
            if (!(obj instanceof String)) {
                return false;
            }

            return ((String) obj).equals("");
        }
    }
}
