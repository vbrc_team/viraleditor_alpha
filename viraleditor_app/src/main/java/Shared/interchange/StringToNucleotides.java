package Shared.interchange;

import java.util.ArrayList;
import java.util.List;

public class StringToNucleotides implements Convert<String, List<Nucleotide>> {
    @Override
    public List<Nucleotide> to(String s) {
        List<Nucleotide> nucleotides = new ArrayList<>();
        for (int i = 0; i < s.length(); i++) {
            nucleotides.add(Nucleotide.fromCode("" + s.charAt(i)));
        }
        return nucleotides;
    }

    @Override
    public String from(List<Nucleotide> nucleotides) {
        StringBuilder builder = new StringBuilder();
        nucleotides.stream().forEach(n -> builder.append(n.getCode()));
        return builder.toString();
    }
}
