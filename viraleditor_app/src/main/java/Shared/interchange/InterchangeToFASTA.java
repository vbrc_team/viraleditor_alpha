package Shared.interchange;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.ListIterator;

public class InterchangeToFASTA implements Convert<Interchange, String> {
    private static final StringToNucleotides toNucleotides = new StringToNucleotides();
    private static final StringToAminoAcids toAminoAcids = new StringToAminoAcids();
    private static final int CHARS_PER_LINE = 80;
    private static final String NEW_LINE = "\n";

    @Override
    public String to(Interchange s) {
        StringBuilder builder = new StringBuilder();

        for (Reference ref : s.getReferences()) {
            String description = generateDescription(ref);
            builder.append(description);
            builder.append(NEW_LINE);

            String origin = ref.getOrigin().getSequence();
            for (int line = 0, end = origin.length() / CHARS_PER_LINE; line < end; line++) {
                builder.append(origin.substring(line * CHARS_PER_LINE, (line + 1) * CHARS_PER_LINE));
                builder.append(NEW_LINE);
            }

            if (origin.length() % CHARS_PER_LINE != 0) {
                int start = origin.length() - (origin.length() % CHARS_PER_LINE);
                builder.append(origin.substring(start, origin.length()));
                builder.append(NEW_LINE);
            }
        }

        return builder.toString();
    }

    /**
     * Generate a FASTA description line using NCBI identifiers.
     */
    private String generateDescription(Reference reference) {
        StringBuilder builder = new StringBuilder(">");
        List<Description> descriptions = reference.getFeatures(Description.class);
        List<Genome> genomes = reference.getFeatures(Genome.class);

        if (descriptions.size() == 1) {
            builder.append(descriptions.get(0).getDescription());
        } else if (genomes.size() == 1) {
            Genome genome = genomes.get(0);

            builder.append("lcl|");

            builder.append(genome.getOrganismAbbrevation());
            builder.append("|");

            builder.append(reference.getAccession());
            builder.append("|");

            builder.append(genome.getOrganismName());
        }

        return builder.toString();
    }

    @Override
    public Interchange from(String d) {
        List<String> lines = Arrays.asList(d.split("\\r?\\n"));

        if (!lines.isEmpty() && !lines.get(0).startsWith(">")) {
            throw new IllegalArgumentException("FASTA file must start with >");
        }

        List<Reference> refs = new ArrayList<>();
        List<Feature> features = new ArrayList<>();
        ListIterator<String> it = lines.listIterator();
        List<Interval> intervals = new ArrayList<>();
        intervals.add(new Interval(false, 1, 2));
        while (it.hasNext()) {
            String line = it.next();
            if (line.startsWith(">")) {
                features.add(Description.builder().withDescription(line.substring(1)).withIntervals(intervals).build());
            } else {
                it.previous();
                Sequence sequence = readSequence(it);
                refs.add(Reference.builder().withOrigin(sequence).withFeatures(features).build());
                features = new ArrayList<>();
            }
        }

        return Interchange.builder().withReferences(refs).build();
    }

    private Sequence readSequence(ListIterator<String> it) {
        StringBuilder builder = new StringBuilder();

        while (it.hasNext()) {
            String line = it.next();

            if (line.startsWith(">")) {
                // Backup iterator so outer loop can read again
                it.previous();
                break;
            } else {
                builder.append(line.toUpperCase());
            }
        }

        // String is either a nucleotide or amino acid sequence
        String sequence = builder.toString();
        try {
            return new NucleotideSequence(toNucleotides.to(sequence));
        } catch (IllegalArgumentException ex) {
            // Ignore exception and try amino acid
        }

        return new AminoAcidSequence(toAminoAcids.to(sequence));
    }

}
