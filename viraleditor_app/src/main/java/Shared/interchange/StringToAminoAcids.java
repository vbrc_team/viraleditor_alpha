package Shared.interchange;

import java.util.ArrayList;
import java.util.List;

public class StringToAminoAcids implements Convert<String, List<AminoAcid>> {
    @Override
    public List<AminoAcid> to(String s) {
        List<AminoAcid> acids = new ArrayList<>();
        for (int i = 0; i < s.length(); i++) {
            acids.add(AminoAcid.fromCode("" + s.charAt(i)));
        }
        return acids;
    }

    @Override
    public String from(List<AminoAcid> acids) {
        StringBuilder builder = new StringBuilder();
        acids.stream().forEach(n -> builder.append(n.getCode()));
        return builder.toString();
    }
}
