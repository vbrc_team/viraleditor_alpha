package Shared.interchange;

import java.util.List;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonSubTypes;
import com.fasterxml.jackson.annotation.JsonTypeInfo;

@JsonTypeInfo(include = JsonTypeInfo.As.PROPERTY, use = JsonTypeInfo.Id.NAME, property = "type")
@JsonSubTypes({ @JsonSubTypes.Type(value = Family.class),
                @JsonSubTypes.Type(value = Annotation.class),
                @JsonSubTypes.Type(value = Genome.class), 
                @JsonSubTypes.Type(value = OrthologGroup.class),
                @JsonSubTypes.Type(value = TaxonomyNode.class),
                @JsonSubTypes.Type(value = CDS.class),
                @JsonSubTypes.Type(value = Gene.class), 
                @JsonSubTypes.Type(value = MatPeptide.class),
                @JsonSubTypes.Type(value = Source.class) })
public interface Feature {
    /**
     * Returns the type of feature.
     * <p>
     * FeatureType must be unique per class.
     * 
     * @return the feature type
     */
    @JsonIgnore
    public FeatureType getType();

    /**
     * Return an unmodifiable list containing the features intervals.
     * <p>
     * A feature must have at least one interval.
     * 
     * @return a List of one or more intervals
     */
    public List<Interval> getIntervals();

    /**
     * Returns a copy of the {@code Feature} with intervals replaced by
     * {@code intervals}.
     */
    public Feature withIntervals(List<Interval> intervals);
}
