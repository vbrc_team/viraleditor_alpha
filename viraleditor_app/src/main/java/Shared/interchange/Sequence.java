package Shared.interchange;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonSubTypes;
import com.fasterxml.jackson.annotation.JsonTypeInfo;

@JsonInclude(value = JsonInclude.Include.NON_DEFAULT)
@JsonTypeInfo(include = JsonTypeInfo.As.PROPERTY, use = JsonTypeInfo.Id.NAME, property = "type")
@JsonSubTypes({ @JsonSubTypes.Type(value = NucleotideSequence.class, name = "NucleotideSequence"),
        @JsonSubTypes.Type(value = AminoAcidSequence.class, name = "AminoAcidSequence") })
public interface Sequence {
    /**
     * Returns a sequence with possible gaps.
     * 
     * @return a sequence with possible gaps
     */
    public String getSequence();

    /**
     * Returns the type of sequence stored.
     * 
     * @return the type of sequence stored
     */
    @JsonIgnore
    public SequenceType getType();

    /**
     * Returns new sequence with {@code m} gaps inserted at {@code index}.
     * 
     * @param index index to insert gaps at
     * @param m     number of gaps to insert
     */
    public Sequence insertGaps(int index, int m);

    /**
     * Returns new sequence with any gaps deleted from {@code start} to {@code end}.
     * 
     * @param index inclusive to start gap removal
     * @param end   exclusive index to end gap removal
     */
    public Sequence removeGaps(int start, int end);


/**
 * returns a new sequence that is a subsequence of the original sequence from {@code start} to {@code end}.
 * @param start
 * @param end
 * @return
 */
	public Sequence subSequence(int start, int end);
}
