package Shared.interchange;

/**
 * Types of biological sequences.
 */
public enum SequenceType {
    NUCLEOTIDE, AMINO_ACID;
}
