package Shared.interchange;

import java.util.EnumMap;
import java.util.List;
import java.util.Map;

import org.biojava.nbio.core.sequence.compound.NucleotideCompound;
import org.biojava.nbio.core.sequence.features.DBReferenceInfo;
import org.biojava.nbio.core.sequence.features.FeatureInterface;
import org.biojava.nbio.core.sequence.features.Qualifier;
import org.biojava.nbio.core.sequence.features.TextFeature;
import org.biojava.nbio.core.sequence.template.AbstractSequence;

/**
 * Convert between Gene objects and GenBank gene features.
 * <p>
 * These GenBank qualifiers are converted
 * <ul>
 * <li>allele
 * <li>db_xref
 * <li>gene
 * <li>gene_synonym
 * <li>locus_tag
 * <li>note
 * <li>old_locus_tag
 * <li>standard_name
 * <li>operon
 * <li>product
 * <li>pseudogene
 * </ul>
 */
public class GeneToFeature
        implements Convert<Gene, FeatureInterface<AbstractSequence<NucleotideCompound>, NucleotideCompound>> {
    private static final IntervalsToAbstractLocation convertLocation = new IntervalsToAbstractLocation();

    @Override
    public FeatureInterface<AbstractSequence<NucleotideCompound>, NucleotideCompound> to(Gene s) {
        final boolean withQuotes = true;

        FeatureInterface<AbstractSequence<NucleotideCompound>, NucleotideCompound> feature = new TextFeature<>("gene",
                "", "", "");
        feature.setLocation(convertLocation.to(s.getIntervals()));

        if (!s.getAllele().equals("")) {
            feature.addQualifier("allele", new Qualifier("allele", s.getAllele(), withQuotes));
        }

        for (DbXRef databaseName : s.getDbXref().keySet()) {
            String database;
            switch (databaseName) {
            case GI:
                database = "GI";
                break;
            case GENE_ID:
                database = "GeneID";
                break;
            case TAXON:
                database = "taxon";
                break;
            default:
                throw new IllegalArgumentException(String.format("Missing case statement for %s", databaseName));
            }

            feature.addQualifier("db_xref", new DBReferenceInfo(database, s.getDbXref().get(databaseName)));
        }

        if (!s.getGene().equals("")) {
            feature.addQualifier("gene", new Qualifier("gene", s.getGene(), withQuotes));
        }

        if (!s.getGeneSynonym().equals("")) {
            feature.addQualifier("gene_synonym", new Qualifier("gene_synonym", s.getGeneSynonym(), withQuotes));
        }

        if (!s.getLocusTag().equals("")) {
            feature.addQualifier("locus_tag", new Qualifier("locus_tag", s.getLocusTag(), withQuotes));
        }

        if (!s.getNote().equals("")) {
            feature.addQualifier("note", new Qualifier("note", s.getNote(), withQuotes));
        }

        if (!s.getOldLocusTag().equals("")) {
            feature.addQualifier("old_locus_tag", new Qualifier("old_locus_tag", s.getOldLocusTag(), withQuotes));
        }

        if (!s.getStandardName().equals("")) {
            feature.addQualifier("standard_name", new Qualifier("standard_name", s.getStandardName(), withQuotes));
        }

        if (!s.getProduct().equals("")) {
            feature.addQualifier("operon", new Qualifier("operon", s.getOperon(), withQuotes));
        }

        if (!s.getProduct().equals("")) {
            feature.addQualifier("product", new Qualifier("product", s.getProduct(), withQuotes));
        }

        if (!s.getPseudogene().equals("")) {
            feature.addQualifier("pseudogene", new Qualifier("pseudogene", s.getPseudogene(), withQuotes));
        }

        return feature;
    }

    @Override
    public Gene from(FeatureInterface<AbstractSequence<NucleotideCompound>, NucleotideCompound> d) {
        List<Interval> intervals = convertLocation.from(d.getLocations());

        Gene.Builder builder = Gene.builder().withIntervals(intervals);
        for (String name : d.getQualifiers().keySet()) {
            List<Qualifier> qualifiers = d.getQualifiers().get(name);
            for (Qualifier qualifier : qualifiers) {
                Map<DbXRef, String> dbXrefs = new EnumMap<>(DbXRef.class);
                String value = (qualifier.getValue());
                switch (name) {
                case "operon":
                    builder.withOperon(value);
                    break;
                case "product":
                    builder.withProduct(value);
                    break;
                case "pseudogene":
                    builder.withPseudogene(value);
                    break;
                case "db_xref":
                    DBReferenceInfo dbxref = (DBReferenceInfo) qualifier;

                    switch (dbxref.getDatabase()) {
                    case "GI":
                        dbXrefs.put(DbXRef.GI, dbxref.getId());
                        break;
                    case "GeneID":
                        dbXrefs.put(DbXRef.GENE_ID, dbxref.getId());
                        break;
                    case "taxon":
                        dbXrefs.put(DbXRef.TAXON, dbxref.getId());
                        break;
                    default:
                        break;
                    }
                    break;
                case "allele":
                    builder.withAllele(value);
                    break;
                case "gene":
                    builder.withGene(value);
                    break;
                case "gene_synonym":
                    builder.withGeneSynonym(value);
                    break;
                case "locus_tag":
                    builder.withLocusTag(value);
                    break;
                case "note":
                    builder.withNote(value);
                    break;
                case "old_locus_tag":
                    builder.withOldLocusTag(value);
                    break;
                case "standard_name":
                    builder.withStandardName(value);
                    break;
                default:
                    break;
                }

                if (!dbXrefs.isEmpty()) {
                    builder.withDbXref(dbXrefs);
                }
            }
        }

        return builder.build();
    }

}
