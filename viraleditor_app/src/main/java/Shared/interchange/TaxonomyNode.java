package Shared.interchange;

import java.util.Collections;
import java.util.List;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonTypeInfo;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;

/**
 * Represents a node in a taxonomy tree. Stores the tree with edges pointing to
 * the nodes parent. This is due to lookups starting at leaf nodes and moving
 * towards the root.
 */
@JsonDeserialize(builder = TaxonomyNode.Builder.class)
@JsonInclude(value = JsonInclude.Include.NON_DEFAULT)
@JsonTypeInfo(include = JsonTypeInfo.As.PROPERTY, use = JsonTypeInfo.Id.NAME, property = "type")
public class TaxonomyNode implements Feature {
    private List<Interval> intervals;
    private final int nodeID;
    private final int parentNodeID;
    private final int familyID;
    private final int genusID;
    private final int speciesID;
    private final String name;

    private TaxonomyNode(Builder builder) {
        if (builder.intervals.isEmpty()) {
            throw new IllegalArgumentException();
        }

        this.intervals = builder.intervals;
        this.nodeID = builder.nodeID;
        this.parentNodeID = builder.parentNodeID;
        this.familyID = builder.familyID;
        this.speciesID = builder.speciesID;
        this.genusID = builder.genusID;
        this.name = builder.name;
    }

    @Override
    @JsonIgnore
    public FeatureType getType() {
        return FeatureType.TAXONOMY_NODE;
    }

    @Override
    public List<Interval> getIntervals() {
        return Collections.unmodifiableList(intervals);
    }

    @Override
    public TaxonomyNode withIntervals(List<Interval> intervals) {
        return builder().copy(this).withIntervals(intervals).build();
    }

    /**
     * Returns the node id.
     * <p>
     * A non-negative integer. The id uniquely identifies the node.
     *
     * @return the node id which is a key in the VBRC database
     */
    public int getNodeID() {
        return nodeID;
    }

    /**
     * Returns the id of the parent node to this one.
     * <p>
     * A non-negative integer.
     *
     * @return the family id
     */
    public int getParentNodeID() {
        return parentNodeID;
    }

    /**
     * Returns the id of the family that the node belongs to.
     * <p>
     * A non-negative integer.
     *
     * @return the family id
     */
    public int getFamilyID() {
        return familyID;
    }

    /**
     * Returns the id of the species that the node belongs to.
     * <p>
     * A non-negative integer.
     *
     * @return the species id
     */
    public int getSpeciesID() {
        return speciesID;
    }

    /**
     * Returns the id of the genus that the node belongs to.
     * <p>
     * A non-negative integer.
     *
     * @return the genus id
     */
    public int getGenusID() {
        return genusID;
    }

    /**
     * Returns the name of the taxonomy level the node represents.
     *
     * @return the name of the taxonomy level the node represents
     */
    public String getName() {
        return name;
    }

    /**
     * Creates builder to build {@link TaxonomyNode}.
     *
     * @return created builder
     */
    public static Builder builder() {
        return new Builder();
    }

    /**
     * Builder to build {@link TaxonomyNode}.
     */
    public static final class Builder {
        private List<Interval> intervals = Collections.emptyList();
        private int nodeID;
        private int parentNodeID;
        private int familyID;
        private int speciesID;
        private int genusID;
        private String name;

        private Builder() {
            name = "";
        }

        public Builder copy(TaxonomyNode node) {
            intervals = node.intervals;
            nodeID = node.nodeID;
            parentNodeID = node.parentNodeID;
            familyID = node.familyID;
            speciesID = node.speciesID;
            genusID = node.genusID;
            name = node.name;
            return this;
        }

        public Builder withIntervals(List<Interval> intervals) {
            if (intervals.isEmpty()) {
                throw new IllegalArgumentException("Empty intervals");
            }

            this.intervals = intervals;
            return this;
        }

        public Builder withNodeID(int nodeID) {
            if (nodeID < 0) {
                throw new IllegalArgumentException(String.format("Node id %d < 0", nodeID));
            }

            this.nodeID = nodeID;
            return this;
        }

        public Builder withParentNodeID(int parentNodeID) {
            if (parentNodeID < 0) {
                throw new IllegalArgumentException(String.format("Parent node id %d < 0", parentNodeID));
            }

            this.parentNodeID = parentNodeID;
            return this;
        }

        public Builder withFamilyID(int familyID) {
            if (familyID < 0) {
                throw new IllegalArgumentException(String.format("Family id %d < 0", familyID));
            }

            this.familyID = familyID;
            return this;
        }

        public Builder withSpeciesID(int speciesID) {
            if (speciesID < 0) {
                throw new IllegalArgumentException(String.format("Species id %d < 0", speciesID));
            }

            this.speciesID = speciesID;
            return this;
        }

        public Builder withGenusID(int genusID) {
            if (genusID < 0) {
                throw new IllegalArgumentException(String.format("Genus id %d < 0", genusID));
            }

            this.genusID = genusID;
            return this;
        }

        public Builder withName(String name) {
            this.name = name;
            return this;
        }

        public TaxonomyNode build() {
            return new TaxonomyNode(this);
        }
    }

}
