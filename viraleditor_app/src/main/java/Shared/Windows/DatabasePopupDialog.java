package Shared.Windows;

import java.util.ArrayList;
import java.util.LinkedHashSet;
import java.util.List;

import Shared.Handling.DatabaseHandler;
import Shared.interchange.*;
import javafx.collections.ListChangeListener;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.SelectionMode;
import javafx.scene.control.TreeItem;
import javafx.scene.control.TreeView;
import javafx.stage.Stage;

public class DatabasePopupDialog {

    @FXML
    private TreeView<Reference> VirusList;
    private ArrayList<Reference> selectedViruses;
    private ArrayList<Reference> sharedViruses;

    @FXML
    private Button closeButton;

    /**
     * Opens a new window for the user to select viruses from the database, returns
     * when window is closed
     * 
     * @param database - name of the database to be opened
     * @return the users selection when "ok" is selected; empty list is returned if
     *         window is canceled or force quit
     */
    public static List<Reference> OpenPopUp(Family database) {
        List<Reference> viruses = new ArrayList<>();
        // Open window for selection
        try {
            FXMLLoader loader = new FXMLLoader(
                    DatabasePopupDialog.class.getClassLoader().getResource("DatabasePopup.fxml"));
            Parent root = loader.load();
            DatabasePopupDialog controller = loader.getController();
            // returns reference to the data
            viruses = controller.setUp(database);

            Stage popup = new Stage();
            popup.setTitle("Add Viruses");
            popup.setScene(new Scene(root));
            popup.showAndWait();

        } catch (Exception e) {
            e.printStackTrace();
        }
        return viruses;
    }

    private List<Reference> setUp(Family database) {
        List<Feature> features = new ArrayList<>();
        List<Interval> intervals = new ArrayList<>();
        intervals.add(new Interval(false, 1, 2));
        features.add(Family.builder().withIntervals(intervals).withFamilyName("Root").build());
        Reference root = Reference.builder().withFeatures(features).build();

        TreeItem<Reference> dbRootNode = new TreeItem<>(root);
        dbRootNode.setExpanded(true);

        DatabaseHandler dbConnection = new DatabaseHandler();
        List<Reference> data = dbConnection.getDummyData();
        for (Reference reference : data) {
            dbRootNode.getChildren().add(new TreeItem<>(reference));
        }

        VirusList.setRoot(dbRootNode);
        VirusList.getSelectionModel().setSelectionMode(SelectionMode.MULTIPLE);
        VirusList.getSelectionModel().getSelectedItems().addListener(dbChange);

        sharedViruses = new ArrayList<>();
        return sharedViruses;
    }

    // Do not move, this does not work as a lambda
    ListChangeListener<TreeItem<Reference>> dbChange = new ListChangeListener<TreeItem<Reference>>() {
        @Override
        public void onChanged(Change c) {
            VirusList.getSelectionModel().getSelectedItems().removeListener(this);
            handleTreeCellSelction();
            VirusList.getSelectionModel().getSelectedItems().addListener(this);
        }
    };

    // checks if children of parents need to be selected
    private void handleTreeCellSelction() {
        List<Reference> viruses = new ArrayList<>();

        List<TreeItem<Reference>> nodes = new ArrayList<>();
        nodes.addAll(VirusList.getSelectionModel().getSelectedItems());

        while (!nodes.isEmpty()) {
            TreeItem<Reference> item = nodes.remove(0);
            // sometimes the list contains null pointers
            if (item == null) {
                continue;
            } else if (item.getValue().getFeatures(Genome.class).isEmpty()) {
                nodes.addAll(item.getChildren());
            } else {
                viruses.add(item.getValue());
            }
        }

        // remove duplicate objects
        selectedViruses = new ArrayList<>(new LinkedHashSet<>(viruses));
    }

    @FXML
    void handleCancel(ActionEvent event) {
        Stage stage = (Stage) closeButton.getScene().getWindow();
        stage.close();
    }

    @FXML
    void handleOK(ActionEvent event) {
        sharedViruses.clear();
        sharedViruses.addAll(selectedViruses);
        Stage stage = (Stage) closeButton.getScene().getWindow();
        stage.close();
    }

}
