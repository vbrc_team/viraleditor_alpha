package Shared.Windows;

import javafx.application.Platform;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;

import java.util.concurrent.FutureTask;

public class LoadingPopupDialog {

    @FXML
    private javafx.scene.text.Text Text;

    /**
     * Opens a loading window
     */
    public static LoadingPopupDialog OpenPopUp(String message) {
        FXMLLoader loader;
        LoadingPopupDialog controller = null;

        try {
            loader = new FXMLLoader(LoadingPopupDialog.class.getClassLoader().getResource("LoadingPopup.fxml"));
            Parent root = loader.load();
            controller = loader.getController();
            Stage popup = new Stage();

            LoadingPopupDialog finalController = controller;

            new Thread(() -> Platform.runLater(() -> {
                System.out.println("worker going");
                popup.setTitle("Loading");
                finalController.Text.setText(message);
                popup.setScene(new Scene(root));
            })).start();


            popup.show();

        } catch (Exception e) {
            e.printStackTrace();
        }
        return controller;
    }

    public void closeWindow(){
        Stage stage = (Stage) this.Text.getScene().getWindow();
        stage.close();
    }



}
