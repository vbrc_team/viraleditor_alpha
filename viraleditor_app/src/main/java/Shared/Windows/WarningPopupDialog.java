package Shared.Windows;

import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.text.Text;
import javafx.stage.Stage;

public class WarningPopupDialog {
	
	@FXML
    private Button okButton;

    @FXML
    private Text warningField;

    
    public void initial(String warning) {
    	warningField.setText(warning);
    }
    
    @FXML
    void onOkClicked() {
    	((Stage) okButton.getScene().getWindow()).close();
    }

}
