package Shared;

/**
 * this class holds constants that define sequences
 */
public final class SequenceConstants {
    public static final String[] nucleotides = { "A", "C", "T", "G" };
    public static final String[] nucleotideNames = { "Adenine", "Cytosine", "Thymine", "Guanine" };
    public static final String[] nucleotideAbbrev = { "Ade", "Cyt", "Thy", "Gua" };
    public static final String[] dinucleotides = { "AA", "AC", "AT", "AG", "CA", "CC", "CT", "CG", "GA", "GC", "GT",
            "GG", "TA", "TC", "TT", "TG" };
    public static final String[] aminoAcids = { "A", "C", "D", "E", "F", "G", "H", "I", "K", "L", "M", "N", "P", "Q",
            "R", "S", "T", "U", "V", "W", "Y", "X" };
    public static final String[] aminoAcidNames = { "Alanine", "Cysteine", "Aspartate", "Glutamate", "Phenylalanine",
            "Glycine", "Histidine", "Isoleucine", "Lysine", "Leucine", "Methionine", "Asparagine", "Proline",
            "Glutamine", "Arginine", "Serine", "Threonine", "Selenocysteine", "Valine", "Tryptophan", "Tyrosine",
            "Unknown" };
    public static final String[] aminoAcidAbbrev = { "Ala", "Cys", "Asp", "Glu", "Phe", "Gly", "His", "Ile", "Lys",
            "Leu", "Met", "Asn", "Pro", "Gln", "Arg", "Ser", "Thr", "Sec", "Val", "Trp", "Tyr", "Unknown" };
    public static final String[] aminoAcidWeights = { "A", "89.09", "C", "121.16", "D", "133.10", "E", "147.13", "F",
            "165.19", "G", "75.07", "H", "155.16", "I", "131.18", "K", "146.19", "L", "131.18", "M", "149.21", "N",
            "132.12", "P", "115.13", "Q", "146.15", "R", "174.20", "S", "105.09", "T", "119.12", "V", "117.15", "W",
            "204.23", "Y", "181.19", "U", "150.04", "X", "135.00", "J", "131.18", "Z", "146.64", "B", "132.61" };
    public static final String[] pka_pkbs = // pKa/pKbs (< & > are default NT & CT)
            { "C", "9.0", "D", "4.05", "E", "4.45", "Y", "10.0", "H", "5.98", "K", "10.0", "R", "12.0", "<", "7.5", ">",
                    "3.55" };
    public static final String[] nt_terminus_pi = // N Terminus Isoelectric Point Values
            { "A", "7.59", "E", "7.70", "M", "7.00", "P", "8.36", "S", "6.93", "T", "6.82", "V", "7.44" };
    public static final String[] ct_terminus_pi = // C Terminus Isoelectric Point Values
            { "D", "4.55", "E", "4.75" };
    public static final String[] codons = { "TTT", "TTC", "TTA", "TTG", "TAT", "TAC", "TAA", "TAG", "CTT", "CTC", "CTA",
            "CTG", "CAT", "CAC", "CAA", "CAG", "ATT", "ATC", "ATA", "ATG", "AAT", "AAC", "AAA", "AAG", "GTT", "GTC",
            "GTA", "GTG", "GAT", "GAC", "GAA", "GAG", "TCT", "TCC", "TCA", "TCG", "TGT", "TGC", "TGA", "TGG", "CCT",
            "CCC", "CCA", "CCG", "CGT", "CGC", "CGA", "CGG", "ACT", "ACC", "ACA", "ACG", "AGT", "AGC", "AGA", "AGG",
            "GCT", "GCC", "GCA", "GCG", "GGT", "GGC", "GGA", "GGG" };
    public static final String[] codonCode = { "F", "F", "L", "L", "Y", "Y", "X", "X", "L", "L", "L", "L", "H", "H",
            "Q", "Q", "I", "I", "I", "M", "N", "N", "K", "K", "V", "V", "V", "V", "D", "D", "E", "E", "S", "S", "S",
            "S", "C", "C", "U", "W", "P", "P", "P", "P", "R", "R", "R", "R", "T", "T", "T", "T", "S", "S", "R", "R",
            "A", "A", "A", "A", "G", "G", "G", "G" };
    public static final String[] codonAbbrev = { "Phe", "Phe", "Leu", "Leu", "Tyr", "Tyr", "Och", "Amb", "Leu", "Leu",
            "Leu", "Leu", "His", "His", "Gln", "Gln", "Ile", "Ile", "Ile", "Met", "Asn", "Asn", "Lys", "Lys", "Val",
            "Val", "Val", "Val", "Asp", "Asp", "Glu", "Glu", "Ser", "Ser", "Ser", "Ser", "Cys", "Cys", "Opa", "Trp",
            "Pro", "Pro", "Pro", "Pro", "Arg", "Arg", "Arg", "Arg", "Thr", "Thr", "Thr", "Thr", "Ser", "Ser", "Arg",
            "Arg", "Ala", "Ala", "Ala", "Ala", "Gly", "Gly", "Gly", "Gly" };
}
