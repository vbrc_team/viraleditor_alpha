package VisualSummary;

import java.util.List;

import Shared.AbstractDialog;
import VisualSummary.structures.DialogActionType;
import VisualSummary.structures.VisualSummaryFeature;
import VisualSummary.structures.VisualSummaryFeatureType;
import javafx.animation.AnimationTimer;
import javafx.beans.value.ObservableValue;
import javafx.fxml.FXML;
import javafx.geometry.VPos;
import javafx.scene.canvas.Canvas;
import javafx.scene.canvas.GraphicsContext;
import javafx.scene.control.ScrollBar;
import javafx.scene.control.Slider;
import javafx.scene.layout.Pane;
import javafx.scene.paint.Color;
import javafx.scene.paint.Paint;
import javafx.scene.text.Font;
import javafx.scene.text.Text;

public class VisualSummaryDialog extends AbstractDialog<VisualSummaryComposer> {
	private static final double FONT_SIZE = 15.0;
	private double FONT_WIDTH;
	private double FONT_HEIGHT = FONT_SIZE;
	/**
	 * True when there is a new frame to draw and false otherwise.
	 */
	private boolean refreshFrame;
	private DialogActionType action;
	/**
	 * Draws new frame whenever {@link refreshFrame} is true.
	 */
	private AnimationTimer drawTimer = new AnimationTimer() {
		@Override
		public void handle(long now) {
			if (refreshFrame) {
				resetCanvas(sequencesDisplay);
				resetCanvas(namesDisplay);
				drawNamesFrame();
				drawFeaturesFrame();
				drawCursor();
				drawHoverCursor();
				refreshFrame = false;
			}
		}
	};
	
	@FXML
	private Pane namesDisplayParent;
	@FXML
	private Canvas namesDisplay;
	@FXML
	private Pane sequencesDisplayParent;
	@FXML
	private Canvas sequencesDisplay;
	@FXML
	private ScrollBar namesHorizontalScroll;
	@FXML
	private ScrollBar sequencesHorizontalScroll;
	@FXML
	private ScrollBar verticalScroll;
	@FXML
	private Slider zoom;

	public DialogActionType getAction() {
		return action;
	}
	
	public double getSequencesDisplayWidth() {
		return sequencesDisplay.getWidth();
	}

	public double getZoom() {
		return zoom.getValue();
	}

	public double getMaxZoom() {
		return zoom.getMax();
	}

	public double getSequencesHorizontalScroll() {
		return sequencesHorizontalScroll.getValue();
	}
	
	public double getMaxSequencesHorizontalScroll() {
		return sequencesHorizontalScroll.getMax();
	}

	public double getMinSequencesHorizontalScroll() {
		return sequencesHorizontalScroll.getMin();
	}
	
	public void setSequencesHorizontalScroll(double value) {
		sequencesHorizontalScroll.setValue(value);
	}

	public void setMaxSequencesHorizontalScroll(double value) {
		sequencesHorizontalScroll.setMax(value);
	}
	
	public void setBlockSequencesHorizontalScroll(double value) {
		sequencesHorizontalScroll.setBlockIncrement(value);
	}
	
	public void setUnitSequencesHorizontalScroll(double value) {
		sequencesHorizontalScroll.setUnitIncrement(value);
	}

	@Override
	public void mountChildPerformances() {
	}

	@Override
	public void initializeDialog() {
		stage.setTitle("Visual Summary");
		FONT_WIDTH = textWidth("A");
		FONT_HEIGHT = textHeight("A");
		refreshFrame = false;
		drawTimer.start();
		action = DialogActionType.OTHER;
	}

	@FXML
	void initialize() {
		assert namesDisplayParent != null : "fx:id=\"sequencesNamesParentPane\" was not injected: check your FXML file 'VisualSummary'.";
		assert namesDisplay != null : "fx:id=\"sequencesnamesDisplay\" was not injected: check your FXML file 'VisualSummary.fxml'.";
		assert sequencesDisplayParent != null : "fx:id=\"sequencesDisplayParent\" was not injected: check your FXML file 'VisualSummary.fxml'.";
		assert sequencesDisplay != null : "fx:id=\"sequencesDisplay\" was not injected: check your FXML file 'VisualSummary.fxml'.";
		assert namesHorizontalScroll != null : "fx:id=\"namesHorizontalScroll\" was not injected: check your FXML file 'VisualSummary.fxml'.";
		assert sequencesHorizontalScroll != null : "fx:id=\"sequencesHorizontalScroll\" was not injected: check your FXML file 'VisualSummary.fxml'.";
		assert verticalScroll != null : "fx:id=\"verticalScroll\" was not injected: check your FXML file 'VisualSummary.fxml'.";
		assert zoom != null : "fx:id=\"zoom\" was not injected: check your FXML file 'VisualSummary.fxml'.";

		initializeNamesDisplayParent();
		initializeNamesDisplay();
		initializeSequencesDisplayParent();
		initializeSequencesDisplay();
		initializeNamesHorizontalScroll();
		initializeSequencesHorizontalScroll();
		initializeVerticalScroll();
		initializeZoom();
	}

	private void initializeNamesDisplayParent() {
		namesDisplayParent.widthProperty().addListener((ObservableValue<? extends Number> observable, Number oldValue, Number newValue) -> {
			namesDisplay.setWidth(newValue.doubleValue());
			refreshFrame = true;
		});
		namesDisplayParent.heightProperty().addListener((ObservableValue<? extends Number> observable, Number oldValue, Number newValue) -> {
			if (newValue.doubleValue() <= FONT_HEIGHT) {
				namesDisplay.setHeight(0);
			} else {
				namesDisplay.setHeight(newValue.doubleValue());
			}
			refreshFrame = true;
		});
	}

	private void initializeNamesDisplay() {
	}

	private void initializeSequencesDisplayParent() {
		sequencesDisplayParent.widthProperty().addListener((ObservableValue<? extends Number> observable, Number oldValue, Number newValue) -> {
			action = DialogActionType.RESIZING;
			sequencesDisplay.setWidth(newValue.doubleValue());
			refreshFrame = true;
		});
		sequencesDisplayParent.heightProperty().addListener((ObservableValue<? extends Number> observable, Number oldValue, Number newValue) -> {
			if (newValue.doubleValue() <= FONT_HEIGHT) {
				sequencesDisplay.setHeight(0);
			} else {
				sequencesDisplay.setHeight(newValue.doubleValue());
			}
			refreshFrame = true;
		});
	}

	private void initializeSequencesDisplay() {
		sequencesDisplay.setOnMouseClicked((event) -> {
			if(!refreshFrame) {
				action = DialogActionType.OTHER;
				composer.updateCursor(event.getX());
				refreshFrame = true;
			}
		});
		sequencesDisplay.setOnMouseMoved((event) -> {
			if(!refreshFrame) {
				action = DialogActionType.OTHER;
				composer.updateHoverCursor(event.getX());
				refreshFrame = true;
			}
		});
	}

	private void initializeNamesHorizontalScroll() {
		namesHorizontalScroll.valueProperty().addListener((ObservableValue<? extends Number> observable, Number oldValue, Number newValue) -> {
			if (!refreshFrame) {
				refreshFrame = true;
			}
		});
	}

	private void initializeSequencesHorizontalScroll() {
		sequencesHorizontalScroll.valueProperty().addListener((ObservableValue<? extends Number> observable, Number oldValue, Number newValue) -> {
			if (!refreshFrame) {
				action = DialogActionType.SCROLLING;
				refreshFrame = true;
			}
		});
	}

	private void initializeVerticalScroll() {
		verticalScroll.valueProperty().addListener((ObservableValue<? extends Number> observable, Number oldValue, Number newValue) -> {
			if (!refreshFrame) {
				refreshFrame = true;
			}
		});
		verticalScroll.setBlockIncrement(FONT_HEIGHT);
		verticalScroll.setUnitIncrement(FONT_HEIGHT);
	}

	private void initializeZoom() {
		zoom.valueProperty().addListener((ObservableValue<? extends Number> observable, Number oldValue, Number newValue) -> {
			if (!refreshFrame) {
				action = DialogActionType.ZOOMING;
				refreshFrame = true;
			}
		});
	}

	private void resetCanvas(Canvas canvas) {
		GraphicsContext gc = canvas.getGraphicsContext2D();
		gc.clearRect(0, 0, canvas.getWidth(), canvas.getHeight());
		gc.setTextBaseline(VPos.TOP);
		gc.setFont(Font.font("Roboto Mono", FONT_SIZE));
	}

	private void drawFeaturesFrame() {
		List<List<VisualSummaryFeature>> displayFrame = composer.getFeaturesFrame();
		GraphicsContext context = sequencesDisplay.getGraphicsContext2D();
		for (int rowNum = 0; rowNum < displayFrame.size(); rowNum++) {
			List<VisualSummaryFeature> row = displayFrame.get(rowNum);
			for (VisualSummaryFeature visualSummaryFeature : row) {
				Paint initialFill = context.getFill();
				if (visualSummaryFeature.getType() == VisualSummaryFeatureType.GENE) {
					if (visualSummaryFeature.isComplement()) {
						context.setFill(Color.BLUE.desaturate());
					} else {
						context.setFill(Color.RED.desaturate());
					}
				}
				if (visualSummaryFeature.getType() == VisualSummaryFeatureType.DIFFERENCE) {
					context.setFill(Color.PURPLE.desaturate());
				}
				double rectXPosition = visualSummaryFeature.getLow();
				double rectYPosition = rowNum * FONT_HEIGHT;
				double rectWidth = visualSummaryFeature.getHigh() - visualSummaryFeature.getLow();
				double rectHeight = 0.95 * FONT_HEIGHT;
				context.fillRect(rectXPosition, rectYPosition, rectWidth, rectHeight);
				context.setFill(initialFill);
			}
		}
	}

	private void drawNamesFrame() {
		List<String> names = composer.getNamesFrame();
		final int numberOfColumns = Math.min(composer.getMaxNameLength(), (int) Math.ceil(namesDisplay.getWidth() / FONT_WIDTH));
		final int startColumn = (int) Math.floor(namesHorizontalScroll.getValue() / FONT_WIDTH);
		double rowOffset = ((verticalScroll.getValue() % FONT_HEIGHT) + FONT_HEIGHT) % FONT_HEIGHT;
		double columnOffset = ((namesHorizontalScroll.getValue() % FONT_WIDTH) + FONT_WIDTH) % FONT_WIDTH;
		GraphicsContext gc = namesDisplay.getGraphicsContext2D();
		for (int row = 0; row < names.size(); row++) {
			String name = names.get(row);
			for (int column = startColumn; column - startColumn < numberOfColumns && column < name.length(); column++) {
				String string = name.substring(column, column + 1);
				double stringXPosition = (column - startColumn) * FONT_WIDTH - columnOffset;
				double stringYPosition = (row * FONT_HEIGHT) - rowOffset;
				gc.fillText(string, stringXPosition, stringYPosition);
			}
		}
	}

	private void drawCursor() {
		double displayCursorPosition = composer.getDisplayCursor();
		double displayCursorHeight = composer.getRowCount() * FONT_HEIGHT;
		GraphicsContext context = sequencesDisplay.getGraphicsContext2D();
		Paint initialFill = context.getFill();
		context.setFill(Color.ORANGE.brighter());
		context.fillRect(displayCursorPosition, 0, sequencesDisplay.getWidth() / 1000, displayCursorHeight);
		context.setFill(initialFill);
	}

	private void drawHoverCursor() {
		double displayCursorPosition = composer.getDisplayHoverCursor();
		double displayCursorHeight = composer.getRowCount() * FONT_HEIGHT;
		GraphicsContext context = sequencesDisplay.getGraphicsContext2D();
		Paint initialFill = context.getFill();
		context.setFill(Color.GREY.brighter());
		context.fillRect(displayCursorPosition, 0, sequencesDisplay.getWidth() / 1000, displayCursorHeight);
		context.setFill(initialFill);
	}

	private static double textWidth(String string) {
		final Text text = new Text(string);
		text.setFont(Font.font("Roboto Mono", FONT_SIZE));
		return Math.floor(text.getBoundsInLocal().getWidth());
	}
	
	private static double textHeight(String string) {
		final Text text = new Text(string);
		text.setFont(Font.font("Roboto Mono", FONT_SIZE));
		return Math.floor(text.getBoundsInLocal().getHeight());
	}
	
	@FXML
	private void center() {
		if (!refreshFrame) {
			action = DialogActionType.CENTER;
			refreshFrame = true;
		}
	}
}