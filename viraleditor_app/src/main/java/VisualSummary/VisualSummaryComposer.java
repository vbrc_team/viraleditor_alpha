package VisualSummary;

import java.util.ArrayList;
import java.util.List;

import Shared.AbstractComposer;
import Shared.interchange.Annotation;
import Shared.interchange.Description;
import Shared.interchange.Feature;
import Shared.interchange.Gene;
import Shared.interchange.Genome;
import Shared.interchange.Reference;
import Shared.interchange.Source;
import VisualSummary.structures.VisualSummaryFeature;
import VisualSummary.structures.VisualSummarySequence;

public class VisualSummaryComposer extends AbstractComposer<VisualSummaryDialog> {    		
	/**
     * Contains data for all VisualSummarySequences.
     * <p>
     * Each feature uses interval coordinates which are normalized to the length of the longest sequence in {@link sequences}.
     */
	private List<VisualSummarySequence> sequences;
	private double cursor; // Cursor position relative to 0 (cannot be negative). Uses normalized coordinate system.
	private double hoverCursor; // Cursor position relative to 0 (cannot be negative). Uses normalized coordinate system.
	private double displayCursor;
	private double displayHoverCursor;
    /**
     * Length of longest sequence name in {@link #sequences}.
     */
    private int maxNameLength;
    /**
     * Length of longest sequence in {@link #sequences}.
     */
    private int maxSequenceLength;
    private int rowCount;
    private double zoomFactor;
    
    public VisualSummaryComposer(List<Reference> sequences) {
    	this.sequences = new ArrayList<>();
    	cursor = 0;
    	hoverCursor = 0;
    	zoomFactor = 1;
    	maxNameLength = 0;
    	maxSequenceLength = 0;
    	for (Reference sequence : sequences) {
			String name = getReferenceName(sequence);
			String rawSequence = sequence.getOrigin().getSequence();
	        if (name.length() > maxNameLength) {
	        	maxNameLength = name.length();
	        }
	        if (rawSequence.length() > maxSequenceLength) {
	        	maxSequenceLength = rawSequence.length();
	        }
			String rawReferenceSequence;
	        if (this.sequences.size() == 0) {
	        	rawReferenceSequence = rawSequence;
	        }
	        else {
	        	rawReferenceSequence = this.sequences.get(0).getSequence();
	        }
	        List<Class<? extends Feature>> geneClasses = new ArrayList<>();
	        geneClasses.add(Annotation.class);
	        geneClasses.add(Gene.class);
	        List<Feature> genes = sequence.getFeatures(geneClasses);
	        VisualSummarySequence newSequence = new VisualSummarySequence(name, rawSequence, rawReferenceSequence, genes);
	        rowCount += newSequence.getRowCount();
	        this.sequences.add(newSequence);
    	}
    }
   
   public int getRowCount() {
	   return rowCount;
   }
   
   public int getMaxNameLength() {
	   return maxNameLength;
   }
   
   public double getDisplayCursor() {
	   return displayCursor;
   }
	
   public double getDisplayHoverCursor() {
	   return displayHoverCursor;
   }
   
   public void updateCursor(double displayCursor) {
	   double horizontalScroll = dialog.getSequencesHorizontalScroll();
	   double displayWidth = dialog.getSequencesDisplayWidth();
	   double scaledDisplayWidth = displayWidth * zoomFactor;
	   double scale = 1 / scaledDisplayWidth;
	   double shift = -1 * horizontalScroll;
	   double offset = 0;
	   cursor = transform(displayCursor, scale, shift, offset);
   }   

   public void updateHoverCursor(double displayHoverCursor) {
	   double horizontalScroll = dialog.getSequencesHorizontalScroll();
	   double displayWidth = dialog.getSequencesDisplayWidth();
	   double scaledDisplayWidth = displayWidth * zoomFactor;
	   double scale = 1 / scaledDisplayWidth;
	   double shift = -1 * horizontalScroll;
	   double offset = 0;
	   hoverCursor = transform(displayHoverCursor, scale, shift, offset);
   }
   
   public List<String> getNamesFrame() {
	   List<String> names = new ArrayList<>();
	   String underline = new String();
	   for (int index = 0; index < maxNameLength; index++) {
		   underline = underline.concat("_");
	   }
	   for (VisualSummarySequence sequence : sequences) {
		   names.add(sequence.getName());
		   names.add("-> Genes");
		   for (int index = 0; index < sequence.getRowCount() - 3; index++) {
			   names.add("\n");
		   }
		   names.add(underline);
	   }
	   return names;
   }
   
   /**
    * @return longest sequence name length in {@link #sequences}.
    */
   public List<List<VisualSummaryFeature>> getFeaturesFrame() {
	   List<List<VisualSummaryFeature>> allFeatures = new ArrayList<>();
	   double displayWidth = dialog.getSequencesDisplayWidth();
	   double scaledDisplayWidth = displayWidth * zoomFactor;
	   double horizontalScroll = dialog.getSequencesHorizontalScroll();
	   double horizontalScrollMax;
	   double shift;
	   double offset;
	   switch(dialog.getAction()) {
	   case CENTER:
	   case ZOOMING:
		   zoomFactor = Math.pow(Math.pow(dialog.getMaxZoom(), 1 / dialog.getMaxZoom()), dialog.getZoom());
		   scaledDisplayWidth = displayWidth * zoomFactor;
		   dialog.setMaxSequencesHorizontalScroll(scaledDisplayWidth - displayWidth);
		   horizontalScrollMax = dialog.getMaxSequencesHorizontalScroll();
		   double horizontalScrollMin = dialog.getMinSequencesHorizontalScroll();
		   dialog.setBlockSequencesHorizontalScroll(0.1 * displayWidth);
		   dialog.setUnitSequencesHorizontalScroll(0.01 * displayWidth);
		   shift = cursor;
		   offset = 0.5 * displayWidth;
		   double newScroll = scaledDisplayWidth * shift - offset;
		   if (newScroll < horizontalScrollMin) {
			   newScroll = horizontalScrollMin;
			   shift = 0;
			   offset = 0;
		   }
		   if (newScroll > horizontalScrollMax) {
			   newScroll = horizontalScrollMax;
			   shift = 1;
			   offset = displayWidth;
		   }
		   dialog.setSequencesHorizontalScroll(newScroll);
		   break;
	   case RESIZING:
		   if (zoomFactor > 1) {
			   horizontalScrollMax = dialog.getMaxSequencesHorizontalScroll();
			   double oldDisplayWidth = horizontalScrollMax / (zoomFactor - 1);
			   double oldScaledDisplayWidth = zoomFactor * oldDisplayWidth;
			   double scrollRatio = horizontalScroll / oldScaledDisplayWidth;
			   newScroll = scrollRatio * scaledDisplayWidth;
			   dialog.setMaxSequencesHorizontalScroll(scaledDisplayWidth - displayWidth);
			   dialog.setSequencesHorizontalScroll(newScroll);
			   shift = 0;
			   offset = -1 * newScroll;
			   break;
		   }
       default:
           shift = 0;
           offset = -1 * horizontalScroll;
	   }
       displayCursor = transform(cursor, scaledDisplayWidth, shift, offset);
       displayHoverCursor = transform(hoverCursor, scaledDisplayWidth, shift, offset);
	   for (VisualSummarySequence sequence : sequences) {
		   List<List<VisualSummaryFeature>> features = sequence.getFeaturesFrame();
		   for (List<VisualSummaryFeature> featuresRow : features) {
    		   List<VisualSummaryFeature> newFeaturesRow = new ArrayList<>();
			   for (VisualSummaryFeature feature : featuresRow) {
				   double absoluteLow = feature.getLow() / (double) maxSequenceLength;
			       double displayLow = transform(absoluteLow, scaledDisplayWidth, shift, offset);
				   double absoluteHigh = feature.getHigh() / (double) maxSequenceLength;
				   double displayHigh = transform(absoluteHigh, scaledDisplayWidth, shift, offset);
			       boolean isComplement = feature.isComplement();
			       newFeaturesRow.add(new VisualSummaryFeature(feature.getType(), displayLow, displayHigh, isComplement));
    		   }
			   allFeatures.add(newFeaturesRow);
		   }
	   }
       return allFeatures;
   }
   
   private double transform(double position, double scale, double shift, double offset) {
	   return scale * (position - shift) + offset;
   }
 
   /**
    * Returns the name of the reference sequences.
    * <p>
    * The name is one of
    * <ul>
    * <li>FASTA description line
    * <li>Organism name from database
    * <li>Organism name from GenBank file
    * <li>the empty string when the previous are not present.
    * 
    * @param reference
    * @return name of the reference
    */
   private String getReferenceName(Reference reference) {
       String name;
       if (!reference.getFeatures(Description.class).isEmpty()) {
           name = reference.getFeatures(Description.class).get(0).getDescription();
       } else if (!reference.getFeatures(Genome.class).isEmpty()) {
           name = reference.getFeatures(Genome.class).get(0).getOrganismName();
       } else if (!reference.getFeatures(Source.class).isEmpty()) {
           name = reference.getFeatures(Source.class).get(0).getOrganism();
       } else {
           name = "";
       }
       return name;
   }
}