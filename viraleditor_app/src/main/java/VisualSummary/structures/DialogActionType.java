package VisualSummary.structures;

public enum DialogActionType {
	ZOOMING, SCROLLING, RESIZING, CENTER, OTHER
}
