package VisualSummary.structures;

public enum VisualSummaryFeatureType {
    GENE, DIFFERENCE;
}
