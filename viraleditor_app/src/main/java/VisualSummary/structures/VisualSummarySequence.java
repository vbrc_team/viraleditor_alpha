package VisualSummary.structures;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import Shared.interchange.Feature;

public class VisualSummarySequence {
	private final String name;
	private final String sequence;
	/**
     * Contains all rows of all features for a single sequence.
     * <p>
     * Each feature uses interval coordinates which are normalized to the length of the longest sequence in {@link sequences}.
     */
	private final List<List<VisualSummaryFeature>> featuresFrame;
	
	private class SortByLow implements Comparator<VisualSummaryFeature> {
		public int compare(VisualSummaryFeature first, VisualSummaryFeature second) {
			return (int)(first.getLow() - second.getLow());
		}
	}
	
	public VisualSummarySequence(String name, String sequence, String referenceSequence, List<Feature> genes) {
		this.name = name;
		this.sequence = sequence;
		featuresFrame = new ArrayList<>();
		addDifferences(sequence, referenceSequence);
		addGenes(genes);
	}
	
	public String getName() {
		return name;
	}
	
	public String getSequence() {
		return sequence;
	}
	
	public List<List<VisualSummaryFeature>> getFeaturesFrame() {
		return featuresFrame;
	}
	
	public int getRowCount() {
		return featuresFrame.size();
	}

	private void addDifferences(String sequence, String referenceSequence) {
		List<VisualSummaryFeature> differences = new ArrayList<>();
		int low = 0;
		int high = 0;
		boolean flag = false;
		int minLength = Math.min(sequence.length(), referenceSequence.length());
		for (int index = 0; index < minLength; index++) {
			if (!flag && (sequence.charAt(index) != referenceSequence.charAt(index))) {
				low = index;
				flag = true;
			}
			if (flag && (sequence.charAt(index) == referenceSequence.charAt(index))) {
				high = index - 1;
				flag = false;
				differences.add(new VisualSummaryFeature(VisualSummaryFeatureType.DIFFERENCE, low, high, false));
			}
		}
		featuresFrame.add(differences);
	}
	
	private void addGenes(List<Feature> genes) {
		List<VisualSummaryFeature> processedGenes = new ArrayList<>();
		for (Feature gene : genes) {
			processedGenes.add(new VisualSummaryFeature(VisualSummaryFeatureType.GENE, gene.getIntervals()));
		}
		Collections.sort(processedGenes, new SortByLow());
		List<List<VisualSummaryFeature>> genesFrame = new ArrayList<>();
        for (VisualSummaryFeature gene : processedGenes) {        	
        	int index;
        	for (index = 0; index < genesFrame.size(); index++) {
        		List<VisualSummaryFeature> genesRow = genesFrame.get(index);
        		VisualSummaryFeature lastGene = genesRow.get(genesRow.size() - 1);
        		if (lastGene.getHigh() < gene.getLow()) {
        			genesRow.add(gene);
        			break;
        		}
        	}
        	if(index == genesFrame.size()) {
        		List<VisualSummaryFeature> newGenesRow = new ArrayList<>();
        		newGenesRow.add(gene);
        		genesFrame.add(newGenesRow);
        	}
        }
		for (List<VisualSummaryFeature> genesRow : genesFrame) {
			featuresFrame.add(genesRow);
		}
	}
}