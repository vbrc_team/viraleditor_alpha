package VisualSummary.structures;

import java.util.List;

import Shared.interchange.Interval;

public class VisualSummaryFeature {
	private final VisualSummaryFeatureType type;
	private final double low;
	private final double high;
	private final boolean isComplement;

	public VisualSummaryFeature(VisualSummaryFeatureType type, List<Interval> intervals) {
		this.type = type;
    	double minLow = Double.MAX_VALUE;
    	double maxHigh = 0;
    	this.isComplement = intervals.get(0).isComplement(); // Each interval should have the same value for isComplement as the first interval.
    	for (Interval interval : intervals) {
    		if (interval.isComplement() != this.isComplement) {
    			throw new IllegalArgumentException("Feature intervals must all contain the same value of isComplement");
    		}
    		double low = interval.getLow();
    		double high = interval.getHigh();
    		if (low < minLow) {
    			minLow = low;
    		}
    		if (high > maxHigh) {
    			maxHigh = high;
    		}
    	}
    	this.low = minLow;
    	this.high = maxHigh;
	}
	
	public VisualSummaryFeature(VisualSummaryFeatureType type, double low, double high, boolean isComplement) {
		this.type = type;
    	this.low = low;
    	this.high = high;
    	this.isComplement = isComplement;
	}
	
	public VisualSummaryFeatureType getType() {
		return type;
	}

	public double getLow() {
		return low;
	}

	public double getHigh() {
		return high;
	}	

	public boolean isComplement() {
		return isComplement;
	}
}