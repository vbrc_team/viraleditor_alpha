package ViralEditor;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.CountDownLatch;

import Shared.FamilyItem;
import Shared.interchange.Family;
import VirusInvestigator.VirusInvestigator;
import VirusInvestigator.VirusInvestigatorComposer;
import VirusInvestigator.VirusInvestigatorDialog;
import VirusInvestigator.VirusInvestigatorView;
import javafx.collections.FXCollections;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.ChoiceBox;
import javafx.stage.Stage;


/**
 * Handles the UI for the opening screen, where tools and database are selected
 */
public class ViralEditorDialog implements ViralEditorView {
    
	private ViralEditorComposer boundComposer;
    private Tool editorTool;
    private Family database;
	//TODO: not sure why we need countdown
	private final CountDownLatch waitForCancel = new CountDownLatch(1);

    @FXML
    private Button openDA;
    @FXML
    private Button openAlign;
    @FXML
    private Button openDotPlot;
    @FXML
    private Button openAnnotation;
    @FXML
    private Button openGenomeMap;
    @FXML
    private Button openGraphDNA;
    @FXML
    private Button openCodonStats;
    //@FXML
    //private ChoiceBox<FamilyItem> databaseList;

    public void initComposer(ViralEditorComposer composer) {
        this.boundComposer = composer;
    }

    @Override
    public void setDatabaseList(List<Family> databases) {
        List<FamilyItem> items = new ArrayList<>();
        for (Family family : databases) {
            items.add(new FamilyItem(family));
        }
    }

    private void chooseDatabase(Family database) {
        this.database = database;
        waitForCancel.countDown();
    }
    
    @FXML
    private void initNode() {
    	System.out.println("Mouse Entered Node");
    }
    
    @FXML
    private void handleOpenCodonStats() {
        chooseTool(Tool.CODONSTATS);
        openTool(Tool.CODONSTATS);
        close();
    }
    @FXML
    private void handleOpenGraphDNA() {
        chooseTool(Tool.GRAPHDNA);
        openTool(Tool.GRAPHDNA);
        close();
    }
    @FXML
    private void handleOpenGenomeMap() {
        chooseTool(Tool.GENOMEMAP);
        openTool(Tool.GENOMEMAP);
        close();
    }
    @FXML
    private void handleOpenAnnotation() {
        chooseTool(Tool.ANNOTATION);
        openTool(Tool.ANNOTATION);
        close();
    }
    @FXML
    private void handleOpenDotPlot() {
        chooseTool(Tool.DOTPLOT);
        openTool(Tool.DOTPLOT);
        close();
    }
    @FXML
    private void handleOpenAlign() {
        chooseTool(Tool.ALIGNMENT);
        openTool(Tool.ALIGNMENT);
        close();
    }
    @FXML
    private void handleOpenDA() {
        chooseTool(Tool.DBANALYSIS);
        openTool(Tool.DBANALYSIS);
        close();
    }

    @FXML
    private void helpAlignClicked() {
        setHelpInfo(Tool.ALIGNMENT);
    }

    @FXML
    private void helpAnnotateClicked() {
        setHelpInfo(Tool.ANNOTATION);
    }

    @FXML
    private void helpCSClicked() {
        setHelpInfo(Tool.CODONSTATS);
    }

    @FXML
    private void helpDAClicked() {
        setHelpInfo(Tool.DBANALYSIS);
    }

    @FXML
    private void helpDPClicked() {
        setHelpInfo(Tool.DOTPLOT);
    }

    @FXML
    private void helpGDNAClicked() {
        setHelpInfo(Tool.GRAPHDNA);
    }

    @FXML
    private void helpGMClicked() {
        setHelpInfo(Tool.GENOMEMAP);
    }
    
    /**
     * Opens a new window containing information about the given tool
     * @param toolName - name of the tool
     */
    private void setHelpInfo(Tool tool) {
        try {
            FXMLLoader loader = new FXMLLoader(getClass().getClassLoader().getResource("ToolInformationDialog.fxml"));
            Parent root = loader.load();
            
            ToolInformationView controller = loader.getController();
            ((ToolInformationDialog)controller).init();
            controller.setTool(tool.name);
            controller.setImage("/assets/" + tool.name.toLowerCase().replaceAll(" ", "") + ".png");
            
            Stage popup = new Stage();
            popup.setTitle(tool.name + " Help");
            popup.setScene(new Scene(root));
            popup.show();
        } catch (Exception e) {
            System.out.println("Can't load window");
            e.printStackTrace();
        }
    }
    /**
     * choose the given tool
     * @param tool - tool name
     */
    private void chooseTool(Tool tool) {
        this.editorTool = tool;
        waitForCancel.countDown();
    }

    /**
     * Opens an investigator window for the given tool
     * @param toolName - name of the tool
     */
    public void openTool(Tool tool) {
        try {
            FXMLLoader loader = new FXMLLoader(getClass().getClassLoader().getResource("VirusInvestigator.fxml"));
            Parent root = (Parent)loader.load();
            
            VirusInvestigatorView controller = loader.getController();
            VirusInvestigatorComposer composer = new VirusInvestigator();
            ((VirusInvestigatorDialog) controller).initComposer(composer, tool);
            ((VirusInvestigator)composer).bind(controller);
            pickTool((VirusInvestigatorDialog)controller);
            
            composer.setDatabaseList(boundComposer.getDatabaseList(), boundComposer.getDatabaseList().get(0));

            Stage popup = new Stage();
            popup.setTitle(tool.name + " Virus Investigator");
            popup.setScene(new Scene(root));
            popup.setWidth(1200);
            popup.show();
        } catch (Exception e) {
            System.out.println("Can't load window");
            e.printStackTrace();
        }
    }

    private void close() {
        Stage stage = (Stage) openAlign.getScene().getWindow();
        stage.close();
    }
    
    /**
     * Sets the upper and lower panels of the investigator dialog based on the chosen tool
     * @param controller - Controller class for an investigator window
     */
    private void pickTool(VirusInvestigatorDialog controller) {
        switch (editorTool) {
        case ALIGNMENT:
        case GENOMEMAP:
        case GRAPHDNA:
            controller.setUpper("SelectViruses.fxml", controller);
            controller.setLower("LocalFiles.fxml", controller);
            break;
        case DBANALYSIS:
            controller.setUpper("SelectViruses.fxml", controller);
            controller.setLower("DBAnalysisButtons.fxml", controller);
            break;
        case ANNOTATION:
            controller.setUpper("SelectViruses.fxml", controller);
            controller.setLower("AnnotationButtons.fxml", controller);
            break;
        case CODONSTATS:
            controller.setUpper("SelectViruses.fxml", controller);
            controller.setLower("CodonStatsButtons.fxml", controller);
            break;
        case DOTPLOT:
            controller.setPanel("DotPlotButtons.fxml", controller);
            break;
        default:
            throw new IllegalArgumentException(String.format("Missing case for %s tool", editorTool.name));
        }

        String tip = editorTool.tip;
        controller.setInvestigatorTips(tip);
    }
    
    //TODO: can some one comment about why we are using a countdown?
    //TODO: can some one comment about why we need chosen method?
    public Family databaseChosen() throws InterruptedException {
        waitForCancel.await();
        return database;
    }
    
    //TODO: can some one comment about why we need chosen method?
    public Tool toolChosen() throws InterruptedException {
        waitForCancel.await();
        return editorTool;
    }
}
