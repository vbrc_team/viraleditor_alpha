package ViralEditor;

//contains literals used by the Viral Editor
public enum Tool {
    ALIGNMENT("Alignment",
            "Please select the sequences you would like, either from the database or upload from your machine."),
    DBANALYSIS("Database Analysis",
            "Please select the viruses you would like to view.\nClick on the buttons to view more detailed information about them."),
    DOTPLOT("DotPlot",
            "Please pick a maximum of two genomes.\nFasta files can include more than one sequence to create a 2x2 plot."),
    ANNOTATION("Annotation",
            "Please select a reference genome (from database or local file) and a target genome from your local machine."),
    GENOMEMAP("Genome Map",
            "Please select the sequences you would like, either from the database or upload from your machine."),
    GRAPHDNA("Graph DNA",
            "Please select the sequences you would like, either from the database or upload from your machine."),
    CODONSTATS("Codon Statistics", "Please select the viruses you would like to view.\n");

    public final String name;
    public final String tip;

    Tool(String name, String tip) {
        this.name = name;
        this.tip = tip;
    }

}
