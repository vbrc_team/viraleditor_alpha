package ViralEditor;

import java.net.URISyntaxException;

public interface ToolInformationView {

    /**
     * Sets the header and description based on the given tool
     * @param toolName - name of the tool
     */
    public void setTool(String toolName);
    
    /**
     * Sets the left hand image
     * @param path - path to the image
     * @throws URISyntaxException - thrown when invalid path is given
     */
    public void setImage(String path) throws URISyntaxException;
}
