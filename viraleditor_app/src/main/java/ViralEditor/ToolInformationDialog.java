package ViralEditor;

import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.stage.Stage;
import java.net.URISyntaxException;
import java.util.HashMap;

public class ToolInformationDialog implements ToolInformationView{
    
	private ViralEditorComposer composer;
	private HashMap<String, String> toolInfo;
	
    @FXML
    private Button closeButton;
    @FXML
    private Label toolDescription;
    @FXML
    private Label tool;
    @FXML
    private ImageView toolImage;

    public void init() {
        loadHash();
    }

    @FXML
    private void close() {
        ((Stage) closeButton.getScene().getWindow()).close();
    }

    @Override
    public void setTool(String toolName) {
        tool.setText(toolName);
        toolDescription.setText(getInfo(toolName));
    }

    @Override
    public void setImage(String path) throws URISyntaxException {
        Image image = new Image(getClass().getResource(path).toURI().toString());
        toolImage.setImage(image);
    }
    
    private String getInfo(String key) {
        return toolInfo.get(key);
    }
    
    /**
     * load tool information into hash map 
     */
    private void loadHash() {
        toolInfo = new HashMap<>();
        toolInfo.put("Database Analysis", "This allows the user to search and extract genome, gene and protein data from the database.It is able to see the view and count for gene, genome, and orcholog group.");
        toolInfo.put("Alignment", "This highlights differences between pairs of alignments and allows the user to easily navigate large alignments of similar sequences.");
        toolInfo.put("DotPlot", "This generates dotplots of large DNA or protein sequences.");
        toolInfo.put("Annotation", "This annotates a genome based on a very closely related reference genome. The proteins/mature peptides of the reference genome are BLASTed against the genome to be annotated in order to find the genes/mature peptides in the genome to be annotated.");
        toolInfo.put("Genome Map", "This can be used to display information about a genome, including its genes, ORFs and start/stop codons. It can also be used to perform a regular expression search, a fuzzy motif search, and a masslist search..");
        toolInfo.put("Graph DNA", "This allows the user to generate graphical representations of raw DNA sequences. To date, there are 8 graphing options and the user can plot any of the individual genes or genomes in any format.");
        toolInfo.put("Codon Statistics", "This enables one to quickly browse statistical information on one or more genomes. One may view statistical information on both graphical and tabular forms.");
    }

    
}
