package ViralEditor;

import java.util.List;

import Shared.interchange.Family;

public class ViralEditor implements ViralEditorComposer{
    private ViralEditorView boundView;
	private String database = null;
    private String editorTool;
    private List<Family> databaseList;

    /**
    * Bind the object to a view.
    * 
    * Warning. Methods calls will return exceptions until is called at least once.
    */
    public void bind(ViralEditorView view) {
    	this.boundView = view;
    }
    
    @Override
    public void setDatabaseList(List<Family> databases) {
        this.databaseList = databases;
        boundView.setDatabaseList(databases);
        
    }
    
    @Override
    public List<Family> getDatabaseList() {
    	return databaseList;
    }
}
