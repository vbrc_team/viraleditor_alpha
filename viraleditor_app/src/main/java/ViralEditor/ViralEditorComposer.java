package ViralEditor;

import java.util.List;

import Shared.interchange.Family;

public interface ViralEditorComposer {
	
    /**
     * set the database list used by viral editor
     */
    public void setDatabaseList(List<Family> databases);

	/**
	 * get the database list used by viral editor
	 * @return database list
	 */
    public List<Family> getDatabaseList();
}
