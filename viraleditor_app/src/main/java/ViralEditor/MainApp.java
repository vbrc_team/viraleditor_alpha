package ViralEditor;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

import Shared.Handling.Database;
import Shared.interchange.Family;
import Shared.interchange.Interchange;
import Shared.interchange.Reference;
import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;

public class MainApp extends Application {
    /**
     * This start() is automatically called when the application is launched from
     * the main method It loads ViralEditorDialog.fxml into a new window and
     * initiates its controller for handling the UI
     * 
     * @param primaryStage - it is constructed by the platform, top level JavaFX
     *                     container
     */
    @Override
    public void start(Stage primaryStage) throws Exception {
        FXMLLoader loader = new FXMLLoader(getClass().getClassLoader().getResource("ViralEditorDialog.fxml"));
        Parent root = loader.load(
                Objects.requireNonNull(getClass().getClassLoader().getResource("ViralEditorDialog.fxml")).openStream());

        initController(loader);

        primaryStage.setTitle("Viral Editor");
        primaryStage.setScene(new Scene(root));
        primaryStage.show();
    }

    /**
     * This gives the controller access to the main app and initialize its composer
     * for logic handling
     * 
     * @param loader - load from ViralEditorDialog.fxml
     */
    public void initController(FXMLLoader loader) {

        ViralEditorView controller = loader.getController();
        ViralEditorComposer composer = new ViralEditor();
        ((ViralEditorDialog) controller).initComposer(composer);
        ((ViralEditor) composer).bind(controller);

        setDatabases(composer);
    }

    /**
     * This adds database choices into the drop down box.
     * 
     * @param controller - controller for ViralEditorDialog
     */
    public void setDatabases(ViralEditorComposer composer) {
        composer.setDatabaseList(getDatabases());
    }

    /**
     * Gets a list of database names from the server.
     * <p>
     * An empty database list is returned when querying the server fails.
     * 
     * @return names of databases
     */
    private List<Family> getDatabases() {
        List<Family> databases = new ArrayList<>();
        try {
            Database database = new Database();
            Interchange families = database.queryFamilies();

            for (Reference reference : families.getReferences()) {
                databases.add(reference.getFeatures(Family.class).get(0));
            }
        } catch (IOException e) {
            // Do nothing
        }

        return databases;
    }

    public static void main(String[] args) {
        launch(args);
    }
}
