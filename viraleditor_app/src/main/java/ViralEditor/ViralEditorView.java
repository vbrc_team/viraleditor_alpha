package ViralEditor;

import java.util.List;

import Shared.interchange.Family;

public interface ViralEditorView {
	
	/**
     * Sets drop down database list, listens for changes to update the composer
     * 
     * @param databases - list of databases
     */
    public void setDatabaseList(List<Family> databases);

}
