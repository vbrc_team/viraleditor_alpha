package CodonStats;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.imageio.ImageIO;

import javafx.embed.swing.SwingFXUtils;
import javafx.geometry.Bounds;
import javafx.print.PageLayout;
import javafx.print.PrinterJob;
import javafx.scene.Node;
import javafx.scene.image.WritableImage;
import javafx.scene.transform.Scale;

/**
 * Handles logic behind bar and line graph data and functionality.
 */
public class CodonStatsGraphComposer {

    private List<Gene> genes;
    private double[] summary;
    private CodonStatsDialog geneSelectionWindow;

    /** Sets the controller from the CodonStatsDialog window.
     * @param controller - CodonStatsDialog controller which will be used to get what is selected.
     */
    public void setGeneSelectionWindow(CodonStatsDialog controller) {
        geneSelectionWindow = controller;
    }

    public CodonStatsDialog getGeneSelectionWindow() {
        return geneSelectionWindow;
    }

    /** Sets the data to be used in mean calculations
     * @param list - list of genes to be used in calculations
     */
    public void setData(List<Gene> list) {
        this.genes = list;
    }

    public List<Gene> getData() {
        return genes;
    }

    /** Gets the mean calculations of the nucleotide counts
     * @return the mean calculated to be displayed on graph
     */
    public double[] getMean() {
        ArrayList<NucleotideCount> nucleotides = new ArrayList<>();
        for (Gene gene : genes) {
            NucleotideCount nc = new NucleotideCount(gene.getGeneName(), gene.getDnaSeq());
            nucleotides.add(nc);
        }
        NucleotideCalculations calculations = new NucleotideCalculations(nucleotides);
       return calculations.getMean();
    }

    /** Calculates the second set of data's mean calculations of nucleotide counts
     * @param geneList - second set of data by user adding plot
     * @return the mean calculated for the second set of data
     */
    public double[] getAddedSetMean(List<Gene> geneList) {
        if (geneList.isEmpty()) {
            return new double[0];
        }
        List<NucleotideCount> nucleotides = new ArrayList<>();
        for (Gene gene : geneList) {
            NucleotideCount nc = new NucleotideCount(gene.getGeneName(), gene.getDnaSeq());
            nucleotides.add(nc);
        }
        NucleotideCalculations calculations = new NucleotideCalculations(nucleotides);
        return calculations.getMean();
    }

    /** Sets the data to be displayed on the graph opened from Codon View
     * @param summary - summary data (data calculated from Codon View)
     */
    public void setSummary(double[] summary) {
        this.summary = summary;
    }

    public double[] getSummary() {
        return summary;
    }

    /** Saves image of chart to file specified on user's machine.
     * @param image - image of chart to be saved
     * @param file - file image will be saved to (location specified by user)
     */
    public void saveAsPng(WritableImage image, File file) {
        try {
            ImageIO.write(SwingFXUtils.fromFXImage(image, null), "png", file);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    /** Scales chart to fit page being printed then prints the chart to user's printer.
     * @param job - printer job
     * @param node - desired chart to be printed
     */
    public void print(PrinterJob job, Node node) {
        PageLayout pageLayout = job.getPrinter().getDefaultPageLayout();
        double pageWidth = pageLayout.getPrintableWidth();
        double pageHeight = pageLayout.getPrintableHeight();

        Bounds bounds = node.getLayoutBounds();
        double nodeWidth = bounds.getWidth();
        double nodeHeight = bounds.getHeight();

        double spaceLeftX = pageWidth - nodeWidth;
        double spaceLeftY = pageHeight - nodeHeight;

        double scaleX = nodeWidth;
        double scaleY = nodeHeight;
        double ratioX = scaleY / scaleX;
        double ratioY = scaleX / scaleY;
        Scale scale = new Scale(scaleX, scaleY);
        if (spaceLeftX < 0) {
            scaleX = pageWidth / nodeWidth;
            scaleY = scaleX * ratioY;
            scale = new Scale(scaleX, scaleY);
        }
        if (spaceLeftY < 0) {
            scaleY = pageHeight / nodeHeight;
            scaleX = scaleY * ratioX;
            scale = new Scale(scaleX, scaleY);
        }

        node.getTransforms().add(scale);
        boolean done = job.printPage(node);
        if (done) {
            job.endJob();
        }
    }


    /** Gets selected items from CodonStatsDialog and then gets the data to be displayed from this list.
     * @return calculated mean of second set of genes.
     */
    public double[] addPlot() {
        List<Gene> geneList = geneSelectionWindow.getSelectedGenes();
        return getAddedSetMean(geneList);
    }

}
