package CodonStats;

import java.math.BigDecimal;
import java.math.RoundingMode;

/**
 * Handles calculations of nucleotides including counts and distribution in each gene
 */
public class NucleotideCount {
    public String geneSeq;
    public int[] a_Cnt, c_Cnt, g_Cnt, t_Cnt;
    String geneName;
    int aSum, cSum, gSum, tSum;
    double[] aCod, cCod, gCod, tCod;
    double a1,a2,a3,c1,c2,c3,g1,g2,g3,t1,t2,t3;
    double[] distribution;

    public NucleotideCount(String name, String sequence) {
        this.geneName = name;
        this.geneSeq = sequence;
        aCod = new double[3];
        cCod = new double[3];
        gCod = new double[3];
        tCod = new double[3];
        a_Cnt = new int[3];
        g_Cnt = new int[3];
        c_Cnt = new int[3];
        t_Cnt = new int[3];
        distribution = new double[4];
        init();
        count();
    }

    /**
     * Initialize the arrays used in this class
     */
    public void init() {
        for (int j = 0; j < 3; j++) {
            a_Cnt[j] = 0;
            c_Cnt[j] = 0;
            g_Cnt[j] = 0;
            t_Cnt[j] = 0;
        }
    }

    /**
     * Count a's, c's, g's, and t's in each codon position
     */
    private void count() {
        int seqLen;
        char ct ;
        seqLen = geneSeq.length();
        for (int j = 0; j <= seqLen-3; j += 3) {
            ct = geneSeq.charAt(j);
            charCount(ct, 1);
            ct = geneSeq.charAt(j+1);
            charCount(ct, 2);
            ct = geneSeq.charAt(j+2);
            charCount(ct, 3);
        }
        computeDis(seqLen);
    }


    /**
     * Count the occurrence of each nucleotide in each position
     * @param ch the letter of nucleotide
     * @param position the codon position
     *                 1 is the first position,
     *                 2 the second position, and 3 the third position
     */
    private void charCount(int ch, int position) {
        switch (ch) {
            case 'A':
            case 'a': a_Cnt[position-1]++; aSum++; break;
            case 'C':
            case 'c': c_Cnt[position-1]++; cSum++; break;
            case 'G':
            case 'g': g_Cnt[position-1]++; gSum++; break;
            case 'T':
            case 't': t_Cnt[position-1]++; tSum++; break;
        }
    }

    /**
     * Compute the distribution of a's, c's, g's, and t's in each gene
     * @param seqL    the length of the gene,
     */
    private void computeDis(int seqL){
        for (int i = 0; i < 3; i++) {
            aCod[i] = (double)a_Cnt[i] / seqL;
            cCod[i] = (double)c_Cnt[i] / seqL;
            gCod[i] = (double)g_Cnt[i] / seqL;
            tCod[i] = (double)t_Cnt[i] / seqL;
        }
    }

    //get functions are used by cell value factory to load into tables.

    public double getA1() {
        BigDecimal bd = BigDecimal.valueOf(aCod[0]).setScale(4, RoundingMode.HALF_UP);
        a1 = bd.doubleValue();
        return a1;
    }

    public double getA2() {
        BigDecimal bd = BigDecimal.valueOf(aCod[1]).setScale(4, RoundingMode.HALF_UP);
        a2 = bd.doubleValue();
        return a2;
    }

    public double getA3() {
        BigDecimal bd = BigDecimal.valueOf(aCod[2]).setScale(4, RoundingMode.HALF_UP);
        a3 = bd.doubleValue();
        return a3;
    }

    public double getC1() {
        BigDecimal bd = BigDecimal.valueOf(cCod[0]).setScale(4, RoundingMode.HALF_UP);
        c1 = bd.doubleValue();
        return c1;
    }

    public double getC2() {
        BigDecimal bd = BigDecimal.valueOf(cCod[1]).setScale(4, RoundingMode.HALF_UP);
        c2 = bd.doubleValue();
        return c2;
    }

    public double getC3() {
        BigDecimal bd = BigDecimal.valueOf(cCod[2]).setScale(4, RoundingMode.HALF_UP);
        c3 = bd.doubleValue();
        return c3;
    }

    public double getG1() {
        BigDecimal bd = BigDecimal.valueOf(gCod[0]).setScale(4, RoundingMode.HALF_UP);
        g1 = bd.doubleValue();
        return g1;
    }

    public double getG2() {
        BigDecimal bd = BigDecimal.valueOf(gCod[1]).setScale(4, RoundingMode.HALF_UP);
        g2 = bd.doubleValue();
        return g2;
    }

    public double getG3() {
        BigDecimal bd = BigDecimal.valueOf(gCod[2]).setScale(4, RoundingMode.HALF_UP);
        g3 = bd.doubleValue();
        return g3;
    }

    public double getT1() {
        BigDecimal bd = BigDecimal.valueOf(tCod[0]).setScale(4, RoundingMode.HALF_UP);
        t1 = bd.doubleValue();
        return t1;
    }

    public double getT2() {
        BigDecimal bd = BigDecimal.valueOf(tCod[1]).setScale(4, RoundingMode.HALF_UP);
        t2 = bd.doubleValue();
        return t2;
    }

    public double getT3() {
        BigDecimal bd = BigDecimal.valueOf(tCod[2]).setScale(4, RoundingMode.HALF_UP);
        t3 = bd.doubleValue();
        return t3;
    }

    public String getGeneName() {
        return geneName;
    }


    public void setNucleotides(double[] given) {
        distribution = given;
    }

    public double getNucleotide() {
        BigDecimal bd = BigDecimal.valueOf(distribution[0]).setScale(4, RoundingMode.HALF_UP);
        return bd.doubleValue();
    }

    public double getNucleotide1() {
        BigDecimal bd = BigDecimal.valueOf(distribution[1]).setScale(4, RoundingMode.HALF_UP);
        return bd.doubleValue();
    }

    public double getNucleotide2() {
        BigDecimal bd = BigDecimal.valueOf(distribution[2]).setScale(4, RoundingMode.HALF_UP);
        return bd.doubleValue();
    }

    public double getNucleotide3() {
        BigDecimal bd = BigDecimal.valueOf(distribution[3]).setScale(4, RoundingMode.HALF_UP);
        return bd.doubleValue();
    }
}
