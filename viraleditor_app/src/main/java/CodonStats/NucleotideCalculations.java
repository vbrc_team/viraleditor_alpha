package CodonStats;

import java.util.ArrayList;
import java.util.List;

/**
 * Handles calculations of nucleotide counts including sum, mean, variance, CI upperbound, CI lowerbound, etc.
 */
public class NucleotideCalculations {
    private final List<NucleotideCount> nucleotideCounts;
    int numSeq;
    double[] sum = new double[12];
    double[] ave = new double[12];
    double[] variance = new double[12];
    double[][] aDis, cDis, gDis, tDis;
    int a_Sum, c_Sum, g_Sum, t_Sum;
    double[] upperbound = new double[12];
    double[] lowerbound = new double[12];
    double[] mean = new double[12];


    public NucleotideCalculations(List<NucleotideCount> nc) {
        this.nucleotideCounts = nc;
        numSeq = nucleotideCounts.size();
        aDis = new double[numSeq][4];
        cDis = new double[numSeq][4];
        gDis =new double[numSeq][4];
        tDis = new double[numSeq][4];
        a_Sum = 0;
        c_Sum = 0;
        g_Sum = 0;
        t_Sum = 0;
        init();
        count();
    }

    /**
     * Initialize the arrays used in this class
     */
    private void init() {
        for (int i = 0; i < 12; i++) {
            sum[i] = 0.0;
            ave[i] = 0.0;
            variance[i] = 0.0;
        }
    }

    /**
     * Count a's, c's, g's, and t's in each codon position
     */
    private void count() {
        for (int i = 0; i < numSeq; i++) {
            a_Sum += nucleotideCounts.get(i).aSum;
            c_Sum += nucleotideCounts.get(i).cSum;
            g_Sum += nucleotideCounts.get(i).gSum;
            t_Sum += nucleotideCounts.get(i).tSum;
        }
        computeDis();
    }

    /**
     * Computes the sum, variance and distribution for each nucleotide in each gene.
     */
    private void computeDis(){
        for (int j = 0; j < numSeq; j++) {
            NucleotideCount element = nucleotideCounts.get(j);
            int length = element.geneSeq.length();
            aDis[j][0] = (double)element.aSum / length;
            cDis[j][0] = (double)element.cSum / length;
            gDis[j][0] = (double)element.gSum / length;
            tDis[j][0] = (double)element.tSum / length;
            for (int i = 0; i < 3; i++) {

                sum[i] += element.aCod[i];
                variance[i] += element.aCod[i] * element.aCod[i];
                aDis[j][i+1] = (double) element.a_Cnt[i] / element.aSum;

                sum[i+3] += element.cCod[i];
                variance[i+3] += element.cCod[i] * element.cCod[i];
                cDis[j][i+1] = (double) element.c_Cnt[i] / element.cSum;

                sum[i+6] += element.gCod[i];
                variance[i+6] += element.gCod[i] * element.gCod[i];
                gDis[j][i+1] = (double) element.g_Cnt[i] / element.gSum;

                sum[i+9] += element.tCod[i];
                variance[i+9] += element.tCod[i] * element.tCod[i];
                tDis[j][i+1] = (double) element.t_Cnt[i] / element.tSum;
            }
        }
    }

    public double[] getSum() {
        return this.sum;
    }

    /** Calculates and returns the mean of all nucleotide counts.
     * @return the calculated mean of nucleotide count (a1,a2,...t3).
     */
    public double[] getMean() {
        for (int i = 0; i < 12; i++) {
            mean[i] = sum[i] / numSeq;
        }
        return mean;
    }

    /** Calculates and returns the variance of all nucleotide counts
     * @return the calculated variance of nucleotide count (a1,a2,...t3).
     */
    public double[] getVariance() {
        double[] result = new double[12];
        for (int i = 0; i < 12; i++) {
            if (numSeq == 1) {
                result[i] = 0.0;
            } else {
                result[i] = (variance[i] - sum[i] * sum[i] / numSeq) * 1000 / (numSeq - 1);
            }
        }
        return result;
    }

    /**
     * Calculates the CI upper and lower bounds of all nucleotide counts.
     */
    public void setCIBounds() {
        double[] var = getVariance();
        double[] mean = getMean();
        for (int i = 0; i < 12; i++) {
            double temp = 1.96 * Math.sqrt(var[i] / (1000 * numSeq));
            upperbound[i] = mean[i] + temp;
            lowerbound[i] = mean[i] - temp;
        }
    }

    public double[] getCIUpperBound() {
        return upperbound;
    }

    public double[] getCILowerBound() {
        return lowerbound;
    }

    public int getNumSeq() {
        return this.numSeq;
    }


    /**
     * Return the summary table of the give nucleotide
     * including sum, mean, and variance
     * @param ch the name of the nucleotide
     * @return a two_dimension array of Double
     */
    public Double[][] getDSum(char ch) {
        Double[][] result = new Double[3][4];
        double[] temp = new double[4];
        double[][] dis = new double[numSeq][4];
        double[] vari = new double[4];
        switch (ch) {
            case 'A':
            case 'a': dis = aDis; break;
            case 'C':
            case 'c': dis = cDis; break;
            case 'G':
            case 'g': dis = gDis; break;
            case 'T':
            case 't': dis = tDis; break;
        }
        for (int i = 0; i < 4; i++) {
            temp[i] = 0.0;
            vari[i] = 0.0;
        }
        for(int i = 0; i < 4; i++) {
            for (int j = 0; j < numSeq; j++) {
                temp[i] += dis[j][i];
                vari[i] += dis[j][i] * dis[j][i];
            }
            result[0][i] = temp[i];
            result[1][i] = temp[i] / numSeq;
            if (numSeq == 1)
                result[2][i] = 0.0;
            else
                result[2][i] = ((vari[i] - temp[i] * temp[i] / numSeq)) * 1000 / (numSeq - 1);
        }
        return result;
    }

    /**
     * Return a nucleotide codon distribution
     * @param ch the name of the nucleotide
     * @return  an Arraylist of double[]'s distributions
     */
    public List<double[]> getDisplayDis (char ch) {
        ArrayList<double[]> result = new ArrayList<>();
        double[][] tt = new double[numSeq][4];
        switch (ch) {
            case 'A':
            case 'a': tt = aDis; break;
            case 'C':
            case 'c': tt = cDis; break;
            case 'G':
            case 'g': tt = gDis; break;
            case 'T':
            case 't': tt = tDis; break;
        }
        for (int j = 0; j < numSeq; j++) {
            result.add(tt[j]);
        }
        return result;
    }
}
