package CodonStats;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import Shared.interchange.Reference;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.stage.FileChooser;
import javafx.stage.Stage;

/**
 * Handles the UI for Codon Statistics Gene Selection View/Opening Codon Stats window.
 */
public class CodonStatsDialog {
    private CodonStatsComposer composer;
    private ObservableList<Gene> genes;
    private static final String HELP_SITE = "https://4virology.net/virology-ca-tools/codonstatistics/";

    @FXML
    private TableView<Gene> table;
    @FXML
    private TableColumn<Integer, Gene> tableID;
    @FXML
    private TableColumn<String, Gene> tableName;
    @FXML
    private TableColumn<Integer, Gene> tableSize;
    @FXML
    private TableColumn<Double, Gene> tableA;
    @FXML
    private TableColumn<Double, Gene> tableC;
    @FXML
    private TableColumn<Double, Gene> tableG;
    @FXML
    private TableColumn<Double, Gene> tableT;
    @FXML
    private MenuItem viewAll;

    public void initComposer(CodonStatsComposer composer) {
        this.composer = composer;
    }

    public static void openWindow(List<Reference> references) throws IOException {
        FXMLLoader loader = new FXMLLoader(CodonStatsDialog.class.getClassLoader().getResource("CodonStatistics.fxml"));
        Parent root = loader.load();
        CodonStatsDialog controller = loader.getController();
        controller.initComposer(new CodonStatsComposer());

        controller.composer.setUp(references);
        controller.setUp();

        Stage popup = new Stage();
        popup.setTitle("Codon Statistics");
        popup.setScene(new Scene(root));
        popup.show();
    }

    private void setUp() {
        setGenes(composer.getGenes());
    }

    @FXML
    private void openICBarPlot() {
        composer.setType("IC");
        ArrayList<Gene> geneList = getSelectedItems();
        composer.openBarPlot(geneList, this);
    }

    @FXML
    private void openICLinePlot() {
        composer.setType("IC");
        ArrayList<Gene> geneList = getSelectedItems();
        composer.openLinePlot(geneList, this);
    }

    @FXML
    private void openNTBarPlot() {
        composer.setType("NT");
        ArrayList<Gene> geneList = getSelectedItems();
        composer.openBarPlot(geneList, this);
    }

    @FXML
    private void openNTLinePlot() {
        composer.setType("NT");
        ArrayList<Gene> geneList = getSelectedItems();
        composer.openLinePlot(geneList, this);
    }

    private ArrayList<Gene> getSelectedItems() {
        ArrayList<Gene> arrayList;
        if (!table.getSelectionModel().getSelectedItems().isEmpty()) {
             arrayList = new ArrayList<>(table.getSelectionModel().getSelectedItems());
        } else {
            arrayList = new ArrayList<>(table.getItems());
        }
        return arrayList;
    }

    @FXML
    private void openCodonView(ActionEvent event) {
        List<Gene> geneList;
        if (event.getSource() == viewAll) {
            geneList = table.getItems();
        } else {
            geneList = table.getSelectionModel().getSelectedItems();
        }
        if (geneList.isEmpty()) {
            showErrorWindow("Please make a selection.");
            return;
        }
        composer.openCodonViewWindow(geneList);
    }

    public static void showErrorWindow(String message){
        Alert alert = new Alert(Alert.AlertType.NONE, message, ButtonType.OK);
        alert.setTitle("Selection Warning");
        alert.showAndWait();
    }

    @FXML
    private void clearSelection() {
        table.getSelectionModel().clearSelection();
    }

    /** Sets the table view data with the list of genes from the file loaded.
     * @param genesList - list of genes to display in the Codon Statistics Gene View Window.
     */
    public void setGenes(List<Gene> genesList) {
        genes = FXCollections.observableArrayList(genesList);
        tableID.setCellValueFactory(new PropertyValueFactory<>("virusID"));
        tableName.setCellValueFactory(new PropertyValueFactory<>("geneName"));
        tableSize.setCellValueFactory(new PropertyValueFactory<>("size"));
        tableA.setCellValueFactory(new PropertyValueFactory<>("aComp"));
        tableC.setCellValueFactory(new PropertyValueFactory<>("cComp"));
        tableG.setCellValueFactory(new PropertyValueFactory<>("gComp"));
        tableT.setCellValueFactory(new PropertyValueFactory<>("tComp"));
        table.setItems(genes);
        table.getSelectionModel().setSelectionMode(SelectionMode.MULTIPLE);
    }

    /**
     * Creates a popup window where user can choose where to save file, then file is saved.
     */
    @FXML
    private void saveFile() {
        try {
            FileChooser fileChooser = new FileChooser();
            fileChooser.setTitle("Save File");
            File file = fileChooser.showSaveDialog(null);
            composer.saveData(file, genes);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @FXML
    private void openDescription() {
        Shared.Functions.getOnlineHelp(HELP_SITE);
    }

    @FXML
    private void closeWindow() {
        Stage stage = (Stage) table.getScene().getWindow();
        stage.close();
    }

    /** Used by CodonStatsGraphComposer to check what genes are selected.
     * @return list of currently selected genes in this window.
     */
    public List<Gene> getSelectedGenes() {
        return new ArrayList<>(table.getSelectionModel().getSelectedItems());
    }

}
