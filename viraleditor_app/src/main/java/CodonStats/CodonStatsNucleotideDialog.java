package CodonStats;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.scene.chart.BarChart;
import javafx.scene.chart.LineChart;
import javafx.scene.chart.XYChart;
import javafx.scene.control.*;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.input.MouseEvent;
import javafx.stage.Stage;

import java.util.List;

/**
 * Handles UI of displaying distribution of a single nucleotide (A, C, G, T).
 */
public class CodonStatsNucleotideDialog {
    public CodonStatsNucleotideComposer composer;
    private char chosenNucleotide;
    private List<NucleotideStats> nucleotideStats;

    @FXML
    private Tab distributionTab;

    @FXML
    private TableView<NucleotideCount> geneTable;
    @FXML
    private TableColumn<String, NucleotideCount> geneName;
    @FXML
    private TableColumn<Double, NucleotideCount> nucleotide;
    @FXML
    private TableColumn<Double, NucleotideCount> nucleotide1;
    @FXML
    private TableColumn<Double, NucleotideCount> nucleotide2;
    @FXML
    private TableColumn<Double, NucleotideCount> nucleotide3;

    @FXML
    private TableView<NucleotideStats> sumTable;
    @FXML
    private TableColumn<String, NucleotideStats> stats;
    @FXML
    private TableColumn<Double, NucleotideStats> nucleotideTotals;
    @FXML
    private TableColumn<Double, NucleotideStats> nucleotideTotals1;
    @FXML
    private TableColumn<Double, NucleotideStats> nucleotideTotals2;
    @FXML
    private TableColumn<Double, NucleotideStats> nucleotideTotals3;

    @FXML
    private TextField linePosition;
    @FXML
    private TextField barPosition;
    @FXML
    private TextField lineFraction;
    @FXML
    private TextField barFraction;
    @FXML
    private BarChart<String, Number> barPlot;
    @FXML
    private LineChart<String, Number> linePlot;

    public void initComposer(CodonStatsNucleotideComposer composer) {
        this.composer = composer;
    }

    /** Sets the table's labels for the chosen nucleotide.
     * @param nucleotide - nucleotide chosen to display distribution for
     */
    public void setNucleotide(char nucleotide) {
        chosenNucleotide = nucleotide;
        distributionTab.setText(nucleotide + "'s Distribution");
        this.nucleotide.setText(nucleotide + "'s");
        nucleotide1.setText(nucleotide + "_1");
        nucleotide2.setText(nucleotide + "_2");
        nucleotide3.setText(nucleotide + "_3");
        nucleotideTotals.setText(nucleotide + "'s");
        nucleotideTotals1.setText(nucleotide + "_1");
        nucleotideTotals2.setText(nucleotide + "_2");
        nucleotideTotals3.setText(nucleotide + "_3");
    }

    /** Adds data into the table from the nucleotide count arraylist
     * @param nc - nucleotide count arraylist of data to be loaded into the table
     */
    public void setTable(List<NucleotideCount> nc) {
        ObservableList<NucleotideCount> nucleotideCounts = FXCollections.observableArrayList(nc);
        geneName.setCellValueFactory(new PropertyValueFactory<>("geneName"));
        nucleotide.setCellValueFactory(new PropertyValueFactory<>("nucleotide"));
        nucleotide1.setCellValueFactory(new PropertyValueFactory<>("nucleotide1"));
        nucleotide2.setCellValueFactory(new PropertyValueFactory<>("nucleotide2"));
        nucleotide3.setCellValueFactory(new PropertyValueFactory<>("nucleotide3"));
        geneTable.setItems(nucleotideCounts);
        geneTable.getSelectionModel().setSelectionMode(SelectionMode.MULTIPLE);
    }

    /** Adds data into the sum table from the nucleotide statistics arraylist.
     * @param ns - nucleotide statistics arraylist of data to be loaded into the sum table
     */
    public void setSumTable(List<NucleotideStats> ns) {
        this.nucleotideStats = ns;
        ObservableList<NucleotideStats> nucleotideStats = FXCollections.observableArrayList(ns);
        stats.setCellValueFactory(new PropertyValueFactory<>("description"));
        nucleotideTotals.setCellValueFactory(new PropertyValueFactory<>("nucleotideTotals"));
        nucleotideTotals1.setCellValueFactory(new PropertyValueFactory<>("nucleotideTotals1"));
        nucleotideTotals2.setCellValueFactory(new PropertyValueFactory<>("nucleotideTotals2"));
        nucleotideTotals3.setCellValueFactory(new PropertyValueFactory<>("nucleotideTotals3"));
        sumTable.setItems(nucleotideStats);
        sumTable.getSelectionModel().setSelectionMode(SelectionMode.MULTIPLE);
    }

    /**
     * Adds data into the bar and line charts in the other tabs. Listeners are setup so when a data point is
     * clicked, the data and corresponding nucleotide are displayed.
     */
    public void setupCharts() {
        XYChart.Series<String, Number> series1 = new XYChart.Series<>();
        series1.getData().add(new XYChart.Data<>(chosenNucleotide + "_1", nucleotideStats.get(1).getNucleotideTotals1()));
        series1.getData().add(new XYChart.Data<>(chosenNucleotide + "_2", nucleotideStats.get(1).getNucleotideTotals2()));
        series1.getData().add(new XYChart.Data<>(chosenNucleotide + "_3", nucleotideStats.get(1).getNucleotideTotals3()));
        XYChart.Series<String, Number> series2 = new XYChart.Series<>();
        series2.getData().add(new XYChart.Data<>(chosenNucleotide+"_1", nucleotideStats.get(1).getNucleotideTotals1()));
        series2.getData().add(new XYChart.Data<>(chosenNucleotide+"_2", nucleotideStats.get(1).getNucleotideTotals2()));
        series2.getData().add(new XYChart.Data<>(chosenNucleotide+"_3", nucleotideStats.get(1).getNucleotideTotals3()));
        barPlot.getData().add(series1);
        linePlot.getData().add(series2);

        for (XYChart.Data<String, Number> item : series2.getData()) {
            item.getNode().setOnMousePressed((MouseEvent event) -> {
                linePosition.setText(item.getXValue());
                lineFraction.setText(item.getYValue().toString());
            });
        }

        for (XYChart.Data<String, Number> item : series1.getData()) {
            item.getNode().setOnMousePressed((MouseEvent event) -> {
                barPosition.setText(item.getXValue());
                barFraction.setText(item.getYValue().toString());
            });
        }
    }

    @FXML
    private void closeWindow() {
        Stage stage = (Stage) linePlot.getScene().getWindow();
        stage.close();
    }

    /**
     * Opens a description of this tool from help menu.
     */
    @FXML
    private void getDescription() {
        composer.openDescription(geneTable.getScene().getWindow());
    }
}
