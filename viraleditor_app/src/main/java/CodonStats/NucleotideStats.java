package CodonStats;

import java.math.BigDecimal;
import java.math.RoundingMode;

/**
 * Stores Nucleotide stats including sum, mean, etc to be loaded into sum tables in Codon View and
 * Nucleotide Dis.
 */
public class NucleotideStats {
    double a1Sum, a2Sum, a3Sum, c1Sum, c2Sum, c3Sum, g1Sum, g2Sum, g3Sum, t1Sum, t2Sum, t3Sum;
    String description;
    double nucleotideTotals, nucleotideTotals1, nucleotideTotals2, nucleotideTotals3;

    /**
     * @param description - description of statistic (i.e. sum, mean, etc)
     */
    public void setDescription(String description) {
        this.description = description;
    }

    public String getDescription() {
        return this.description;
    }

    //get functions are used by cell value factory to load into tables.

    public double getA1Sum() {
        BigDecimal bd = BigDecimal.valueOf(a1Sum).setScale(4, RoundingMode.HALF_UP);
        return bd.doubleValue();
    }

    public double getA2Sum() {
        BigDecimal bd = BigDecimal.valueOf(a2Sum).setScale(4, RoundingMode.HALF_UP);
        return bd.doubleValue();
    }

    public double getA3Sum() {
        BigDecimal bd = BigDecimal.valueOf(a3Sum).setScale(4, RoundingMode.HALF_UP);
        return bd.doubleValue();
    }

    public double getC1Sum() {
        BigDecimal bd = BigDecimal.valueOf(c1Sum).setScale(4, RoundingMode.HALF_UP);
        return bd.doubleValue();
    }

    public double getC2Sum() {
        BigDecimal bd = BigDecimal.valueOf(c2Sum).setScale(4, RoundingMode.HALF_UP);
        return bd.doubleValue();
    }

    public double getC3Sum() {
        BigDecimal bd = BigDecimal.valueOf(c3Sum).setScale(4, RoundingMode.HALF_UP);
        return bd.doubleValue();
    }

    public double getG1Sum() {
        BigDecimal bd = BigDecimal.valueOf(g1Sum).setScale(4, RoundingMode.HALF_UP);
        return bd.doubleValue();
    }

    public double getG2Sum() {
        BigDecimal bd = BigDecimal.valueOf(g2Sum).setScale(4, RoundingMode.HALF_UP);
        return bd.doubleValue();
    }

    public double getG3Sum() {
        BigDecimal bd = BigDecimal.valueOf(g3Sum).setScale(4, RoundingMode.HALF_UP);
        return bd.doubleValue();
    }

    public double getT1Sum() {
        BigDecimal bd = BigDecimal.valueOf(t1Sum).setScale(4, RoundingMode.HALF_UP);
        return bd.doubleValue();
    }

    public double getT2Sum() {
        BigDecimal bd = BigDecimal.valueOf(t2Sum).setScale(4, RoundingMode.HALF_UP);
        return bd.doubleValue();
    }

    public double getT3Sum() {
        BigDecimal bd = BigDecimal.valueOf(t3Sum).setScale(4, RoundingMode.HALF_UP);
        return bd.doubleValue();
    }

    public double getNucleotideTotals() {
        BigDecimal bd = BigDecimal.valueOf(nucleotideTotals).setScale(4, RoundingMode.HALF_UP);
        return bd.doubleValue();
    }

    public double getNucleotideTotals1() {
        BigDecimal bd = BigDecimal.valueOf(nucleotideTotals1).setScale(4, RoundingMode.HALF_UP);
        return bd.doubleValue();
    }

    public double getNucleotideTotals2() {
        BigDecimal bd = BigDecimal.valueOf(nucleotideTotals2).setScale(4, RoundingMode.HALF_UP);
        return bd.doubleValue();
    }

    public double getNucleotideTotals3() {
        BigDecimal bd = BigDecimal.valueOf(nucleotideTotals3).setScale(4, RoundingMode.HALF_UP);
        return bd.doubleValue();
    }

    /** Sets the data for the specified statistic (i.e. sum, mean, etc)
     * @param result - data calculated for statistic
     */
    public void setSums(double[] result) {
        if (result.length != 12) {
            System.out.println("There should be 12 elements given.");
            return;
        }
        a1Sum = result[0];
        a2Sum = result[1];
        a3Sum = result[2];
        c1Sum = result[3];
        c2Sum = result[4];
        c3Sum = result[5];
        g1Sum = result[6];
        g2Sum = result[7];
        g3Sum = result[8];
        t1Sum = result[9];
        t2Sum = result[10];
        t3Sum = result[11];
    }

    /** Sets the nucleotide sum statistic
     * @param data - data calculated for nucleotide statistic
     */
    public void setNucleotideTotals(double[] data) {
        if (data.length != 4) {
            System.out.println("There should be 4 elements given.");
            return;
        }

        nucleotideTotals = data[0];
        nucleotideTotals1 = data[1];
        nucleotideTotals2 = data[2];
        nucleotideTotals3 = data[3];
    }
}
