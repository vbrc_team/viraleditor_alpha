package CodonStats;

import java.math.BigDecimal;
import java.math.RoundingMode;

import Shared.GeneComposition;
import Shared.interchange.Annotation;

public class Gene {
    private final String geneName;
    private final int geneID;
    private final String dnaSequence;
    private GeneComposition composition;

    public Gene(Annotation annotation) {
        geneName = annotation.getGeneAbbreviation();
        geneID = annotation.getGeneID();
        dnaSequence = annotation.getNTSequence().getSequence();

        this.composition = new GeneComposition();
        composition.setNucleotideCounts(dnaSequence);
    }

    /**
     * Returns the gene name.
     * 
     * @return the gene name
     */
    public String getGeneName() {
        return geneName;
    }

    /**
     * Returns an id for the gene.
     * 
     * @return the gene id
     */
    public int getGeneID() {
        return geneID;
    }

    /**
     * Returns the gene's nucleotide sequence.
     * 
     * @return a nucleotide sequence
     */
    public String getDnaSeq() {
        return dnaSequence;
    }

    /**
     * Returns the gene's nucleotide sequence length.
     * 
     * @return the nucleotide sequence length
     */
    public int getSize() {
        return dnaSequence.length();
    }

    /**
     * Return portion of A in the sequence.
     * 
     * @return the portion of A in the sequence
     */
    public double getAComp() {
        return round((double) composition.getNucleotideCounts().getOrDefault("A", 0) / dnaSequence.length());
    }

    /**
     * Return portion of C in the sequence.
     * 
     * @return the portion of C in the sequence
     */
    public double getCComp() {
        return round((double) composition.getNucleotideCounts().getOrDefault("C", 0) / dnaSequence.length());
    }

    /**
     * Return portion of T in the sequence.
     * 
     * @return the portion of T in the sequence
     */
    public double getTComp() {
        return round((double) composition.getNucleotideCounts().getOrDefault("T", 0) / dnaSequence.length());
    }

    /**
     * Return portion of G in the sequence.
     * 
     * @return the portion of G in the sequence
     */
    public double getGComp() {
        return round((double) composition.getNucleotideCounts().getOrDefault("G", 0) / dnaSequence.length());
    }

    /**
     * Round a value to four decimal places.
     * 
     * @param value to round
     * @return value rounded to 4 decimal places
     */
    private double round(double value) {
        return BigDecimal.valueOf(value).setScale(4, RoundingMode.HALF_UP).doubleValue();
    }
}
