package CodonStats;

import java.io.*;
import java.text.NumberFormat;
import java.util.ArrayList;
import java.util.List;

import Shared.interchange.Annotation;
import Shared.interchange.Reference;
import javafx.collections.ObservableList;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;

/**
 * Handles loading of data into Codon Statistics Gene Selection window and
 * logic of menu items.
 */
public class CodonStatsComposer {
    String type;

    private List<Gene> genes;

    //Composer
    public CodonStatsComposer() {
        genes = new ArrayList<>();
    }

    /**
     * Initialize CodonStatsComposer using any Annotations features in references.
     *
     * @param references to read Annotation features from
     */
    public void setUp(List<Reference> references) {
        for (Reference reference : references) {
            for (Annotation annotation : reference.getFeatures(Annotation.class)) {
                genes.add(new Gene(annotation));
            }
        }
    }

    protected List<Gene> getGenes() {
        return genes;
    }

    /** Opens a bar plot window.
     * @param geneList - genes to be loaded into the bar plot (from selection by user)
     * @param codonStatsController - controller of codon stats to be able to view what is selected.
     */
    public void openBarPlot(List<Gene> geneList, CodonStatsDialog codonStatsController) {
        try {
            FXMLLoader loader = new FXMLLoader(getClass().getClassLoader().getResource("CodonStatsBarGraph.fxml"));
            Parent root = loader.load();
            CodonStatsBarGraphDialog controller = loader.getController();
            CodonStatsGraphComposer composer = new CodonStatsGraphComposer();
            composer.setData(geneList);
            composer.setGeneSelectionWindow(codonStatsController);
            controller.initComposer(composer);
            controller.setType(type);
            controller.setSource("Gene View");
            controller.setLabels("Percent:");
            controller.setupGraphData();

            openWindow(root, getTitleAddition());
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    /** Opens a line chart window
     * @param geneList - genes to be loaded into the bar plot (from selection by user)
     * @param codonStatsController - controller of codon stats to be able to view what is selected.
     */
    public void openLinePlot(List<Gene> geneList, CodonStatsDialog codonStatsController) {
        try {
            FXMLLoader loader = new FXMLLoader(getClass().getClassLoader().getResource("CodonStatsLineGraph.fxml"));
            Parent root = loader.load();
            CodonStatsLineGraphDialog controller = loader.getController();
            CodonStatsGraphComposer composer = new CodonStatsGraphComposer();
            composer.setGeneSelectionWindow(codonStatsController);
            composer.setData(geneList);
            controller.initComposer(composer);
            controller.setType(type);
            controller.setSource("Gene View");
            controller.setLabels("Percent:");
            controller.setupGraphData();

            openWindow(root, getTitleAddition());
        } catch (IOException e) {
            e.printStackTrace();
        }

    }

    private String getTitleAddition() {
        String titleAddition;
        if (type.equals("NT")) {
            titleAddition = "Nucleotide";
        } else {
            titleAddition = "Intracodon Position";
        }
        return titleAddition;
    }

    private void openWindow(Parent root, String plotType) {
        Stage popup = new Stage();
        popup.setTitle("Codon Data Plotter - " + plotType);
        popup.setScene(new Scene(root));
        popup.show();
    }

    public void setType(String type) {
        this.type = type;
    }

    protected String getType() {
        return type;
    }

    /** Opens a Codon View Window
     * @param genes - list of genes to be displayed in the Codon View window (by selection of user)
     */
    public void openCodonViewWindow(List<Gene> genes) {
        try {
            FXMLLoader loader = new FXMLLoader(getClass().getClassLoader().getResource("CodonViewWindow.fxml"));
            Parent root = loader.load();
            CodonViewDialog controller = loader.getController();
            CodonViewComposer composer = new CodonViewComposer();
            ArrayList<Gene> genesList = new ArrayList<>(genes);
            List<NucleotideCount> nucleotideCounts = composer.setTableData(genesList);
            controller.initComposer(composer);
            controller.setColumns(nucleotideCounts);

            Stage popup = new Stage();
            popup.setTitle("Codon View");
            popup.setScene(new Scene(root));
            popup.show();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    /** Saves Codon Statistics Gene View Window data (i.e. data displayed in CodonStatsDialog) to
     * text file on user's machine.
     * @param file - file where data will be saved to
     * @param genes - list of genes to be saved to the file
     * @throws IOException - throws an exception if unable to close or flush the writer
     */
    public void saveData(File file, ObservableList<Gene> genes) throws IOException {
        Writer writer = null;
        if (!file.getName().contains(".")) {
            file = new File(file.getAbsolutePath() + ".txt");
        }
        NumberFormat numberFormat = NumberFormat.getNumberInstance();
        numberFormat.setMaximumFractionDigits(4);
        numberFormat.setMinimumFractionDigits(4);
        try {
            writer = new BufferedWriter(new FileWriter(file));
            writer.write("#Gene Table\n");
            String line = "ID" + "\t\t" + "Gene Name" + "\t\t" + "Size" + "\t\t" + "#a/Size" + "\t\t"
                    + "#c/Size" + "\t\t" + "#g/Size" + "\t\t" + "#t/Size" + "\n";
            writer.write(line);
            for (Gene gene : genes) {
                line = gene.getGeneID() + "\t\t" + gene.getGeneName() + "\t\t" + gene.getSize() + "\t\t" + numberFormat.format(gene.getAComp()) + "\t\t"
                        + numberFormat.format(gene.getCComp()) + "\t\t" + numberFormat.format(gene.getGComp()) + "\t\t" + numberFormat.format(gene.getTComp()) + "\n";
                writer.write(line);
            }
        } catch (IOException e) {
            e.printStackTrace();

        } finally {
            assert writer != null;
            writer.flush();
            writer.close();
        }
    }

}
