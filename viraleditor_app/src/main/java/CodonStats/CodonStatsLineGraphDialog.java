package CodonStats;

import javafx.fxml.FXML;
import javafx.geometry.Point2D;
import javafx.print.*;
import javafx.scene.SnapshotParameters;
import javafx.scene.chart.*;
import javafx.scene.control.Alert;
import javafx.scene.control.ButtonType;
import javafx.scene.control.Label;
import javafx.scene.control.Menu;
import javafx.scene.control.MenuItem;
import javafx.scene.control.TextField;
import javafx.scene.image.ImageView;
import javafx.scene.image.WritableImage;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyCodeCombination;
import javafx.scene.input.KeyCombination;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.AnchorPane;
import javafx.scene.paint.Color;
import javafx.scene.shape.Rectangle;
import javafx.stage.FileChooser;
import javafx.stage.Stage;

import java.io.File;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

/**
 * Handles the UI for line graphs in Codon Statistics from Codon View and Codon Stats Gene Selection View.
 */
public class CodonStatsLineGraphDialog {
    private CodonStatsGraphComposer composer;
    private String type;
    private String source;
    private double[] data;
    private double[] addedPlotData;
    private static final String HELP_SITE = "https://4virology.net/virology-ca-tools/codonstatistics/";

    @FXML
    private Label xAxisValue;
    @FXML
    private Label yAxisValue;
    @FXML
    private LineChart<String, Number> lineChart;
    @FXML
    private CategoryAxis xAxis;
    @FXML
    private NumberAxis yAxis;
    @FXML
    private TextField xValueLabel;
    @FXML
    private TextField yValueLabel;
    @FXML
    private AnchorPane pane;
    @FXML
    private Rectangle draggedRectangle;
    @FXML
    private Menu editMenu;
    @FXML
    private MenuItem resetMenu;
    @FXML
    private MenuItem closeMenu;
    @FXML
    private MenuItem plotMenu;

    private XYChart.Series<String, Number> series1;
    private XYChart.Series<String, Number> series2;
    private XYChart.Series<String, Number> series3;
    private XYChart.Series<String, Number> series4;

    private XYChart.Series<String, Number> addedPlotSeries1;
    private XYChart.Series<String, Number> addedPlotSeries2;
    private XYChart.Series<String, Number> addedPlotSeries3;
    private XYChart.Series<String, Number> addedPlotSeries4;

    private double startYPosition;
    private double startXPosition;
    private boolean addedPlotFlag = false;
    private String set1Name = "";
    private String set2Name = "";

    public void initComposer(CodonStatsGraphComposer composer) {
        this.composer = composer;
    }

    /** This sets the labels at the top of the screen where data is filled in.
     *  These labels are either set to Nucleotide or Position and when data is
     *  clicked on, the fields beside them are filled in.
     *  @param yAxis - Text value for yAxisLabel on top of screen
     */
    public void setLabels(String yAxis) {
        this.xAxisValue.setText(getXAxisLabel());
        this.yAxisValue.setText(yAxis);
    }

    /** Gets the text for labels.
     * @return - Label text for the top of the screen.
     */
    private String getXAxisLabel() {
        if (type.equals("IC")) {
            return "Nucleotide:";
        } else {
            return "Position:";
        }
    }

    /**Sets the type of the graph, nucleotide or intracodon position
     * @param type - the type of graph
     */
    public void setType(String type) {
        this.type = type;
    }

    /** Sets the source of the graph, which determines which data will be displayed.
     * @param source - the source of the graph
     */
    public void setSource(String source) {
        this.source = source;
    }

    /**
     * Sets data to be loaded into the chart and listeners to display data on mouse clicked.
     */
    public void setupGraphData() {
        if (source.equals("Gene View")) {
            yAxis.setLabel("Frequency");
            editMenu.setVisible(true);
            plotMenu.setAccelerator(new KeyCodeCombination(KeyCode.A, KeyCombination.CONTROL_DOWN));
            data = composer.getMean();
        } else if (source.equals("Codon Plot")) {
            yAxis.setLabel("Fraction");
            data = composer.getSummary();
        }
        resetMenu.setAccelerator(new KeyCodeCombination(KeyCode.R, KeyCombination.CONTROL_DOWN));
        closeMenu.setAccelerator(new KeyCodeCombination(KeyCode.Q, KeyCombination.CONTROL_DOWN));
        yAxis.setAnimated(false);
        xAxis.setAnimated(false);
        lineChart.setAnimated(false);
        series1 = new XYChart.Series<>();
        series2 = new XYChart.Series<>();
        series3 = new XYChart.Series<>();
        series4 = new XYChart.Series<>();

        if (type.equals("NT")) {
            setupNTSeries();
        } else {
            setupICSeries();
        }

        for (XYChart.Series<String, Number> serie : lineChart.getData()) {
            for (XYChart.Data<String, Number> item : serie.getData()) {
                item.getNode().setOnMousePressed((MouseEvent event) -> {
                    xValueLabel.setText(serie.getName());
                    yValueLabel.setText(item.getYValue().toString());
                });
            }
        }
    }

    /** Sets the Nucleotide Series data points for a category of a nucleotide.
     * @param nucleotide - nucleotide given and to be displayed on graph (A, c, G or T)
     * @param index - the index of the corresponding data for the nucleotide
     */
    private void setSeriesNT(String nucleotide, int index) {
        series1.getData().add(new XYChart.Data<>(nucleotide, data[index*3]));
        series2.getData().add(new XYChart.Data<>(nucleotide, data[index*3+1]));
        series3.getData().add(new XYChart.Data<>(nucleotide, data[index*3+2]));
    }

    /** Sets the Intracodon position series data points for a category of an intracodon position.
     * @param position - intracodon position given and to be displayed on graph (1, 2, or 3)
     * @param index - the index of the corresponding data for the intracodon position
     */
    private void setSeriesIC(String position, int index) {
        series1.getData().add(new XYChart.Data<>(position, data[index]));
        series2.getData().add(new XYChart.Data<>(position, data[index+3]));
        series3.getData().add(new XYChart.Data<>(position, data[index+6]));
        series4.getData().add(new XYChart.Data<>(position, data[index+9]));
    }

    /**
     * Saves an image of the graph to the location specified by the user
     */
    @FXML
    private void saveImage() {
        FileChooser fileChooser = new FileChooser();
        fileChooser.setTitle("Save Image");
        File file = fileChooser.showSaveDialog(null);
        WritableImage image = lineChart.snapshot(new SnapshotParameters(), null);
        composer.saveAsPng(image, file);
    }

    /**
     *  Prints the current chart being displayed.
     */
    @FXML
    private void printChart() {
        WritableImage image = lineChart.snapshot(new SnapshotParameters(), null);
        ImageView imageView = new ImageView(image);

        PrinterJob job = PrinterJob.createPrinterJob();
        boolean print = job.showPrintDialog(null);
        if (print) {
            composer.print(job, imageView);
        }
    }

    @FXML
    private void closeWindow() {
        Stage stage = (Stage) lineChart.getScene().getWindow();
        stage.close();
    }

    /** Creates a rectangle user can drag over chart. When done, the chart becomes the
     * area inside the rectangle.
     * @param event - mouse event from user, press, drag, or release.
     */
    @FXML
    private void zoomIn(MouseEvent event) {
        if (event.getEventType() == MouseEvent.MOUSE_PRESSED) {
            draggedRectangle = new Rectangle();
            draggedRectangle.setFill(Color.web("blue", 0.1));
            draggedRectangle.setStroke(Color.BLUE);
            draggedRectangle.setStrokeDashOffset(50);
            pane.getChildren().add(draggedRectangle);
            draggedRectangle.setTranslateX(event.getSceneX());
            draggedRectangle.setTranslateY(event.getSceneY());

            Point2D mouseSceneCoords = new Point2D(event.getSceneX(), event.getSceneY());

            startXPosition = xAxis.sceneToLocal(mouseSceneCoords).getX();
            startYPosition = (double) yAxis.getValueForDisplay(yAxis.sceneToLocal(mouseSceneCoords).getY());
        }
        if (event.getEventType() == MouseEvent.MOUSE_DRAGGED) {
            draggedRectangle.setWidth(event.getSceneX() - draggedRectangle.getTranslateX());
            draggedRectangle.setHeight(event.getSceneY() - draggedRectangle.getTranslateY());
        }
        if (event.getEventType() == MouseEvent.MOUSE_RELEASED) {
            Point2D mouseSceneCoords = new Point2D(event.getSceneX(), event.getSceneY());
            double endXPosition = xAxis.sceneToLocal(mouseSceneCoords).getX();
            double endYPosition = (double) yAxis.getValueForDisplay(yAxis.sceneToLocal(mouseSceneCoords).getY());

            setYBounds(endYPosition);
            setXBounds(endXPosition);
            pane.getChildren().remove(draggedRectangle);
        }
    }

    /** Sets the bounds of the zoomed in chart for the y values.
     * @param endYPosition - position where user dragged mouse until.
     */
    private void setYBounds(double endYPosition) {
        double lowerBound = Math.min(endYPosition, startYPosition);
        double upperBound = Math.max(endYPosition, startYPosition);

        if (Math.abs((upperBound - lowerBound)) > 0.0005 ) {
            yAxis.setAutoRanging(false);
            yAxis.setUpperBound(upperBound);
            if (lowerBound >= 0) {
                yAxis.setLowerBound(lowerBound);
            } else {
                yAxis.setLowerBound(0);
            }
            yAxis.setTickUnit(Math.abs((upperBound - lowerBound)) / 10);
        }
    }

    /** Removes all items which are not between the starting x position and ending x position
     * set by the user (via creating rectangle).
     * @param endXPosition - position where user dragged mouse until.
     */
    private void setXBounds(double endXPosition) {
        double max = Math.max(endXPosition, startXPosition);
        double min = Math.min(endXPosition, startXPosition);

        if (Math.abs((max - min)) > 1 ) {
            HashMap<XYChart.Series<String, Number>, ArrayList<XYChart.Data<String, Number>>> removeMap = getRemovalItems(min, max);

            for (Map.Entry<XYChart.Series<String, Number>, ArrayList<XYChart.Data<String, Number>>> element : removeMap.entrySet()) {
                ArrayList<XYChart.Data<String, Number>> serieList = element.getValue();
                if (serieList.isEmpty()) {
                    continue;
                }
                if (element.getKey().getName().equals(series1.getName())) {
                    series1.getData().removeAll(serieList);
                }
                if (element.getKey().getName().equals(series2.getName())) {
                    series2.getData().removeAll(serieList);
                }
                if (element.getKey().getName().equals(series3.getName())) {
                    series3.getData().removeAll(serieList);
                }
                if (type.equals("IC")) {
                    if (element.getKey().getName().equals(series4.getName())) {
                        series4.getData().removeAll(serieList);
                    }
                }
                if (addedPlotFlag) {
                    if (element.getKey().getName().equals(addedPlotSeries1.getName())) {
                        addedPlotSeries1.getData().removeAll(serieList);
                    }
                    if (element.getKey().getName().equals(addedPlotSeries2.getName())) {
                        addedPlotSeries2.getData().removeAll(serieList);
                    }
                    if (element.getKey().getName().equals(addedPlotSeries3.getName())) {
                        addedPlotSeries3.getData().removeAll(serieList);
                    }
                    if (type.equals("IC")) {
                        if (element.getKey().getName().equals(addedPlotSeries4.getName())) {
                            addedPlotSeries4.getData().removeAll(serieList);
                        }
                    }
                }
            }
            xAxis.autosize();
        }
    }

    /** Creates a hashmap including all values that have to be removed to create the zoomed in view for user.
     * @param min - minimum value included in the rectangle/minimum value to be included in zoomed in view
     * @param max- maximum value included in the rectangle/maximum value to be included in zoomed in view
     * @return hashmap including the data points to be removed from the current chart view.
     */
    private HashMap<XYChart.Series<String, Number>, ArrayList<XYChart.Data<String, Number>>> getRemovalItems(double min, double max) {
        HashMap<XYChart.Series<String, Number>, ArrayList<XYChart.Data<String, Number>>> removeMap = new HashMap<>();
        for (XYChart.Series<String, Number> serie : lineChart.getData()) {
            for (XYChart.Data<String, Number> item : serie.getData()) {
                double itemX = item.getNode().getLayoutX();
                double width = item.getNode().sceneToLocal(item.getNode().getBoundsInLocal()).getWidth();
                if (itemX < min || itemX + width > max) {
                    if (removeMap.get(serie) == null) {
                        ArrayList<XYChart.Data<String, Number>> arrayList = new ArrayList<>();
                        arrayList.add(item);
                        removeMap.put(serie, arrayList);
                    } else {
                        removeMap.get(serie).add(item);
                    }
                }
            }
        }
        return removeMap;
    }

    /**
     *  Resets the view, to original values. If a plot has been added, the view will be reset including it.
     */
    @FXML
    private void resetView() {
        xAxis.getCategories().clear();
        lineChart.getData().clear();
        lineChart.layout();
        series1.getData().clear();
        series2.getData().clear();
        series3.getData().clear();
        series4.getData().clear();
        xAxis.setAutoRanging(true);
        yAxis.setAutoRanging(true);
        setupGraphData();
        xAxis.autosize();
    }

    @FXML
    private void getDescription() {
        Shared.Functions.getOnlineHelp(HELP_SITE);
    }

    /**
     *  Adds a second set of data plotted alongside the original set of data.
     *  The second set is loaded from the codon statistics gene selection window.
     *  If no gene is selected, an error message popup will appear.
     */
    @FXML
    private void addPlot() {
        addedPlotData = composer.addPlot();
        if (addedPlotData.length > 0) {
            addedPlotFlag = true;
            addPlottedData();
        } else {
            showErrorWindow();
        }
    }

    /**
     * Error window which appears if there are no genes selected in the codon statistics
     * gene selection window.
     */
    private static void showErrorWindow(){
        String errorMessage = "Please select gene(s) from the previous Codon Statistics Gene Selection window.";
        Alert alert = new Alert(Alert.AlertType.NONE, errorMessage, ButtonType.OK);
        alert.setTitle("Selection Warning");
        alert.showAndWait();
    }

    /**
     * Clears all data in the chart.
     */
    private void clear() {
        xAxis.getCategories().clear();
        lineChart.getData().clear();
        lineChart.layout();
        series1.getData().clear();
        series2.getData().clear();
        series3.getData().clear();
        series4.getData().clear();
        xAxis.setAutoRanging(true);
        yAxis.setAutoRanging(true);
        lineChart.setAnimated(false);
        xAxis.setAnimated(false);
        yAxis.setAnimated(false);
    }

    /**
     *  Adds the newly added dataset to the chart to series' alongside the original series'.
     *  Sets up a listener which will display the data corresponding to item clicked.
     */
    private void addPlottedData() {
        clear();
        set1Name = "Set 1 ";
        set2Name = "Set 2 ";
        addedPlotSeries1 = new XYChart.Series<>();
        addedPlotSeries2 = new XYChart.Series<>();
        addedPlotSeries3 = new XYChart.Series<>();
        addedPlotSeries4 = new XYChart.Series<>();

        if (type.equals("NT")) {
            setupNTSeries();
        } else {
            setupICSeries();
        }

        for (XYChart.Series<String, Number> serie : lineChart.getData()) {
            for (XYChart.Data<String, Number> item : serie.getData()) {
                item.getNode().setOnMousePressed((MouseEvent event) -> {
                    xValueLabel.setText(serie.getName());
                    yValueLabel.setText(item.getYValue().toString());
                });
            }
        }
    }

    /** Sets the Added Nucleotide Series data points for a category of a nucleotide.
     * @param nucleotide - nucleotide given and to be displayed on graph (A, c, G or T)
     * @param index - the index of the corresponding data for the nucleotide
     */
    private void setAddedSeriesNT(String nucleotide, int index) {
        addedPlotSeries1.getData().add(new XYChart.Data<>(nucleotide, addedPlotData[index*3]));
        addedPlotSeries2.getData().add(new XYChart.Data<>(nucleotide, addedPlotData[index*3+1]));
        addedPlotSeries3.getData().add(new XYChart.Data<>(nucleotide, addedPlotData[index*3+2]));
    }

    /** Sets the Added Intracodon position series data points for a category of an intracodon position.
     * @param position - intracodon position given and to be displayed on graph (1, 2, or 3)
     * @param index - the index of the corresponding data for the intracodon position
     */
    private void setAddedSeriesIC(String position, int index) {
        addedPlotSeries1.getData().add(new XYChart.Data<>(position, addedPlotData[index]));
        addedPlotSeries2.getData().add(new XYChart.Data<>(position, addedPlotData[index+3]));
        addedPlotSeries3.getData().add(new XYChart.Data<>(position, addedPlotData[index+6]));
        addedPlotSeries4.getData().add(new XYChart.Data<>(position, addedPlotData[index+9]));
    }

    /**
     *   Sets names for the chart's legend and adds all data points to the chart.
     */
    private void setupNTSeries() {
        series1.setName(set1Name + "Position 1");
        series2.setName(set1Name + "Position 2");
        series3.setName(set1Name + "Position 3");
        xAxis.setLabel("Nucleotide");
        setSeriesNT("A", 0);
        setSeriesNT("C", 1);
        setSeriesNT("G", 2);
        setSeriesNT("T", 3);
        lineChart.getData().add(series1);
        lineChart.getData().add(series2);
        lineChart.getData().add(series3);
        if (addedPlotFlag) {
            addedPlotSeries1.setName(set2Name + "Position 1");
            addedPlotSeries2.setName(set2Name + "Position 2");
            addedPlotSeries3.setName(set2Name + "Position 3");
            setAddedSeriesNT("A", 0);
            setAddedSeriesNT("C", 1);
            setAddedSeriesNT("G", 2);
            setAddedSeriesNT("T", 3);
            lineChart.getData().add(addedPlotSeries1);
            lineChart.getData().add(addedPlotSeries2);
            lineChart.getData().add(addedPlotSeries3);
        }
    }

    /**
     *  Sets names for the chart's legend and adds all data points to the chart.
     */
    private void setupICSeries() {
        series1.setName(set1Name + "A");
        series2.setName(set1Name + "C");
        series3.setName(set1Name + "G");
        series4.setName(set1Name + "T");
        xAxis.setLabel("Position");
        setSeriesIC("1", 0);
        setSeriesIC("2", 1);
        setSeriesIC("3", 2);
        lineChart.getData().add(series1);
        lineChart.getData().add(series2);
        lineChart.getData().add(series3);
        lineChart.getData().add(series4);
        if (addedPlotFlag) {
            addedPlotSeries1.setName(set2Name + "A");
            addedPlotSeries2.setName(set2Name + "C");
            addedPlotSeries3.setName(set2Name + "G");
            addedPlotSeries4.setName(set2Name + "T");
            setAddedSeriesIC("1", 0);
            setAddedSeriesIC("2", 1);
            setAddedSeriesIC("3", 2);
            lineChart.getData().add(addedPlotSeries1);
            lineChart.getData().add(addedPlotSeries2);
            lineChart.getData().add(addedPlotSeries3);
            lineChart.getData().add(addedPlotSeries4);
        }
    }
}
