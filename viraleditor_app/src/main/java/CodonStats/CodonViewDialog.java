package CodonStats;
//import Shared.Gene;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.MenuItem;
import javafx.scene.control.SelectionMode;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.stage.FileChooser;
import javafx.stage.Stage;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
//import java.util.List;
import java.util.List;

/**
 * Handles UI of Codon View window, displaying statistics of nucleotides (a1, a2, ... t3).
 */
public class CodonViewDialog {
    private CodonViewComposer composer;
    private ObservableList<NucleotideCount> nucleotideCounts;
    private ObservableList<NucleotideStats> nucleotideStats;
    private List<NucleotideStats> statistics;

    private static final String HELP_SITE = "https://4virology.net/virology-ca-tools/codonstatistics/";

    @FXML
    private TableView<NucleotideCount> table;
    @FXML
    private TableColumn<String, NucleotideCount> tableName;
    @FXML
    private TableColumn<Double, NucleotideCount> a1;
    @FXML
    private TableColumn<Double, NucleotideCount> a2;
    @FXML
    private TableColumn<Double, NucleotideCount> a3;
    @FXML
    private TableColumn<Double, NucleotideCount> c1;
    @FXML
    private TableColumn<Double, NucleotideCount> c2;
    @FXML
    private TableColumn<Double, NucleotideCount> c3;
    @FXML
    private TableColumn<Double, NucleotideCount> g1;
    @FXML
    private TableColumn<Double, NucleotideCount> g2;
    @FXML
    private TableColumn<Double, NucleotideCount> g3;
    @FXML
    private TableColumn<Double, NucleotideCount> t1;
    @FXML
    private TableColumn<Double, NucleotideCount> t2;
    @FXML
    private TableColumn<Double, NucleotideCount> t3;

    @FXML
    private TableView<NucleotideStats> sumTable;
    @FXML
    private TableColumn<String, NucleotideStats> description;
    @FXML
    private TableColumn<Double, NucleotideStats> a1Sum;
    @FXML
    private TableColumn<Double, NucleotideStats> a2Sum;
    @FXML
    private TableColumn<Double, NucleotideStats> a3Sum;
    @FXML
    private TableColumn<Double, NucleotideStats> c1Sum;
    @FXML
    private TableColumn<Double, NucleotideStats> c2Sum;
    @FXML
    private TableColumn<Double, NucleotideStats> c3Sum;
    @FXML
    private TableColumn<Double, NucleotideStats> g1Sum;
    @FXML
    private TableColumn<Double, NucleotideStats> g2Sum;
    @FXML
    private TableColumn<Double, NucleotideStats> g3Sum;
    @FXML
    private TableColumn<Double, NucleotideStats> t1Sum;
    @FXML
    private TableColumn<Double, NucleotideStats> t2Sum;
    @FXML
    private TableColumn<Double, NucleotideStats> t3Sum;

    @FXML
    private MenuItem viewA;
    @FXML
    private MenuItem viewC;
    @FXML
    private MenuItem viewG;


    public void initComposer(CodonViewComposer composer) {
        this.composer = composer;
    }

    /** Loads data into tables for display.
     * @param nucleotides - list of nucleotideCounts which will appear in chart
     */
    public void setColumns(List<NucleotideCount> nucleotides) {
        nucleotideCounts = FXCollections.observableArrayList(nucleotides);
        tableName.setCellValueFactory(new PropertyValueFactory<>("geneName"));
        a1.setCellValueFactory(new PropertyValueFactory<>("a1"));
        a2.setCellValueFactory(new PropertyValueFactory<>("a2"));
        a3.setCellValueFactory(new PropertyValueFactory<>("a3"));
        c1.setCellValueFactory(new PropertyValueFactory<>("c1"));
        c2.setCellValueFactory(new PropertyValueFactory<>("c2"));
        c3.setCellValueFactory(new PropertyValueFactory<>("c3"));
        g1.setCellValueFactory(new PropertyValueFactory<>("g1"));
        g2.setCellValueFactory(new PropertyValueFactory<>("g2"));
        g3.setCellValueFactory(new PropertyValueFactory<>("g3"));
        t1.setCellValueFactory(new PropertyValueFactory<>("t1"));
        t2.setCellValueFactory(new PropertyValueFactory<>("t2"));
        t3.setCellValueFactory(new PropertyValueFactory<>("t3"));
        table.setItems(nucleotideCounts);
        table.getSelectionModel().setSelectionMode(SelectionMode.MULTIPLE);

        statistics = composer.setSumColumnData(nucleotides);
        setSumColumns();
    }

    /**
     * Populates the sum table with data of calculated statistics of displayed nucleotide counts.
     */
    public void setSumColumns() {
        nucleotideStats = FXCollections.observableArrayList(statistics);
        description.setCellValueFactory(new PropertyValueFactory<>("description"));
        a1Sum.setCellValueFactory(new PropertyValueFactory<>("a1Sum"));
        a2Sum.setCellValueFactory(new PropertyValueFactory<>("a2Sum"));
        a3Sum.setCellValueFactory(new PropertyValueFactory<>("a3Sum"));
        c1Sum.setCellValueFactory(new PropertyValueFactory<>("c1Sum"));
        c2Sum.setCellValueFactory(new PropertyValueFactory<>("c2Sum"));
        c3Sum.setCellValueFactory(new PropertyValueFactory<>("c3Sum"));
        g1Sum.setCellValueFactory(new PropertyValueFactory<>("g1Sum"));
        g2Sum.setCellValueFactory(new PropertyValueFactory<>("g2Sum"));
        g3Sum.setCellValueFactory(new PropertyValueFactory<>("g3Sum"));
        t1Sum.setCellValueFactory(new PropertyValueFactory<>("t1Sum"));
        t2Sum.setCellValueFactory(new PropertyValueFactory<>("t2Sum"));
        t3Sum.setCellValueFactory(new PropertyValueFactory<>("t3Sum"));
        sumTable.setItems(nucleotideStats);
        sumTable.getSelectionModel().setSelectionMode(SelectionMode.MULTIPLE);
    }

    @FXML
    private void openBarPlot() {
        composer.openBarChart(new ArrayList<>(nucleotideCounts));
    }

    @FXML
    private void openLinePlot() {
        composer.openLineChart(new ArrayList<>(nucleotideCounts));
    }

    /** Opens nucleotide distribution window from menu.
     * @param event - event of which menu item is selected
     */
    @FXML
    private void viewNucleotide(ActionEvent event) {
        ArrayList<NucleotideCount> selectedItems = new ArrayList<>(nucleotideCounts);
        if (event.getSource() == viewA) {
            composer.openNucleotideView('A', selectedItems);
        } else if (event.getSource() == viewC) {
            composer.openNucleotideView('C', selectedItems);
        } else if (event.getSource() == viewG) {
            composer.openNucleotideView('G', selectedItems);
        } else {
            composer.openNucleotideView('T', selectedItems);
        }
    }

    @FXML
    private void saveFile() {
        try {
            FileChooser fileChooser = new FileChooser();
            fileChooser.setTitle("Save File");
            File file = fileChooser.showSaveDialog(null);
            composer.saveData(file, nucleotideCounts, nucleotideStats);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @FXML
    private void openDescription() {
        Shared.Functions.getOnlineHelp(HELP_SITE);
    }

    @FXML
    private void closeWindow() {
        Stage stage = (Stage) table.getScene().getWindow();
        stage.close();
    }
}
