package CodonStats;

import javafx.fxml.FXML;
import javafx.scene.control.Label;
import javafx.stage.Stage;

/**
 * Creates a popup including a description of what this portion of the tool does.
 */
public class CodonPopupDialog {

    @FXML
    private Label description;

    /**
     * Sets the description to the corresponding one for the distribution of a
     * nucleotide.
     */
    public void setDescription() {
        description.setText("It displays the distribution of the nucleotide"
                + " in each codon position.\n"
                + "A's represents the percentage of a's in each"
                + " of genes computed by the number of a's "
                + "occurred in one gene divided by the length of that gene. "
                + "a_1 represents the percentage of a's occurred"
                + " in the first position of each codon "
                + "which is computed by the number of a's occurred in"
                + " the position divided by the number "
                + "of a's in a gene; a_2 and a_3 similarly to a_1.\n"
                + "In the bottom of the table, the sum, mean, and "
                + "the variance are displayed, "
                + "which are the statistical data of the set of genes."
                + " The mean are calculated by "
                + "the formula of: \n\n Mean = Sum/number of genes selected"
                + "\n\nThe variance is estimated by the formula of:\n\n"
                + "Var = sum of(yi - Mean)^2 / (number of genes - 1)\n Where yi is"
                + " the ith gene in the table and i from 1 to the number"
                + " of genes.\n\n");
    }

    /**
     * Closes the description window.
     */
    @FXML
    private void closeWindow() {
        Stage stage = (Stage) description.getScene().getWindow();
        stage.close();
    }

}
