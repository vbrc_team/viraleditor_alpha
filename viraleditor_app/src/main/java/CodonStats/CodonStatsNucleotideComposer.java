package CodonStats;

import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Modality;
import javafx.stage.Stage;
import javafx.stage.Window;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

/**
 * Handles calculations of data and opening menu items from Nucleotide Dis View.
 */
public class CodonStatsNucleotideComposer {
    private Double[][] sumData;

    /** Calculates the Nucleotide Statistics for the table in Nucleotide Distribution table.
     * @param nucleotide - nucleotide statistics are calculated for
     * @param nc - arraylist of nucleotide counts for genes selected
     * @return arraylist of nucleotide stats calculated
     */
    public List<NucleotideStats> calculateNucleotideDisStats(char nucleotide, List<NucleotideCount> nc) {
        NucleotideCalculations calculations = new NucleotideCalculations(nc);
        sumData = calculations.getDSum(nucleotide);
        List<double[]> data = calculations.getDisplayDis(nucleotide);
        List<NucleotideStats> stats = new ArrayList<>();

        for (int i = 0; i < nc.size(); i++) {
            nc.get(i).setNucleotides(data.get(i));
        }
        NucleotideStats stat = setStatistics("Sum", 0);
        stats.add(stat);
        stat = setStatistics("Mean", 1);
        stats.add(stat);
        stat = setStatistics("Var * 0.001", 2);
        stats.add(stat);
        return stats;
    }

    /**
     * @param description - description of the statistic (i.e. sum, mean, etc)
     * @param index - index of sum data where data corresponding to description is
     * @return nucleotide statistic created
     */
    private NucleotideStats setStatistics(String description, int index) {
        NucleotideStats ns = new NucleotideStats();
        ns.setDescription(description);
        double[] dataDisplay = new double[4];
        for (int i = 0; i < 4; i++) {
            dataDisplay[i] = sumData[index][i];
        }
        ns.setNucleotideTotals(dataDisplay);
        return ns;
    }

    /** Loads the popup window and opens it.
     * @param window - window which is the owner of the popup
     */
    public void openDescription(Window window) {
        try {
            FXMLLoader loader = new FXMLLoader(getClass().getClassLoader().getResource("CodonPopup.fxml"));
            Parent root = loader.load();
            CodonPopupDialog controller = loader.getController();
            controller.setDescription();
            openWindow(root, window);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    /** Opens a popup window of description of current window.
     * @param root - parent of opening window
     * @param window - window which is to be set as owner of popup window
     */
    private void openWindow(Parent root, Window window) {
        Stage popup = new Stage();
        popup.initModality(Modality.WINDOW_MODAL);
        popup.initOwner(window);
        popup.setTitle("Codon Stats Description");
        popup.setScene(new Scene(root));
        popup.show();
    }

}
