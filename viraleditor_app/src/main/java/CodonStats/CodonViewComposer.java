package CodonStats;

import java.io.*;
import java.text.NumberFormat;
import java.util.ArrayList;
import java.util.List;

import javafx.collections.ObservableList;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;

/**
 * Handles calculations of data to be loaded in Codon View and logic of menu items.
 */
public class CodonViewComposer {
    private ArrayList<NucleotideStats> statistics;


    /**Calculates gene's nucleotide counts and displays them in the table.
     * @param genesList - list of genes of which data will be displayed for.
     * @return nucleotideList to be added to the table
     */
    public List<NucleotideCount> setTableData(List<Gene> genesList) {
        ArrayList<NucleotideCount> nucleotides = new ArrayList<>();
        for (Gene gene : genesList) {
            NucleotideCount nc = new NucleotideCount(gene.getGeneName(), gene.getDnaSeq());
            nucleotides.add(nc);
        }
        return nucleotides;
    }

    /** Calculates the data to be added into the sum table.
     * @param nucleotideCounts arraylist of nucleotide counts calculated
     * @return arraylist of nucleotide statistics calculations
     */
    public List<NucleotideStats> setSumColumnData(List<NucleotideCount> nucleotideCounts) {
        NucleotideCalculations calculations = new NucleotideCalculations(nucleotideCounts);
        statistics = new ArrayList<>();
        for (int i = 0; i < 6; i++) {
            NucleotideStats ns = new NucleotideStats();
            statistics.add(ns);
        }
        calculations.setCIBounds();
        double[] result = new double[12];
        int numSeq = calculations.getNumSeq();
        for (int i = 0; i < 12; i++) {
            result[i] = numSeq;
        }

        setRowData(0, "N", result);
        setRowData(1, "Sum", calculations.getSum());
        setRowData(2, "Mean", calculations.getMean());
        setRowData(3, "Var*0.001", calculations.getVariance());
        setRowData(4, "95%CI Low Bound", calculations.getCILowerBound());
        setRowData(5, "95%CI Up Bound", calculations.getCIUpperBound());

        return statistics;
    }

    public NucleotideStats setRowData(int index, String description, double[] data) {
        statistics.get(index).setDescription(description);
        statistics.get(index).setSums(data);
        return statistics.get(index);
    }


    /** Opens a bar chart with the nucleotide counts (a1,a2,...t3).
     * @param nc - nucleotide count for each gene
     */
    public void openBarChart(List<NucleotideCount> nc) {
        NucleotideCalculations calculations = new NucleotideCalculations(nc);
        Double[][] sumA = calculations.getDSum('A');
        Double[][] sumC = calculations.getDSum('C');
        Double[][] sumG = calculations.getDSum('G');
        Double[][] sumT = calculations.getDSum('T');
        double[] plotData = new double[12];
        for (int i = 0; i < 3; i++) {
            plotData[i] = sumA[1][i + 1];
            plotData[i+3] = sumC[1][i+1];
            plotData[i+6] = sumG[1][i+1];
            plotData[i+9] = sumT[1][i+1];
        }
        try {
            FXMLLoader loader = new FXMLLoader(getClass().getClassLoader().getResource("CodonStatsBarGraph.fxml"));
            Parent root = loader.load();
            CodonStatsBarGraphDialog controller = loader.getController();
            CodonStatsGraphComposer composer = new CodonStatsGraphComposer();
            composer.setSummary(plotData);
            controller.initComposer(composer);
            controller.setType("IC");
            controller.setSource("Codon Plot");
            controller.setLabels("Fraction:");
            controller.setupGraphData();
            openWindow(root);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    /** Opens a line chart with the nucleotide counts (a1,a2,...t3).
     * @param nc - nucleotide count for each gene
     */
    public void openLineChart(List<NucleotideCount> nc) {
        NucleotideCalculations calculations = new NucleotideCalculations(nc);
        Double[][] sumA = calculations.getDSum('A');
        Double[][] sumC = calculations.getDSum('C');
        Double[][] sumG = calculations.getDSum('G');
        Double[][] sumT = calculations.getDSum('T');
        double[] plotData = new double[12];
        for (int i = 0; i < 3; i++) {
            plotData[i] = sumA[1][i+1];
            plotData[i+3] = sumC[1][i+1];
            plotData[i+6] = sumG[1][i+1];
            plotData[i+9] = sumT[1][i+1];
        }
        try {
            FXMLLoader loader = new FXMLLoader(getClass().getClassLoader().getResource("CodonStatsLineGraph.fxml"));
            Parent root = loader.load();
            CodonStatsLineGraphDialog controller = loader.getController();
            CodonStatsGraphComposer composer = new CodonStatsGraphComposer();
            composer.setSummary(plotData);
            controller.initComposer(composer);
            controller.setType("IC");
            controller.setSource("Codon Plot");
            controller.setLabels("Fraction:");
            controller.setupGraphData();

            openWindow(root);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private void openWindow(Parent root) {
        Stage popup = new Stage();
        popup.setTitle("Distribution Plot");
        popup.setScene(new Scene(root));
        popup.show();
    }



    /**Sets the data to be displayed in the Nucleotide Distribution window and opens it.
     * @param nucleotide - nucleotide to display distribution for.
     * @param nc - nucleotide counts for each gene
     */
    public void openNucleotideView(char nucleotide, List<NucleotideCount> nc) {
        try {
            FXMLLoader loader = new FXMLLoader(getClass().getClassLoader().getResource("CodonStatsNucleotideDis.fxml"));
            Parent root = loader.load();
            CodonStatsNucleotideDialog controller = loader.getController();
            CodonStatsNucleotideComposer composer = new CodonStatsNucleotideComposer();
            List<NucleotideStats> stats = composer.calculateNucleotideDisStats(nucleotide, nc);
            controller.initComposer(composer);
            controller.setNucleotide(nucleotide);
            controller.setTable(nc);
            controller.setSumTable(stats);
            controller.setupCharts();

            Stage popup = new Stage();
            popup.setTitle("Codon Stats Distribution");
            popup.setScene(new Scene(root));
            popup.show();

        } catch (IOException e) {
            e.printStackTrace();
        }
    }



    /** Saves data to text file on user's machine with data displayed on the screen.
     * @param file - file where data will be saved (specified by user)
     * @param nucleotideCounts - nucleotide counts displayed
     * @param nucleotideStats - nucleotide statistics displayed
     * @throws IOException - throws exception if writer cannot be flushed or closed
     */
    public void saveData(File file, ObservableList<NucleotideCount> nucleotideCounts, ObservableList<NucleotideStats> nucleotideStats) throws IOException {
        Writer writer = null;
        NumberFormat numberFormat = NumberFormat.getNumberInstance();
        numberFormat.setMaximumFractionDigits(4);
        numberFormat.setMinimumFractionDigits(4);
        if (!file.getName().contains(".")) {
            file = new File(file.getAbsolutePath() + ".txt");
        }
        try {
            writer = new BufferedWriter(new FileWriter(file));
            writer.write("#Results Table\n");
            String line = "Gene Name" + "\t\t" + "a_1" + "\t" + "a_2" + "\t" + "a_3" + "\t" + "c_1" + "\t" + "c_2" + "\t"
                    + "c_3" + "\t" + "g_1" + "\t" + "g_2" + "\t" + "g_3" + "\t" + "t_1" + "\t" + "t_2" + "\t" + "t_3" + "\n\n";
            writer.write(line);
            for (NucleotideCount count : nucleotideCounts) {
                line = count.getGeneName() + "\t\t" + count.getA1() + "\t" + count.getA2() + "\t" + count.getA3() + "\t"
                        + count.getC1() + "\t" + count.getC2() + "\t" + count.getC3() + "\t"  + count.getG1() + "\t"
                        + count.getG2() + "\t" + count.getG3() + "\t" + count.getT1() + "\t" + count.getT2() + "\t"
                        + count.getT3() + "\n";
                writer.write(line);
            }
            for (NucleotideStats stat : nucleotideStats) {
                if ( stat.getDescription().startsWith("Var") || stat.getDescription().startsWith("95")) {
                    line = stat.getDescription() + "\t\t" + stat.getA1Sum() + "\t" + stat.getA2Sum() + "\t" + stat.getA3Sum() + "\t"
                            + stat.getC1Sum() + "\t" + stat.getC2Sum() + "\t" + stat.getC3Sum() + "\t"  + stat.getG1Sum() + "\t"
                            + stat.getG2Sum() + "\t" + stat.getG3Sum() + "\t" + stat.getT1Sum() + "\t" + stat.getT2Sum() + "\t"
                            + stat.getT3Sum() + "\n";
                } else {
                    line = stat.getDescription() + "\t\t\t" + stat.getA1Sum() + "\t" + stat.getA2Sum() + "\t" + stat.getA3Sum() + "\t"
                            + stat.getC1Sum() + "\t" + stat.getC2Sum() + "\t" + stat.getC3Sum() + "\t" + stat.getG1Sum() + "\t"
                            + stat.getG2Sum() + "\t" + stat.getG3Sum() + "\t" + stat.getT1Sum() + "\t" + stat.getT2Sum() + "\t"
                            + stat.getT3Sum() + "\n";
                }
                writer.write(line);
            }
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            assert writer != null;
            writer.flush();
            writer.close();
        }
    }
}
