package Annotator.preferences;
import Shared.preferences.VocsPrefs;

public class GatuPreferences {
    //with default values in case of parse exception
    private final VocsPrefs vpref = VocsPrefs.getInstance();
    public int orfLength = 180;
    public int maxNoChannels = 4;
    public int minSimilarity = 60;
    public int orfDifference = 50;
    public int xpos = 100, ypos = 100;
    public int blast_dropoff = 50;
    public int blastp_tblastn_wz = 2;
    public int blastn_wz = 7;
    public int blast_descriptions = 10;
    public int blast_alignments = 10;
    public boolean showRefMap;
    public boolean showOrfsInMap;
    public boolean showBelowThreshold;
    public boolean htmlBlast;
    public boolean ncbidb;
    public boolean useScreenMenuBar;
    public boolean geneOnBothStrands;
    public double blast_expect = 0.01;
    public int divider_loc = 350;
    public String matrix;
    public String blastdb;


    /**
     * Constructor
     * - Loads the preferences for the gatu program from the vocs preferences
     * @param vdb vocs db name
     */
    public GatuPreferences(String vdb) {
        blastdb = vdb;
        if (!vdb.equalsIgnoreCase("none")) {
            vpref.set_general("GenomeAnnotator_VOCS_blast_db", vdb);
        }

        String str = vpref.get_general("GenomeAnnotator_MinSimilarity");
        if (str != null) {
            try{
                minSimilarity = Integer.parseInt(str);
            } catch(Exception e){System.out.println("minSimilarity: using default");}
        }


        str = vpref.get_general("GenomeAnnotator_orf_difference");
        if (str != null){
            try{
                orfDifference = Integer.parseInt(str);
            } catch(Exception e){System.out.println("orfDifference: using default");}
        }

        str = vpref.get_general("GenomeAnnotator_OrfLength");
        if (str != null) {
            try{
                orfLength = Integer.parseInt(str);
            } catch(Exception e){System.out.println("orfLength: using default");}
        }

        str = vpref.get_general("GenomeAnnotator_Max_No_Channels");
        if (str != null) {
            try{
                maxNoChannels = Integer.parseInt(str);
            } catch(Exception e){System.out.println("maxNoChannels: using default");}
        }

        str = vpref.get_general("GenomeAnnotator_GenesOnBothStrands");
        if (str == null) str = "false";
        geneOnBothStrands = !str.equalsIgnoreCase("false");

        str = vpref.get_general("GenomeAnnotator_NCBI_blast_db");
        if (str == null ||str.length() < 1) str = "true";
        ncbidb = !str.equalsIgnoreCase("false");

        str = vpref.get_general("GenomeAnnotator_VOCS_blast_db");
        if (str == null ||str.length() < 1 ) str = "none";
        if (!str.equalsIgnoreCase("none")) blastdb = str;

        str = vpref.get_general("GenomeAnnotator_show_orf_in_map");
        if (str == null || str.length() < 1) str = "false";
        showOrfsInMap = str.equalsIgnoreCase("true");

        str = vpref.get_general("GenomeAnnotator_show_below_threshold");
        if (str == null || str.length() < 1) str = "false";
        showBelowThreshold = str.equalsIgnoreCase("true");

        str = vpref.get_general("GenomeAnnotator_show_reference_map");
        if (str == null || str.length() < 1) str = "true";
        showRefMap = !str.equalsIgnoreCase("false");

        str = vpref.get_general("GenomeAnnotator_blast_output_format_html");
        if (str == null || str.length() < 1) str = "true";
        htmlBlast = !str.equalsIgnoreCase("false");

        str = vpref.get_general("GenomeAnnotator_blast_wordsize");
        try{
            if (str != null) blastp_tblastn_wz = Integer.parseInt(str);
        } catch(Exception e){System.out.println("blastp_tblastn_wz: using default");}


        str = vpref.get_general("GenomeAnnotator_blastn_wordsize");
        try{
            if (str != null) blastn_wz = Integer.parseInt(str);
        } catch(Exception e){System.out.println(" blastn_wz: using default");}

        str = vpref.get_general("GenomeAnnotator_blast_expect");
        try {
            if (str != null) blast_expect = Double.parseDouble(str);
        } catch (Exception e) {System.out.println("blast_expect: using default");}


        str = vpref.get_general("GenomeAnnotator_blast_dropoff");
        try {
            if (str != null) blast_dropoff = Integer.parseInt(str);
        } catch (Exception e) {System.out.println("blast_dropoff: using default");}

        str = vpref.get_general("GenomeAnnotator_blast_alignments");
        try {
            if (str != null) blast_alignments = Integer.parseInt(str);
        } catch (Exception e) {System.out.println("blast_alignments: using default");}

        str = vpref.get_general("GenomeAnnotator_blast_descriptions");
        try {
            if (str != null) blast_descriptions = Integer.parseInt(str);
        } catch (Exception e){System.out.println("blast_descriptions: using default");}


        str = vpref.get_general("GenomeAnnotator_blast_matrix");
        if (str == null || str.length() < 1) str = "BLOSUM62";
        matrix = str;

        str = vpref.get_general("GenomeAnnotator_window_x_location");
        try {
            if (str != null) xpos = Integer.parseInt(str);
        } catch (Exception e) {System.out.println("xpos : using default");}


        str = vpref.get_general("GenomeAnnotator_window_y_location");
        try {
            if (str != null) ypos = Integer.parseInt(str);
        } catch (Exception e) {System.out.println("ypos: using default");}

        str = vpref.get_general("GenomeAnnotator_divider_location");
        try {
            if (str != null) divider_loc = Integer.parseInt(str);
        } catch (Exception e) {System.out.println("divider_loc: using default");}

        str = vpref.get_general("GenomeAnnotator_useScreenMenuBar");
        if (str == null || str.length() < 1) str = "true";
        useScreenMenuBar = !str.equalsIgnoreCase("false");
    }

    /**
     * set the minimum orf length
     * @param olen the minimum orf length
     * @return 1 if value has been changed, -1 otherwise
     */
    public int setOrfLength(int olen) {
        if (orfLength != olen) {
            orfLength = olen;
            vpref.set_general("GenomeAnnotator_OrfLength", "" + orfLength);
            return 1;
        } else  {
            return -1;
        }
    }

    /**
     * set the maximum numbr of display channels in the genome map
     * @param mch the maximum number of channels
     * @return 1 if value has been changed, -1 otherwise
     */
    public int setMaxChannels(int mch) {
        if (maxNoChannels != mch) {
            maxNoChannels = mch;
            vpref.set_general("GenomeAnnotator_Max_No_Channels", "" + maxNoChannels);
            return 1;
        } else {
            return -1;
        }
    }

    /**
     * set the maximum difference in an ORF to an annotation to
     * consider the equal
     * @param mo the maximum difference between orf and an annotation
     * @return 1 if value has been changed, -1 otherwise
     */
    public int setMaxOrfDifference(int mo) {
        if (orfDifference != mo) {
            orfDifference = mo;
            vpref.set_general("GenomeAnnotator_orf_difference", "" + orfDifference);
            return 1;
        } else {
            return -1;
        }
    }

    /**
     * Define the needle threshold, ie the minimum
     * % similarity of an alignment in order to be accepted
     * @param msim the minimum similarity
     * @return 1 if value has been changed, -1 otherwise
     */
    public int setMinSimilarity(int msim) {
        if (minSimilarity != msim) {
            minSimilarity = msim;
            vpref.set_general("GenomeAnnotator_MinSimilarity", "" + minSimilarity);
            return 1;
        } else {
            return -1;
        }
    }

    /**
     * Define the vocs datatbase to blast ORFs against
     * @param vbdb the vocs database
     */
    public void setVocsBlastDb(String vbdb) {
        if (! blastdb.equals(vbdb)) {
            blastdb = vbdb;
            vpref.set_general("GenomeAnnotator_VOCS_blast_db", blastdb);

        }

    }

    /**
     * Define if genes are to be found on bothg strands, ie independend
     * from the strand the reference gene was on
     * @param onb true if gene can be on both strands,
     *            false if gene must be on same strand than reference gene
     * @return 1 if value has been changed, -1 otherwise
     */
    public int setFindGenesOnBothStrands(boolean onb) {
        if (geneOnBothStrands != onb) {
            geneOnBothStrands = onb;
            if (geneOnBothStrands) {
                vpref.set_general("GenomeAnnotator_GenesOnBothStrands", "true");
            } else {
                vpref.set_general("GenomeAnnotator_GenesOnBothStrands", "false");
            }
            return 1;
        } else {
            return -1;
        }
    }

    /**
     * Define if blasting ORFs against NCBI nr db or against VOCs db
     * @param nbdb true if blast against NCBI nr db
     *             false if blast against VOCs db
     * @return 1 if value has been changed, -1 otherwise
     */
    public int setBlastNcbi(boolean nbdb) {
        if (ncbidb != nbdb) {
            ncbidb = nbdb;
            if (ncbidb) {
                vpref.set_general("GenomeAnnotator_NCBI_blast_db", "true");
            } else {
                vpref.set_general("GenomeAnnotator_NCBI_blast_db", "false");
            }
            return 1;
        } else {
            return -1;
        }
    }

    /**
     * Define the format of the balst outputs when blasting ORFs
     * @param bform true if blast output in HTML
     *              false if blast output in TEXT
     * @return 1 if value has been changed, -1 otherwise
     */
    public int setBlastOutputFormat(boolean bform) {
        if (htmlBlast != bform) {
            htmlBlast = bform;
            if (htmlBlast) {
                vpref.set_general("GenomeAnnotator_blast_output_format_html", "true");
            } else {
                vpref.set_general("GenomeAnnotator_blast_output_format_html", "false");
            }
            return 1;
        } else {
            return -1;
        }
    }

    /**
     * Define if ORFs are to be displayed in genome map
     * @param dorf true if orfs are displayed
     *             false if orfs are hidden
     * @return 1 if value has been changed, -1 otherwise
     */
    public int setShowOrfsInMap(boolean dorf) {
        if (showOrfsInMap != dorf) {
            showOrfsInMap = dorf;
            if (showOrfsInMap) {
                vpref.set_general("GenomeAnnotator_show_orf_in_map", "true");
            } else {
                vpref.set_general("GenomeAnnotator_show_orf_in_map", "false");
            }
            return 1;
        } else {
            return -1;
        }
    }

    /**
     * Define if annotations below the threshold are to be displayed in genome map as bars
     * @param belowThresh true if annotations below threshold are displayed
     *             false if annotations below threshold are hidden
     * @return 1 if value has been changed, -1 otherwise
     */
    public int setShowBelowThreshold(boolean belowThresh) {
        if (showBelowThreshold != belowThresh) {
            showBelowThreshold = belowThresh;
            if (showBelowThreshold) {
                vpref.set_general("GenomeAnnotator_show_below_threshold", "true");
            } else {
                vpref.set_general("GenomeAnnotator_show_below_threshold", "false");
            }
            return 1;
        } else {
            return -1;
        }
    }



    /**
     * Define if genome map of reference genome is to be displayed
     * @param srefmap true if genome map for reference genome is displayed
     *                false if genome map for reference genome is hidden
     * @return 1 if value has been changed, -1 otherwise
     */
    public int setShowReferenceMap(boolean srefmap) {
        if (showRefMap != srefmap) {
            showRefMap = srefmap;
            if (showRefMap) {
                vpref.set_general("GenomeAnnotator_show_reference_map", "true");
            } else {
                vpref.set_general("GenomeAnnotator_show_reference_map", "false");
            }
            return 1;
        } else {
            return -1;
        }
    }

    /**
     * Define blast expect value
     * @param str the blast expect value
     */
    public void setBlastExpect(String str) {
        if (str == null) str = "" + blast_expect;
        if (str.length() < 1) str = "" + blast_expect;
        try {
            blast_expect = Double.parseDouble(str);
        } catch (Exception e) {e.printStackTrace();}
        vpref.set_general("GenomeAnnotator_blast_expect", "" + blast_expect);
    }

    /**
     * Define blast expect dropoff
     * @param str the blast dropoff value
     */
    public void setBlastDropoff(String str) {
        if (str == null) str = "" + blast_dropoff;
        if (str.length() < 1) str = "" + blast_dropoff;
        try {
            blast_dropoff = Integer.parseInt(str);
        } catch (Exception e) {e.printStackTrace();}
        vpref.set_general("GenomeAnnotator_blast_dropoff", "" + blast_dropoff);
    }

    /**
     * Define blast number of alignments
     * @param str the blast number of alignments
     */
    public void setBlastAlignments(String str) {
        if (str == null) str = "" + blast_alignments;
        if (str.length() < 1) str = "" + blast_alignments;
        try {
            blast_alignments = Integer.parseInt(str);
        } catch (Exception e) {e.printStackTrace();}
        vpref.set_general("GenomeAnnotator_blast_alignments",
                "" + blast_alignments);
    }

    /**
     * Define blast number of descriptions
     * @param str the blast number of descriptions
     */
    public void setBlastDescriptions(String str) {
        if (str == null) str = "" + blast_descriptions;
        if (str.length() < 1) str = "" + blast_descriptions;
        try {
            blast_descriptions = Integer.parseInt(str);
        } catch (Exception e) {e.printStackTrace();}
        vpref.set_general("GenomeAnnotator_blast_descriptions",
                "" + blast_descriptions);
    }

    /**
     * Define wordsize for blastp and tblastn
     * @param wz the wordsize for blastp and tblastn
     */
    public void setBlastWordSize(int wz) {
        if (blastp_tblastn_wz != wz) {
            blastp_tblastn_wz = wz;
            vpref.set_general("GenomeAnnotator_blast_wordsize",
                    "" + blastp_tblastn_wz);
        }
    }

    /**
     * Define wordsize for blastn
     * @param wz the wordsize for blastn
     */
    public void setBlastnWordSize(int wz) {
        if (blastn_wz != wz) {
            blastn_wz = wz;
            vpref.set_general("GenomeAnnotator_blastn_wordsize",
                    "" + blastn_wz);
        }
    }

    /**
     * Define matrix for blast
     * @param mat the matrix for blast
     */
    public void setMatrix(String mat) {
        if (!matrix.equals(mat) && mat != null) {
            if (mat.length() >= 1) {
                matrix = mat;
                vpref.set_general("GenomeAnnotator_blast_matrix", matrix);
            }
        }
    }






}
