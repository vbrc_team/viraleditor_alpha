package Annotator;

import Shared.Windows.LoadingPopupDialog;
import javafx.application.Platform;


public class AnnotatorComposer {

    public void setUp() {
        LoadingPopupDialog window = LoadingPopupDialog.OpenPopUp("Running BLAST, please wait...");

        //Start the task in a new thread so the UI doesn't freeze
        new Thread(() -> {
            try {
                Thread.sleep(1000); // give window priority
                testLoop();
                Platform.runLater(window::closeWindow);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }).start();

    }

    //fake task, replace with loading in data
    private void testLoop() {
        for(int i = 0; i < 199999; i++){
            System.out.println(i);
        }
    }
}
