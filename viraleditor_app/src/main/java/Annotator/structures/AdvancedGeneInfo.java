package Annotator.structures;

import Shared.Gene;

import java.util.Arrays;

/**
 * Created by localadmin on 2018-02-28.
 */
public class AdvancedGeneInfo {

    String dnaSequence;
    String proteinSequence;

    String[] aminoAcids = {"a", "r", "n", "d", "c", "e", "q", "g", "h", "i", "l", "k", "m", "f", "p", "s", "t", "w", "y", "v"};
    int[] countArray = new int[aminoAcids.length];

    public AdvancedGeneInfo(String dnaSequence, String proteinSequence) {
        this.dnaSequence = dnaSequence;
        this.proteinSequence = proteinSequence;

        Arrays.fill(countArray, 0);
    }

    public String getATPercentage() {
        int count = 0;

        for (int i = 0; i < dnaSequence.length(); i++) {

            String currentChar = dnaSequence.charAt(i) + "";

            if (currentChar.equalsIgnoreCase("a") || currentChar.equalsIgnoreCase("t")) {
                count++;
            }
        }
        return String.format("%.2f", (count / (double) dnaSequence.length()) * 100);
    }

    public String getIsoelectricPoint() {
        Gene m = new Gene();
        m.setProteinSeq(proteinSequence);
        m.calculatePI();

        return String.format("%.2f", m.getPi());
    }

    public String[] aminoAcidComposition() {

        String[] aminoAcidComposition = new String[aminoAcids.length];

        for (int i = 0; i < aminoAcids.length; i++) {
            for (int j = 0; j < proteinSequence.length(); j++) {

                String currentChar = proteinSequence.charAt(j) + "";

                if (currentChar.equalsIgnoreCase(aminoAcids[i])) {
                    countArray[i] = countArray[i] + 1;
                }
            }
        }

        for (int i = 0; i < aminoAcids.length; i++) {
            aminoAcidComposition[i] = String.format("%.2f", (countArray[i] / (double) proteinSequence.length()) * 100);
        }

        return aminoAcidComposition;
    }
}
