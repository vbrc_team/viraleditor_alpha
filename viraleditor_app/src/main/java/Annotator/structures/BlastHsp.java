package Annotator.structures;

/**
 * Stores the HSP data of a blast hit
 */
public class BlastHsp implements java.io.Serializable {

	public int num;
	public double bitScore;
	public int score;
	public double evalue_base;
	public int evalue_exp;
	public int query_from;
	public int query_to;
	public int hit_from;
	public int hit_to;
	public int query_frame;
	public int hit_frame;
	public int identity;
	public int positive;
	public int align_len;
	public String qseq;
	public String hseq;
	public String midline;


	/** 
	 * Default constructor
	 * Initializes all class variables
	 */
	public BlastHsp() {
		num = 0; bitScore = 0; score = 0; evalue_base = 0; evalue_exp = 0;
		query_from = 0; query_to = 0; hit_from = 0; hit_to = 0;
		query_frame = 0; hit_frame = 0; identity = 0; positive = 0;
		align_len = 0; qseq = ""; hseq = ""; midline = "";
	}

	public String toString() {
		String hsp = "HSP# " + num;
		hsp = hsp + "\nScore: " + score + " Bit Score: " + bitScore;
		hsp = hsp + "  E-value: " + evalue_base + "^" + evalue_exp;
		hsp = hsp + "\nQuery: " + query_from + "-" + query_to + " Frame: " + query_frame;
		hsp = hsp + "  Hit: " + hit_from + "-" + hit_to + " Frame: " + hit_frame;
		hsp = hsp + "\nIdentity: " + identity + " Positive: " + positive;
		hsp = hsp + "\nQuerySeq: " + qseq;
		hsp = hsp + "\nMidline_: " + midline;
		hsp = hsp + "\nHitSeq__: " + hseq;
		return hsp;
	}
}
