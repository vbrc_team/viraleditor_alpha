package Annotator.structures;

import java.util.List;
import java.util.LinkedList;
import java.util.ListIterator;

/**
 * Stores the HIT data of a blast iteration
 */
public class BlastHit implements java.io.Serializable {

	public int num;
	public String id;
	public String definition;
	public String accession;
	public int len;
	public List<BlastHsp> hsps;
	public String gene;
	public String family;

	/** 
	 * Default constructor
	 * Initializes all class variables
	 */
	public BlastHit() {
		num = 0; len = 0;
		id = ""; definition = ""; accession = "";
		hsps = new LinkedList<BlastHsp>();
		gene = ""; family = "";
	}

	/**
	 * Add a HSP (alignment) to the hit
	 * @param hsp the HSP to be added
	 */
	public void addHsp(BlastHsp hsp) {
		hsps.add(hsp);
	}

	/**
	 * Get the number of HSP for this hit
	 * @return the number of HSP for this hit
	 */
	public int numHsps() {
		return hsps.size();
	}

	/**
	 * Convert object to string
	 * @return string representation of object
	 */
	public String toString() {
		StringBuffer objStr = new StringBuffer();
		String str = "";

		str = "HIT: #" + num + " id=" + id + " definition=" + definition +
		" (gene=" + gene + ";family=" + family + ")" +
		" accession=" + accession + " length=" + len + " #hsp=" + hsps.size();
		objStr.append(str);
		ListIterator<BlastHsp> itr = hsps.listIterator(0);
		while (itr.hasNext())
			objStr.append("\n" + ((BlastHsp)itr.next()).toString()); 
		return objStr.toString();
	}
}
