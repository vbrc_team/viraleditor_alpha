package Annotator.structures;

import java.util.List;
import java.util.ArrayList;

/**
 * Contents of the genbank file feature CDS, used to write to a genbank
 */

public class GBKCDSData {
    public String name;
    public List<Integer> start;
    public List<Integer> stop;
    public boolean complement;
    public String protein_sequence;
    public String nucl_sequence;
    public int id;
    public int gi;
    public String type;
    public String accession;
    public String family;        //or 'product'
    public String function;       //function tag
    public String note;
    public String genomeName;

    /**
     * Default Constructor
     */
    public GBKCDSData() {
        name = ""; start = new ArrayList<>(); stop = new ArrayList<>(); complement = false;
        protein_sequence = ""; id = 0; gi = 0; accession = ""; family = ""; function = "";
        type = "protein"; note = ""; genomeName = "";
    }

    /**
     * Add an orf (exon) to the CDS
     * @param strt the start position oif the orf
     * @param stp the stop position of the orf
     */
    public void addOrf(int strt, int stp) {
        start.add(strt);
        stop.add(stp);
    }

    /**
     * Convert object to string
     * @return string representation of object
     */
    public String toString() {
        StringBuilder line;

        if (start.size() > 1) {
            if (complement) {
                line = new StringBuilder("join(complement(");
                line.append(start.get(0)).append("..").append(stop.get(0));

                for (int i=1; i<start.size(); i++) {
                    line.append(",").append(start.get(i))
                            .append("..").append(stop.get(i));
                }
                line = new StringBuilder("))");

            } else {
                line = new StringBuilder("join(");
                line.append(start.get(0)).append("..").append(stop.get(0));

                for (int i=1; i<start.size(); i++) {
                    line.append(",").append(start.get(i))
                            .append("..").append(stop.get(i));
                }
                line = new StringBuilder(")");
            }
        } else {
            if (start.size() > 0) {
                if (complement) {
                    line = new StringBuilder("complement(" + start.get(0) + ".." +
                            stop.get(0) + ")");
                } else {
                    line = new StringBuilder("" + start.get(0) + ".." +
                            stop.get(0));
                }
            } else {
                line = new StringBuilder("0..0");
            }
        }
        line = new StringBuilder(name + " " + line + " " + family);

        return line.toString();
    }

    /**
     *  get the id assigned to the object
     * @return  id of the GBKCDSData object
     */
    public int getId() {
        return id;
    }
}
