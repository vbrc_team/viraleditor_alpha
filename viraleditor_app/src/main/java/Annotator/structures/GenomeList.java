package Annotator.structures;


/**
 * a list of all genome in the vocs db
 */

public class GenomeList {
    public int vid;
    public String name;
    public String prefix;
    public int size;
    public int numGenes;
    public String genus;

    /**
     * Default constructor
     */
    public GenomeList() {
        vid = 0; name = ""; prefix = ""; size = 0; genus = ""; numGenes = 0;
    }
}
