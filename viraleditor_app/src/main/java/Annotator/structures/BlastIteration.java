package Annotator.structures;

import java.util.List;
import java.util.LinkedList;
import java.util.ListIterator;

/**
 * Stores the iteration data of a blast output
 */
public class BlastIteration implements java.io.Serializable {

	public int num;
	public List<BlastHit> hits;
	public double kappa;
	public double lambda;
	public double entropy;

	/**
	 * Default constructor 
	 * Initializes all class variables.
	 */
	public BlastIteration() {
		num = 0; kappa = 0; lambda = 0; entropy = 0;
		hits = new LinkedList<BlastHit>();
	}

	/**
	 * Add an blast HIT to this iteration
	 * @param hit the HIT to be added
	 */
	public void addHit(BlastHit hit) {
		hits.add(hit);
	}

	/**
	 * Get the number of hits for this iteration
	 * @return the number of hits for this iteration
	 */
	public int numHits() {
		return hits.size();
	}

	/**
	 * Convert object to string
	 * @return string representation of object
	 */
	public String toString() {
		StringBuffer objStr = new StringBuffer();
		String str = "";

		str = "Iteration: #" + num + " kappa=" + kappa + " lambda=" + lambda +
		" entropy=" + entropy + " #hits=" + hits.size();
		objStr.append(str);
		ListIterator<BlastHit> itr = hits.listIterator(0);
		while (itr.hasNext())
			objStr.append("\n" + ((BlastHit)itr.next()).toString());
		return objStr.toString();
	}
}
