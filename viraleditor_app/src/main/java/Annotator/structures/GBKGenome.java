package Annotator.structures;

/**
 * Stores the genome data
 */

public class GBKGenome {
    public int id;
    public String name;
    public String genome_abbr;
    public int gi;
    public String gb_accession;
    public String genome_seq;
    public String gbkname;
    public String order;
    public String strain_name;

    /**
     * Default constructor
     */
    public GBKGenome() {
        id = 0; name = ""; genome_abbr = ""; gi = 0; gb_accession = "";
        order = ""; strain_name = ""; gbkname = "";
        genome_seq = "";
    }
}
