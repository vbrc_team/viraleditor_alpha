package Annotator.structures;
import java.util.List;

/**
 * Represents a blast hit
 */
public class BlastHitData implements Comparable <BlastHitData> {
    public Integer vid;         //Virus id
    public String vabbrev;      //Virus abbreviation
    public String vname;        //Virus Name
    public String vacc;         //Virus Accession Number
    public Integer vgi;         //Virus GI number
    public Integer gid;         //Gene id??
    public String description;  //'product' tag
    public String note_desc;         //'note' tag
    public int exon;
    public int totalnoexons;    //
    public String gname;        //Gene name
    public int fid;
    public String fname;        //'product' tag
    public String function;     //'function' tag
    public Integer start;      //starting base number
    public Integer stop;       //ending base number
    public String evalue;
    public int exp;
    public double base;
    public Double score;
    public Double identity;
    public Double similarity;
    public StringBuffer text;
    public StringBuffer needle;
    public int queryIndex;
    public int length;
    public int frame;
    public String frag;      //always 'protein'?
    public String seq;
    public String pseq;
    public int psize;
    public boolean first;

    /**
     * Default Constructor
     */
    public BlastHitData() {
        vid = 0; vname = ""; vacc = ""; fname = ""; function = ""; fid = 0;
        gid = 0; vgi = 0; frag = "protein"; frame = 0;
        vabbrev = ""; gname = ""; start = 0; stop = 0;
        evalue = ""; exp = 0; base = 0; score = null; length = 0; first = true;
        identity = null; text = new StringBuffer(); needle = new StringBuffer();
        queryIndex = -1; pseq = ""; seq = ""; psize = 0; exon = 0; totalnoexons = 0;
        similarity = null; description = ""; note_desc = "";
    }

    /**
     * Set needle text
     * @param needleResult the needle alignment
     */
    public void setNeedle(List<String> needleResult) {
        String str, tmp;
        double myNumber;
        int pos, pos1;

        if (needleResult == null){
            System.out.println(" no needle result!");
            return;
        }

        needle = new StringBuffer();
        for (String o : needleResult) {
            needle.append(o);
            needle.append("\n");
        }
        str = needle.toString();
        pos = str.indexOf("Identity:");
        if (pos != -1) {
            pos = str.indexOf('(', pos) + 1;
            pos1 = str.indexOf('%', pos);
            tmp = str.substring(pos, pos1);
            try {
                myNumber = Double.parseDouble(tmp);
                identity = myNumber;
            } catch (Exception e) {
                System.out.println("Parse error thrown while setting needle text");
            }
        }
        pos = str.indexOf("Similarity:");
        if (pos != -1) {
            pos = str.indexOf('(', pos) + 1;
            pos1 = str.indexOf('%', pos);
            tmp = str.substring(pos, pos1);
            try {
                myNumber = Double.parseDouble(tmp);
                similarity = myNumber;
            } catch (Exception e) {
                System.out.println("Parse error thrown while setting needle text");
            }
        }
    }

    /**
     * Compare two objects
     * @param b the object to compare two
     * @return a negative integer, zero, or a positive integer
     *         as this object is less than, equal to, or greater than
     *         the specified object
     */
    public int compareTo(BlastHitData b) {
        return Integer.compare(gid, b.gid);
    }

    /**
     * Convert BlastHitData object to String object
     * @return String object representing BlastHitData
     */
    public String toString() {
        String str = "Virus: " + vid + " " + vabbrev + " " + vname +
                " " + vacc + " " + vgi + "\n";
        str += "Gene: " + gname + "\n";
        str += "frame=" + frame + " start=" + start  + "  stop=" + stop + "\n";
        str += "eval=" + evalue  + "(" + base + "^" + exp + ") \n";
        str += "score=" + score  + "  identity=" + identity + "\n";
        if (seq != null) {
            if (seq.length() > 40) {
                str += "nseq=" + seq.substring(0,12) + " ... " +
                        seq.substring(seq.length()-12) + "\n";
            } else {
                str += "nseq=" + seq + "\n";
            }
        }
        if (pseq != null) {
            if (pseq.length() > 40) {
                str += "pseq=" + pseq.substring(0,12) + " ... " +
                        pseq.substring(pseq.length()-12) + "\n";
            } else {
                str += "pseq=" + pseq + "\n";
            }
        }
        return str;
    }
}

