package Annotator.structures;
import java.awt.Rectangle;

public class Annotation implements Comparable <Annotation> {
    String name;
    int start;
    int stop;
    String strand;
    String type;
    boolean isORF;
    Rectangle mapPosition;

    /**
     * Default constructor
     */
    public Annotation() {
        start = 0; stop = 0; strand = "+"; type = "GENE"; name = ""; isORF = false;
        mapPosition = new Rectangle();
    }

    /**
     * Constructor
     * @param nm the name of the annotation
     * @param start the start position of the annotation
     * @param stp the stop position of the annotation
     * @param s the strand of the annotation
     * @param t the type of the annotation
     */
    public Annotation(String nm, int start, int stp, String s, String t) {
        this.name = nm;
        this.type = t;
        this.start = start;
        this.stop = stp;
        this.strand  = s;
        this.isORF = false;
        this.mapPosition = new Rectangle();
    }

    /**
     * Constructor
     * @param nm the name of the annotation
     * @param start the start position of the annotation
     * @param stp the stop position of the annotation
     * @param s the strand of the annotation
     * @param t the type of the annotation
     * @param o true if unassigned-ORF, false if annotation
     */
    public Annotation(String nm, int start, int stp, String s,
                      String t, boolean o) {
        this.name = nm;
        this.type = t;
        this.start = start;
        this.stop = stp;
        this.strand  = s;
        this.isORF = o;
        this.mapPosition = new Rectangle();
    }

    /**
     * Convert object to string
     * @return string representation of object
     */
    public String toString() {
        String str;
        if (isORF) {
            str = "ORF-";
        } else {
            str = "Annotation-";
        }
        str = str + name + ": " + start + "-" + stop;
        str = str + "  " + strand + "  " + type;
        return str;
    }

    /**
     * Define the coordinates of the gene in the genome map
     * @param pos the coordinates ff the gene in the genome map
     */
    public void setMapLocation(Rectangle pos) {
        mapPosition = pos;
    }

    /**
     * Get the coordinates of the gene in the genome map
     * @return the coordinates ff the gene in the genome map
     */
    public Rectangle getMapLocation() {
        return mapPosition;
    }

    /**
     * Comparable - equals
     * @param k object to compare to
     * @return true if equal, false otherwise
     */
    public boolean equals(Annotation k) {
        boolean e = false;
        if (k.start == start) {
            if (k.stop == stop) {
                e = true;
            }
        }
        return e;
    }

    /**
     * Compares this object with the specified object for order
     * @param o the object to compare to
     * @return -1 (less than), 0 (equal) , or 1 (greater than)
     */
    public int compareTo(Annotation o) {
        int c ;
        if (start == o.start) {
            c = Integer.compare(stop, o.stop);
        } else {
            if (start > o.start) {
                c = 1;
            } else {
                c = -1;
            }
        }
        return c;
    }
}


