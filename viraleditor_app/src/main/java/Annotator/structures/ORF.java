package Annotator.structures;

import Annotator.utility.GATUSequenceUtility;
import Shared.Gene;

import java.util.List;
import java.util.ArrayList;

//contains diffrent information then the orf in shared

public class ORF implements Comparable<ORF> {

        public int id;
        public String name;
        public String product;
        public int start;
        public int stop;
        public int strand;
        public String pseq;
        public String nseq;
        public boolean ignore;        //if false then an unassigned orf
        public boolean accept;        //if true then accepted unassigned orf
        public double score;
        public double identity;
        public String type;
        public List<String> blastOutput;
        public boolean blastOutputInHTML;
        public String genomeName;

        /**
         * Default constructor
         */
        public ORF() {
            id = 0; start = 0; stop = 0; strand = 1; pseq = ""; nseq = "";
            type = "protein"; name = ""; ignore = false; accept = false; blastOutput = new ArrayList<>();
            score = 0.0; identity = 0.0;
            blastOutputInHTML = false; genomeName = "";
        }

        /**
         * Constructor
         * @param num the id of the orf
         * @param strt the start position of the orf
         * @param stp the stop position of the orf
         * @param s the strand of the orf
         * @param seq the genome sequence
         * @param c defines if genome is circular
         */
        public ORF(int num, int strt, int stp, int s, String seq, boolean c) {
            id = num;
            name = "";
            product = "";
            type = "protein";
            start = strt;
            stop = stp;
            strand  = s;
            ignore = false;
            accept = false;
            genomeName = "";
            Gene gene = new Gene();
            gene.isCircular(c);
            if (strand < 0) {
                gene.strand = "-";
                gene.orf_start = stop;
                gene.orf_stop = start;
            } else {
                gene.orf_start = start;
                gene.orf_stop = stop;
                gene.strand = "+";
            }
            String ntseq =
                    GATUSequenceUtility.setDNASeq(seq, gene.orf_start, gene.orf_stop,
                            gene.strand);
            gene.setDNA(ntseq);
            gene.setSize();
            gene.setProteinSeq();
            pseq = gene.protein_seq;
            nseq = gene.dnaSeq;
        }

        /**
         * Define the blast output (result of a blastp run)
         * @param bout the blast output
         * @param boutform the blast output format (true=html;false=text)
         */
        public void setBlastOutput(List<String> bout, boolean boutform) {
            blastOutput = bout;
            blastOutputInHTML = boutform;
            setNameScoreAndIdentity();
        }

        /**
         * Set the name, score and the identity of the orf
         * to the core/identity of the orfs  best hit
         */
        private void setNameScoreAndIdentity() {
            // TODO: pretty sure this can be removed since its part of egatu, replaced by interchange
        }

        /**
         * Convert object to string
         * @return string representation of object
         */
        public String toString() {
            String str = "ORF-" + id + ": " + start + "-" + stop;
            if (strand > 0) {
                str = str + " + ";
            } else {
                str = str + " - ";
            }
            return str + pseq + " " + nseq;
        }

        /**
         * Comparable - equals
         * @param k object to compare to
         * @return true if equal, false otherwise
         */
        public boolean equals(ORF k) {
            boolean e = false;
            if (k.start == start) {
                if (k.stop == stop) {
                    e = true;
                }
            }
            return e;
        }

        /**
         * Compares this object with the specified object for order
         * @param o the object to compare to
         * @return -1 (less than), 0 (equal) , or 1 (greater than)
         */
        public int compareTo(ORF o) {
            int c;
            if (start == o.start) {
                c = Integer.compare(stop, o.stop);
            } else {
                if (start > o.start) {
                    c = 1;
                } else {
                    c = -1;
                }
            }
            return c;
        }
    }
