package Annotator.structures;
import java.awt.Rectangle;

/**
 * store genes from reference genome
 */
public class CDS implements Comparable<CDS> {
    public int gid;
    public String gname;
    public int fid;
    public String fname;
    public String function;
    public int vid;
    public String vname;
    public String frag;
    public String prot_sequence;
    public String nucl_sequence;
    public int [] start;
    public int [] stop;
    public String [] exon_nseq;
    public int strand;
    public String description;
    public Rectangle mapPosition;
    public String note_desc;

    /**
     * Default constructor
     */
    public CDS() {
        gid = 0; gname = ""; fid = 0; fname = ""; function = ""; vid=0; vname=""; frag="protein";
        nucl_sequence = ""; prot_sequence = "";
        description = "";
        start = new int[1];  stop = new int[1];
        exon_nseq = new String[1]; exon_nseq[0] = ""; strand = 0;
        mapPosition = null; note_desc = "";
    }

    /**
     * Convert object to string
     * @return string representation of object
     */
    public String toString() {
        StringBuilder str = new StringBuilder("CDS-" + gid + ": ");

        for (int k=0; k<start.length; k++) {
            str.append("(").append(start[k]);
            str.append("-").append(stop[k]);
            str.append(") ").append(exon_nseq[k]);
            str.append("  ");
        }
        if (strand > 0) str.append(" + ");
        else str.append(" - ");
        str.append(prot_sequence).append(" ").append(nucl_sequence);
        return str.toString();
    }

    /**
     * Define the coordinates of the gene in the genome map
     * @param pos the coordinates ff the gene in the genome map
     */
    public void setMapLocation(Rectangle pos) {
        mapPosition = pos;
    }

    /**
     * Get the coordinates of the gene in the genome map
     * @return the coordinates ff the gene in the genome map
     */
    public Rectangle getMapLocation() {
        if (mapPosition == null) {
            return new Rectangle();
        } else {
            return mapPosition;
        }
    }

    /**
     * Comparable - equals
     * @param k object to compare to
     * @return true if equal, false otherwise
     */
    public boolean equals(CDS k){
        boolean e = true;
        if (start.length == k.start.length) {
            for (int i=0; i<start.length; i++) {
                if (start[i] != k.start[i]) {
                    e = false;
                }
                if (stop[i] != k.stop[i]) {
                    e = false;
                }
            }
        } else {
            e = false;
        }
        return e;
    }

    /**
     * Compares this object with the specified object for order
     * @param cds the object to compare to
     * @return -1 (less than), 0 (equal) , or 1 (greater than)
     */
    public int compareTo(CDS cds) {
        int c;
        if (start[0] == cds.start[0]) {
            if (stop[stop.length-1] == cds.stop[cds.stop.length-1]) {
                c = Integer.compare(stop.length, cds.stop.length);
            } else {
                if (stop[stop.length-1] > cds.stop[cds.stop.length-1]) {
                    c = 1;
                } else {
                    c = -1;
                }
            }
        } else {
            if (start[0] > cds.start[0]) {
                c = 1;
            } else {
                c = -1;
            }
        }
        return c;
    }
}
