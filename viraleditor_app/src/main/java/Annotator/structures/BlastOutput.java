package Annotator.structures;

import java.util.List;
import java.util.LinkedList;
import java.util.ListIterator;

/**
 * Stores the content of a blast output 
 */
public class BlastOutput implements java.io.Serializable {

	public String program;
	public String database;
	public String queryId;
	public String queryDef;
	public int queryLen;
	public String matrix;
	public double expect;
	public int gapOpen;
	public int gapExtend;
	public String filter;
	public List<BlastIteration> iterations;
	public String gene;
	public String family;

	/**
	 * Default constructor
	 * Initializes all class variables.
	 */
	public BlastOutput() {
		program = ""; database = ""; queryId = ""; queryDef = ""; matrix = ""; 
		filter = ""; queryLen = 0; expect = 0; gapOpen = 0; gapExtend = 0;
		iterations = new LinkedList<BlastIteration>();
		gene = ""; family = "";
	}

	/**
	 * Add a blast iteration to this blast output
	 * @param iteration the iteration to be added
	 */
	public void addIteration(BlastIteration iteration) {
		iterations.add(iteration);
	}

	/**
	 * Get the number of iterations for this blast output
	 * @return the number of iterations for this blast output
	 */
	public int numIterations() {
		return iterations.size();
	}

	/**
	 * Convert object to string
	 * @return string representation of object
	 */
	public String toString() {
		StringBuffer objStr = new StringBuffer();
		String str = "";

		str = "Blast output: program=" + program + " database=" + database + 
		" query ID=" + queryId + " query definition=" + queryDef +
		" (gene=" + gene + ";family=" + family + ")" + 
		" query length=" + queryLen + " matrix=" + matrix +
		" expect=" + expect + " gap open=" + gapOpen +
		" gap extend=" + gapExtend + " filter=" + filter +
		" #iterations=" + iterations.size();
		objStr.append(str);
		ListIterator<BlastIteration> itr = iterations.listIterator(0);
		while (itr.hasNext())
			objStr.append("\n" + ((BlastIteration)itr.next()).toString());
		return objStr.toString();
	}
}
