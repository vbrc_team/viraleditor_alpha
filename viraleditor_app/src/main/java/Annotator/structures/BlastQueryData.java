package Annotator.structures;
/**
 * Stores the balst query data
 */

public class BlastQueryData {
    public Integer gid;
    public String gname;
    public Integer fid;
    public String fname;
    public String vname;
    public String function;
    public String description;
    public String note_desc;
    public StringBuffer text;
    public int length;
    public String frag;
    public int start;
    public int stop;
    public int exon;
    public int totalnoexons;
    public String pseq;
    public String seq;
    public int strand;

    /**
     * Default constructor
     */
    public BlastQueryData() {
        gid = 0; fid = 0; exon = 0;
        vname = ""; gname = ""; fname = ""; function = ""; length = 0; frag = "protein";
        text = new StringBuffer(); start = 0; stop = 0; strand = 0;
        totalnoexons = 0; seq = ""; pseq = ""; description = ""; note_desc = "";
    }

    /**
     * Convert object to string
     * @return string representation of object
     */
    public String toString() {
        String str = "Virus: " + vname + " ";
        str += "Gene: " + gid + " " + gname + "\n";
        str += "Family: " + fid + " " + fname + "\n";
        str += "Function: " + function + "\n";
        str += "Protein Sequence: " + pseq + "\n";
        str += "DNA Sequence: " + seq + "\n";
        return str;
    }
}

