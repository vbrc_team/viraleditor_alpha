package Annotator.utility;

import Annotator.structures.GBKCDSData;
import Annotator.structures.GBKGenome;

import javax.swing.*;
import java.io.*;
import java.text.NumberFormat;
import java.util.*;

import Annotator.structures.ORF;
import org.apache.commons.csv.CSVFormat;
import org.apache.commons.csv.CSVPrinter;

/**
 *Used for writing to diffrent file formats
 *
 * @author Francesco Marass, 23 May 2008
 *
 */
public class FileOperations {

    private final GenomeAnnotator gatu;
    private final String filename;

    /**
     * Constructor for GATU
     * Needed to set variables for reading file.
     *
     * @param parent reference to GATU
     */
    public FileOperations(GenomeAnnotator parent) {
        gatu = parent;
        filename = "";
    }


    /**
     * reads the target genome from interchange
     * TODO: update this to read interchage, see how old code does it
     */
    public int readGenome() {
        return -1;
    }

    /**
     * reads the reference genome from interchange
     * TODO: update this to read interchange, see how old code does it
     */
    public boolean readReference() {
        return false;
    }



    /**
     * @param gbkf -
     * @param genome -
     * @param refgenome -
     * @return -
     */
    public boolean writeGBK(String gbkf, GBKGenome genome, GBKGenome refgenome) {
        return writeGBK(gbkf, genome, refgenome, true, "","");
    }

    /**
     * @param gbkf -
     * @param genome -
     * @param refgenome -
     * @param gbkProductLine -
     * @return -
     */
    public boolean writeGBK(String gbkf, GBKGenome genome, GBKGenome refgenome, boolean gbkProductLine, String inputName,String strainName) {
        GBKCDSData gbkg;
        ORF [] aorfs;
        String today = (new java.util.Date()).toString();
        StringBuilder line;
        int nbp, cds_start;
        String version, acc;
        List<Integer> starts, stops;
        Hashtable<String, GBKCDSData> gbkcdslist;
        Hashtable<String, GBKCDSData> gbkGenes;
        List<Map.Entry<String, GBKCDSData>> sorted_gbkcdslist;

        NumberFormat nfm;
        String key;

        if (genome == null) {
            return false;
        }
        nbp = genome.genome_seq.length();
        line = new StringBuilder("" + nbp);
        nfm = NumberFormat.getNumberInstance();
        nfm.setMaximumIntegerDigits(line.length());
        nfm.setMaximumFractionDigits(0);

        try {
            File out = new File(gbkf);
            FileOutputStream fos = new FileOutputStream(out);
            PrintWriter file = new PrintWriter(fos);
            version = null;
            if ((genome.gb_accession == null) || (genome.gb_accession.length() < 1)) {
                acc = genome.genome_abbr;
            } else {
                version = genome.gb_accession;
                if (genome.gb_accession.indexOf('.') == -1)
                    acc = genome.gb_accession;
                else
                    acc =
                            genome.gb_accession.substring(0,genome.gb_accession.indexOf('.'));
            }
            if (genome.gi > 0 && version != null) {
                version = version + "  GI:" + genome.gi;
            }
            line = new StringBuilder("  " + genome.genome_seq.length() + " bp    DNA    VRL ");
            line.append(today, 8, 10).append("-").append(today, 4, 7).append("-").append(today, 24, 28);
            if ((line.length()+12+acc.length()) > 79) {
                line.insert(0, "LOCUS       " +
                        acc.substring(0, 79 - (line.length() + 12)));
            } else {
                line.insert(0, "LOCUS       " + acc);
            }
            file.println(line);
            line = new StringBuilder("DEFINITION  " + genome.name + ".");
            file.println(line);

            //no accession number from gatu so set version and accession to 0 in gbk file
            if(acc.equals(genome.genome_abbr)) {
                acc="0";
                version="0";
            }
            file.println("ACCESSION   " + acc);
            file.println("VERSION     " + version);
            file.println("KEYWORDS    .");
            file.println("SOURCE      " + genome.name + ".");
            file.println("  ORGANISM  " + genome.name);
            for (int ol=0; ol<genome.order.length(); ol=ol+67) {
                if ((ol+67) < genome.order.length()) {
                    line = new StringBuilder(genome.order.substring(ol, ol + 67));
                } else {
                    line = new StringBuilder(genome.order.substring(ol));
                }
                line = new StringBuilder(line.toString().trim());
                file.println("            " + line);
            }
            line = new StringBuilder("FEATURES             Location/Qualifiers");
            file.println(line);
            line = new StringBuilder("     source          1.." + genome.genome_seq.length());
            file.println(line);
            line = new StringBuilder("                     /organism=" + '"' + genome.gbkname + '"');
            file.println(line);
            if (genome.strain_name.equalsIgnoreCase("null")) {
                genome.strain_name = "";
            }
            line = new StringBuilder("                     /strain=" + '"' + genome.strain_name + '"');
            file.println(line);
            line = new StringBuilder("Annotations transferred from " + refgenome.gbkname +
                    " (" + refgenome.gb_accession + ")");
            if (line.length() > 51)
                file.println("                     /note=" + '"' +
                        line.substring(0,51));
            else
                file.println("                     /note=" +
                        '"' + line + '"');
            for (int nl=51; nl<line.length(); nl=nl+58) {
                if (nl+58 < line.length())
                    file.println("                     " +
                            line.substring(nl,nl+58));
                else
                    file.println("                     " +
                            line.substring(nl) + '"');
            }

            // write genes
            gbkGenes = gatu.getCurrentGenes(); // initializes and sets gbkGenes


            //aorfs = gatu.orfPanel.getAcceptedOrfs();
            //TODO: important this needs to be replaces by the java fx table or composer
            System.out.println("this code will not work, see line 560 of File operations");
            aorfs = new ORF[1];
            aorfs[0] = new ORF();


            gbkcdslist = new Hashtable<>();
            Enumeration <GBKCDSData> egbk = gbkGenes.elements();
            while (egbk.hasMoreElements()) {
                gbkg = egbk.nextElement();
                cds_start = gbkg.start.get(0);
                gbkcdslist.put(nfm.format(cds_start) + "." + gbkg.name, gbkg);
            }
            for (ORF aorf : aorfs) {
                starts = new ArrayList<>();
                starts.add(aorf.start);
                stops = new ArrayList<>();
                stops.add(aorf.stop);
                cds_start = aorf.start;
                gbkg = new GBKCDSData();
                if (!aorf.name.startsWith("Unassigned")) {
                    gbkg.name = aorf.name;
                } else {
                    gbkg.name = inputName + aorf.id;
                }
                gbkg.family = aorf.product;
                gbkg.genomeName = aorf.genomeName;
                for (int ns = 0; ns < starts.size(); ns++) {
                    gbkg.addOrf(starts.get(ns), stops.get(ns));
                }
                gbkg.complement = aorf.strand <= 0;
                gbkg.protein_sequence = aorf.pseq;
                gbkg.nucl_sequence = aorf.nseq;
                gbkg.id = aorf.id;
                gbkg.type = aorf.type;
                gbkcdslist.put(nfm.format(cds_start) + "." + gbkg.name, gbkg);
            }

            // sort by start position (lowest to highest)
            sorted_gbkcdslist = new ArrayList<>(gbkcdslist.entrySet());
            sorted_gbkcdslist.sort(Comparator.comparingInt(o -> Collections.min(o.getValue().start)));

            for (int ncds=0; ncds<sorted_gbkcdslist.size(); ncds++) {
                key = sorted_gbkcdslist.get(ncds).getKey();
                gbkg = gbkcdslist.get(key);

                String name;
                if (inputName.equals("")) {
                    // uses the name as it appears in GATU GUI (first column)
                    name = gbkg.name;
                }
                else {
                    /* using ncds index as ID for gene name, as per Cindy's request
                     * so that genes are listed in ascending and sequential numerical order
                     * (so numerical values assigned based on order of genes as they appear in gene table */
                    name = getGeneName(inputName, strainName, ncds);
                }

                GenomeAnnotatorUtility.writeGeneToGB(
                        file, name, gbkg.start, gbkg.stop, gbkg.complement,
                        gbkg.protein_sequence, gbkg.gi, gbkg.accession, gbkg.id,
                        gbkg.family, gbkg.function, gbkg.type, gbkProductLine, gbkg.note);
            }
            // write genome sequence
            line = new StringBuilder("ORIGIN");
            file.println(line);
            for (int seql=0; seql<genome.genome_seq.length();) {
                line = new StringBuilder("" + (seql + 1));
                for (int sc=0; sc<8; sc++) if (line.length() < 9) line.insert(0, " ");
                for (int cnt=0; cnt<6; cnt++) {

                    if ((seql+10) < genome.genome_seq.length()) {
                        line.append(" ").append(genome.genome_seq, seql, seql + 10);
                        seql = seql + 10;
                    } else {
                        line.append(" ").append(genome.genome_seq.substring(seql));
                        seql = genome.genome_seq.length();
                        cnt = 6;
                    }
                }
                file.println(line);
            }
            file.println("//");
            file.flush();
            file.close();
        }
        catch (Exception e) {
            e.printStackTrace();
            JOptionPane.showMessageDialog(null, "Write GenBank file error: " + e.getMessage() + "\n" + gbkf, "Genome editor", JOptionPane.ERROR_MESSAGE);
            return false;
        }
        return true;
    }

    public static String getGeneName(String geneName,String strainName,int id){
        int number = id + 1;
        String num;

        if (number < 10) {
            num = "00" + number;
        } else if (number >= 10 && number < 100)
            num = "0" + number;
        else {
            num = number + "";
        }

        if (strainName.equals("")) {
            return geneName+"-"+num;
        } else {
            return geneName+"-"+strainName+"-"+num;
        }
    }

    public boolean writeSequin(File fileToSave, String filename, Hashtable<String, GBKCDSData> gbkGenes,String geneAb,String strainAb) {
        PrintWriter file = null;
        GBKCDSData gbkg;

        try {
            File sequinOut = new File(filename);
            FileOutputStream fos = new FileOutputStream(sequinOut);
            file = new PrintWriter(fos);
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }


        try {
            assert file != null;
            file.println(">Feature " + fileToSave.getName());
            Enumeration<GBKCDSData> egbk = gbkGenes.elements();

            List<GBKCDSData> s = new ArrayList<>();
            while(egbk.hasMoreElements()) {
                s.add(egbk.nextElement());
            }

            s.sort(new geneCompare());

            int i = 0;
            while (i < s.size()) {

                gbkg = s.get(i);
                String name;
                if (geneAb.equals("")) {
                    name = gbkg.name;
                } else {
                    name = getGeneName(geneAb,strainAb,gbkg.id);
                }
                if(gbkg.start.size() > 1) {
                    if(gbkg.complement) {
                        file.println(gbkg.stop.get(0)+ "\t" + gbkg.start.get(0)+ "\t" + "gene");
                        file.println("\t\t\t" + "gene\t" + name);
                        file.println(gbkg.stop.get(0)+ "\t" + gbkg.start.get(0)+ "\t" + "CDS");
                        for (int j=1; j<gbkg.start.size(); j++) {
                            file.println(gbkg.stop.get(j)+ "\t" + gbkg.start.get(j));
                        }
                    }
                    else {
                        file.println(gbkg.start.get(0)+ "\t" + gbkg.stop.get(0)+ "\t" + "gene");
                        file.println("\t\t\t" + "gene\t" + name);
                        file.println(gbkg.start.get(0)+ "\t" + gbkg.stop.get(0)+ "\t" + "CDS");
                        for (int j=1; j<gbkg.start.size(); j++) {
                            file.println(gbkg.start.get(j)+ "\t" + gbkg.stop.get(j));
                        }
                    }
                }
                else {
                    if(gbkg.start.size() > 0) {
                        if(gbkg.complement) {
                            file.println(gbkg.stop.get(0)+ "\t" + gbkg.start.get(0)+ "\t" + "gene");
                            file.println("\t\t\t" + "gene\t" + name);
                            file.println(gbkg.stop.get(0)+ "\t" + gbkg.start.get(0)+ "\t" + "CDS");
                        }
                        else {
                            file.println(gbkg.start.get(0)+ "\t" + gbkg.stop.get(0)+ "\t" + "gene");
                            file.println("\t\t\t" + "gene\t" + name);
                            file.println(gbkg.start.get(0)+ "\t" + gbkg.stop.get(0)+ "\t" + "CDS");
                        }
                    }
                    else {
                        file.println("0"+"\t" + "0"+"\t" + "gene");
                        file.println("\t\t\t" + "gene\t" + name);
                        file.println("0"+"\t" + "0"+"\t" + "CDS");
                    }
                }
                file.println("\t\t\t" + "gene\t" + name);
                file.println("\t\t\t" + "codon_start\t" + "1");
                file.println("\t\t\t" + "product\t" + gbkg.family);
                file.println("\t\t\t" + "note\t" + gbkg.note);
                i++;
            }
            file.flush();
            file.close();
            return true;
        } catch (Exception e) {
            e.printStackTrace();
            return false;
        }
    }

    public static boolean writeCSV(JProgressBar progressBar, String filename, List<List<Object>> blastData, List<ORF> geneData, String inputName, String strainName) {
        CSVFileWriter csvFileWriter = new CSVFileWriter();
        csvFileWriter.writeCSVFile(progressBar, filename, blastData, geneData,inputName,strainName);
        return csvFileWriter.completedSuccessfully();
    }

    public static boolean writeCSVAll(JProgressBar progressBar, String filename, List<List<Object>> blastData, List<ORF> geneData) {
        CSVFileWriterAll csvFileWriterAll = new CSVFileWriterAll();
        csvFileWriterAll.writeCSVFileAll(progressBar, filename, blastData, geneData);
        return csvFileWriterAll.completedSuccessfully();
    }

    public static class CSVFileWriter {
        static final String NEW_LINE_SEPARATOR = "\n";
        final Object[] ANNOTATIONS_TABLE_HEADER = {"Gene Name", "Product", "Function", "Exon", "Start", "Stop", "+/-", "Size", "P.Size", "Genetype", "Score", "% Identity", "Note"};
        final Object[] UNASSIGNED_ORFS_HEADER = {"ORF Name", "Product", "Genome","", "Start", "Stop", "+/-", "Size", "Genetype", "Score", "% Identity"};
        boolean success = false;

        public void writeCSVFile(JProgressBar progressBar, String filename, List<List<Object>> blastData, List<ORF> geneData,String inputName,String strainName) {
            List<List<Object>> annotations = new ArrayList<>();
            List<ORF> unassignedORFs = new ArrayList<>();

            for (List<Object> b : blastData) {
                if (b.toString().contains("true"))
                    annotations.add(b);
            }
            for (ORF g : geneData) {
                if (g.accept){
                    unassignedORFs.add(g);
                }
            }
            int total = annotations.size() + unassignedORFs.size();
            int numRecords = 0;

            FileWriter fileWriter = null;
            CSVPrinter csvFilePrinter = null;

            // create CSVFormat object with "\n" as record delimiter
            CSVFormat csvFileFormat = CSVFormat.DEFAULT.withRecordSeparator(NEW_LINE_SEPARATOR);

            try {
                // initialize FileWriter object
                fileWriter = new FileWriter(filename);

                // initialize CSVPrinter object
                csvFilePrinter = new CSVPrinter(fileWriter, csvFileFormat);

                // create CSV file header for annotations
                csvFilePrinter.printRecord(ANNOTATIONS_TABLE_HEADER);

                // write new annotations object list to CSV file
                for (List<Object> anno : annotations) {
                    List<String> blastDataRecord = new ArrayList<>();

                    String name;
                    if (inputName.equals("")) {
                        name = anno.get(1).toString();
                    } else {
                        name = getGeneName(inputName,strainName,(int)anno.get(0));
                    }

                    blastDataRecord.add(name); // gene name plus id
                    //blastDataRecord.add(annoInfo.get(1).toString()); // gene name
                    blastDataRecord.add(anno.get(2).toString()); // product
                    blastDataRecord.add(anno.get(3).toString().replaceAll(",", ";")); // function
                    blastDataRecord.add(anno.get(4).toString()); // exon
                    blastDataRecord.add(anno.get(5).toString()); // start
                    blastDataRecord.add(anno.get(6).toString()); //stop
                    blastDataRecord.add(anno.get(7).toString().trim()); // +/-
                    blastDataRecord.add(anno.get(8).toString()); //size
                    blastDataRecord.add(anno.get(9).toString()); // p.size
                    blastDataRecord.add(anno.get(10).toString().trim()); // genetype
                    blastDataRecord.add(anno.get(11).toString()); // score
                    blastDataRecord.add(anno.get(12).toString()); // % identity
                    blastDataRecord.add(anno.get(14).toString().replaceAll(",", ";")); // note

                    csvFilePrinter.printRecord(blastDataRecord);
                    ++numRecords;
                    progressBar.setValue(numRecords/total * 100);
                }

                // create CSV file header for unassigned ORFs
                csvFilePrinter.printRecord("\n\n\n");
                csvFilePrinter.printRecord(UNASSIGNED_ORFS_HEADER);

                // write accepted unassigned ORFs object list to CSV file
                for (Object u : unassignedORFs) {
                    List<Object> orfDataRecord = new ArrayList<>();
                    ORF orf = (ORF) u;
                    orfDataRecord.add(orf.name);
                    orfDataRecord.add(orf.product.replaceAll(",", ";"));
                    orfDataRecord.add(orf.genomeName.replaceAll(",", ";"));
                    orfDataRecord.add("");
                    orfDataRecord.add(orf.start);
                    orfDataRecord.add(orf.stop);
                    if (orf.strand == 1) orfDataRecord.add("+");
                    else orfDataRecord.add("-");
                    orfDataRecord.add(orf.nseq.length());
                    orfDataRecord.add(orf.type);
                    orfDataRecord.add(orf.score);
                    orfDataRecord.add(orf.identity);

                    csvFilePrinter.printRecord(orfDataRecord);
                    ++numRecords;
                    progressBar.setValue(numRecords/total * 100);
                }

                success = true;
            }
            catch (Exception e) {
                e.printStackTrace();
            }
            finally {
                try {
                    if (fileWriter != null ) {
                        fileWriter.flush();
                        fileWriter.close();
                    }

                    if (csvFilePrinter != null) {
                        csvFilePrinter.close();
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }

        public boolean completedSuccessfully() {return success;}
    }

    public static class CSVFileWriterAll {
        static final String NEW_LINE_SEPARATOR = "\n";
        final Object[] ANNOTATIONS_TABLE_HEADER = {"Gene Name", "Product", "Function", "Exon", "Start", "Stop", "+/-", "Size", "P.Size", "Genetype", "Score", "% Identity", "Accepted", "Note"};
        final Object[] UNASSIGNED_ORFS_HEADER = {"ORF Name", "Product", "Genome","", "Start", "Stop", "+/-", "Size", "Genetype", "Score", "% Identity", "Accepted"};
        boolean success = false;

        public void writeCSVFileAll(JProgressBar progressBar, String filename, List<List<Object>> blastData, List<ORF>  geneData) {
            List<List<Object>> annotationsAll = new ArrayList<>(blastData);
            List<ORF> unassignedORFsAll = new ArrayList<>(geneData);

            int total = annotationsAll.size() + unassignedORFsAll.size();
            int numRecords = 0;

            FileWriter fileWriter = null;
            CSVPrinter csvFilePrinter = null;

            // create CSVFormat object with "\n" as record delimiter
            CSVFormat csvFileFormat = CSVFormat.DEFAULT.withRecordSeparator(NEW_LINE_SEPARATOR);

            try {
                // initialize FileWriter object
                fileWriter = new FileWriter(filename);

                // initialize CSVPrinter object
                csvFilePrinter = new CSVPrinter(fileWriter, csvFileFormat);

                // create CSV file header for annotations
                csvFilePrinter.printRecord(ANNOTATIONS_TABLE_HEADER);

                // write new annotations object list to CSV file
                for (List<Object> anno : annotationsAll) {
                    List<String> blastDataRecordAll = new ArrayList<>();
                    blastDataRecordAll.add(anno.get(1).toString()); // gene name
                    blastDataRecordAll.add(anno.get(2).toString()); // product
                    blastDataRecordAll.add(anno.get(3).toString().replaceAll(",", ";")); // function
                    blastDataRecordAll.add(anno.get(4).toString()); // exon
                    blastDataRecordAll.add(anno.get(5).toString()); // start
                    blastDataRecordAll.add(anno.get(6).toString()); //stop
                    blastDataRecordAll.add(anno.get(7).toString().trim()); // +/-
                    blastDataRecordAll.add(anno.get(8).toString()); //size
                    blastDataRecordAll.add(anno.get(9).toString()); // p.size
                    blastDataRecordAll.add(anno.get(10).toString().trim()); // genetype
                    blastDataRecordAll.add(anno.get(11).toString()); // score
                    blastDataRecordAll.add(anno.get(12).toString()); // % identity
                    blastDataRecordAll.add(anno.get(13).toString()); // checked
                    blastDataRecordAll.add(anno.get(14).toString().replaceAll(",", ";")); // note

                    csvFilePrinter.printRecord(blastDataRecordAll);
                    ++numRecords;
                    progressBar.setValue(numRecords/total * 100);
                }

                // create CSV file header for unassigned ORFs
                csvFilePrinter.printRecord("\n\n\n");
                csvFilePrinter.printRecord(UNASSIGNED_ORFS_HEADER);

                // write accepted unassigned ORFs object list to CSV file
                for (Object u : unassignedORFsAll) {
                    List<Object> orfDataRecordAll = new ArrayList<>();
                    ORF orf = (ORF) u;
                    orfDataRecordAll.add(orf.name);
                    orfDataRecordAll.add(orf.product.replaceAll(",", ";"));
                    orfDataRecordAll.add(orf.genomeName.replaceAll(",", ";"));
                    orfDataRecordAll.add("");
                    orfDataRecordAll.add(orf.start);
                    orfDataRecordAll.add(orf.stop);
                    if (orf.strand == 1) orfDataRecordAll.add("+");
                    else orfDataRecordAll.add("-");
                    orfDataRecordAll.add(orf.nseq.length());
                    orfDataRecordAll.add(orf.type);
                    orfDataRecordAll.add(orf.score);
                    orfDataRecordAll.add(orf.identity);
                    orfDataRecordAll.add(orf.accept);

                    csvFilePrinter.printRecord(orfDataRecordAll);
                    ++numRecords;
                    progressBar.setValue(numRecords/total * 100);
                }

                success = true;
            }
            catch (Exception e) {
                e.printStackTrace();
            }
            finally {
                try {
                    if (fileWriter != null) {
                        fileWriter.flush();
                        fileWriter.close();
                    }

                    if (csvFilePrinter != null) {
                        csvFilePrinter.close();
                    }
                }
                catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }

        public boolean completedSuccessfully() {return success;}
    }

    static class geneCompare implements Comparator<GBKCDSData> {
        @Override
        public int compare(GBKCDSData o1, GBKCDSData o2) {
            int one = o1.start.get(0);
            int two = o2.start.get(0);
            return one - two;
        }
    }

    public String getFilename() {
        return filename;
    }

}
