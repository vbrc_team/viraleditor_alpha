package Annotator.utility;

import javax.swing.*;
import java.io.*;
import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.util.*;

import Annotator.preferences.GatuPreferences;
import Annotator.structures.*;
//import ca.virology.lib.io.sequenceData.FeaturedSequence;
//import ca.virology.lib.io.writer.BSMLFeaturedSequenceWriter;
import Shared.preferences.DBPrefs;
import javafx.scene.control.Alert;
import javafx.scene.control.ButtonBar;
import javafx.scene.control.ButtonType;


public class GenomeAnnotator {
    private int [] statsTable;
    private List<BlastHitData> hits;
    private List<ORF> genomeOrfs;
    private int currentMaxGid = 0;

    private List<List<Object>> blastData;
    private Hashtable <String,String> hitProteinSequences;
    private Hashtable <String, GBKCDSData> gbkGenes;
    private BlastQueryData [] bq;
    public GatuPreferences gatuPrefs;
    public CDS[] cds;
    public ORF[] aorfs;
    public GBKGenome genome, refgenome;
    public GenomeList [] genomeList;
    String dbName;
    public DBPrefs dbpref = DBPrefs.getInstance();

    public boolean gbkProductLine = true;

    /**
     * Constructor
     */
    public GenomeAnnotator() {
        setup();
    }


    /**
     * load preferences, initialize variables, etc.
     */
    public void setup() {
        gatuPrefs = new GatuPreferences(dbName);
        statsTable = new int[10];
        Arrays.fill(statsTable, 0);
        genomeList = new GenomeList[1];
        genomeList[0] = new GenomeList();
        genomeList[0].vid = 0;
        genomeList[0].name = "Upload file";
        genomeList[0].prefix = "";
        genomeList[0].size = 0;
    }


    /**
     * read the data for the genome to be annotated from a interchange
     * @return true if file was read, fales otherwise
     */
    public boolean loadToAnnotateFromFile() {
        FileOperations fo = new FileOperations(this);
        int ret = fo.readGenome();

        return ret == 2;
    }

    /**
     * read the reference genome data from a genbank file
     * @return true if file was read, fales otherwise
     */
    public boolean loadReferenceFromFile() {
        FileOperations fo = new FileOperations(this);
        return fo.readReference();
    }



    /**
     * Find all Orfs with a define minimum length
     * @return all orfs on all 6 frames longer than the min orf length
     */

    public List<ORF> getOrfs() {
        String codon;
        int frame, min, max;
        List<ORF> orfs = new ArrayList<>();
        int strand;
        int cnt;
        ORF m_orf;
        boolean circular = dbpref.isCircular(dbName);

        cnt = 1;
        strand = 1;
        // orfs on 5'-3' strand
        for (frame=0; frame<3; frame++) {
            for (min=frame; min<genome.genome_seq.length()-3; min+=3) {
                codon = genome.genome_seq.substring(min,min+3);
                if (codon.equalsIgnoreCase("atg")) {
                    for (max=min; max<genome.genome_seq.length()-3; max+=3) {
                        codon = genome.genome_seq.substring(max,max+3);
                        if ((codon.equalsIgnoreCase("taa")) ||
                                (codon.equalsIgnoreCase("tga")) ||
                                (codon.equalsIgnoreCase("tag"))) {
                            if ((max-min+1) > gatuPrefs.orfLength) {
                                m_orf = new ORF(0, min+1, max+3, strand,
                                        genome.genome_seq, circular);
                                orfs.add(m_orf);
                                cnt++;
                                min = max;
                            }
                            max = genome.genome_seq.length();
                        }
                    }
                }
            }
        }
        strand = -1;
        for (frame=0; frame<3; frame++) {
            for (min=genome.genome_seq.length()-frame; min>2; min-=3) {
                codon = genome.genome_seq.substring(min-3, min);
                if (codon.equalsIgnoreCase("cat")) {
                    for (max=min; max>2; max-=3) {
                        codon = genome.genome_seq.substring(max-3, max);
                        if ((codon.equalsIgnoreCase("tta")) ||
                                (codon.equalsIgnoreCase("tca")) ||
                                (codon.equalsIgnoreCase("cta"))) {
                            if ((min-max+1) > gatuPrefs.orfLength) {
                                m_orf = new ORF(cnt, max-2, min, strand,
                                        genome.genome_seq, circular);
                                orfs.add(m_orf);
                                cnt++;
                                min = max;
                            }
                            max = 2;
                        }
                    }
                }
            }
        }
        return orfs;
    }

    /**
     * Create the blast db (fasta format) for the blast run
     * @return the blast db for the blast run
     */
    public List<String> makeDbFasta() {
        List<String> fasta;
        fasta = new ArrayList<>();
        fasta.add(">" + genome.id + ";" + genome.genome_abbr +
                ";" + genome.gi + ";" + genome.gb_accession +
                ";" + genome.name);
        fasta.add(genome.genome_seq);
        return fasta;
    }

    /**
     * Create the input (fasta format) for the blast run
     * @param z the current gene number/index
     * @param exn the current exon of the current gene number/index
     * @return the input for the blast run
     */
    public List<String> makeInputFasta(int z, int exn) {
        DecimalFormat dcf = new DecimalFormat("##000.00");
        List<String> fasta = new ArrayList<>();
        String gid;
        double number;
        String defline;
        String seq;


        dcf.setMaximumFractionDigits(2);
        number = z + exn / 100.0;
        gid = dcf.format(number);

        defline = ">" + gid + ";" + cds[z].gname + ";" + cds[z].frag +
                ";" + cds[z].fid + ";" + cds[z].fname + ";" + cds[z].vname;
        fasta.add(defline);
        if (cds[z].start.length > 1) {
            seq = cds[z].exon_nseq[exn];
        } else {
            seq = cds[z].prot_sequence;
        }
        fasta.add(seq);
        return fasta;
    }

    /**
     * Run the blast
     */
    public void runBlast() {
        List<Object[]> results;
        RunBlast rb = new RunBlast(this);
        results = rb.runWithReference(genome);

        if (results.size() < 1) return;

        bq = new BlastQueryData[results.size()];
        hits = new ArrayList<>();

        BlastOutput bo;
        StringBuffer sb;
        int c;
        for (int j = 0; j < results.size(); j++) {
            bo = (BlastOutput)results.get(j)[0];
            c = ((Integer)results.get(j)[3]);
            sb = (StringBuffer)results.get(j)[4];
            rb.processResults(bo, c, sb);
        }
    }

    /**
     * defines and returns the sequences for the hits
     */
    private Hashtable<String, String> defineHitSequences() {
        String gs, ps;
        BlastHitData bh;

        Hashtable<String, String> hitDNASequences = new Hashtable<>();
        hitProteinSequences = new Hashtable<>();
        System.out.println("hits.size="+hits.size());
        for (int r=0; r<hits.size(); r++) {
            bh = hits.get(r);
            if (bh.totalnoexons == 1) {
                hitDNASequences.remove(bh.gname);
                hitDNASequences.put(bh.gname, bh.seq);
                hitProteinSequences.remove(bh.gname);
                hitProteinSequences.put(bh.gname, bh.pseq);
            } else {
                if (hitDNASequences.containsKey(bh.gname)) {
                    gs = hitDNASequences.get(bh.gname);
                    if (bh.seq.startsWith("atg") ||
                            bh.seq.startsWith("ATG")) {
                        gs = bh.seq + gs;
                    }
                    else {
                        if (bh.exon > 0) {
                            gs = gs + bh.seq;
                        } else {
                            gs = bh.seq + gs;
                        }
                    }
                    hitDNASequences.remove(bh.gname);
                    hitDNASequences.put(bh.gname, gs);
                    hitProteinSequences.remove(bh.gname);
                    ps = GATUSequenceUtility.makeProtein(gs, bh.frame);
                } else {
                    hitDNASequences.put(bh.gname, bh.seq);
                    hitProteinSequences.remove(bh.gname);
                    ps = GATUSequenceUtility.makeProtein(bh.seq, bh.frame);
                }
                hitProteinSequences.put(bh.gname, ps);
            }
        }
        return hitDNASequences;
    }

    /**
     * TODO: replace query with new server
     * Run needle for each hit
     */
    public void runNeedle() {
        List<String> seqA, seqB;
        ArrayList <String> ans;
        BlastHitData bh;
        String gs;

        for (int r=0; r<hits.size(); r++) {
            bh = hits.get(r);
            seqA = new ArrayList<>();
            seqA.add(">" + bq[bh.queryIndex].gname);
            seqA.add(bq[bh.queryIndex].pseq);
            seqB = new ArrayList<>();
            seqB.add("> Query");
            gs = hitProteinSequences.get(bh.gname);
            seqB.add(gs);
            try {

                // replace with new server make sure application waits
                //ans = asr.needleQuery(seqA, seqB, gop, gex, mat, format);
                ans = new ArrayList<>();

                bh.setNeedle(ans);
                if (bh.similarity.compareTo(100.0) == 0) {
                    statsTable[0]++;
                } else if (bh.similarity.compareTo(95.0) >= 0) {
                    statsTable[1]++;
                } else if (bh.similarity.compareTo(90.0) >= 0) {
                    statsTable[2]++;
                } else if (bh.similarity.compareTo(85.0) >= 0) {
                    statsTable[3]++;
                } else if (bh.similarity.compareTo(80.0) >= 0) {
                    statsTable[4]++;
                } else if (bh.similarity.compareTo(75.0) >= 0) {
                    statsTable[5]++;
                } else if (bh.similarity.compareTo(70.0) >= 0) {
                    statsTable[6]++;
                } else {
                    statsTable[7]++;
                }

            }catch(Exception e){
                e.printStackTrace();
            }
        }
    }


    /**
     * Save new annotations of genome to be annotated to a file
     * //TODO replace with java FX and add CVS to file operations
     */
    public void saveData() {
        String filename = "";
        int gbk = 0;
        int bsml = 0;
        int embl = 0;
        int csv=0;
        int seq = 0;

        //open window here

        if (gbk == 1) writeGBK(filename);
        if (embl == 1) writeEMBL(filename);
        if (bsml == 1) ;//writeBSML(filename);
        if (seq == 1) writeSeq(filename);
        if (csv == 1) ;//writeCSV(filename);
    }



    /*
     * Export gene to a sequin table
     */
    public void writeSequin(File fileToSave, String filename,Hashtable<String, GBKCDSData> gbkGenes,String geneAb,String strainAb) {
        FileOperations fo = new FileOperations(this);
        boolean success = fo.writeSequin(fileToSave, filename, gbkGenes, geneAb,strainAb);

        if(!success) {
            Alert alert = new Alert(Alert.AlertType.INFORMATION);
            alert.setTitle("Information Dialog");
            alert.setContentText("Writing sequin table has failed");
            alert.showAndWait();
        }

    }



    /**
     * Write a XML (BSML) file for BBB, updated for java fx
     * @param bf BSML(XML) file name
     */
//    public void writeBSML(String bf){
//        if(!bf.endsWith(".bsml")){
//            bf+=".bsml";
//        }
//
//        File f= new File(bf);
//        if(f.exists()) {
//            if(!overwritePopup()){
//                return;
//            }
//        }
//
//        String bsmlFile = bf;
//        BSMLFeaturedSequenceWriter bsml;
//        ArrayList <FeaturedSequence> seqs =  getFeaturedSeqs(true);
//
//        if (cds == null) return;
//        try {
//            bsml = new BSMLFeaturedSequenceWriter(bsmlFile, "");
//        } catch (Exception e) {
//            e.printStackTrace();
//            return;
//        }
//
//        try {
//            bsml.writeSequences(seqs.listIterator());
//        } catch (Exception e) {
//            e.printStackTrace();
//        }
//    }

    /**
     * Write a Sequin file
     * @param seqf Sequin file name
     */
    public void writeSeq(String seqf) {
        if(blastData.isEmpty()){
            Alert alert = new Alert(Alert.AlertType.INFORMATION);
            alert.setTitle("Information Dialog");
            alert.setHeaderText(null);
            alert.setContentText("Tried to write a Sequin file but blast data not found");

            alert.showAndWait();
            return;
        }

        String inputName = JOptionPane.showInputDialog(
                "To use a custom gene name enter a virus abbreviation. Leave blank to use reference gene name: ");
        String strainName= JOptionPane.showInputDialog("Enter a strain abbreviation (optional): ");

        File fileToSave = new File(seqf);
        if(fileToSave.exists()){
            if(!overwritePopup()){
                return;
            }
        }
        gbkGenes = getCurrentGenes();
        writeSequin(fileToSave,seqf ,gbkGenes,inputName,strainName);

    }

    /**
     * opens a window that asks the user if they want to overwrite
     * @return true if the user says the overwriting is ok
     */
    private static boolean overwritePopup(){
        Alert alert = new Alert(Alert.AlertType.CONFIRMATION);
        alert.setTitle("Overwrite");
        alert.setContentText("The file already exists, overwrite?");
        ButtonType okButton = new ButtonType("Yes", ButtonBar.ButtonData.YES);
        ButtonType noButton = new ButtonType("No", ButtonBar.ButtonData.NO);
        alert.getButtonTypes().setAll(okButton, noButton);

        Optional<ButtonType> result = alert.showAndWait();
        return result.isPresent() && result.get().equals(okButton);
    }

    /**
     * Write a Genbank file
     * @param emblf EMBL file name
     */
    public void writeEMBL(String emblf) {
        List<Integer> starts, stops;
        GBKCDSData gbkg;
        int nbp, cds_start;
        Hashtable <String,GBKCDSData> gbkcdslist;
        List<String> sorted_gbkcdslist;
        NumberFormat nfm;
        String key;
        String line;

        if (genome == null) return;
        nbp = genome.genome_seq.length();
        line = "" + nbp;
        nfm = NumberFormat.getNumberInstance();
        nfm.setMaximumIntegerDigits(line.length());
        nfm.setMinimumIntegerDigits(line.length());
        nfm.setMaximumFractionDigits(0);
        nfm.setMinimumFractionDigits(0);

        try {
            if(!emblf.endsWith(".embl")){
                emblf+=".embl";
            }
            File out = new File(emblf);
            if(out.exists()){
                if(!overwritePopup()){
                    return;
                }
            }
            FileOutputStream fos = new FileOutputStream(out);
            PrintWriter file = new PrintWriter(fos);
            // write genes
            gbkGenes = getCurrentGenes();

            //TODO: needs to be replaced by java fx
            //aorfs = orfPanel.getAcceptedOrfs();
            aorfs = null;
            System.out.println("this code cant get ORFs yet see WriteEmbl in genome annotator");

                    gbkcdslist = new Hashtable<>();
            Enumeration<GBKCDSData> egbk = gbkGenes.elements();
            while (egbk.hasMoreElements()) {
                gbkg = egbk.nextElement();
                cds_start = (gbkg.start.get(0));
                gbkcdslist.put(nfm.format(cds_start) + "." + gbkg.name, gbkg);
            }
            for (ORF aorf : aorfs) {
                starts = new ArrayList<>();
                starts.add(aorf.start);
                stops = new ArrayList<>();
                stops.add(aorf.stop);
                cds_start = aorf.start;
                gbkg = new GBKCDSData();
                gbkg.name = aorf.name;
                gbkg.family = aorf.product;
                for (int ns = 0; ns < starts.size(); ns++) {
                    gbkg.addOrf((starts.get(ns)),
                            (stops.get(ns)));
                }
                gbkg.complement = aorf.strand <= 0;
                gbkg.protein_sequence = aorf.pseq;
                gbkg.nucl_sequence = aorf.nseq;
                gbkg.id = aorf.id;
                gbkg.type = aorf.type;
                gbkcdslist.put(nfm.format(cds_start) + "." + gbkg.name, gbkg);
            }
            sorted_gbkcdslist = new ArrayList<>(gbkcdslist.keySet());
            Collections.sort(sorted_gbkcdslist);
            for (int ncds=0; ncds<sorted_gbkcdslist.size(); ncds++) {
                key = sorted_gbkcdslist.get(ncds);
                gbkg = gbkcdslist.get(key);
                GenomeAnnotatorUtility.writeGeneToEMBL(file, gbkg.name,
                        gbkg.start, gbkg.stop,
                        gbkg.complement,
                        gbkg.nucl_sequence,
                        gbkg.accession, gbkg.family);
            }
            file.flush();
            file.close();
        } catch (Exception e) {
            JOptionPane.showMessageDialog(null, "Write EMBL file error: " +
                            e.getMessage() + "\n" + emblf,
                    "Genome editor",
                    JOptionPane.ERROR_MESSAGE);
        }

    }

    /**
     * Write a Genbank file
     * @param gbkf Genbank file name
     */
    public void writeGBK(String gbkf) {
        String inputName = JOptionPane.showInputDialog(
                "To use a custom gene name enter a virus abbreviation. Leave blank to use reference gene name: ");
        String strainName= "";

        if (!inputName.equals(""))
            strainName = JOptionPane.showInputDialog( "Enter a strain abbreviation (optional): ");

        if (!gbkf.contains(".gbk"))
            gbkf+=".gb";

        System.out.println(gbkf);
        File f= new File(gbkf);
        if(f.exists()){
            if(!overwritePopup()){
                return;
            }
        }

        FileOperations fo = new FileOperations(this);
        fo.writeGBK(gbkf, genome, refgenome, gbkProductLine,inputName,strainName);

    }

    /**
     * Get all genes and their actual data
     */
    public Hashtable<String, GBKCDSData> getCurrentGenes() {
        //TODO: needs to be replaced by java fx, see old code
        System.out.println("accessed a method that needs to be replaced");
        return null;
    }

    /**
     * Create featured sequences, used for writing bsml
     * @param annoGenomeOnly if true create featured seq for genome
     *                       to be annotaed only
     *                       otherwise create featured seq for both genomes
     * @return the featured sequences
     */
//    public List<FeaturedSequence> getFeaturedSeqs(boolean annoGenomeOnly) {
//        //TODO: needs to be replaced by java fx, see old code
//        System.out.println("accessed a method that needs to be replaced");
//        return null;
//    }

    /**
     * Check if nucleotide sequence has an internal stop codon
     */
    private boolean hasInternalStop(String seq) {
        boolean hasStop = false;
        String codon;
        for (int i=0; i<(seq.length()-6); i+=3) {
            codon = seq.substring(i,i+3);
            if ((codon.equalsIgnoreCase("taa")) ||
                    (codon.equalsIgnoreCase("tga")) ||
                    (codon.equalsIgnoreCase("tag"))) {
                hasStop = true;
            }
        }
        return hasStop;
    }


    //------- functions used by LoadFile -----

    public void setRefGenome(GBKGenome ref) {
        refgenome = ref;
    }

    public void setAnnGenome(GBKGenome ann) {
        genome = ann;
    }

    public void setCDS(CDS[] c) {
        cds = c;
    }

    //------- functions used by RunBlast -----
    public CDS[] getCDS() {
        return cds;
    }

    public int getCurrentMaxGid() {
        return currentMaxGid;
    }

    public void setCurrentMaxGid(int x) {
        currentMaxGid = x;
    }

    public void setBlastQueryData(BlastQueryData bqd, int bc) {
        bq[bc] = bqd;
    }

    public void mergeHits(List<BlastHitData> bhd) {
        hits.addAll(bhd);
    }

    public void setStats(int s8, int s9) {
        statsTable[8] = s8;
        statsTable[9] = s9;
    }
}
