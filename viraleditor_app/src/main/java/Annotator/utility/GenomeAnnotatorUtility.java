package Annotator.utility;

import java.util.List;
import java.util.ArrayList;
import java.util.Properties;
import java.io.PrintWriter;
import javax.naming.Context;

public final class GenomeAnnotatorUtility {

    /**
     * Parse the definition string from a blast report
     * @param def the definition line from a blast output (query or hit)
     * @return the words in the definition
     */
    public static String [] parseDefinition(String def) {
        String [] tokens = null;
        String seperator = ";";

        try {
            tokens = def.split(seperator);
        } catch (Exception pse) {
            System.err.println(pse.getMessage());
        }
        return tokens;
    }

    /**
     * Set the properties for the application server
     * @param appServerIP the IP address of the application server
     * @param appServerPort the TCP/IP port number of the application server
     * @return asp the properties object
     */
    public static Properties setProperties(String appServerIP,
                                           int appServerPort) {
        Properties asp = new Properties();
        asp.put(Context.INITIAL_CONTEXT_FACTORY,
                "org.jnp.interfaces.NamingContextFactory");
        asp.put(Context.URL_PKG_PREFIXES, "jboss.naming:org.jnp.interfaces");
        asp.put(Context.PROVIDER_URL, appServerIP + ":" + appServerPort);
        return asp;
    }

    /**
     * Find logest orf w/o internal stop codon in given sequence
     * @param seq the sequence (5'-3')
     * @return the start/stop position of the longest orf
     */
    public static int [] findLongestOrf(String seq) {
        String codon1, codon2, stpcodon, orfs;
        int slen,pos,cpos,olen;
        boolean isStop ;
        boolean hasStop ;
        int start = 0, stop = 0;
        int [] orfpos = {0, seq.length() - 1};

        slen = seq.length() - 2;
        for (pos=0; pos<slen; pos+=3) {
            codon1 = seq.substring(pos, pos+3);
            if (codon1.equalsIgnoreCase("atg")) {
                for (cpos=pos+3; cpos<slen; cpos+=3) {
                    codon2 = seq.substring(cpos, cpos+3);
                    isStop = false;
                    if (codon2.equalsIgnoreCase("taa")) {
                        isStop = true;
                    }
                    if (codon2.equalsIgnoreCase("tga")) {
                        isStop = true;
                    }
                    if (codon2.equalsIgnoreCase("tag")) {
                        isStop = true;
                    }
                    if (isStop) {
                        orfs = seq.substring(pos, cpos+3);
                        olen = orfs.length() - 5;
                        hasStop = false;
                        for (int k=0; k<olen; k+=3) {
                            stpcodon = orfs.substring(k, k+3);
                            if (stpcodon.equalsIgnoreCase("taa")) hasStop = true;
                            if (stpcodon.equalsIgnoreCase("tga")) hasStop = true;
                            if (stpcodon.equalsIgnoreCase("tag")) hasStop = true;
                            if (hasStop) k=olen;
                        }
                        if (!hasStop) {
                            if ((stop-start+1) < (cpos+3-1-pos+1)) {
                                start = pos;
                                stop = cpos + 3 - 1;  // includes stop codon and last position
                            }
                        }
                    }
                }
            }
        }
        orfpos[0] = start;
        orfpos[1] = stop;
        return orfpos;
    }

    /**
     * Write entry for the given gene to the GenBank file
     * @param file the genbank file
     * @param gname the gene name
     * @param start the start location of the gene
     * @param stop the stop location of the gene
     * @param complement the strand of the gene (if true, then minus)
     * @param proteinSeq the protein sequence of the gene
     * @param gi the genbank id of the gene
     * @param proteinId the protein id of the gene
     * @param type the gene type
     *             ("protein"=gene,"pseudogene"=fragment,
     *              "mature_protein"=mat_peptide);
     * @param fname the family name
     * @param function the gene function
     * @param gbkProductLine write either product line or note line
     * @param note the text for the CDS note qualifier
     */
    public static void writeGeneToGB(PrintWriter file, String gname, List<Integer> start,
                                     List<Integer> stop, boolean complement,
                                     String  proteinSeq, int gi, String proteinId,
                                     int gid,String fname, String function, String type,
                                     boolean gbkProductLine, String note) {
        String line;
        if (start.size() > 1) {
            if (complement) {
                line = "join(complement(";
                line = line + start.get(0) + ".." + stop.get(0);
                for (int i=1; i<start.size(); i++) {
                    line = line + "," + start.get(i) + ".." + stop.get(i);
                }
                line += "))";
            } else {
                line = "join(";
                line = line + start.get(0) + ".." + stop.get(0);
                for (int i=1; i<start.size(); i++) {
                    line = line + "," + start.get(i) + ".." + stop.get(i);
                }
                line += ")";
            }
        } else {
            if (start.size() > 0) {
                if (complement) {
                    line = "complement(" + start.get(0) + ".." + stop.get(0) + ")";
                } else {
                    line = "" + start.get(0) + ".." + stop.get(0);
                }
            } else {
                line = "0..0";
            }
        }

        if (!type.equals("mature_protein")) {
            file.println("     gene            " + line);
            file.println("                     " + "/gene=" + '"' + gname + '"');
            file.println("     CDS             " + line);
            file.println("                     " + "/gene=" + '"' + gname + '"');
            file.println("                     /codon_start=1");
        } else {
            file.println("     mat_peptide     " + line);
        }
        if (fname != null) {
            if (fname.length() > 0) {
                if (gbkProductLine) {
                    file.println("                     /product=" + '"' + fname + '"');
                } else {
                    file.println("                     /note=" + '"' + fname + '"');
                }
            }
        }
        if(function != null && function.length() > 0) {
            file.println("                     /function=" + '"' + function + '"');
        }

        if (note != null) {
            if (note.length() > 0) {
                if (note.length() > 51) {
                    file.println("                     /note=" + '"' + note.substring(0,51));
                } else {
                    file.println("                     /note=" + '"' + note + '"');
                }
                for (int nl=51; nl<note.length(); nl=nl+58) {
                    if (nl+58 < note.length()) {
                        file.println("                     " + note.substring(nl,nl+58));
                    } else {
                        file.println("                     " + note.substring(nl) + '"');
                    }
                }
            }
        }

        if (proteinId != null && proteinId.length() > 0)
            file.println("                     /protein_id=" + '"' + proteinId + '"');
        if (gi > 0)
            file.println("                     /db_xref=" + '"' + "GI:" + gi + '"');
        if (proteinSeq.length() > 44)
            file.println("                     /translation=" + '"' + proteinSeq.substring(0,44));
        else
            file.println("                     /translation=" + '"' + proteinSeq + '"');
        for (int sl=44; sl<proteinSeq.length(); sl=sl+58) {
            if (sl+58 < proteinSeq.length())
                file.println("                     " + proteinSeq.substring(sl,sl+58));
            else
                file.println("                     " + proteinSeq.substring(sl) + '"');
        }
    }

    /**
     * write the entry for the given gene to the EMBL file
     * @param file the embl file
     * @param gname the gene name
     * @param start the start location of the gene
     * @param stop the stop location of the gene
     * @param complement the strand of the gene (if true, then minus)
     * @param nuclSeq the nucleotide sequence of the gene
     * @param proteinId the protein id of the gene
     * @param fname the family name
     */
    public static void writeGeneToEMBL(PrintWriter file, String gname,
                                       List<Integer> start, List<Integer> stop,
                                       boolean complement,
                                       String nuclSeq, String proteinId,
                                       String fname) {
        String line;
        int cnt, len;
        line = "ID   " + gname;
        file.println(line);
        line = "AC   " + proteinId;
        file.println(line);
        if (start.size() > 1) {
            if (complement) {
                line = "DE   " + "join(complement(";
                line = line + start.get(0) + ".." + stop.get(0);
                for (int i=1; i<start.size(); i++) {
                    line = line + "," + start.get(i) + ".." + stop.get(i);
                }
                line += "))";
            } else {
                line = "DE   " + "join(";
                line = line + start.get(0) + ".." + stop.get(0);
                for (int i=1; i<start.size(); i++) {
                    line = line + "," + start.get(i) + ".." + stop.get(i);
                }
                line += ")";
            }
        } else {
            if (start.size() > 0) {
                if (complement) {
                    line = "DE   " + "complement(" + start.get(0) + ".." + stop.get(0) + ")";
                } else {
                    line = "DE   " + start.get(0) + ".." + stop.get(0);
                }
            } else {
                line = "DE   " + "0..0";
            }
        }
        line = line + " ; " + fname;
        file.println(line);
        line = "SQ           " + nuclSeq.length() + " BP";
        file.println(line);
        cnt = 0;
        len = 10;
        for (int k=0; k<nuclSeq.length(); k=k+10) {
            if ((k+10) > nuclSeq.length()) {
                len = nuclSeq.length() - k;
            }
            if (cnt > 0) {
                line = line + " " + nuclSeq.substring(k, k+len);
            } else {
                line = "     " + nuclSeq.substring(k, k+len);
            }
            cnt++;
            if (cnt > 5) {
                file.println(line);
                cnt = 0;
            }
        }
        if (cnt > 0) {
            file.println(line);
        }
        line = "//";
        file.println(line);
    }
}
