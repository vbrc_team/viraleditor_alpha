package Annotator.utility;

import Annotator.preferences.GatuPreferences;
import Annotator.structures.BlastHitData;
import Annotator.structures.BlastQueryData;
import Annotator.structures.CDS;
import Annotator.structures.GBKGenome;
import Annotator.structures.BlastHit;
import Annotator.structures.BlastHsp;
import Annotator.structures.BlastIteration;
import Annotator.structures.BlastOutput;
import Shared.preferences.ExternalAppsPrefs;
import Shared.Gene;

import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.util.ArrayList;
import java.util.List;
import java.util.ListIterator;
import java.util.Properties;

import javax.swing.*;

/**
 * Runs a blast and returns the results as
 * a list of object arrays.
 *
 * @author Francesco Marass, 23 May 2008
 *
 * 2020: refrences to egatu (the old vocs gatu) has been removed with permission from upton, use interchange with this
 *
 */
public class RunBlast {

    private GatuPreferences gatupref = null;
    private final GenomeAnnotator gatu;

    public GBKGenome genomein = null;
    public List<Object[]> outputin = new ArrayList<>();


    /**
     * Creates an object to run a blast from GATU.
     * @param parent	parent GATU
     */
    public RunBlast(GenomeAnnotator parent) {
        gatu = parent;
    }


    /**
     * Sets the necessary preferences in order to run a blast.
     *
     * @param eap	external applications preferences
     * @param gp	gatu preferences
     * @param p		properties
     */
    public void setPrefs(ExternalAppsPrefs eap, GatuPreferences gp, Properties p) {
        gatupref = gp;
    }

    /**
     * Currently stripped, to be used for dummy data if needed
     *
     * Runs a blast on the genome in the argument after a reference
     * has been set and its CDS are available. This is the blast GATU
     * normally does.
     *
     * @param genome the genome to be annotated
     * @return the results of the blast
     */
    public List<Object[]> runWithReference(GBKGenome genome) {

        this.genomein = genome;
        // reference and input file
        List<String> fasta;
        List<String> dbfasta;
        // dna sequence to annotate
        StringBuffer dnaseqtop = new StringBuffer(genome.genome_seq);
        Object[] tmp;
        int blastCnt = 0;
        // temporarily holds the result of each blast
        List<Object> result = new ArrayList<>();
        // CDS from the reference
        CDS[] cds = gatu.getCDS();
        dbfasta = makeDbFasta(genome);

        for (int j = 0; j < cds.length; j++) {
            tmp = new Object[5];
            // for each exon in the sequence
            for (int exon = 0; exon < cds[j].start.length; exon++) {
                fasta = makeInputFasta(cds, j, exon);
                    // blast!
                    try {
                            //TODO: replace
                            //result = ((BlastMessage)bmess).getResult();
                            if (result != null) {
                                if (result.size() > 0) {
                                    BlastOutput bo = (BlastOutput) result.get(0);
                                    tmp[0] = bo;
                                    tmp[1] =  j;
                                    tmp[2] = exon;
                                    tmp[3] = blastCnt;
                                    tmp[4] = dnaseqtop;
                                    outputin.add(tmp);

                                    blastCnt++;
                                }
                            }
                            // clean
                            result = null;
                    } catch (Exception e) {
                        JOptionPane.showMessageDialog(null, e.getMessage(), "Application Server Error", JOptionPane.ERROR_MESSAGE);
                    }
            }
        }
        return outputin;
    }


    /**
     * Creates a list representing a fasta file for the genome
     * that needs annotations.
     *
     * @param genome genome to annotate
     * @return a list containing one string for the definition, and one for the sequence
     */
    public List<String> makeDbFasta(GBKGenome genome) {
        List<String> fasta;
        fasta = new ArrayList<>();
        fasta.add(">" + genome.id + ";" + genome.genome_abbr +
                ";" + genome.gi + ";" + genome.gb_accession +
                ";" + genome.name);
        fasta.add(genome.genome_seq);
        return fasta;
    }

    /**
     * Creates a list representing a fasta file for an exon in a gene
     * of the reference genome.
     *
     * @param cds array of coding sequences from the reference genome
     * @param gene the id of the current gene
     * @param exon the id of the current exon in the current gene
     * @return -
     */
    public List<String> makeInputFasta(CDS[] cds, int gene, int exon) {
        DecimalFormat dcf = new DecimalFormat("##000.00");
        dcf.setMaximumFractionDigits(2);
        dcf.setMinimumFractionDigits(2);

        List<String> fasta;
        fasta = new ArrayList<>();

        String gid;
        double number;
        String defline;
        String seq;

        number = gene + exon / 100.0;
        gid = dcf.format(number);


        // creating definition
        defline = ">" + gid + ";" + cds[gene].gname + ";" + cds[gene].frag +
                ";" + cds[gene].fid + ";" + cds[gene].fname + ";" + cds[gene].vname;
        fasta.add(defline);

        // retrieving amino acid sequence
        if (cds[gene].start.length > 1) {
            seq = cds[gene].exon_nseq[exon];
        }
        else {
            seq = cds[gene].prot_sequence;
        }
        fasta.add(seq);
        return fasta;
    }

    /**
     * TODO: this needs to be refactored
     */
    public void processResults(BlastOutput bout, int bc, StringBuffer dnaseqtop) {
        ListIterator <BlastIteration> litr;
        ListIterator <BlastHit> hitr;
        ListIterator <BlastHsp> pitr;
        BlastIteration bi;
        BlastHit bhit;
        BlastHsp bhsp;
        String queryIndex, queryExon, queryFamilyId, queryFrag;
        String queryGeneName, queryVirusName;
        String queryDef;
        String hitVirusId, hitVirusGI, hitVirusAcc;
        String hitVirusName, hitVirusAbbrev;
        String hitDef;
        String str;
        int qid, qexon, qfid;
        int hvid, hvgi;
        int qstart, qstop, hstart, hstop;
        int tmpno;
        int maxblanks;
        int hspno, ngaps;
        boolean findStopCodon;
        boolean findStartCodon;
        String subhseq, submid, subqseq;
        String [] qgstr, hgstr, tmpstr;
        StringBuffer qalignmentText, alignmentText;
        String codon;
        boolean startCodon, stopCodon;
        int m_start, m_stop;
        NumberFormat nf;
        double percent;
        BlastHitData bh;
        String [] tokens;
        Gene gene;
        String bprg;
        int remainder, qremainder, remainderdiff;
        int ratio;
        int chkpnt;

        // vars that are global in GenomeAnnotator
        CDS[] cds = gatu.getCDS();

        boolean pickLongestOrf;

        // vars that need to be set in GATU
        BlastQueryData bqd ;

        // safe to assume they are 0 because this is the only function that alters their value
        int statsTable8 = 0;	// statsTable[8]
        int statsTable9 = 0;	// statsTable[9]

        int currentMax = gatu.getCurrentMaxGid();

        List<BlastHitData> bhd = new ArrayList<>();


        nf = NumberFormat.getNumberInstance();
        nf.setMaximumFractionDigits(1);
        nf.setMinimumFractionDigits(1);
        bprg = bout.program.toLowerCase();
        queryDef = bout.queryDef;
        tokens = GenomeAnnotatorUtility.parseDefinition(queryDef);
        queryIndex = "0";
        queryExon = "0";
        queryGeneName = "";
        queryFamilyId = "0";
        queryVirusName = "";
        queryFrag = "protein";
        if (tokens != null) {
            if (tokens.length > 0) {
                queryExon = "0";
                int seperatorIdx = tokens[0].indexOf(((DecimalFormat) DecimalFormat.getInstance()).getDecimalFormatSymbols().getDecimalSeparator());
                if (seperatorIdx != -1) {
                    queryIndex = tokens[0].substring(0,seperatorIdx);
                    queryExon = tokens[0].substring(seperatorIdx+1);
                }
                else {
                    queryIndex = tokens[0];
                }
            }
            if (tokens.length > 1) queryGeneName = tokens[1];
            if (tokens.length > 2) queryFrag = tokens[2];
            if (tokens.length > 3) queryFamilyId = tokens[3];
            if (tokens.length > 5) queryVirusName = tokens[5];
        }
        qid = 0;
        try {
            qid = Integer.parseInt(queryIndex);
        }
        catch (Exception e) {
            System.out.println("ERROR: Invalid query id " + queryIndex);
        }
        qexon = 0;
        try {
            qexon = Integer.parseInt(queryExon);
        }
        catch (Exception e) {
            System.out.println("ERROR: Invalid query exon " + queryExon);
        }
        qfid = 0;
        try {
            qfid = Integer.parseInt(queryFamilyId);
        }
        catch (Exception e) {
            System.out.println("ERROR: Invalid query family id " + queryFamilyId);
        }
        qalignmentText = new StringBuffer("Query = " + queryDef + "\n           (" + bout.queryLen + " letters)\n");
        bqd = new BlastQueryData();
        bqd.gid = cds[qid].gid;
        bqd.start = cds[qid].start[qexon];
        bqd.stop = cds[qid].stop[qexon];
        bqd.exon = qexon;
        bqd.totalnoexons = cds[qid].start.length;
        bqd.strand = cds[qid].strand;
        bqd.description = cds[qid].description;
        bqd.note_desc = cds[qid].note_desc;
        bqd.gname = queryGeneName;
        bqd.vname = queryVirusName;
        bqd.fid = qfid;
        bqd.fname = cds[qid].fname;
        bqd.function = cds[qid].function;
        bqd.length = bout.queryLen;
        ratio = bqd.length / 3;
        qremainder = bqd.length - (ratio * 3);
        bqd.frag = queryFrag;
        bqd.text = new StringBuffer(qalignmentText.toString());
        if (bqd.totalnoexons > 1) {
            bqd.seq = cds[qid].exon_nseq[bqd.exon];
        }
        else {
            bqd.seq = cds[qid].nucl_sequence;
        }
        bqd.pseq = cds[qid].prot_sequence;

        litr = bout.iterations.listIterator(0);
        while (litr.hasNext()) {
            bi = litr.next();
            hitr = bi.hits.listIterator(0);
            while (hitr.hasNext()) {
                bhit = hitr.next();
                hitDef = bhit.definition;
                tokens = GenomeAnnotatorUtility.parseDefinition(hitDef);
                hitVirusId = "0";
                hitVirusAbbrev = "";
                hitVirusGI = "0";
                hitVirusAcc = "";
                hitVirusName = "";
                if (tokens != null) {
                    if (tokens.length > 0) hitVirusId = tokens[0];
                    if (tokens.length > 1) hitVirusAbbrev = tokens[1];
                    if (tokens.length > 2) hitVirusGI = tokens[2];
                    if (tokens.length > 3) hitVirusAcc = tokens[3];
                    if (tokens.length > 4) hitVirusName = tokens[4];
                }
                hvid = 0;
                try {
                    hvid = Integer.parseInt(hitVirusId);
                }
                catch (Exception e) {
                    System.out.println("ERROR: Invalid hit virus id " + hitVirusId);
                }
                hvgi = 0;
                try {
                    hvgi = Integer.parseInt(hitVirusGI);
                }
                catch (Exception e) {
                    System.out.println("ERROR: Invalid hit virus GI " + hitVirusGI);
                }
                alignmentText = new StringBuffer(">" + hitDef + "\n          Length = " + bhit.len + "\n\n");
                bh = new BlastHitData();
                bh.vid = hvid;
                bh.vabbrev = hitVirusAbbrev;
                bh.vname = hitVirusName;
                bh.vacc = hitVirusAcc;
                bh.vgi = hvgi;
                bh.gname = bqd.gname;
                bh.fid = bqd.fid;
                bh.fname = bqd.fname;
                bh.function = bqd.function;
                bh.description = bqd.description;
                bh.note_desc = bqd.note_desc;
                bh.gid = currentMax;
                bh.exon = bqd.exon;
                bh.totalnoexons = bqd.totalnoexons;
                bh.queryIndex = bc;
                bh.frag = bqd.frag;
                bh.psize = bqd.stop - bqd.start + 1;
                hspno = 0;
                pitr = bhit.hsps.listIterator(0);
                while (pitr.hasNext()) {
                    bhsp = pitr.next();
                    percent = bhsp.identity * 100.0 / bhsp.align_len;
                    if (hspno > 0) {
                        if (bh.score == bhsp.bitScore) {
                            if (bqd.strand > 0) {
                                if (bh.frame < 0) hspno = 0;
                            }
                            else {
                                if (bh.frame > 0) hspno = 0;
                            }
                        }
                    }
                    if (hspno == 0) {
                        bh.first = true;
                        bh.exp = bhsp.evalue_exp;
                        bh.base = bhsp.evalue_base;
                        bh.score = bhsp.bitScore;
                        bh.length = bhsp.align_len;
                        bh.start = bhsp.hit_from;
                        bh.stop = bhsp.hit_to;
                        bh.frame = bhsp.hit_frame;
                        ratio = bh.length / 3;
                        if (bqd.totalnoexons > 1) {
                            remainder = bh.length - (ratio * 3);
                            remainderdiff = qremainder - remainder;
                            if (remainder != qremainder) {
                                chkpnt = Math.max(bhsp.query_to, bhsp.query_from);
                                if (chkpnt == bh.psize) {
                                    if (bh.frame > 0) {
                                        if (bqd.start > 1) {
                                            bh.start = bh.start - remainderdiff;
                                        }
                                    }
                                    else {
                                        bh.stop = bh.stop + remainderdiff;
                                    }
                                }
                                else {
                                    if (bh.frame > 0) {
                                        bh.stop = bh.stop + remainderdiff;
                                    }
                                    else {
                                        bh.start = bh.start - remainderdiff;
                                    }
                                }
                            }
                        }

                        bh.identity = percent;
                        bh.similarity = percent;
                        if (bhsp.evalue_exp > 0) {
                            if (bhsp.evalue_exp != 1) {
                                bh.evalue = "" + nf.format(bhsp.evalue_base) + "e+" +
                                        bhsp.evalue_exp;
                            }
                            else {
                                bh.evalue = "" + bhsp.evalue_base;
                            }
                        }
                        else {
                            bh.evalue = "" + nf.format(bhsp.evalue_base) + "e" +
                                    bhsp.evalue_exp;
                        }

                        if (gatupref.geneOnBothStrands) {
                            hspno = 1;
                        }
                        else {
                            if (bqd.strand > 0) {
                                if (bh.frame > 0) hspno = 1;
                            }
                            else {
                                if (bh.frame < 0) hspno = 1;
                            }
                        }
                        if (hspno > 0) {
                            codon = bqd.seq.substring(0,3);
                            findStartCodon = codon.equalsIgnoreCase("atg");
                            if (bqd.frag.equals("mature_protein")) findStartCodon = false;
                            if (findStartCodon) {
                                startCodon = false;
                                if (bh.frame > 0) {
                                    m_start = bh.start + 2;
                                    while (!startCodon) {
                                        if ((m_start-3) < 0) {
                                            startCodon = true;
                                            m_start = bh.start- 1;
                                        }
                                        else {
                                            codon = dnaseqtop.substring(m_start-3,m_start);
                                            if (codon.equalsIgnoreCase("atg")) startCodon = true;
                                            m_start -= 3;
                                        }
                                    }
                                    m_start++;
                                    if (bh.start != m_start) statsTable8++;
                                    bh.start = m_start;
                                }
                                else {
                                    m_start = bh.stop - 3;
                                    while (!startCodon) {
                                        if ((m_start+3) >= dnaseqtop.length()) {
                                            startCodon = true;
                                            m_start = bh.stop;
                                        }
                                        else {
                                            codon = dnaseqtop.substring(m_start,m_start+3);
                                            if (codon.equalsIgnoreCase("cat")) startCodon = true;
                                            m_start += 3;
                                        }
                                    }
                                    if (bh.start!= m_start) statsTable8++;
                                    bh.stop = m_start;
                                }
                            }
                            findStopCodon = false;
                            codon = bqd.seq.substring(bqd.seq.length()-3);
                            if (codon.equalsIgnoreCase("taa")) findStopCodon = true;
                            if (codon.equalsIgnoreCase("tga")) findStopCodon = true;
                            if (codon.equalsIgnoreCase("tag")) findStopCodon = true;
                            if (bqd.frag.equals("mature_protein")) findStopCodon = false;
                            if (findStopCodon) {
                                stopCodon = false;
                                if (bh.frame > 0) {
                                    m_stop = bh.stop - 3;
                                    while (!stopCodon) {
                                        if ((m_stop < 0) || ((m_stop+3) > dnaseqtop.length())) {
                                            stopCodon = true;
                                            m_stop = bh.stop;
                                        }
                                        else {
                                            codon = dnaseqtop.substring(m_stop,m_stop+3);
                                            if (codon.equalsIgnoreCase("taa")) stopCodon = true;
                                            if (codon.equalsIgnoreCase("tag")) stopCodon = true;
                                            if (codon.equalsIgnoreCase("tga")) stopCodon = true;
                                            m_stop += 3;
                                        }
                                    }
                                    if (bh.stop != m_stop) statsTable9++;
                                    bh.stop = m_stop;
                                }
                                else {
                                    m_stop = bh.start + 3;
                                    while (!stopCodon) {
                                        if (m_stop < 1) {
                                            stopCodon = true;
                                            m_stop = bh.start;
                                        }
                                        else {
                                            codon = dnaseqtop.substring(m_stop-1,m_stop+3-1);
                                            if (codon.equalsIgnoreCase("tta")) stopCodon = true;
                                            if (codon.equalsIgnoreCase("tca")) stopCodon = true;
                                            if (codon.equalsIgnoreCase("cta")) stopCodon = true;
                                            if (m_stop < 2) {
                                                m_stop = 1;
                                                stopCodon = true;
                                                m_stop = bh.start;
                                            }
                                            if (!stopCodon) m_stop -= 3;
                                        }
                                    }
                                    if (bh.stop != m_stop) statsTable9++;
                                    bh.start = m_stop;
                                }
                            }

                            gene = new Gene();
                            if (bh.frame > 0) {
                                gene.strand = "+";
                                gene.orf_start = bh.start;
                                gene.orf_stop = bh.stop;
                            }
                            else {
                                gene.strand = "-";
                                gene.orf_start = bh.stop;
                                gene.orf_stop = bh.start;
                            }
                            gene.fragment = bh.frag;
                            if (bh.totalnoexons > 1) gene.fragment = "mature_protein";
                            String ntseq =
                                    GATUSequenceUtility.setDNASeq(dnaseqtop.toString(),
                                            gene.orf_start, gene.orf_stop,
                                            gene.strand);
                            gene.setDNA(ntseq);
                            bh.seq = gene.getDnaSeq();
                            if (bh.totalnoexons == 1) {
                                pickLongestOrf = true;
                                for (int cpos=0; cpos<bqd.seq.length()-6; cpos+=3) {
                                    codon = bqd.seq.substring(cpos,cpos+3);
                                    if (codon.equalsIgnoreCase("taa")) pickLongestOrf = false;
                                    if (codon.equalsIgnoreCase("tga")) pickLongestOrf = false;
                                    if (codon.equalsIgnoreCase("tag")) pickLongestOrf = false;
                                }
                                if (pickLongestOrf) {
                                    if ((bh.frag.equals("protein")) ||
                                            (bh.frag.equals("polyprotein"))) {

                                        int [] orf = GenomeAnnotatorUtility.findLongestOrf(bh.seq);

                                        if ((orf[1] - orf[0]) > 0) {
                                            int gorflen;
                                            if (gene.orf_start > gene.orf_stop)
                                                gorflen = gene.orf_start - gene.orf_stop + 1;
                                            else
                                                gorflen = gene.orf_stop - gene.orf_start + 1;
                                            if (gorflen != (orf[1] - orf[0] + 1)) {

                                                if (bh.frame > 0) {
                                                    gene.orf_start = gene.orf_start + orf[0];
                                                    gene.orf_stop = gene.orf_start + (orf[1] - orf[0]);
                                                    bh.start = gene.orf_start;
                                                    bh.stop = gene.orf_stop;
                                                } else {
                                                    gene.orf_start = gene.orf_start - orf[0];
                                                    gene.orf_stop = gene.orf_start - (orf[1] - orf[0]);
                                                    bh.start = gene.orf_stop;
                                                    bh.stop = gene.orf_start;
                                                }
                                            }
                                            ntseq =
                                                    GATUSequenceUtility.setDNASeq(dnaseqtop.toString(),
                                                            gene.orf_start, gene.orf_stop, gene.strand);
                                            gene.setDNA(ntseq);
                                            bh.seq = gene.getDnaSeq();
                                        }
                                    }
                                }
                            }
                            gene.addOrf(gene.orf_start, gene.orf_stop);
                            gene.setSize();
                            gene.setProteinSeq();
                            bh.pseq = gene.protein_seq;
                        }
                    }
                    alignmentText.append(" Score = ").append(bhsp.bitScore);
                    alignmentText.append(" bits (").append(bhsp.score);
                    alignmentText.append("), Expect = ").append(bhsp.evalue_base);

                    if (bhsp.evalue_exp > 0) {
                        if (bhsp.evalue_exp != 1) {
                            alignmentText.append("e+").append(bhsp.evalue_exp).append("\n");
                        }
                        else {
                            alignmentText.append("\n");
                        }
                    }
                    else {
                        alignmentText.append("e").append(bhsp.evalue_exp).append("\n");
                    }

                    alignmentText.append(" Identities = ").append(bhsp.identity).append("/");
                    alignmentText.append(bhsp.align_len).append(" (").append(nf.format(percent));
                    alignmentText.append("%), Positives = ").append(bhsp.positive).append("/").append(bhsp.align_len);

                    percent = bhsp.positive * 100.0 / bhsp.align_len;
                    alignmentText.append(" (").append(nf.format(percent)).append("%)");
                    qgstr = bhsp.qseq.split("-", -2);
                    hgstr = bhsp.hseq.split("-", -2);
                    ngaps = qgstr.length + hgstr.length - 2;

                    if (ngaps > 0) {
                        percent = ngaps * 100 / bhsp.align_len;
                        alignmentText.append(", Gaps = ").append(ngaps).append("/")
                                .append(bhsp.align_len).append(" (").append(percent).append("%)");
                    }

                    if (bprg.equals("blastn")) {
                        bhsp.hseq = bhsp.hseq.toLowerCase();
                        bhsp.qseq = bhsp.qseq.toLowerCase();

                        if ((bhsp.hit_frame < 0) && (bhsp.query_frame < 0)) {
                            alignmentText.append("\n Frame = ").append(bhsp.query_frame)
                                    .append(" / ").append(bhsp.hit_frame);
                        }
                        if ((bhsp.hit_frame < 0) && (bhsp.query_frame > 0)) {
                            tmpno = bhsp.query_from;
                            bhsp.query_from = bhsp.query_to;
                            bhsp.query_to = tmpno;
                            bhsp.hseq = GATUSequenceUtility.complement(bhsp.hseq);
                            bhsp.qseq = GATUSequenceUtility.complement(bhsp.qseq);
                            bhsp.midline = GATUSequenceUtility.reverse(bhsp.midline);

                            alignmentText.append("\n Frame = +").append(bhsp.query_frame)
                                    .append(" / ").append(bhsp.hit_frame);
                        }
                        if ((bhsp.hit_frame > 0) && (bhsp.query_frame < 0)) {
                            tmpno = bhsp.hit_from;
                            bhsp.hit_from = bhsp.hit_to;
                            bhsp.hit_to = tmpno;
                            bhsp.hseq = GATUSequenceUtility.complement(bhsp.hseq);
                            bhsp.qseq = GATUSequenceUtility.complement(bhsp.qseq);
                            bhsp.midline = GATUSequenceUtility.reverse(bhsp.midline);

                            alignmentText.append("\n Frame = ").append(bhsp.query_frame)
                                    .append(" / +").append(bhsp.hit_frame);
                        }
                        if ((bhsp.hit_frame > 0) && (bhsp.query_frame > 0)) {
                            alignmentText.append("\n Frame = +").append(bhsp.query_frame)
                                    .append(" / +").append(bhsp.hit_frame);
                        }
                    }
                    else {
                        if (bhsp.hit_frame < 0)
                            alignmentText.append("\n Frame = ").append(bhsp.hit_frame);
                        else
                            alignmentText.append("\n Frame = +").append(bhsp.hit_frame);
                    }
                    alignmentText.append("\n\n");

                    maxblanks = 0;
                    str = "" + bhsp.query_from;
                    maxblanks = Math.max(str.length(), maxblanks);
                    str = "" + bhsp.query_to;
                    maxblanks = Math.max(str.length(), maxblanks);
                    str = "" + bhsp.hit_from;
                    maxblanks = Math.max(str.length(), maxblanks);
                    str = "" + bhsp.hit_to;
                    maxblanks = Math.max(str.length(), maxblanks);

                    if (bhsp.qseq.length() > 60) {
                        qstart = bhsp.query_from;
                        hstart = bhsp.hit_from;
                        for (int spos=0; spos<bhsp.qseq.length(); spos+=60) {
                            if (spos+60 < bhsp.qseq.length()) {
                                subqseq = bhsp.qseq.substring(spos, spos+60);
                                subhseq = bhsp.hseq.substring(spos, spos+60);
                                submid = bhsp.midline.substring(spos, spos+60);
                                tmpstr = subqseq.split("-", -2);
                                ngaps = tmpstr.length - 1;
                                qstop = qstart + 59 - ngaps;
                                tmpstr = subhseq.split("-", -2);
                                ngaps = tmpstr.length - 1;

                                if (bh.totalnoexons > 1) {
                                    hstop = hstart + 59 - ngaps;
                                } else {
                                    hstop = hstart + 179 - ngaps;
                                }
                                str = "" + qstart;
                                str = getSpacing(maxblanks,  str);

                                alignmentText.append("Query: ");
                                alignmentText.append(qstart).append(str).append(" ").append(subqseq).append(" ").append(qstop).append("\n");
                                for (int h=0; h<maxblanks; h++) {
                                    alignmentText.append(" ");
                                }
                                alignmentText.append("        ").append(submid).append("\n");
                                alignmentText.append("Sbjct: ");

                                str = "" + hstart;
                                str = getSpacing(maxblanks,  str);
                                alignmentText.append(hstart).append(str).append(" ")
                                        .append(subhseq).append(" ").append(hstop).append("\n\n");
                                qstart = qstop + 1;
                                hstart = hstop + 1;
                            }
                            else {
                                str = "" + qstart;
                                str = getSpacing(maxblanks,  str);

                                subhseq = bhsp.hseq.substring(spos);
                                subqseq = bhsp.qseq.substring(spos);
                                submid = bhsp.midline.substring(spos);
                                alignmentText.append("Query: ");
                                alignmentText.append(qstart).append(" ").append(str).append(subqseq).append(" ")
                                        .append(bhsp.query_to).append("\n");

                                for (int h=0; h<maxblanks; h++) alignmentText.append(" ");

                                alignmentText.append("        ").append(submid).append("\n");
                                alignmentText.append("Sbjct: ");

                                str = "" + hstart;
                                str = getSpacing(maxblanks,  str);

                                alignmentText.append(hstart).append(" ")
                                        .append(str).append(subhseq).append(" ").append(bhsp.hit_to).append("\n\n");
                            }
                        }
                    }
                    else {
                        str = "" + bhsp.query_from;
                        str = getSpacing(maxblanks,  str);

                        alignmentText.append("Query: ");
                        alignmentText.append(bhsp.query_from).append(" ").append(str);
                        alignmentText.append(bhsp.qseq).append(" ").append(bhsp.query_to).append("\n");
                        for (int h=0; h<maxblanks; h++) alignmentText.append(" ");
                        alignmentText.append("        ").append(bhsp.midline).append("\n");
                        alignmentText.append("Sbjct: ");
                        alignmentText.append(bhsp.hit_from).append(" ");

                        str = "" + bhsp.hit_from;
                        str = getSpacing(maxblanks,  str);

                        alignmentText.append(str);
                        alignmentText.append(bhsp.hseq).append(" ").append(bhsp.hit_to).append("\n\n");
                    }
                }
                alignmentText.append("\n");
                bh.text = alignmentText;
                bhd.add(bh);
                currentMax++;
            }
        }


        gatu.setCurrentMaxGid(currentMax);
        gatu.setBlastQueryData(bqd, bc);
        gatu.mergeHits(bhd);
        gatu.setStats(statsTable8, statsTable9);

    }

    private String getSpacing(int maxblanks, String line){
        StringBuilder spacing = new StringBuilder();
        int len = 0;

        if (line.length() < maxblanks) {
            len = maxblanks - line.length();
        }
        for (int g=0; g<len; g++) {
            spacing.append(" ");
        }
        return spacing.toString();
    }

}