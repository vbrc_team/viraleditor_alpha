package Annotator.utility;

public final class GATUSequenceUtility {

    /**
     * Define the dna sequence of a gene
     * @param genome_seq the genome sequence
     * @param ostart the start position of the gene (offset 1)
     * @param ostop the start position of the gene (offset 1)
     * @param strand the strand the gene is on (if "-" then start>stop)
     * @return the nucleotide sequence of the gene
     */
    public static String setDNASeq(String genome_seq, int ostart, int ostop,
                                   String strand) {
        String temp;
        String ntseq = "";
        boolean plus;
        int start, stop;
        if (strand.equals("-")) {
            plus = false;
            start = ostop;
            stop = ostart;
        } else {
            plus = true;
            start = ostart;
            stop = ostop;
        }
        if (start <= stop) {
            if (stop > genome_seq.length()) stop = genome_seq.length();
            temp = genome_seq.substring(start-1, stop);
            ntseq = ntseq + temp;
        } else {
            if (start <= genome_seq.length()) {
                temp = genome_seq.substring(start-1);
                ntseq = ntseq + temp;
            }
            if (stop <= genome_seq.length()) {
                temp = genome_seq.substring(0, stop);
                ntseq = ntseq + temp;
            }
        }
        if (!plus) {
            ntseq = complement(ntseq);
        }
        return ntseq.toUpperCase();
    }

    /**
     * reverse a string
     */
    public static String reverse(String str) {
        StringBuffer temp;
        int len;
        len = str.length();
        temp = new StringBuffer(len);
        len--;
        for (int i=len; i>=0; i--) {
            temp.append(str.charAt(i));
        }
        return temp.toString();
    }

    /**
     * Make the complement of a dna seq
     * @param plusStr the 5'-3' dna seq
     * @return the 3'-5' dna seq
     */
    public static String complement(String plusStr) {
        StringBuffer temp;
        int len;

        len = plusStr.length();
        temp = new StringBuffer(len);
        len--;
        for (int i=len; i>=0; i--) {
            char letter = Character.toUpperCase(plusStr.charAt(i));
            switch(letter) {
                case 'A':
                    temp.append('t');
                    break;
                case 'C':
                    temp.append('g');
                    break;
                case 'G':
                    temp.append('c');
                    break;
                case 'T':
                    temp.append('a');
                    break;
                case 'U':
                    temp.append('a');
                    break;
                case 'M':
                    temp.append('k');
                    break;
                case 'R':
                    temp.append('y');
                    break;
                case 'W':
                    temp.append('w');
                    break;
                case 'S':
                    temp.append('s');
                    break;
                case 'Y':
                    temp.append('r');
                    break;
                case 'K':
                    temp.append('m');
                    break;
                case 'V':
                    temp.append('b');
                    break;
                case 'H':
                    temp.append('d');
                    break;
                case 'D':
                    temp.append('h');
                    break;
                case 'B':
                    temp.append('v');
                    break;
                case 'X':
                    temp.append('x');
                    break;
                case 'N':
                    temp.append('x');
                    break;
                default:
                    temp.append(plusStr.charAt(i));
            }
        }
        return temp.toString();
    }

    /**
     * Set the protein sequence based on a given dna sequence
     * @param dna_seq the dna sequence
     * @param frame if >0 3'-5', else 5'-3'
     * @return the protein sequence
     */
    public static String makeProtein(String dna_seq, int frame) {
        String str;
        StringBuilder protein_seq = new StringBuilder();
        int size;
        int remainder;
        int ratio;
        int x;

        size = dna_seq.length();
        ratio = size / 3;
        remainder = size - (ratio * 3);
        if (remainder == 0) {
            x = 0;
        } else {
            if (frame > 0) x = 0;
            else x = remainder;
        }
        for (int i=x; i<size; i+=3) {
            if ((i+3) <= size) {
                str = dna_seq.substring(i,i+3);
                str = str.toUpperCase();
                if (str.equals("AAA")) {
                    protein_seq.append("K"); continue;}
                if (str.equals("AAC")) {
                    protein_seq.append("N"); continue;}
                if (str.equals("AAG")) {
                    protein_seq.append("K"); continue;}
                if (str.equals("AAT")) {
                    protein_seq.append("N"); continue;}
                if (str.equals("ACA")) {
                    protein_seq.append("T"); continue;}
                if (str.equals("ACC")) {
                    protein_seq.append("T"); continue;}
                if (str.equals("ACG")) {
                    protein_seq.append("T"); continue;}
                if (str.equals("ACT")) {
                    protein_seq.append("T"); continue;}
                if (str.equals("AGA")) {
                    protein_seq.append("R"); continue;}
                if (str.equals("AGC")) {
                    protein_seq.append("S"); continue;}
                if (str.equals("AGG")) {
                    protein_seq.append("R"); continue;}
                if (str.equals("AGT")) {
                    protein_seq.append("S"); continue;}
                if (str.equals("ATA")) {
                    protein_seq.append("I"); continue;}
                if (str.equals("ATC")) {
                    protein_seq.append("I"); continue;}
                if (str.equals("ATG")) {
                    protein_seq.append("M"); continue;}
                if (str.equals("ATT")) {
                    protein_seq.append("I"); continue;}
                if (str.equals("CAA")) {
                    protein_seq.append("Q"); continue;}
                if (str.equals("CAC")) {
                    protein_seq.append("H"); continue;}
                if (str.equals("CAG")) {
                    protein_seq.append("Q"); continue;}
                if (str.equals("CAT")) {
                    protein_seq.append("H"); continue;}
                if (str.equals("CCA")) {
                    protein_seq.append("P"); continue;}
                if (str.equals("CCC")) {
                    protein_seq.append("P"); continue;}
                if (str.equals("CCG")) {
                    protein_seq.append("P"); continue;}
                if (str.equals("CCT")) {
                    protein_seq.append("P"); continue;}
                if (str.equals("CGA")) {
                    protein_seq.append("R"); continue;}
                if (str.equals("CGC")) {
                    protein_seq.append("R"); continue;}
                if (str.equals("CGG")) {
                    protein_seq.append("R"); continue;}
                if (str.equals("CGT")) {
                    protein_seq.append("R"); continue;}
                if (str.equals("CTA")) {
                    protein_seq.append("L"); continue;}
                if (str.equals("CTC")) {
                    protein_seq.append("L"); continue;}
                if (str.equals("CTG")) {
                    protein_seq.append("L"); continue;}
                if (str.equals("CTT")) {
                    protein_seq.append("L"); continue;}
                if (str.equals("GAA")) {
                    protein_seq.append("E"); continue;}
                if (str.equals("GAC")) {
                    protein_seq.append("D"); continue;}
                if (str.equals("GAG")) {
                    protein_seq.append("E"); continue;}
                if (str.equals("GAT")) {
                    protein_seq.append("D"); continue;}
                if (str.equals("GCA")) {
                    protein_seq.append("A"); continue;}
                if (str.equals("GCC")) {
                    protein_seq.append("A"); continue;}
                if (str.equals("GCG")) {
                    protein_seq.append("A"); continue;}
                if (str.equals("GCT")) {
                    protein_seq.append("A"); continue;}
                if (str.equals("GGA")) {
                    protein_seq.append("G"); continue;}
                if (str.equals("GGC")) {
                    protein_seq.append("G"); continue;}
                if (str.equals("GGG")) {
                    protein_seq.append("G"); continue;}
                if (str.equals("GGT")) {
                    protein_seq.append("G"); continue;}
                if (str.equals("GTA")) {
                    protein_seq.append("V"); continue;}
                if (str.equals("GTC")) {
                    protein_seq.append("V"); continue;}
                if (str.equals("GTG")) {
                    protein_seq.append("V"); continue;}
                if (str.equals("GTT")) {
                    protein_seq.append("V"); continue;}
                if (str.equals("TAA")) {
                    protein_seq.append("X"); continue;}
                if (str.equals("TAC")) {
                    protein_seq.append("Y"); continue;}
                if (str.equals("TAG")) {
                    protein_seq.append("X"); continue;}
                if (str.equals("TAT")) {
                    protein_seq.append("Y"); continue;}
                if (str.equals("TCA")) {
                    protein_seq.append("S"); continue;}
                if (str.equals("TCC")) {
                    protein_seq.append("S"); continue;}
                if (str.equals("TCG")) {
                    protein_seq.append("S"); continue;}
                if (str.equals("TCT")) {
                    protein_seq.append("S"); continue;}
                if (str.equals("TGA")) {
                    protein_seq.append("U"); continue;}
                if (str.equals("TGC")) {
                    protein_seq.append("C"); continue;}
                if (str.equals("TGG")) {
                    protein_seq.append("W"); continue;}
                if (str.equals("TGT")) {
                    protein_seq.append("C"); continue;}
                if (str.equals("TTA")) {
                    protein_seq.append("L"); continue;}
                if (str.equals("TTC")) {
                    protein_seq.append("F"); continue;}
                if (str.equals("TTG")) {
                    protein_seq.append("L"); continue;}
                if (str.equals("TTT")) {
                    protein_seq.append("F"); continue;}
                protein_seq.append("X");
            }
        }
        return protein_seq.toString();
    }
}
