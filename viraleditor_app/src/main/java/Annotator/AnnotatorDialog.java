package Annotator;

import java.io.IOException;

import Shared.interchange.Interchange;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.text.Font;
import javafx.scene.text.Text;
import javafx.scene.text.TextFlow;
import javafx.stage.Stage;

public class AnnotatorDialog {

    private AnnotatorComposer composer;
    private final Font font = new Font(14);

    public void initComposer(AnnotatorComposer composer) {
        this.composer = composer;
    }

    public static AnnotatorDialog openWindow(Interchange A, Interchange B) throws IOException {
            FXMLLoader loader = new FXMLLoader(AnnotatorDialog.class.getClassLoader().getResource("AnnotatorDialog.fxml"));
            Parent root = loader.load();
            AnnotatorDialog controller = loader.getController();
            controller.initComposer(new AnnotatorComposer());

            controller.composer.setUp();

            Stage popup = new Stage();
            popup.setTitle("Annotation");
            popup.setScene(new Scene(root));
            popup.show();

            controller.setUp();

            return controller;
    }

    public void setUp(){
        // fill tables and columns
        setBlastText("aaaa");
        setNeedleText("What is Lorem Ipsum?\n" +
                "Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.\n" +
                "\n" +
                "Why do we use it?\n" +
                "It is a long established fact that a reader will be distracted by the readable content of a page when looking at its layout. The point of using Lorem Ipsum is that it has a more-or-less normal distribution of letters, as opposed to using 'Content here, content here', making it look like readable English. Many desktop publishing packages and web page editors now use Lorem Ipsum as their default model text, and a search for 'lorem ipsum' will uncover many web sites still in their infancy. Various versions have evolved over the years, sometimes by accident, sometimes on purpose (injected humour and the like).\n" +
                "\n" +
                "\n" +
                "Where does it come from?\n" +
                "Contrary to popular belief, Lorem Ipsum is not simply random text. It has roots in a piece of classical Latin literature from 45 BC, making it over 2000 years old. Richard McClintock, a Latin professor at Hampden-Sydney College in Virginia, looked up one of the more obscure Latin words, consectetur, from a Lorem Ipsum passage, and going through the cites of the word in classical literature, discovered the undoubtable source. Lorem Ipsum comes from sections 1.10.32 and 1.10.33 of \"de Finibus Bonorum et Malorum\" (The Extremes of Good and Evil) by Cicero, written in 45 BC. This book is a treatise on the theory of ethics, very popular during the Renaissance. The first line of Lorem Ipsum, \"Lorem ipsum dolor sit amet..\", comes from a line in section 1.10.32.");
        BlastBox.setLineSpacing(1.5);
        NeedleBox.setLineSpacing(1.5);
    }



    private void setBlastText(String message){
        Text text = new Text(message);
        text.setFont(font);
        BlastBox.getChildren().clear();
        BlastBox.getChildren().add(text);
    }

    private void setNeedleText(String message){
        Text text = new Text(message);
        text.setFont(font);
        NeedleBox.getChildren().clear();
        NeedleBox.getChildren().add(text);
    }


    @FXML
    private TextFlow BlastBox;
    @FXML
    private TextFlow NeedleBox;

}
