package Alignment;

import Shared.AbstractDialog;


import Shared.AbstractWindow;


import javafx.collections.ObservableList;
import javafx.scene.control.TableView;
import javafx.scene.control.TableColumn;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.input.MouseEvent;
import javafx.event.EventHandler;
import javafx.scene.text.Text;
import javafx.scene.control.TitledPane;
import javafx.scene.control.TableSelectionModel;

import javafx.fxml.FXML;


public class SearchFuzzyMotifDialog extends AbstractDialog<SearchFuzzyMotifComposer>{
	
	@FXML
	TitledPane Top;
	@FXML
	TitledPane Bottom;
	@FXML
	Text searchedSequence;
	private ObservableList dataTop;
	private ObservableList dataBot;
	
	
	@Override
	public void mountChildPerformances() {
		
	}
	
	@Override
	public void initializeDialog(){
		stage.setTitle("Search Fuzzy Motif");
		searchedSequence.setText(composer.getName());
		this.dataTop = composer.getDataTop();
		this.dataBot = composer.getDataBot();
		initTopSequences();
		initBottomSequences();
		composer.updateSearchResults();
		stage.show();
	}
	
	
	public void initTopSequences(){
		TableView topTable = composer.initSequences();
		TableSelectionModel topModel = topTable.getSelectionModel();
		topTable.setItems(this.dataTop);
		Top.setContent(topTable);
		topTable.addEventFilter(MouseEvent.MOUSE_CLICKED, new EventHandler<MouseEvent>() {
			public void handle(MouseEvent event) {
				if (topModel.getSelectedIndex() != -1) {
					int selected = topModel.getSelectedIndex();
					String[] selectData = (dataTop.get(selected).toString().split(", "));
					composer.moveToMatch(selectData[0], selectData[4]);
				}
			}
		});
					
		
	}
	
	public void initBottomSequences() {
		TableView botTable = composer.initSequences();
		TableSelectionModel botModel = botTable.getSelectionModel();
		botTable.setItems(dataBot);
		Bottom.setContent(botTable);
		botTable.addEventFilter(MouseEvent.MOUSE_CLICKED, new EventHandler<MouseEvent>() {
			public void handle(MouseEvent event) {
				if (botModel.getSelectedIndex() != -1) {
					int selected = botModel.getSelectedIndex();
					String[] selectData = (dataBot.get(selected).toString().split(", "));
					composer.moveToMatch(selectData[0], selectData[4]);
				}

				
			
				
			}
			
			
		});
	}
	
	@FXML
	public void save() {
		composer.save();
	}
}
