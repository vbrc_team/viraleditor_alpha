package Alignment;

import Alignment.structures.AlignmentSequence;
import Shared.AbstractComposer;
import javafx.collections.ObservableList;

/**
 * Handles the updating of current selection and unselected lists
 */
public class ShowSequencesComposer extends AbstractComposer<ShowSequencesDialog> {
    private ObservableList<AlignmentSequence> sequences;
    private SequencesComposer sequencesComposer;

    public void setSequencesComposer(SequencesComposer composer) {
        this.sequencesComposer = composer;
        dialog.setList(sequencesComposer);
    }

    public void selectAll() {
        for (AlignmentSequence sequence : sequences) {
            if (sequence.isHidden()) {
                sequence.reveal();
            }
        }
    }
}