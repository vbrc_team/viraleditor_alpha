package Alignment;

import Alignment.structures.AlignmentSequence;
import Alignment.structures.Orderable;

public class SequenceEditorMode extends SequencesComposer implements Orderable<AlignmentSequence>{
	 
	
	private boolean editMouseMode;
	private int lastStop;
	    
    public SequenceEditorMode() {
    	editMouseMode = false;
    	lastStop = -1;
    }
    
    
    /**
     * turns edit mouse mode on and off, allowing for insertions/deletions of gaps with the mouse
     */
    
    public void toggleEditMouseMode() {
    	editMouseMode = !editMouseMode;
    }
    
    /**
     * sets the last stop variable to an impossible value, so we know a new dragging instance has started.
     */
    public void resetLastStop() {
    	lastStop = -1;
    }
    
    /**
     * if the last stop is greater than the previous, and not -1, it will add a gap of size 1.
     * if the last stop is less than the previous, and not -1, it will remove 1 gap.
     * @param stop
     */
    
    public void updateLastStop(int stop) {
    	if (editMouseMode == true) {
    		if (lastStop == -1) {
    			lastStop = stop;
    		}
    		
    		if (stop > lastStop){
    			insertGaps(1);
    		}
    		else if (stop < lastStop) {
    			removeGaps();
    		}
    		else {
    		
    		}
    		lastStop = stop;
    	}
    	else {
    		
    	}
    }
    

	
}
