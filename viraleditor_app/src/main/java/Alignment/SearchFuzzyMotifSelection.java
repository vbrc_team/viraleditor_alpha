package Alignment;

import java.io.IOException;
import java.util.Objects;

import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.*;
import javafx.fxml.FXML;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyEvent;
import javafx.scene.control.*;


public class SearchFuzzyMotifSelection {

	public static String subString = "";
	public static String misMatch = "0";
	public static String[] subStrAndMismatch = new String[2];
	public static String[] subSeqAndMismatch = new String[2];
	
	@FXML
	private Button Search;
	@FXML
	private Button Cancel;
	
	public static String[] openPopUp() throws IOException {
        subString = "";
        misMatch = "0";
		subStrAndMismatch[0] = "";
		subStrAndMismatch[1] = "0";
		subSeqAndMismatch[0] = "";
		subSeqAndMismatch[1] = "0";
		FXMLLoader loader = new FXMLLoader(SearchFuzzyMotifSelection.class.getClassLoader().getResource("SearchFuzzyMotifSelection.fxml"));
		Parent root = loader.load(Objects.requireNonNull(SearchFuzzyMotifSelection.class.getClassLoader().getResource("SearchFuzzyMotifSelection.fxml")).openStream());
		Stage popup = new Stage();
        popup.setTitle("Enter subsequence and desired number of mismatches");
        popup.setScene(new Scene(root));
        popup.showAndWait();
        

        
	
        return subStrAndMismatch;
	}
	
	
	@FXML
	public void updateSubSequence(KeyEvent event){
		if (event.getCode() == (KeyCode.ENTER) && this.subString.length() >=1) {
			search();
		}
		if (event.getCode() == (KeyCode.BACK_SPACE) && this.subString.length() >=1) {
			this.subString = this.subString.substring(0, this.subString.length()-1);
			subSeqAndMismatch[0] = this.subString;
		}
		else if (Character.isLetter(event.getCode().getChar().charAt(0))) {
			subString += event.getCode().getChar();
			subSeqAndMismatch[0] = this.subString;
		}
		
	}
	
	@FXML
	public void updateMismatch(KeyEvent event) {
		if (event.getCode() == (KeyCode.ENTER)) {
			search();
		}
		if (event.getCode() == (KeyCode.BACK_SPACE) && this.misMatch.length() >=1) {
			this.misMatch = this.misMatch.substring(0, this.misMatch.length()-1);
			subSeqAndMismatch[1] = this.misMatch;
		}
		else if (Character.isDigit(event.getCode().getChar().charAt(0))) {
			this.misMatch += event.getCode().getChar();
			subSeqAndMismatch[1] = this.misMatch;
		}
		
	}
	
	@FXML
	public void search() {
		subStrAndMismatch = subSeqAndMismatch;
		Stage stage = (Stage) Search.getScene().getWindow();
        stage.close();  
	}
	
	@FXML
	public void cancel() {
		Stage stage = (Stage) Cancel.getScene().getWindow();
		stage.close();
	}
        
}
