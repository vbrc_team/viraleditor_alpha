package Alignment;



import Shared.AbstractDialog;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.ColorPicker;
import javafx.scene.control.TextArea;
import javafx.scene.control.TextField;
import javafx.stage.Stage;

public class CommentDialog extends AbstractDialog<CommentComposer> {
	
    @FXML
    private ColorPicker textColor;

    @FXML
    private ColorPicker backgroundColor;

    @FXML
    private TextArea commentString;

    @FXML
    private TextField title;

    @FXML
    private Button confirm;

    @FXML
    private Button cancel;
    
    @FXML
    void cancelComment(ActionEvent event) {
    	Stage stage = (Stage) cancel.getScene().getWindow();
        stage.close();
    }

    @FXML
    void confirmComment(ActionEvent event) {
    	Stage stage = (Stage) confirm.getScene().getWindow();
        stage.close();
    }
    
	public void initComposer(CommentComposer composer) {
        this.composer = composer;
    }
	
	
    @Override
    public void initializeDialog() {
    	stage.setTitle("Add Comment");
    	Comment original = this.composer.getOriginal();
    	if (original != null){
    		title.setText(original.getTitle());
    		commentString.setText(original.getCommentText());
    		textColor.setValue(original.getTextColor());
    		backgroundColor.setValue(original.getBackgroundColor());
    	}
    	
        
    }    
    

	@Override
	public void mountChildPerformances() {
		// TODO Auto-generated method stub
		
	}

}
