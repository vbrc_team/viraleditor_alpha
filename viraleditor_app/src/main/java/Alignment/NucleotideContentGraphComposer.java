package Alignment;

import java.util.List;

import Shared.AbstractComposer;
import Shared.interchange.Reference;
import Shared.interchange.Sequence;
import javafx.scene.chart.XYChart;
import javafx.scene.chart.XYChart.Data;
import javafx.scene.chart.XYChart.Series;

public class NucleotideContentGraphComposer extends AbstractComposer<NucleotideContentGraphDialog> {
    private List<Reference> sequences;
	private char[] searchParameters;
	
    public NucleotideContentGraphComposer(List<Reference> sequences, char[] searchParameters) {
		this.sequences = sequences;
		this.searchParameters = searchParameters;
	}
	
	public void setSearchParameters(char[] searchParameters) {
		this.searchParameters = searchParameters;
	}

	
	public XYChart.Series<Number, Number> calculateLineChartSeries() {
		
		XYChart.Series<Number, Number> series = new Series<>();
		double windowSum;
        int start;

        for (int i = 0; i < sequences.size(); i++) {
            Sequence seq = sequences.get(i).getOrigin();
            for (start = 0; start + dialog.getWindowSize() <= seq.getSequence().length(); start += dialog.getStepSize()) {
                windowSum = calculateIntermediateNucleotideGraph(seq, start);
                double x = start + ((dialog.getWindowSize() - 1) / 2.0);
                double y = windowSum / dialog.getWindowSize();

                //Take care of odd/even window size. even -> x = x+1, odd x = x+0.5
                if (dialog.getWindowSize() % 2 != 0) {
                    series.getData().add(new Data<>(x + 1, y));
                } else {
                    series.getData().add(new Data<>(x + 1.5, y));
                }
            }
        }
        
        return series;
	}

    protected double calculateIntermediateNucleotideGraph(Sequence seq, int start) {
	    int count = 0;
        String sequence = seq.getSequence().toLowerCase();
	
	    for (int i = start; i < start + dialog.getWindowSize(); i++) {
	        for (int j = 0; j < searchParameters.length; j++) {
	            if ((searchParameters[j] == 'a') || (searchParameters[j] == 'A')) {
                    if ((sequence.charAt(i) == 'a') || (sequence.charAt(i) == 'A')) {
	                    count++;
	                }
	            } else if ((searchParameters[j] == 'c') || (searchParameters[j] == 'C')) {
                    if ((sequence.charAt(i) == 'c') || (sequence.charAt(i) == 'C')) {
	                    count++;
	                }
	            } else if ((searchParameters[j] == 'g') || (searchParameters[j] == 'G')) {
                    if ((sequence.charAt(i) == 'g') || (sequence.charAt(i) == 'G')) {
	                    count++;
	                }
	            } else if ((searchParameters[j] == 't') || (searchParameters[j] == 'T')) {
                    if ((sequence.charAt(i) == 't') || (sequence.charAt(i) == 'T')) {
	                    count++;
	                }
	            }
	        }
	    }
	
	    return count;
	}
}
