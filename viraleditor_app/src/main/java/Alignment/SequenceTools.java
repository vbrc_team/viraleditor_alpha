package Alignment;

import java.util.HashMap;
import java.util.Map;

/**
 * This class defines a bunch of convenience and tool methods for  dealing with
 * and editing sequences.
 *
 * Taken from legacy tool
 *
 * @author Ryan Brodie
 * @version 1.0
 */
public class SequenceTools
{
    //~ Static fields/initializers /////////////////////////////////////////////

    private static String[] m_aminoAcids =
            {
                    "F", "F", "L", "L", "Y", "Y", "*", "*", // last 2 used to be X
                    "L", "L", "L", "L", "H", "H", "Q", "Q", "I", "I", "I", "M", "N", "N",
                    "K", "K", "V", "V", "V", "V", "D", "D", "E", "E", "S", "S", "S", "S",
                    "C", "C", "*", "W", //2nd last used to be U
                    "P", "P", "P", "P", "R", "R", "R", "R", "T", "T", "T", "T", "S", "S",
                    "R", "R", "A", "A", "A", "A", "G", "G", "G", "G", "F", "L", "S", "S",
                    "S", "S", "S", "S", "S", "S", "S", "S", "S", "Y", "C", "L", "L", "L",
                    "L", "L", "L", "L", "L", "L", "L", "L", "P", "P", "P", "P", "P", "P",
                    "P", "P", "P", "P", "P", "H", "Q", "R", "R", "R", "R", "R", "R", "R",
                    "R", "R", "R", "R", "I", "I", "I", "I", "T", "T", "T", "T", "T", "T",
                    "T", "T", "T", "T", "T", "N", "K", "S", "R", "V", "V", "V", "V", "V",
                    "V", "V", "V", "V", "V", "V", "A", "A", "A", "A", "A", "A", "A", "A",
                    "A", "A", "A", "D", "E", "G", "G", "G", "G", "G", "G", "G", "G", "G",
                    "G", "G", "L", "L", "L", "R", "R", "R"
            };
    private static String[] m_codons =
            {
                    "TTT", "TTC", "TTA", "TTG", "TAT", "TAC", "TAA", "TAG", "CTT", "CTC",
                    "CTA", "CTG", "CAT", "CAC", "CAA", "CAG", "ATT", "ATC", "ATA", "ATG",
                    "AAT", "AAC", "AAA", "AAG", "GTT", "GTC", "GTA", "GTG", "GAT", "GAC",
                    "GAA", "GAG", "TCT", "TCC", "TCA", "TCG", "TGT", "TGC", "TGA", "TGG",
                    "CCT", "CCC", "CCA", "CCG", "CGT", "CGC", "CGA", "CGG", "ACT", "ACC",
                    "ACA", "ACG", "AGT", "AGC", "AGA", "AGG", "GCT", "GCC", "GCA", "GCG",
                    "GGT", "GGC", "GGA", "GGG", "TTY", "TTR", "TCN", "TCR", "TCY", "TCK",
                    "TCM", "TCS", "TCW", "TCB", "TCD", "TCH", "TCV", "TAY", "TGY", "CTN",
                    "CTR", "CTY", "CTK", "CTM", "CTS", "CTW", "CTB", "CTD", "CTH", "CTV",
                    "CCN", "CCR", "CCY", "CCK", "CCM", "CCS", "CCW", "CCB", "CCD", "CCH",
                    "CCV", "CAY", "CAR", "CGN", "CGR", "CGY", "CGK", "CGM", "CGS", "CGW",
                    "CGB", "CGD", "CGH", "CGV", "ATH", "ATY", "ATM", "ATW", "ACN", "ACR",
                    "ACY", "ACK", "ACM", "ACS", "ACW", "ACB", "ACD", "ACH", "ACV", "AAY",
                    "AAR", "AGY", "AGR", "GTN", "GTR", "GTY", "GTK", "GTM", "GTS", "GTW",
                    "GTB", "GTD", "GTH", "GTV", "GCN", "GCR", "GCY", "GCK", "GCM", "GCS",
                    "GCW", "GCB", "GCD", "GCH", "GCV", "GAY", "GAR", "GGN", "GGR", "GGY",
                    "GGK", "GGM", "GGS", "GGW", "GGB", "GGD", "GGH", "GGV", "YTR", "YTA",
                    "YTG", "MGR", "MGA", "MGG"
            };

    private static final Map<String, String> CODONS_TO_AA = new HashMap<>();
    static {
        for (int i = 0; i < m_codons.length; i++) {
            CODONS_TO_AA.put(m_codons[i], m_aminoAcids[i]);
        }
    }

    //~ Methods ////////////////////////////////////////////////////////////////

    /**
     * This method gets the amino acid name of the AA that is associated with
     * the specified codon.
     *
     * @param codon String of 3 nucleotides
     *
     * @return amino acid name of inputted codon
     */
    public static String getAminoAcid(String codon)
    {
        codon = codon.toUpperCase();

        for (int i = 0; i < m_codons.length; i++) {
            if (codon.equals(m_codons[i])) {
                return m_aminoAcids[i];
            }
        }

        return "X";
    }

    /**
     * get the ungapped version of the given string
     *
     * @param dna a dna string
     *
     * @return the same string, but with no gaps
     */
    public final static StringBuilder getUngappedBuffer(String dna)
    {
        // faster execution
        StringBuilder buff = new StringBuilder(dna.replaceAll("-", ""));

        return buff;
    }

    /**
     * Returns a pretty printed amino acid string based on the translation of the
     * DNA sequence.
     * <p>
     * The translation is started at the reading frame. Gaps are ignored during the
     * translation.
     * <p>
     * Each amino acid is written using the standard ambiguity codes. The two
     * character prefix '=>' is added to each amino acid. Gaps are written as as the
     * single character '='. The string pretty printed string will have a final
     * '=>'. The start of the returned sequence will have frame - 1 spaces.
     * <p>
     * If dna is shorter than 3 + frame - 1 characters "=>" is returned.
     *
     * @param dna          a nucleotide sequence
     * @param readingFrame the readingFrame in {1,2,3}
     *
     * @return a pretty printed amino acid string
     * @throws IllegalArgumentException if (frame < 1 || frame > 3)
     */
    public static String getGappedAAString(String dna, int readingFrame) {
        StringBuilder ret = new StringBuilder();
        ret.append(getAAString(dna, readingFrame));
        readingFrame--;

        // space out
        for (int i = 1; i < ret.length(); ++i) {
            ret.insert(i, "=>");
            i += 2;
        }

        ret.insert(ret.length(), "=>");

        // reframe
        for (int i = readingFrame; i > 0; --i) {
            ret.insert(0, ' ');
        }

        for (int i = 0; i < dna.length(); ++i) {
            if (dna.charAt(i) == '-') {
                if (i > ret.length()) {
                    ret.append('-');
                } else {
                    ret.insert(i, '-');
                }
            }
        }

        for (int i = 1; i < (ret.length() - 1); ++i) {
            char c1 = ret.charAt(i - 1);
            char c2 = ret.charAt(i);
            char c3 = ret.charAt(i + 1);

            if (c2 == '-') {
                if (((c1 != ' ') && (c1 != '-') && (c1 != '>')) && ((c3 == '-') || (c3 == '>') || (c3 == '='))) {
                    ret.replace(i, i + 1, "=");
                }
            }
        }

        return ret.toString();
    }

    /**
     * Get the amino acid string for a given dna string.
     *
     * @param dna   The dna string (gapped or ungapped) to translate
     * @param frame The frame, in {1,2,3}
     *
     * @return the aa string
     * @throws IllegalArgumentException if (frame < 1 || frame > 3)
     */
    public static String getAAString(String dna, int frame) {
        if (frame < 1 || frame > 3) {
            throw new IllegalArgumentException(String.format("frame = %d is not in {1,2,3}", frame));
        }

        frame -= 1;

        if (dna.length() < 3 + frame) {
            return "";
        }

        dna = dna.substring(frame).replace("-", "");
        StringBuilder out = new StringBuilder();
        for (int i = 0; (i + 2) < dna.length(); i += 3) {
            String codon = dna.substring(i, i + 3);

            if (CODONS_TO_AA.containsKey(codon)) {
                out.append(CODONS_TO_AA.get(codon));
            }
        }

        return out.toString();
    }

    /**
     * get the dna complement for a character
     *
     * @param c the character to complement
     *
     * @return its complement
     */
    public static char getDNAComplement(char c)
    {
        if ((c == 'A') || (c == 'a')) {
            return ('T');
        } else if ((c == 'T') || (c == 't') || (c == 'U') || (c == 'u')) {
            return ('A');
        } else if ((c == 'C') || (c == 'c')) {
            return ('G');
        } else if ((c == 'G') || (c == 'g')) {
            return ('C');
        } else if ((c == 'M') || (c == 'm')) {
            return ('K');
        } else if ((c == 'R') || (c == 'r')) {
            return ('Y');
        } else if ((c == 'W') || (c == 'w')) {
            return ('W');
        } else if ((c == 'S') || (c == 's')) {
            return ('S');
        } else if ((c == 'Y') || (c == 'y')) {
            return ('R');
        } else if ((c == 'K') || (c == 'k')) {
            return ('M');
        } else if ((c == 'V') || (c == 'v')) {
            return ('B');
        } else if ((c == 'H') || (c == 'h')) {
            return ('D');
        } else if ((c == 'D') || (c == 'd')) {
            return ('H');
        } else if ((c == 'B') || (c == 'b')) {
            return ('V');
        } else if ((c == 'X') || (c == 'x')) {
            return ('X');
        } else if ((c == 'N') || (c == 'n')) {
            return ('N');
        } else {
            return (c);
        }
    }

    /**
     * This method converts sequence string to the corresponding complement
     * sequence
     *
     * @param seqString the string stands for a DNA sequence
     *
     * @return reversed the complement sequence of the input parameter
     */
    public static String getDNAComplement(String seqString)
    {
        StringBuilder reversed = new StringBuilder();

        synchronized (reversed) {
            for (int i = 0; i < seqString.length(); i++) {
                char c = seqString.charAt(i);

                if ((c == 'A') || (c == 'a')) {
                    reversed.append("T");
                } else if ((c == 'T') || (c == 't') || (c == 'U') ||
                        (c == 'u')) {
                    reversed.append("A");
                } else if ((c == 'C') || (c == 'c')) {
                    reversed.append("G");
                } else if ((c == 'G') || (c == 'g')) {
                    reversed.append("C");
                } else if ((c == 'M') || (c == 'm')) {
                    reversed.append("K");
                } else if ((c == 'R') || (c == 'r')) {
                    reversed.append("Y");
                } else if ((c == 'W') || (c == 'w')) {
                    reversed.append("W");
                } else if ((c == 'S') || (c == 's')) {
                    reversed.append("S");
                } else if ((c == 'Y') || (c == 'y')) {
                    reversed.append("R");
                } else if ((c == 'K') || (c == 'k')) {
                    reversed.append("M");
                } else if ((c == 'V') || (c == 'v')) {
                    reversed.append("B");
                } else if ((c == 'H') || (c == 'h')) {
                    reversed.append("D");
                } else if ((c == 'D') || (c == 'd')) {
                    reversed.append("H");
                } else if ((c == 'B') || (c == 'b')) {
                    reversed.append("V");
                } else if ((c == 'X') || (c == 'x')) {
                    reversed.append("X");
                } else if ((c == 'N') || (c == 'n')) {
                    reversed.append("N");
                } else {
                    reversed.append(c);
                }
            }
        }

        return reversed.toString();
    }
}

