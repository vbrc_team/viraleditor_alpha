package Alignment;

import java.util.List;
import java.util.Hashtable;
import java.util.ArrayList;
import java.io.IOException;

import org.biojava.nbio.core.alignment.matrices.SubstitutionMatrixHelper;

import Alignment.structures.DrawFrame;
import Alignment.structures.Element;
import Alignment.structures.ElementType;
import Alignment.structures.NamedElements;
import Alignment.structures.AlignmentSequence;
import Shared.AbstractDialog;
import javafx.animation.Animation;
import javafx.animation.AnimationTimer;
import javafx.animation.KeyFrame;
import javafx.animation.Timeline;
import javafx.application.Platform;
import javafx.beans.value.ObservableValue;
import javafx.fxml.FXML;
import javafx.geometry.Point2D;
import javafx.geometry.VPos;
import javafx.scene.canvas.Canvas;
import javafx.scene.canvas.GraphicsContext;
import javafx.scene.control.ScrollBar;
import javafx.scene.control.Tooltip;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.Pane;
import javafx.scene.paint.Color;
import javafx.scene.paint.Paint;
import javafx.scene.text.Font;
import javafx.scene.text.Text;
import javafx.stage.WindowEvent;
import javafx.util.Duration;

public class SequencesDialog extends AbstractDialog<SequenceEditorMode> {
    private static final double FONT_SIZE = 15.0;
    private double FONT_WIDTH;
    private double FONT_HEIGHT = FONT_SIZE;
    private double columnSelectStart;
    private Timeline skipRight;
    private Timeline skipLeft;
    private boolean dragging;
    private Tooltip sequencePosition;
    private String currentColourScheme = "Default";
    private boolean[] customColours = new boolean[]
    		{false, false, false, false, false,false, false, false, false, false,false, false, false, false, false,false, false, false, false, false};

    private static final Paint BRIGHT_RED = Color.rgb(255, 100, 100);
    private static final Paint BRIGHT_GREEN = Color.rgb(100, 255, 100);
    /**
     * Elements that have been drawn.
     */
    private DrawFrame drawnFrame;

    /**
     * Frame to draw next.
     */
    private DrawFrame nextFrame;

    /**
     * True when there are new drawnElements to draw and false otherwise.
     */
    private boolean newFrame;

    /**
     * Reference to this that draw can use to synchronize on.
     */
    private final SequencesDialog self = this;

    /**
     * Draws drawnElements every frame that has new drawnElements.
     */
    private AnimationTimer draw = new AnimationTimer() {
        @Override
        public void handle(long now) {
            synchronized (self) {
                if (newFrame) {
                    newFrame = false;
                    drawnFrame = nextFrame;

                    resetCanvas(sequences);
                    resetCanvas(sequenceNames);
                    resetCanvas(sequencesTopScale);
                    resetCanvas(sequencesBottomScale);
                    resetCanvas(namesTopScale);
                    resetCanvas(namesBottomScale);
                    updateScrollBar();
                    drawSequences(drawnFrame.getElements());
                    drawNames(drawnFrame.getElements());
                    drawScale(drawnFrame.getScale());
                }
            }
        }
    };

    @FXML
    private Pane sequenceNamesParent;

    @FXML
    private Canvas sequenceNames;

    @FXML
    private Canvas namesTopScale;

    @FXML
    private Canvas namesBottomScale;

    @FXML
    private ScrollBar namesScroll;

    @FXML
    private Pane sequencesParent;

    @FXML
    private Canvas sequences;

    @FXML
    private Canvas sequencesTopScale;

    @FXML
    private Canvas sequencesBottomScale;

    @FXML
    private ScrollBar horizontalScroll;

    @FXML
    private ScrollBar verticalScroll;

    @FXML
    void initialize() {
        assert sequenceNamesParent != null
                : "fx:id=\"sequenceNamesParent\" was not injected: check your FXML file 'SequencesDialog.fxml'.";
        assert sequenceNames != null
                : "fx:id=\"sequenceNames\" was not injected: check your FXML file 'SequencesDialog.fxml'.";
        assert namesTopScale != null
                : "fx:id=\"namesTopScale\" was not injected: check your FXML file 'SequencesDialog.fxml'.";
        assert namesScroll != null
                : "fx:id=\"namesScroll\" was not injected: check your FXML file 'SequencesDialog.fxml'.";
        assert sequencesParent != null
                : "fx:id=\"sequencesParent\" was not injected: check your FXML file 'SequencesDialog.fxml'.";
        assert sequencesTopScale != null
                : "fx:id=\"sequencesTopScale\" was not injected: check your FXML file 'SequencesDialog.fxml'.";
        assert sequences != null : "fx:id=\"sequences\" was not injected: check your FXML file 'SequencesDialog.fxml'.";
        assert horizontalScroll != null
                : "fx:id=\"horizontalScroll\" was not injected: check your FXML file 'SequencesDialog.fxml'.";
        assert verticalScroll != null
                : "fx:id=\"verticalScroll\" was not injected: check your FXML file 'SequencesDialog.fxml'.";

        addScrollBarRedrawEvents();

        sequencesParent.widthProperty().addListener(this::sequencesWidthChanged);
        sequencesParent.heightProperty().addListener(this::sequenceHeightChanged);
        sequenceNamesParent.widthProperty().addListener(this::namesWidthChanged);
        sequenceNamesParent.heightProperty().addListener(this::namesHeightChanged);

        // Window fails to load if assigned 0 and textWidth("A") won't return correct
        // value until after custom fonts load.
        FONT_WIDTH = 1;

        skipRight = new Timeline(new KeyFrame(Duration.millis(50), evt -> {
            int end = 10 + (int) Math.floor((horizontalScroll.getValue() + sequences.getWidth()) / FONT_WIDTH);
            shiftColumn(10);
            composer.selectRange((int) Math.floor(columnSelectStart / FONT_WIDTH), end);
        }));
        skipRight.setCycleCount(Animation.INDEFINITE);

        skipLeft = new Timeline(new KeyFrame(Duration.millis(50), evt -> {
            int start = Math.max(0, getLeftmostColumn() - 10);
            int end = (int) Math.max(0, Math.floor(columnSelectStart / FONT_WIDTH));
            shiftColumn(-10);
            composer.selectRange(start, end);
        }));
        skipLeft.setCycleCount(Animation.INDEFINITE);

        sequenceNames.addEventHandler(MouseEvent.MOUSE_CLICKED, this::selectSequence);

        sequences.setOnMouseDragged(this::onDragged);
        sequences.setOnMouseDragged(this::onDragged);
        sequences.setOnMousePressed(this::deselectColumns);
        sequences.setOnMouseReleased(this::stopDrag);

        verticalScroll.setBlockIncrement(FONT_HEIGHT);
        verticalScroll.setUnitIncrement(FONT_HEIGHT);

        initPositionTooltip();
    }

    /**
     * Initializes the position tooltip.
     *
     * @throws UnsupportedOperationException if position tooltip has been created
     */
    private void initPositionTooltip() {
        if (sequencePosition != null) {
            throw new UnsupportedOperationException("Position tooltip was already initialized");
        }

        sequencePosition = new Tooltip();
        sequencePosition.setOnShown(this::positionToolTipShown);
        Tooltip.install(sequences, sequencePosition);

        sequences.setOnMouseMoved((event) -> {
            if (sequencePosition.isShowing()) {
                setPositionText(new Point2D(event.getX(), event.getY()));
            }
        });
    }

    /**
     * Updates the position tooltip text when shown.
     *
     * @param event shown event
     */
    public void positionToolTipShown(WindowEvent event) {
        setPositionText(sequences.screenToLocal(sequencePosition.getX(), sequencePosition.getY()));
    }

    /**
     * Sets the position tooltip to display the global and sequnce postion.
     *
     * @param position the position the tooltip is located
     */
    public void setPositionText(Point2D position) {
        int row = (int) Math.floor(position.getY() / FONT_HEIGHT);

        if (row < drawnFrame.getElements().size() && !drawnFrame.getElements().get(row).getName().equals("")) {
            int column = (int) Math.floor(position.getX() / FONT_WIDTH);
            Element element = drawnFrame.getElements().get(row).getElements().get(column);

            String tip = "";
            if (element.getGappedPosition() != -1) {
                tip = "Global position: " + (element.getGappedPosition() + 1);
            }

            if (element.getUngappedPosition() != -1) {
                tip += System.lineSeparator() + "Sequence position: " + (element.getUngappedPosition() + 1);
            }
            sequencePosition.setText(tip);
        } else {
            // hide() causes NullPointerException when run inside event handler.
            // This bypasses the issue by running in another thread.
            Platform.runLater(() -> sequencePosition.hide());
        }
    }

    
    /**
     * updates the edit mouse mode toggle to on/off, allowing for insertion or deletion of gaps using the mouse.
     */
    
    public void updateEditMouseMode() {
    	composer.toggleEditMouseMode();
    }
    
    /**
     * updates colour scheme to a new colour scheme, depending on what scheme was selected
     * @param newScheme
     */
    
    public void updateColourScheme(String newScheme) {
    	try {
    	currentColourScheme = newScheme;
    	if (newScheme.equals("Custom")) {
    		for (int x=0; x<customColours.length; x++) {
    			customColours[x] = false;
    		}
    		customColours = CustomColours.openPopUp();
    	}
    	}catch(IOException e) {
    		e.printStackTrace();
    	}
    }
    
    private void sequencesWidthChanged(ObservableValue<?> observable, Number oldValue, Number newValue) {
        sequences.setWidth(newValue.doubleValue());
        sequencesTopScale.setWidth(newValue.doubleValue());
        sequencesBottomScale.setWidth(newValue.doubleValue());
        composer.setSequencesWidth(Math.max(0, numberOfElements(sequences.getWidth(), FONT_WIDTH)));
    }

    private void namesWidthChanged(ObservableValue<?> observable, Number oldValue, Number newValue) {
        sequenceNames.setWidth(newValue.doubleValue());
        namesTopScale.setWidth(newValue.doubleValue());
        namesBottomScale.setWidth(newValue.doubleValue());
        composer.setNamesWidth(numberOfElements(sequenceNames.getWidth(), FONT_WIDTH));
    }

    private void sequenceHeightChanged(ObservableValue<?> observable, Number oldValue, Number newValue) {
        sequencesTopScale.setTranslateY(0);
        if (newValue.doubleValue() <= FONT_HEIGHT) {
            sequencesTopScale.setHeight(newValue.doubleValue());
            sequencesBottomScale.setHeight(0);
            sequences.setHeight(0);
        } else if (newValue.doubleValue() <= 2 * FONT_HEIGHT) {
            sequencesTopScale.setHeight(FONT_HEIGHT);
            sequencesBottomScale.setHeight(Math.min(FONT_HEIGHT, newValue.doubleValue() - FONT_HEIGHT));
            sequencesBottomScale.setTranslateY(FONT_HEIGHT);
            sequences.setHeight(0);
            sequences.setTranslateY(FONT_HEIGHT);
        } else {
            sequencesTopScale.setHeight(FONT_HEIGHT);
            sequencesBottomScale.setHeight(FONT_HEIGHT);
            sequencesBottomScale.setTranslateY(newValue.doubleValue() - FONT_HEIGHT);
            sequences.setHeight(newValue.doubleValue() - 2 * FONT_HEIGHT);
            sequences.setTranslateY(FONT_HEIGHT);
        }
        composer.setHeight(numberOfElements(sequences.getHeight(), FONT_HEIGHT));
    }

    private void namesHeightChanged(ObservableValue<?> observable, Number oldValue, Number newValue) {
        if (newValue.doubleValue() <= FONT_HEIGHT) {
            namesTopScale.setHeight(newValue.doubleValue());
            namesBottomScale.setHeight(0);
            sequenceNames.setHeight(0);
        } else if (newValue.doubleValue() <= 2 * FONT_HEIGHT) {
            namesTopScale.setHeight(FONT_HEIGHT);
            namesBottomScale.setHeight(Math.min(FONT_HEIGHT, newValue.doubleValue() - FONT_HEIGHT));
            namesBottomScale.setTranslateY(FONT_HEIGHT);
            sequenceNames.setHeight(0);
            sequenceNames.setTranslateY(FONT_HEIGHT);
        } else {
            namesTopScale.setHeight(FONT_HEIGHT);
            namesBottomScale.setHeight(FONT_HEIGHT);
            namesBottomScale.setTranslateY(newValue.doubleValue() - FONT_HEIGHT);
            sequenceNames.setHeight(newValue.doubleValue() - 2 * FONT_HEIGHT);
            sequenceNames.setTranslateY(FONT_HEIGHT);
        }
    }

    /**
     * Draw named elements.
     */
    public void draw(DrawFrame frame) {
        synchronized (self) {
            newFrame = true;
            nextFrame = frame;
        }
    }

    /**
     * Shift the displayed sequences shift characters.
     *
     * @param shift the number of characters to shift
     */
    public void shiftColumn(int shift) {
    	shift = shift/2;
        int newColumn = getLeftmostColumn() + shift;

        if (newColumn < 0) {
            composer.setColumn(0);
            horizontalScroll.setValue(0);
        } else if (newColumn > 0 && newColumn >= composer.getNumberOfColumns()) {
            composer.setColumn(composer.getNumberOfColumns() - 1);
            horizontalScroll.setValue(horizontalScroll.getMax());
        } else {
            composer.setColumn(newColumn);
            horizontalScroll.setValue(horizontalScroll.getValue() + shift * FONT_WIDTH);
        }
    }

    /**
     * Sets the displayed sequences to position column.
     *
     * @throws IndexOutOfBoundsException if (column < 0 || (column
     *                                   >=getNumberOfColumns()))
     */
    public void setColumn(int column) {
        horizontalScroll.setValue(column * FONT_WIDTH);
    }

    /**
     * Add events to redraw the canvasses when scroll bars move.
     */
    private void addScrollBarRedrawEvents() {
        horizontalScroll.valueProperty().addListener(evt -> composer.setColumn(getLeftmostColumn()));
        verticalScroll.valueProperty()
                      .addListener(evt -> composer.setRow((int) Math.floor(verticalScroll.getValue() / FONT_HEIGHT)));
        namesScroll.valueProperty()
                   .addListener(evt -> composer.setNamesColumn((int) Math.floor(namesScroll.getValue() / FONT_WIDTH)));
    }

    private void stopDrag(MouseEvent event) {
        if (dragging) {
            dragging = false;
            skipRight.stop();
            skipLeft.stop();
            columnSelectStart = 0;
            composer.resetLastStop();
        }
    }

    private void onDragged(MouseEvent event) {
        if (!dragging) {
            columnSelectStart = Math.max(0, event.getX() + horizontalScroll.getValue());
            dragging = true;
        } else if (event.getX() > 0 && event.getX() < sequences.getWidth()) {
            skipRight.stop();
            skipLeft.stop();
            int start = (int) Math.floor(columnSelectStart / FONT_WIDTH);
            int stop = (int) Math.ceil((horizontalScroll.getValue() + event.getX()) / FONT_WIDTH);
            if (start <= stop) {
                composer.selectRange(start, stop);
                composer.updateLastStop(stop);
            } else {
                composer.selectRange(stop, start);
                composer.updateLastStop(stop);
            }
        }

        if (event.getX() > sequences.getWidth() && skipRight.getStatus() != Animation.Status.RUNNING) {
            skipRight.play();
        } else if (event.getX() < 0 && skipLeft.getStatus() != Animation.Status.RUNNING) {
            skipLeft.play();
        }
    }

    /**
     * Deselect all selected columns.
     *
     * @param event mouse event that triggered deselection
     */
    private void deselectColumns(MouseEvent event) {
        columnSelectStart = 0;
        composer.selectRange(0, 0);
    }

    /**
     * Update the scroll bar to fit the current sequence canvas size.
     */
    private void updateScrollBar() {
        if (sequences.getWidth() < composer.getNumberOfColumns() * FONT_WIDTH) {
            horizontalScroll.setMax(composer.getNumberOfColumns() * FONT_WIDTH - sequences.getWidth());
            if (horizontalScroll.getMax() < horizontalScroll.getValue()) {
                horizontalScroll.setValue(horizontalScroll.getMax());
            }
        } else {
            horizontalScroll.setMax(horizontalScroll.getValue());
        }

        if (sequences.getHeight() < composer.getNumberOfRows() * FONT_HEIGHT) {
            verticalScroll.setMax(composer.getNumberOfRows() * FONT_HEIGHT - sequences.getHeight());
            if (verticalScroll.getMax() < verticalScroll.getValue()) {
                verticalScroll.setValue(verticalScroll.getMax());
            }
        } else {
            verticalScroll.setMax(verticalScroll.getValue());
        }

        if (sequenceNames.getWidth() < composer.getNumberOfNameColumns() * FONT_WIDTH) {
            namesScroll.setMax(composer.getNumberOfNameColumns() * FONT_WIDTH - sequenceNames.getWidth());
            if (namesScroll.getMax() < namesScroll.getValue()) {
                namesScroll.setValue(namesScroll.getMax());
            }
        } else {
            namesScroll.setMax(namesScroll.getValue());
        }
    }

    private int numberOfElements(double length, double width) {
        return (int) Math.ceil(length / width);
    }

    private void drawScale(NamedElements scale) {
        GraphicsContext top = sequencesTopScale.getGraphicsContext2D();
        GraphicsContext bottom = sequencesBottomScale.getGraphicsContext2D();
        double columnOffset = ((horizontalScroll.getValue() % FONT_WIDTH) + FONT_WIDTH) % FONT_WIDTH;
        for (int column = 0; column < scale.getElements().size(); column++) {
            drawElement(scale.getElements().get(column), top, 0, (column * FONT_WIDTH) - columnOffset, 0, 0);
            drawElement(scale.getElements().get(column), bottom, 0, (column * FONT_WIDTH) - columnOffset, 0, 0);
        }

        namesTopScale.getGraphicsContext2D().fillText(scale.getName(), 0, 0);
        namesBottomScale.getGraphicsContext2D().fillRect(0, 0, namesBottomScale.getWidth(), 1.5);
        namesBottomScale.getGraphicsContext2D().fillText(scale.getName(), 0, 0);
    }

    private void drawSequences(List<NamedElements> namedElements) {
        double columnOffset = ((horizontalScroll.getValue() % FONT_WIDTH) + FONT_WIDTH) % FONT_WIDTH;
        double rowOffset = ((verticalScroll.getValue() % FONT_HEIGHT) + FONT_HEIGHT) % FONT_HEIGHT;
        int SeqNum = 0;
        GraphicsContext context = sequences.getGraphicsContext2D();
        for (int row = 0; row < namedElements.size(); row++) {
            List<Element> elements = namedElements.get(row).getElements();
            for (int column = 0; column < elements.size(); column++) {
                switch (drawnFrame.getStrand()) {
                case NEGATIVE:
                    int x = elements.size() - 1 - column;
                    drawElement(elements.get(x), context, (row * FONT_HEIGHT) - rowOffset,
                            (x * FONT_WIDTH) - columnOffset, row, column);
                    break;
                case POSITIVE:
                    drawElement(elements.get(column), context, (row * FONT_HEIGHT) - rowOffset,
                            (column * FONT_WIDTH) - columnOffset, row, column);
                    break;
                default:
                    throw new IllegalArgumentException();
                }
            }
        }
    }

    private void drawElement(Element element, GraphicsContext context, double row, double column, int SeqNum, int ColNum) {
        String symbol = element.getSymbol();
        switch (element.getType()) {
        case NUCLEOTIDE:
        	if (currentColourScheme == "HideResidualIdentical") {
        		drawNucleotideHideIdentical(symbol, context, row, column, element.isSelected(), SeqNum, ColNum);
        	}
        	
        	else if (currentColourScheme == "Similarity") {
        		drawNucleotideSimilar(symbol, context, row, column, element.isSelected(), SeqNum, ColNum);
        	}
        	
        	else if (currentColourScheme == "Blosum62") {
        		drawNucleotideBlosum(symbol, context, row, column, element.isSelected(), SeqNum, ColNum);
        	}
        	
        	else if (currentColourScheme == "Pam250") {
        		drawNucleotidePam(symbol, context, row, column, element.isSelected(), SeqNum, ColNum);
        	}
        	else {
        		drawNucleotide(symbol, context, row, column, element.isSelected());
        	}
           
            break;
        case BLANK:
            highlightSelected(element, context, column, row);
            break;
        case SUBSTITUTION:
        case INSERTION:
        case DELETION:
            drawDifference(context, row, column, element.getType(), element.isSelected());
            break;
        case GLOBAL_SCALE:
            drawScale(symbol, context, row, column, element.isSelected());
            break;
        case LOCAL_SCALE:
            drawScale(symbol, context, row, column, element.isSelected());
            break;
        case TRANSLATION:
            drawSymbol(symbol, context, row, column, element.isSelected());
            break;
        case START_CODON:
            drawArrow(context, row, column, BRIGHT_GREEN);
            drawSymbol(symbol, context, row, column, element.isSelected());
            break;
        case STOP_CODON:
            drawArrow(context, row, column, BRIGHT_RED);
            drawSymbol(symbol, context, row, column, element.isSelected());
            break;
        case FEATURE:
            drawBlock(context, row, column, element.isSelected());
            break;     	
        case ANNOTATION:
        	drawAnnotation(context, row, column, element.isSelected(), element.getSymbol());
        	break;
        default:
            throw new IllegalArgumentException(String.format("Missing case for: %s", element.getType()));
        }
    }

    /**
     * Draws an arrow that is three FONT_WIDTH width and one FONT_HEIGHT tall. The
     * arrow points right when {@code drawnFrame.getStrand() == POSITIVE} and left
     * when {@code drawnFrame.getStrand() == NEGATIVE}.
     * 
     * @param context context to draw on
     * @param row     top to start drawing
     * @param column  side to start drawing
     * @param color   arrow color
     */
    private void drawArrow(GraphicsContext context, double row, double column, Paint color) {
        Paint initial = context.getFill();
        context.setFill(color);

        double[] xRightPoints = { column, column + (FONT_WIDTH * 2), column + (FONT_WIDTH * 3),
                column + (FONT_WIDTH * 2), column, column + FONT_WIDTH };
        double[] yRightPoints = { row, row, row + (FONT_HEIGHT / 2), row + FONT_HEIGHT, row + FONT_HEIGHT,
                row + (FONT_HEIGHT / 2) };
        double[] xLeftPoints = { column + FONT_WIDTH, column - FONT_WIDTH, column - (2 * FONT_WIDTH),
                column - FONT_WIDTH, column + FONT_WIDTH, column };
        double[] yleftPoints = { row, row, row + (FONT_HEIGHT / 2), row + FONT_HEIGHT, row + FONT_HEIGHT,
                row + (FONT_HEIGHT / 2) };
        switch (drawnFrame.getStrand()) {
        case NEGATIVE:
            context.fillPolygon(xLeftPoints, yleftPoints, xLeftPoints.length);
            break;
        case POSITIVE:
            context.fillPolygon(xRightPoints, yRightPoints, xRightPoints.length);
            break;
        default:
            throw new IllegalArgumentException("Unsupported strand type");
        }
        context.setFill(initial);
    }

    private void drawSymbol(String symbol, GraphicsContext context, double row, double column, boolean selected) {
        if (selected) {
            Paint initial = context.getFill();
            context.setFill(Color.LIGHTBLUE.darker());
            context.setGlobalAlpha(0.75);
            context.fillRect(column, row, FONT_WIDTH, FONT_HEIGHT);
            context.setGlobalAlpha(1.0);
            context.setFill(initial);
        }
        context.fillText(symbol, column, row);
    }

    private void drawScale(String symbol, GraphicsContext context, double row, double column, boolean selected) {
        if (selected) {
            Paint initial = context.getFill();
            context.setFill(Color.LIGHTBLUE.darker());
            context.setGlobalAlpha(0.75);
            context.fillRect(column, row, FONT_WIDTH, FONT_HEIGHT);
            context.setGlobalAlpha(1.0);
            context.setFill(initial);
        }
        context.fillText(symbol, column, row);
    }

    private void drawDifference(GraphicsContext context, double row, double column, ElementType type,
            boolean selected) {
        Paint initial = context.getFill();
        switch (type) {
        case SUBSTITUTION:
            context.setFill(Color.BLUE);
            break;
        case INSERTION:
            context.setFill(Color.LAWNGREEN);
            break;
        case DELETION:
            context.setFill(Color.RED);
            break;
        default:
            throw new IllegalArgumentException();
        }

        context.fillRect(column, row, FONT_WIDTH, FONT_HEIGHT);

        if (selected) {
            context.setFill(Color.LIGHTBLUE.darker());
            context.setGlobalAlpha(0.75);
            context.fillRect(column, row, FONT_WIDTH, FONT_HEIGHT);
            context.setGlobalAlpha(1.0);
        }
        context.setFill(initial);
    }

    private void drawNucleotide(String symbol, GraphicsContext context, double row, double column, boolean selected) {
    	Paint initial = context.getFill();
        ColourScheme colDefault = new ColourScheme(currentColourScheme);
        Hashtable<String, Paint> colScheme;
        
        if (currentColourScheme.equals("Custom")) {
        	colDefault = new ColourScheme(currentColourScheme, customColours);
        	colScheme = colDefault.getColours();
        }
        
        else {
        	colScheme = colDefault.getColours();
        }
        for (int i = 0; i<symbol.length(); i++) {
        	if (colScheme.containsKey(Character.toString(symbol.charAt(i)))) {
        		context.setFill(colScheme.get(Character.toString(symbol.charAt(i))));
        		
        	}
        	else {
        		context.setFill(Color.WHITE);
        	}
        }

        context.fillRect(column, row, FONT_WIDTH, FONT_HEIGHT);

        if (selected) {
            context.setFill(Color.LIGHTBLUE.darker());
            context.setGlobalAlpha(0.75);
            context.fillRect(column, row, FONT_WIDTH, FONT_HEIGHT);
            context.setGlobalAlpha(1.0);
        }

        context.setFill(initial);
        context.fillText(symbol, column, row);
    }
    
    /*
     * draws A.As darker if more of the same A.As are in the same row
     */
    
    private void drawNucleotideSimilar(String symbol, GraphicsContext context, double row, double column, boolean selected, int RowNum, int ColNum) {
    	Paint initial = context.getFill();
    	List<List<NamedElements>> elementList = new ArrayList<List<NamedElements>>();
    	
    	for (AlignmentSequence seq: composer.getAllSequences()) {
    		
    		elementList.add(seq.getElements(1, ColNum, 1, 1));
    		}
    	
    	Paint twenty = Color.WHITE;
    	Paint fourty = Color.LIGHTGRAY;
    	Paint sixty = Color.GRAY;
    	Paint eighty = Color.DARKGRAY;
    	Paint hundred = Color.GRAY.darker();
		String similar = "";
		for (int j = 0; j<composer.getAllSequences().size(); j++) {
			similar += elementList.get(j).get(0).getElements().get(0).getSymbol();
			
		}
    	for (int i =0; i<symbol.length(); i++) {
    		int[] freq = new int[similar.length()];  
            int m, n;  
            char[] strarray = similar.toCharArray();
            for(m = 0; m <similar.length(); m++) {  
                freq[m] = 1;  
                for(n = m+1; n <similar.length(); n++) {  
                    if(strarray[m] == strarray[n]) {  
                        freq[m]++;   
                        strarray[n] = '0'; 
                    }  
                }  
            }
            String uniqueChar = "";
            for (int j=0; j<similar.length(); j++)
            	if (uniqueChar.indexOf(similar.charAt(j)) == -1){
            		uniqueChar += similar.charAt(j);
            }
            ArrayList uniqueFreq = new ArrayList();
            for (int num: freq) {
            	if (num !=0) {
            		uniqueFreq.add(num);
            	}
            }
            float brightness = 2;
            for (i=0; i<freq.length; i++) {
            	if (strarray[i] != ' ' && strarray[i] != '0' && strarray[i] == symbol.charAt(0)) {
            		brightness = (float) freq[i]/composer.getAllSequences().size();
            	}
            }
            if (brightness <= 0.20) {
            	context.setFill(twenty);
            }
            
            else if (brightness <= 0.40) {
            	context.setFill(fourty);
            }
            
            else if (brightness <= 0.60) {
            	context.setFill(sixty);
            }
            
            else if (brightness <= 0.80) {
            	context.setFill(eighty);
            }
            
            else if (brightness <=1.00)  {
            	context.setFill(hundred);
            }
            
            

            
    	}

        context.fillRect(column, row, FONT_WIDTH, FONT_HEIGHT);

        if (selected) {
            context.setFill(Color.LIGHTBLUE.darker());
            context.setGlobalAlpha(0.75);
            context.fillRect(column, row, FONT_WIDTH, FONT_HEIGHT);
            context.setGlobalAlpha(1.0);
        }

        context.setFill(initial);
        context.fillText(symbol, column, row);
    }
    
    /**
     * Colours A.As in sequences relative to their blosum62 scores.
     * @param symbol
     * @param context
     * @param row
     * @param column
     * @param selected
     * @param RowNum
     * @param ColNum
     */
    
    private void drawNucleotideBlosum(String symbol, GraphicsContext context, double row, double column, boolean selected, int RowNum, int ColNum) {

    	ColourScheme blosumColourScheme = new ColourScheme(currentColourScheme);
    	Hashtable<String, Integer> blosumKey = blosumColourScheme.getMatrixKey();
    	int[][] blosumMatrix = blosumColourScheme.getBlosum62();
    	Paint initial = context.getFill();
    	List<List<NamedElements>> elementList = new ArrayList<List<NamedElements>>();
    	
    	for (AlignmentSequence seq: composer.getAllSequences()) {
    		
    		elementList.add(seq.getElements(1, ColNum, 1, 1));
    		}
    	
    	Paint twenty = Color.WHITE;
    	Paint fourty = Color.LIGHTBLUE;
    	Paint sixty = Color.BLUE;
    	Paint eighty = Color.BLUE.darker();
    	Paint hundred = Color.DARKBLUE;
		String similar = "";
		for (int j = 0; j<composer.getAllSequences().size(); j++) {
			similar += elementList.get(j).get(0).getElements().get(0).getSymbol();
			
		}
    	for (int i =0; i<symbol.length(); i++) {
    		int blosumScore = 0;
    		boolean countItself = true;
    		for (int j = 0; j<similar.length(); j++) {
    			if ((similar.charAt(j) == symbol.charAt(0) && countItself == true) || (similar.charAt(j) != symbol.charAt(0))) {
    				if (blosumKey.containsKey(symbol)){
    				blosumScore += blosumMatrix[blosumKey.get(Character.toString(similar.charAt(j)))][blosumKey.get(symbol)];
    			
    				}
    			}
    			if (similar.charAt(j) == symbol.charAt(0)) {
    				countItself = false;
    			}
    		}
            
            
    		if (blosumScore <= -6) {
    			context.setFill(twenty);
    		}
    		
    		else if (blosumScore <= -2) {
    			context.setFill(fourty);
    		}
    		
    		else if (blosumScore <=2) {
    			context.setFill(sixty);
    		}
    		
    		else if (blosumScore <=6) {
    			context.setFill(eighty);
    		}
    		
    		else {
    			context.setFill(hundred);
    		}
            
    	}

        context.fillRect(column, row, FONT_WIDTH, FONT_HEIGHT);

        if (selected) {
            context.setFill(Color.LIGHTBLUE.darker());
            context.setGlobalAlpha(0.75);
            context.fillRect(column, row, FONT_WIDTH, FONT_HEIGHT);
            context.setGlobalAlpha(1.0);
        }

        context.setFill(initial);
        context.fillText(symbol, column, row);
    }
    
    /**
     * colours Amino Acids based on their pam250 score.
     * @param symbol
     * @param context
     * @param row
     * @param column
     * @param selected
     * @param RowNum
     * @param ColNum
     */
    
    private void drawNucleotidePam(String symbol, GraphicsContext context, double row, double column, boolean selected, int RowNum, int ColNum) {

    	ColourScheme pamColourScheme = new ColourScheme(currentColourScheme);
    	Hashtable<String, Integer> pamKey = pamColourScheme.getMatrixKey();
    	int[][] pamMatrix = pamColourScheme.getPam250();
    	Paint initial = context.getFill();
    	List<List<NamedElements>> elementList = new ArrayList<List<NamedElements>>();
    	
    	for (AlignmentSequence seq: composer.getAllSequences()) {
    		
    		elementList.add(seq.getElements(1, ColNum, 1, 1));
    		}
    	
    	Paint twenty = Color.WHITE;
    	Paint fourty = Color.PINK;
    	Paint sixty = Color.RED;
    	Paint eighty = Color.RED.darker();
    	Paint hundred = Color.DARKRED;
		String similar = "";
		for (int j = 0; j<composer.getAllSequences().size(); j++) {
			similar += elementList.get(j).get(0).getElements().get(0).getSymbol();
			
		}
    	for (int i =0; i<symbol.length(); i++) {
    		int pamScore = 0;
    		boolean countItself = true;
    		for (int j = 0; j<similar.length(); j++) {
    			
    			if ((similar.charAt(j) == symbol.charAt(0) && countItself == true) || (similar.charAt(j) != symbol.charAt(0))) {
    				if (pamKey.containsKey(symbol)){
    					pamScore += pamMatrix[pamKey.get(Character.toString(similar.charAt(j)))][pamKey.get(symbol)];
    				}
    				}
    			if (similar.charAt(j) == symbol.charAt(0)) {
    				countItself = false;
    			}
    		}
            
            
    		if (pamScore <= -6) {
    			context.setFill(twenty);
    		}
    		
    		else if (pamScore <= -2) {
    			context.setFill(fourty);
    		}
    		
    		else if (pamScore <=2) {
    			context.setFill(sixty);
    		}
    		
    		else if (pamScore <=6) {
    			context.setFill(eighty);
    		}
    		
    		else {
    			context.setFill(hundred);
    		}
            
    	}

        context.fillRect(column, row, FONT_WIDTH, FONT_HEIGHT);

        if (selected) {
            context.setFill(Color.LIGHTBLUE.darker());
            context.setGlobalAlpha(0.75);
            context.fillRect(column, row, FONT_WIDTH, FONT_HEIGHT);
            context.setGlobalAlpha(1.0);
        }

        context.setFill(initial);
        context.fillText(symbol, column, row);
    }
    
    /**
     * Draws Nucleotides, coloured same as default, except when the nucleotide in the sequence is the same as the nucleotide in the 1st sequence, it colours that NT black.
     * @param symbol
     * @param context
     * @param row
     * @param column
     * @param selected
     */
    private void drawNucleotideHideIdentical(String symbol, GraphicsContext context, double row, double column, boolean selected, int RowNum, int ColNum) {
        Paint initial = context.getFill();
        List<NamedElements> Sequences = drawnFrame.getElements();
        List<Element> FirstSeq = Sequences.get(1).getElements();
        switch (symbol) {
        case "A":
        	if (FirstSeq.get(ColNum).getSymbol() == "A" && RowNum != 1) {
        		context.setFill(Color.BLACK);
        	}
        	else {
        		context.setFill(Color.rgb(235, 235, 0));
        	}
        	
            break;
        case "C":
        	if (FirstSeq.get(ColNum).getSymbol() == "C" && RowNum != 1) {
        		context.setFill(Color.BLACK);
        	}
        	else {
        		context.setFill(Color.rgb(0, 255, 0).darker());
        	}
            break;
        case "D":
        	if (FirstSeq.get(ColNum).getSymbol() == "D" && RowNum != 1) {
        		context.setFill(Color.BLACK);
        	}
        	else {
        		context.setFill(Color.LAVENDER);
        	}
        	
            break;
        case "E":
        	if (FirstSeq.get(ColNum).getSymbol() == "E" && RowNum != 1) {
        		context.setFill(Color.BLACK);
        	}
        	else {
        		context.setFill(Color.CORAL);
        	}
        	
            break;
        case "F":
        	if (FirstSeq.get(ColNum).getSymbol() == "F" && RowNum != 1) {
        		context.setFill(Color.BLACK);
        	}
        	else {
        		context.setFill(Color.MISTYROSE);
        	}
        	
            break;
        case "G":
        	if (FirstSeq.get(ColNum).getSymbol() == "G" && RowNum != 1) {
        		context.setFill(Color.BLACK);
        	}
        	else {
        		context.setFill(Color.CYAN);
        	}
            break;
        case "H":
        	if (FirstSeq.get(ColNum).getSymbol() == "H" && RowNum != 1) {
        		context.setFill(Color.BLACK);
        	}
        	else {
        		context.setFill(Color.YELLOWGREEN);
        	}
        	
            break;
        case "I":
        	if (FirstSeq.get(ColNum).getSymbol() == "I" && RowNum != 1) {
        		context.setFill(Color.BLACK);
        	}
        	else {
        		context.setFill(Color.GRAY);
        	}
        	
            break;
        case "K":
        	if (FirstSeq.get(ColNum).getSymbol() == "K" && RowNum != 1) {
        		context.setFill(Color.BLACK);
        	}
        	else {
        		context.setFill(Color.LIGHTSEAGREEN);
        	}
        	
            break;
        case "L":
        	if (FirstSeq.get(ColNum).getSymbol() == "L" && RowNum != 1) {
        		context.setFill(Color.BLACK);
        	}
        	else {
        		context.setFill(Color.FUCHSIA);
        	}
        	
            break;
        case "M":
        	if (FirstSeq.get(ColNum).getSymbol() == "M" && RowNum != 1) {
        		context.setFill(Color.BLACK);
        	}
        	else {
        		context.setFill(Color.ORCHID);
        	}
        	
            break;
        case "N":
        	if (FirstSeq.get(ColNum).getSymbol() == "N" && RowNum != 1) {
        		context.setFill(Color.BLACK);
        	}
        	else {
        		context.setFill(Color.CORNFLOWERBLUE);
        	}
        	
            break;
        case "P":
        	if (FirstSeq.get(ColNum).getSymbol() == "P" && RowNum != 1) {
        		context.setFill(Color.BLACK);
        	}
        	else {
        		context.setFill(Color.MEDIUMBLUE);
        	}
        	
            break;
        case "Q":
        	if (FirstSeq.get(ColNum).getSymbol() == "Q" && RowNum != 1) {
        		context.setFill(Color.BLACK);
        	}
        	else {
        		context.setFill(Color.PURPLE);
        	}
        	
            break;
        case "R":
        	if (FirstSeq.get(ColNum).getSymbol() == "R" && RowNum != 1) {
        		context.setFill(Color.BLACK);
        	}
        	else {
        		context.setFill(Color.TEAL);
        	}
        	
            break;
        case "S":
        	if (FirstSeq.get(ColNum).getSymbol() == "S" && RowNum != 1) {
        		context.setFill(Color.BLACK);
        	}
        	else {
        		context.setFill(Color.SPRINGGREEN);
        	}
        	
            break;

        case "T":
        	if (FirstSeq.get(ColNum).getSymbol() == "T" && RowNum != 1) {
        		context.setFill(Color.BLACK);
        	}
        	else {
        		context.setFill(Color.ORANGE);
        	}
            break;
        default:
            context.setFill(Color.WHITE);
            break;
        case "V":
        	if (FirstSeq.get(ColNum).getSymbol() == "V" && RowNum != 1) {
        		context.setFill(Color.BLACK);
        	}
        	else {
        		context.setFill(Color.rgb(255, 175, 66));
        	}
        	
            break;
        case "W":
        	if (FirstSeq.get(ColNum).getSymbol() == "W" && RowNum != 1) {
        		context.setFill(Color.BLACK);
        	}
        	else {
        		context.setFill(Color.INDIANRED);
        	}
        	
            break;
        case "Y":
        	if (FirstSeq.get(ColNum).getSymbol() == "Y" && RowNum != 1) {
        		context.setFill(Color.BLACK);
        	}
        	else {
        		context.setFill(Color.LIGHTPINK);
        	}
        	
            break;
        }

        context.fillRect(column, row, FONT_WIDTH, FONT_HEIGHT);

        if (selected) {
            context.setFill(Color.LIGHTBLUE.darker());
            context.setGlobalAlpha(0.75);
            context.fillRect(column, row, FONT_WIDTH, FONT_HEIGHT);
            context.setGlobalAlpha(1.0);
        }

        context.setFill(initial);
        context.fillText(symbol, column, row);
    }

    private void drawNames(List<NamedElements> names) {
        final int numberOfColumns = Math.min(composer.getNumberOfNameColumns(),
                numberOfElements(sequenceNames.getWidth(), FONT_WIDTH));
        final int startColumn = (int) Math.floor(namesScroll.getValue() / FONT_WIDTH);
        double rowOffset = ((verticalScroll.getValue() % FONT_HEIGHT) + FONT_HEIGHT) % FONT_HEIGHT;
        double columnOffset = ((namesScroll.getValue() % FONT_WIDTH) + FONT_WIDTH) % FONT_WIDTH;

        GraphicsContext gc = sequenceNames.getGraphicsContext2D();
        gc.fillRect(0, 0, sequenceNames.getWidth(), 1.5);
        for (int row = 0; row < names.size(); row++) {
            if (names.get(row).isSelected()) {
                Paint initialColor = gc.getFill();
                gc.setFill(Color.LIGHTBLUE.darker());
                gc.setGlobalAlpha(0.75);
                gc.fillRect(0, (row * FONT_HEIGHT) - rowOffset, sequenceNames.getWidth(), FONT_HEIGHT);
                gc.setGlobalAlpha(1.0);
                gc.setFill(initialColor);
            }

            String name = names.get(row).getName();
            for (int column = startColumn; column - startColumn < numberOfColumns && column < name.length(); column++) {
                gc.fillText(name.substring(column, column + 1), ((column - startColumn) * FONT_WIDTH) - columnOffset,
                        (row * FONT_HEIGHT) - rowOffset);
            }

            if (names.get(row).isOverline()) {
                gc.fillRect(0, (row * FONT_HEIGHT) - rowOffset, sequenceNames.getWidth(), 1.5);
            }
        }
    }

    private void drawBlock(GraphicsContext context, double row, double column, boolean selected) {
        Paint initial = context.getFill();
        context.setFill(Color.rgb(254, 155, 155));
        context.fillRect(column, row, FONT_WIDTH, FONT_HEIGHT);

        if (selected) {
            context.setFill(Color.LIGHTBLUE.darker());
            context.setGlobalAlpha(0.75);
            context.fillRect(column, row, FONT_WIDTH, FONT_HEIGHT);
            context.setGlobalAlpha(1.0);
        }
        context.setFill(initial);
    }
    
    private void drawAnnotation(GraphicsContext context, double row, double column, boolean selected, String symbol) {
        Paint initial = context.getFill();
        context.setFill(Color.PURPLE);
        context.fillRect(column, row, FONT_WIDTH, FONT_HEIGHT);

        if (selected) {
            context.setFill(Color.LIGHTBLUE.darker());
            context.setGlobalAlpha(0.75);
            context.fillRect(column, row, FONT_WIDTH, FONT_HEIGHT);
            context.setGlobalAlpha(1.0);
        }
        context.setFill(initial);
        context.fillText(symbol, column, row);

        
    }

    private void resetCanvas(Canvas canvas) {
        GraphicsContext gc = canvas.getGraphicsContext2D();
        gc.clearRect(0, 0, canvas.getWidth(), canvas.getHeight());
        gc.setTextBaseline(VPos.TOP);
        gc.setFont(Font.font("Roboto Mono", FONT_SIZE));
    }

    /**
     * Returns the index of the left most displayed column in sequences.
     *
     * @return
     */
    private int getLeftmostColumn() {
        return (int) Math.floor(horizontalScroll.getValue() / FONT_WIDTH);
    }

    private void selectSequence(MouseEvent event) {
        int index = (int) Math.floor((verticalScroll.getValue() + event.getY()) / FONT_HEIGHT);

        if (index < composer.getNumberOfRows()) {
            if (event.isControlDown()) {
                composer.toggleRowSelect(index);
            } else {
                composer.deselectAllRows();
                composer.selectRow(index);
            }
        }
    }

    private static double textWidth(String string) {
        final Text text = new Text(string);
        text.setFont(Font.font("Roboto Mono", FONT_SIZE));

        return text.getBoundsInLocal().getWidth();
    }

    private static double textHeight(String string) {
        final Text text = new Text(string);
        text.setFont(Font.font("Roboto Mono", FONT_SIZE));

        return Math.floor(text.getBoundsInLocal().getHeight());
    }

    /**
     * Highlight character at (column, row) row if element is selected.
     * 
     * @param e       element to highlight if selected
     * @param context context to draw highlighting
     * @param column  column to highlight
     * @param row     row to highlight
     */
    private void highlightSelected(Element e, GraphicsContext context, double column, double row) {
        if (e.isSelected()) {
            Paint initial = context.getFill();
            context.setFill(Color.LIGHTBLUE.darker());
            context.setGlobalAlpha(0.75);
            context.fillRect(column, row, FONT_WIDTH, FONT_HEIGHT);
            context.setGlobalAlpha(1.0);
            context.setFill(initial);
        }
    }

    @Override
    public void initializeDialog() {
        FONT_WIDTH = textWidth("A");
        FONT_HEIGHT = textHeight("A");
        draw.start();
    }

    @Override
    public void mountChildPerformances() {
    }
}
