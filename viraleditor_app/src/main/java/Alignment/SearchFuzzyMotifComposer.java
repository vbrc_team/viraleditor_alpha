package Alignment;

import Shared.AbstractComposer;
import Shared.Performance;

import java.util.List;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.scene.control.TableView;
import javafx.scene.control.TableColumn;
import javafx.scene.control.cell.PropertyValueFactory;


public class SearchFuzzyMotifComposer extends AbstractComposer<SearchFuzzyMotifDialog>{
	
	
	public String subString;
	public int misMatch;
	public List<String> fuzzyMotifData;
	public Performance<SequencesDialog, SequenceEditorMode> sequenceWindow;
	public SequenceEditorMode sequencesComposer;
	
	public SearchFuzzyMotifComposer(String[] seqAndMismatch, List<String> Data, Performance<SequencesDialog, SequenceEditorMode> sequencesWindow, SequenceEditorMode sequencesComposer) {
		this.subString = seqAndMismatch[0];
		this.misMatch = Integer.valueOf(seqAndMismatch[1]);
		this.fuzzyMotifData = Data;
		this.sequenceWindow = sequencesWindow;
		this.sequencesComposer = sequencesComposer;
	}
	
	public String getName() {
		return this.subString;
	}
	
	public int getMismatch() {
		return this.misMatch;
	}
	
	public ObservableList getDataTop() {
		ObservableList<FuzzyMotifResult> data = FXCollections.observableArrayList();
		for (int i = 0; i <fuzzyMotifData.size()-5; i+=6) {
			if (fuzzyMotifData.get(i+5) == "Top") {
				data.add(new FuzzyMotifResult(fuzzyMotifData.get(i), fuzzyMotifData.get(i+1), fuzzyMotifData.get(i+2), fuzzyMotifData.get(i+3), fuzzyMotifData.get(i+4), "Top"));
			}
		}
		
		
		
		
		return data;
		
	}
	
	public ObservableList getDataBot() {
		ObservableList<FuzzyMotifResult> data = FXCollections.observableArrayList();
		for (int i = 0; i <fuzzyMotifData.size()-5; i+=6) {
			if (fuzzyMotifData.get(i+5) == "Bottom") {
				data.add(new FuzzyMotifResult(fuzzyMotifData.get(i), fuzzyMotifData.get(i+1), fuzzyMotifData.get(i+2), fuzzyMotifData.get(i+3), fuzzyMotifData.get(i+4), "Bottom"));
		
			}
		}
		
		
		
		
		return data;
		
	}
	
	public void save() {

	}
	
	public TableView initSequences() {
		TableView Table = new TableView();
		TableColumn name = new TableColumn("Seq Name");
		name.setCellValueFactory(new PropertyValueFactory("Name"));
		TableColumn seq = new TableColumn("Sequence");
		seq.setCellValueFactory(new PropertyValueFactory("Sequence"));
		TableColumn numMismatch = new TableColumn("#Mismatches");
		numMismatch.setCellValueFactory(new PropertyValueFactory("Mismatch"));
		TableColumn percentAcc = new TableColumn("%Accuracy");
		percentAcc.setCellValueFactory(new PropertyValueFactory("Accuracy"));
		TableColumn start = new TableColumn("Start#");
		start.setCellValueFactory(new PropertyValueFactory("Start"));
		TableColumn end = new TableColumn("End#");
		end.setCellValueFactory(new PropertyValueFactory("End"));
		Table.getColumns().setAll(name, seq, numMismatch, percentAcc, start, end);
		return Table;
	}
	
	public void moveToMatch(String seqName, String start) {
		sequenceWindow.getController().shiftColumn(-1000000);
		sequenceWindow.getController().shiftColumn((Integer.valueOf(start))-50);
	}
	
	public void updateSearchResults() {
		ObservableList data = getDataTop();
		
		data.addAll(getDataBot());
		sequencesComposer.updateSearchResults(data);
		
		
	}

}
