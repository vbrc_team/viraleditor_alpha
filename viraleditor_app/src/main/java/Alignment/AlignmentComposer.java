package Alignment;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.List;
import java.util.Map;

import javax.swing.JFileChooser;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;

import org.biojava.nbio.core.sequence.DNASequence;
import org.biojava.nbio.core.sequence.io.GenbankReaderHelper;

import Alignment.structures.AlignmentSequence;
import Shared.AbstractComposer;
import Shared.interchange.*;
import java.util.ArrayList;
/**
 * Handles logic for the main Alignment window
 */
public class AlignmentComposer extends AbstractComposer<AlignmentDialog> {
    public Family database;
    private List<Reference> references;
    public List<Reference> annotations;
    public String preference;

    /**
     * Gets genome information and features from the interchange format to add to
     * Alignment Window.
     *
     * Parses the interchange data for each sequence whether from a fasta file
     * (which would have no metadata and only records - see Interchange) or from
     * genbank files (which would have metadata and records - see Interchange). The
     * relevant information in the Interchange is loaded into the featuredSequences
     * list to be later displayed in the Alignment window.
     * 
     * @param interchange - interchange given to alignment window
     */
    public AlignmentComposer(Interchange interchange, Family database, List<Reference> annotationList, String preference) {
        references = interchange.getReferences();
        annotations = annotationList;
        this.preference = preference;
        
        
    }

    /**
     * Parses the local files and creates a list of genomes from them NOTE: right
     * now only handles fasta and genbank files
     * 
     * @param localUploads - local files that have been loaded into tool
     * @throws IOException
     */
    public void addFiles(List<File> localUploads) throws IOException {
        for (File upload : localUploads) {
            references.addAll(readFile(upload.toPath()).getReferences());
        }
    }
    
    /**
     * Gets the selected preference made in the virusinvestigator window, relevant if 
     * selected amino acids is picked.
     */
    
    public String getPreference() {
    	return preference;
    }
    
    /**
     * Gets a list of the annotations that were selected and loaded into the tool
     * @return the list of annotations loaded into the tool
     */
    
    public List<Reference> getAnnotations(){
    	return annotations;
    }

    /**
     * Gets the list of references that have been loaded into the tool
     * 
     * @return the list of reference loaded into the tool
     */
    public List<Reference> getReferences() {
        return references;
    }

    /**
     * adds a new reference after the tool has already loaded
     * @param reference
     */
    public void addReference(Reference reference) {
    	List<Reference> NewList = new ArrayList();
    	for (Reference ref: references) {
    		NewList.add(ref);
    	}
    	NewList.add(reference);
    	this.references = NewList;
    }
    
    /**
     * takes a reference and removes it from refList, which is our list of references
     * 
     * @param reference
     * @return
     */
    
    public void removeReference(List<Reference> refList, Reference reference){
    	int index = refList.indexOf(reference);
    	List<Reference> NewList = new ArrayList<Reference>();
    	for (Reference ref : refList) {
    		NewList.add(ref);
    	}
    	NewList.remove(index);
    	this.references = NewList;
    }

    	
    
    
    /**
     * opens a window to confirm connection to the server to perform the alignment, 1 is a yes, anything else is a no.
     * @return
     */
    public int ConfirmAlignment() {
    	JPanel Confirm = new JPanel();
    	JLabel ConfirmMessage = new JLabel("Are you sure you would you like to connect to the server to run this alignment");
    	Confirm.add(ConfirmMessage);
    	int answer = JOptionPane.showConfirmDialog(null, Confirm, "Confirm", JOptionPane.YES_NO_OPTION, JOptionPane.PLAIN_MESSAGE);
    	return answer;
    }
    
    /**
     * Read GenBank or FASTA file.
     * <p>
     * File type is determined by extension
     * <ul>
     * <li>GenBank - .gb, .gkb
     * <li>FASTA - .fasta
     * 
     * @param file converted to interchange
     * @return file converted to interchange
     * @throws IOException              when file either cannot be read or converted
     *                                  to interchange
     * @throws IllegalArgumentException when the file extension is not supported
     */
    protected Interchange readFile(Path file) throws IOException {
        int lastIndex = file.toString().lastIndexOf(".");

        String fileExtension;
        if (lastIndex >= 0 && lastIndex + 1 < file.toString().length()) {
            fileExtension = file.toString().substring(lastIndex + 1);
        } else {
            fileExtension = "";
        }

        Interchange interchange;
        switch (fileExtension) {
        case "gb":
        case "gbk":
            try {
                Map<String, DNASequence> genBank;
                genBank = GenbankReaderHelper.readGenbankDNASequence(Files.newInputStream(file));

                InterchangeToNTGenBank fromGenBank = new InterchangeToNTGenBank();
                interchange = fromGenBank.from(genBank);
            } catch (Exception e) {
                throw new IOException(String.format("Unable to read %s as GenBank file", file.toString()));
            }
            break;
        case "fasta":
            InterchangeToFASTA fromFASTA = new InterchangeToFASTA();

            try {
                List<String> lines = Files.readAllLines(file);
                interchange = fromFASTA.from(String.join(System.lineSeparator(), lines));
            } catch (IOException e) {
                throw new IOException(String.format("Unable to read %s as FASTA file", file.toString()));
            }
            break;
        default:
            throw new IllegalArgumentException(String.format("Unsupported file extension: %s", fileExtension));
        }

        return interchange;
    }

}
