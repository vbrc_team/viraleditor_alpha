package Alignment;

import Alignment.structures.AlignmentSequence;
import Shared.AbstractDialog;
import javafx.fxml.FXML;
import javafx.scene.control.ListCell;
import javafx.scene.control.ListView;
import javafx.scene.input.*;
import javafx.stage.WindowEvent;
import javafx.util.Callback;

/**
 * Handles the UI for the Reorder Sequences window.
 */
public class ReorderSequencesDialog extends AbstractDialog<ReorderSequencesComposer> {
    private static final DataFormat LIST_ITEM_DATA_FORMAT = new DataFormat("AlignmentSequence");
    /**
     * The cell a drag event starts at.
     */
    private ListCell<AlignmentSequence> dragSource;

    @FXML
    private ListView<AlignmentSequence> listView;

    /**
     * Sets up the ability to drag to reorder items in list via cell factory.
     */
    private void setupCellFactory() {
        listView.setCellFactory(new Callback<ListView<AlignmentSequence>, ListCell<AlignmentSequence>>() {
            @Override
            public ListCell<AlignmentSequence> call(ListView<AlignmentSequence> param) {
                ListCell<AlignmentSequence> listCell = new ListCell<AlignmentSequence>() {
                    @Override
                    public void updateItem(AlignmentSequence item, boolean empty) {
                        super.updateItem(item, empty);
                        if (item != null) {
                            setText(item.getName());
                        }
                    }
                };
                listCell.setOnDragDetected(event -> dragDetected(event, listCell));
                listCell.setOnDragOver(event -> dragOver(event, listCell));
                listCell.setOnDragDropped(event -> dragDropped(event, listCell));

                return listCell;
            }
        });
    }

    /** Gets item to be dragged and adds it to the dragboard to be moved.
     * @param event - mouse event of drag detected
     * @param listCell - listCell to be moved
     */
    private void dragDetected(MouseEvent event, ListCell<AlignmentSequence> listCell) {
        int index = listCell.getIndex();
        Dragboard dragboard = listCell.startDragAndDrop(TransferMode.MOVE);
        ClipboardContent content = new ClipboardContent();
        content.put(LIST_ITEM_DATA_FORMAT, index);
        dragboard.setContent(content);
        dragboard.setDragView(listCell.snapshot(null, null));
        event.consume();
        dragSource = listCell;
    }

    /** Drags item over and sets the transfer mode as long as item dragged over
     * is not the source item and something is being dragged
     * @param event - drag event for dragging list item over
     * @param item - list item which dragSource is being dragged over
     */
    private void dragOver(DragEvent event, ListCell<AlignmentSequence> item) {
        Dragboard dragboard = event.getDragboard();
        if (dragboard.hasContent(LIST_ITEM_DATA_FORMAT) && dragSource != item) {
            event.acceptTransferModes(TransferMode.MOVE);
        }
        event.consume();
    }

    /** Drops item in correct index and reorders list.
     * @param event - drag event to drop item being dragged
     * @param item - item to drop dragged item on
     */
    private void dragDropped(DragEvent event, ListCell<AlignmentSequence> item) {
        Dragboard dragboard = event.getDragboard();
        if (dragboard.hasContent(LIST_ITEM_DATA_FORMAT) &&
                event.getAcceptedTransferMode() == TransferMode.MOVE) {
            int index = dragSource.getIndex();
            composer.move(index, item.getIndex());
        }
        event.setDropCompleted(true);
        event.consume();
        listView.setItems(composer.getElements());
    }

    /**
     * Close dialog.
     */
    @FXML
    private void close() {
        stage.fireEvent(new WindowEvent(stage, WindowEvent.WINDOW_CLOSE_REQUEST));
        stage.close();
    }

    @Override
    public void initializeDialog() {
        setupCellFactory();
        listView.setItems(composer.getElements());
    }

    @Override
    public void mountChildPerformances() {
    }
}
