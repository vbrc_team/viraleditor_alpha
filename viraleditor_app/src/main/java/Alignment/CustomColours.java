package Alignment;

import java.io.IOException;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.ToggleButton;
import javafx.stage.Stage;
import javafx.fxml.FXMLLoader;

import java.util.Objects;

import javafx.fxml.FXML;

public class CustomColours {
	
	@FXML
	public ToggleButton A;
	@FXML
	public ToggleButton C;
	@FXML
	public ToggleButton D;
	@FXML
	public ToggleButton E;
	@FXML
	public ToggleButton F;
	@FXML
	public ToggleButton G;
	@FXML
	public ToggleButton H;
	@FXML
	public ToggleButton I;
	@FXML
	public ToggleButton K;
	@FXML
	public ToggleButton L;
	@FXML
	public ToggleButton M;
	@FXML
	public ToggleButton N;
	@FXML
	public ToggleButton P;
	@FXML
	public ToggleButton Q;
	@FXML
	public ToggleButton R;
	@FXML
	public ToggleButton S;
	@FXML
	public ToggleButton T;
	@FXML
	public ToggleButton V;
	@FXML
	public ToggleButton W;
	@FXML
	public ToggleButton Y;
	@FXML
	public Button Go;
	public static boolean[] selectedColours = new boolean[]
			{false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false};
	
	
	public static boolean[] openPopUp() throws IOException {
		
		
		FXMLLoader loader = new FXMLLoader(CustomColours.class.getClassLoader().getResource("CustomColour.fxml"));
		Parent root = loader.load(Objects.requireNonNull(CustomColours.class.getClassLoader().getResource("CustomColour.fxml")).openStream());
		Stage popup = new Stage();
		popup.setAlwaysOnTop(true);
        popup.setTitle("Select Amino Acids to be Coloured");
        popup.setScene(new Scene(root));
        popup.showAndWait();
        
        return selectedColours;
	}
	
	@FXML
	public void updateA() {
		selectedColours[0] = !selectedColours[0];
	}
	
	@FXML
	public void updateC() {
		selectedColours[1] = !selectedColours[1];
	}
	
	@FXML
	public void updateD() {
		selectedColours[2] = !selectedColours[2];
	}
	
	@FXML
	public void updateE() {
		selectedColours[3] = !selectedColours[3];
	}
	
	@FXML
	public void updateF() {
		selectedColours[4] = !selectedColours[4];
	}
	
	@FXML
	public void updateG() {
		selectedColours[5] = !selectedColours[5];
	}
	
	@FXML
	public void updateH() {
		selectedColours[6] = !selectedColours[6];
	}
	
	@FXML
	public void updateI() {
		selectedColours[7] = !selectedColours[7];
	}
	
	@FXML
	public void updateK() {
		selectedColours[8] = !selectedColours[8];
	}
	
	@FXML
	public void updateL() {
		selectedColours[9] = !selectedColours[9];
	}
	
	@FXML
	public void updateM() {
		selectedColours[10] = !selectedColours[10];
	}
	
	@FXML
	public void updateN() {
		selectedColours[11] = !selectedColours[11];
	}
	
	@FXML
	public void updateP() {
		selectedColours[12] = !selectedColours[12];
	}
	
	@FXML
	public void updateQ() {
		selectedColours[13] = !selectedColours[13];
	}
	
	@FXML
	public void updateR() {
		selectedColours[14] = !selectedColours[14];
	}
	
	@FXML
	public void updateS() {
		selectedColours[15] = !selectedColours[15];
	}
	
	@FXML
	public void updateT() {
		selectedColours[16] = !selectedColours[16];
	}
	
	@FXML
	public void updateV() {
		selectedColours[17] = !selectedColours[17];
	}
	
	@FXML
	public void updateW() {
		selectedColours[18] = !selectedColours[18];
	}
	
	@FXML
	public void updateY() {
		selectedColours[19] = !selectedColours[19];
	}
	
	@FXML
	public void Accept() {
		Stage stage = (Stage) Go.getScene().getWindow();
        stage.close();  
	}
	

}
