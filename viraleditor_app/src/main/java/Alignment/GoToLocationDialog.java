package Alignment;

import java.math.BigDecimal;
import java.math.RoundingMode;

import Alignment.structures.AlignmentSequence;
import Shared.AbstractDialog;
import javafx.application.Platform;
import javafx.beans.value.ObservableValue;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.scene.control.ChoiceBox;
import javafx.scene.control.RadioButton;
import javafx.scene.control.Slider;
import javafx.scene.control.TextField;
import javafx.stage.WindowEvent;
import javafx.util.StringConverter;

/**
 * Handles the UI for the GoToLocation window.
 */
public class GoToLocationDialog extends AbstractDialog<GoToLocationComposer> {
    @FXML
    private ChoiceBox<AlignmentSequence> sequenceList;

    @FXML
    private TextField inputPosition;

    @FXML
    private RadioButton gapped;

    @FXML
    private Slider slider;

    /**
     * Sets up the sequences in the drop down list
     * 
     * @param sequences - sequences currently displayed in sequence window
     */
    public void setSequences(ObservableList<AlignmentSequence> sequences) {
        sequenceList.setItems(sequences);
        sequenceList.getSelectionModel().select(0);
        Platform.runLater(() -> setDefaultValues(sequenceList.getSelectionModel().getSelectedItem()));
    }

    /**
     * Sets the position input by user on sequence window as long as sequence is that long, depending on
     * whether the gapped or absolute position is selected.
     *
     * If gapped is selected, it will jump to the gapped sequence location. If absolute, it will jump to the
     * absolute sequence location.
     */
    @FXML
    private void goToLocation() {
        String location = inputPosition.getText();
        updateLocation(location);
        if (location.matches("[1-9]([0-9]*)")) {
            composer.setGappedPosition(Integer.parseInt(location) - 1);
        }
        close();
    }

    @FXML
    private void close() {
        stage.fireEvent(new WindowEvent(stage, WindowEvent.WINDOW_CLOSE_REQUEST));
        stage.close();
    }

    private void changed(ObservableValue<? extends AlignmentSequence> observable, AlignmentSequence oldValue,
            AlignmentSequence newValue) {
        setDefaultValues(newValue);
    }

    private void setDefaultValues(AlignmentSequence sequence) {
        slider.setMin(1);
        slider.setValue(1);
        slider.setMajorTickUnit(sequence.getNumberOfColumns());
        if (gapped.isSelected()) {
            slider.setMax(sequence.getNumberOfColumns());
        } else {
            slider.setMax(sequence.ungappedPosition(sequence.getNumberOfColumns() - 1));
        }
        inputPosition.setText("1");

        double tickSize = slider.getMax() / 5.0;
        int numberOfDigits = (int) Math.ceil(Math.log10(tickSize));
        slider.setMajorTickUnit(
                new BigDecimal(Double.toString(tickSize)).setScale(-(numberOfDigits - 1), RoundingMode.HALF_EVEN)
                                                         .doubleValue());
    }

    @FXML
    public void initialize() {
        sequenceList.getSelectionModel().selectedItemProperty().addListener(this::changed);
        sequenceList.setConverter(new StringConverter<AlignmentSequence>() {
            @Override
            public String toString(AlignmentSequence object) {
                return object.getName();
            }

            @Override
            public AlignmentSequence fromString(String string) {
                throw new UnsupportedOperationException();
            }
        });
        gapped.setOnAction(
                evt -> Platform.runLater(() -> setDefaultValues(sequenceList.getSelectionModel().getSelectedItem())));
        slider.valueProperty()
              .addListener(
                      (obs, oldValue, newValue) -> {
                              inputPosition.setText(Integer.toString((int) slider.getValue()));
                      });
        inputPosition.textProperty().addListener(this::textChanged);
    }

    private void textChanged(ObservableValue<? extends String> observable, String oldValue, String newValue) {
        updateLocation(newValue);
    }

    private void updateLocation(String location) {
        if (location.matches("[1-9]([0-9]*)")) {
            AlignmentSequence sequence = sequenceList.getSelectionModel().getSelectedItem();
            int position = Integer.parseInt(location) - 1;
            if (position >= sequence.getNumberOfColumns()) {
                position = sequence.getNumberOfColumns() - 1;
                inputPosition.setText(Integer.toString(position));
            }

            if (!gapped.isSelected()) {
                position = sequence.gappedPosition(position);
            }
            slider.setValue(position + 1.0);
            inputPosition.setStyle("-fx-background-color:green");
        } else {
            inputPosition.setStyle("-fx-background-color:red");
        }
    }

    @Override
    public void initializeDialog() {
        slider.setMin(1);
        slider.setMax(0);
    }

    @Override
    public void mountChildPerformances() {
    }

}
