package Alignment;

import Alignment.structures.AlignmentSequence;
import Alignment.structures.Orderable;
import Shared.AbstractComposer;
import javafx.collections.ObservableList;

public class ReorderSequencesComposer extends AbstractComposer<ReorderSequencesDialog>
        implements Orderable<AlignmentSequence> {
    private Orderable<AlignmentSequence> sequences;

    public ReorderSequencesComposer(Orderable<AlignmentSequence> sequences) {
        this.sequences = sequences;
    }

    @Override
    public int size() {
        return sequences.size();
    }

    @Override
    public void move(int source, int target) {
        sequences.move(source, target);
    }

    @Override
    public ObservableList<AlignmentSequence> getElements() {
        return sequences.getElements();
    }
}
