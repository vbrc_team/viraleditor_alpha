package Alignment;

import javafx.scene.control.Button;
import javafx.scene.control.TextField;
import java.io.IOException;

import Shared.AbstractDialog;
import Shared.AbstractWindow;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.event.Event;
import javafx.fxml.FXML;
import javafx.scene.chart.LineChart;
import javafx.scene.chart.NumberAxis;
import javafx.scene.layout.AnchorPane;
import javafx.scene.shape.Rectangle;

public class NucleotideContentGraphDialog extends AbstractDialog<NucleotideContentGraphComposer> {
	
	@FXML
	Button replot;	
	@FXML
    LineChart<Number, Number> NucleotideContentGraph;
    @FXML
    NumberAxis xAxis;
    @FXML
    NumberAxis yAxis;
    @FXML
    private AnchorPane graphPane;
    @FXML
    TextField windowSize;
    @FXML
    TextField stepSize;
    @FXML
    private Rectangle draggedRectangle;
    
    @Override
    public void initializeDialog() {
    	stage.setTitle("Nucleotide Content Graph");
    	windowSize.setText("300");
        stepSize.setText("100");
        
        drawLineChart();
    }
    
    public int getWindowSize() {
    	return Integer.parseInt(windowSize.getText());
    }
    
    public int getStepSize() {
    	return Integer.parseInt(stepSize.getText());
    }
    
    @FXML
    public void handleReplot(Event event) throws IOException {
    	drawLineChart();
    }
    
    @FXML
    public void windowSizeUpdate() {
    	System.out.println(windowSize.getText());
    }
    
    public void initLineChart() {
    	NucleotideContentGraph.setCreateSymbols(false);
    	NucleotideContentGraph.getData().clear();
    	
    	// force the field to be numeric only
    	windowSize.textProperty().addListener(new ChangeListener<String>() {
    	    @Override
    	    public void changed(ObservableValue<? extends String> observable, String oldValue, String newValue) {
    	        if (!newValue.matches("\\d*")) {
    	        	windowSize.setText(newValue.replaceAll("[^\\d]", ""));
    	        }
    	    }
    	});
    	
    	// force the field to be numeric only
    	stepSize.textProperty().addListener(new ChangeListener<String>() {
    	    @Override
    	    public void changed(ObservableValue<? extends String> observable, String oldValue, String newValue) {
    	        if (!newValue.matches("\\d*")) {
    	        	windowSize.setText(newValue.replaceAll("[^\\d]", ""));
    	        }
    	    }
    	});
    }
    
    public void drawLineChart() {
    	initLineChart();
        NucleotideContentGraph.getData().add( composer.calculateLineChartSeries() );
    }

	@Override
	public void mountChildPerformances() {
		// TODO Auto-generated method stub
		
	}

}
