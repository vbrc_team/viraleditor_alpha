package Alignment;

import java.io.IOException;
import java.util.Objects;

import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.stage.*;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.*;

public class ContentGraphBaseSelection {

	@FXML
	private CheckBox A;
	@FXML
	private CheckBox C;
	@FXML
	private CheckBox G;
	@FXML
	private CheckBox T;
	@FXML
	private Button Select;
	@FXML
	private Button Cancel;
	
	static char[] search;
	
	public static char[] openPopUp() throws IOException {
		FXMLLoader loader = new FXMLLoader(ContentGraphBaseSelection.class.getClassLoader().getResource("ContentGraphBaseSelection.fxml"));
		Parent root = loader.load(Objects.requireNonNull(ContentGraphBaseSelection.class.getClassLoader().getResource("ContentGraphBaseSelection.fxml")).openStream());
		Stage popup = new Stage();
        popup.setTitle("Select Bases");
        popup.setScene(new Scene(root));
        popup.showAndWait();
        
        return search;
    }
	
	@FXML
	private void SelectPressed() throws IOException {
        
        search = new char[4];
        
        if (A.isSelected()) {
            search[0] = 'A';
        } else {
            search[0] = '0';
        }
        if (C.isSelected()) {
            search[1] = 'C';
        } else {
            search[1] = '0';
        }
        if (G.isSelected()) {
            search[2] = 'G';
        } else {
            search[2] = '0';
        }
        if (T.isSelected()) {
            search[3] = 'T';
        } else {
            search[3] = '0';
        }
        
        Stage stage = (Stage) Cancel.getScene().getWindow();
        stage.close();  
    }
	
	@FXML
	private void CancelPressed() {
        search = null;
        Stage stage = (Stage) Cancel.getScene().getWindow();
        stage.close();
    }
	
}
