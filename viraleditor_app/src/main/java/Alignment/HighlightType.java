package Alignment;

import java.util.HashMap;
import java.util.Map;

/**
 * HighlightType contains the corresponding css styling, start offset and end offset for a particular regex pattern.
 */
public class HighlightType {
    public enum hType {
        A("A", "a", 0, 0),
        C("C", "c", 0, 0),
        G("G", "g", 0, 0),
        T("T", "t", 0, 0),

        AA("\\*=>", "aa", 1, 0),
        M("M=>", "m", 1, 0),
        AAR("<=\\*", "aa", 0, 1),
        MR("<=M", "m", 0, 1),

        NONE("", "none", 0, 0);

        public final String pattern;
        public final String style;
        public final int startOffset;
        public final int endOffset;
        private static final Map<String, String> lookup = new HashMap<>();
        private static final Map<String, Integer> offsets = new HashMap<>();
        private static final Map<String, Integer> endOffsets = new HashMap<>();
        static {
            for (HighlightType.hType t : HighlightType.hType.values()) {
                lookup.put(t.pattern, t.style);
                offsets.put(t.pattern, t.startOffset);
                endOffsets.put(t.pattern, t.endOffset);
            }
        }

        hType(String pattern, String style, int startOffset, int endOffset) {
            this.pattern = pattern;
            this.style = style;
            this.startOffset = startOffset;
            this.endOffset = endOffset;
        }

        /**
         * Finds the corresponding css style to the inputted regex pattern.
         *
         * @param pattern - regex pattern to find
         * @return css style corresponding to regex pattern
         */
        public static String retrieveStyleByPattern(String pattern) {
            return lookup.get(pattern);
        }

        /**
         * Finds the corresponding start offset to the inputted regex pattern. The end offset represents
         * where the highlights should start at.
         *
         * @param pattern - regex pattern to find
         * @return start offset corresponding to regex pattern
         */
        public static int retrieveStartOffset(String pattern) {
            return offsets.get(pattern);
        }

        /**
         * Finds the corresponding end offset to the inputted regex pattern. The end offset represents
         * where the highlights should continue/stop at.
         *
         * @param pattern - regex pattern to find
         * @return end offset corresponding to regex pattern
         */
        public static int retrieveEndOffset(String pattern) {
            return endOffsets.get(pattern);
        }
    }
}

