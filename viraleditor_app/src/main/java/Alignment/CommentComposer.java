package Alignment;

import Shared.AbstractComposer;

public class CommentComposer extends AbstractComposer<CommentDialog> {
	private Comment toEdit;
	
	/**
	 * @return the comment that's being edited or null if not editing a comment
	 */
	public Comment getOriginal(){
		return toEdit;
	}
}
