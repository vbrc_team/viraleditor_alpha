package Alignment;


import javafx.scene.paint.Color;
import javafx.scene.paint.Paint;
import java.util.Hashtable;

public class ColourScheme {
	public Hashtable<String, Paint> colScheme = new Hashtable<String, Paint>();
	public Hashtable<String, Integer> matrixCord = new Hashtable<String, Integer>();
	
	
	public ColourScheme(String Scheme) {
		if (Scheme.equals("Default")){
			colScheme.put("A", Color.rgb(235, 235, 0)) ;
			colScheme.put("C", Color.rgb(0, 255, 0));
			colScheme.put("D", Color.LAVENDER);
			colScheme.put("E", Color.CORAL);
			colScheme.put("F", Color.MISTYROSE);
			colScheme.put("G", Color.CYAN);
			colScheme.put("H", Color.YELLOWGREEN);
			colScheme.put("I", Color.GRAY);
			colScheme.put("K", Color.LIGHTSEAGREEN);
			colScheme.put("L", Color.FUCHSIA);
			colScheme.put("M", Color.ORCHID) ;
			colScheme.put("N", Color.CORNFLOWERBLUE);
			colScheme.put("P", Color.MEDIUMBLUE);
			colScheme.put("Q", Color.PURPLE);
			colScheme.put("R", Color.TEAL);
			colScheme.put("S", Color.SPRINGGREEN) ;
			colScheme.put("T", Color.ORANGE);
			colScheme.put("V", Color.rgb(255, 175, 66));
			colScheme.put("W", Color.INDIANRED);
			colScheme.put("Y", Color.LIGHTPINK);

		}
		
		if (Scheme.equals("Hydrophobicity")){
			//PYWST neutral
			//RKNDEQH Hydrophilic
			//GAMCFLVI Hydrophobic
			Paint neutral = Color.PURPLE;
			Paint hydrophilic = Color.BLUE;
			Paint hydrophobic = Color.RED;
			colScheme.put("A", hydrophobic);
			colScheme.put("C", hydrophobic);
			colScheme.put("D", hydrophilic);
			colScheme.put("E", hydrophilic);
			colScheme.put("F", hydrophobic);
			colScheme.put("G", hydrophobic);
			colScheme.put("H", hydrophilic);
			colScheme.put("I", hydrophobic);
			colScheme.put("K", hydrophilic);
			colScheme.put("L", hydrophobic);
			colScheme.put("M", hydrophobic);
			colScheme.put("N", hydrophilic);
			colScheme.put("P", neutral);
			colScheme.put("Q", hydrophilic);
			colScheme.put("R", hydrophilic);
			colScheme.put("S", neutral);
			colScheme.put("T", neutral);
			colScheme.put("V", hydrophobic);
			colScheme.put("W", neutral);
			colScheme.put("Y", neutral);
		}
		
		if (Scheme.equals("Similarity")) {
			colScheme.put("A", Color.rgb(235, 235, 0)) ;
			colScheme.put("C", Color.rgb(0, 255, 0));
			colScheme.put("D", Color.LAVENDER);
			colScheme.put("E", Color.CORAL);
			colScheme.put("F", Color.MISTYROSE);
			colScheme.put("G", Color.CYAN);
			colScheme.put("H", Color.YELLOWGREEN);
			colScheme.put("I", Color.GRAY);
			colScheme.put("K", Color.LIGHTSEAGREEN);
			colScheme.put("L", Color.FUCHSIA);
			colScheme.put("M", Color.ORCHID) ;
			colScheme.put("N", Color.CORNFLOWERBLUE);
			colScheme.put("P", Color.MEDIUMBLUE);
			colScheme.put("Q", Color.PURPLE);
			colScheme.put("R", Color.TEAL);
			colScheme.put("S", Color.SPRINGGREEN) ;
			colScheme.put("T", Color.ORANGE);
			colScheme.put("V", Color.rgb(255, 175, 66));
			colScheme.put("W", Color.INDIANRED);
			colScheme.put("Y", Color.LIGHTPINK);
		}
		
		if (Scheme.equals("Blosum62")) {
			matrixCord.put("A", 0);
			matrixCord.put("B", 1);
			matrixCord.put("C", 2);
			matrixCord.put("D", 3);
			matrixCord.put("E", 4);
			matrixCord.put("F", 5);
			matrixCord.put("G", 6);
			matrixCord.put("H", 7);
			matrixCord.put("I", 8);
			matrixCord.put("J", 9);
			matrixCord.put("K", 10);
			matrixCord.put("L", 11);
			matrixCord.put("M", 12);
			matrixCord.put("N", 13);
			matrixCord.put("O", 14);
			matrixCord.put("P", 15);
			matrixCord.put("Q", 16);
			matrixCord.put("R", 17);
			matrixCord.put("S", 18);
			matrixCord.put("T", 19);
			matrixCord.put("U", 20);
			matrixCord.put("V", 21);
			matrixCord.put("W", 22);
			matrixCord.put("X", 23);
			matrixCord.put("Y", 24);
			matrixCord.put("Z", 25);
			matrixCord.put("*", 26);
			matrixCord.put("-", 27);
		}
		
		if (Scheme.equals("Pam250")) {
			matrixCord.put("A", 0);
			matrixCord.put("R", 1);
			matrixCord.put("N", 2);
			matrixCord.put("D", 3);
			matrixCord.put("C", 4);
			matrixCord.put("Q", 5);
			matrixCord.put("E", 6);
			matrixCord.put("G", 7);
			matrixCord.put("H", 8);
			matrixCord.put("I", 9);
			matrixCord.put("L", 10);
			matrixCord.put("K", 11);
			matrixCord.put("M", 12);
			matrixCord.put("F", 13);
			matrixCord.put("P", 14);
			matrixCord.put("S", 15);
			matrixCord.put("T", 16);
			matrixCord.put("W", 17);
			matrixCord.put("Y", 18);
			matrixCord.put("V", 19);
			
		}
	}
	
	public ColourScheme(String Scheme, boolean[] selectedColour) {
		if (selectedColour[0]) {
			colScheme.put("A", Color.rgb(235, 235, 0)) ;
		}
		else {
			colScheme.put("A", Color.WHITE);
		}
		
		if (selectedColour[1]) {
			colScheme.put("C", Color.rgb(0, 255, 0));
		}
		
		else {
			colScheme.put("C", Color.WHITE);
		}
		
		if (selectedColour[2]) {
			colScheme.put("D", Color.LAVENDER);
		}
		
		else {
			colScheme.put("D", Color.WHITE);
		}
		
		if (selectedColour[3]) {
			colScheme.put("E", Color.CORAL);
		}
		
		else {
			colScheme.put("E", Color.WHITE);
		}
		
		if (selectedColour[4]) {
			colScheme.put("F", Color.MISTYROSE);
		}
		
		else {
			colScheme.put("F", Color.WHITE);
		}
		
		if (selectedColour[5]) {
			colScheme.put("G", Color.CYAN);
		}
		
		else {
			colScheme.put("G", Color.WHITE);
		}
		
		if (selectedColour[6]) {
			colScheme.put("H", Color.YELLOWGREEN);
		}
		
		else {
			colScheme.put("H", Color.WHITE);
		}
		
		if (selectedColour[7]) {
			colScheme.put("I", Color.GRAY);
		}
		
		else {
			colScheme.put("I", Color.WHITE);
		}
		
		if (selectedColour[8]) {
			colScheme.put("K", Color.LIGHTSEAGREEN);
		}
		
		else {
			colScheme.put("K", Color.WHITE);
		}
		
		if (selectedColour[9]) {
			colScheme.put("L", Color.FUCHSIA);
		}
		
		else {
			colScheme.put("L", Color.WHITE);
		}
		
		if (selectedColour[10]) {
			colScheme.put("M", Color.ORCHID);
		}
		
		else {
			colScheme.put("M", Color.WHITE);
		}
		
		if (selectedColour[11]) {
			colScheme.put("N", Color.CORNFLOWERBLUE);
		}
		
		else {
			colScheme.put("N", Color.WHITE);
		}
		
		if (selectedColour[12]) {
			colScheme.put("P", Color.MEDIUMBLUE);
		}
		
		else {
			colScheme.put("P", Color.WHITE);
		}
		
		if (selectedColour[13]) {
			colScheme.put("Q", Color.PURPLE);
		}
		
		else {
			colScheme.put("Q", Color.WHITE);
		}
		
		if (selectedColour[14]) {
			colScheme.put("R", Color.TEAL);
		}
		
		else {
			colScheme.put("R", Color.WHITE);
		}
		
		if (selectedColour[15]) {
			colScheme.put("S", Color.SPRINGGREEN);
		}
		
		else {
			colScheme.put("S", Color.WHITE);
		}
		
		if (selectedColour[16]) {
			colScheme.put("T", Color.ORANGE);
		}
		
		else {
			colScheme.put("T", Color.WHITE);
		}
		
		if (selectedColour[17]) {
			colScheme.put("V", Color.rgb(255, 175, 66));
		}
		
		else {
			colScheme.put("V", Color.WHITE);
		}
		
		if (selectedColour[18]) {
			colScheme.put("W", Color.INDIANRED);
		}
		
		else {
			colScheme.put("W", Color.WHITE);
		}
		
		if (selectedColour[19]) {
			colScheme.put("Y", Color.LIGHTPINK);
		}
		
		else {
			colScheme.put("Y", Color.WHITE);
		}
		
	}
	
	public Hashtable<String, Paint> getColours() {
		return colScheme;
	}
	
	public Hashtable<String, Integer> getMatrixKey(){
		return matrixCord;
	}
	
	public int[][] getPam250(){
		final int[][] matrix = new int[][]
				{
				/*A  R  N  D  C  Q  E  G  H  I  L  K  M  F  P  S  T  W  Y  V*/
		   /*A*/{ 2,-2, 0, 0,-2, 0, 0, 1,-1,-1,-2,-1,-1,-4, 1, 1, 1,-6,-3, 0},
		   /*R*/{-2, 6, 0,-1,-4, 1, 1, 0, 2,-2,-3, 3, 0,-4, 0, 0,-1, 2,-4,-2},
		   /*N*/{ 0, 0, 2, 2,-4, 1, 1, 0, 2,-2,-3, 1,-2,-4,-1, 1, 0,-4,-2,-2},
		   /*D*/{ 0,-1, 2, 4,-5, 2, 3, 1, 1,-2,-4, 0,-3,-6,-1, 0, 0,-7,-4,-2},
		   /*C*/{-2,-4,-4,-5, 4,-5,-5,-3,-3,-2,-6,-5,-5,-4,-3, 0,-2,-8, 0,-2},
		   /*Q*/{ 0, 1, 1, 2,-5, 4, 2,-1, 3,-2,-2, 1,-1,-5, 0,-1,-1,-5,-4,-2},
		   /*E*/{ 0,-1, 1, 3,-5, 2, 4, 0, 1,-2,-3, 0,-2,-5,-1, 0, 0,-7,-4,-2},
		   /*G*/{ 1,-3, 0, 1,-3,-1, 0, 5,-2,-3,-4,-2,-3,-5,-1, 1, 0,-7,-5,-1},
		   /*H*/{-1, 2, 2, 1,-3, 3, 1,-2, 6,-2,-2, 0,-2,-2, 0,-1,-1,-3, 0,-2},
		   /*I*/{-1,-2,-2,-2,-2,-2,-2,-3,-2, 5, 2,-2, 2, 1,-2,-1, 0,-5,-1,-4},
		   /*L*/{-2,-3,-3,-4,-6,-2,-3,-4,-2, 2, 6,-3, 4, 2,-3,-3,-2,-2,-1, 2},
		   /*K*/{-1, 3, 1, 0,-5, 1, 0,-2, 0,-2, 3, 5, 0,-5,-1, 0, 0,-3,-4,-2},
		   /*M*/{-1, 0,-2,-3,-5,-1,-2,-3,-2, 2, 4, 0, 6, 0,-2,-2,-1,-4,-2, 2},
		   /*F*/{-4,-4,-4,-6,-4,-5,-5,-5,-2, 1, 2,-5, 0, 9,-5,-3,-2, 0, 7,-1},
		   /*P*/{ 1, 0,-1,-1,-3, 0,-1,-1, 0,-2,-3,-1,-2,-5, 6, 1, 0,-6,-5,-1},
		   /*S*/{ 1, 0, 1, 0, 0,-1, 0, 1,-1,-1,-3, 0,-2,-3, 1, 3, 1,-2,-3,-1},
		   /*T*/{ 1,-1, 0, 0,-2,-1, 0, 0,-1, 0,-2, 0,-1,-2, 0, 1, 3,-5,-3, 0},
		   /*W*/{-6, 2,-4,-7,-8,-5,-7,-7,-3,-5,-2,-3,-4, 0,-6,-2,-5,17, 0,-6},
		   /*Y*/{-3,-4,-2,-4, 0,-4,-4,-5, 0,-1,-1,-4,-2, 7,-5,-3,-3, 0,10,-2},
		   /*V*/{ 0,-2,-2,-2,-2,-2,-2,-1,-2, 4, 2,-2, 2,-1,-1,-1, 0,-6,-2, 4},
				};
				return matrix;
	}
	
	public int[][] getBlosum62(){
		
		final int[][] matrix = new int[][]
				{
		      /*A  B  C  D  E  F  G  H  I  J  K  L  M  N  O  P  Q  R  S  T  U  V  W  X  Y  Z  *  -*/
		/*A*/ { 4,-2, 0,-2,-1,-2, 0,-2,-1,-4,-1,-1,-1,-2,-4,-1,-1,-1, 1, 0, 0, 0,-3, 0,-2,-1,-4,-4},
		/*B*/ {-2, 4,-3, 4, 1,-3,-1, 0,-3,-4, 0,-4,-3, 3,-4,-2, 0,-1, 0,-1,-3,-3,-4,-1,-3, 1,-4,-4},
		/*C*/ { 0,-3, 9,-3,-4,-2,-3,-3,-1,-4,-3,-1,-1,-3,-4,-3,-3,-3,-1,-1, 9,-1,-2,-2,-2,-3,-4,-4},
		/*D*/ {-2, 4,-3, 6, 2,-3,-1,-1,-3,-4,-1,-4,-3, 1,-4,-1, 0,-2, 0,-1,-3,-3,-4,-1,-3, 1,-4,-4},
		/*E*/ {-1, 1,-4, 2, 5,-3,-2, 0,-3,-4, 1,-3,-2, 0,-4,-1, 2, 0, 0,-1,-4,-2,-3,-1,-2, 4,-4,-4},
		/*F*/ {-2,-3,-2,-3,-3, 6,-3,-1, 0,-4,-3, 0, 0,-3,-4,-4,-3,-3,-2,-2,-2,-1, 1,-1, 3,-3,-4,-4},
		/*G*/ { 0,-1,-3,-1,-2,-3, 6,-2,-4,-4,-2,-4,-3, 0,-4,-2,-2,-2, 0,-2,-3,-3,-2,-1,-3,-2,-4,-4},
		/*H*/ {-2, 0,-3,-1, 0,-1,-2, 8,-3,-4,-1,-3,-2, 1,-4,-2, 0, 0,-1,-2,-3,-3,-2,-1, 2, 0,-4,-4},
		/*I*/ {-1,-3,-1,-3,-3, 0,-4,-3, 4,-4,-3, 2, 1,-3,-4,-3,-3,-3,-2,-1,-1, 3,-3,-1,-1,-3,-4,-4},
		/*J*/ {-4,-4,-4,-4,-4,-4,-4,-4,-4, 1,-4,-4,-4,-4,-4,-4,-4,-4,-4,-4,-4,-4,-4,-4,-4,-4,-4,-4},
		/*K*/ {-1, 0,-3,-1, 1,-3,-2,-1,-3,-4, 5,-2,-1, 0,-4,-1, 1, 2, 0,-1,-3,-2,-3,-1,-2, 1,-4,-4},
		/*L*/ {-1,-4,-1,-4,-3, 0,-4,-3, 2,-4,-2, 4, 2,-3,-4,-3,-2,-2,-2,-1,-1, 1,-2,-1,-1,-3,-4,-4},
		/*M*/ {-1,-3,-1,-3,-2, 0,-3,-2, 1,-4,-1, 2, 5,-2,-4,-2, 0,-1,-1,-1,-1, 1,-1,-1,-1,-1,-4,-4},
		/*N*/ {-2, 3,-3, 1, 0,-3, 0, 1,-3,-4, 0,-3,-2, 6,-4,-2, 0, 0, 1, 0,-3,-3,-4,-1,-2, 0,-4,-4},
		/*O*/ {-4,-4,-4,-4,-4,-4,-4,-4,-4,-4,-4,-4,-4,-4, 1,-4,-4,-4,-4,-4,-4,-4,-4,-4,-4,-4,-4,-4},
		/*P*/ {-1,-2,-3,-1,-1,-4,-2,-2,-3,-4,-1,-3,-2,-2,-4, 7,-1,-2,-1,-1,-3,-2,-4,-2,-3,-1,-4,-4},
		/*Q*/ {-1, 0,-3, 0, 2,-3,-2, 0,-3,-4, 1,-2, 0, 0,-4,-1, 5, 1, 0,-1,-3,-2,-2,-1,-1, 3,-4,-4},
		/*R*/ {-1,-1,-3,-2, 0,-3,-2, 0,-3,-4, 2,-2,-1, 0,-4,-2, 1, 5,-1,-1,-3,-3,-3,-1,-2, 0,-4,-4},
		/*S*/ { 1, 0,-1, 0, 0,-2, 0,-1,-2,-4, 0,-2,-1, 1,-4,-1, 0,-1, 4, 1,-1,-2,-3, 0,-2, 0,-4,-4},
		/*T*/ { 0,-1,-1,-1,-1,-2,-2,-2,-1,-4,-1,-1,-1, 0,-4,-1,-1,-1, 1, 5,-1, 0,-2, 0,-2,-1,-4,-4},
		/*U*/ { 0,-3, 9,-3,-4,-2,-3,-3,-1,-4,-3,-1,-1,-3,-4,-3,-3,-3,-1,-1, 9,-1,-2,-2,-2,-3,-4,-4},
		/*V*/ { 0,-3,-1,-3,-2,-1,-3,-3, 3,-4,-2, 1, 1,-3,-4,-2,-2,-3,-2, 0,-1, 4,-3,-1,-1,-2,-4,-4},
		/*W*/ {-3,-4,-2,-4,-3, 1,-2,-2,-3,-4,-3,-2,-1,-4,-4,-4,-2,-3,-3,-2,-2,-3,11,-2, 2,-3,-4,-4},
		/*X*/ { 0,-1,-2,-1,-1,-1,-1,-1,-1,-4,-1,-1,-1,-1,-4,-2,-1,-1, 0, 0,-2,-1,-2,-1,-1,-1,-4,-4},
		/*Y*/ {-2,-3,-2,-3,-2, 3,-3, 2,-1,-4,-2,-1,-1,-2,-4,-3,-1,-2,-2,-2,-2,-1, 2,-1, 7,-2,-4,-4},
		/*Z*/ {-1, 1,-3, 1, 4,-3,-2, 0,-3,-4, 1,-3,-1, 0,-4,-1, 3, 0, 0,-1,-3,-2,-3,-1,-2, 4,-4,-4},
		/***/ {-4,-4,-4,-4,-4,-4,-4,-4,-4,-4,-4,-4,-4,-4,-4,-4,-4,-4,-4,-4,-4,-4,-4,-4,-4,-4, 1,-4},
		/*-*/ {-4,-4,-4,-4,-4,-4,-4,-4,-4,-4,-4,-4,-4,-4,-4,-4,-4,-4,-4,-4,-4,-4,-4,-4,-4,-4,-4, 1},
				};
		
		return matrix;
	}
	

}
