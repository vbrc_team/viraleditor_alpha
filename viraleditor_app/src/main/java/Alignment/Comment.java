/*
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package Alignment;


import javafx.scene.paint.Color;

/**
 * A comment that the user may enter.
 * Stored as a list with each sequence.
 */
public class Comment {		//currently contains getters and setters for all properties, change as needed.
    private String title;
    private String commentString;
    private String category;
    private Color textColor;
    private Color backgroundColor;
    private int start;
    private int end;
    public int paragraph;


	public Comment(String name, String commentString, String category, Color textColor, Color backgroundColor, int start, int end) {
		this.title = name;
		this.commentString = commentString;
		this.category = category;
		this.textColor = textColor;
		this.backgroundColor = backgroundColor;
		this.start = start;
		this.end = end;
	}
	
	public String toString() {
        return "From position " + start + " to " + end + " --> " + title + ": " + commentString;
    }
	
	public void edit(String name, String commentString, Color textColor, Color backgroundColor){
		this.title = name;
		this.commentString = commentString;
		this.textColor = textColor;
		this.backgroundColor = backgroundColor;
	}

	/**
	 * @return the title of the comment
	 */
	protected String getTitle() {
		return title;
	}

	/**
	 * @return the text within the comment
	 */
	protected String getCommentText() {
		return commentString;
	}

	/**
	 * @return the category of the comment 
	 */
	protected String getCategory() {
		return category;
	}

	/**
	 * @return the text color of the comment
	 */
	protected Color getTextColor() {
		return textColor;
	}

	/**
	 * @return the background color of the comment
	 */
	protected Color getBackgroundColor() {
		return backgroundColor;
	}

	/**
	 * @return the start point of the comment
	 */
	protected int getStart() {
		return start;
	}
	
	/**
	 * @return the end point of the comment
	 */
	protected int getEnd() {
		return end;
	}
	
	/**
	 * @return the paragraph number
	 */
	protected int getParagraph() {
		return paragraph;
	}

	/**
	 * @param paragraph the paragraph number
	 */
	protected void setParagraph(int paragraph) {
		this.paragraph = paragraph;
	}

}
