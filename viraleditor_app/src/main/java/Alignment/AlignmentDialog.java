package Alignment;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import Alignment.structures.AlignmentSequence;
import Shared.AbstractDialog;
import Shared.Performance;
import Shared.Windows.DatabasePopupDialog;
import Shared.interchange.*;
import VisualSummary.VisualSummaryComposer;
import VisualSummary.VisualSummaryDialog;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.ComboBox;
import javafx.scene.control.TextInputDialog;
import javafx.scene.control.Alert;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.control.Tooltip;
import javafx.scene.image.ImageView;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.AnchorPane;
import javafx.scene.effect.*;
import javafx.stage.FileChooser;
import javafx.stage.Stage;
import javafx.stage.WindowEvent;
import javafx.util.StringConverter;
import openapi.api.tools.DefaultApi;
import openapi.invoker.tools.ApiClient;
import openapi.invoker.tools.ApiException;
import openapi.invoker.tools.ApiResponse;
import openapi.model.tools.Created;
import openapi.model.tools.Request;
import openapi.model.tools.Sequence;
import openapi.model.tools.Status;
import java.awt.datatransfer.StringSelection;
import java.awt.Toolkit;
import java.awt.datatransfer.Clipboard;

/**
 * Handles the UI for the Alignment window, including the two toolbars and menu items.
 */
public class AlignmentDialog extends AbstractDialog<AlignmentComposer> {
    @FXML
    private AnchorPane sequenceWindow;
    @FXML
    private ImageView moveTop;
    @FXML
    private ImageView moveUp;
    @FXML
    private ImageView moveDown;
    @FXML
    private ImageView moveBottom;
    @FXML
    private ImageView show;
    @FXML
    private ImageView hide;
    @FXML
    private ImageView reorderSequences;
    @FXML
    private ImageView prevGap;
    @FXML
    private ImageView nextGap;
    @FXML
    private ImageView prevDiff;
    @FXML
    private ImageView nextDiff;
    @FXML
    private ImageView prevComment;
    @FXML
    private ImageView nextComment;
    @FXML
    private Button toggleStrand;
    
    //toolbar
    @FXML
    private ImageView file;
    @FXML
    private ImageView save;
    @FXML
    private ImageView editMouseMode;
    @FXML
    private ImageView skipForward20;
    @FXML
    private ImageView skipForward1kbp;
    @FXML
    private ImageView skipForward10kbp;
    @FXML
    private ImageView skipBackward20;
    @FXML
    private ImageView skipBackward1kbp;
    @FXML
    private ImageView skipBackward10kbp;

    @FXML
    private ComboBox<Feature> geneBox;
    @FXML
    private Button strandButton;

    private Performance<SequencesDialog, SequenceEditorMode> sequencesWindow;
    private SequenceEditorMode sequencesComposer;
    private SequenceTools AAConverter = new SequenceTools();
    
   

    
    /**
     * True when the reorder windows is open and false when closed.
     */
    private boolean reorderOpen;

    /**
     * True when show sequences window is open and false when closed.
     */
    private boolean showOpen;

    /**
     * True when go to location window is open and false when closed.
     */
    private boolean showGoToLocation;

    /**
     * True when the positive strand is displayed and false otherwise.
     */
    private boolean forward;

    private ObservableList<Feature> genes;

    @Override
    public void mountChildPerformances() {
        try {        	
        	// Sequence Window Mount

            sequencesComposer = new SequenceEditorMode();
            sequencesWindow = new Performance<>("sequencesWindow", stage, sequencesComposer, "Alignment/SequencesDialog.fxml");
            performance.mount(sequenceWindow, sequencesWindow);

            // Add sequence references to each composer
            if (composer.getPreference().equals("Selected Genes annotated onto sequences")) {
            	
            	sequencesComposer.setAnnotated();
            	
            }
            
            for (Reference reference : composer.getReferences()) {
            	if (composer.getPreference().equals("Display only Genes as Amino Acids")) {
            		String Aminos = AAConverter.getAAString(reference.getSequence(), 1);
            		AminoAcidSequence AASeq = new AminoAcidSequence(Aminos);
            		Reference AARef = reference.builder()
            		.withReference(reference)
            		.withOrigin(AASeq)
            		.build();
            		sequencesComposer.addSequence(AARef);
            	}
            	else {
            		sequencesComposer.addSequence(reference);
            	}
            }
            if (composer.getPreference().equals("Selected Genes annotated onto sequences")) {
            	
            	sequencesComposer.setAnnotations(composer.getAnnotations());
            	
            }
            
            
            

        } catch (IOException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
    }   
    
    @Override
    public void initializeDialog() {
        stage.setTitle("Alignment");
        setupToolTips();
        initGeneBox();
        forward = true;
    }   

    /**
     * Sets up tooltips for buttons in toolbars
     */
    private void setupToolTips() {
        Tooltip.install(file, new Tooltip("Load Sequences"));
        Tooltip.install(save,  new Tooltip("Save alignment to device"));
        Tooltip.install(editMouseMode, new Tooltip("Go to 'Edit' with Mouse Mode"));
        Tooltip.install(skipForward20, new Tooltip("Skip Right 20"));
        Tooltip.install(skipForward1kbp, new Tooltip("Skip Right 1k"));
        Tooltip.install(skipForward10kbp, new Tooltip("Skip Right 10k"));
        Tooltip.install(skipBackward20, new Tooltip("Skip Left 20"));
        Tooltip.install(skipBackward1kbp, new Tooltip("Skip Left 1k"));
        Tooltip.install(skipBackward10kbp, new Tooltip("Skip Left 10k"));
        Tooltip.install(moveBottom, new Tooltip("Move Marked Sequence(s) to Bottom"));
        Tooltip.install(moveDown, new Tooltip("Move Marked Sequence(s) Down"));
        Tooltip.install(moveTop, new Tooltip("Move Marked Sequence(s) to Top"));
        Tooltip.install(moveUp, new Tooltip("Move Marked Sequence(s) Up"));
        Tooltip.install(hide, new Tooltip("Hide Marked Sequence(s)"));
        Tooltip.install(show, new Tooltip("Set Sequence Filter"));
        Tooltip.install(reorderSequences, new Tooltip("Reorder Sequences"));
    }

    /**
     * Opens a new window so the user can select viruses to add, then Alignment Dialog is updated with the selection
     */
    @FXML
    private void loadSequencesFromDatabase() {
        List<Reference> selectedViruses = new ArrayList<>();
        selectedViruses = DatabasePopupDialog.OpenPopUp(composer.database);

        /*ToDo when we have real data
            handler.DatabaseToInterchange (selectedViruses)
            composer.addSequences( interchange );

        */

    }

    /**
     * Adds local file from user's machine via menu item.
     * 
     * @throws IOException
     */
    @FXML
    private void addLocalSequence() throws IOException {
        FileChooser fc = new FileChooser();
        fc.getExtensionFilters()
          .addAll(new FileChooser.ExtensionFilter("All Sequences", "*.gbk", "*.gb", "*.fasta", "*.fa"),
                  new FileChooser.ExtensionFilter("GBK", "*.gbk", "*.gb"),
                  new FileChooser.ExtensionFilter("FASTA", "*.fasta", "*.fa"));
        File readFile = fc.showOpenDialog(stage);
        if (readFile != null) {
            for (Reference reference : composer.readFile(readFile.toPath()).getReferences()) {
                sequencesComposer.addSequence(reference);
                composer.addReference(reference);
            }
        }
    }

    @FXML
    private void moveSelectedSequence(MouseEvent event) {
        if (event.getSource().equals(moveUp)) {
            sequencesComposer.moveSelectedSquencesUp();
        } else if (event.getSource().equals(moveDown)) {
            sequencesComposer.moveSelectedSquencesDown();
        } else if (event.getSource().equals(moveTop)) {
            sequencesComposer.moveSelectedSequencesToFront();
        } else if (event.getSource().equals(moveBottom)) {
            sequencesComposer.moveSelectedSequencesToEnd();
        }
    }

    /**
     * Saves the current alignment to a path specified by user
     */
    
    @FXML
    public void saveAs() {
    	String[] NewStrings = new String[sequencesComposer.getElements().size()];
    	int i = 0;
    	for (AlignmentSequence Sequence: sequencesComposer.getElements()) {
    		NewStrings[i] = Sequence.getUngappedString();
    		i++;
    		
    	}
    	
    	sequencesComposer.saveAs(NewStrings);
    }
    
    /**
     * opens a new Alignment window
     */
    
    @FXML
    public void newAlignment(){
    	
    	try {
    		Interchange newinterchange = Interchange.builder()
    				
    			.build();
    		
    		AlignmentComposer newComposer = new AlignmentComposer(newinterchange, composer.database, new ArrayList<Reference>(), "Sequences and Genes Separate");
    		Performance<AlignmentDialog, AlignmentComposer> p = new Performance<>(
                "Alignment_main", new Stage(), newComposer, "Alignment/Alignment.fxml");
    		p.render();
    	}
    	catch (IOException e) {
    		e.printStackTrace();
    	}
    }
    
    /** Quits the alignment window upon selection
     * 
     */
    
    @FXML
    public void OnQuit() {
    	stage.close();
    }
    
    /**
     * closes current alignments, does not close window.
     */
    
    @FXML
    public void CloseAlignment() {
    	List<Reference> NewRefList = composer.getReferences();
    	for (Reference reference : NewRefList) {
    		
    	   	composer.removeReference(composer.getReferences(), reference);
    	   	sequencesComposer.removeSequence(reference);
    	}
    }
    
    
   /**
     * updates the edit mouse mode toggle to on/off, allowing for insertion or deletion of gaps using the mouse.
     */
    
    @FXML
    public void indelGapWithMouse(MouseEvent event){
    	boolean IsToggled = sequencesComposer.updateEditMouseMode();
    	if (IsToggled) {	
    		Alert EditAlert = new Alert(AlertType.INFORMATION, "There will be a noticeable delay between the GUI and the users actions. Please be patient and refrain from erratically moving sequences.");
    		EditAlert.setTitle("'Edit' Mouse Mode warning");
    		EditAlert.show();
    		
    	}
    	if (IsToggled) {
    		ColorAdjust DarkColour = new ColorAdjust();
    		DarkColour.setBrightness(.25);
    		editMouseMode.setEffect(DarkColour);
    	}
    	else {
    		editMouseMode.setEffect(null);
    	}
    	

	}
    	
    
    /** Determines how far to skip positive/backward in sequence
     * @param event - type of mouse event
     */
    @FXML
    private void skip(MouseEvent event) {
        int shift = 0;
        if (event.getSource() == skipBackward20) {
            shift = -20;
        } else if (event.getSource() == skipBackward1kbp) {
            shift = -1_000;
        } else if (event.getSource() == skipBackward10kbp) {
            shift = -10_000;
        } else if (event.getSource() == skipForward20) {
            shift = 20;
        } else if (event.getSource() == skipForward1kbp) {
            shift = 1_000;
        } else if (event.getSource() == skipForward10kbp) {
            shift = 10_000;
        }

        sequencesWindow.getController().shiftColumn(shift);
    }
    
    /**
     * Skips to next gap in sequences.
     *
     * @param event
     */
    @FXML
    void skipToNextGap(MouseEvent event) {
        sequencesComposer.skipToNextGap();
    }

    /**
     * Skips to previous gap in sequences.
     *
     * @param event
     */
    @FXML
    void skipToPreviousGap(MouseEvent event) {
        sequencesComposer.skipToPreviousGap();
    }

    @FXML
    void toggleReadingFrames(MouseEvent event) {
        sequencesComposer.toggleShowTranslation();
    }

    /**
     * Skips to next difference in sequences.
     */
    @FXML
    void skipToNextDifference(MouseEvent event) {
        sequencesComposer.skipToNextDifference();
    }

    /**
     * Skips to previous difference in sequences.
     */
    @FXML
    void skipToPreviousDifference(MouseEvent event) {
        sequencesComposer.skipToPreviousDifference();
    }
    
    /** skips to next/previous difference in sequence
     * @param event - type of mouse event
     */
    @FXML
    void skipToComment(MouseEvent event) {
        throw new UnsupportedOperationException("Not implemented");
    }
    
    
    /**
     * Opens menu to load file into alignment window.
     * 
     * @param event - mouse event by user clicking on file icon
     * @throws IOException if file fails to load
     */
    @FXML
    private void openFile(MouseEvent event) throws IOException {
        addLocalSequence();
    }

    /**
     * Hides selected sequences in sequence window and sequence list unless the show
     * windows window is open.
     */
    @FXML
    private void hideSequences() {
        if (!showOpen) {
            sequencesComposer.hideSelected();
        }
    }

    /**
     * Loads the show sequences window and opens it.
     *
     * @throws IOException if window fails to load
     */
    @FXML
    private void showSequences() throws IOException {
        if (!showOpen) {
            showOpen = true;
            Stage showStage = new Stage();
            showStage.setOnCloseRequest(evt -> showOpen = false);
            Performance<ShowSequencesDialog, ShowSequencesComposer> perf;
            ShowSequencesComposer composer = new ShowSequencesComposer();
            perf = new Performance<>("showSequences", showStage, composer, "Alignment/ShowSequencesAlignment.fxml");
            composer.setSequencesComposer(sequencesComposer);
            perf.render();
        }
    }

    /**
     * If the reorder sequences window is closed then load and open it.
     *
     * @throws IOException if window fails to load
     */
    @FXML
    private void reorderSequences() throws IOException {
        if (!reorderOpen) {
            reorderOpen = true;
            Performance<ReorderSequencesDialog, ReorderSequencesComposer> perf;
            Stage reorderStage = new Stage();
            reorderStage.setOnCloseRequest(evt -> reorderOpen = false);
            perf = new Performance<>("reorder", reorderStage, new ReorderSequencesComposer(sequencesComposer),
                    "Alignment/ReorderSequences.fxml");
            perf.render();
        }
    }

    /**
     * If the go to location window is closed then load and open it.
     *
     * @throws IOException if window fails to load
     */
    @FXML
    private void goToLocation() throws IOException {
        if (!showGoToLocation) {
            showGoToLocation = true;
            Performance<GoToLocationDialog, GoToLocationComposer> perf;
            Stage goToStage = new Stage();
            GoToLocationComposer goToComposer = new GoToLocationComposer(sequencesComposer);
            perf = new Performance<>("goto", goToStage, goToComposer,
                    "Alignment/GoToLocationAlignment.fxml");
            goToComposer.setSequences();
            goToStage.addEventFilter(WindowEvent.WINDOW_CLOSE_REQUEST, evt -> showGoToLocation = false);
            perf.render();
        }
    }

    public void setDatabase(Family database) {
        composer.database = database;
    }

    /**
     * Marks all sequences in the sequence list.
     */
    @FXML
    private void markAllSequences() {
        for (int row = 0; row < sequencesComposer.getNumberOfRows(); row++) {
            sequencesComposer.selectRow(row);
        }
    }

    /**
     * Unmarks/unselects all sequences in sequence list.
     */
    @FXML
    private void unmarkSequences() {
        for (int row = 0; row < sequencesComposer.getNumberOfRows(); row++) {
            sequencesComposer.selectRow(row);
            sequencesComposer.toggleRowSelect(row);
        }
    }

    /**
     * Selects the whole region (whole sequence) for the sequences currently selected.
     */
    @FXML
    private void selectWholeRegion() {
        sequencesComposer.selectRange(0, sequencesComposer.getNumberOfColumns());
    }

    @FXML
    private void selectRegion() {

    }
    
    /**
     * updates the colour scheme back to default.
     */
    
    @FXML
    private void defaultColours() {
    	sequencesComposer.updateColourScheme("Default");
    }
    
    /**
     * Updates colour scheme to colour any Nucleotide Black if it is the same as the nucleotide in that position in the first sequence.
     */
    @FXML
    private void hideResidualIdentical() {
    	sequencesComposer.updateColourScheme("HideResidualIdentical");
    }
    
    /**
     * updates colour scheme to colour any A.A red if its hydrophobic, blue if hydrophilic, and purple if neutral
     */
    @FXML
    private void hydrophobicColours() {
    	sequencesComposer.updateColourScheme("Hydrophobicity");
    }
    
    /**
     * updates colour scheme to colour A.As darker if there are more of the same A.A in the same colour
     */
    @FXML
    private void similarityColours() {
    	sequencesComposer.updateColourScheme("Similarity");
    }
    
    /**
     * updates colour scheme to colour A.As relative to their blosum62 scores
     */
    @FXML
    private void blosum62Colours() {
    	stage.setFullScreen(true);
    	sequencesComposer.updateColourScheme("Blosum62");
    	stage.setFullScreen(false);
    }
    

    /**
     * updates colour scheme to colour A.As relative to their PAM250 scores
     */
    
    @FXML
    private void pam250Colours() {
    	stage.setFullScreen(true);
    	sequencesComposer.updateColourScheme("Pam250");
    	stage.setFullScreen(false);
    }
    
    @FXML
    private void customColours(){
    	sequencesComposer.updateColourScheme("Custom");
    }
    
    /**
     * Shows a window that displays the current gap counts of all the sequences in the window.
     */
    
    @FXML
    private void  viewGapCount() {
    	sequencesComposer.viewGapCount();
    }
    
    /**
     * Opens a new window which reports the number of columns and the number of columns
     *  that contain; a gap, a single nucleotide, two nucleotides (all pairs listed), 
     *  three nucleotides, four nucleotides.
     */
    
    @FXML
    private void getCounts() {
    	sequencesComposer.getCounts();
    	}
    
    /**
     * Opens a new window which displays the number of columns that contain two nucleotides.
     *  The counts listed under it represent the number of SNPs between two sequences. 
     */
    
    @FXML
    private void getCountsTop2() {
    	sequencesComposer.getCountsTop2();
    }
    
    /**
     * searches sequences for specified subsequence, with a specified number of mismatches
     * @throws IOException
     */
    @FXML
    private void searchFuzzyMotif() throws IOException {
    	
    	String[] seqandMismatch = SearchFuzzyMotifSelection.openPopUp();
    	List<String> fuzzyMotifData = sequencesComposer.searchFuzzyMotif(seqandMismatch);
    	if (seqandMismatch[0] != "") {
    		SearchFuzzyMotifComposer sfmComposer = new SearchFuzzyMotifComposer(seqandMismatch, fuzzyMotifData, sequencesWindow, sequencesComposer);
        	new Performance<>("SFM_main", new Stage(), sfmComposer, "SearchFuzzyMotif.fxml").render();
    	}
    	else {
    	}
    	
    }
    
    @FXML
    private void contentGraph() throws IOException {
        char[] searchParameters = ContentGraphBaseSelection.openPopUp();
        if(searchParameters != null) {
            NucleotideContentGraphComposer ncgComposer = new NucleotideContentGraphComposer(composer.getReferences(),
                    searchParameters);
            new Performance<>("NCG_main", new Stage(), ncgComposer, "NucleotideContentGraph.fxml").render();
        }
    }
    
    @FXML
    private void visualSummary() throws IOException {
        VisualSummaryComposer visualSummaryComposer = new VisualSummaryComposer(composer.getReferences());
        Performance<VisualSummaryDialog, VisualSummaryComposer> visualSummary = new Performance<>("visualSummary", new Stage(), visualSummaryComposer, "VisualSummary.fxml");
        visualSummary.render();
    }
    
    /**
     * Loads the comment window and opens it.
     */
    @FXML
    private void addComment(ActionEvent event) throws IOException {
        throw new UnsupportedOperationException("Not implemented");
    }
    
    /**
     * Opens the comment window to the currently selected comment.
     */
    @FXML
    private void editComment(ActionEvent event) throws IOException {
        throw new UnsupportedOperationException("Not implemented");
    }

    /**
     * Deletes the currently selected comment.
     */
    @FXML
    private void deleteComment(ActionEvent event) throws IOException {
        throw new UnsupportedOperationException("Not implemented");
    }

    /**
     * Initializes the gene box to display features.
     */
    private void initGeneBox() {
        geneBox.setConverter(new StringConverter<Feature>() {
            @Override
            public String toString(Feature feature) {
                if (feature instanceof Gene) {
                    return ((Gene) feature).getGene();
                } else if (feature instanceof Annotation) {
                    return ((Annotation) feature).getGeneAbbreviation();
                } else {
                    return "Unknown";
                }
            }

            @Override
            public Feature fromString(String string) {
                throw new UnsupportedOperationException();
            }
        });
        genes = sequencesComposer.getGenes();
        geneBox.setItems(genes.filtered(gene -> !gene.getIntervals().get(0).isComplement()));
    }

    /**
     * When the user selects the Edit > Insert Gaps option, this is sent to the insertGaps
     * function in (SequenceWindow_main.getController()). {@link (SequenceWindow_main.getController())#insertGaps()}
     */
    @FXML
    public void insertGaps() {
        TextInputDialog gaps = new TextInputDialog();
        gaps.setContentText("What is the gap length you would like to insert?");
        gaps.setTitle("Insert Gaps");

        Optional<String> length = gaps.showAndWait();
        if (length.isPresent()) {
            sequencesComposer.insertGaps(Integer.valueOf(length.get()));
        }
        genes = sequencesComposer.getGenes();
        geneBox.setItems(genes.filtered(gene -> !gene.getIntervals().get(0).isComplement()));
    }

    /**
     * When the user selects the Edit > Remove > Gaps in Selected Region(s), this is sent to the removeGaps
     * function in (SequenceWindow_main.getController()). {@link (SequenceWindow_main.getController())#removeGaps()}
     */
    @FXML
    public void removeGaps() {
        sequencesComposer.removeGaps();
        genes = sequencesComposer.getGenes();
        geneBox.setItems(genes.filtered(gene -> !gene.getIntervals().get(0).isComplement()));
    }
    
    /**
     * removes the selected sequences from the alignment window
     */
    @FXML
    public void removeSelectedSequences() {
    	List<Reference> NewRefList = composer.getReferences();
    	for (Reference ref: NewRefList) {
    		AlignmentSequence SeqtoRemove = sequencesComposer.isSelected(ref);
    		if (SeqtoRemove != null) {
    			composer.removeReference(composer.getReferences(), ref);
    			sequencesComposer.removeSequence(ref);
    		}
    		
    		
    	}
    	
    	
    }
    /**
     * when the user selects remove All-gap-columns, this removes all gaps that are present in every sequence
     */
    
    @FXML
    public void removeColumnGaps() {
    	sequencesComposer.removeColumnGaps();
        genes = sequencesComposer.getGenes();
        geneBox.setItems(genes.filtered(gene -> !gene.getIntervals().get(0).isComplement()));
    }
    
    /**
     * when the user selects remove Identical-Sequences, this removes all sequences that are identical.
     */
    
    @FXML
    public void removeIdenticalSequences() {
    	List<Reference> TempRefList = sequencesComposer.removeIdenticalSequences();
    	for(Reference reference: TempRefList) {
    		composer.removeReference(composer.getReferences(), reference);
    	   	sequencesComposer.removeSequence(reference);
    	}
    }

    /**
     * When the user selects the Edit > Remove > Selected Sequence Region(s), this is sent to the removeSelectedRegion
     * function in (SequenceWindow_main.getController()). {@link (SequenceWindow_main.getController())#removeSelectedRegion()}
     */
    @FXML
    public void removeSelectedRegion() {
        throw new UnsupportedOperationException("Not implemented");
    }

    /**
     * When the user selects the Edit > Copy As > Nucleotide , this is sent to the copyNucleotide
     * function in (SequenceWindow_main.getController()). {@link (SequenceWindow_main.getController())#copyNucleotide()}
     */
    @FXML
    public void copy() {
    	String CopiedString = sequencesComposer.getRegion();
    	StringSelection stringSelection = new StringSelection(CopiedString);
    	Clipboard clipboard = Toolkit.getDefaultToolkit().getSystemClipboard();
    	clipboard.setContents(stringSelection, null);
    }

    @FXML
    /**
     * Toggles the displayed strand between the 5' - > 3' and 3' -> 5' directions.
     */
    public void toggleStrand(ActionEvent event) {
        forward = !forward;
        if (forward) {
            toggleStrand.setText("5' Top 3'");
            geneBox.setItems(genes.filtered(gene -> !gene.getIntervals().get(0).isComplement()));
        } else {
            toggleStrand.setText("3' Bottom 5'");
            geneBox.setItems(genes.filtered(gene -> gene.getIntervals().get(0).isComplement()));
        }
        sequencesComposer.toggleComplement();
    }
    
    
    @FXML
    public void mafftAlignSelect(ActionEvent event) throws ApiException{
    	int answer = composer.ConfirmAlignment();
    	if (answer == 0) {
        	ArrayList<AlignmentSequence> SelectedSeq = sequencesComposer.getAllSelectedSequences();
        	if (SelectedSeq.size() > 0) {
        		doAlignment(Request.ToolEnum.MAFFT, SelectedSeq);
        	}
        	else {
        	}
    	}
    	else {
    	}
    }
    
    @FXML
    public void clustalAlignSelect(ActionEvent event) throws ApiException{
    	int answer = composer.ConfirmAlignment();
    	if (answer == 0) {
        	ArrayList<AlignmentSequence> SelectedSeq = sequencesComposer.getAllSelectedSequences();
        	if (SelectedSeq.size() > 1) {
        		doAlignment(Request.ToolEnum.CLUSTALO, SelectedSeq);
        	}
        	else {
        	}
    	}
    	else {
    	}
    }

    
    @FXML
    public void muscleAlignSelect(ActionEvent event) throws ApiException{
    	int answer = composer.ConfirmAlignment();
    	if (answer == 0) {
    		ArrayList<AlignmentSequence> SelectedSeq = sequencesComposer.getAllSelectedSequences();
        	if (SelectedSeq.size() > 0) {
        		doAlignment(Request.ToolEnum.MUSCLE, SelectedSeq);
        	}
        	else {
        	}
    	}
    	else {
    	}
    }


    @FXML
    public void mafftAlignAll(ActionEvent event) throws ApiException {
    	int answer = composer.ConfirmAlignment();
		if (answer == 0) {
	        doAlignment(Request.ToolEnum.MAFFT, sequencesComposer.getAllSequences());
		}
		else {
		}
    }

    @FXML
    public void clustaloAlignAll(ActionEvent event) throws ApiException {
    	int answer = composer.ConfirmAlignment();
		if (answer == 0) {
			doAlignment(Request.ToolEnum.CLUSTALO, sequencesComposer.getAllSequences());
		}
		else {
		}
    }

    @FXML
    public void muscleAlignAll(ActionEvent event) throws ApiException {
    	int answer = composer.ConfirmAlignment();
    	if (answer == 0) {
    		doAlignment(Request.ToolEnum.MUSCLE, sequencesComposer.getAllSequences());
    	}
    	else {
    	}
    }
    
    private void doAlignment(Request.ToolEnum tool, List<AlignmentSequence> Selected) throws ApiException {
		Alert Running = new Alert(AlertType.INFORMATION, "Alignment is running, please be patient, this window will close when alignment is complete");
		Running.setTitle("Running");
		Running.setWidth(25);;
		Running.show();
    	Request request = new Request();
        request.setTool(tool);
        for (AlignmentSequence sequence : Selected) {
        	Sequence seq = new Sequence();
        	seq.setName(sequence.getName());
        	seq.setSequence(sequence.getSequence().getSequence().replace("-", ""));
        	request.addSequencesItem(seq);
        		}

        ApiClient client = new ApiClient();
        client.setHost("4virology.net");
        client.setBasePath("/api");
        DefaultApi api = new DefaultApi(client);
        ApiResponse<Created> created = api.createRequestWithHttpInfo(request);
        ApiResponse<Status> status;
        try {
            do {
                Thread.sleep(10000);
                status = api.getRequestByIdWithHttpInfo(created.getData().getId());
            } while (!status.getData().getStatus().equals(Status.StatusEnum.COMPLETE));
        } catch (InterruptedException ex) {
            Thread.currentThread().interrupt();
            return;
        }
        ApiResponse<List<Sequence>> result = api.getResultByIdWithHttpInfo(status.getData().getId());
        List<AlignmentSequence> sequences = new ArrayList<>();
        for (Sequence seq : result.getData()) {
            List<Nucleotide> nucleotides = new ArrayList<>();
            String sequence = seq.getSequence().toUpperCase();
            for (int i = 0; i < sequence.length(); i++) {
                nucleotides.add(Nucleotide.fromCode("" + sequence.charAt(i)));
            }
            sequences.add(new AlignmentSequence(seq.getName(),
                    Reference.builder().withOrigin(new NucleotideSequence(nucleotides)).build()));
        }
        sequencesComposer.applyAlignment(sequences);
		Running.close();
    }
    
    /**
     * Runs JCodeHop tool
     */
    
    public void jCodeHop() {
    	System.out.println(composer.getAnnotations().size());
    			//TODO
    }
    
    /**
     * opens window with hyperlink to VBRC page
     */
    public void openVBRCLink() {
    	SupportWindows openVBRC = new SupportWindows();
    	Stage vbrcStage = new Stage();
    	openVBRC.start(vbrcStage, "https://4virology.net/");
    }
    
    /**
     * opens window with hyperlink to Help Manual
     */
    public void openHelpManual() {
    	SupportWindows openHelpManual = new SupportWindows();
    	Stage helpManualStage = new Stage();
    	openHelpManual.start(helpManualStage, "https://4virology.net/virology-ca-tools/base-by-base/");
    }
    
}


