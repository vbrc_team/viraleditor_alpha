package Alignment;

import Alignment.structures.AlignmentSequence;
import Shared.AbstractDialog;
import javafx.beans.property.BooleanProperty;
import javafx.beans.property.SimpleBooleanProperty;
import javafx.beans.value.ObservableValue;
import javafx.fxml.FXML;
import javafx.scene.control.ListView;
import javafx.scene.control.SelectionMode;
import javafx.scene.control.cell.CheckBoxListCell;
import javafx.util.Callback;
import javafx.util.StringConverter;

/**
 * Handles the UI for Show Sequences window.
 */
public class ShowSequencesDialog extends AbstractDialog<ShowSequencesComposer> {
    @FXML
    private ListView<AlignmentSequence> listView;

    private SequencesComposer sequencesComposer;

    /**
     * Sets up the checkbox list with the shown items selected.
     */
    public void setList(SequencesComposer sequencesComposer) {
        this.sequencesComposer = sequencesComposer;
        listView.getSelectionModel().setSelectionMode(SelectionMode.MULTIPLE);
        listView.setCellFactory(CheckBoxListCell.forListView(callback(), new StringConverter<AlignmentSequence>() {
            @Override
            public String toString(AlignmentSequence object) {
                return object.getName();
            }

            @Override
            public AlignmentSequence fromString(String string) {
                // Backwards conversion should never be done by CellFactory
                throw new UnsupportedOperationException();
            }
        }));

        listView.getItems().addAll(sequencesComposer.getAllSequences());
    }

    private Callback<AlignmentSequence, ObservableValue<Boolean>> callback() {
        return seq -> {
            BooleanProperty observable = new SimpleBooleanProperty();
            observable.setValue(!seq.isHidden());
            observable.addListener((obs, wasSelected, isNowSelected) -> {
                int index = sequencesComposer.getAllSequences().indexOf(seq);
                if (Boolean.TRUE.equals(isNowSelected)) {
                    if (index >= 0) {
                        sequencesComposer.reveal(index);
                    }
                } else {
                    if (index >= 0) {
                        sequencesComposer.hide(index);
                    }
                }
            });
            return observable;
        };
    }

    @FXML
    private void selectAll() {
        composer.selectAll();
    }

    @FXML
    private void close() {
        stage.close();
    }

    @Override
    public void initializeDialog() {
    }

    @Override
    public void mountChildPerformances() {
    }
}
