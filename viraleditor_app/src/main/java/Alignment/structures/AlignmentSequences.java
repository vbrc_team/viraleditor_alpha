package Alignment.structures;

import java.util.*;
import java.util.stream.Collectors;

import Shared.interchange.Annotation;
import Shared.interchange.Feature;
import Shared.interchange.Gene;
import javafx.beans.Observable;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.collections.transformation.FilteredList;

public class AlignmentSequences implements Orderable<AlignmentSequence> {
    /**
     * All stored sequences.
     */
    private ObservableList<AlignmentSequence> allSequences;

    /**
     * All sequences that are not hidden.
     */
    private FilteredList<AlignmentSequence> visibleSequences;

    /**
     * All genes in selected sequences.
     */
    private ObservableList<Feature> selectedGenes;

    private int maxColumnLength;
    private int maxNameLength;
    private List<Integer> rowToSequence = new ArrayList<>();
    private List<Integer> rowToOffset = new ArrayList<>();
    private int selectStart;
    private int selectEnd;

    private boolean positive;
    private boolean annotations = false;

    /**
     * The total number of rows stored by every sequence in sequences.
     */
    private int numberOfRows;

    public AlignmentSequences() {
        allSequences = FXCollections.observableArrayList(item -> new Observable[] { item.getHiddenValueProperty() });
        visibleSequences = allSequences.filtered(this::filterVisible);
        selectedGenes = FXCollections.observableArrayList();
        positive = true;
    }

    private boolean filterVisible(AlignmentSequence sequence) {
        return !sequence.getHiddenValueProperty().get();
    }

    /**
     * Adds sequence to end of alignment sequences.
     *
     * @param sequence sequence to add
     */
    public void add(AlignmentSequence sequence) {
        for (int i = 0; i < sequence.getNumberOfRows(); i++) {
        	
            rowToSequence.add(allSequences.size());
            rowToOffset.add(i);
        }
        allSequences.add(sequence);

        maxColumnLength = findMaxSequenceLength();
        maxNameLength = findMaxNameLength();
        numberOfRows += sequence.getNumberOfRows();
        updateGenes();
    }
    
    /**
     * removes a sequence
     * @param sequence
     */
    
    public void remove(AlignmentSequence sequence) {
    	int i = 0;
    	int allSequencesLen = allSequences.size();
    	while (i < allSequencesLen) {
    		if (allSequences.get(i).getName() == sequence.getName()) {
    			allSequences.remove(i);
    	    	maxColumnLength = findMaxSequenceLength();
    	    	maxNameLength = findMaxNameLength();
    	    	numberOfRows -= sequence.getNumberOfRows();
    	    	allSequencesLen--;
    		}
    		else {
    			i++;
    		}
    	}
    	
    	List<Integer> newRowToSequence = new ArrayList<Integer>();
    	List<Integer> newRowToOffset = new ArrayList<Integer>();
    	List<AlignmentSequence> TempAllSeq = new ArrayList<AlignmentSequence>();
    	
    	for (AlignmentSequence Seq : allSequences) {
    		for (int j = 0; j < Seq.getNumberOfRows(); j++) {
    			newRowToSequence.add(TempAllSeq.size());
    			newRowToOffset.add(j);
    			
    		}
    		TempAllSeq.add(Seq);
    	}
    	
    	
    	rowToSequence = newRowToSequence;
    	rowToOffset = newRowToOffset;
    	updateGenes();
    	
    	
    }


    /**
     * Returns the length of the longest NamedElements sequence.
     *
     * @return the length of the longest NamedElements sequence
     */
    private int findMaxSequenceLength() {
        int max = 0;
        for (AlignmentSequence sequence : visibleSequences) {
            if (sequence.getNumberOfColumns() > max) {
                max = sequence.getNumberOfColumns();
            }
        }

        return max;
    }

    /**
     * Returns the length of the longest NamedElements name.
     *
     * @return the length of the longest NamedElements name
     */
    private int findMaxNameLength() {
        int max = 0;
        for (AlignmentSequence sequence : visibleSequences) {
            if (sequence.getName().length() > max) {
                max = sequence.getName().length();
            }
        }

        return max;
    }

    /**
     * Returns a list of named elements starting at a specific element in the
     * sequences.
     *
     * @param rowStart    start point to get n rows
     * @param columnStart start point to get m columns
     * @param n           number of rows
     * @param m           number of columns
     * @return list of n named elements with m rows each
     * @throws IndexOutOfBoundsException if (rowStart + n < 0 || rowStart + n >=
     *                                   getNumberOfRows()) or ( columnStart + m < 0
     *                                   || columnStart + m >= getNumberOfColumns())
     */
    public List<NamedElements> get(int rowStart, int columnStart, int n, int m) {
        List<NamedElements> buffer = new ArrayList<>();
        for (int row = rowStart; row < rowStart + n; row++) {
            int sequencesIndex = rowToSequence.get(row);
            int sequenceOffset = rowToOffset.get(row);
            AlignmentSequence sequence = visibleSequences.get(sequencesIndex);
            NamedElements elements = sequence.getElements(sequenceOffset, columnStart, 1, m).get(0);

            if (elements.isSelected()) {
                markSelected(elements.getElements(), columnStart, m);
            }
            buffer.add(elements);
        }

        return buffer;
    }

    public int getNumberOfRows() {
        return numberOfRows;
    }

    public int getNumberOfSequenceColumns() {
        return maxColumnLength;
    }

    public int getNumberOfNameColumns() {
        return maxNameLength;
    }

    /**
     * Select columns from start to end.
     *
     * @param start inclusive index to start selection
     * @param end   exclusive index to end selection
     */
    public void selectRange(int start, int end) {
        selectStart = start;
        selectEnd = end;
    }

    public void selectRow(int row) {
        getSequenceByRow(row).setSelected(true);
        visibleSequences = allSequences.filtered(this::filterVisible);
        updateGenes();
    }

    public void deselectRow(int row) {
        getSequenceByRow(row).setSelected(false);
        visibleSequences = allSequences.filtered(this::filterVisible);
        updateGenes();
    }

    public void toggleRowSelect(int row) {
        AlignmentSequence sequence = getSequenceByRow(row);
        sequence.setSelected(!sequence.isSelected());
    }

    private AlignmentSequence getSequenceByRow(int row) {
        int count = 0;
        for (AlignmentSequence sequence : visibleSequences) {
            if (row < count + sequence.getNumberOfRows()) {
                return sequence;
            } else {
                count += sequence.getNumberOfRows();
            }
        }

        throw new IndexOutOfBoundsException("" + row);
    }

    private void markSelected(List<Element> elements, int rowStart, int m) {
        int start = 0;
        int stop = 0;

        if (!(selectStart > rowStart + m || rowStart > selectEnd)) {
            start = Math.max(rowStart, selectStart) - rowStart;
            stop = Math.min(rowStart + m, selectEnd) - rowStart;
        }

        for (int i = start; i < stop; i++) {
            elements.get(i).select();
        }
    }

    public NamedElements getGlobalScale(int columnStart, int m) {
        List<Element> scale = new ArrayList<>();
        int column = columnStart - (columnStart % 20);

        while (column < columnStart + m) {
            if (column % 20 == 0) {
                scale.add(new Element("|", ElementType.GLOBAL_SCALE, column, column));

                String number = Integer.toString(column + 1);
                for (int i = 0; i < number.length(); i++) {
                    scale.add(new Element("" + number.charAt(i), ElementType.GLOBAL_SCALE, column + i, column + i));
                }
                column += number.length() + 1;
            } else {
                scale.add(new Element(" ", ElementType.GLOBAL_SCALE, column, column));
                column += 1;
            }
        }

        for (int number = 0; number < columnStart % 20; number++) {
            scale.remove(0);
        }

        while (scale.size() > m) {
            scale.remove(scale.size() - 1);
        }

        for (column = 0; column < scale.size(); column++) {
            selectIfInRange(scale.get(column), columnStart + column);
        }

        NamedElements elements = new NamedElements("Scale", scale);

        if (selectStart < (columnStart + m) && columnStart < selectEnd) {
            elements.setSelected();
        }
        return elements;
    }

    /**
     * Inserts m gaps into all selected sequences starting at the lowest selection
     * index.
     *
     * @param m number of gaps to insert
     */
    public void insertGaps(int m) {
        for (AlignmentSequence sequence : visibleSequences) {
            if (sequence.isSelected() && sequence.getNumberOfColumns() >= selectStart) {
                sequence.insertGap(selectStart, m);
            }
        }

        updateReference();
        updateGenes();
        maxColumnLength = findMaxSequenceLength();
    }

    /**
     * Removes gaps from selected sequences in selected range.
     */
    public void removeGaps() {
        for (AlignmentSequence sequence : visibleSequences) {
            if (sequence.isSelected() && sequence.getNumberOfColumns() >= selectStart) {
                sequence.removeGaps(selectStart, Math.min(selectEnd, sequence.getNumberOfColumns()));
            }
        }

        updateReference();
        updateGenes();
        maxColumnLength = findMaxSequenceLength();
    }
    
    /**
     * removes gaps that exist in the same position of every sequence.
     */
    
    public void removeColumnGaps() {
    	char gap = '-';
    	int startmax = visibleSequences.get(0).getSequence().getSequence().length();
    	for (AlignmentSequence sequence : visibleSequences) {
    		startmax = Math.max(startmax, sequence.getSequence().getSequence().length());
    	}
    	int i = 0;
		Boolean allgap = true;
    	while (i < startmax) {
    		allgap = true;
    		for(AlignmentSequence sequence : visibleSequences) {
    			if (i < sequence.getSequence().getSequence().length()) {
    				if ((sequence.getSequence().getSequence().charAt(i)) == gap){
    				continue;
    				}
    				else {
    					allgap = false;
    				}
    			}
    			
    		}
    		if (allgap == true) {
    			for (AlignmentSequence sequence : visibleSequences) {
    				if ((i < sequence.getSequence().getSequence().length())) {
    					sequence.removeGaps(i,1);
    				}
    				
    				else {
    					
    				}
    				
    			}
        		startmax -= 1;
        		
    		}
    		
    		else {
    			i++;
    		}
    		
    	}
    	
    	updateReference();
    	updateGenes();
    	maxColumnLength = findMaxSequenceLength();

    }
    
    /**
     * gets selected sequences from alignment window
     */
    
    public AlignmentSequence getSelectedSequences(AlignmentSequence Seq) {
    	for (AlignmentSequence sequence: visibleSequences) {
    		if (sequence.isSelected() && sequence.getName() == Seq.getName()) {
    			return sequence;
    		}
    		
    		else {
    			
    		}
    	}
    	
    	
    	return null;
    }
    
    /**
     * returns the selected range
     */
    
    public String getRegion() {
    	for (AlignmentSequence sequence : visibleSequences) {
    		if (sequence.isSelected() && sequence.getNumberOfColumns() >= selectStart) {
    			return sequence.getSequence().getSequence().substring(selectStart, selectEnd);
    		}
    	}
    	return "";
    }

    /**
     * Returns index of the next gap after start in any sequence.
     *
     * @param start index to start searching for gap
     * @return the index of next gap or start when there are no more gaps
     */
    public int getNextGap(int start) {
        int nextGap = maxColumnLength;
        boolean gapFound = false;
        for (AlignmentSequence sequence : visibleSequences) {
            String characters = sequence.getSequence().getSequence();
            int length = Math.min(nextGap, characters.length());
            for (int i = start + 1; i < length && i < nextGap; i++) {
                if (characters.charAt(i) == '-') {
                    nextGap = i;
                    gapFound = true;
                    break;
                }
            }
        }

        if (!gapFound) {
            nextGap = start;
        }
        return nextGap;
    }

    /**
     * Returns the index of the first gap before start in any sequence.
     *
     * @param start index to start searching for gap
     * @return the index of the first gap before start or start if no more gaps
     */
    public int getPreviousGap(int start) {
        int previousGap = Math.max(0, start);
        boolean gapFound = false;
        for (AlignmentSequence sequence : visibleSequences) {
            String characters = sequence.getSequence().getSequence();
            for (int i = Math.min(characters.length() - 1, previousGap - 1); i >= 0; i--) {
                if (characters.charAt(i) == '-') {
                    previousGap = i;
                    gapFound = true;
                    break;
                }
            }
        }

        if (!gapFound) {
            previousGap = start;
        }
        return previousGap;
    }

    /**
     * Returns index of the next difference after start in any sequence.
     *
     * @param start index to start searching for differences
     * @return the index of next difference or start when there are no more
     *         differences
     */
    public int getNextDifference(int start) {
        int nextDifference = start;
        if (visibleSequences.size() > 1) {
            nextDifference = maxColumnLength;
            boolean differenceFound = false;
            String base = visibleSequences.get(0).getSequence().getSequence();
            Iterator<AlignmentSequence> it = visibleSequences.listIterator(1);
            while (it.hasNext()) {
                String characters = it.next().getSequence().getSequence();
                int length = Math.min(nextDifference, characters.length());
                for (int i = start + 1; i < length && i < nextDifference; i++) {
                    if (characters.charAt(i) != base.charAt(i)) {
                        nextDifference = i;
                        differenceFound = true;
                        break;
                    }
                }
            }

            if (!differenceFound) {
                nextDifference = start;
            }
        }
        return nextDifference;
    }

    /**
     * Returns index of the previous difference before start in any sequence.
     *
     * @param start index to start searching for differences
     * @return the index of previous difference or start when there are no more
     *         differences
     */
    public int getPreviousDifference(int start) {
        int previousDifference = start;
        boolean differenceFound = false;
        if (visibleSequences.size() > 1) {
            previousDifference = Math.max(0, start);
            String base = visibleSequences.get(0).getSequence().getSequence();
            Iterator<AlignmentSequence> it = visibleSequences.listIterator(1);
            while (it.hasNext()) {
                String characters = it.next().getSequence().getSequence();
                for (int i = Math.min(characters.length() - 1, previousDifference - 1); i >= 0; i--) {
                    if (characters.charAt(i) != base.charAt(i)) {
                        previousDifference = i;
                        differenceFound = true;
                        break;
                    }
                }
            }
        }

        if (!differenceFound) {
            previousDifference = start;
        }
        return previousDifference;
    }

    public void moveSelectedSquencesUp() {
        int start = 0;
        for (; start < visibleSequences.size() && visibleSequences.get(start).isSelected(); start++)
            ;

        for (int i = start; i < visibleSequences.size(); i++) {
            if (visibleSequences.get(i).isSelected()) {
                Collections.swap(visibleSequences, i, i - 1);
            }
        }

        updateRowToLists();
        updateReference();
    }

    public void moveSelectedSquencesDown() {
        int end = visibleSequences.size();
        for (; end > 0 && visibleSequences.get(end - 1).isSelected(); end--)
            ;

        for (int i = end - 1; i >= 0; i--) {
            if (visibleSequences.get(i).isSelected()) {
                Collections.swap(visibleSequences, i, i + 1);
            }
        }

        updateRowToLists();
        updateReference();
    }

    /**
     * Moves selected sequences to front while keeping their relative order.
     */
    public void moveSelectedSequencesToFront() {
        List<AlignmentSequence> selected = new ArrayList<>();
        for (AlignmentSequence sequence : visibleSequences) {
            if (sequence.isSelected()) {
                selected.add(sequence);
            }
        }
        visibleSequences.removeAll(selected);

        for (int i = selected.size() - 1; i >= 0; i--) {
            visibleSequences.add(0, selected.get(i));
        }

        updateRowToLists();
        updateReference();
    }

    /**
     * Moves selected sequences to end while keeping their relative order.
     */
    public void moveSelectedSequencesToEnd() {
        List<AlignmentSequence> selected = new ArrayList<>();
        for (AlignmentSequence sequence : visibleSequences) {
            if (sequence.isSelected()) {
                selected.add(sequence);
            }
        }
        visibleSequences.removeAll(selected);
        visibleSequences.addAll(selected);

        updateRowToLists();
        updateReference();
    }

    /**
     * Toggles selected sequences showing translation.
     */
    public void toggleReadingFrames() {
        for (AlignmentSequence sequence : visibleSequences) {
            if (sequence.isSelected()) {
                numberOfRows -= sequence.getNumberOfRows();
                if (sequence.isShowingReadingFrames()) {
                    sequence.removeReadingFrames();
                } else {
                    sequence.addReadingFrames();
                }
                numberOfRows += sequence.getNumberOfRows();
            }
        }
        updateRowToLists();
    }

    public void toggleDirection() {
        positive = !positive;
        for (AlignmentSequence sequence : visibleSequences) {
            sequence.setComplement(!positive);
        }
    }

    private void updateRowToLists() {
        rowToOffset = new ArrayList<>();
        rowToSequence = new ArrayList<>();

        for (int i = 0; i < visibleSequences.size(); i++) {
            AlignmentSequence sequence = visibleSequences.get(i);

            for (int row = 0; row < sequence.getNumberOfRows(); row++) {
                rowToOffset.add(row);
                rowToSequence.add(i);
            }
        }
    }

    /**
     * Updates every sequence to have the first sequence as a reference.
     */
    private void updateReference() {
        if (!visibleSequences.isEmpty()) {
            AlignmentSequence reference = visibleSequences.get(0);
            for (AlignmentSequence sequence : visibleSequences) {
                sequence.setReference(reference.getSequence());
            }
        }
    }

    /**
     * Selects element if column is within selection range.
     *
     * @param element  element to mark selected if in selection range
     * @param position position element is at
     * @return element marked as selected
     *         {@code if selectStart <= position < selectEnd}
     */
    private Element selectIfInRange(Element element, int position) {
        if (position >= selectStart && position < selectEnd) {
            element.select();
        }
        return element;
    }

    /**
     * Hide selected sequences.
     */
    public void hideSelected() {
        List<AlignmentSequence> selected = visibleSequences.stream()
                                                           .filter(AlignmentSequence::isSelected)
                                                           .collect(Collectors.toList());
        selected.stream().forEach(seq -> {
            seq.hide();
            numberOfRows -= seq.getNumberOfRows();
        });

        updateRowToLists();
        updateReference();
    }

    /**
     * Hide sequence at {@code index}.
     *
     * @param sequence sequence to hide
     */
    public void hide(int index) {
        AlignmentSequence sequence = allSequences.get(index);
        if (!sequence.isHidden()) {
            sequence.hide();
            numberOfRows -= sequence.getNumberOfRows();
        }

        updateRowToLists();
        updateReference();
    }

    /**
     * Reveal sequence at {@code index}.
     */
    public void reveal(int index) {
        AlignmentSequence sequence = allSequences.get(index);
        if (sequence.isHidden()) {
            sequence.reveal();
            numberOfRows += sequence.getNumberOfRows();
        }

        updateRowToLists();
        updateReference();
    }

    /**
     * Returns all {@link Gene} or {@link Annotation} features.
     *
     * @return all {@link Gene} or {@link Annotation} features
     */
    public ObservableList<Feature> getGenes() {
        return FXCollections.unmodifiableObservableList(selectedGenes);
    }

    /**
     * Update {@code genes} to contain all genes from selected sequences/
     */
    private void updateGenes() {
        List<Class<? extends Feature>> classes = new ArrayList<>();
        classes.add(Annotation.class);
        classes.add(Gene.class);
        List<Feature> selectedFeatures = new ArrayList<>();
        for (AlignmentSequence sequence : visibleSequences) {
            if (sequence.isSelected()) {
                selectedFeatures.addAll(sequence.getFeatures(classes));
            }
        }

        selectedGenes.removeIf(p -> true);
        selectedGenes.addAll(selectedFeatures);
    }

    public void applyAlignment(List<AlignmentSequence> sequences) {
        Map<String, Integer> nameToIndex = new HashMap<>();
        for (int i = 0; i < visibleSequences.size(); i++) {
            nameToIndex.put(visibleSequences.get(i).getName(), i);
        }

        for (AlignmentSequence sequence : sequences) {
            if (nameToIndex.containsKey(sequence.getName())) {
                visibleSequences.get(nameToIndex.get(sequence.getName())).applyAlignment(sequence.getSequence());
            }
        }
    }

    @Override
    public int size() {
        return visibleSequences.size();
    }

    @Override
    public void move(int source, int target) {
        AlignmentSequence sequence = visibleSequences.get(source);
        allSequences.remove(visibleSequences.getSourceIndex(source));

        if (source < target) {
            target -= 1;
        }

        allSequences.add(visibleSequences.getSourceIndex(target), sequence);
        updateRowToLists();
        updateReference();
    }

    @Override
    public ObservableList<AlignmentSequence> getElements() {
        return FXCollections.unmodifiableObservableList(visibleSequences);
    }

    /**
     * Returns a read only copy of all stored sequences
     *
     * @return all sequences
     */
    public ObservableList<AlignmentSequence> getAllSequences() {
        return FXCollections.unmodifiableObservableList(allSequences);
    }
}
