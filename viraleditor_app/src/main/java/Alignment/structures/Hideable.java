package Alignment.structures;

import javafx.beans.value.ObservableBooleanValue;

public interface Hideable {
    /**
     * Returns true when hidden and false when not.
     *
     * @return true when hidden and false when not
     */
    public boolean isHidden();

    /**
     * Hides the object.
     */
    public void hide();

    /**
     * Reveals the object.
     */
    public void reveal();

    /**
     * Returns the objects name.
     *
     * @return the objects name
     */
    public String getName();

    /**
     * Returns hidden value property;
     */
    public ObservableBooleanValue getHiddenValueProperty();
}
