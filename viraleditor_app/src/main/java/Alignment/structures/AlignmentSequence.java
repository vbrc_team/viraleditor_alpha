package Alignment.structures;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import Alignment.SequenceTools;
import Shared.IntervalTree;
import Shared.interchange.*;
import javafx.beans.property.BooleanProperty;
import javafx.beans.property.SimpleBooleanProperty;
import javafx.beans.value.ObservableBooleanValue;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;

public class AlignmentSequence implements Hideable {
    /**
     * The stored sequence.
     */
    private Reference sequence;

    /**
     * The search matches
     */
    
    private ObservableList searchMatches = FXCollections.observableArrayList();
    
    /**
     * if annotations are needed
     */
    
    private boolean isAnnotated = false;
    
    /**
     * List of Annotations
     */
    
    private String[] annotationList;
    
    /**
     * Sequence to compute differences against.
     */
    private String reference;

    /**
     * The name of the stored sequence;
     */
    private String name;

    private List<RowType> rowTypes;

    private boolean selected;

    private UngappedPosition ungapped;
    
    

    private PrettyAminoAcid frameOne;
    private IntervalTree<Feature> featuresFrameOne;
    private PrettyAminoAcid frameTwo;
    private IntervalTree<Feature> featuresFrameTwo;
    private PrettyAminoAcid frameThree;
    private IntervalTree<Feature> featuresFrameThree;
    

    /**
     * True when the complement sequence is returned by getReadingFrame and false
     * otherwise.
     */
    private boolean complement;

    /**
     * True when the sequence is hidden and false when not.
     */
    private BooleanProperty hidden;
    

    /**
     * Constructs a sequence with a name and sequence.
     * 
     * @param name     name of alignment sequence
     * @param sequence
     */
    public AlignmentSequence(String name, Reference sequence) {
        this.sequence = sequence;
        this.reference = sequence.getOrigin().getSequence();
        ungapped = new UngappedPosition(sequence.getOrigin().getSequence());
        this.name = name;
        hidden = new SimpleBooleanProperty();
        rowTypes = new ArrayList<>();
        rowTypes.add(RowType.SCALE);
        rowTypes.add(RowType.SEQUENCE);
        rowTypes.add(RowType.DIFFERENCES);
        rowTypes.add(RowType.SEARCH_RESULTS);

        updateReadingFrames();
        initFeatureFrames();
    }
    
    public AlignmentSequence(String name, Reference sequence, boolean annotation) {
    	
    	this.sequence = sequence;
        this.reference = sequence.getOrigin().getSequence();
        ungapped = new UngappedPosition(sequence.getOrigin().getSequence());
        this.name = name;
        hidden = new SimpleBooleanProperty();
        rowTypes = new ArrayList<>();
        rowTypes.add(RowType.SCALE);
        rowTypes.add(RowType.SEQUENCE);
        rowTypes.add(RowType.DIFFERENCES);
        rowTypes.add(RowType.ANNOTATIONS);
        rowTypes.add(RowType.SEARCH_RESULTS);
        

        isAnnotated = true;
        
        updateReadingFrames();
        initFeatureFrames();
    }

    public void initFeatureFrames() {
        List<Class<? extends Feature>> featureClasses = new ArrayList<>();
        featureClasses.add(Gene.class);
        featureClasses.add(Annotation.class);

        featuresFrameOne = getTree(sequence.getFeatures(featureClasses), RowType.READING_FRAME_ONE);
        featuresFrameTwo = getTree(sequence.getFeatures(featureClasses), RowType.READING_FRAME_TWO);
        featuresFrameThree = getTree(sequence.getFeatures(featureClasses), RowType.READING_FRAME_THREE);
    }

    private IntervalTree<Feature> getTree(List<Feature> features, RowType readingFrame) {
        int frame;
        switch (readingFrame) {
        case READING_FRAME_ONE:
            frame = 0;
            break;
        case READING_FRAME_TWO:
            frame = 1;
            break;
        case READING_FRAME_THREE:
            frame = 2;
            break;
        default:
            throw new IllegalArgumentException();
        }

        IntervalTree<Feature> tree = new IntervalTree<>();
        for (Feature feature : features) {
            if (ungapped.ungapped((feature.getIntervals().get(0).getLow() - 1)) % 3 == frame) {
                int low = feature.getIntervals().get(0).getLow();
                int high = feature.getIntervals().get(feature.getIntervals().size() - 1).getHigh();
                tree.insert(feature, low, high);
            }
        }

        return tree;
    }

    /**
     * Returns the name of the sequence.
     * 
     * @return the name of the sequence
     */
    @Override
    public String getName() {
        return name;
    }

    public int getNumberOfRows() {
        return rowTypes.size();
    }

    public int getNumberOfColumns() {
        return sequence.getOrigin().getSequence().length();
    }
    
    public void updateSearchResults(ObservableList data) {
    	this.searchMatches = data;
    }
    
    /**
     * Get n NamedElements with m columns each.
     * <p>
     * If m > {@code #getNumberOfColumns()} then empty elements will be padded.
     * 
     * @param rowStart    row index to start at
     * @param columnStart column index to start at
     * @param n           number of rows
     * @param m           number of columns
     * @throws IndexOutOfBoundsException when (m < 0 || rowStart + n >
     *                                   getNumberOfRows()
     * @throws IndexOutOfBoundsException when columnStart < 0
     * @return
     */
    public List<NamedElements> getElements(int rowStart, int columnStart, int n, int m) {
        List<NamedElements> namedElements = new ArrayList<>();
        for (int row = 0; row < n; row++) {
            NamedElements elements;
           
            RowType type = rowTypes.get(rowStart + row);
            if (!isAnnotated) {
            switch (type) {
            case DIFFERENCES:
                elements = new NamedElements("Differences", getDifferences(columnStart, m));
                break;
            case SEQUENCE:
                elements = new NamedElements(name, getNucleotides(columnStart, m));
                break;
            case SCALE:
                elements = new NamedElements("Local Scale", getLocalScale(columnStart, m), true);
                break;
            case SEARCH_RESULTS:
            	elements = new NamedElements("Search Results", getSearchResults(columnStart, m));
            	break;
            case READING_FRAME_ONE:
                elements = new NamedElements("Frame 1",
                        getReadingFrame(columnStart, columnStart + m, RowType.READING_FRAME_ONE));
                break;
            case READING_FRAME_TWO:
                elements = new NamedElements("Frame 2",
                        getReadingFrame(columnStart, columnStart + m, RowType.READING_FRAME_TWO));
                break;
            case READING_FRAME_THREE:
                elements = new NamedElements("Frame 3",
                        getReadingFrame(columnStart, columnStart + m, RowType.READING_FRAME_THREE));
                break;
            case FEATURES_FRAME_ONE:
                elements = new NamedElements("Features",
                        getFeaturesFrame(columnStart, columnStart + m, type));
                break;
            case FEATURES_FRAME_TWO:
                elements = new NamedElements("Features", getFeaturesFrame(columnStart, columnStart + m, type));
                break;
            case FEATURES_FRAME_THREE:
                elements = new NamedElements("Features", getFeaturesFrame(columnStart, columnStart + m, type));
                break;
            default:
                throw new IllegalArgumentException(String.format("Missing case for: %s", type));
            }

            if (isSelected()) {
                elements.setSelected();
            }
            namedElements.add(elements);
        }
            else if (isAnnotated) {
            	switch (type) {
                case DIFFERENCES:
                    elements = new NamedElements("Differences", getDifferences(columnStart, m));
                    break;
                case SEQUENCE:
                    elements = new NamedElements(name, getNucleotides(columnStart, m));
                    break;
                case SCALE:
                    elements = new NamedElements("Local Scale", getLocalScale(columnStart, m), true);
                    break;
                case SEARCH_RESULTS:
                	elements = new NamedElements("Search Results", getSearchResults(columnStart, m));
                	break;
                case ANNOTATIONS:
                	elements = new NamedElements("Annotations", getAnnotations(columnStart, m));
                	break;
                case READING_FRAME_ONE:
                    elements = new NamedElements("Frame 1",
                            getReadingFrame(columnStart, columnStart + m, RowType.READING_FRAME_ONE));
                    break;
                case READING_FRAME_TWO:
                    elements = new NamedElements("Frame 2",
                            getReadingFrame(columnStart, columnStart + m, RowType.READING_FRAME_TWO));
                    break;
                case READING_FRAME_THREE:
                    elements = new NamedElements("Frame 3",
                            getReadingFrame(columnStart, columnStart + m, RowType.READING_FRAME_THREE));
                    break;
                case FEATURES_FRAME_ONE:
                    elements = new NamedElements("Features",
                            getFeaturesFrame(columnStart, columnStart + m, type));
                    break;
                case FEATURES_FRAME_TWO:
                    elements = new NamedElements("Features", getFeaturesFrame(columnStart, columnStart + m, type));
                    break;
                case FEATURES_FRAME_THREE:
                    elements = new NamedElements("Features", getFeaturesFrame(columnStart, columnStart + m, type));
                    break;
                default:
                    throw new IllegalArgumentException(String.format("Missing case for: %s", type));
                }

                if (isSelected()) {
                    elements.setSelected();
                }
                namedElements.add(elements);
            }
        }

        return namedElements;
    }

    /**
     * Returns the stored sequence.
     *
     * @return the stored sequence
     */
    public Sequence getSequence() {
        return sequence.getOrigin();
    }

    public void applyAlignment(Sequence seq) {
        sequence = Reference.builder()
                            .withReference(sequence)
                            .withOrigin(seq)
                            .withFeatures(updateIntervals(seq))
                            .build();
        ungapped = new UngappedPosition(seq.getSequence());
        
        initFeatureFrames();
        updateReadingFrames();
    }

    /**
     * Update intervals to account for gaps inserted by aligning
     * {@code sequence.geOrigin()} with another sequence.
     * <p>
     * Aligning {@code sequence.geOrigin()} means inserting zero or more gaps.
     *
     * @param seq sequence created by aligning {@code sequence} with another
     *            sequence
     * @return all features with updated intervals to account for inserted gaps
     */
    private List<Feature> updateIntervals(Sequence seq) {
        String aligned = seq.getSequence();
        UngappedPosition ungapped = new UngappedPosition(aligned);

        List<Feature> ungappedFeatures = new ArrayList<>();
        for (Feature feature : sequence.getFeatures(Feature.class)) {
            List<Interval> intervals = new ArrayList<>();
            for (Interval interval : feature.getIntervals()) {
                int lowIndex = ungappedPosition(interval.getLow() - 1) + 1;
                lowIndex += ungapped.gapsBefore(lowIndex);
                int highIndex = ungappedPosition(interval.getHigh() - 1) + 1;
                highIndex += ungapped.gapsBefore(highIndex);
                intervals.add(new Interval(interval.isComplement(), lowIndex, highIndex));
            }
            ungappedFeatures.add(feature.withIntervals(intervals));
        }
        return ungappedFeatures;
    }

    /**
     * Set reference sequence to use for comparisons.
     */
    public void setReference(Sequence reference) {
        this.reference = reference.getSequence();
    }

    /**
     * Sets the sequence select state.
     *
     * @param select the sequence select state
     */
    public void setSelected(boolean select) {
        selected = select;
    }
    
    /**
     * sets list of annotations
     */
    
    public void setAnnotations(String[] list) {

    	annotationList = list;
    	
   
    }

    /**
     * Returns true if selected and false otherwise.
     *
     * @return is the sequence selected
     */
    public boolean isSelected() {
        return selected;
    }

    private List<Element> getDifferences(int startColumn, int n) {
        Element[] diffs = new Element[n];
        int defaultColumns = 0;
        int minLength = Math.min(sequence.getOrigin().getSequence().length(), reference.length());
        if (startColumn + n > minLength) {
            defaultColumns = Math.min(n, (startColumn + n) - minLength);
            n = Math.max(0, n - defaultColumns);
        }

        /*
         * Types of differences between reference and sequence.
         *
         * An insertion occurs when a gap is in the reference but not in the sequence.
         *
         * A deletion occurs when a gap is in the sequence but not in the reference.
         *
         * A substitution occurs when both characters differ and are not gaps.
         */
        final String gap = "-";
        for (int i = 0; i < n; i++) {
            String referenceSymbol = "" + reference.charAt(startColumn + i);
            String symbol = "" + sequence.getOrigin().getSequence().charAt(startColumn + i);
            int gappedPosition = startColumn + i;
            int ungappedPosition = ungapped.ungapped(startColumn + i);
            if (referenceSymbol.equals(symbol)) {
                diffs[i] = new Element(" ", ElementType.BLANK, gappedPosition, ungappedPosition);
            } else if (referenceSymbol.equals(gap)) {
                diffs[i] = new Element(" ", ElementType.INSERTION, gappedPosition, ungappedPosition);
            } else if (symbol.equals(gap)) {
                diffs[i] = new Element(" ", ElementType.DELETION, gappedPosition, ungappedPosition);
            } else {
                diffs[i] = new Element(" ", ElementType.SUBSTITUTION, gappedPosition, ungappedPosition);
            }
        }

        int gappedPosition = sequence.getOrigin().getSequence().length();
        int ungappedPosition = ungapped.ungapped(sequence.getOrigin().getSequence().length() - 1);
        for (int i = 0; i < defaultColumns; i++) {
            diffs[n + i] = new Element(" ", ElementType.BLANK, gappedPosition, ungappedPosition);
        }

        return Arrays.asList(diffs);
    }
    
    /**
     * gets annotations specified by the user in the virusinvestigator window.
     * returns a list of elements containing data on whether an annotation is present
     * in that column or not.
     * @param startColumn
     * @param n
     * @return
     */
    
    private List<Element> getAnnotations(int startColumn, int n) {
    	Element[] elements = new Element[n];
        int defaultColumns = 0;
        if (startColumn + n > getNumberOfColumns()) {
        	defaultColumns = Math.min(n, startColumn + n - getNumberOfColumns());
            n = Math.max(0, n - defaultColumns);
        }

        String seq = sequence.getOrigin().getSequence();
        int m = 0;
        int[] intervals = new int[annotationList.length*2];
        for (String str: annotationList) {
        	String[] tempIntervals = str.split("-");
        	if (tempIntervals[2].equals(getName())) {
        	intervals[m] = Integer.valueOf(tempIntervals[0]);
        	intervals[m+1] = Integer.valueOf(tempIntervals[1]);
        	m = m+2;
        	
        	}
        }
        
        if (intervals.length == 0) {
        	for (int i = 0; i<n; i++) {
        		elements[i] = new Element(" ", ElementType.BLANK);
        	}
        }
        else {
        for (int i=0; i<n; i++) {
        	 
        	 int gappedPosition = startColumn + i;
             int ungappedPosition = ungapped.ungapped(startColumn + i);
        	 boolean anno = false;
        	 boolean start = false;
        	 boolean end = false;
        	 for (int interval=0; interval<intervals.length; interval+=2) {

        		 if (startColumn+i >= intervals[interval] && startColumn+i <= intervals[interval+1]) {
        			 anno = true;
        			 if (startColumn+i == intervals[interval]) {
        				 start = true;
        			 }
        			 if (startColumn+i == intervals[interval+1]) {
        				 end = true;
        			 }
        		 }
        	 }
        	 
        	 if (anno) {
        		 if (start) {
        			 elements[i] = new Element("<", ElementType.ANNOTATION, gappedPosition, ungappedPosition);
        		 }
        		 else if (end) {
        			 elements[i] = new Element(">", ElementType.ANNOTATION, gappedPosition, ungappedPosition);
        		 }
        		 else {
        			 elements[i] = new Element(" ", ElementType.ANNOTATION, gappedPosition, ungappedPosition);
        		 }
        	 }
        	 else {
        		 elements[i] = new Element(" ", ElementType.BLANK, gappedPosition, ungappedPosition);
        	 }
        	 
        }
        }

        int gappedPosition = seq.length();
        int ungappedPosition = ungapped.ungapped(seq.length() - 1) + 1;
        for (int j = 0; j < defaultColumns; j++) {
            elements[n + j] = new Element(" ", ElementType.BLANK, gappedPosition, ungappedPosition);
        }
        return Arrays.asList(elements);
    }

    /**
     * Inserts m gaps at index. The symbols with indices >= index are shifted right
     * one.
     *
     * @param index index to insert gap at
     * @param m     number of gaps to insert
     * @throws IndexOutOfBoundsException if (index < 0 || index >
     *                                   {@link #getNumberOfSequenceColumns()})
     */
    public void insertGap(int index, int m) {
        List<Feature> updatedFeatures = new ArrayList<>();
        for (Feature feature : sequence.getFeatures(Feature.class)) {
            List<Interval> updatedIntervals = new ArrayList<>();
            for (Interval interval : feature.getIntervals()) {
                if (index + 1 > interval.getLow() && index + 1 < interval.getHigh()) {
                    updatedIntervals.add(
                            new Interval(interval.isComplement(), interval.getLow(), interval.getHigh() + m));
                } else if (index + 1 < interval.getLow()) {
                    updatedIntervals.add(
                            new Interval(interval.isComplement(), interval.getLow() + m, interval.getHigh() + m));
                } else {
                    updatedIntervals.add(interval);
                }
            }
            updatedFeatures.add(feature.withIntervals(updatedIntervals));
        }
        

        sequence = Reference.builder()
                            .withReference(sequence)
                            .withFeatures(updatedFeatures)
                            .withOrigin(sequence.getOrigin().insertGaps(index, m))
                            .build();
        ungapped.insertGaps(index, m);
        updateReadingFrames();
        initFeatureFrames();
    }

    /**
     * Removes gaps from selected sequences in region from start to start + m.
     *
     * @param index index to start gap removal
     * @param m     length to remove gaps from
     * @throws IndexOutOfBoundsException if (start < 0 || start + m >
     *                                   {@link #getNumberOfSequenceColumns()})
     */
    public void removeGaps(int start, int m) {
        List<Feature> updatedFeatures = new ArrayList<>();
        for (Feature feature : sequence.getFeatures(Feature.class)) {
            List<Interval> updatedIntervals = new ArrayList<>();
            for (Interval interval : feature.getIntervals()) {
                int low = start;
                int high = start + m;
                int deletedGapsBeforeInterval = ungapped.gapsBefore(Math.min(high, interval.getLow()))
                        - ungapped.gapsBefore(Math.min(low, interval.getLow()));
                int deletedGapsInInterval = 0;
                if (low <= interval.getHigh() && high >= interval.getLow()) {
                    deletedGapsInInterval = ungapped.gapsBefore(Math.min(interval.getHigh(), high))
                            - ungapped.gapsBefore(Math.max(interval.getLow(), low));
                }

                low = interval.getLow() - deletedGapsBeforeInterval;
                // Edge case where gaps are at start of sequence
                if (low == 0) {
                    low += 1;
                }
                high = interval.getHigh() - (deletedGapsBeforeInterval + deletedGapsInInterval);
                updatedIntervals.add(new Interval(interval.isComplement(), low, high));
            }
            updatedFeatures.add(feature.withIntervals(updatedIntervals));
        }
        String middle = sequence.getOrigin().getSequence().substring(start, start + m);
        int offset = 0;
        for (int i = 0; i < m; i++) {
            if (middle.charAt(i) == '-') {
                ungapped.deleteGap(start + i - offset);
                offset += 1;
            }
        }
        

        sequence = Reference.builder()
                            .withReference(sequence)
                            .withFeatures(updatedFeatures)
                            .withOrigin(sequence.getOrigin().removeGaps(start, start + m))
                            .build();

        updateReadingFrames();
        initFeatureFrames();
    }

    /**
     * Returns the ungapped position from a gapped position
     * 
     * @param ungappedPosition
     * @return the gapped position
     */
    public int ungappedPosition(int gappedPosition) {
        return ungapped.ungapped(gappedPosition);
    }
    
    
    public String getUngappedString() {
    	return ungapped.getSequence();
    }
    
    	
    /**
     * Returns the lowest gapped sequence position from a ungapped position.
     * 
     * @param ungappedPosition the ungapped sequence position
     * @return the lowest gapped sequence position
     */
    public int gappedPosition(int ungappedPosition) {
        int left = 0;
        int right = ungapped.size() - 1;
        while (ungapped.ungapped(left) < ungapped.ungapped(right)) {
            int mean = (left + right) / 2;
            if (ungapped.ungapped(mean) < ungappedPosition) {
                left = mean + 1;
            } else {
                right = mean;
            }
        }

        return left;
    }

    /**
     * Returns n nucleotide elements from sequence starting at startColumn.
     * 
     * @param startColumn index to start at
     * @param n           number of elements to return
     * @return n nucleotide elements from sequence
     */
    private List<Element> getNucleotides(int startColumn, int n) {
        Element[] elements = new Element[n];
        int defaultColumns = 0;
        if (startColumn + n > getNumberOfColumns()) {
            defaultColumns = Math.min(n, startColumn + n - getNumberOfColumns());
            n = Math.max(0, n - defaultColumns);
        }

        String seq = sequence.getOrigin().getSequence();
        for (int i = 0; i < n; i++) {
            int gappedPosition = startColumn + i;
            int ungappedPosition = ungapped.ungapped(startColumn + i);
            String character = "" + seq.charAt(startColumn + i);
            switch (character) {
            case "A":
                elements[i] = new Element(getAminoAcid("A"), ElementType.NUCLEOTIDE, gappedPosition, ungappedPosition);
                break;
            case "C":
                elements[i] = new Element(getAminoAcid("C"), ElementType.NUCLEOTIDE, gappedPosition, ungappedPosition);
                break;
            case "D":
                elements[i] = new Element(getAminoAcid("D"), ElementType.NUCLEOTIDE, gappedPosition, ungappedPosition);
                break;
            case "E":
                elements[i] = new Element(getAminoAcid("E"), ElementType.NUCLEOTIDE, gappedPosition, ungappedPosition);
                break;
            case "F":
                elements[i] = new Element(getAminoAcid("F"), ElementType.NUCLEOTIDE, gappedPosition, ungappedPosition);
                break;
            case "G":
                elements[i] = new Element(getAminoAcid("G"), ElementType.NUCLEOTIDE, gappedPosition, ungappedPosition);
                break;
            case "H":
                elements[i] = new Element(getAminoAcid("H"), ElementType.NUCLEOTIDE, gappedPosition, ungappedPosition);
                break;
            case "I":
                elements[i] = new Element(getAminoAcid("I"), ElementType.NUCLEOTIDE, gappedPosition, ungappedPosition);
                break;
            case "K":
                elements[i] = new Element(getAminoAcid("K"), ElementType.NUCLEOTIDE, gappedPosition, ungappedPosition);
                break;
            case "L":
                elements[i] = new Element(getAminoAcid("L"), ElementType.NUCLEOTIDE, gappedPosition, ungappedPosition);
                break;
            case "M":
                elements[i] = new Element(getAminoAcid("M"), ElementType.NUCLEOTIDE, gappedPosition, ungappedPosition);
                break;
            case "P":
                elements[i] = new Element(getAminoAcid("P"), ElementType.NUCLEOTIDE, gappedPosition, ungappedPosition);
                break;
            case "Q":
                elements[i] = new Element(getAminoAcid("Q"), ElementType.NUCLEOTIDE, gappedPosition, ungappedPosition);
                break;
            case "R":
                elements[i] = new Element(getAminoAcid("R"), ElementType.NUCLEOTIDE, gappedPosition, ungappedPosition);
                break;
            case "S":
                elements[i] = new Element(getAminoAcid("S"), ElementType.NUCLEOTIDE, gappedPosition, ungappedPosition);
                break;
            case "T":
                elements[i] = new Element(getAminoAcid("T"), ElementType.NUCLEOTIDE, gappedPosition, ungappedPosition);
                break;
            case "V":
                elements[i] = new Element(getAminoAcid("V"), ElementType.NUCLEOTIDE, gappedPosition, ungappedPosition);
                break;
            case "W":
                elements[i] = new Element(getAminoAcid("W"), ElementType.NUCLEOTIDE, gappedPosition, ungappedPosition);
                break;
            case "Y":
                elements[i] = new Element(getAminoAcid("Y"), ElementType.NUCLEOTIDE, gappedPosition, ungappedPosition);
                break;
            case "N":
                elements[i] = new Element(getAminoAcid("N"), ElementType.NUCLEOTIDE, gappedPosition, ungappedPosition);
                break;
            case "-":
                elements[i] = new Element("-", ElementType.NUCLEOTIDE, gappedPosition, ungappedPosition);
                break;
            case "*":
            	elements[i] = new Element(getAminoAcid("*"), ElementType.NUCLEOTIDE, gappedPosition, ungappedPosition);
            	break;
            default:
                throw new IllegalArgumentException();
            }
        }

        int gappedPosition = seq.length();
        int ungappedPosition = ungapped.ungapped(seq.length() - 1) + 1;
        for (int i = 0; i < defaultColumns; i++) {
            elements[n + i] = new Element(" ", ElementType.BLANK, gappedPosition, ungappedPosition);
        }

        return Arrays.asList(elements);
    }

    public List<Element> getLocalScale(int columnStart, int m) {
        String seq = sequence.getOrigin().getSequence();
        int start = Math.max(0, columnStart - (columnStart % 20));
        if (columnStart > seq.length()) {
            start = seq.length();
            columnStart = start;
        }

        int end = Math.min(seq.length(), columnStart + m);
        int padding = columnStart + m - end;

        List<Element> scale = new ArrayList<>();
        int column = start;
        while (column < end) {
            if (seq.charAt(column) != '-' && ungapped.ungapped(column) % 20 == 0) {
                scale.add(new Element("|", ElementType.LOCAL_SCALE, column, ungapped.ungapped(column)));
                String number = Integer.toString(ungapped.ungapped(column) + 1);
                int length = Math.min(number.length(), Math.max(0, seq.length() - column));
                for (int i = 0; i < length; i++) {
                    scale.add(new Element("" + number.charAt(i), ElementType.LOCAL_SCALE, column, column));
                }
                column += number.length() + 1;
            } else {
                scale.add(new Element(" ", ElementType.LOCAL_SCALE, column, ungapped.ungapped(column)));
                column += 1;
            }
        }

        scale = scale.subList(columnStart - start, scale.size());

        for (int i = 0; i < padding; i++) {
            scale.add(new Element(" ", ElementType.LOCAL_SCALE));
        }

        return scale;
    }
    
    
    /**
     * returns the sequence matches found from the search results
     */
    
    public List<Element> getSearchResults(int startColumn, int n){
    	
    	Element[] elements = new Element[n];
        int defaultColumns = 0;
        if (startColumn + n > getNumberOfColumns()) {
            defaultColumns = Math.min(n, startColumn + n - getNumberOfColumns());
            n = Math.max(0, n - defaultColumns);
        }

        String seq = sequence.getOrigin().getSequence();
        
        if (searchMatches.size() == 0) {
        	for (int i = 0; i<n; i++) {
        		elements[i] = new Element(" ", ElementType.BLANK);
        	}
        }
        else {
        	ObservableList matchSeq = FXCollections.observableArrayList();
			for (int j = 0; j<searchMatches.size(); j++) {
				String[] result = searchMatches.get(j).toString().split(", ");
				if (result[0].toString().equals(getName())){
					matchSeq.add(searchMatches.get(j));
				}
			}
			int i = 0;
         while (i < n){
            int gappedPosition = startColumn + i;
            int ungappedPosition = ungapped.ungapped(startColumn + i);
            String character = "" + seq.charAt(startColumn + i);
            boolean isMatch = false;
            int match = -1;
            for (int j = 0; j<matchSeq.size(); j++) {
            	if (startColumn + i == Integer.valueOf(matchSeq.get(j).toString().split(", ")[4])){
            		isMatch = true;
            		match = j;
            	}
            }
            
           
            
            if (isMatch == true){
            	if (matchSeq.get(match).toString().split(", ")[6].equals("Top")) {
            		elements[i] = new Element("-", ElementType.NUCLEOTIDE, gappedPosition, ungappedPosition);
            		
            		int k = 0;
            		int end = matchSeq.get(match).toString().split(", ")[1].length()-1;
            		while (k<=end) {
            			if (ungapped.getSequence().charAt(i+k) == '-') {
            				end++;
            			}
            			elements[i+k] = new Element("-", ElementType.NUCLEOTIDE, gappedPosition, ungappedPosition);
            			k++;
            		}
            		elements[i+k-1] = new Element(">", ElementType.NUCLEOTIDE, gappedPosition, ungappedPosition);
            		i = i+k;
            	}
            	else {
            		elements[i] = new Element("<", ElementType.NUCLEOTIDE, gappedPosition, ungappedPosition);
            		int k = 1;
            		int end = matchSeq.get(match).toString().split(", ")[1].length()-1;
            		while (k<=end) {
            			if (ungapped.getSequence().charAt(i+k) == '-') {
            				end++;
            			}
            			elements[i+k] = new Element("-", ElementType.NUCLEOTIDE, gappedPosition, ungappedPosition);
            			k++;
            		}
            		elements[i+k-1] = new Element("-", ElementType.NUCLEOTIDE, gappedPosition, ungappedPosition);
            		i = i+k;
            	}
            }
            
            else {
            	elements[i] = new Element(" ", ElementType.BLANK);
            	i++;
            }
            
        }
        }

        int gappedPosition = seq.length();
        int ungappedPosition = ungapped.ungapped(seq.length() - 1) + 1;
        for (int i = 0; i < defaultColumns; i++) {
            elements[n + i] = new Element(" ", ElementType.BLANK, gappedPosition, ungappedPosition);
        }
        return Arrays.asList(elements);
    }


    /**
     * Returns true if the sequence will show reading frames and false otherwise.
     * 
     * @return true if the sequence will show reading frames and false otherwise
     */
    public boolean isShowingReadingFrames() {
        return rowTypes.indexOf(RowType.READING_FRAME_ONE) != -1
                && rowTypes.indexOf(RowType.READING_FRAME_TWO) != -1
                && rowTypes.indexOf(RowType.READING_FRAME_THREE) != -1;
    }

    /**
     * Adds reading frames as displayed rows. If reading frames are already added
     * nothing is done.
     */
    public void addReadingFrames() {
        removeReadingFrames();
        int start = Math.max(0, rowTypes.indexOf(RowType.DIFFERENCES));

        rowTypes.add(start, RowType.FEATURES_FRAME_THREE);
        rowTypes.add(start, RowType.READING_FRAME_THREE);
        rowTypes.add(start, RowType.FEATURES_FRAME_TWO);
        rowTypes.add(start, RowType.READING_FRAME_TWO);
        rowTypes.add(start, RowType.FEATURES_FRAME_ONE);
        rowTypes.add(start, RowType.READING_FRAME_ONE);
    }

    /**
     * Removes reading frames from displayed rows if they are present.
     */
    public void removeReadingFrames() {
        rowTypes.remove(RowType.READING_FRAME_ONE);
        rowTypes.remove(RowType.FEATURES_FRAME_ONE);
        rowTypes.remove(RowType.READING_FRAME_TWO);
        rowTypes.remove(RowType.FEATURES_FRAME_TWO);
        rowTypes.remove(RowType.READING_FRAME_THREE);
        rowTypes.remove(RowType.FEATURES_FRAME_THREE);
    }

    /**
     * Sets complement to be displayed when true and disables complement display
     * when false.
     * 
     * @param complement true to display complement and false otherwise
     */
    public void setComplement(boolean complement) {
        this.complement = complement;
        updateReadingFrames();
    }

    /**
     * Return all features of type {@code clazz}.
     *
     * @param <C>   type of feature to return
     * @param clazz class of feature to return
     * @return all features of type {@code <C>}
     */
    public <C extends Feature> List<C> getFeatures(Class<C> clazz) {
        return sequence.getFeatures(clazz);
    }

    /**
     * Returns all features that can be converted to one of the types in
     * {@code types}.
     *
     * @param types types of features to return
     * @return all features that can be converted to one of the types in
     *         {@code types}
     */
    public List<Feature> getFeatures(List<Class<? extends Feature>> clazzes) {
        return sequence.getFeatures(clazzes);
    }

    private List<Element> getReadingFrame(int start, int end, RowType type) {
        List<Element> frames = new ArrayList<>();
        String aminoAcids;
        switch (type) {
        case READING_FRAME_ONE:
            aminoAcids = frameOne.substring(Math.min(frameOne.length(), start), Math.min(frameOne.length(), end));
            break;
        case READING_FRAME_TWO:
            aminoAcids = frameTwo.substring(Math.min(frameTwo.length(), start), Math.min(frameTwo.length(), end));
            break;
        case READING_FRAME_THREE:
            aminoAcids = frameThree.substring(Math.min(frameThree.length(), start), Math.min(frameThree.length(), end));
            break;
        default:
            throw new IllegalArgumentException(type + " is not a reading frame.");
        }

        for (int i = 0; i < aminoAcids.length(); i++) {
            char symbol = aminoAcids.charAt(i);
            switch (symbol) {
            case 'M':
                frames.add(new Element("" + symbol, ElementType.START_CODON, start + i, ungapped.ungapped(start + i)));
                break;
            case '*':
                frames.add(new Element("" + symbol, ElementType.STOP_CODON, start + i, ungapped.ungapped(start + i)));
                break;
            default:
                frames.add(new Element("" + symbol, ElementType.TRANSLATION, start + i, ungapped.ungapped(start + i)));
                break;
            }
        }

        for (int i = frames.size(); i < end - start; i++) {
            frames.add(new Element(" ", ElementType.BLANK));
        }

        return frames;
    }

    private List<Element> getFeaturesFrame(int start, int end, RowType type) {
        IntervalTree<Feature> tree;
        switch (type) {
        case FEATURES_FRAME_ONE:
            tree = featuresFrameOne;
            break;
        case FEATURES_FRAME_TWO:
            tree = featuresFrameTwo;
            break;
        case FEATURES_FRAME_THREE:
            tree = featuresFrameThree;
            break;
        default:
            throw new IllegalArgumentException(type + " is not a feature frame.");
        }

        List<Element> elements = new ArrayList<>();
        for (int index = start; index < end; index++) {
            List<Feature> features = tree.find(index + 1);
            if (features.isEmpty()) {
                elements.add(new Element(" ", ElementType.BLANK));
            } else {
                elements.add(new Element(" ", ElementType.FEATURE));
            }
        }

        return elements;
    }

    /**
     * Returns the complement of {@code acid} when {@code complement} is true and
     * {@code acid} otherwise
     * 
     * @param acid an amino acid ambiguity code
     * @return the complement of {@code acid} when {@code complement} is true and
     *         {@code acid} otherwise
     */
    private String getAminoAcid(String acid) {
        if (complement) {
            return SequenceTools.getDNAComplement(acid);
        } else {
            return acid;
        }
    }

    private void updateReadingFrames() {
        Strand strand;
        if (complement) {
            strand = Strand.NEGATIVE;
        } else {
            strand = Strand.POSITIVE;
        }
        frameOne = new PrettyAminoAcid(sequence.getOrigin().getSequence(), ReadingFrame.ONE, strand);
        frameTwo = new PrettyAminoAcid(sequence.getOrigin().getSequence(), ReadingFrame.TWO, strand);
        frameThree = new PrettyAminoAcid(sequence.getOrigin().getSequence(), ReadingFrame.THREE, strand);
    }

    private enum RowType {
        SEQUENCE, DIFFERENCES, SCALE, SEARCH_RESULTS, ANNOTATIONS, READING_FRAME_ONE, READING_FRAME_TWO, READING_FRAME_THREE, FEATURES_FRAME_ONE,
        FEATURES_FRAME_TWO, FEATURES_FRAME_THREE;
    }

    @Override
    public boolean isHidden() {
        return hidden.getValue();
    }

    @Override
    public void hide() {
        hidden.setValue(Boolean.TRUE);
    }

    @Override
    public void reveal() {
        hidden.setValue(Boolean.FALSE);
    }

    @Override
    public ObservableBooleanValue getHiddenValueProperty() {
        return hidden;
    }
}
