package Alignment.structures;

import java.util.Collections;
import java.util.List;

public class DrawFrame {
    private final Strand strand;
    private final List<NamedElements> elements;
    private final NamedElements scale;

    public DrawFrame(List<NamedElements> elements, NamedElements scale, Strand strand) {
        this.elements = Collections.unmodifiableList(elements);
        this.scale = scale;
        this.strand = strand;
    }

    public List<NamedElements> getElements() {
        return elements;
    }

    public NamedElements getScale() {
        return scale;
    }

    public Strand getStrand() {
        return strand;
    }
}
