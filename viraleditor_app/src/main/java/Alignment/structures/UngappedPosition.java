package Alignment.structures;

import java.util.ArrayList;
import java.util.List;

public class UngappedPosition {
    private List<Integer> positions;
    private String Sequence;
    public UngappedPosition(String sequence) {
    	this.Sequence = sequence;
        positions = new ArrayList<>();

        int gaps = 0;
        for (int i = 0; i < sequence.length(); i++) {
            if (sequence.charAt(i) == '-') {
                gaps += 1;
            }

            positions.add(gaps);
        }
    }

    public String getSequence() {
    	return Sequence;
    }
    
    /**
     * Returns the ungapped position at index. An ungapped position is equal to the
     * index minus the cumulative number of gaps
     * 
     * @throws IllegalArgumentException if the index is out of range(index < 0 ||
     *                                  index > size())
     * 
     * @return the ungapped position at index
     */
    public int ungapped(int index) {
        return Math.max(0, index - positions.get(index));
    }

    /**
     * Inserts a gap at index. Inserting a gap increases all values in interval
     * (index, size()] by one.
     *
     * @throws IllegalArgumentException if the index is out of range(index < 0 ||
     *                                  index > size())
     */
    public void insertGap(int index) {
        positions.add(index, positions.get(index) + 1);

        for (int i = index + 1; i < size(); i++) {
            positions.set(i, positions.get(i) + 1);
        }
        
        String NewSeq = Sequence.substring(0, index+1) + "-" + Sequence.substring(index);
        
    }

    public void insertGaps(int index, int number) {
        if (positions.isEmpty()) {
            for (int i = 0; i < number; i++) {
                positions.add(i + 1);
            }
        } else {
            int count = positions.get(positions.size() - 1);
            for (int i = 0; i < number; i++) {
                positions.add(count);
            }

            for (int i = 0; i < number; i++) {
                positions.set(index + i, Math.min(number, i + 1));
            }

            for (int i = index + number; i < positions.size(); i++) {
                positions.set(i, positions.get(i) + number);
            }
        }
        String newSeq = Sequence.substring(0, index);
        for (int i = 0; i<number; i++) {
        	newSeq += "-";
        }
        newSeq += Sequence.substring(index);
        Sequence = newSeq;
    }

    /**
     * Deletes a gap at index if it exists. Deleting a gap decreases all values in
     * interval (index, size()] by one.
     * 
     * @throws IllegalArgumentException if the index is out of range(index < 0 ||
     *                                  index > size())
     */
    public void deleteGap(int index) {
        if (positions.get(index) > 0) {
            positions.remove(index);

            for (int i = index; i < size(); i++) {
                positions.set(i, positions.get(i) - 1);
            }
        }
        
        String newSeq = Sequence.substring(0, index) + Sequence.substring(index+1);
        Sequence = newSeq;
    }

    /**
     * Returns the number of gaps before {@code index}.
     *
     * @param position
     * @return the number of gaps before {@code index}
     */
    public int gapsBefore(int position) {
        if (position > 0) {
            return positions.get(position - 1);
        } else {
            return 0;
        }
    }

    /**
     * Returns the number of positions including gaps.
     * 
     * @return the number of positions including gaps
     */
    public int size() {
        return positions.size();
    }
}
