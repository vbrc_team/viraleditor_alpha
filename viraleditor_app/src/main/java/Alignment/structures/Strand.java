package Alignment.structures;

public enum Strand {
    /**
     * 5'3' strand
     */
    POSITIVE,
    /**
     * 3'5' strand
     */
    NEGATIVE;
}
