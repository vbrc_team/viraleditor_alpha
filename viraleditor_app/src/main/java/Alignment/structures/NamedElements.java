package Alignment.structures;

import java.util.ArrayList;
import java.util.List;

public class NamedElements {
    /**
     * The elements name.
     */
    private final String name;

    /**
     * The stored elements.
     */
    private final List<Element> elements;

    /**
     * True when the name should have an overline and false otherwise.
     */
    private final boolean overline;

    /**
     * True when the name elements are selected and false otherwise.
     */
    private boolean selected;

    /**
     * Construct NamedElements with a name.
     * 
     * @param name     the elements name
     * @param overline should the name have an overline
     */
    public NamedElements(String name, List<Element> elements, boolean overline) {
        this.name = name;
        this.elements = elements;
        elements = new ArrayList<>();
        this.overline = overline;
    }

    public NamedElements(String name, List<Element> elements) {
        this.name = name;
        this.elements = elements;
        overline = false;
    }

    /**
     * Returns the elements name.
     * 
     * @return the elements name
     */
    public String getName() {
        return name;
    }

    /**
     * Returns the stored elements.
     * 
     * @return the stored elements
     */
    public List<Element> getElements() {
        return elements;
    }

    /**
     * Returns if the name should have an overline.
     *
     * @return true if the name has an overline and otherwise false
     */
    public boolean isOverline() {
        return overline;
    }

    /**
     * Sets the name elements as selected.
     */
    public void setSelected() {
        selected = true;
    }

    /**
     * Returns of the name elements is selected.
     *
     * @return true when selected and false otherwise
     */
    public boolean isSelected() {
        return selected;
    }
}
