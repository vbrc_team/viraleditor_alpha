package Alignment.structures;

import javafx.collections.ObservableList;

public interface Orderable<T> {
    /**
     * Returns the number of movable elements.
     * 
     * @return the number of movable element
     */
    public int size();

    /**
     * Moves element at index {@code source} to index {@code target}.
     * <p>
     * Moving an element is the same as inserting the element at {@code target} then
     * removing the element originally at {@code source}.
     * 
     * @param source index of element to move
     * @param target index to move element to
     * @throws IndexOutOfBoundsException if (source < 0 || target < 0 || source >=
     *                                   size() || target >= size())
     */
    public void move(int source, int target);

    /**
     * Returns an unmodifiable view of the orderable elements.
     * @return an unmodifiable view of the orderable elements
     */
    public ObservableList<T> getElements();
}
