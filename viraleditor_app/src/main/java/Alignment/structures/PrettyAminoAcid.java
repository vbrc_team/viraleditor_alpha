package Alignment.structures;

import Alignment.SequenceTools;

/**
 * A formated translation of a nucleotide sequence to an amino acid sequence.
 * The translation is formated so the amino acids line up with their codons.
 * Gaps in the sequence appear as a = in the formated translation when within a
 * codon and as a - when between codons.
 * 
 * Example translations for each frame.
 * Sequence: "ATTTTCTAAAATATTAA"
 * Frame 1:  "I=>F=>*=>N=>I=>"
 * Frame 2:  " F=>S=>K=>I=>L=>"
 * Frame 3:  "  F=>L=>K=>Y=>*=>"
 * 
 * Sequence: "TATT-TTA"
 * Frame 1:  "Y=>F==>I"
 * Frame 2:  " I=>-L=>"
 * Frame 3:  "  F==>Y="
 */
public class PrettyAminoAcid {
    private final int offset;
    private final String sequence;
    private final Strand strand;
    private final UngappedPosition ungapped;

    /**
     * Constructs a pretty amino acid sequence from {@code sequence} using the
     * reading frame specified by {@code frame}.
     * 
     * @param sequence nucleotide sequence
     * @param frame    reading frame to translate the nucleotide sequence in
     */
    public PrettyAminoAcid(String sequence, ReadingFrame frame, Strand strand) {
        switch (frame) {
        case ONE:
            offset = 0;
            break;
        case TWO:
            offset = 1;
            break;
        case THREE:
            offset = 2;
            break;
        default:
            throw new IllegalArgumentException(frame + " is not a reading frame.");
        }

        this.sequence = sequence;
        this.strand = strand;
        ungapped = new UngappedPosition(sequence);
    }

    /**
     * Returns a substring of this pretty amino acid sequence from {@code start} to
     * {@code end}.
     * 
     * @param start inclusive start index
     * @param end   exclusive end index
     * @return the substring from start to end
     * @exception IndexOutOfBoundsException if the {@code start < 0} or
     *                                      {@code start > end} or
     *                                      {@code end > length()}
     */
    public String substring(int start, int end) {
        StringBuilder builder = new StringBuilder();
        for (int i = start; i < end; i++) {
            switch (strand) {
            case POSITIVE:
                builder.append(positiveSymbolAt(i));
                break;
            case NEGATIVE:
                builder.append(negativeSymbolAt(i));
                break;
            default:
                throw new IllegalArgumentException();                
            }
        }
        return builder.toString();
    }

    /**
     * Returns the length of the formated sequence.
     * 
     * @return the length of the formated sequence
     */
    public int length() {
        return sequence.length();
    }

    private String positiveSymbolAt(int index) {
        String symbol;
        if (ungapped.ungapped(index) < offset) {
            symbol = " ";
        } else if (sequence.charAt(index) == '-') {
            if ((ungapped.ungapped(index) - offset) % 3 == 2) {
                symbol = "-";
            } else {
                symbol = "=";
            }
        } else {
            index = ungapped.ungapped(index);
            int start = 3 * (index / 3) + offset;
            switch ((index - offset) % 3) {
            case 0:
                if (start + 2 >= ungapped.ungapped(ungapped.size() - 1)) {
                    symbol = " ";
                } else {
                    String codon = "" + sequence.charAt(findUngapped(start)) + sequence.charAt(findUngapped(start + 1))
                            + sequence.charAt(findUngapped(start + 2));
                    symbol = SequenceTools.getAAString(codon, 1);
                }
                break;
            case 1:
                symbol = "=";
                break;
            case 2:
                symbol = ">";
                break;
            default:
                throw new IllegalStateException();
            }
        }

        return symbol;
    }

    private String negativeSymbolAt(int index) {
        String symbol;
        int lastIndex = ungapped.ungapped(ungapped.size() - 1);
        if (lastIndex - ungapped.ungapped(index) < offset) {
            symbol = " ";
        } else if (sequence.charAt(index) == '-') {
            if ((lastIndex - ungapped.ungapped(index) - offset) % 3 == 2) {
                return "-";
            } else {
                return "=";
            }
        } else {
            index = ungapped.ungapped(index);
            int start = 3 * ((lastIndex - index) / 3) + offset;
            switch ((lastIndex - index - offset) % 3) {
            case 0:
                if (lastIndex - start - 2 < 0) {
                    symbol = " ";
                } else {
                    String codon = "" + sequence.charAt(findUngapped(lastIndex - start))
                            + sequence.charAt(findUngapped(lastIndex - start - 1))
                            + sequence.charAt(findUngapped(lastIndex - start - 2));
                    codon = SequenceTools.getDNAComplement(codon);
                    symbol = SequenceTools.getAAString(codon, 1);
                }
                break;
            case 1:
                symbol = "=";
                break;
            case 2:
                symbol = "<";
                break;
            default:
                throw new IllegalStateException("" + ((lastIndex - index - offset) % 3));
            }
        }

        return symbol;
    }

    private int findUngapped(int target) {
        int left = 0;
        int right = sequence.length();

        while (left < right) {
            int mean = (left + right) / 2;
            if (ungapped.ungapped(mean) < target) {
                left = mean + 1;
            } else {
                right = mean;
            }
        }

        return left;
    }
}
