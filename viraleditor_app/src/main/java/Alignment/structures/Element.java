package Alignment.structures;

import java.util.Objects;

public class Element {
    private final String symbol;
    private final ElementType type;
    private final int gappedPosition;
    private final int ungappedPosition;
    private boolean selected;

    /**
     * Constructs a node with a symbol and type.
     * 
     * @param symbol the node symbol
     * @param type   the node type
     * @throws IllegalArgumentException when the symbol length is not one
     */
    public Element(String symbol, ElementType type) {
        if (symbol.length() != 1) {
            throw new IllegalArgumentException("");
        }

        this.symbol = symbol;
        this.type = type;
        this.gappedPosition = -1;
        this.ungappedPosition = -1;
    }

    /**
     * Constructs a node with a symbol, type, gapped position, and ungapped
     * position.
     * 
     * @param symbol           the node symbol
     * @param type             the node type
     * @param gappedPosition   the gapped position
     * @param ungappedPosition the ungapped position
     * @throws IllegalArgumentException when the symbol length is not one
     */
    public Element(String symbol, ElementType type, int gappedPosition, int ungappedPosition) {
        if (symbol.length() != 1) {
            throw new IllegalArgumentException("");
        }

        this.symbol = symbol;
        this.type = type;
        this.gappedPosition = gappedPosition;
        this.ungappedPosition = ungappedPosition;
    }

    /**
     * Returns the element symbol.
     * 
     * @return the element symbol
     */
    public String getSymbol() {
        return symbol;
    }

    /**
     * Returns the element background color.
     * 
     * @return the element background color
     */
    public ElementType getType() {
        return type;
    }

    /**
     * Selects the node.
     */
    public void select() {
        selected = true;
    }

    /**
     * Returns true if the node is selected and false otherwise.
     *
     * @return is the node selected
     */
    public boolean isSelected() {
        return selected;
    }

    /**
     * Returns the ungapped position of the element.
     *
     * @return the ungapped position of the element or -1 when no position
     */
    public int getUngappedPosition() {
        return ungappedPosition;
    }

    /**
     * Returns the gapped position of the element.
     *
     * @return the gapped position of the element or -1 when no position
     */
    public int getGappedPosition() {
        return gappedPosition;
    }

    @Override
    public int hashCode() {
        return Objects.hash(symbol, type);
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj)
            return true;
        if (obj == null)
            return false;
        if (getClass() != obj.getClass())
            return false;
        Element other = (Element) obj;
        return Objects.equals(symbol, other.symbol) && type == other.type;
    }

}
