package Alignment;

import java.math.BigDecimal;
import java.math.RoundingMode;

public class FuzzyMotifResult {
	
	
	String name;
	String seq;
	String mismatch;
	String start;
	String end;
	String orientation;
	
	
	public FuzzyMotifResult(String seqname, String seq, String mismatch, String start, String end, String orientation) {
		this.name = seqname;
		this.seq = seq;
		this.mismatch = mismatch;
		this.start = start;
		this.end = end;
		this.orientation = orientation;
		
	}
	
	public String getName() {
		return this.name;
	}
	
	public String getSequence(){
		return this.seq;
	}
	
	public String getMismatch() {
		return this.mismatch;
	}
	
	public String getAccuracy() {
		double accuracy = 100*(1-Double.valueOf(this.mismatch)/seq.length());
		BigDecimal accRound = new BigDecimal(accuracy).setScale(2, RoundingMode.HALF_UP);
		return accRound.toString();
	}
	
	public String getStart() {
		return this.start;
	}
	
	public String getEnd() {
		return this.end;
	}
	
	public String toString() {
		return (this.name + ", " + this.seq + ", " + this.mismatch + ", " + getAccuracy() + ", " + this.start + ", " + this.end + ", " + this.orientation);
	}
	
	
	

}
