package Alignment;

import java.awt.GridLayout;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.swing.JFileChooser;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;

import Alignment.structures.*;
import Shared.AbstractComposer;
import Shared.interchange.*;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;

public class SequencesComposer extends AbstractComposer<SequencesDialog> implements Orderable<AlignmentSequence> {
    private List<AlignmentSequence> alignmentSequences;
    private AlignmentSequences sequences;
    private List<Reference> references;
    private int row;
    private int column;
    private int namesColumn;
    private int sequencesWidth;
    private int namesWidth;
    private int height;
    private Strand strand;
    private boolean editMouseMode = false;
    private boolean isAnnotated = false;
    
    
    public SequencesComposer() {
        alignmentSequences = new ArrayList<>();
        references = new ArrayList<>();
        sequences = new AlignmentSequences();
        strand = Strand.POSITIVE;
    }

    /**
     * updates the edit mouse mode toggle to on/off, allowing for insertion or deletion of gaps using the mouse.
     */
    public boolean updateEditMouseMode() {
    	editMouseMode = !editMouseMode;
    	dialog.updateEditMouseMode();
    	return editMouseMode;
    }
   
    /**
     * updates the colour scheme string, to the new selected colour Scheme
     */
    public void updateColourScheme(String newScheme) {
    	dialog.updateColourScheme(newScheme);
    	dialog.draw(nextFrame());
    }
    
    public void setAnnotated() {
    	isAnnotated = !isAnnotated;
    }
    
    /**
     * sets the annotation status of alignmentSequences to true, enabling 
     * annotations to be visible on the sequences.
     */
    
    public void setAnnotations(List<Reference> annotationList) {
    	String[] annotations = new String[annotationList.size()];
    	for (int i =0; i< annotationList.size(); i++) {
    		annotations[i] = annotationList.get(i).getVersion();
    	}
    	for (AlignmentSequence seq: alignmentSequences) {
    		seq.setAnnotations(annotations);
    	}

    	dialog.draw(nextFrame());
    }
    
    /**
     * Add sequence stored in reference's origin.
     */
    public void addSequence(Reference reference) {
        references.add(reference);
        AlignmentSequence sequence;
        if (!isAnnotated) {
        sequence = new AlignmentSequence(getReferenceName(reference), reference);
        }
        else {
        	sequence = new AlignmentSequence(getReferenceName(reference), reference, isAnnotated);
        }
        if (!references.isEmpty()) {
            sequence.setReference(references.get(0).getOrigin());
        }
        sequences.add(sequence);
        alignmentSequences.add(sequence);
        
        dialog.draw(nextFrame());
    }
    
    
    /**
     * Saves the current alignment to a path specified by user
     */
    
    public void saveAs(String[] NewStrings) {
    	
	JFileChooser NewFile = new JFileChooser();
	NewFile.setCurrentDirectory(new File(""));
	int retrieve = NewFile.showSaveDialog(null);
	String SaveString = "";
	int i = 0
			;
	for (AlignmentSequence seq: alignmentSequences) {
		SaveString += (">");
		SaveString += seq.getName();
		SaveString += ("\n");
		SaveString += NewStrings[i];
		SaveString += ("\n");
		i++;
	}
	
	if (retrieve == JFileChooser.APPROVE_OPTION) {
		try(FileWriter FW = new FileWriter(NewFile.getSelectedFile()+ ".fasta")){
			FW.write(SaveString);
		}
		catch (IOException e) {
			e.printStackTrace();
		}
	}
    }
    
    
    /**
     * Returns a list of subsequences that match some given k-mer, along with, % accuracy computed by
		1-mismatch/sequence length, whether its a top match or a bottom match, which sequence
		it is, and what its ungapped position is.
     */
    
    public List<String> searchFuzzyMotif(String[] seqandMismatch){
    	ArrayList<String> fuzzyMotifData = new ArrayList<String>();
    	String subseq = seqandMismatch[0];
    	int mismatch = Integer.valueOf(seqandMismatch[1]);
    	int tempMismatch = mismatch;
    	boolean isMatch = true;
    	
    	for (AlignmentSequence seq: alignmentSequences) {
    		for (int i=0; i<=seq.getUngappedString().length()-subseq.length(); i++) {
        		tempMismatch = mismatch;
        		isMatch = true;
        		int k = 0;
        		int L = 0;
        		int subLength = subseq.length();
        			while (k<subLength) {
        				if (seq.getUngappedString().charAt(i+k) == subseq.charAt(L)) {
        					k++;
        					L++;
        					continue;
        				}
        				
        				else if (seq.getUngappedString().charAt(i+k) == '-') {
        					k++;
        					subLength++;
        					continue;
        				}
        				else {
        					tempMismatch -=1;
        					k++;
        					L++;
        					if (tempMismatch <0) {
        						isMatch = false;
        					}
        				}
        			}
        			
        			if (isMatch == true) {
        				String newMatch = "";
        				int j = 0;
        				while (newMatch.length() != subseq.length()) {
        					newMatch = seq.getUngappedString().substring(i, i+subseq.length()+j).replace("-", "");
        					j++;
        				}
        				fuzzyMotifData.add(seq.getName());
        				fuzzyMotifData.add(newMatch);
        				fuzzyMotifData.add(Integer.toString(mismatch-tempMismatch));
        				fuzzyMotifData.add(Integer.toString(i));
        				fuzzyMotifData.add(Integer.toString(i+subseq.length()));
        				fuzzyMotifData.add("Top");
        				
        			}
        			else {
        				
        			}
    		}
        			
        			
    			
    		
    		
    		
    		
    	}
    	
    	for (AlignmentSequence seq: alignmentSequences) {
    		for (int i=0; i<=seq.getUngappedString().length()-subseq.length(); i++) {
        		tempMismatch = mismatch;
        		isMatch = true;
        		int k = 0;
        		int L = 0;
        		int subLength = subseq.length();
        			while (k<subLength) {
        				if (seq.getUngappedString().charAt(i+k) == 'A') {
        					if (subseq.charAt((subseq.length()-1)-L) == 'T') {
        						k++;
        						L++;
        						continue;
        					}
        					else {
        						tempMismatch -=1;
        						k++;
        						L++;
        						if (tempMismatch <0) {
        							isMatch = false;
        						}
        					}
        				}
        				
        				else if (seq.getUngappedString().charAt(i+k) == 'T') {
        					if (subseq.charAt((subseq.length()-1)-L) == 'A') {
        						k++;
        						L++;
        						continue;
        					}
        					else {
        						tempMismatch -=1;
        						k++;
        						L++;
        						if (tempMismatch <0) {
        							isMatch = false;
        						}
        					}
        					
        				}
        				
        				else if (seq.getUngappedString().charAt(i+k) == 'G') {
        					if (subseq.charAt((subseq.length()-1)-L) == 'C') {
        						k++;
        						L++;
        						continue;
        					}
        					else {
        						k++;
        						L++;
        						tempMismatch -=1;
        						if (tempMismatch <0) {
        							isMatch = false;
        						}
        					}
        					
        				}
        				
        				else if (seq.getUngappedString().charAt(i+k) == 'C') {
        					if (subseq.charAt((subseq.length()-1)-L) == 'G') {
        						k++;
        						L++;
        						continue;
        					}
        					else {
        						k++;
        						L++;
        						tempMismatch -=1;
        						if (tempMismatch <0) {
        							isMatch = false;
        						}
        					}
        					
        				}
        				
        				else if (seq.getUngappedString().charAt(i+k) == '-') {
        					k++;
        					subLength++;
        					continue;
        				}

        			}
        			
        			if (isMatch == true) {
        				String newMatch = "";
        				int j = 0;
        				while (newMatch.length() != subseq.length()) {
        					newMatch = seq.getUngappedString().substring(i, i+subseq.length()+j).replace("-", "");
        					j++;
        				}
        				fuzzyMotifData.add(seq.getName());
        				fuzzyMotifData.add(newMatch);
        				fuzzyMotifData.add(Integer.toString(mismatch-tempMismatch));
        				fuzzyMotifData.add(Integer.toString(i));
        				fuzzyMotifData.add(Integer.toString(i+subseq.length()));
        				fuzzyMotifData.add("Bottom");
        				
        			}
        			else {
        				
        			}
        			
        		}
        	}
        			

        			
        			
    			
    		
    	
    	
    	
    	return fuzzyMotifData;
    }
    
    /**
     * Updates results of search results
     */
    
    public void updateSearchResults(ObservableList data) {
    	for (AlignmentSequence seq: alignmentSequences) {
    		seq.updateSearchResults(data);
    		
    	}
    }
    
    
    /**
     * removes sequence stored in reference's origin/
     * @param reference
     */
    
    public void removeSequence(Reference reference) {
    	AlignmentSequence sequence;
    	if(!isAnnotated) {
    	sequence = new AlignmentSequence(getReferenceName(reference), reference);
    	}
    	else {
    	sequence = new AlignmentSequence(getReferenceName(reference), reference, true);
    	}
    	sequences.remove(sequence);
    	for (AlignmentSequence seq: alignmentSequences) {
    		if (seq.getSequence() == reference.getOrigin()) {
    			alignmentSequences.remove(seq);
    			break;
    		}
    	}
    	references.remove(reference);
    	dialog.draw(nextFrame());
    }
    
    public String[] getSequences() {
    	String[] Sequences = new String[this.references.size()];
    	int i = 0;
    	for (Reference reference : this.references) {
    		Sequences[i] = reference.getOrigin().toString();
    		i++;
    	}
    	return Sequences;
    }

    public void setSequencesWidth(int width) {
        if (width < 0) {
            throw new IllegalArgumentException(String.format("%s < 0", width));
        }
        sequencesWidth = width;
        dialog.draw(nextFrame());
    }

    public void setNamesWidth(int width) {
        if (width < 0) {
            throw new IllegalArgumentException(String.format("%s < 0", width));
        }
        namesWidth = width;
        dialog.draw(nextFrame());
    }

    public void setHeight(int height) {
        if (height < 0) {
            throw new IllegalArgumentException(String.format("%s < 0", height));
        }
        this.height = height;
        dialog.draw(nextFrame());
    }

    /**
     *
     * @param row
     * @throws IndexOutOfBoundsException if (row < 0 || (row >= getNumberOfRows() &&
     *                                   row > 0))
     */
    public void setRow(int row) {
        if (row < 0 || (row >= getNumberOfRows() && row > 0)) {
            throw new IndexOutOfBoundsException(Integer.toString(row));
        }
        this.row = row;
        dialog.draw(nextFrame());
    }

    /**
     *
     * @param newColumn
     * @throws IndexOutOfBoundsException if (column < 0 || (column >=
     *                                   getNumberOfColumns() && column > 0))
     */
    public void setColumn(int newColumn) {
        if (newColumn < 0 || (newColumn >= getNumberOfColumns() && newColumn > 0)) {
            throw new IndexOutOfBoundsException(Integer.toString(newColumn));
        }
        column = newColumn;
        dialog.setColumn(column);
        dialog.draw(nextFrame());
    }

    public void setNamesColumn(int newColumn) {
        namesColumn = newColumn;
        dialog.draw(nextFrame());
    }

    public int getNumberOfColumns() {
        return sequences.getNumberOfSequenceColumns();
    }

    public int getNumberOfRows() {
        return sequences.getNumberOfRows();
    }

    public int getNumberOfNameColumns() {
        return sequences.getNumberOfNameColumns();
    }

    /**
     * Select columns from start to end.
     *
     * @param start inclusive index to start selection
     * @param end   exclusive index to end selection
     */
    public void selectRange(int start, int end) {
        sequences.selectRange(start, end);
        dialog.draw(nextFrame());
    }

    public void selectRow(int row) {
        sequences.selectRow(row);
        dialog.draw(nextFrame());
    }

    public void deselectAllRows() {
        for (int row = 0; row < sequences.getNumberOfRows(); row++) {
            sequences.deselectRow(row);
        }
        dialog.draw(nextFrame());
    }

    public void toggleRowSelect(int row) {
        sequences.toggleRowSelect(row);
        dialog.draw(nextFrame());
    }

    public void insertGaps(int length) {
        sequences.insertGaps(length);
        dialog.draw(nextFrame());
    }

    /**
     * Remove gaps from selected sequences.
     */
    public void removeGaps() {
        sequences.removeGaps();
        dialog.draw(nextFrame());
    }
    
    /**
     * When the user selects Remove All-Column-Sequences, this removes all gaps that are present in every sequence
     */
    
    public void removeColumnGaps() {
    	sequences.removeColumnGaps();

    	dialog.draw(nextFrame());
    }
  
    
    
    /**
     * returns all selected sequences from the alignment window
     */
    
    public ArrayList<AlignmentSequence> getAllSelectedSequences(){
    	ArrayList<AlignmentSequence> SelectedSeq = new ArrayList<AlignmentSequence>();
    	for (AlignmentSequence Seq: getAllSequences()) {
    		if (sequences.getSelectedSequences(Seq) != null) {
    			SelectedSeq.add(Seq);
    		}
    	}
    	return SelectedSeq;
    }
    
    /**
     * gets selected sequences from the alignment window
     */
    public AlignmentSequence isSelected(Reference ref) {
    	for (AlignmentSequence Seq: alignmentSequences) {
        	if (getReferenceName(ref) == Seq.getName()){
        		if (sequences.getSelectedSequences(Seq) != null) {
        			return sequences.getSelectedSequences(Seq);
        		}
        		
        		else {
        			return null;
        		}
        		
        	}

    	}

    	return null;
    }
    
    /**
     * when the user selects remove Identical-Sequences, this removes all sequences that are identical.
     */
    
    public List<Reference> removeIdenticalSequences() {
    	
    	List<Reference> NewRefList = references;
    	List<Reference> TempRefList = new ArrayList<Reference>();
    	for (Reference ref: NewRefList) {
    		for (Reference reference : NewRefList) {
    			if (!ref.equals(reference)) {
    				if (ref.getSequence().equals(reference.getSequence())){
    					if(!TempRefList.contains(reference) && NewRefList.indexOf(reference) > NewRefList.indexOf(ref)){
    						TempRefList.add(reference);
    					}
    				}
    			}
    		}
    	}
    	
    	return TempRefList;
    }

    /**
     * Shows a window that displays the current gap counts of each sequence in the window.
     */
    
    public void viewGapCount() {
    	final JFrame parent = new JFrame("GridLayout");
        JPanel MyPanel = new JPanel();
        MyPanel.setLayout(new GridLayout(getAllSequences().size(), 1));
        for(int i = 0; i<= getAllSequences().size()-1; i++) {
        	int gapnum = 0;
        	for (int j = 0; j <= getAllSequences().get(i).getNumberOfColumns()-1; j++) {
        		 if (getAllSequences().get(i).getSequence().getSequence().charAt(j) == '-'){
        			gapnum++;
        			
        		}
        	}
        	JLabel myLabel = new JLabel(getAllSequences().get(i).getFeatures(Genome.class).get(0).getOrganismAbbrevation() + ": " + Integer.toString(gapnum) + " Total Gaps");
        	MyPanel.add(myLabel);    	
        }
        parent.getContentPane().add(MyPanel, "Center");
        parent.setSize(300, 200*getAllSequences().size());
        parent.setVisible(true);
    }
    
    /**
     * Opens a new window which reports the number of columns and
     *  the number of columns that contain; a gap, a single nucleotide,
     *  two nucleotides (all pairs listed), three nucleotides, four nucleotides.
     */
    
    public void getCounts() {
    	ArrayList<Integer> counts = new ArrayList<Integer>();
    	for (int i=0; i<12; i++) {
    		counts.add(0);
    	}
    	int shortest = references.get(0).getSequence().length();
    	for (AlignmentSequence seq: alignmentSequences) {
    		if (seq.getSequence().getSequence().length() < shortest) {
    			shortest = seq.getSequence().getSequence().length();
    		}
    	}
    	
    	counts.set(0, shortest);
    	
    	for (int i =0; i<shortest; i++) {
    		String ntTypes = "";
    		for (AlignmentSequence seq: alignmentSequences) {
    			String letter = Character.toString(seq.getSequence().getSequence().charAt(i));
    			if (ntTypes.contains(letter)){
    				continue;
    			}
    			else{
    				ntTypes += seq.getSequence().getSequence().charAt(i);
    			}
    		}
    		
    		if (ntTypes.contains("-")){
    			counts.set(1, counts.get(1)+1);
    		}
    		
    		else if (ntTypes.length() == 4) {
    			counts.set(11, counts.get(11)+1);
    		}
    		
    		else if (ntTypes.length() == 3) {
    			counts.set(10, counts.get(10)+1);
    		}
    		
    		else if (ntTypes.equals("CG") || ntTypes.equals("GC")) {
    			counts.set(9, counts.get(9)+1);
    			counts.set(3, counts.get(3)+1);
    		}
    		
    		else if (ntTypes.equals("AG") || ntTypes.equals("GA")) {
    			counts.set(8, counts.get(8)+1);
    			counts.set(3, counts.get(3)+1);
    		}
    		
    		else if (ntTypes.equals("CA") || ntTypes.equals("AC")) {
    			counts.set(7,  counts.get(7)+1);
    			counts.set(3, counts.get(3)+1);
    		}
    		
    		else if (ntTypes.equals("TA") || ntTypes.equals("AT")){
    			counts.set(6, counts.get(6)+1);
    			counts.set(3, counts.get(3)+1);
    		}
    		
    		else if (ntTypes.equals("CT") || ntTypes.equals("TC")){
    			counts.set(5, counts.get(5)+1);
    			counts.set(3, counts.get(3)+1);
    		}
    		
    		else if (ntTypes.equals("TG") || ntTypes.equals("GT")){
    			counts.set(4, counts.get(4)+1);
    			counts.set(3, counts.get(3)+1);
    		}
    		
    		else if (ntTypes.length() == 1) {
    			counts.set(2, counts.get(2)+1);
    		}
    		
    		else {
    			continue;
    		}
    	}
    		
    	final JFrame parent = new JFrame("GridLayout");
        JPanel MyPanel = new JPanel();
        MyPanel.setLayout(new GridLayout(12, 1));
        JLabel label1 = new JLabel("Total Number of Columns: " + Integer.toString(counts.get(0)));
        MyPanel.add(label1);
        JLabel label2 = new JLabel("# of columns with a gap: " + Integer.toString(counts.get(1)));
        MyPanel.add(label2);
        JLabel label3 = new JLabel("# of columns with a single Nucleotide: " + Integer.toString(counts.get(2)));
        MyPanel.add(label3);
        JLabel label4 = new JLabel("# of columns with 2 Nucleotides: " + Integer.toString(counts.get(3)));
        MyPanel.add(label4);
        JLabel label5 = new JLabel("T/G: " + Integer.toString(counts.get(4)));
        MyPanel.add(label5);
        JLabel label6 = new JLabel("C/T: " + Integer.toString(counts.get(5)));
        MyPanel.add(label6);
        JLabel label7 = new JLabel("A/T: " + Integer.toString(counts.get(6)));
        MyPanel.add(label7);
        JLabel label8 = new JLabel("A/C: " + Integer.toString(counts.get(7)));
        MyPanel.add(label8);
        JLabel label9 = new JLabel("A/G: " + Integer.toString(counts.get(8)));
        MyPanel.add(label9);
        JLabel label10 = new JLabel("C/G: " + Integer.toString(counts.get(9)));
        MyPanel.add(label10);
        JLabel label11 = new JLabel("# of columns with 3 Nucleotides: " + Integer.toString(counts.get(10)));
        MyPanel.add(label11);
        JLabel label12 = new JLabel("# of columns with 4 Nucleotides: " + Integer.toString(counts.get(11)));
        MyPanel.add(label12);
        
        parent.getContentPane().add(MyPanel, "Center");
        parent.setSize(300, 200*getAllSequences().size());
        parent.setVisible(true);
    }
    
    
    /**
     * Opens a new window which displays the number of columns that contain two nucleotides.
     *  The counts listed under it represent the number of SNPs between two sequences. 
     */
    
    public void getCountsTop2() {
    	List<AlignmentSequence> top2 = new ArrayList<AlignmentSequence>();
    	top2.add(alignmentSequences.get(0));
    	top2.add(alignmentSequences.get(1));
    	ArrayList<Integer> counts = new ArrayList<Integer>();
    	for (int i=0; i<13; i++) {
    		counts.add(0);
    	}
    	int shortest = references.get(0).getSequence().length();
    	for (int j=0; j<2; j++) {
    		if (alignmentSequences.get(j).getSequence().getSequence().length() < shortest){
    			shortest = alignmentSequences.get(j).getSequence().getSequence().length();
    		}
    	}
    	
    	
    	for (int i =0; i<shortest; i++) {
    		String ntTypes = "";
    		for (AlignmentSequence seq: top2) {
    			String letter = Character.toString(seq.getSequence().getSequence().charAt(i));
    			if (ntTypes.contains(letter)){
    				continue;
    			}
    			else{
    				ntTypes += seq.getSequence().getSequence().charAt(i);
    			}
    		}

    		if (ntTypes.equals("CG")) {
    			counts.set(1, counts.get(1)+1);
    			counts.set(0, counts.get(0)+1);
    		}
    		
    		else if (ntTypes.equals("GC")) {
    			counts.set(2, counts.get(2)+1);
    			counts.set(0, counts.get(0)+1);
    		}
    		
    		else if (ntTypes.equals("AC")) {
    			counts.set(3, counts.get(3)+1);
    			counts.set(0, counts.get(0)+1);
    		}
    		
    		else if (ntTypes.equals("CA")) {
    			counts.set(4, counts.get(4)+1);
    			counts.set(0, counts.get(0)+1);
    		}
    		
    		else if (ntTypes.equals("CT")) {
    			counts.set(5, counts.get(5)+1);
    			counts.set(0, counts.get(0)+1);
    		}
    		
    		else if (ntTypes.equals("TC")) {
    			counts.set(6, counts.get(6)+1);
    			counts.set(0, counts.get(0)+1);
    		}
    		
    		else if (ntTypes.equals("AG")) {
    			counts.set(7, counts.get(7)+1);
    			counts.set(0, counts.get(0)+1);
    		}
    		
    		else if (ntTypes.equals("GA")) {
    			counts.set(8, counts.get(8)+1);
    			counts.set(0, counts.get(0)+1);
    		}
    		
    		else if (ntTypes.equals("GT")) {
    			counts.set(9, counts.get(9)+1);
    			counts.set(0, counts.get(0)+1);
    		}
    		
    		else if (ntTypes.equals("TG")) {
    			counts.set(10, counts.get(10)+1);
    			counts.set(0, counts.get(0)+1);
    		}
    		
    		else if (ntTypes.equals("AT")) {
    			counts.set(11, counts.get(11)+1);
    			counts.set(0, counts.get(0)+1);
    		}
    		
    		else if (ntTypes.equals("TA")) {
    			counts.set(12, counts.get(12)+1);
    			counts.set(0, counts.get(0)+1);
    		}
    		
    		else {
    			continue;
    		}
    	}
    		
    	final JFrame parent = new JFrame("GridLayout");
        JPanel MyPanel = new JPanel();
        MyPanel.setLayout(new GridLayout(14, 1));
        JLabel label1 = new JLabel("Total Number of Columns with 2 Nucleotides: " + Integer.toString(counts.get(0)));
        MyPanel.add(label1);
        JLabel label2 = new JLabel("C/G: " + Integer.toString(counts.get(1)));
        MyPanel.add(label2);
        JLabel label3 = new JLabel("G/C: " + Integer.toString(counts.get(2)));
        MyPanel.add(label3);
        JLabel label4 = new JLabel("A/C: " + Integer.toString(counts.get(3)));
        MyPanel.add(label4);
        JLabel label5 = new JLabel("C/A: " + Integer.toString(counts.get(4)));
        MyPanel.add(label5);
        JLabel label6 = new JLabel("C/T: " + Integer.toString(counts.get(5)));
        MyPanel.add(label6);
        JLabel label7 = new JLabel("T/C: " + Integer.toString(counts.get(6)));
        MyPanel.add(label7);
        JLabel label8 = new JLabel("A/G: " + Integer.toString(counts.get(7)));
        MyPanel.add(label8);
        JLabel label9 = new JLabel("G/A: " + Integer.toString(counts.get(8)));
        MyPanel.add(label9);
        JLabel label10 = new JLabel("G/T: " + Integer.toString(counts.get(9)));
        MyPanel.add(label10);
        JLabel label11 = new JLabel("T/G: " + Integer.toString(counts.get(10)));
        MyPanel.add(label11);
        JLabel label12 = new JLabel("A/T: " + Integer.toString(counts.get(11)));
        MyPanel.add(label12);
        JLabel label13 = new JLabel("T/A: " + Integer.toString(counts.get(12)));
        MyPanel.add(label13);
        JLabel label14 = new JLabel("These counts represent the number of SNPS between"
        		+ " two sequences. For example, �A/C: 5� means that in 5 columns the first"
        		+ " sequence contains an A and the second sequence contains a C.");
        MyPanel.add(label14);
        
        parent.getContentPane().add(MyPanel, "Center");
        parent.setSize(300, 200*getAllSequences().size());
        parent.setVisible(true);
    }
    
    /**
     * Skips to next gap in sequences.
     */
    public void skipToNextGap() {
        setColumn(sequences.getNextGap(column));
    }
    
    /**
     * Returns a copy of the selected region
     */
    
    public String getRegion() {
    	return sequences.getRegion();
    }

    /**
     * Skips to previous gap in sequences.
     */
    public void skipToPreviousGap() {
        setColumn(sequences.getPreviousGap(column));
    }

    /**
     * Skips to next difference in sequences.
     */
    public void skipToNextDifference() {
        setColumn(sequences.getNextDifference(column));
    }

    /**
     * Skips to next difference in sequences.
     */
    public void skipToPreviousDifference() {
        setColumn(sequences.getPreviousDifference(column));
    }

    public void moveSelectedSquencesUp() {
        sequences.moveSelectedSquencesUp();
        dialog.draw(nextFrame());
    }

    public void moveSelectedSquencesDown() {
        sequences.moveSelectedSquencesDown();
        dialog.draw(nextFrame());
    }

    public void moveSelectedSequencesToFront() {
        sequences.moveSelectedSequencesToFront();
        dialog.draw(nextFrame());
    }

    public void moveSelectedSequencesToEnd() {
        sequences.moveSelectedSequencesToEnd();
        dialog.draw(nextFrame());
    }

    public void toggleShowTranslation() {
        sequences.toggleReadingFrames();
        dialog.draw(nextFrame());
    }

    private List<NamedElements> getVisibleElements() {
        int numberOfRows = Math.min(sequences.getNumberOfRows() - row, height);
        int numberOfColumns = Math.min(sequences.getNumberOfSequenceColumns() - column, sequencesWidth);
        return sequences.get(row, column, numberOfRows, numberOfColumns);
    }

    public void toggleComplement() {
        sequences.toggleDirection();
        if (strand == Strand.POSITIVE) {
            strand = Strand.NEGATIVE;
        } else {
            strand = Strand.POSITIVE;
        }
        dialog.draw(nextFrame());
    }

    /**
     * Returns the name of the reference sequences.
     * <p>
     * The name is one of
     * <ul>
     * <li>FASTA description line
     * <li>Organism name from database
     * <li>Organism name from GenBank file
     * <li>the empty string when the previous are not present.
     *
     * @param reference
     * @return name of the reference
     */
    private String getReferenceName(Reference reference) {
        String name;
        if (!reference.getFeatures(Description.class).isEmpty()) {
            name = reference.getFeatures(Description.class).get(0).getDescription();
        } else if (!reference.getFeatures(Genome.class).isEmpty()) {
            name = reference.getFeatures(Genome.class).get(0).getOrganismName();
        } else if (!reference.getFeatures(Source.class).isEmpty()) {
            name = reference.getFeatures(Source.class).get(0).getOrganism();
        } else {
        	name = "";
        } if (!reference.getAccession().isEmpty()){
            name = reference.getAccession();

        }

        return name;
    }

    private DrawFrame nextFrame() {
        List<NamedElements> elements = getVisibleElements();
        NamedElements scale = sequences.getGlobalScale(column, sequencesWidth);
        return new DrawFrame(elements, scale, strand);
    }

    /**
     * Hide selected sequences.
     */
    public void hideSelected() {
        sequences.hideSelected();
        dialog.draw(nextFrame());
    }

    /**
     * Hide sequence at {@code index}.
     *
     * @param index index of sequence to hide
     */
    public void hide(int index) {
        sequences.hide(index);
        dialog.draw(nextFrame());
    }

    /**
     * Reveal sequence at {@code index}.
     *
     * @param index index of sequence to reveal
     */
    public void reveal(int index) {
        sequences.reveal(index);
        dialog.draw(nextFrame());
    }

    /**
     * Returns all {@link Gene} or {@link Annotation} features
     *
     * @return all {@link Gene} or {@link Annotation} features from selected
     *         sequences
     */
    public ObservableList<Feature> getGenes() {
        return sequences.getGenes();
    }

    @Override
    public int size() {
        return sequences.size();
    }

    @Override
    public void move(int source, int target) {
        sequences.move(source, target);
        dialog.draw(nextFrame());
    }

    @Override
    public ObservableList<AlignmentSequence> getElements() {
        return FXCollections.unmodifiableObservableList(sequences.getElements());
    }

    /**
     * Return a read only copy of all sequences
     *
     * @return all sequences
     */
    public ObservableList<AlignmentSequence> getAllSequences() {
        return sequences.getAllSequences();
    }

    public void applyAlignment(List<AlignmentSequence> sequences) {
        this.sequences.applyAlignment(sequences);
        dialog.draw(nextFrame());
    }
    
}
