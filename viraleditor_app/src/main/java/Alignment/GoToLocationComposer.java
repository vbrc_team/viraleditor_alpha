package Alignment;

import Shared.AbstractComposer;

public class GoToLocationComposer extends AbstractComposer<GoToLocationDialog> {
    private final SequencesComposer composer;

    public GoToLocationComposer(SequencesComposer composer) {
        this.composer = composer;
    }

    /**
     * Sets the alignment sequences to go to in.
     */
    public void setSequences() {
        dialog.setSequences(composer.getElements());
    }

    /**
     * 
     */
    public void setGappedPosition(int position) {
        composer.setColumn(position);
    }
}