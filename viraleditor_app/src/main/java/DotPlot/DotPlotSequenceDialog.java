package DotPlot;

import java.io.File;
import java.io.IOException;

import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.MenuBar;
import javafx.scene.control.TextArea;
import javafx.stage.FileChooser;
import javafx.stage.Stage;

public class DotPlotSequenceDialog {

    @FXML
    MenuBar dotPlotSequenceMenu;

    private DotPlotSequenceComposer composer;

    public void initComposer(DotPlotSequenceComposer composer) {
        this.composer = composer;
    }

    @FXML
    private TextArea hSeqText;
    @FXML
    private TextArea vSeqText;

    /**
     * Initializes the FXML file and its components.
     */
    @FXML
    public void initialize() {
        hSeqText.setEditable(false);
        vSeqText.setEditable(false);
    }


    /**
     * Opens a new Sequence window where the user can select their viruses
     * @return - dialog controller for the opened window
     */
    public static DotPlotSequenceDialog openWindow()  {
       try {
           FXMLLoader loader = new FXMLLoader(DotPlotSequenceDialog.class.getClassLoader().getResource("DotPlotSequence.fxml"));
           Parent root = loader.load();
           DotPlotSequenceDialog controller = loader.getController();
           controller.initComposer(new DotPlotSequenceComposer());

           controller.composer.setUp();
           controller.setUp();

           Stage popup = new Stage();
           popup.setTitle("Dot Plot");
           popup.setScene(new Scene(root));
           popup.show();
           return controller;
       }catch (IOException e){
           System.out.println("cant open Dot Plot Session");
           return null;
       }

    }


    private void setUp(){

    }

    /**
     * Imports a fasta file into the horizontal sequence list
     * 
     * @param actionEvent
     */
    @FXML
    public void importHSeq(ActionEvent actionEvent) {
        importFile('h');
    }

    /**
     * Imports a fasta file into the vertical sequence list
     * 
     * @param actionEvent
     */
    @FXML
    public void importVSeq(ActionEvent actionEvent) {
        importFile('v');
    }

    /**
     * Imports a JDotter file directly into the dotplot and opens the session window
     * 
     * @param actionEvent
     */
    @FXML
    public void importJDotter(ActionEvent actionEvent) {
        importFile('j');
    }

    /**
     * Imports a Dotter file directly into the dotplot and opens the session window
     * 
     * @param actionEvent
     */
    @FXML
    public void importDotter(ActionEvent actionEvent) {
        importFile('d');
    }

    /**
     * Imports a file directly
     * 
     * @param key indicates which type of file and how the data is handled
     */
    private void importFile(char key) {
        FileChooser fc = new FileChooser();
        fc.setTitle("Open File");

        if (key == 'v' || key == 'h') {
            fc.getExtensionFilters().addAll(new FileChooser.ExtensionFilter("FASTA", "*.fasta"));
        } else if (key == 'j') {
            fc.getExtensionFilters().addAll(new FileChooser.ExtensionFilter("JDotter", "*.jdt"));
        } else {
            fc.getExtensionFilters().addAll(new FileChooser.ExtensionFilter("Dotter", "*.dot"));
        }

        // open window
        File file = fc.showOpenDialog(null);
        if (file != null) {
            String name = file.getName();
            System.out.println(name);
        }
    }

    /**
     * Closes the sequence window from the menu.
     * 
     * @param actionEvent
     */
    @FXML
    private void quitMenuAction(ActionEvent actionEvent) {
        composer.closeFromMenu(dotPlotSequenceMenu);
    }

    /**
     * Closes the sequence window from the interface (button).
     * 
     * @param actionEvent
     */
    @FXML
    private void closeButtonAction(ActionEvent actionEvent) {
        composer.close(actionEvent);
    }

    /**
     * Opens the session window from the interface (button)
     * 
     * @param actionEvent
     */
    @FXML
    private void openSessionAction(ActionEvent actionEvent) {
        composer.openSession(actionEvent);
    }

    /**
     * Opens the UI preferences from the menu
     * 
     * @param actionEvent
     */
    @FXML
    private void openPrefsAction(ActionEvent actionEvent) {
        composer.openPrefs(actionEvent);
    }

    /**
     * Opens DotPlot help page in browser
     * 
     * @param actionEvent
     */
    @FXML
    private void helpMenuAction(ActionEvent actionEvent) {
        composer.getOnlineHelp(actionEvent);
    }
}