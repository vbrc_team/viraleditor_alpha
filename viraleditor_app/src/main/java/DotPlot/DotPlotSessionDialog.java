package DotPlot;

import java.io.File;
import java.io.FileInputStream;
import java.io.ObjectInputStream;

import Shared.interchange.Interchange;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.MenuBar;
import javafx.scene.control.TextArea;
import javafx.scene.control.TitledPane;
import javafx.stage.FileChooser;
import javafx.stage.Stage;

public class DotPlotSessionDialog {

    private DotPlotSessionComposer composer;

    @FXML
    MenuBar dotPlotSessionMenu;

    @FXML
    TextArea plotInfoText;
    
    @FXML
    TitledPane alignmentTool;
    
    @FXML
    TitledPane greyMapTool;
    
    @FXML
    TitledPane plotInfo;
    
    /**
     * Initializes the FXML file and its components.
     */
    @FXML
    public void initialize() {
        plotInfoText.setEditable(false);
    }

    public void initComposer(DotPlotSessionComposer composer) {
        this.composer = composer;
    }

    /**
     * Sets up and displays session window
     * @param top - interchange for the top axis
     * @param left - interchange for the left axis
     */
    public static void openSessionWindow(Interchange top, Interchange left) {
        try {
            FXMLLoader loader = new FXMLLoader(DotPlotSessionDialog.class.getClassLoader().getResource("DotPlotSession.fxml"));
            Parent root = loader.load();
            DotPlotSessionDialog controller = loader.getController();
            controller.initComposer(new DotPlotSessionComposer());

            //TODO: deal with interchange here

            Stage popup = new Stage();
            popup.setTitle("Dot Plot Session");
            popup.setScene(new Scene(root));
            popup.show();
        } catch (Exception e) {
            System.out.println("Can't load window");
            e.printStackTrace();
        }
    }

    /**
     * Opens DotPlot help page in browser
     * 
     * @param actionEvent
     */
    @FXML
    private void helpMenuAction(ActionEvent ev) {
        composer.getOnlineHelp(ev);
    }

    /**
     * Loads JDotter file into dot plot panel.
     */
    @FXML
    private void loadPlot() {

        FileChooser fc = new FileChooser();
        fc.setTitle("Open File");
        fc.getExtensionFilters().addAll(new FileChooser.ExtensionFilter("JDotter", "*.jdt"));
        DotPlotPanel dp;

        // open window
        File file = fc.showOpenDialog(null);
        if (file != null) {
            String name = file.getName();
            System.out.println(name);
        }
        try {
            DotPlotData data = null;
            ObjectInputStream in = new ObjectInputStream(new FileInputStream(file));
            data = (DotPlotData) in.readObject();
            dp = new DotPlotPanel(data);
            in.close();
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    /**
     * Closes the sequence window from the menu.
     */
    @FXML
    private void quitMenuAction() {
        composer.closeFromMenu(dotPlotSessionMenu);
    }

    /**
     * Opens the UI preferences from the menu
     * 
     * @param actionEvent
     */
    @FXML
    private void openPrefsAction(ActionEvent actionEvent) {
        composer.openPrefs(actionEvent, this);
    }
    
    /**
     * Opens a new window (see below) with a description of what the Data Plotter does. 
     */
    @FXML
    private void openAboutDotPlot() {
    	composer.openAbout();
    }
    
    
}