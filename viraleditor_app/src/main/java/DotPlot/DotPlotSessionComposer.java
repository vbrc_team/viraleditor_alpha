package DotPlot;

import java.util.HashMap;

import ViralEditor.ToolInformationDialog;
import ViralEditor.ToolInformationView;
import javafx.event.ActionEvent;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.MenuBar;
import javafx.stage.Stage;
import javafx.stage.StageStyle;

public class DotPlotSessionComposer {
    
    private static final String HELP_SITE = "https://4virology.net/virology-ca-tools/jdotter/";

    private HashMap<String, String> toolInfo;
    
    /**
     * Opens DotPlot help page in browser
     * 
     * @param actionEvent
     */
    public void getOnlineHelp(ActionEvent actionEvent) {
        Shared.Functions.getOnlineHelp(HELP_SITE);
    }
    
    /**
     * Closes the DotPlot window from menu
     * 
     * @param dotPlotSessionMenu the menu attached to the window
     */
    public void closeFromMenu(MenuBar dotPlotSessionMenu) {
        Stage stage = (Stage) dotPlotSessionMenu.getScene().getWindow();
        System.out.println("Closing JDotter.  Goodbye :)");
        stage.close();
    }

    /**
     * Opens the UI preferences window
     * 
     * @param actionEvent
     */
    public void openPrefs(ActionEvent actionEvent, DotPlotSessionDialog plotSession) {
        try {
            FXMLLoader loader = new FXMLLoader(getClass().getClassLoader().getResource("DotPlotPrefs.fxml"));
            Parent root = loader.load();
            DotPlotPrefDialog controller = loader.getController();
            controller.initComposer(new DotPlotPrefComposer());
            controller.initSession(plotSession);

            Stage popup = new Stage();
            popup.initStyle(StageStyle.UTILITY);
            popup.setTitle("Preferences");
            popup.setScene(new Scene(root));
            popup.show();
        } catch (Exception e) {
            System.out.println("Can't load window");
            e.printStackTrace();
        }
    }
    
    /**
     * Opens a new window (see below) with a description of what the Data Plotter does. 
     */
	public void openAbout() {
        try {
        	String toolName = "DotPlot";
            FXMLLoader loader = new FXMLLoader(getClass().getClassLoader().getResource("ToolInformationDialog.fxml"));
            Parent root = loader.load();
            ToolInformationView controller = loader.getController();
            ((ToolInformationDialog)controller).init();
            controller.setTool(toolName);
            System.out.println("/assets/"+toolName.toLowerCase().replaceAll(" ", "")+".png");
            controller.setImage("/assets/"+toolName.toLowerCase().replaceAll(" ", "")+".png");
            Stage popup = new Stage();
            popup.setTitle(toolName + " Help");
            popup.setScene(new Scene(root));
            popup.show();
        } catch (Exception e) {
            System.out.println("Can't load window");
            e.printStackTrace();
        }
	}
}