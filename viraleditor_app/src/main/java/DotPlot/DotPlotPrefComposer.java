package DotPlot;

public class DotPlotPrefComposer {
	public Boolean littleEndian;
	public Boolean bigEndian;
	public Boolean virusNames;
	public Boolean virusAbbreviations;
	public Boolean alignment;
	public Boolean greyscale;
	public Boolean information;
	String selectType;
	String selectView;
	
    //composer
    public DotPlotPrefComposer(){
    	littleEndian = true;
    	bigEndian = false;
    	virusNames = true;
    	virusAbbreviations = true;
    }
    
    public void setType(String type) {
    	selectType = type;
    }
	
    public void setView(String view) {
    	selectView = view;
    }
}