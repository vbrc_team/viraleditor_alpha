package DotPlot;

import javafx.fxml.FXML;
import javafx.scene.control.*;
import javafx.stage.Stage;

public class DotPlotPrefDialog {

    private DotPlotPrefComposer composer;
    
    private DotPlotSessionDialog dotPlotSession;
    
    //main UI elements
    @FXML
    private Button applyButton;
    @FXML
    private Button closeButton;
    @FXML
    private CheckBox checkAlignment;
    @FXML 
    private CheckBox checkGreyscale;
    @FXML
    private CheckBox checkInformation;
    @FXML
    private RadioButton littleEndian;
    @FXML
    private RadioButton bigEndian;
    @FXML
    private RadioButton virusNames;
    @FXML
    private RadioButton virusAbbreviations;
    @FXML
    private ToggleGroup dotterType;
    @FXML
    private ToggleGroup defaultListView;

    public void initComposer(DotPlotPrefComposer composer) {
        this.composer = composer;
    }
    
    /**
     * pass DotPlotSessionDialog here for changing panes' visibility
     * restore interface visibility in preference check box.
     * 
     * @param plotSession - DotPlotSessionDialog
     */
    public void initSession(DotPlotSessionDialog plotSession) {
    	dotPlotSession = plotSession;
    	checkAlignment.setSelected(plotSession.alignmentTool.isVisible());
    	checkGreyscale.setSelected(plotSession.greyMapTool.isVisible());
    	checkInformation.setSelected(plotSession.plotInfo.isVisible());
    }
    
    /**
     * applying the preference change: interface visibility, doter type and list view
     */
    @FXML
    void applyPref() {
    	radioSelect();
    	interfacePref();
    }
    
    /**
     * getting radio button selection, set the values to composer
     */
    public void radioSelect() {
    	if (dotterType.getSelectedToggle() != null) {
    		composer.setType(((RadioButton) dotterType.getSelectedToggle()).getText());
    	}
    	if (defaultListView.getSelectedToggle() != null) {
    		composer.setView(((RadioButton) defaultListView.getSelectedToggle()).getText());
    	}
    }
    
    /**
     * changing panes' visibility based on the checkbox selection
     */
    public void interfacePref() {
    	dotPlotSession.alignmentTool.setVisible(checkAlignment.isSelected());
    	dotPlotSession.greyMapTool.setVisible(checkGreyscale.isSelected());
    	dotPlotSession.plotInfo.setVisible(checkInformation.isSelected());
    }

    @FXML
    void closePref() {
        Stage stage = (Stage) closeButton.getScene().getWindow();
        stage.close();
    }

}