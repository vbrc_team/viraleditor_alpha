package DotPlot;

/*
 * JDotter: Java Interface to Dotter
 * Copyright (C) 2003  Dr. Chris Upton University of Victoria
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */

import java.util.*;

/**
 * This class represents a dot matrix and all information related to it as
 * created by a dotter-type program. This class can be used as an interface to
 * send information to JDotter from a server running dotter in a shell, or java
 * classes which process their own information.
 * 
 * @author Ryan Brodie
 * Modified by Christian Knowles
 * 
 * This program is modified from JDotter in VBRC Tools
 */
public class DotPlotData implements java.io.Serializable {
    private static final long serialVersionUID = 6935151121693585805L;
    public static final int DNADNA = 0;
    public static final int DNAPROT = 1;
    public static final int PROTPROT = 2;

    // data members
    protected int m_type;
    protected byte[] m_buffer;
    protected int m_width;
    protected int m_height;

    // misc members
    protected int m_windowLen = 0;
    protected int m_zoom = 1;
    protected int m_pixelFac = 0;
    protected String m_matrixName = "";
    protected int[] m_matrix = new int[(24 * 24)];
    protected String[] m_matrixKeys;

    // sequence members
    protected List<String> m_hSeqNames;
    protected List<String> m_hSequences;
    protected List<String> m_vSeqNames;
    protected List<String> m_vSequences;

    protected int m_hStart = 1;
    protected int m_vStart = 1;
    protected int m_hLength = 0;
    protected int m_vLength = 0;

    protected List<?> m_hFeatures = new ArrayList<>();
    protected List<?> m_vFeatures = new ArrayList<>();

    public DotPlotData() {
        this(new byte[0], 0, 0, DNADNA);
    }

    /**
     * Create a new matrix
     * 
     * @param buffer    The matrix data
     * @param bufWidth  The width of the matrix
     * @param bufHeight The height of the matrix
     * @param type      The type of the matrix ( must be one of DNADNA or PROTPROT )
     *                  to indicate what the types of the sequences displayed are.
     */
    public DotPlotData(byte[] buffer, int bufWidth, int bufHeight, int type) {
        if (type != DNADNA && type != DNAPROT && type != PROTPROT)
            throw new IllegalArgumentException("\'type\' parameter must be one of " + "DotPlotData.DNADNA, "
                    + "DotPlotData.DNAPROT " + "or DotPlotData.PROTPROT");
        m_buffer = buffer;
        m_width = bufWidth;
        m_height = bufHeight;
        m_type = type;
        m_hSeqNames = new ArrayList<>();
        m_hSequences = new ArrayList<>();
        m_vSeqNames = new ArrayList<>();
        m_vSequences = new ArrayList<>();
    }

    /**
     * return the type of sequences this matrix represents
     */
    public int getType() {
        return m_type;
    }

    /**
     * get the databuffer which stores the raw matrix data
     */
    public byte[] getBuffer() {
        return m_buffer;
    }

    /**
     * Get the width of the buffer
     */
    public int getWidth() {
        return m_width;
    }

    /**
     * get the height of the buffer
     */
    public int getHeight() {
        return m_height;
    }

    /**
     * set the 'pixel factor' as defined in dotter files
     * 
     * @param newfac the new value
     */
    public void setPixelFactor(int newfac) {
        m_pixelFac = newfac;
    }

    /**
     * get the 'pixel factor' as defined in dotter files
     */
    public int getPixelFactor() {
        return m_pixelFac;
    }

    /**
     * set the list of features for the sequence on the vertical axis of the matrix.
     * 
     * @param newList the new list of features
     */
    public void setVertFeatures(List<?> newList) {
        m_vFeatures = newList;
    }

    /**
     * get the list of features for the sequence on the vertical axis of the matrix
     */
    public List<?> getVertFeatures() {
        return m_vFeatures;
    }

    /**
     * set the list of features for the sequence on the horizontal axis of the
     * matrix.
     * 
     * @param newList the new list of features
     */
    public void setHorizFeatures(List<?> newList) {
        m_hFeatures = newList;
    }

    /**
     * get the list of features for the sequence on the horizontal axis of the
     * matrix
     */
    public List<?> getHorizFeatures() {
        return m_hFeatures;
    }

    /**
     * add a sequence string to the list of horizontal sequences
     * 
     * @param name The name of the sequence
     * @param seq  The sequence itself (dna or protein, but must be consistent with
     *             previously added sequences)
     */
    public void addHorizSequence(String name, String seq) {
        if (m_hSeqNames.contains(name))
            throw new IllegalArgumentException(name + " Already exists in the list!");
        m_hSeqNames.add(name);
        m_hSequences.add(seq.toUpperCase());
        m_hLength += seq.length();
    }

    /**
     * add a sequence string to the list of vertical sequences
     * 
     * @param name The name of the sequence
     * @param seq  The sequence itself (dna or protein, but must be consistent with
     *             previously added sequences)
     */

    public void addVertSequence(String name, String seq) {
        if (m_vSeqNames.contains(name))
            throw new IllegalArgumentException(name + " Already exists in the list!");
        m_vSeqNames.add(name);
        m_vSequences.add(seq.toUpperCase());
        m_vLength += seq.length();
    }

    /**
     * get the list of strings representing the names of sequences in the horizontal
     * axis
     */
    public List<String> getHorizNames() {
        return m_hSeqNames;
    }

    /**
     * get the list of strings representing the names of sequences in the vertical
     * axis
     */
    public List<String> getVertNames() {
        return m_vSeqNames;
    }

    /**
     * get the list of strings representing the sequences in the horizontal axis
     */
    public List<String> getHorizSequences() {
        return m_hSequences;
    }

    /**
     * get the list of strings representing the sequences in the vertical axis
     */
    public List<String> getVertSequences() {
        return m_vSequences;
    }

    /**
     * if the vertical sequence is a subsequence of a larger sequence this returns
     * the starting position of this subsequence within the larger sequence
     */
    public int getVSeqStart() {
        return m_vStart;
    }

    /**
     * Set the vertical start position
     * 
     * @param newVal the value of the vertical start position
     */
    public void setVSeqStart(int newVal) {
        m_vStart = newVal;
    }

    /**
     * if the horizontal sequence is a subsequence of a larger sequence this returns
     * the starting position of this subsequence within the larger sequence
     */
    public int getHSeqStart() {
        return m_hStart;
    }

    /**
     * Set the horizontal start position
     * 
     * @param newVal the value of the horizontal start position
     */
    public void setHSeqStart(int newVal) {
        m_hStart = newVal;
    }

    /**
     * return the total length of all sequences on the horizontal axis
     */
    public int getHSeqLength() {
        return m_hLength;
    }

    /**
     * return the total length of all sequences on the vertical axis
     */
    public int getVSeqLength() {
        return m_vLength;
    }

    /**
     * set the sliding window size used to caluclate the dot matrix
     * 
     * @param newLen the new value for window length
     */
    public void setWindowLength(int newLen) {
        m_windowLen = newLen;
    }

    /**
     * get the sliding window size used to caluclate the dot matrix
     */
    public int getWindowLength() {
        return m_windowLen;
    }

    /**
     * set the zoom factor for this dot matrix in bases/pixel
     * 
     * @param newZoom the new zoom factor
     */
    public void setZoom(int newZoom) {
        m_zoom = newZoom;
    }

    /**
     * get the zoom factor for this dot matrix in bases/pixel
     */
    public int getZoom() {
        return m_zoom;
    }

    /**
     * Set the header for the scoring matrix. This should be as long as sqrt(
     * getMatrix() )
     * 
     * @param keys The new header sequence
     */
    public void setMatrixKeys(String[] keys) {
        m_matrixKeys = keys;
    }

    /**
     * get the header for the scoring matrix. This should be as long as sqrt(
     * getMatrix() )
     */
    public String[] getMatrixKeys() {
        return m_matrixKeys;
    }

    /**
     * Set the name of the scoring matrix used to evaluate this dot matrix
     * 
     * @param name the name of the scoring matrix
     */
    public void setMatrixName(String name) {
        m_matrixName = name;
    }

    /**
     * Get the name of the scoring matrix used to evaluate this dot matrix
     */
    public String getMatrixName() {
        return m_matrixName;
    }

    /**
     * Set the scoring matrix used to evaluate this dot matrix
     * 
     * @param matrix the matrix of integers used to score this dot matrix
     */
    public void setMatrix(int[] matrix) {
        m_matrix = matrix;
    }

    /**
     * Get the scoring matrix used to evaluate this dot matrix
     */
    public int[] getMatrix() {
        return m_matrix;
    }
}
