package DotPlot;

import java.awt.Color;
import java.awt.Font;
import java.awt.FontMetrics;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Component;
import java.awt.Image;
import java.awt.Point;
import java.awt.Rectangle;
import java.awt.RenderingHints;
import java.awt.RenderingHints.Key;
import java.awt.image.BufferedImage;
import java.awt.image.IndexColorModel;
import java.awt.image.MemoryImageSource;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

public class DotPlotPanel extends Component {

    private static final long serialVersionUID = 8667270900442527910L;
    private static final int SCALE = 256;
    private static final int DEF_MIN = 0;
    private static final int DEF_MAX = 255;

    private static final int SCALE_WIDTH = 18;
    private static final int FEATURE_WIDTH = 30;

    // data members
    private DotPlotData plotData;

    // color members
    private byte[] grey;
    private int lowThresh = DEF_MIN;
    private int highThresh = DEF_MAX;

    // image members
    private IndexColorModel colorModel;
    private MemoryImageSource imageSource;
    private Image image;
    private BufferedImage topScale;
    private BufferedImage leftScale;
    private BufferedImage topFeatures;
    private BufferedImage leftFeatures;

    private Map<Key, Object> renderHints;

    // selection members
    private Rectangle zoomRectangle;
    private Point crossPoint;

    private Feature hFeature;
    private Feature vFeature;

    private int fWid = FEATURE_WIDTH;

    public DotPlotPanel() {
        this(new DotPlotData(new byte[0], 0, 0, DotPlotData.DNADNA));
    }

    /**
     * Make a new panel to dispaly the given dataset
     * 
     * @param data The dataset to display
     */
    DotPlotPanel(DotPlotData data) {
        renderHints = new HashMap<>();
        renderHints.put(RenderingHints.KEY_ALPHA_INTERPOLATION, RenderingHints.VALUE_ALPHA_INTERPOLATION_SPEED);
        renderHints.put(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_OFF);
        renderHints.put(RenderingHints.KEY_COLOR_RENDERING, RenderingHints.VALUE_COLOR_RENDER_SPEED);
        renderHints.put(RenderingHints.KEY_DITHERING, RenderingHints.VALUE_DITHER_DISABLE);
        renderHints.put(RenderingHints.KEY_FRACTIONALMETRICS, RenderingHints.VALUE_FRACTIONALMETRICS_OFF);
        renderHints.put(RenderingHints.KEY_RENDERING, RenderingHints.VALUE_RENDER_SPEED);
        renderHints.put(RenderingHints.KEY_STROKE_CONTROL, RenderingHints.VALUE_STROKE_PURE);
        renderHints.put(RenderingHints.KEY_TEXT_ANTIALIASING, RenderingHints.VALUE_TEXT_ANTIALIAS_OFF);

        setData(data);

        setPosition(plotData.getHSeqLength() / 2, plotData.getVSeqLength() / 2);

        System.setProperty("awt.image.incrementalDraw", "true");
    }

    /**
     * get the dataset displayed by this panel
     * 
     * @return The <CODE>DotPlotData</CODE> object displayed by this panel
     */
    public DotPlotData getData() {
        return plotData;
    }

    /**
     * Set the dataset displayed by this panel
     * 
     * @param data The dataset to display
     */
    public void setData(DotPlotData data) {
        zoomRectangle = new Rectangle(0, 0, 0, 0);
        crossPoint = new Point(0, 0);

        plotData = data;

        initScaleImages();
        initFeatureImages();

        grey = new byte[SCALE];

        // setup the grey scale
        updateScale();

        imageSource = new MemoryImageSource(plotData.getWidth(), plotData.getHeight(), colorModel, plotData.getBuffer(),
                0, plotData.getWidth());
        imageSource.setAnimated(true);

        image = createImage(imageSource);

    }

    /**
     * init the images that display the scales on the screen
     */
    private void initScaleImages() {
        int horScaleDiv = 75 * plotData.getZoom();
        int vertScaleDiv = 75 * plotData.getZoom();

        if (System.getProperty("os.name").startsWith("Mac")) {
            topScale = new BufferedImage(plotData.getHSeqLength() / plotData.getZoom() + (SCALE_WIDTH + fWid) * 2,
                    (SCALE_WIDTH + fWid), BufferedImage.TYPE_INT_ARGB);
            leftScale = new BufferedImage(SCALE_WIDTH + fWid,
                    plotData.getVSeqLength() / plotData.getZoom() + (SCALE_WIDTH + fWid) * 2,
                    BufferedImage.TYPE_INT_ARGB);
        } else {
            topScale = new BufferedImage(plotData.getHSeqLength() / plotData.getZoom() + (SCALE_WIDTH + fWid) * 2,
                    (SCALE_WIDTH + fWid), BufferedImage.TYPE_BYTE_INDEXED);
            leftScale = new BufferedImage(SCALE_WIDTH + fWid,
                    plotData.getVSeqLength() / plotData.getZoom() + (SCALE_WIDTH + fWid) * 2,
                    BufferedImage.TYPE_BYTE_INDEXED);
        }

        Graphics2D g = topScale.createGraphics();
        g.setRenderingHints(renderHints);

        // top scale
        g.setColor(Color.white);
        g.fillRect(0, 0, plotData.getHSeqLength() / plotData.getZoom() + (SCALE_WIDTH + fWid) * 2,
                (SCALE_WIDTH + fWid));
        g.setColor(Color.black);
        int pos = (plotData.getHSeqStart() / horScaleDiv) * horScaleDiv;
        for (; pos <= plotData.getHSeqLength() + plotData.getHSeqStart(); pos += horScaleDiv) {
            if (sequenceToGraphics(pos - plotData.getHSeqStart()) < SCALE_WIDTH + fWid)
                continue;
            g.drawLine(sequenceToGraphics(pos - plotData.getHSeqStart()), SCALE_WIDTH + fWid - 5,
                    sequenceToGraphics(pos - plotData.getHSeqStart()), SCALE_WIDTH + fWid);
            g.drawString(pos + "",
                    sequenceToGraphics(pos - plotData.getHSeqStart())
                            - (int) (g.getFontMetrics().stringWidth(pos + "") / 2.0)
                            + (int) (g.getFontMetrics().stringWidth("0") / 2.0),
                    SCALE_WIDTH + fWid - 6);
        }

        g.dispose();

        // left scale
        g = leftScale.createGraphics();
        g.setRenderingHints(renderHints);

        g.rotate(1.5 * Math.PI, ((double) plotData.getVSeqLength() / plotData.getZoom()) / 2.0 + (SCALE_WIDTH + fWid),
                ((double) plotData.getVSeqLength() / plotData.getZoom()) / 2.0 + (SCALE_WIDTH + fWid));

        g.setColor(Color.white);
        g.fillRect(0, 0, plotData.getVSeqLength() / plotData.getZoom() + (SCALE_WIDTH + fWid) * 2,
                (SCALE_WIDTH + fWid));
        g.setColor(Color.black);

        pos = (plotData.getVSeqStart() / vertScaleDiv) * vertScaleDiv;
        for (; pos <= plotData.getVSeqLength() + plotData.getVSeqStart(); pos += vertScaleDiv) {
            if (sequenceToGraphics(pos - plotData.getHSeqStart()) < SCALE_WIDTH + fWid)
                continue;
            g.drawLine(
                    plotData.getVSeqLength() / plotData.getZoom() + (SCALE_WIDTH + fWid)
                            - sequenceToGraphics(pos - plotData.getVSeqStart()),
                    SCALE_WIDTH + fWid - 5, plotData.getVSeqLength() / plotData.getZoom() + (SCALE_WIDTH + fWid)
                            - sequenceToGraphics(pos - plotData.getVSeqStart()),
                    SCALE_WIDTH + fWid);

            // WINDOWS
            g.drawString(pos + "",
                    plotData.getVSeqLength() / plotData.getZoom() + (SCALE_WIDTH + fWid)
                            - sequenceToGraphics(pos - plotData.getVSeqStart())
                            - (int) (g.getFontMetrics().stringWidth(pos + "") / 2.0)
                            - (int) (g.getFontMetrics().stringWidth("0") / 2.0),
                    SCALE_WIDTH + fWid - 6);

        }

        g.dispose();
    }

    /**
     * init the feature images
     */
    private void initFeatureImages() {
        if (System.getProperty("os.name").startsWith("Mac")) {
            topFeatures = new BufferedImage(plotData.getHSeqLength() / plotData.getZoom() + (SCALE_WIDTH + fWid) * 2,
                    (fWid), BufferedImage.TYPE_INT_ARGB);
            leftFeatures = new BufferedImage(fWid,
                    plotData.getVSeqLength() / plotData.getZoom() + (SCALE_WIDTH + fWid) * 2, // RTB
                    BufferedImage.TYPE_INT_ARGB);

        } else {
            topFeatures = new BufferedImage(plotData.getHSeqLength() / plotData.getZoom() + (SCALE_WIDTH + fWid) * 2,
                    (fWid), BufferedImage.TYPE_BYTE_INDEXED);
            leftFeatures = new BufferedImage(fWid,
                    plotData.getVSeqLength() / plotData.getZoom() + (SCALE_WIDTH + fWid) * 2, // RTB
                    BufferedImage.TYPE_BYTE_INDEXED);
        }

        Font fo = new Font("", Font.PLAIN, 9);

        // top
        Graphics2D g2d = topFeatures.createGraphics();
        g2d.setRenderingHints(renderHints);
        g2d.setPaint(Color.white);
        g2d.fillRect(0, 0, plotData.getHSeqLength() / plotData.getZoom() + (SCALE_WIDTH + fWid) * 2, fWid);
        g2d.setFont(fo);

        // features
        Iterator<?> i = plotData.getHorizFeatures().iterator();
        while (i.hasNext()) {
            Feature f = (Feature) i.next();
            if (f.getStrand() == Feature.NEGATIVE)
                g2d.setPaint(Color.blue);
            else
                g2d.setPaint(Color.red);
            g2d.fillRect(sequenceToGraphics(f.getStart() - plotData.getHSeqStart()), fWid - 5,
                    Math.abs(f.getStop() - f.getStart()) / plotData.getZoom(), 5);
            g2d.setPaint(Color.black);

            boolean showName = true;
            FontMetrics met = g2d.getFontMetrics();
            int strSize = met.stringWidth(f.getName());
            if (strSize > Math.abs(f.getStop() - f.getStart()) / plotData.getZoom())
                showName = false;

            if (showName) {

                g2d.drawString(f.getName(), sequenceToGraphics((f.getStart() - plotData.getHSeqStart()) + 5), fWid - 7);
            }
        }
        // title

        g2d.setPaint(Color.white);
        g2d.fillRect(0, 0, SCALE_WIDTH + fWid, SCALE_WIDTH + fWid);

        g2d.dispose();

        // left
        g2d = leftFeatures.createGraphics();
        g2d.setRenderingHints(renderHints);
        g2d.setFont(fo);
        g2d.rotate(1.5 * Math.PI, ((double) plotData.getVSeqLength() / plotData.getZoom()) / 2.0 + (SCALE_WIDTH + fWid),
                ((double) plotData.getVSeqLength() / plotData.getZoom()) / 2.0 + (SCALE_WIDTH + fWid));
        System.out.println(plotData.getVSeqLength() / plotData.getZoom() + (SCALE_WIDTH + fWid) * 2);
        g2d.setColor(Color.white);
        g2d.fillRect(0, 0, plotData.getVSeqLength() / plotData.getZoom() + (SCALE_WIDTH + fWid) * 2,
                (SCALE_WIDTH + fWid));

        // features
        i = plotData.getVertFeatures().iterator();
        while (i.hasNext()) {
            Feature f = (Feature) i.next();
            if (f.getStrand() == Feature.NEGATIVE)
                g2d.setPaint(Color.blue);
            else
                g2d.setPaint(Color.red);

            g2d.fillRect(
                    plotData.getVSeqLength() / plotData.getZoom() + (SCALE_WIDTH + fWid)
                            - sequenceToGraphics(f.getStart() - plotData.getVSeqStart())
                            - Math.abs(f.getStop() - f.getStart()) / plotData.getZoom(),
                    fWid - 6, Math.abs(f.getStop() - f.getStart()) / plotData.getZoom(), 5);
            g2d.setPaint(Color.black);

            boolean showName = true;
            FontMetrics met = g2d.getFontMetrics();
            int strSize = met.stringWidth(f.getName());
            if (strSize > Math.abs(f.getStop() - f.getStart()) / plotData.getZoom())
                showName = false;

            if (showName) {
                g2d.drawString(f.getName(),
                        plotData.getVSeqLength() / plotData.getZoom() + (SCALE_WIDTH + fWid)
                                - sequenceToGraphics(f.getStart() - plotData.getVSeqStart())
                                - Math.abs(f.getStop() - f.getStart()) / plotData.getZoom(),
                        fWid - 8);
            }
        }

        g2d.dispose();

    }

    /**
     * convert the given sequence position to the graphics location on the screen
     */
    private int sequenceToGraphics(int position) {
        return SCALE_WIDTH + fWid + (position / plotData.getZoom());
    }

    /**
     * update the grey map scale information in this object
     */
    private void updateScale() {
        float spread = Math.max((float) 1, Math.abs((float) highThresh - (float) lowThresh));
        float div = Math.max((float) 1, ((float) 255 / spread));
        float val;

        for (int i = 0; i < grey.length; i++) {
            if (highThresh > lowThresh) {
                if (i <= lowThresh)
                    val = (float) (SCALE - 1);
                else if (i < highThresh)
                    val = (float) ((1.0 * SCALE - 1.0) - ((float) i - (float) lowThresh) * div);
                else
                    val = (float) 0;
            } else {
                if (i <= highThresh)
                    val = (float) (0.0);
                else if (i < lowThresh)
                    val = (((float) i - (float) highThresh) * div);
                else
                    val = (float) (SCALE - 1);
            }
            grey[i] = (byte) val;
        }
        colorModel = new IndexColorModel(8, SCALE, grey, grey, grey);
    }

    void setPosition(int hpos, int ypos) {
        if (hpos == (int) crossPoint.getX() && ypos == (int) crossPoint.getY())
            return;
        boolean repaintscreen = false;
        if (Math.abs(sequenceToGraphics(hpos) - sequenceToGraphics((int) crossPoint.getX())) > 100
                || Math.abs(sequenceToGraphics(ypos) - sequenceToGraphics((int) crossPoint.getY())) > 100)
            repaintscreen = true;

        if (hpos < 0)
            hpos = 0;
        if (ypos < 0)
            ypos = 0;
        if (hpos >= plotData.getHSeqLength())
            hpos = plotData.getHSeqLength();
        if (ypos >= plotData.getVSeqLength())
            ypos = plotData.getVSeqLength();

        int x1 = Math.max(sequenceToGraphics(hpos) - 50, SCALE_WIDTH + fWid);
        int y1 = Math.max(sequenceToGraphics(ypos) - 50, SCALE_WIDTH + fWid);
        int x2 = Math.min(sequenceToGraphics(hpos) + 50, plotData.getWidth() + SCALE_WIDTH + fWid);
        int y2 = Math.min(sequenceToGraphics(ypos) + 50, plotData.getHeight() + SCALE_WIDTH + fWid);

        if (repaintscreen) {
            repaint(10);
        } else {
            repaint(x1 - 55, y1 - 55, x2 - x1 + 210, y2 - y1 + 110);
        }

        crossPoint.setLocation(hpos, ypos);

    }

    public void paintComponent(final Graphics og) {

        Rectangle rect = getBounds();

        og.clearRect(0, 0, (int) rect.getWidth(), (int) rect.getHeight());

        // dot plot
        paintImage(og);

        paintHeaderBG(og);
        paintScales(og);

        paintFeatures(og);

        // broken sequence borders
        if (plotData.getHorizSequences().size() > 1 || plotData.getVertSequences().size() > 1) {
            paintBorders(og);
        }

        // cross hairs
        paintCrosshairs(og, (int) crossPoint.getX(), (int) crossPoint.getY());

        // zoom rectangle
        paintZoomRect(og, (int) zoomRectangle.getX(), (int) zoomRectangle.getY(), (int) zoomRectangle.getWidth(),
                (int) zoomRectangle.getHeight());

        // feature selections
        paintFeatureSelections(og);

        og.setColor(Color.white);
        og.fillRect(0, 0, SCALE_WIDTH + fWid, SCALE_WIDTH + fWid);

        og.dispose();
    }

    /**
     * paint the feature selection rectangle on the screen
     */
    private void paintFeatureSelections(Graphics g) {
        Color c = new Color(0, 0, 0, 20);
        g.setColor(c);
        if (hFeature != null) {
            int x1 = sequenceToGraphics(hFeature.getStart() - plotData.getHSeqStart());
            int y1 = 0;
            int x2 = sequenceToGraphics(hFeature.getStop() - plotData.getHSeqStart());
            int y2 = SCALE_WIDTH + fWid;
            g.fillRect(x1, y1, Math.abs(x2 - x1) + 1, Math.abs(y2 - y1) + 1);
        }

        if (vFeature != null) {
            int y1 = sequenceToGraphics(vFeature.getStart() - plotData.getVSeqStart());
            int x1 = 0;
            int y2 = sequenceToGraphics(vFeature.getStop() - plotData.getVSeqStart());
            int x2 = SCALE_WIDTH + fWid;
            g.fillRect(x1, y1, Math.abs(x2 - x1) + 1, Math.abs(y2 - y1) + 1);
        }
    }

    /**
     * paint the background for headers on the screen
     */
    private void paintHeaderBG(Graphics g) {
        g.setColor(Color.white);
        g.fillRect(0, 0, plotData.getWidth() + (SCALE_WIDTH + fWid) * 2, SCALE_WIDTH + fWid);
        g.fillRect(0, 0, SCALE_WIDTH + fWid, plotData.getHeight() + (SCALE_WIDTH + fWid) * 2);
        g.fillRect(plotData.getWidth() + (SCALE_WIDTH + fWid), 0, SCALE_WIDTH + fWid,
                plotData.getHeight() + (SCALE_WIDTH + fWid) * 2);
        g.fillRect(0, plotData.getHeight() + (SCALE_WIDTH + fWid), plotData.getWidth() + (SCALE_WIDTH + fWid) * 2,
                SCALE_WIDTH + fWid);
        g.setColor(Color.black);
        g.drawRect(SCALE_WIDTH + fWid, SCALE_WIDTH + fWid, plotData.getWidth(), plotData.getHeight());
    }

    /**
     * paint the zoom rectangle on the screen with the given dimensions
     */
    private void paintZoomRect(Graphics g, int x, int y, int w, int h) {
        g.setColor(Color.red);
        g.drawRect(sequenceToGraphics(x), sequenceToGraphics(y), w / plotData.getZoom(), h / plotData.getZoom());
    }

    /**
     * paint the crosshairs on the screen with the given dimensions
     */
    private void paintCrosshairs(Graphics g, int x, int y) {
        g.setColor(Color.blue);
        g.drawLine(Math.max(sequenceToGraphics(x), SCALE_WIDTH + fWid),
                Math.max(sequenceToGraphics(y) - 50, SCALE_WIDTH + fWid),
                Math.max(sequenceToGraphics(x), SCALE_WIDTH + fWid),
                Math.min(sequenceToGraphics(y) + 50, plotData.getHeight() + SCALE_WIDTH + fWid));

        g.drawLine(Math.max(sequenceToGraphics(x) - 50, SCALE_WIDTH + fWid),
                Math.max(sequenceToGraphics(y), SCALE_WIDTH + fWid),
                Math.max(sequenceToGraphics(x) + 50, SCALE_WIDTH + fWid),
                Math.min(sequenceToGraphics(y), plotData.getHeight() + SCALE_WIDTH + fWid));
        g.drawString("(" + (x + plotData.getHSeqStart()) + ", " + (y + plotData.getVSeqStart()) + ")",
                sequenceToGraphics(x) + 3, sequenceToGraphics(y) + 12);
    }

    /**
     * paint the matrix image on the screen
     */
    private void paintImage(Graphics g) {
        int x = SCALE_WIDTH + fWid; // x offset
        int y = SCALE_WIDTH + fWid; // y offset
        g.drawImage(image, x, y, DotPlotPanel.this);
    }

    /**
     * paint the features on the header bars
     */
    private void paintFeatures(Graphics g) {
        g.drawImage(topFeatures, 0, 0, this);
        g.drawImage(leftFeatures, 0, -(SCALE_WIDTH + fWid), this);
    }

    /**
     * paint the scales on the screen
     */
    private void paintScales(Graphics g) {
        g.drawImage(topScale, 0, 0, this);
        g.drawImage(leftScale, 0, -(SCALE_WIDTH + fWid), this);
    }

    /**
     * paint the broken sequence borders
     */
    private void paintBorders(Graphics g) {
        Rectangle r = getBounds();
        int height = (int) r.getHeight();
        int width = (int) r.getWidth();

        Iterator<?> seqs = plotData.getHorizSequences().iterator();

        int pos = 0;
        int count = 1;
        while (seqs.hasNext()) {
            pos += ((String) seqs.next()).length();
            g.setColor(Color.green);
            g.drawLine(sequenceToGraphics(pos), SCALE_WIDTH + fWid, sequenceToGraphics(pos),
                    height - (SCALE_WIDTH + fWid) + 5);
            g.setColor(Color.black);
            g.drawString("H-" + count, sequenceToGraphics(pos) - 2, height - (SCALE_WIDTH + fWid) + 17);
            count++;
        }

        seqs = plotData.getVertSequences().iterator();
        pos = 0;
        count = 1;
        while (seqs.hasNext()) {
            pos += ((String) seqs.next()).length();
            g.setColor(Color.green);
            g.drawLine(SCALE_WIDTH + fWid, sequenceToGraphics(pos), width - (SCALE_WIDTH + fWid) + 5,
                    sequenceToGraphics(pos));
            g.setColor(Color.black);
            g.drawString("V-" + count, width - (SCALE_WIDTH + fWid) + 7, sequenceToGraphics(pos));
            count++;
        }
    }
}
