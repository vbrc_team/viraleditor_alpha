package DotPlot;

public class Feature {

    /** Positive Strand */
    static final int POSITIVE = 1;
    /** Neutral (no) Strand */
    private static final int NEUTRAL = 0;
    /** Negative (bottom) Strand */
    static final int NEGATIVE = -1;

    private int m_start;
    private int m_stop;
    private int m_strand;
    private String m_name;

    /**
     * Constructs a new Feature
     * 
     * @param start  The first base location of the feature
     * @param stop   The last base location of the feature
     * @param strand The strand of the feature
     * @param name   The name given to the feature
     */
    Feature(int start, int stop, int strand, String name) {
        if (strand != POSITIVE && strand != NEUTRAL && strand != NEGATIVE) {
            throw new IllegalArgumentException("Strand argument must be one of POSITIVE, NEGATIVE or NEUTRAL");
        }

        m_start = start;
        m_stop = stop;
        m_name = name;
        m_strand = strand;
    }

    /** returns the name of this feature */
    public String getName() {
        return m_name;
    }

    /** returns the first base position of this feature */
    int getStart() {
        return m_start;
    }

    /** returns the last base position of this feature */
    int getStop() {
        return m_stop;
    }

    /** returns the strand of this feature */
    int getStrand() {
        return m_strand;
    }
}
