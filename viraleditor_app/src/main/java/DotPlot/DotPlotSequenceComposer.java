package DotPlot;

import Shared.interchange.Interchange;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.MenuBar;
import javafx.stage.Stage;
import javafx.stage.StageStyle;

public class DotPlotSequenceComposer {

    private static final String HELP_SITE = "https://4virology.net/virology-ca-tools/jdotter/";

    public void setUp(){

    }

    /**
     * Closes the DotPlot window
     * 
     * @param actionEvent
     */
    @FXML
    public void close(ActionEvent actionEvent) {
        final Node source = (Node) actionEvent.getSource();
        final Stage stage = (Stage) source.getScene().getWindow();
        System.out.println("Closing JDotter.  Goodbye :)");
        stage.close();
    }

    /**
     * Closes the DotPlot window from menu
     * 
     * @param dotPlotSequenceMenu the menu attached to the window
     */
    public void closeFromMenu(MenuBar dotPlotSequenceMenu) {
        Stage stage = (Stage) dotPlotSequenceMenu.getScene().getWindow();
        System.out.println("Closing JDotter.  Goodbye :)");
        stage.close();
    }

    /**
     * Opens a new dot plot session
     * 
     * @param actionEvent
     */
    public void openSession(ActionEvent actionEvent) {
        //TODO: hook up data here
        DotPlotSessionDialog.openSessionWindow(Interchange.builder().build(), Interchange.builder().build());
    }

    /**
     * Opens the UI preferences window
     * 
     * @param actionEvent
     */
    public void openPrefs(ActionEvent actionEvent) {
        try {
            FXMLLoader loader = new FXMLLoader(getClass().getClassLoader().getResource("DotPlotPrefs.fxml"));
            Parent root = loader.load();
            DotPlotPrefDialog controller = loader.getController();
            controller.initComposer(new DotPlotPrefComposer());

            Stage popup = new Stage();
            popup.initStyle(StageStyle.UTILITY);
            popup.setTitle("Preferences");
            popup.setScene(new Scene(root));
            popup.show();
        } catch (Exception e) {
            System.out.println("Can't load window");
            e.printStackTrace();
        }
    }

    /**
     * Opens DotPlot help page in browser
     * 
     * @param actionEvent
     */
    public void getOnlineHelp(ActionEvent actionEvent) {
        Shared.Functions.getOnlineHelp(HELP_SITE);
    }

}