package Alignment.structures;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

import java.util.ArrayList;
import java.util.List;

import org.junit.jupiter.api.Test;

class UngappedPositionTest {

    @Test
    void no_gaps_have_same_ungapped_position() {
        String sequence = "ACGTN";
        UngappedPosition gapped = new UngappedPosition(sequence);
        List<Integer> positions = new ArrayList<>();
        positions.add(0);
        positions.add(1);
        positions.add(2);
        positions.add(3);
        positions.add(4);

        for (int i = 0; i < gapped.size(); i++) {
            assertEquals(positions.get(i), gapped.ungapped(i));
        }
    }

    @Test
    void inserting_gap_increases_gapped_position() {
        String sequence = "ACGTN";
        UngappedPosition gapped = new UngappedPosition(sequence);
        List<Integer> positions = new ArrayList<>();
        positions.add(0);
        positions.add(1);
        positions.add(1);
        positions.add(2);
        positions.add(3);
        positions.add(4);

        gapped.insertGap(2);

        for (int i = 0; i < gapped.size(); i++) {
            assertEquals(positions.get(i), gapped.ungapped(i));
        }
    }

    @Test
    void deleting_gap_decreases_gapped_position() {
        String sequence = "ACGTN";
        UngappedPosition gapped = new UngappedPosition(sequence);
        List<Integer> positions = new ArrayList<>();
        positions.add(0);
        positions.add(1);
        positions.add(2);
        positions.add(3);
        positions.add(4);

        gapped.insertGap(2);
        gapped.deleteGap(2);

        for (int i = 0; i < gapped.size(); i++) {
            assertEquals(positions.get(i), gapped.ungapped(i));
        }
    }

    @Test
    void inserting_gap_into_empty_sequence_fails() {
        String sequence = "";
        UngappedPosition gapped = new UngappedPosition(sequence);
        assertThrows(IndexOutOfBoundsException.class, () -> gapped.insertGap(0));
    }

    @Test
    void deleting_gap_where_none_exists_does_nothing() {
        String sequence = "ACGTN";
        UngappedPosition gapped = new UngappedPosition(sequence);
        gapped.deleteGap(1);
        assertEquals(1, gapped.ungapped(1));
    }

    @Test
    void gaps_before_zero_when_sequence_has_no_gaps() {
        String sequence = "ACGTN";
        UngappedPosition gapped = new UngappedPosition(sequence);
        for (int i = 0; i < sequence.length(); i++) {
            assertEquals(0, gapped.gapsBefore(i));
        }
    }

    @Test
    void gaps_equals_number_of_gaps() {
        String sequence = "AC--GT--N";
        UngappedPosition gapped = new UngappedPosition(sequence);
        assertEquals(4, gapped.gapsBefore(sequence.length()));
    }

    @Test
    void gaps_equals_number_of_gaps_less_than_position() {
        String sequence = "AC--GT--N";
        UngappedPosition gapped = new UngappedPosition(sequence);
        assertEquals(0, gapped.gapsBefore(1));
        assertEquals(1, gapped.gapsBefore(3));
        assertEquals(2, gapped.gapsBefore(5));
    }
}
