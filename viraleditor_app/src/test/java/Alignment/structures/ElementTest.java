package Alignment.structures;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

import org.junit.jupiter.api.Test;

class ElementTest {
    @Test
    void throws_illegal_argument() {
        assertThrows(IllegalArgumentException.class, () -> new Element("", null));
    }

    @Test
    void get_symbol_returns_symbol() {
        Element element = new Element("A", null);
        assertEquals("A", element.getSymbol());
    }

    @Test
    void get_background_returns_background() {
        Element element = new Element("A", ElementType.NUCLEOTIDE);
        assertEquals(ElementType.NUCLEOTIDE, element.getType());
    }

    @Test
    void same_value_elements_are_equal() {
        Element one = new Element("A", ElementType.NUCLEOTIDE);
        Element two = new Element("A", ElementType.NUCLEOTIDE);
        assertEquals(one, two);
    }

    @Test
    void different_symbols_are_not_equal() {
        Element one = new Element("A", ElementType.NUCLEOTIDE);
        Element two = new Element("G", ElementType.NUCLEOTIDE);
        assertNotEquals(one, two);
    }

    @Test
    void equal_objects_have_equal_hashes() {
        for (int i = Character.MIN_VALUE; i < Character.MAX_VALUE; i++) {
            assertEquals(new Element("" + (char) i, ElementType.NUCLEOTIDE),
                    new Element("" + (char) i, ElementType.NUCLEOTIDE));
        }

        assertEquals(new Element("A", ElementType.NUCLEOTIDE).hashCode(),
                new Element("A", ElementType.NUCLEOTIDE).hashCode());
    }
}
