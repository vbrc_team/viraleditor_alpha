package Alignment.structures;

import static org.junit.jupiter.api.Assertions.assertEquals;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import Shared.interchange.Nucleotide;
import Shared.interchange.NucleotideSequence;
import Shared.interchange.Reference;
import Shared.interchange.StringToNucleotides;

class AlignmentSequenceTest {
    private List<Element> elements;
    private List<Nucleotide> nucleotides;

    @BeforeEach
    void createElements() {
        elements = new ArrayList<>();
        elements.add(new Element("A", ElementType.NUCLEOTIDE));
        elements.add(new Element("G", ElementType.NUCLEOTIDE));
        elements.add(new Element("C", ElementType.NUCLEOTIDE));
        elements.add(new Element("T", ElementType.NUCLEOTIDE));
        elements = Collections.unmodifiableList(elements);
    }

    @BeforeEach
    void createNucleotides() {
        nucleotides = Collections.unmodifiableList(new StringToNucleotides().to("ACGTN"));
    }

    @Test
    void get_name_returns_name() {
        Reference reference = Reference.builder().withOrigin(new NucleotideSequence(Collections.emptyList())).build();
        AlignmentSequence sequence = new AlignmentSequence("Name", reference);
        assertEquals("Name", sequence.getName());
    }

    @Test
    void insertGap_increases_getNumberOfColumns() {
        Reference reference = Reference.builder().withOrigin(new NucleotideSequence(nucleotides)).build();
        AlignmentSequence sequence = new AlignmentSequence("Name", reference);

        final int initialLength = sequence.getNumberOfColumns();
        sequence.insertGap(1, 5);
        assertEquals(initialLength + 5, sequence.getNumberOfColumns());
    }

    @Test
    void insertGap_gappedPosition() {
        Reference reference = Reference.builder().withOrigin(new NucleotideSequence(nucleotides)).build();
        AlignmentSequence sequence = new AlignmentSequence("Name", reference);

        sequence.insertGap(2, 5);

        int[] ungapped = { 0, 1, 1, 1, 1, 1, 1, 2, 3, 4 };
        for (int i = 0; i < sequence.getNumberOfColumns(); i++) {
            assertEquals(ungapped[i], sequence.ungappedPosition(i));
        }
    }

    @Test
    void removeGap_decreases_getNumberOfColumns() {
        Reference reference = Reference.builder().withOrigin(new NucleotideSequence(nucleotides)).build();
        AlignmentSequence sequence = new AlignmentSequence("Name", reference);
        final int initialLength = sequence.getNumberOfColumns();

        sequence.insertGap(1, 5);
        sequence.removeGaps(2, 3);

        assertEquals(initialLength + 2, sequence.getNumberOfColumns());
    }

    @Test
    void removeGap_gappedPosition() {
        Reference reference = Reference.builder().withOrigin(new NucleotideSequence(nucleotides)).build();
        AlignmentSequence sequence = new AlignmentSequence("Name", reference);

        sequence.insertGap(1, 5);
        sequence.removeGaps(2, 3);

        int[] ungapped = { 0, 0, 0, 1, 2, 3, 4 };
        for (int i = 0; i < sequence.getNumberOfColumns(); i++) {
            assertEquals(ungapped[i], sequence.ungappedPosition(i));
        }
    }

    @Test
    void removeGap_only_deletes_gaps() {
        Reference reference = Reference.builder().withOrigin(new NucleotideSequence(nucleotides)).build();
        AlignmentSequence sequence = new AlignmentSequence("Name", reference);

        sequence.insertGap(1, 5);
        sequence.removeGaps(2, 3);
        sequence.removeGaps(1, 3);

        int[] ungapped = { 0, 1, 2, 3, 4 };
        for (int i = 0; i < sequence.getNumberOfColumns(); i++) {
            int one = ungapped[i];
            int two = sequence.ungappedPosition(i);
            assertEquals(one, two);
        }
    }
}
