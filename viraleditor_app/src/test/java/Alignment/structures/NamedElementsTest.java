package Alignment.structures;

import static org.junit.jupiter.api.Assertions.assertEquals;

import java.util.ArrayList;

import org.junit.jupiter.api.Test;

class NamedElementsTest {

    @Test
    void get_name_returns_name() {
        NamedElements namedElements = new NamedElements("Name", new ArrayList<>());
        assertEquals("Name", namedElements.getName());
    }

    @Test
    void is_overline_returns_true() {
        NamedElements namedElements = new NamedElements("Name", new ArrayList<>(), true);
        assertEquals(true, namedElements.isOverline());
    }

    @Test
    void is_overline_returns_false() {
        NamedElements namedElements = new NamedElements("Name", new ArrayList<>(), false);
        assertEquals(false, namedElements.isOverline());

        namedElements = new NamedElements("Name", new ArrayList<>());
        assertEquals(false, namedElements.isOverline());
    }

    @Test
    void is_selected_returns_false_by_default() {
        NamedElements namedElements = new NamedElements("Name", new ArrayList<>(), false);
        assertEquals(false, namedElements.isSelected());
    }

    @Test
    void is_selected_returns_true_when_set() {
        NamedElements namedElements = new NamedElements("Name", new ArrayList<>(), false);
        namedElements.setSelected();
        assertEquals(true, namedElements.isSelected());
    }

}
