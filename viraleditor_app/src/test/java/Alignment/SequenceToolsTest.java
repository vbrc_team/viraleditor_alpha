package Alignment;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

import org.junit.jupiter.api.Test;


// Will be writing comments here, for reference when writing docs
class SequenceToolsTest {

	// Static fields and initializers //

    private static final String[] AMINO_ACIDS =
        {
                "F", "F", "L", "L", "Y", "Y", "*", "*",
                "L", "L", "L", "L", "H", "H", "Q", "Q", "I", "I", "I", "M", "N", "N",
                "K", "K", "V", "V", "V", "V", "D", "D", "E", "E", "S", "S", "S", "S",
                "C", "C", "*", "W",
                "P", "P", "P", "P", "R", "R", "R", "R", "T", "T", "T", "T", "S", "S",
                "R", "R", "A", "A", "A", "A", "G", "G", "G", "G", "F", "L", "S", "S",
                "S", "S", "S", "S", "S", "S", "S", "S", "S", "Y", "C", "L", "L", "L",
                "L", "L", "L", "L", "L", "L", "L", "L", "P", "P", "P", "P", "P", "P",
                "P", "P", "P", "P", "P", "H", "Q", "R", "R", "R", "R", "R", "R", "R",
                "R", "R", "R", "R", "I", "I", "I", "I", "T", "T", "T", "T", "T", "T",
                "T", "T", "T", "T", "T", "N", "K", "S", "R", "V", "V", "V", "V", "V",
                "V", "V", "V", "V", "V", "V", "A", "A", "A", "A", "A", "A", "A", "A",
                "A", "A", "A", "D", "E", "G", "G", "G", "G", "G", "G", "G", "G", "G",
                "G", "G", "L", "L", "L", "R", "R", "R"
        };

    private static final String[] CODONS =
        {
                "TTT", "TTC", "TTA", "TTG", "TAT", "TAC", "TAA", "TAG", "CTT", "CTC",
                "CTA", "CTG", "CAT", "CAC", "CAA", "CAG", "ATT", "ATC", "ATA", "ATG",
                "AAT", "AAC", "AAA", "AAG", "GTT", "GTC", "GTA", "GTG", "GAT", "GAC",
                "GAA", "GAG", "TCT", "TCC", "TCA", "TCG", "TGT", "TGC", "TGA", "TGG",
                "CCT", "CCC", "CCA", "CCG", "CGT", "CGC", "CGA", "CGG", "ACT", "ACC",
                "ACA", "ACG", "AGT", "AGC", "AGA", "AGG", "GCT", "GCC", "GCA", "GCG",
                "GGT", "GGC", "GGA", "GGG", "TTY", "TTR", "TCN", "TCR", "TCY", "TCK",
                "TCM", "TCS", "TCW", "TCB", "TCD", "TCH", "TCV", "TAY", "TGY", "CTN",
                "CTR", "CTY", "CTK", "CTM", "CTS", "CTW", "CTB", "CTD", "CTH", "CTV",
                "CCN", "CCR", "CCY", "CCK", "CCM", "CCS", "CCW", "CCB", "CCD", "CCH",
                "CCV", "CAY", "CAR", "CGN", "CGR", "CGY", "CGK", "CGM", "CGS", "CGW",
                "CGB", "CGD", "CGH", "CGV", "ATH", "ATY", "ATM", "ATW", "ACN", "ACR",
                "ACY", "ACK", "ACM", "ACS", "ACW", "ACB", "ACD", "ACH", "ACV", "AAY",
                "AAR", "AGY", "AGR", "GTN", "GTR", "GTY", "GTK", "GTM", "GTS", "GTW",
                "GTB", "GTD", "GTH", "GTV", "GCN", "GCR", "GCY", "GCK", "GCM", "GCS",
                "GCW", "GCB", "GCD", "GCH", "GCV", "GAY", "GAR", "GGN", "GGR", "GGY",
                "GGK", "GGM", "GGS", "GGW", "GGB", "GGD", "GGH", "GGV", "YTR", "YTA",
                "YTG", "MGR", "MGA", "MGG"
        };

	// takes a string of 3 codons, returns single related amino acid symbol.
	@Test
    public void getAminoAcid() {

		// test well-formatted input
		// these are taken fromSequenceTools (CODONS, AMINO_ACIDS)

		for (int i=0; i< CODONS.length; i++) {
			assertEquals(AMINO_ACIDS[i], SequenceTools.getAminoAcid(CODONS[i]));
		}

		// test poorly-formatted input:

		// with unexpected chars in string, expected return is 'X'
		assertEquals("X", SequenceTools.getAminoAcid("TT-"));
		assertEquals("X", SequenceTools.getAminoAcid("A K"));
		assertEquals("X", SequenceTools.getAminoAcid("foo"));

		// with strings that are too long / too short
		assertEquals("X", SequenceTools.getAminoAcid("GT"));
		assertEquals("X", SequenceTools.getAminoAcid("CGTA"));

		// with an empty string
		assertEquals("X", SequenceTools.getAminoAcid(""));

	}

	// takes a dna string, returns one without gaps ('-' char)
	@Test
    public void getUngappedBuffer() {

		String gap = "AC-GT";
		String no_gap = "ACGT";
		String only_gap = "----";
		String empty_str = "";

		StringBuffer no_gap_buff = new StringBuffer(no_gap);
		StringBuffer empty_buff = new StringBuffer("");

		assertEquals(no_gap_buff.toString(), SequenceTools.getUngappedBuffer(gap).toString());
		assertEquals(no_gap_buff.toString(), SequenceTools.getUngappedBuffer(no_gap).toString());
		assertEquals(empty_buff.toString(), SequenceTools.getUngappedBuffer(only_gap).toString());
		assertEquals(empty_buff.toString(), SequenceTools.getUngappedBuffer(empty_str).toString());

	}

    @Test
    void getGappedAAString_frame_one_returns_base_case() {
        String dna = "A";
        assertEquals("=>", SequenceTools.getGappedAAString(dna, 1));
    }

    @Test
    void getGappedAAString_frame_two_returns_base_case() {
        String dna = "AAA";
        assertEquals(" =>", SequenceTools.getGappedAAString(dna, 2));
    }

    @Test
    void getGappedAAString_frame_three_returns_base_case() {
        String dna = "AAAA";
        assertEquals("  =>", SequenceTools.getGappedAAString(dna, 3));
    }

    @Test
    void getGappedAAString_frame_one_returns_pretty_print() {
        String dna = "AAATTGCA";
        assertEquals("K=>L=>", SequenceTools.getGappedAAString(dna, 1));
    }

    @Test
    void getGappedAAString_frame_two_returns_pretty_print() {
        String dna = "AAATTGCA";
        assertEquals(" N=>C=>", SequenceTools.getGappedAAString(dna, 2));
    }

    @Test
    void getGappedAAString_frame_three_returns_pretty_print() {
        String dna = "AAATTGCA";
        assertEquals("  I=>A=>", SequenceTools.getGappedAAString(dna, 3));
    }

    @Test
    void getAAString_frame_one() {
        for (int i = 0; i < CODONS.length; i++) {
            assertEquals(AMINO_ACIDS[i], SequenceTools.getAAString(CODONS[i], 1));
        }
    }

    @Test
    void getAAString_frame_two() {
        for (int i = 0; i < CODONS.length; i++) {
            assertEquals(AMINO_ACIDS[i], SequenceTools.getAAString("A" + CODONS[i], 2));
        }
    }

    @Test
    void getAAString_frame_three() {
        for (int i = 0; i < CODONS.length; i++) {
            assertEquals(AMINO_ACIDS[i], SequenceTools.getAAString("AA" + CODONS[i], 3));
        }
    }

    @Test
    void getAAString_frame_less_than_one_throws_exception() {
        assertThrows(IllegalArgumentException.class, () -> SequenceTools.getAAString("AAA", 0));
    }

    @Test
    void getAAString_frame_greater_than_three_throws_exception() {
        assertThrows(IllegalArgumentException.class, () -> SequenceTools.getAAString("AAA", 4));
    }

    // These all look to be deprecated, tests should be written if they're used
	// @Test
	// void insertGaps() {}

	//@Test
	// void deleteGaps() {}

	// Only used in deleteGaps()
	// @Test
	// void deleteSelection() {}

	//@Test
	// void getDifferences() {}

	// for getDNAComplement char method
	// @Test
	// void getBaseComplement() {}

	// for getDNAComplement string method
	// @Test
    // void getDNAComplement() {}
}
