package Alignment;

public class AlignmentComposerTest {
/*	
=======

>>>>>>> b25f4fac65eb0a3281f321c30b6dd14d3ed8a26b
	AlignmentComposer composer;
	String pathToResource = "src/test/resources";
	File fastaFile = new File(pathToResource+"/229E.fasta");
	File genbankFile = new File(pathToResource+"/229E.gbk");

	static int[] start_loc;
	static int[] stop_loc;

	@BeforeAll
	static void initValidationData() {
		start_loc = new int[] {293, 293, 20570, 24091, 24482, 24750, 24995, 25686};
		stop_loc = new int[] {20568, 12550, 24091, 24492, 24748, 24983, 25672, 26855};
	}

	@BeforeEach
	void initComposer() {
		composer = new AlignmentComposer();
	}

	@Test
	void setUp() {

		ArrayList<Interchange> interchangeList = new ArrayList<Interchange>();
		interchangeList.add(new Interchange(fastaFile));
		interchangeList.add(new Interchange(genbankFile));

		composer.setUp(interchangeList);
		List<FeaturedSequence> featuredSeq = composer.getFeaturedSequences();

		// Ensure both loaded
		assertEquals(2, featuredSeq.size());

		// Check genome sequence against hash
		assertEquals(2100161200, featuredSeq.get(0).toString().hashCode());
		assertEquals(2100161200, featuredSeq.get(1).toString().hashCode());

		// Check features loaded correctly (only the genbank ones include features)
		// TODO: the rest of the data?
		List<Feature> features = featuredSeq.get(1).getFeatures();
		for (int i=0; i<8; i++) {
			assertEquals(start_loc[i], features.get(i).getLocation().getStart());
			assertEquals(stop_loc[i], features.get(i).getLocation().getEnd());
		}

	};

	@Test
	void addFiles() {

		ArrayList<File> fileArr = new ArrayList<File>();
		fileArr.add(fastaFile);
		fileArr.add(genbankFile);

		composer.addFiles(fileArr);
		List<FeaturedSequence> featuredSeq = composer.getFeaturedSequences();

		// Ensure both loaded
		assertEquals(2, featuredSeq.size());

		// Check genome sequence against hash
		assertEquals(2100161200, featuredSeq.get(0).toString().hashCode());
		assertEquals(2100161200, featuredSeq.get(1).toString().hashCode());

		// Check features loaded correctly (only the genbank ones include features)
		// TODO: the rest of the data?
		List<Feature> features = featuredSeq.get(1).getFeatures();
		for (int i=0; i<8; i++) {
			assertEquals(start_loc[i], features.get(i).getLocation().getStart());
			assertEquals(stop_loc[i], features.get(i).getLocation().getEnd());
		}

	};

	@Test
	void addSequence() {
		Genome genome = composer.addSequence(0, "test", "this is a test sequence");

		assertEquals(0, genome.id);
		assertEquals("test", genome.name);
		assertEquals("this is a test sequence", genome.getDNA());

	};
<<<<<<< HEAD
	*/
}
