package CodonStats;

import Shared.Gene;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.ArrayList;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

class CodonStatsNucleotideComposerTest {

    CodonStatsNucleotideComposer composer;
    ArrayList<NucleotideCount> nucleotideCounts;

    //Stats for Nucleotide A
    double[] seq4TableNucleotidesA = new double[]{0.2488, 0.3500, 0.3000, 0.3500};
    double[] seq5TableNucleotidesA = new double[]{0.2959, 0.2785, 0.4051, 0.3165};
    double[] seq6TableNucleotidesA = new double[]{0.2863, 0.3731, 0.3284, 0.2985};
    double[] sumA = new double[]{0.8310, 1.0016, 1.0334, 0.9650};
    double[] meanA = new double[]{0.2770, 0.3339, 0.3445, 0.3217};
    double[] varA = new double[]{0.6206, 2.4349, 2.9544, 0.6831};
    //Stats for Nucleotide C
    double[] seq4TableNucleotidesC = new double[]{0.1940, 0.3077, 0.3718, 0.3205};
    double[] seq5TableNucleotidesC = new double[]{0.1648, 0.2955, 0.2273, 0.4773};
    double[] seq6TableNucleotidesC = new double[]{0.1667, 0.4615, 0.2051, 0.3333};
    double[] sumC = new double[]{0.5255, 1.0647, 0.8042, 1.1311};
    double[] meanC = new double[]{0.1752, 0.3549, 0.2681, 0.3770};
    double[] varC = new double[]{0.2678, 8.5670, 8.1925, 7.5761};
    //Stats for Nucleotide G
    double[] seq4TableNucleotidesG = new double[]{0.1990, 0.4625, 0.2375, 0.3000};
    double[] seq5TableNucleotidesG = new double[]{0.2060, 0.5091, 0.3091, 0.1818};
    double[] seq6TableNucleotidesG = new double[]{0.1667, 0.4359, 0.2308, 0.3333};
    double[] sumG = new double[]{0.5717, 1.4075, 0.7774, 0.8152};
    double[] meanG = new double[]{0.1906, 0.4692, 0.2591, 0.2717};
    double[] varG = new double[]{0.4402, 1.3726, 1.8841, 6.3391};
    //Stats for Nucleotide T
    double[] seq4TableNucleotidesT = new double[]{0.3582, 0.2639, 0.3889, 0.3472};
    double[] seq5TableNucleotidesT = new double[]{0.3333, 0.2921, 0.3371, 0.3708};
    double[] seq6TableNucleotidesT = new double[]{0.3803, 0.2022, 0.4382, 0.3596};
    double[] sumT = new double[]{1.0719, 0.7583, 1.1642, 1.0776};
    double[] meanT = new double[]{0.3573, 0.2528, 0.3881, 0.3592};
    double[] varT = new double[]{0.5531, 2.1129, 2.5570, 0.1389};


    @BeforeEach
    void init() {
        composer = new CodonStatsNucleotideComposer();
        ArrayList<Gene> genes = setup();
        nucleotideCounts = new ArrayList<>();
        for (Gene gene : genes) {
            NucleotideCount nc = new NucleotideCount(gene.getGeneName(), gene.getDnaSeq());
            nucleotideCounts.add(nc);
        }
    }

    ArrayList<Gene> setup() {
        ArrayList<Gene> genes = new ArrayList<>();
        genes.add(addGene(4, "HCoV-229E-004", RefSequenceData.sequence4));
        genes.add(addGene(5, "HCoV-229E-005", RefSequenceData.sequence5));
        genes.add(addGene(6, "HCoV-229E-006", RefSequenceData.sequence6));
        return genes;
    }

    Gene addGene(int ID, String name, String sequence) {
        Gene gene = new Gene();
        gene.setGeneName(name);
        gene.setVirusID(ID);
        gene.setDNA(sequence);
        gene.setCompositions();
        return gene;
    }

    @Test
    void calculateNucleotideDisStatsA() {
        List<NucleotideStats> statistics = composer.calculateNucleotideDisStats('A', nucleotideCounts);
        assertEquals(seq4TableNucleotidesA[0], nucleotideCounts.get(0).getNucleotide());
        assertEquals(seq5TableNucleotidesA[0], nucleotideCounts.get(1).getNucleotide());
        assertEquals(seq6TableNucleotidesA[0], nucleotideCounts.get(2).getNucleotide());
        assertEquals(sumA[0], statistics.get(0).getNucleotideTotals());
        assertEquals(meanA[0], statistics.get(1).getNucleotideTotals());
        assertEquals(varA[0], statistics.get(2).getNucleotideTotals());

    }

    @Test
    void calculateNucleotideDisStatsA1() {
        List<NucleotideStats> statistics = composer.calculateNucleotideDisStats('A', nucleotideCounts);
        assertEquals(seq4TableNucleotidesA[1], nucleotideCounts.get(0).getNucleotide1());
        assertEquals(seq5TableNucleotidesA[1], nucleotideCounts.get(1).getNucleotide1());
        assertEquals(seq6TableNucleotidesA[1], nucleotideCounts.get(2).getNucleotide1());
        assertEquals(sumA[1], statistics.get(0).getNucleotideTotals1());
        assertEquals(meanA[1], statistics.get(1).getNucleotideTotals1());
        assertEquals(varA[1], statistics.get(2).getNucleotideTotals1());
    }

    @Test
    void calculateNucleotideDisStatsA2() {
        List<NucleotideStats> statistics = composer.calculateNucleotideDisStats('A', nucleotideCounts);
        assertEquals(seq4TableNucleotidesA[2], nucleotideCounts.get(0).getNucleotide2());
        assertEquals(seq5TableNucleotidesA[2], nucleotideCounts.get(1).getNucleotide2());
        assertEquals(seq6TableNucleotidesA[2], nucleotideCounts.get(2).getNucleotide2());
        assertEquals(sumA[2], statistics.get(0).getNucleotideTotals2());
        assertEquals(meanA[2], statistics.get(1).getNucleotideTotals2());
        assertEquals(varA[2], statistics.get(2).getNucleotideTotals2());
    }

    @Test
    void calculateNucleotideDisStatsA3() {
        List<NucleotideStats> statistics = composer.calculateNucleotideDisStats('A', nucleotideCounts);
        assertEquals(seq4TableNucleotidesA[3], nucleotideCounts.get(0).getNucleotide3());
        assertEquals(seq5TableNucleotidesA[3], nucleotideCounts.get(1).getNucleotide3());
        assertEquals(seq6TableNucleotidesA[3], nucleotideCounts.get(2).getNucleotide3());
        assertEquals(sumA[3], statistics.get(0).getNucleotideTotals3());
        assertEquals(meanA[3], statistics.get(1).getNucleotideTotals3());
        assertEquals(varA[3], statistics.get(2).getNucleotideTotals3());
    }

    @Test
    void calculateNucleotideDisStatsC() {
        List<NucleotideStats> statistics = composer.calculateNucleotideDisStats('C', nucleotideCounts);

        assertEquals(seq4TableNucleotidesC[0], nucleotideCounts.get(0).getNucleotide());
        assertEquals(seq5TableNucleotidesC[0], nucleotideCounts.get(1).getNucleotide());
        assertEquals(seq6TableNucleotidesC[0], nucleotideCounts.get(2).getNucleotide());
        assertEquals(sumC[0], statistics.get(0).getNucleotideTotals());
        assertEquals(meanC[0], statistics.get(1).getNucleotideTotals());
        assertEquals(varC[0], statistics.get(2).getNucleotideTotals());

    }

    @Test
    void calculateNucleotideDisStatsC1() {
        List<NucleotideStats> statistics = composer.calculateNucleotideDisStats('C', nucleotideCounts);

        assertEquals(seq4TableNucleotidesC[1], nucleotideCounts.get(0).getNucleotide1());
        assertEquals(seq5TableNucleotidesC[1], nucleotideCounts.get(1).getNucleotide1());
        assertEquals(seq6TableNucleotidesC[1], nucleotideCounts.get(2).getNucleotide1());
        assertEquals(sumC[1], statistics.get(0).getNucleotideTotals1());
        assertEquals(meanC[1], statistics.get(1).getNucleotideTotals1());
        assertEquals(varC[1], statistics.get(2).getNucleotideTotals1());
    }

    @Test
    void calculateNucleotideDisStatsC2() {
        List<NucleotideStats> statistics = composer.calculateNucleotideDisStats('C', nucleotideCounts);

        assertEquals(seq4TableNucleotidesC[2], nucleotideCounts.get(0).getNucleotide2());
        assertEquals(seq5TableNucleotidesC[2], nucleotideCounts.get(1).getNucleotide2());
        assertEquals(seq6TableNucleotidesC[2], nucleotideCounts.get(2).getNucleotide2());
        assertEquals(sumC[2], statistics.get(0).getNucleotideTotals2());
        assertEquals(meanC[2], statistics.get(1).getNucleotideTotals2());
        assertEquals(varC[2], statistics.get(2).getNucleotideTotals2());
    }

    @Test
    void calculateNucleotideDisStatsC3() {
        List<NucleotideStats> statistics = composer.calculateNucleotideDisStats('C', nucleotideCounts);

        assertEquals(seq4TableNucleotidesC[3], nucleotideCounts.get(0).getNucleotide3());
        assertEquals(seq5TableNucleotidesC[3], nucleotideCounts.get(1).getNucleotide3());
        assertEquals(seq6TableNucleotidesC[3], nucleotideCounts.get(2).getNucleotide3());
        assertEquals(sumC[3], statistics.get(0).getNucleotideTotals3());
        assertEquals(meanC[3], statistics.get(1).getNucleotideTotals3());
        assertEquals(varC[3], statistics.get(2).getNucleotideTotals3());
    }


    @Test
    void calculateNucleotideDisStatsG() {
        List<NucleotideStats> statistics = composer.calculateNucleotideDisStats('G', nucleotideCounts);

        assertEquals(seq4TableNucleotidesG[0], nucleotideCounts.get(0).getNucleotide());
        assertEquals(seq5TableNucleotidesG[0], nucleotideCounts.get(1).getNucleotide());
        assertEquals(seq6TableNucleotidesG[0], nucleotideCounts.get(2).getNucleotide());
        assertEquals(sumG[0], statistics.get(0).getNucleotideTotals());
        assertEquals(meanG[0], statistics.get(1).getNucleotideTotals());
        assertEquals(varG[0], statistics.get(2).getNucleotideTotals());

    }

    @Test
    void calculateNucleotideDisStatsG1() {
        List<NucleotideStats> statistics = composer.calculateNucleotideDisStats('G', nucleotideCounts);

        assertEquals(seq4TableNucleotidesG[1], nucleotideCounts.get(0).getNucleotide1());
        assertEquals(seq5TableNucleotidesG[1], nucleotideCounts.get(1).getNucleotide1());
        assertEquals(seq6TableNucleotidesG[1], nucleotideCounts.get(2).getNucleotide1());
        assertEquals(sumG[1], statistics.get(0).getNucleotideTotals1());
        assertEquals(meanG[1], statistics.get(1).getNucleotideTotals1());
        assertEquals(varG[1], statistics.get(2).getNucleotideTotals1());
    }

    @Test
    void calculateNucleotideDisStatsG2() {
        List<NucleotideStats> statistics = composer.calculateNucleotideDisStats('G', nucleotideCounts);

        assertEquals(seq4TableNucleotidesG[2], nucleotideCounts.get(0).getNucleotide2());
        assertEquals(seq5TableNucleotidesG[2], nucleotideCounts.get(1).getNucleotide2());
        assertEquals(seq6TableNucleotidesG[2], nucleotideCounts.get(2).getNucleotide2());
        assertEquals(sumG[2], statistics.get(0).getNucleotideTotals2());
        assertEquals(meanG[2], statistics.get(1).getNucleotideTotals2());
        assertEquals(varG[2], statistics.get(2).getNucleotideTotals2());
    }

    @Test
    void calculateNucleotideDisStatsG3() {
        List<NucleotideStats> statistics = composer.calculateNucleotideDisStats('G', nucleotideCounts);

        assertEquals(seq4TableNucleotidesG[3], nucleotideCounts.get(0).getNucleotide3());
        assertEquals(seq5TableNucleotidesG[3], nucleotideCounts.get(1).getNucleotide3());
        assertEquals(seq6TableNucleotidesG[3], nucleotideCounts.get(2).getNucleotide3());
        assertEquals(sumG[3], statistics.get(0).getNucleotideTotals3());
        assertEquals(meanG[3], statistics.get(1).getNucleotideTotals3());
        assertEquals(varG[3], statistics.get(2).getNucleotideTotals3());
    }

    @Test
    void calculateNucleotideDisStatsT() {
        List<NucleotideStats> statistics = composer.calculateNucleotideDisStats('T', nucleotideCounts);

        assertEquals(seq4TableNucleotidesT[0], nucleotideCounts.get(0).getNucleotide());
        assertEquals(seq5TableNucleotidesT[0], nucleotideCounts.get(1).getNucleotide());
        assertEquals(seq6TableNucleotidesT[0], nucleotideCounts.get(2).getNucleotide());
        assertEquals(sumT[0], statistics.get(0).getNucleotideTotals());
        assertEquals(meanT[0], statistics.get(1).getNucleotideTotals());
        assertEquals(varT[0], statistics.get(2).getNucleotideTotals());
    }

    @Test
    void calculateNucleotideDisStatsT1() {
        List<NucleotideStats> statistics = composer.calculateNucleotideDisStats('T', nucleotideCounts);

        assertEquals(seq4TableNucleotidesT[1], nucleotideCounts.get(0).getNucleotide1());
        assertEquals(seq5TableNucleotidesT[1], nucleotideCounts.get(1).getNucleotide1());
        assertEquals(seq6TableNucleotidesT[1], nucleotideCounts.get(2).getNucleotide1());
        assertEquals(sumT[1], statistics.get(0).getNucleotideTotals1());
        assertEquals(meanT[1], statistics.get(1).getNucleotideTotals1());
        assertEquals(varT[1], statistics.get(2).getNucleotideTotals1());
    }

    @Test
    void calculateNucleotideDisStatsT2() {
        List<NucleotideStats> statistics = composer.calculateNucleotideDisStats('T', nucleotideCounts);

        assertEquals(seq4TableNucleotidesT[2], nucleotideCounts.get(0).getNucleotide2());
        assertEquals(seq5TableNucleotidesT[2], nucleotideCounts.get(1).getNucleotide2());
        assertEquals(seq6TableNucleotidesT[2], nucleotideCounts.get(2).getNucleotide2());
        assertEquals(sumT[2], statistics.get(0).getNucleotideTotals2());
        assertEquals(meanT[2], statistics.get(1).getNucleotideTotals2());
        assertEquals(varT[2], statistics.get(2).getNucleotideTotals2());
    }

    @Test
    void calculateNucleotideDisStatsT3() {
        List<NucleotideStats> statistics = composer.calculateNucleotideDisStats('T', nucleotideCounts);

        assertEquals(seq4TableNucleotidesT[3], nucleotideCounts.get(0).getNucleotide3());
        assertEquals(seq5TableNucleotidesT[3], nucleotideCounts.get(1).getNucleotide3());
        assertEquals(seq6TableNucleotidesT[3], nucleotideCounts.get(2).getNucleotide3());
        assertEquals(sumT[3], statistics.get(0).getNucleotideTotals3());
        assertEquals(meanT[3], statistics.get(1).getNucleotideTotals3());
        assertEquals(varT[3], statistics.get(2).getNucleotideTotals3());
    }

}