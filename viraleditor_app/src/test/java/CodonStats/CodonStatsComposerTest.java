package CodonStats;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import Shared.interchange.*;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;

class CodonStatsComposerTest {
    private static final StringToNucleotides toNucleotides = new StringToNucleotides();

    CodonStatsComposer composer;
    String pathToResource = "src/test/resources";

    @BeforeEach
    void init() {
        composer = new CodonStatsComposer();
    }

    @Test
    void setType() {
        composer.setType("NT");
        assertEquals("NT", composer.getType());
    }

    @Test
    void getTitleAddition() {
        composer.setType("NT");
        String result = null;
        try {
            Method method = composer.getClass().getDeclaredMethod("getTitleAddition");
            method.setAccessible(true);
            result = (String) method.invoke(composer);
        } catch (NoSuchMethodException | InvocationTargetException | IllegalAccessException e) {
            e.printStackTrace();
        }
        assertNotNull(result);
        assertEquals(result, "Nucleotide");
    }

    @Test
    void saveData() {
        List<Feature> features = new ArrayList<>();
        List<Interval> intervals = new ArrayList<>();
        intervals.add(new Interval(false, 1, 2));
        features.add(Annotation.builder()
                               .withIntervals(intervals)
                               .withNTSequence(new NucleotideSequence(toNucleotides.to("ACGT")))
                               .withGeneID(1)
                               .withGeneAbbreviation("Name")
                               .build());
        List<Reference> references = new ArrayList<>();
        references.add(Reference.builder().withFeatures(features).build());
        composer.setUp(references);

        List<Gene> genes = composer.getGenes();
        assertNotNull(genes);
        Gene gene = genes.get(0);
        ObservableList<Gene> geneList = FXCollections.observableArrayList(genes);

        File file = new File(pathToResource + "/CodonStatsOutput.txt");
        try {
            composer.saveData(file, geneList);
        } catch (IOException e) {
            e.printStackTrace();
        }
        //what output should be
        String firstLine = "#Gene Table";
        String secondLine = "ID" + "\t\t" + "Gene Name" + "\t\t" + "Size" + "\t\t" + "#a/Size" + "\t\t"
                + "#c/Size" + "\t\t" + "#g/Size" + "\t\t" + "#t/Size";
        String thirdLine = gene.getGeneID() + "\t\t"
                         + gene.getGeneName() + "\t\t"
                         + gene.getSize() + "\t\t"
                         + String.format("%.4f", gene.getAComp()) + "\t\t"
                         + String.format("%.4f", gene.getCComp()) + "\t\t"
                         + String.format("%.4f", gene.getGComp()) + "\t\t"
                         + String.format("%.4f", gene.getTComp());

        try {
            Scanner myReader = new Scanner(file);
            String line = myReader.nextLine();
            assertNotNull(line);
            //assert first line of file matches what it should be
            assertEquals(firstLine, line);
            line = myReader.nextLine();
            assertNotNull(line);
            //assert second line of file matches what it should be
            assertEquals(secondLine, line);
            line = myReader.nextLine();
            assertNotNull(line);
            //assert third line of file matches what it should be
            assertEquals(thirdLine, line);
            myReader.close();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }

        boolean success = file.delete();
        if (!success) {
            System.out.println("File cannot be removed.");
        }

    }

}