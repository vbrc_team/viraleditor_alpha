package CodonStats;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;

import java.util.ArrayList;
import java.util.List;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import Shared.interchange.Annotation;
import Shared.interchange.Interval;
import Shared.interchange.NucleotideSequence;
import Shared.interchange.StringToNucleotides;

class CodonStatsGraphComposerTest {
    private static final StringToNucleotides toNucleotides = new StringToNucleotides();

    CodonStatsGraphComposer composer;


    @BeforeEach
    void init() {
        composer = new CodonStatsGraphComposer();
    }

    @Test
    void setGeneSelectionWindow() {
        CodonStatsDialog dialog = new CodonStatsDialog();
        composer.setGeneSelectionWindow(dialog);
        assertNotNull(composer.getGeneSelectionWindow());
        assertEquals(dialog, composer.getGeneSelectionWindow());
    }

    Gene addGene(int ID, String name, String sequence) {
        List<Interval> intervals = new ArrayList<>();
        intervals.add(new Interval(false, 1, sequence.length()));

        Annotation annotation = Annotation.builder()
                                          .withIntervals(intervals)
                                          .withGeneID(ID)
                                          .withGeneAbbreviation(name)
                                          .withNTSequence(new NucleotideSequence(toNucleotides.to(sequence)))
                                          .build();

        return new Gene(annotation);
    }

    @Test
    ArrayList<Gene> setData() {
        ArrayList<Gene> genes = new ArrayList<>();
        genes.add(addGene(4, "HCoV-229E-004", RefSequenceData.sequence4));
        genes.add(addGene(5, "HCoV-229E-005", RefSequenceData.sequence5));
        genes.add(addGene(6, "HCoV-229E-006", RefSequenceData.sequence6));

        composer.setData(genes);
        assertEquals(genes, composer.getData());
        return genes;
    }

    @Test
    void getMean() {
        setData();
        double[] mean = composer.getMean();

        //data from legacy calculations for the sequences used
        assertEquals(0.09209976239994687, mean[0]);
        assertEquals(0.09616471565155114, mean[1]);
        assertEquals(0.08872257362949969, mean[2]);
        assertEquals(0.0617712360123898, mean[3]);
        assertEquals(0.04792684039707351, mean[4]);
        assertEquals(0.06546543189172738, mean[5]);
        assertEquals(0.08985276250075848, mean[6]);
        assertEquals(0.04979854401286566, mean[7]);
        assertEquals(0.05090341053782275, mean[8]);
        assertEquals(0.08960957242023819, mean[9]);
        assertEquals(0.13944323327184302, mean[10]);
        assertEquals(0.12824191727428355, mean[11]);
    }

    @Test
    void getAddedSetMean() {
        ArrayList<Gene> genes = setData();
        double[] mean = composer.getAddedSetMean(genes);

        //data from legacy calculations for the sequences used
        assertEquals(0.09209976239994687, mean[0]);
        assertEquals(0.09616471565155114, mean[1]);
        assertEquals(0.08872257362949969, mean[2]);
        assertEquals(0.0617712360123898, mean[3]);
        assertEquals(0.04792684039707351, mean[4]);
        assertEquals(0.06546543189172738, mean[5]);
        assertEquals(0.08985276250075848, mean[6]);
        assertEquals(0.04979854401286566, mean[7]);
        assertEquals(0.05090341053782275, mean[8]);
        assertEquals(0.08960957242023819, mean[9]);
        assertEquals(0.13944323327184302, mean[10]);
        assertEquals(0.12824191727428355, mean[11]);
    }

    @Test
    void setSummary() {
        double[] summary = new double[12];
        for (int i = 0; i < 12; i++) {
            summary[i] = 1.0;
        }
        composer.setSummary(summary);
        double[] newSummary = composer.getSummary();
        assertEquals(12, newSummary.length);
        for (int i = 0; i < 12; i++) {
            assertEquals(1.0, newSummary[i]);
        }
    }
}