package CodonStats;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import Shared.interchange.Annotation;
import Shared.interchange.Interval;
import Shared.interchange.NucleotideSequence;
import Shared.interchange.StringToNucleotides;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;

class CodonViewComposerTest {
    private static final StringToNucleotides toNucleotides = new StringToNucleotides();

    CodonViewComposer composer;
    String pathToResource = "src/test/resources";
    ArrayList<Gene> genes;

    double[] seq4TableNucleotides = new double[]{0.0871, 0.0746, 0.0871, 0.0597, 0.0721, 0.0622,
                                                0.0920, 0.0473, 0.0597, 0.0945, 0.1393, 0.1244};
    double[] seq5TableNucleotides = new double[]{0.0824, 0.1199, 0.0936, 0.0487, 0.0375, 0.0787,
                                                0.1049, 0.0637, 0.0375, 0.0974, 0.1124, 0.1236};
    double[] seq6TableNucleotides = new double[]{0.1068, 0.0940, 0.0855, 0.0769, 0.0342, 0.0556,
                                                0.0726, 0.0385, 0.0556, 0.0769, 0.1667, 0.1368};
    double[] seqSums = new double[]{0.2763, 0.2885, 0.2662, 0.1853, 0.1438, 0.1964,
                                    0.2696, 0.1494, 0.1527, 0.2688, 0.4183, 0.3847};
    double[] seqMeans = new double[]{0.0921, 0.0962, 0.0887, 0.0618, 0.0479, 0.0655,
                                    0.0899, 0.0498, 0.0509, 0.0896, 0.1394, 0.1282};
    double[] seqVariance = new double[]{0.1683, 0.5147, 0.0187, 0.2025, 0.4423, 0.1414,
                                        0.2631, 0.1637, 0.1400, 0.1227, 0.7373, 0.0545};
    double[] seqLowerbound = new double[]{0.0774, 0.0705, 0.0838, 0.0457, 0.0241, 0.0520,
                                         0.0715, 0.0353, 0.0375, 0.0771, 0.1087, 0.1199};
    double[] seqUpperbound = new double[]{0.1068, 0.1218, 0.0936, 0.0779, 0.0717, 0.0789,
                                         0.1082, 0.0643, 0.0643, 0.1021, 0.1702, 0.1366};

    String firstLine = "#Results Table";
    String secondLine = "Gene Name" + "\t\t" + "a_1" + "\t" + "a_2" + "\t" + "a_3" + "\t" + "c_1" + "\t" + "c_2" + "\t"
            + "c_3" + "\t" + "g_1" + "\t" + "g_2" + "\t" + "g_3" + "\t" + "t_1" + "\t" + "t_2" + "\t" + "t_3";


    @BeforeEach
    void init() {
        composer = new CodonViewComposer();
        genes = setup();
    }

    ArrayList<Gene> setup() {
        ArrayList<Gene> genes = new ArrayList<>();
        genes.add(addGene(4, "HCoV-229E-004", RefSequenceData.sequence4));
        genes.add(addGene(5, "HCoV-229E-005", RefSequenceData.sequence5));
        genes.add(addGene(6, "HCoV-229E-006", RefSequenceData.sequence6));
        return genes;
    }

    Gene addGene(int ID, String name, String sequence) {
        List<Interval> intervals = new ArrayList<>();
        intervals.add(new Interval(false, 1, sequence.length()));

        Annotation annotation = Annotation.builder()
                                          .withIntervals(intervals)
                                          .withGeneID(ID)
                                          .withGeneAbbreviation(name)
                                          .withNTSequence(new NucleotideSequence(toNucleotides.to(sequence)))
                                          .build();

        return new Gene(annotation);
    }

    @Test()
    void setTableDataA1() {
        List<NucleotideCount> nucleotideCounts = composer.setTableData(genes);
        assertNotNull(nucleotideCounts);
        assertEquals(seq4TableNucleotides[0], nucleotideCounts.get(0).getA1());
        assertEquals(seq5TableNucleotides[0], nucleotideCounts.get(1).getA1());
        assertEquals(seq6TableNucleotides[0], nucleotideCounts.get(2).getA1());
    }

    @Test
    void setTableDataA2() {
        List<NucleotideCount> nucleotideCounts = composer.setTableData(genes);
        assertNotNull(nucleotideCounts);
        assertEquals(seq4TableNucleotides[1], nucleotideCounts.get(0).getA2());
        assertEquals(seq5TableNucleotides[1], nucleotideCounts.get(1).getA2());
        assertEquals(seq6TableNucleotides[1], nucleotideCounts.get(2).getA2());
    }

    @Test
    void setTableDataA3() {
        List<NucleotideCount> nucleotideCounts = composer.setTableData(genes);
        assertNotNull(nucleotideCounts);
        assertEquals(seq4TableNucleotides[2], nucleotideCounts.get(0).getA3());
        assertEquals(seq5TableNucleotides[2], nucleotideCounts.get(1).getA3());
        assertEquals(seq6TableNucleotides[2], nucleotideCounts.get(2).getA3());
    }

    @Test
    void setTableDataC1() {
        List<NucleotideCount> nucleotideCounts = composer.setTableData(genes);
        assertNotNull(nucleotideCounts);
        assertEquals(seq4TableNucleotides[3], nucleotideCounts.get(0).getC1());
        assertEquals(seq5TableNucleotides[3], nucleotideCounts.get(1).getC1());
        assertEquals(seq6TableNucleotides[3], nucleotideCounts.get(2).getC1());
    }

    @Test
    void setTableDataC2() {
        List<NucleotideCount> nucleotideCounts = composer.setTableData(genes);
        assertNotNull(nucleotideCounts);
        assertEquals(seq4TableNucleotides[4], nucleotideCounts.get(0).getC2());
        assertEquals(seq5TableNucleotides[4], nucleotideCounts.get(1).getC2());
        assertEquals(seq6TableNucleotides[4], nucleotideCounts.get(2).getC2());
    }

    @Test
    void setTableDataC3() {
        List<NucleotideCount> nucleotideCounts = composer.setTableData(genes);
        assertNotNull(nucleotideCounts);
        assertEquals(seq4TableNucleotides[5], nucleotideCounts.get(0).getC3());
        assertEquals(seq5TableNucleotides[5], nucleotideCounts.get(1).getC3());
        assertEquals(seq6TableNucleotides[5], nucleotideCounts.get(2).getC3());
    }

    @Test
    void setTableDataG1() {
        List<NucleotideCount> nucleotideCounts = composer.setTableData(genes);
        assertNotNull(nucleotideCounts);
        assertEquals(seq4TableNucleotides[6], nucleotideCounts.get(0).getG1());
        assertEquals(seq5TableNucleotides[6], nucleotideCounts.get(1).getG1());
        assertEquals(seq6TableNucleotides[6], nucleotideCounts.get(2).getG1());
    }

    @Test
    void setTableDataG2() {
        List<NucleotideCount> nucleotideCounts = composer.setTableData(genes);
        assertNotNull(nucleotideCounts);
        assertEquals(seq4TableNucleotides[7], nucleotideCounts.get(0).getG2());
        assertEquals(seq5TableNucleotides[7], nucleotideCounts.get(1).getG2());
        assertEquals(seq6TableNucleotides[7], nucleotideCounts.get(2).getG2());
    }

    @Test
    void setTableDataG3() {
        List<NucleotideCount> nucleotideCounts = composer.setTableData(genes);
        assertNotNull(nucleotideCounts);
        assertEquals(seq4TableNucleotides[8], nucleotideCounts.get(0).getG3());
        assertEquals(seq5TableNucleotides[8], nucleotideCounts.get(1).getG3());
        assertEquals(seq6TableNucleotides[8], nucleotideCounts.get(2).getG3());
    }

    @Test
    void setTableDataT1() {
        List<NucleotideCount> nucleotideCounts = composer.setTableData(genes);
        assertNotNull(nucleotideCounts);
        assertEquals(seq4TableNucleotides[9], nucleotideCounts.get(0).getT1());
        assertEquals(seq5TableNucleotides[9], nucleotideCounts.get(1).getT1());
        assertEquals(seq6TableNucleotides[9], nucleotideCounts.get(2).getT1());
    }

    @Test
    void setTableDataT2() {
        List<NucleotideCount> nucleotideCounts = composer.setTableData(genes);
        assertNotNull(nucleotideCounts);
        assertEquals(seq4TableNucleotides[10], nucleotideCounts.get(0).getT2());
        assertEquals(seq5TableNucleotides[10], nucleotideCounts.get(1).getT2());
        assertEquals(seq6TableNucleotides[10], nucleotideCounts.get(2).getT2());
    }

    @Test
    void setTableDataT3() {
        List<NucleotideCount> nucleotideCounts = composer.setTableData(genes);
        assertNotNull(nucleotideCounts);
        assertEquals(seq4TableNucleotides[11], nucleotideCounts.get(0).getT3());
        assertEquals(seq5TableNucleotides[11], nucleotideCounts.get(1).getT3());
        assertEquals(seq6TableNucleotides[11], nucleotideCounts.get(2).getT3());
    }

    @Test
    void setSumColumnDataA1() {
        List<NucleotideCount> nucleotideCounts = composer.setTableData(genes);
        assertNotNull(nucleotideCounts);
        List<NucleotideStats> nucleotideStats = composer.setSumColumnData(nucleotideCounts);
        assertNotNull(nucleotideStats);
        assertEquals(3.0, nucleotideStats.get(0).getA1Sum());
        assertEquals(seqSums[0], nucleotideStats.get(1).getA1Sum());
        assertEquals(seqMeans[0], nucleotideStats.get(2).getA1Sum());
        assertEquals(seqVariance[0], nucleotideStats.get(3).getA1Sum());
        assertEquals(seqLowerbound[0], nucleotideStats.get(4).getA1Sum());
        assertEquals(seqUpperbound[0], nucleotideStats.get(5).getA1Sum());
    }

    @Test
    void setSumColumnDataA2() {
        List<NucleotideCount> nucleotideCounts = composer.setTableData(genes);
        assertNotNull(nucleotideCounts);
        List<NucleotideStats> nucleotideStats = composer.setSumColumnData(nucleotideCounts);
        assertNotNull(nucleotideStats);
        assertEquals(3.0, nucleotideStats.get(0).getA2Sum());
        assertEquals(seqSums[1], nucleotideStats.get(1).getA2Sum());
        assertEquals(seqMeans[1], nucleotideStats.get(2).getA2Sum());
        assertEquals(seqVariance[1], nucleotideStats.get(3).getA2Sum());
        assertEquals(seqLowerbound[1], nucleotideStats.get(4).getA2Sum());
        assertEquals(seqUpperbound[1], nucleotideStats.get(5).getA2Sum());
    }

    @Test
    void setSumColumnDataA3() {
        List<NucleotideCount> nucleotideCounts = composer.setTableData(genes);
        assertNotNull(nucleotideCounts);
        List<NucleotideStats> nucleotideStats = composer.setSumColumnData(nucleotideCounts);
        assertNotNull(nucleotideStats);
        assertEquals(3.0, nucleotideStats.get(0).getA3Sum());
        assertEquals(seqSums[2], nucleotideStats.get(1).getA3Sum());
        assertEquals(seqMeans[2], nucleotideStats.get(2).getA3Sum());
        assertEquals(seqVariance[2], nucleotideStats.get(3).getA3Sum());
        assertEquals(seqLowerbound[2], nucleotideStats.get(4).getA3Sum());
        assertEquals(seqUpperbound[2], nucleotideStats.get(5).getA3Sum());
    }

    @Test
    void setSumColumnDataC1() {
        List<NucleotideCount> nucleotideCounts = composer.setTableData(genes);
        assertNotNull(nucleotideCounts);
        List<NucleotideStats> nucleotideStats = composer.setSumColumnData(nucleotideCounts);
        assertNotNull(nucleotideStats);
        assertEquals(3.0, nucleotideStats.get(0).getC1Sum());
        assertEquals(seqSums[3], nucleotideStats.get(1).getC1Sum());
        assertEquals(seqMeans[3], nucleotideStats.get(2).getC1Sum());
        assertEquals(seqVariance[3], nucleotideStats.get(3).getC1Sum());
        assertEquals(seqLowerbound[3], nucleotideStats.get(4).getC1Sum());
        assertEquals(seqUpperbound[3], nucleotideStats.get(5).getC1Sum());
    }

    @Test
    void setSumColumnDataC2() {
        List<NucleotideCount> nucleotideCounts = composer.setTableData(genes);
        assertNotNull(nucleotideCounts);
        List<NucleotideStats> nucleotideStats = composer.setSumColumnData(nucleotideCounts);
        assertNotNull(nucleotideStats);
        assertEquals(3.0, nucleotideStats.get(0).getC2Sum());
        assertEquals(seqSums[4], nucleotideStats.get(1).getC2Sum());
        assertEquals(seqMeans[4], nucleotideStats.get(2).getC2Sum());
        assertEquals(seqVariance[4], nucleotideStats.get(3).getC2Sum());
        assertEquals(seqLowerbound[4], nucleotideStats.get(4).getC2Sum());
        assertEquals(seqUpperbound[4], nucleotideStats.get(5).getC2Sum());
    }

    @Test
    void setSumColumnDataC3() {
        List<NucleotideCount> nucleotideCounts = composer.setTableData(genes);
        assertNotNull(nucleotideCounts);
        List<NucleotideStats> nucleotideStats = composer.setSumColumnData(nucleotideCounts);
        assertNotNull(nucleotideStats);
        assertEquals(3.0, nucleotideStats.get(0).getC3Sum());
        assertEquals(seqSums[5], nucleotideStats.get(1).getC3Sum());
        assertEquals(seqMeans[5], nucleotideStats.get(2).getC3Sum());
        assertEquals(seqVariance[5], nucleotideStats.get(3).getC3Sum());
        assertEquals(seqLowerbound[5], nucleotideStats.get(4).getC3Sum());
        assertEquals(seqUpperbound[5], nucleotideStats.get(5).getC3Sum());
    }

    @Test
    void setSumColumnDataG1() {
        List<NucleotideCount> nucleotideCounts = composer.setTableData(genes);
        assertNotNull(nucleotideCounts);
        List<NucleotideStats> nucleotideStats = composer.setSumColumnData(nucleotideCounts);
        assertNotNull(nucleotideStats);
        assertEquals(3.0, nucleotideStats.get(0).getG1Sum());
        assertEquals(seqSums[6], nucleotideStats.get(1).getG1Sum());
        assertEquals(seqMeans[6], nucleotideStats.get(2).getG1Sum());
        assertEquals(seqVariance[6], nucleotideStats.get(3).getG1Sum());
        assertEquals(seqLowerbound[6], nucleotideStats.get(4).getG1Sum());
        assertEquals(seqUpperbound[6], nucleotideStats.get(5).getG1Sum());
    }

    @Test
    void setSumColumnDataG2() {
        List<NucleotideCount> nucleotideCounts = composer.setTableData(genes);
        assertNotNull(nucleotideCounts);
        List<NucleotideStats> nucleotideStats = composer.setSumColumnData(nucleotideCounts);
        assertNotNull(nucleotideStats);
        assertEquals(3.0, nucleotideStats.get(0).getG2Sum());
        assertEquals(seqSums[7], nucleotideStats.get(1).getG2Sum());
        assertEquals(seqMeans[7], nucleotideStats.get(2).getG2Sum());
        assertEquals(seqVariance[7], nucleotideStats.get(3).getG2Sum());
        assertEquals(seqLowerbound[7], nucleotideStats.get(4).getG2Sum());
        assertEquals(seqUpperbound[7], nucleotideStats.get(5).getG2Sum());
    }

    @Test
    void setSumColumnDataG3() {
        List<NucleotideCount> nucleotideCounts = composer.setTableData(genes);
        assertNotNull(nucleotideCounts);
        List<NucleotideStats> nucleotideStats = composer.setSumColumnData(nucleotideCounts);
        assertNotNull(nucleotideStats);
        assertEquals(3.0, nucleotideStats.get(0).getG3Sum());
        assertEquals(seqSums[8], nucleotideStats.get(1).getG3Sum());
        assertEquals(seqMeans[8], nucleotideStats.get(2).getG3Sum());
        assertEquals(seqVariance[8], nucleotideStats.get(3).getG3Sum());
        assertEquals(seqLowerbound[8], nucleotideStats.get(4).getG3Sum());
        assertEquals(seqUpperbound[8], nucleotideStats.get(5).getG3Sum());
    }

    @Test
    void setSumColumnDataT1() {
        List<NucleotideCount> nucleotideCounts = composer.setTableData(genes);
        assertNotNull(nucleotideCounts);
        List<NucleotideStats> nucleotideStats = composer.setSumColumnData(nucleotideCounts);
        assertNotNull(nucleotideStats);
        assertEquals(3.0, nucleotideStats.get(0).getT1Sum());
        assertEquals(seqSums[9], nucleotideStats.get(1).getT1Sum());
        assertEquals(seqMeans[9], nucleotideStats.get(2).getT1Sum());
        assertEquals(seqVariance[9], nucleotideStats.get(3).getT1Sum());
        assertEquals(seqLowerbound[9], nucleotideStats.get(4).getT1Sum());
        assertEquals(seqUpperbound[9], nucleotideStats.get(5).getT1Sum());
    }

    @Test
    void setSumColumnDataT2() {
        List<NucleotideCount> nucleotideCounts = composer.setTableData(genes);
        assertNotNull(nucleotideCounts);
        List<NucleotideStats> nucleotideStats = composer.setSumColumnData(nucleotideCounts);
        assertNotNull(nucleotideStats);
        assertEquals(3.0, nucleotideStats.get(0).getT2Sum());
        assertEquals(seqSums[10], nucleotideStats.get(1).getT2Sum());
        assertEquals(seqMeans[10], nucleotideStats.get(2).getT2Sum());
        assertEquals(seqVariance[10], nucleotideStats.get(3).getT2Sum());
        assertEquals(seqLowerbound[10], nucleotideStats.get(4).getT2Sum());
        assertEquals(seqUpperbound[10], nucleotideStats.get(5).getT2Sum());
    }

    @Test
    void setSumColumnDataT3() {
        List<NucleotideCount> nucleotideCounts = composer.setTableData(genes);
        assertNotNull(nucleotideCounts);
        List<NucleotideStats> nucleotideStats = composer.setSumColumnData(nucleotideCounts);
        assertNotNull(nucleotideStats);
        assertEquals(3.0, nucleotideStats.get(0).getT3Sum());
        assertEquals(seqSums[11], nucleotideStats.get(1).getT3Sum());
        assertEquals(seqMeans[11], nucleotideStats.get(2).getT3Sum());
        assertEquals(seqVariance[11], nucleotideStats.get(3).getT3Sum());
        assertEquals(seqLowerbound[11], nucleotideStats.get(4).getT3Sum());
        assertEquals(seqUpperbound[11], nucleotideStats.get(5).getT3Sum());
    }

    @Test
    void setRowData() {
        List<NucleotideCount> nucleotideCounts = composer.setTableData(genes);
        composer.setSumColumnData(nucleotideCounts);
        double[] sum = new double[]{1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 1.0};
        NucleotideStats statistic = composer.setRowData(0, "Test", sum);
        assertEquals("Test", statistic.getDescription());
        assertEquals(1.0, statistic.getA1Sum());
        assertEquals(1.0, statistic.getA2Sum());
        assertEquals(1.0, statistic.getA3Sum());
        assertEquals(1.0, statistic.getC1Sum());
        assertEquals(1.0, statistic.getC2Sum());
        assertEquals(1.0, statistic.getC3Sum());
        assertEquals(1.0, statistic.getG1Sum());
        assertEquals(1.0, statistic.getG2Sum());
        assertEquals(1.0, statistic.getG3Sum());
        assertEquals(1.0, statistic.getT1Sum());
        assertEquals(1.0, statistic.getT2Sum());
        assertEquals(1.0, statistic.getT3Sum());
    }

    @Test
    void saveData() {
        List<NucleotideCount> nucleotideCounts = composer.setTableData(genes);
        assertNotNull(nucleotideCounts);
        List<NucleotideStats> nucleotideStats = composer.setSumColumnData(nucleotideCounts);
        assertNotNull(nucleotideStats);

        File file = new File(pathToResource+"/CodonViewOutput.txt");
        ObservableList<NucleotideCount> counts = FXCollections.observableArrayList(nucleotideCounts);
        ObservableList<NucleotideStats> stats = FXCollections.observableArrayList(nucleotideStats);
        try {
            composer.saveData(file, counts, stats);
        } catch (IOException e) {
            e.printStackTrace();
        }

        try (Scanner scanner = new Scanner(file)) {
            String line = scanner.nextLine();
            assertEquals(firstLine, line);
            line = scanner.nextLine();
            assertEquals(secondLine, line);
            //empty new line follows so parse it.
            line = scanner.nextLine();
            assertEquals("", line);

            for (NucleotideCount count : nucleotideCounts) {
                String thirdLine = count.getGeneName() + "\t\t" + count.getA1() + "\t" + count.getA2() + "\t" + count.getA3() + "\t"
                        + count.getC1() + "\t" + count.getC2() + "\t" + count.getC3() + "\t" + count.getG1() + "\t"
                        + count.getG2() + "\t" + count.getG3() + "\t" + count.getT1() + "\t" + count.getT2() + "\t"
                        + count.getT3();
                line = scanner.nextLine();
                assertEquals(thirdLine, line);
            }
            for (NucleotideStats stat : nucleotideStats) {
                String fourthLine = stat.getDescription() + "\t\t\t" + stat.getA1Sum() + "\t" + stat.getA2Sum() + "\t" + stat.getA3Sum() + "\t"
                        + stat.getC1Sum() + "\t" + stat.getC2Sum() + "\t" + stat.getC3Sum() + "\t" + stat.getG1Sum() + "\t"
                        + stat.getG2Sum() + "\t" + stat.getG3Sum() + "\t" + stat.getT1Sum() + "\t" + stat.getT2Sum() + "\t"
                        + stat.getT3Sum();
                if (stat.getDescription().startsWith("Var") || stat.getDescription().startsWith("95")) {
                    fourthLine = stat.getDescription() + "\t\t" + stat.getA1Sum() + "\t" + stat.getA2Sum() + "\t" + stat.getA3Sum() + "\t"
                            + stat.getC1Sum() + "\t" + stat.getC2Sum() + "\t" + stat.getC3Sum() + "\t" + stat.getG1Sum() + "\t"
                            + stat.getG2Sum() + "\t" + stat.getG3Sum() + "\t" + stat.getT1Sum() + "\t" + stat.getT2Sum() + "\t"
                            + stat.getT3Sum();
                }
                line = scanner.nextLine();
                assertEquals(fourthLine, line);
            }
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
        boolean success = file.delete();
        if (!success) {
            System.out.println("File cannot be removed.");
        }
    }
}