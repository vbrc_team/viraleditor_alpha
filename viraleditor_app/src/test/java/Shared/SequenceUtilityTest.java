package Shared;

import Shared.Orf;
import java.util.Arrays;
import java.util.ArrayList;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.BeforeEach;
import static org.junit.jupiter.api.Assertions.*;

public class SequenceUtilityTest {

	// TODO: Maybe put a whole genome in here and use it for testing?
	
	@Test
	void make_simple_complement() {
		
		String seq = "ACTG-MRKY-VHBD-WS-XN";
		String seqc = "TGAC-KYMR-BDVH-WS-XN";
		String lowerSeq = "actg-mrky-vhbd-ws-xn";
		
		assertEquals(seqc, SequenceUtility.make_simple_complement(seq));
		assertEquals(seq, SequenceUtility.make_simple_complement(seqc));
		assertEquals(seqc, SequenceUtility.make_simple_complement(lowerSeq));		
	}
	
	@Test 
	void make_complement() {
		
		String seq = "ACTG-MRKY-VHBD-WS-XN";
		String seqc = "NX-SW-HVDB-RMYK-CAGT";
		String lowerSeq = "actg-mrky-vhbd-ws-xn";
		
		assertEquals(seqc, SequenceUtility.make_complement(seq));
		assertEquals(seqc, SequenceUtility.make_complement(lowerSeq));
	}
	
	@Test
	/* Takes a genome (String vir_dna), strand (string, either "-"/"bottom" for -,
	 * or anything else for +), and an arraylist of orfs. Then, returns a string with
	 * bases between start and stop positions of each orf.
	 * 
	 * Some notes on behavior:
	 * - if stop > start, will loop around
	 * 	= if stop > genome length, stop = genome length
	 *  
	 * - if bottom/-, returns reverse complement
	 * 	= Actually, it never will because of an error in the method
	 * 
	 * - Indexing starts at 1 in Orf start/stop as well as position
	 * 	= It will handle start/stop > length, but will break on start/stop < 1
	 * 	= Orf positions MUST start at 1 and increment by 1 or it breaks!
	 * 
	 * whoever wrote this method was off their gourd
	 */
	void setDNAseq() {

		String pizza = "fooxbarxrio'sxpizzeria ";
		String sequence = "aaccttgg";
		
		// "FOOBARRIO'S PIZZERIA"
		ArrayList<Orf> testOrfs1 = new ArrayList<Orf>();
		testOrfs1.add(new Orf(0, 1, 3, 1));
		testOrfs1.add(new Orf(0, 5, 7, 2));
		testOrfs1.add(new Orf(0, 9, 13, 3));
		testOrfs1.add(new Orf(0, 23, 23, 4));
		testOrfs1.add(new Orf(0, 15, 22, 5));
		
		// "PIZZERIA FOOBARRIO"
		ArrayList<Orf> testOrfs2 = new ArrayList<Orf>();
		testOrfs2.add(new Orf(0, 15, 3, 1));
		testOrfs2.add(new Orf(0, 5, 7, 2));
		testOrfs2.add(new Orf(0, 9, 11, 3));
		
		// FOOBAR PIZZA
		ArrayList<Orf> testOrfs3 = new ArrayList<Orf>();
		testOrfs3.add(new Orf(0, 1, 3, 1)); 
		testOrfs3.add(new Orf(0, 5, 7, 2));
		testOrfs3.add(new Orf(0, 23, 23, 3));
		testOrfs3.add(new Orf(0, 15, 18, 4));
		testOrfs3.add(new Orf(0, 22, 25, 5)); // 25 out of range
		
		// CCAAGGTT
		ArrayList<Orf> testOrfs4 = new ArrayList<Orf>();
		testOrfs4.add(new Orf(0, 1, 8, 1));
		
		
		// Test cases:
		
		// Top	
		//	All orfs have start < stop
		assertEquals("FOOBARRIO'S PIZZERIA", SequenceUtility.setDNASeq(pizza, "+", testOrfs1));
		
		//	Some orfs have start > stop
		assertEquals("PIZZERIA FOOBARRIO", SequenceUtility.setDNASeq(pizza, "+", testOrfs2));
		
		// 	Orfs start/stop out of range
		assertEquals("FOOBAR PIZZA ", SequenceUtility.setDNASeq(pizza, "+", testOrfs3));
		
		// Bottom
		// 	Test complement/reversal
		assertEquals("CCAAGGTT", SequenceUtility.setDNASeq(sequence, "-", testOrfs4));
		
		
	}
	
	@Test
	void setProteinSeq() {
		// TODO: Make this test more comprehensive
		// Test with a gene sequence (CoV2-WuhanHu1_19_12_Chn-011)
		String geneseq = 
				"ATGTCTGATAATGGACCCCAAAATCAGCGAAATGCACCCCGCATTACGTTTGGTGGACCC"
				+ "TCAGATTCAACTGGCAGTAACCAGAATGGAGAACGCAGTGGGGCGCGATCAAAACAAC"
				+ "GTCGGCCCCAAGGTTTACCCAATAATACTGCGTCTTGGTTCACCGCTCTCACTCAACA"
				+ "TGGCAAGGAAGACCTTAAATTCCCTCGAGGACAAGGCGTTCCAATTAACACCAATAGC"
				+ "AGTCCAGATGACCAAATTGGCTACTACCGAAGAGCTACCAGACGAATTCGTGGTGGTG"
				+ "ACGGTAAAATGAAAGATCTCAGTCCAAGATGGTATTTCTACTACCTAGGAACTGGGCC"
				+ "AGAAGCTGGACTTCCCTATGGTGCTAACAAAGACGGCATCATATGGGTTGCAACTGAG"
				+ "GGAGCCTTGAATACACCAAAAGATCACATTGGCACCCGCAATCCTGCTAACAATGCTG"
				+ "CAATCGTGCTACAACTTCCTCAAGGAACAACATTGCCAAAAGGCTTCTACGCAGAAGG"
				+ "GAGCAGAGGCGGCAGTCAAGCCTCTTCTCGTTCCTCATCACGTAGTCGCAACAGTTCA"
				+ "AGAAATTCAACTCCAGGCAGCAGTAGGGGAACTTCTCCTGCTAGAATGGCTGGCAATG"
				+ "GCGGTGATGCTGCTCTTGCTTTGCTGCTGCTTGACAGATTGAACCAGCTTGAGAGCAA"
				+ "AATGTCTGGTAAAGGCCAACAACAACAAGGCCAAACTGTCACTAAGAAATCTGCTGCT"
				+ "GAGGCTTCTAAGAAGCCTCGGCAAAAACGTACTGCCACTAAAGCATACAATGTAACAC"
				+ "AAGCTTTCGGCAGACGTGGTCCAGAACAAACCCAAGGAAATTTTGGGGACCAGGAACT"
				+ "AATCAGACAAGGAACTGATTACAAACATTGGCCGCAAATTGCACAATTTGCCCCCAGC"
				+ "GCTTCAGCGTTCTTCGGAATGTCGCGCATTGGCATGGAAGTCACACCTTCGGGAACGT"
				+ "GGTTGACCTACACAGGTGCCATCAAATTGGATGACAAAGATCCAAATTTCAAAGATCA"
				+ "AGTCATTTTGCTGAATAAGCATATTGACGCATACAAAACATTCCCACCAACAGAGCCT"
				+ "AAAAAGGACAAAAAGAAGAAGGCTGATGAAACTCAAGCCTTACCGCAGAGACAGAAGA"
				+ "AACAGCAAACTGTGACTCTTCTTCCTGCTGCAGATTTGGATGATTTCTCCAAACAATT"
				+ "GCAACAATCCATGAGCAGTGCTGACTCAACTCAGGCCTAA";
		String proteinseq = 
				"MSDNGPQNQRNAPRITFGGPSDSTGSNQNGERSGARSKQRRPQGLPNNTASWFTALTQHG"
				+ "KEDLKFPRGQGVPINTNSSPDDQIGYYRRATRRIRGGDGKMKDLSPRWYFYYLGTGPE"
				+ "AGLPYGANKDGIIWVATEGALNTPKDHIGTRNPANNAAIVLQLPQGTTLPKGFYAEGS"
				+ "RGGSQASSRSSSRSRNSSRNSTPGSSRGTSPARMAGNGGDAALALLLLDRLNQLESKM"
				+ "SGKGQQQQGQTVTKKSAAEASKKPRQKRTATKAYNVTQAFGRRGPEQTQGNFGDQELI"
				+ "RQGTDYKHWPQIAQFAPSASAFFGMSRIGMEVTPSGTWLTYTGAIKLDDKDPNFKDQV"
				+ "ILLNKHIDAYKTFPPTEPKKDKKKKADETQALPQRQKKQQTVTLLPAADLDDFSKQLQ"
				+ "QSMSSADSTQA*";
		
		assertEquals(proteinseq, SequenceUtility.setProteinSeq(geneseq));
		
		// TODO: Test with sequences containing ambiguity
	}
	
	// Observed behavior: returns ulen + 10 characters, starting ulen characters 
	// before gene start.
	@Test
	void makeUpstreamSeq() {
		String geneseq = 
				"ATGTCTGATAATGGACCCCAAAATCAGCGAAATGCACCCCGCATTACGTTTGGTGGACCC"
				+ "TCAGATTCAACTGGCAGTAACCAGAATGGAGAACGCAGTGGGGCGCGATCAAAACAAC"
				+ "GTCGGCCCCAAGGTTTACCCAATAATACTGCGTCTTGGTTCACCGCTCTCACTCAACA"
				+ "TGGCAAGGAAGACCTTAAATTCCCTCGAGGACAAGGCGTTCCAATTAACACCAATAGC"
				+ "AGTCCAGATGACCAAATTGGCTACTACCGAAGAGCTACCAGACGAATTCGTGGTGGTG"
				+ "ACGGTAAAATGAAAGATCTCAGTCCAAGATGGTATTTCTACTACCTAGGAACTGGGCC"
				+ "AGAAGCTGGACTTCCCTATGGTGCTAACAAAGACGGCATCATATGGGTTGCAACTGAG"
				+ "GGAGCCTTGAATACACCAAAAGATCACATTGGCACCCGCAATCCTGCTAACAATGCTG"
				+ "CAATCGTGCTACAACTTCCTCAAGGAACAACATTGCCAAAAGGCTTCTACGCAGAAGG"
				+ "GAGCAGAGGCGGCAGTCAAGCCTCTTCTCGTTCCTCATCACGTAGTCGCAACAGTTCA"
				+ "AGAAATTCAACTCCAGGCAGCAGTAGGGGAACTTCTCCTGCTAGAATGGCTGGCAATG"
				+ "GCGGTGATGCTGCTCTTGCTTTGCTGCTGCTTGACAGATTGAACCAGCTTGAGAGCAA"
				+ "AATGTCTGGTAAAGGCCAACAACAACAAGGCCAAACTGTCACTAAGAAATCTGCTGCT"
				+ "GAGGCTTCTAAGAAGCCTCGGCAAAAACGTACTGCCACTAAAGCATACAATGTAACAC"
				+ "AAGCTTTCGGCAGACGTGGTCCAGAACAAACCCAAGGAAATTTTGGGGACCAGGAACT"
				+ "AATCAGACAAGGAACTGATTACAAACATTGGCCGCAAATTGCACAATTTGCCCCCAGC"
				+ "GCTTCAGCGTTCTTCGGAATGTCGCGCATTGGCATGGAAGTCACACCTTCGGGAACGT"
				+ "GGTTGACCTACACAGGTGCCATCAAATTGGATGACAAAGATCCAAATTTCAAAGATCA"
				+ "AGTCATTTTGCTGAATAAGCATATTGACGCATACAAAACATTCCCACCAACAGAGCCT"
				+ "AAAAAGGACAAAAAGAAGAAGGCTGATGAAACTCAAGCCTTACCGCAGAGACAGAAGA"
				+ "AACAGCAAACTGTGACTCTTCTTCCTGCTGCAGATTTGGATGATTTCTCCAAACAATT"
				+ "GCAACAATCCATGAGCAGTGCTGACTCAACTCAGGCCTAA";
		
		String sequence = 
				  "11111111111111111111111111111111111111111111111111"
				+ "!++++++++!2222222222222222222222222222222222222222"
				+ "!========!3333333333333333333333333333333333333333"
				+ "!--------!4444444444444444444444444444444444444444"
				+ "55555555555555555555555555555555555555555555555555";
		
		String circular = 
				  "11111111111111111111111!========!11111111111111111";
		
		ArrayList<Orf> geneOrf = new ArrayList<Orf>();
		geneOrf.add(new Orf(0, 101, 110, 1));
		
		ArrayList<Orf> circularOrf = new ArrayList<Orf>();
		circularOrf.add(new Orf(0, 24, 26, 1));
		
		// Non-circular
		
		// Strand +, contiguous gene
		assertEquals("!++++++++!2222222222222222222222222222222222222222!========!",
				SequenceUtility.makeUpstreamSeq(sequence, 50, "+", geneOrf, false));
		
		// Strand -, contiguous gene
		assertEquals("!--------!3333333333333333333333333333333333333333!========!",
				SequenceUtility.makeUpstreamSeq(sequence, 50, "-", geneOrf, false));
		
		// TODO: test noncontiguous genes
		
		// Circular
		
		// Strand +, contiguous gene
		assertEquals("!========!1111111111111111111111111111111111111111!========!",
				SequenceUtility.makeUpstreamSeq(circular, 50, "+", circularOrf, true));
		
		// TODO: test noncontiguous genes
	}
	

	@Test 
	// if >= 90% of the sequence is ACGT, returns true
	// probably pretty robust but seems like it could provide false
	// positives in extremely rare edge cases
	void isDNA() {
		
		String allNuc = "ACTGactgAAAACCCCTTTTGGGGaaaaccccttttgggg";
		String actualDNA = 
				"gatgggacccctgtgcacggcgtcgttcacaatcaggctgcattcaagcaagccccgttc" + 
				"tagtacccgctcaacctcgtatattaactcaccgttatgtttgtcggctcgggagttaaa" + 
				"gcaacgggtagtgtccggtactaagccatttcgccgggaaggcgacgccctaaaggtgcc" + 
				"tccggaggaccaccggtgcgatcttgagggcctggtgccatgaatttctgacctcgactg" + 
				"atgccacaggcatttggctcgagatactgagaccatatgccaagtgatgatcagcttgtt" + 
				"cgaactatttttactccctcgtcctttcacgaggcggtgttgccgctcacgttcatattg" + 
				"atacagggaagatgaaagctgcctttaggctgagacgacagtgatcagatagattgcttt" + 
				"gcgggataattcctggagaccgttaacatacttacgtgcagagtgatatggggtttggag" + 
				"gttgctccattactgggttacgatcttctgagagcttactgtacgctagtattggacgta" + 
				"actattgttactgacgggagttttgttagcaactcgtaacggcttgaacagcggtatggc" + 
				"gagcacgctgcttggatagtgagtcctttagcccgctaccaacaagtctttcgttcccat" + 
				"cccagtgtagtggtcttcgtaggagtgcatgcggcacttttctatttcctctattcaaga" + 
				"tagtcgctcatggtgcagggacgaattccttttagtccgttgactcatgaactgcctact" + 
				"tctcgttaccatctcctgaaacctgcgcccccaaggtgacgtcggggtcatgagttatag" + 
				"aggatggtagctgacgcagtatttgaagaaggacaattccgatctcctcaccgatgctgt" + 
				"tggacattaacttctatgatgataacccgttccaaatatgcggagtaaaaagtttatatg" + 
				"cgacttgtgctcgaacttggcagagatctcttaaacggga";
		String proteins = 
				"VKVAIDRDYMKWLGCRWYLPLLSSENNHYQAPWFSEQYFEQGKRLESMKYLKKPNWTFAE" + 
				"HFPWYYFRQISRFAMCINDPVQETYSYVHMDGQVMYLAPKAEEICWFEMLGREDRVCPRW" + 
				"FCCCFVEMSFMFQQPMPLYFKTEESCKSCIAPGYVWALVEYNLDWQAIWALYAHLNRSGV" + 
				"VDVCNRHPMNFYILEKFPLMVDINMAKPEITRMIWLHQNSHATTHICVGYCQARCWYHAL" + 
				"HTMQPRESFPRPHLPKWPSCILSVILSQNWQEKDRDQCAHLGGTKTVFNQNDRDYYTKYD" + 
				"KSKRAFYVEDSMGDINVLMSMPTEMVAIPFNCIFLVRSSDNRRICDVTEEDVMCDGTRKD" + 
				"PMFRGVDKEIDHLSRTFGEAQLIMRYVHAPPESKAEWKCPWLFLEIENNSHSFMIKAEFT" + 
				"GSPWRLCHQRVDVDWFLQYHYDMQYWLQAYLVRYSLASANCHMETQDFNCIKMDCKHCQS" + 
				"VIHADMDGSSTICAMLMWDCCNQTDVWCPWPCWKYAFQCVSEMMLKYHARQCHCPRSNIS" + 
				"LYAYKVSLRWMDIIYFNTKMCFGHIETWLHYSSGMSMKIMTYWTWPRHFYAHECVQSWVW" + 
				"CSKHGTIHQWLNNQGCSYQAWWHTFVDYVAQFHFFLTVVSEGHTEDDYTLWHDDDDTEEL" + 
				"IDDADWKYTQRIPCNKPHFYDFCYKQIQFMQDEPRMEKVSTAVIFSHTCIKDHPMCMPSM" + 
				"VTQKSMGLLRVHKVYTFWIDNGCVDLEVRIDHRGGMGETDWGDEAMSIMFSPIQYVNLCE" + 
				"PADLGMCIKINYERLGCLCSRKTGTEHQQWNFIEKMVEMYHRVQRNDISSVTCLPHSNFN" + 
				"KVWCQHIRCYGLWVDWVAWQWWAVHKCTNQVLGLRCNDTSEWEMYTPQGTMWLKVNLFYD" + 
				"NVQQEKPCYHMENMCIFACEPCKDAYRKELWTHQPFEQAVQCCREENWLNLGKERCEWWY" + 
				"VDCYSTCSDCHWLRHPFGHIPMTELYDHDHMNKYPSQIKF";
		
		assertTrue(SequenceUtility.isDNA(allNuc));
		assertTrue(SequenceUtility.isDNA(actualDNA));
		assertFalse(SequenceUtility.isDNA(proteins));

		
	}
	
}
