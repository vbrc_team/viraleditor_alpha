package Shared;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.stream.Stream;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtensionContext;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.ArgumentsProvider;
import org.junit.jupiter.params.provider.ArgumentsSource;

class IntervalTreeTest {

    @Test
    void insert_with_low_equals_high_throws_exception() {
        IntervalTree<String> tree = new IntervalTree<>();
        assertThrows(IllegalArgumentException.class, () -> tree.insert("", 1, 1));
    }

    @Test
    void insert_with_low_greater_than_high_throws_exception() {
        IntervalTree<String> tree = new IntervalTree<>();
        assertThrows(IllegalArgumentException.class, () -> tree.insert("", 2, 1));
    }

    @Test
    void find_returns_single_value() {
        IntervalTree<String> tree = new IntervalTree<>();
        tree.insert("", 0, 4);

        assertEquals(1, tree.find(1).size());
    }

    @ParameterizedTest
    @ArgumentsSource(IntervalTreeProvider.class)
    void find_returns_elements_intersecting_point(IntervalTree<String> tree, int point, List<String> matches) {
        List<String> found = tree.find(point);
        found.sort((s1, s2) -> s1.compareTo(s2));
        assertEquals(matches.size(), found.size());
        for (int i = 0; i < matches.size(); i++) {
            assertEquals(matches.get(i), found.get(i));
        }
    }

    public static class IntervalTreeProvider implements ArgumentsProvider {
        @Override
        public Stream<? extends Arguments> provideArguments(ExtensionContext context) throws Exception {
            int[] lows = { 57, 35, 23, 20, 14, 29, 7, 35, 62, 55, 12, 27, 12, 44, 13 };
            int[] highs = { 70, 72, 43, 55, 25, 89, 9, 62, 75, 71, 100, 75, 21, 57, 55 };
            IntervalTree<String> tree = new IntervalTree<>();
            for (int i = 0; i < lows.length; i++) {
                tree.insert(lows[i] + "-" + highs[i], lows[i], highs[i]);
            }

            List<Arguments> args = new ArrayList<>();
            args.add(Arguments.of(tree, 0, Collections.emptyList()));
            args.add(Arguments.of(tree, 100, Arrays.asList("12-100")));
            args.add(Arguments.of(tree, 88, Arrays.asList("12-100", "29-89")));
            args.add(Arguments.of(tree, 70,
                    Arrays.asList("12-100", "27-75", "29-89", "35-72", "55-71", "57-70", "62-75")));
            args.add(Arguments.of(tree, 23, Arrays.asList("12-100", "13-55", "14-25", "20-55", "23-43")));

            return args.stream();
        }
    }
}
