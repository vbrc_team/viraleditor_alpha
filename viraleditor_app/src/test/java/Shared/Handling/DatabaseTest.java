package Shared.Handling;

import static org.junit.jupiter.api.Assertions.assertEquals;

import java.net.URISyntaxException;
import java.util.ArrayList;
import java.util.List;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

/**
 * Unit tests for database access class.
 * Simply tests that the correct queries are being sent, integration testing
 * will be required to test database access functionality.
 * @author Mattias Park
 *
 */
class DatabaseTest {

	Database db;
	
	@BeforeEach
	void init() {
		db = new Database();
	}
	
	@Test
	void buildFamilyQuery() throws URISyntaxException {
		
		// Default- all families
        assertEquals("http://4virology.net:80/proxy/families", db.buildFamilyQuery().toString());
		
		List<Integer> family_ids = new ArrayList<> ();
		family_ids.add(1);
		family_ids.add(2);
		family_ids.add(3);
		
		// W/ params
        assertEquals("http://4virology.net:80/proxy/families?ids=1,2,3",
                db.families(family_ids).buildFamilyQuery().toString());
		
	}
	
	@Test
	void buildGenomeQuery() throws URISyntaxException {
		
		// Default- all genomes
        assertEquals("http://4virology.net:80/proxy/genomes",
				db.buildGenomeQuery().toString());
		
		List<Integer> genome_ids = new ArrayList<> ();
		genome_ids.add(123);
		genome_ids.add(456);
		genome_ids.add(789);
		
		// With genome IDs, no sequences
        assertEquals("http://4virology.net:80/proxy/genomes?ids=123,456,789",
				db.genomes(genome_ids).buildGenomeQuery().toString());
		
		// Genome IDs, with sequences
        assertEquals("http://4virology.net:80/proxy/genomes?ids=123,456,789&sequence=true",
				db.genomes(genome_ids).withSequences().buildGenomeQuery().toString());
		
		
		List<Integer> family_ids = new ArrayList<> ();
		family_ids.add(1);
		family_ids.add(2);
		family_ids.add(3);
		
		// W/ family IDs, no sequences
        assertEquals("http://4virology.net:80/proxy/genomes?family-ids=1,2,3",
				db.families(family_ids).buildGenomeQuery().toString());
		
		// Family IDs, with sequences
        assertEquals("http://4virology.net:80/proxy/genomes?family-ids=1,2,3&sequence=true",
				db.families(family_ids).withSequences().buildGenomeQuery().toString());
		
		// With both- should be the same as with genome IDs as they take precedence
        assertEquals("http://4virology.net:80/proxy/genomes?ids=123,456,789",
				db.families(family_ids).genomes(genome_ids).buildGenomeQuery().toString());
		
		// Both, with sequences
	}
	
	@Test
	void buildGeneQuery() throws URISyntaxException {
		
		// Default- nothing lol
        assertEquals("http://4virology.net:80/proxy/genes", db.buildGeneQuery().toString());
		
		List<Integer> gene_ids = new ArrayList<> ();
		gene_ids.add(1234);
		gene_ids.add(5678);
		gene_ids.add(9012);
		
		// W/ gene IDs, no sequences
        assertEquals("http://4virology.net:80/proxy/genes?ids=1234,5678,9012",
				db.genes(gene_ids).buildGeneQuery().toString());
		
		// W/ gene IDs, with sequences
        assertEquals("http://4virology.net:80/proxy/genes?ids=1234,5678,9012&sequence=true",
				db.genes(gene_ids).withSequences().buildGeneQuery().toString());
		
		List<Integer> genome_ids = new ArrayList<> ();
		genome_ids.add(123);
		genome_ids.add(456);
		genome_ids.add(789);
		
		// W/ genome IDs, no sequences
        assertEquals("http://4virology.net:80/proxy/genes?genome-ids=123,456,789",
				db.genomes(genome_ids).buildGeneQuery().toString());
		
		// W/ genome IDs, with sequences
        assertEquals("http://4virology.net:80/proxy/genes?genome-ids=123,456,789&sequence=true",
				db.genomes(genome_ids).withSequences().buildGeneQuery().toString());
	}
	
	@Test 
	void buildTaxnodeQuery() throws URISyntaxException {
        assertEquals("http://4virology.net:80/proxy/taxnodes", db.buildTaxnodeQuery().toString());
	}
	
	@Test
	void buildOrthologGroupQuery() throws URISyntaxException {
		// No params- all ortholog groups
        assertEquals("http://4virology.net:80/proxy/ortholog-groups",
				db.buildOrthologGroupQuery().toString());
		
		// W/ ids
		List<Integer> ortholog_ids = new ArrayList<> ();
		ortholog_ids.add(1);
		ortholog_ids.add(2);
		ortholog_ids.add(3);
		
        assertEquals("http://4virology.net:80/proxy/ortholog-groups?ids=1,2,3",
				db.orthologGroups(ortholog_ids).buildOrthologGroupQuery().toString());
	}

}
