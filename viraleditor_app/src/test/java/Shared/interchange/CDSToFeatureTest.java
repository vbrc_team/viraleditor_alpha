package Shared.interchange;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.biojava.nbio.core.sequence.compound.NucleotideCompound;
import org.biojava.nbio.core.sequence.features.DBReferenceInfo;
import org.biojava.nbio.core.sequence.features.FeatureInterface;
import org.biojava.nbio.core.sequence.features.Qualifier;
import org.biojava.nbio.core.sequence.features.TextFeature;
import org.biojava.nbio.core.sequence.location.SimpleLocation;
import org.biojava.nbio.core.sequence.location.template.AbstractLocation;
import org.biojava.nbio.core.sequence.template.AbstractSequence;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

class CDSToFeatureTest {
    private CDSToFeature convert;
    private List<Interval> defaultIntervals;
    private FeatureInterface<AbstractSequence<NucleotideCompound>, NucleotideCompound> feature;
    private AbstractLocation location;

    private static final String CDS_NAME = "CDS";

    @BeforeEach
    void before() {
        convert = new CDSToFeature();
        defaultIntervals = new ArrayList<>();
        defaultIntervals.add(new Interval(false, 1, 3));
        location = new SimpleLocation(1, 3);
        feature = null;
    }

    @Test
    void fromCDSWithDbXrefGI() {
        Map<DbXRef, String> dbXrefs = new HashMap<>();
        dbXrefs.put(DbXRef.GI, "12345");
        CDS dbxref = CDS.builder().withIntervals(defaultIntervals).withDbXref(dbXrefs).build();
        feature = convert.to(dbxref);

        List<Qualifier> dbxrefs = feature.getQualifiers().get("db_xref");
        assertEquals(1, dbxrefs.size());

        DBReferenceInfo dbInfo = (DBReferenceInfo) dbxrefs.get(0);
        assertEquals("GI", dbInfo.getDatabase());
        assertEquals("12345", dbInfo.getId());
    }

    @Test
    void withDBXrefGI() {
        feature = new TextFeature<>(CDS_NAME, "", "", "");
        feature.addQualifier("db_xref", new DBReferenceInfo("GI", "GI ID"));
        feature.setLocation(location);

        CDS cds = convert.from(feature);
        Map<DbXRef, String> dbxrefs = cds.getDbXref();

        assertTrue(dbxrefs.containsKey(DbXRef.GI));
        assertEquals("GI ID", dbxrefs.get(DbXRef.GI));
    }

    @Test
    void fromCDSWithDbXrefGeneID() {
        Map<DbXRef, String> dbXrefs = new HashMap<>();
        dbXrefs.put(DbXRef.GENE_ID, "12345");
        CDS dbxref = CDS.builder().withIntervals(defaultIntervals).withDbXref(dbXrefs).build();
        feature = convert.to(dbxref);

        List<Qualifier> dbxrefs = feature.getQualifiers().get("db_xref");
        assertEquals(1, dbxrefs.size());

        DBReferenceInfo dbInfo = (DBReferenceInfo) dbxrefs.get(0);
        assertEquals("GeneID", dbInfo.getDatabase());
        assertEquals("12345", dbInfo.getId());
    }

    @Test
    void withDBXrefGeneID() {
        feature = new TextFeature<>(CDS_NAME, "", "", "");
        feature.addQualifier("db_xref", new DBReferenceInfo("GeneID", "Gene ID"));
        feature.setLocation(location);

        CDS cds = convert.from(feature);
        Map<DbXRef, String> dbxrefs = cds.getDbXref();

        assertTrue(dbxrefs.containsKey(DbXRef.GENE_ID));
        assertEquals("Gene ID", dbxrefs.get(DbXRef.GENE_ID));
    }

    @Test
    void fromCDSWithDbXrefGeneTaxon() {
        Map<DbXRef, String> dbXrefs = new HashMap<>();
        dbXrefs.put(DbXRef.TAXON, "12345");
        CDS dbxref = CDS.builder().withIntervals(defaultIntervals).withDbXref(dbXrefs).build();
        feature = convert.to(dbxref);

        List<Qualifier> dbxrefs = feature.getQualifiers().get("db_xref");
        assertEquals(1, dbxrefs.size());

        DBReferenceInfo dbInfo = (DBReferenceInfo) dbxrefs.get(0);
        assertEquals("taxon", dbInfo.getDatabase());
        assertEquals("12345", dbInfo.getId());
    }

    @Test
    void fromCDSWithAllele() {
        CDS cds = CDS.builder().withIntervals(defaultIntervals).withAllele("Allele").build();

        checkSizeAndValue(convert.to(cds), "allele", 1, "Allele");
    }

    @Test
    void withAlleleFromFeature() {
        CDS cds = convert.from(createFeature("allele", "Allele", location));

        assertEquals("Allele", cds.getAllele());
    }

    @Test
    void fromCDSWithCodonStart() {
        CDS cds = convert.from(createFeature("codon_start", "Codon Start", location));

        checkSizeAndValue(convert.to(cds), "codon_start", 1, "Codon Start");
    }

    @Test
    void withCodonStartFromFeature() {
        CDS cds = convert.from(createFeature("codon_start", "Codon Start", location));

        assertEquals("Codon Start", cds.getCodonStart());
    }

    @Test
    void withEcNumberFromFeature() {
        CDS cds = CDS.builder().withIntervals(defaultIntervals).withEcNumber("Number").build();

        assertEquals("Number", cds.getEcNumber());
    }

    @Test
    void fromCDSWithEcNumber() {
        CDS cds = convert.from(createFeature("EC_number", "Number", location));

        checkSizeAndValue(convert.to(cds), "EC_number", 1, "Number");
    }

    @Test
    void withExceptionFromFeature() {
        CDS cds = CDS.builder().withIntervals(defaultIntervals).withException("Exception").build();

        assertEquals("Exception", cds.getException());
    }

    @Test
    void fromCDSWithException() {
        CDS cds = convert.from(createFeature("exception", "Exception", location));

        checkSizeAndValue(convert.to(cds), "exception", 1, "Exception");
    }

    @Test
    void withGeneFromFeature() {
        CDS cds = CDS.builder().withIntervals(defaultIntervals).withGene("Gene").build();

        assertEquals("Gene", cds.getGene());
    }

    @Test
    void fromCDSWithGene() {
        CDS cds = convert.from(createFeature("gene", "Gene", location));

        checkSizeAndValue(convert.to(cds), "gene", 1, "Gene");
    }

    @Test
    void withGeneSynonymFromFeature() {
        CDS cds = CDS.builder().withIntervals(defaultIntervals).withGeneSynonym("Gene Synonym").build();

        assertEquals("Gene Synonym", cds.getGeneSynonym());
    }

    @Test
    void fromCDSWithGeneSynonym() {
        CDS cds = convert.from(createFeature("gene_synonym", "Gene Synonym", location));

        checkSizeAndValue(convert.to(cds), "gene_synonym", 1, "Gene Synonym");
    }

    @Test
    void withLocusTagFromFeature() {
        CDS cds = CDS.builder().withIntervals(defaultIntervals).withGeneSynonym("Locus Tag").build();

        assertEquals("Locus Tag", cds.getGeneSynonym());
    }

    @Test
    void fromCDSWithLocusTag() {
        CDS cds = convert.from(createFeature("locus_tag", "Locus Tag", location));

        checkSizeAndValue(convert.to(cds), "locus_tag", 1, "Locus Tag");
    }

    @Test
    void withNoteFromFeature() {
        CDS cds = CDS.builder().withIntervals(defaultIntervals).withNote("Note").build();

        assertEquals("Note", cds.getNote());
    }

    @Test
    void fromCDSWithNote() {
        CDS cds = convert.from(createFeature("note", "Note", location));

        checkSizeAndValue(convert.to(cds), "note", 1, "Note");
    }

    @Test
    void withNumberFromFeature() {
        CDS cds = CDS.builder().withIntervals(defaultIntervals).withNumber("Number").build();

        assertEquals("Number", cds.getNumber());
    }

    @Test
    void fromCDSWithNumber() {
        CDS cds = convert.from(createFeature("number", "Number", location));

        checkSizeAndValue(convert.to(cds), "number", 1, "Number");
    }

    @Test
    void withOldLocusTagFromFeature() {
        CDS cds = CDS.builder().withIntervals(defaultIntervals).withOldLocusTag("Old Locus Tag").build();

        assertEquals("Old Locus Tag", cds.getOldLocusTag());
    }

    @Test
    void fromCDSWithOldLocusTag() {
        CDS cds = convert.from(createFeature("old_locus_tag", "Old Locus Tag", location));

        checkSizeAndValue(convert.to(cds), "old_locus_tag", 1, "Old Locus Tag");
    }

    @Test
    void withOperonFromFeature() {
        CDS cds = CDS.builder().withIntervals(defaultIntervals).withOperon("Operon").build();

        assertEquals("Operon", cds.getOperon());
    }

    @Test
    void fromCDSWithOperon() {
        CDS cds = convert.from(createFeature("operon", "Operon", location));

        checkSizeAndValue(convert.to(cds), "operon", 1, "Operon");
    }

    @Test
    void withProductFromFeature() {
        CDS cds = CDS.builder().withIntervals(defaultIntervals).withProduct("Product").build();

        assertEquals("Product", cds.getProduct());
    }

    @Test
    void fromCDSWithProt() {
        CDS cds = convert.from(createFeature("product", "Product", location));

        checkSizeAndValue(convert.to(cds), "product", 1, "Product");
    }

    @Test
    void withProteinIdFromFeature() {
        CDS cds = CDS.builder().withIntervals(defaultIntervals).withProteinId("Protein Id").build();

        assertEquals("Protein Id", cds.getProteinId());
    }

    @Test
    void fromCDSWithProteinId() {
        CDS cds = convert.from(createFeature("protein_id", "Protein Id", location));

        checkSizeAndValue(convert.to(cds), "protein_id", 1, "Protein Id");
    }

    @Test
    void withPseudogeneFromFeature() {
        CDS cds = CDS.builder().withIntervals(defaultIntervals).withPseudogene("Pseudogene").build();

        assertEquals("Pseudogene", cds.getPseudogene());
    }

    @Test
    void fromCDSWithPseudogene() {
        CDS cds = convert.from(createFeature("pseudogene", "Pseudogene", location));

        checkSizeAndValue(convert.to(cds), "pseudogene", 1, "Pseudogene");
    }

    @Test
    void withStandardNameFromFeature() {
        CDS cds = CDS.builder().withIntervals(defaultIntervals).withStandardName("Standard Name").build();

        assertEquals("Standard Name", cds.getStandardName());
    }

    @Test
    void fromCDSWithStandardName() {
        CDS cds = convert.from(createFeature("standard_name", "Standard Name", location));

        checkSizeAndValue(convert.to(cds), "standard_name", 1, "Standard Name");
    }

    @Test
    void withTranslationFromFeature() {
        CDS cds = CDS.builder().withIntervals(defaultIntervals).withTranslation("Translation").build();

        assertEquals("Translation", cds.getTranslation());
    }

    @Test
    void fromCDSWithTranslation() {
        CDS cds = convert.from(createFeature("translation", "Translation", location));

        checkSizeAndValue(convert.to(cds), "translation", 1, "Translation");
    }

    @Test
    void withTranslExceptFromFeature() {
        CDS cds = CDS.builder().withIntervals(defaultIntervals).withTranslExcept("Transl Except").build();

        assertEquals("Transl Except", cds.getTranslExcept());
    }

    @Test
    void fromCDSWithTranslExcept() {
        CDS cds = convert.from(createFeature("transl_except", "Transl Except", location));

        checkSizeAndValue(convert.to(cds), "transl_except", 1, "Transl Except");
    }

    @Test
    void withTranslTableFromFeature() {
        CDS cds = CDS.builder().withIntervals(defaultIntervals).withTranslTable("Transl Table").build();

        assertEquals("Transl Table", cds.getTranslTable());
    }

    @Test
    void fromCDSWithTranslTable() {
        CDS cds = convert.from(createFeature("transl_table", "Transl", location));

        checkSizeAndValue(convert.to(cds), "transl_table", 1, "Transl");
    }

    private void checkSizeAndValue(FeatureInterface<?, ?> feature, String key, int size, String value) {
        List<Qualifier> qualifiers = feature.getQualifiers().get(key);
        assertEquals(size, qualifiers.size());
        assertEquals(value, qualifiers.get(0).getValue());
    }

    private FeatureInterface<AbstractSequence<NucleotideCompound>, NucleotideCompound> createFeature(String key,
            String value, AbstractLocation location) {
        FeatureInterface<AbstractSequence<NucleotideCompound>, NucleotideCompound> feature;

        feature = new TextFeature<>(CDS_NAME, "", "", "");
        feature.addQualifier(key, new Qualifier(key, value));
        feature.setLocation(location);

        return feature;
    }

}
