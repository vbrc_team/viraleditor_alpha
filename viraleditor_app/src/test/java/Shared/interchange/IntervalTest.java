package Shared.interchange;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.junit.jupiter.api.Assertions.assertTrue;

import org.junit.jupiter.api.Test;

class IntervalTest {
    @Test
    void constructorInput() {
        boolean complement = false;
        // Both values must be positive
        assertThrows(IllegalArgumentException.class, () -> new Interval(complement, -1, 2));
        assertThrows(IllegalArgumentException.class, () -> new Interval(complement, -3, -1));
        assertThrows(IllegalArgumentException.class, () -> new Interval(complement, -5, -3));

        // low < high
        assertThrows(IllegalArgumentException.class, () -> new Interval(complement, 3, 3));
        assertThrows(IllegalArgumentException.class, () -> new Interval(complement, 3, 2));

        // low and high must be non-zero
        assertThrows(IllegalArgumentException.class, () -> new Interval(complement, 0, 3));
        assertThrows(IllegalArgumentException.class, () -> new Interval(complement, 3, 0));
    }

    @Test
    void isComplement() {
        assertFalse((new Interval(false, 1, 2)).isComplement());
        assertTrue((new Interval(true, 1, 2)).isComplement());
    }

    @Test
    void getLow() {
        Interval interval = new Interval(false, 1, 2);
        assertEquals(1, interval.getLow());

        interval = new Interval(false, 5, 50);
        assertEquals(5, interval.getLow());

        interval = new Interval(true, 10, 70);
        assertEquals(10, interval.getLow());

        interval = new Interval(true, 7, 90);
        assertEquals(7, interval.getLow());
    }

    @Test
    void getHigh() {
        Interval interval = new Interval(false, 1, 2);
        assertEquals(2, interval.getHigh());

        interval = new Interval(false, 5, 50);
        assertEquals(50, interval.getHigh());

        interval = new Interval(true, 10, 70);
        assertEquals(70, interval.getHigh());

        interval = new Interval(true, 7, 90);
        assertEquals(90, interval.getHigh());
    }
}
