package Shared.interchange;

import static org.junit.jupiter.api.Assertions.assertDoesNotThrow;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

import java.util.ArrayList;
import java.util.List;

import org.junit.jupiter.api.Test;

class ReferenceTest {
    private static final StringToNucleotides toNucleotides = new StringToNucleotides();
    private static final StringToAminoAcids toAminoAcids = new StringToAminoAcids();

    @Test
    void defaultValues() {
        Reference reference = Reference.builder().build();
        assertEquals("", reference.getAccession());
        assertEquals("", reference.getVersion());
        assertEquals("", reference.getOrigin().getSequence());

        List<? extends Feature> features = new ArrayList<Feature>();
        assertEquals(features, reference.getFeatures(Feature.class));
    }

    @Test
    void getAccession() {
        Reference reference = Reference.builder().withAccession("Accession").build();
        assertEquals("Accession", reference.getAccession());
    }

    @Test
    void getVersion() {
        Reference reference = Reference.builder().withVersion("1").build();
        assertEquals("1", reference.getVersion());
    }

    @Test
    void getFeatures() {
        Reference ref = Reference.builder().build();
        assertTrue(ref.getFeatures(Feature.class).isEmpty());

        List<Feature> features = new ArrayList<>();
        List<Interval> intervals = new ArrayList<>();
        intervals.add(new Interval(true, 1, 2));
        features.add(Annotation.builder().withIntervals(intervals).build());
        features.add(Annotation.builder().withIntervals(intervals).build());
        features.add(Family.builder().withIntervals(intervals).build());
        ref = Reference.builder().withFeatures(features).build();

        assertEquals(1, ref.getFeatures(Family.class).size());
        assertEquals(2, ref.getFeatures(Annotation.class).size());
        assertEquals(3, ref.getFeatures(Feature.class).size());
        assertEquals(0, ref.getFeatures(Genome.class).size());

        assertEquals(features.get(0), ref.getFeatures(Annotation.class).get(0));
        assertEquals(features.get(1), ref.getFeatures(Annotation.class).get(1));
        assertEquals(features.get(2), ref.getFeatures(Family.class).get(0));
    }

    @Test
    void getOrigin() {
        Reference reference = Reference.builder().withOrigin(new NucleotideSequence(toNucleotides.to("ACGTN"))).build();
        assertEquals("ACGTN", reference.getOrigin().getSequence());
        reference = Reference.builder().withOrigin(new AminoAcidSequence(toAminoAcids.to("AX-"))).build();
        assertEquals("AX-", reference.getOrigin().getSequence());
    }

    @Test
    void originFormat() {
        assertDoesNotThrow(() -> Reference.builder().withOrigin(new NucleotideSequence(toNucleotides.to("A"))));
        assertDoesNotThrow(() -> Reference.builder().withOrigin(new NucleotideSequence(toNucleotides.to("ACGTN"))));
        assertDoesNotThrow(() -> Reference.builder().withOrigin(new AminoAcidSequence(toAminoAcids.to("H"))));
        assertDoesNotThrow(
                () -> Reference.builder()
                               .withOrigin(new AminoAcidSequence(toAminoAcids.to("ACDEFGHIKLMNPQRSTVWYX-"))));
    }

}
