package Shared.interchange;

import static org.junit.jupiter.api.Assertions.assertEquals;

import java.io.IOException;
import java.net.URISyntaxException;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Stream;

import org.junit.jupiter.api.Test;

public class InterchangeToFASTATest {
    private static final StringToNucleotides toNucleotides = new StringToNucleotides();

    @Test
    void toFASTASingleLineSequence() {
        Reference ref = Reference.builder().withOrigin(new NucleotideSequence(toNucleotides.to("ACGTN"))).build();
        List<Reference> refs = new ArrayList<>();
        refs.add(ref);

        Interchange interchange = Interchange.builder().withReferences(refs).build();
        InterchangeToFASTA toFASTA = new InterchangeToFASTA();

        String fasta = toFASTA.to(interchange);

        assertEquals(">\nACGTN\n", fasta);
    }

    @Test
    void toFASTAMultiLineSequence() throws IOException, URISyntaxException {
        StringBuilder builder = new StringBuilder();
        Path path = Paths.get(getClass().getResource("longSequence.fasta").toURI());

        try (Stream<String> lines = Files.lines(path, StandardCharsets.UTF_8);) {
            lines.forEach(s -> builder.append(s + "\n"));
        }

        String longSeq = "ACTTAAGTACCTTATCTATCTACAGATAGAAAAGTTGCTTTTTAGACTTTGTGTCTACTTTTCTCAACTAAACGAAATTTTTGCTAT";
        Reference ref = Reference.builder().withOrigin(new NucleotideSequence(toNucleotides.to(longSeq))).build();
        List<Reference> refs = new ArrayList<>();
        refs.add(ref);

        Interchange interchange = Interchange.builder().withReferences(refs).build();
        InterchangeToFASTA toFASTA = new InterchangeToFASTA();

        String fasta = toFASTA.to(interchange);

        assertEquals(builder.toString(), fasta);
    }

    @Test
    void toFASTAGenomeSequence() {
        List<Interval> intervals = new ArrayList<>();
        intervals.add(new Interval(false, 1, 5));

        List<Feature> features = new ArrayList<>();
        features.add(Genome.builder()
                           .withIntervals(intervals)
                           .withOrganismAbbrevation("ACGTN-1")
                           .withOrganismName("Sequence")
                           .build());

        Reference ref = Reference.builder()
                                 .withOrigin(new NucleotideSequence(toNucleotides.to("ACGTN")))
                                 .withFeatures(features)
                                 .withAccession("NC_000000")
                                 .build();
        List<Reference> refs = new ArrayList<>();
        refs.add(ref);

        Interchange interchange = Interchange.builder().withReferences(refs).build();
        InterchangeToFASTA toFASTA = new InterchangeToFASTA();

        String fasta = toFASTA.to(interchange);

        assertEquals(">lcl|ACGTN-1|NC_000000|Sequence\nACGTN\n", fasta);
    }

    @Test
    void fromFASTASingleLineSequenceNoHeader() {
        String fasta = ">\nacgtn\n";
        InterchangeToFASTA toFASTA = new InterchangeToFASTA();
        Interchange interchange = toFASTA.from(fasta);
        Reference ref = interchange.getReferences().get(0);

        assertEquals("ACGTN", ref.getOrigin().getSequence());
    }

    @Test
    void fromFASTASingleLineSequenceWithHeader() {
        String fasta = ">Header line\nACGTN\n";
        InterchangeToFASTA toFASTA = new InterchangeToFASTA();
        Interchange interchange = toFASTA.from(fasta);
        Reference ref = interchange.getReferences().get(0);
        Description description = ref.getFeatures(Description.class).get(0);

        assertEquals("ACGTN", ref.getOrigin().getSequence());
        assertEquals("Header line", description.getDescription());
    }

    @Test
    void fromFASTAMultiLineSequenceWithHeader() {
        String fasta = ">Header line\nacgtn\n>Header line\nACGTN\n";
        InterchangeToFASTA toFASTA = new InterchangeToFASTA();
        Interchange interchange = toFASTA.from(fasta);

        assertEquals(2, interchange.getReferences().size());
        for (Reference ref : interchange.getReferences()) {
            Description description = ref.getFeatures(Description.class).get(0);
            assertEquals("ACGTN", ref.getOrigin().getSequence());
            assertEquals("Header line", description.getDescription());
        }
    }

    @Test
    void fromFASTAMixOfHeaderAndNoHeader() {
        String fasta = ">Header line\nacgtn\n>lcl|ACGTN-1|NC_000000|Sequence\nAACCGTN\n";
        InterchangeToFASTA toFASTA = new InterchangeToFASTA();
        Interchange interchange = toFASTA.from(fasta);

        assertEquals(2, interchange.getReferences().size());
        Reference noHeader = interchange.getReferences().get(0);
        Reference header = interchange.getReferences().get(1);

        Description description = noHeader.getFeatures(Description.class).get(0);
        assertEquals("ACGTN", noHeader.getOrigin().getSequence());
        assertEquals("Header line", description.getDescription());

        description = header.getFeatures(Description.class).get(0);
        assertEquals("AACCGTN", header.getOrigin().getSequence());
        assertEquals("lcl|ACGTN-1|NC_000000|Sequence", description.getDescription());
    }

}
