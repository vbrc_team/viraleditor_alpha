package Shared.interchange;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.biojava.nbio.core.sequence.compound.NucleotideCompound;
import org.biojava.nbio.core.sequence.features.DBReferenceInfo;
import org.biojava.nbio.core.sequence.features.FeatureInterface;
import org.biojava.nbio.core.sequence.features.Qualifier;
import org.biojava.nbio.core.sequence.features.TextFeature;
import org.biojava.nbio.core.sequence.location.SimpleLocation;
import org.biojava.nbio.core.sequence.location.template.AbstractLocation;
import org.biojava.nbio.core.sequence.template.AbstractSequence;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

class SourceToFeatureTest {
    private SourceToFeature convert;
    private List<Interval> defaultIntervals;
    private FeatureInterface<AbstractSequence<NucleotideCompound>, NucleotideCompound> feature;
    private AbstractLocation location;

    private static final String SOURCE = "source";

    @BeforeEach
    void before() {
        convert = new SourceToFeature();
        defaultIntervals = new ArrayList<>();
        defaultIntervals.add(new Interval(false, 1, 3));
        location = new SimpleLocation(1, 3);
        feature = null;
    }

    @Test
    void fromSourceWithDbXrefGI() {
        Map<DbXRef, String> dbXrefs = new HashMap<>();
        dbXrefs.put(DbXRef.GI, "12345");
        Source dbxref = Source.builder().withIntervals(defaultIntervals).withDbXref(dbXrefs).build();
        feature = convert.to(dbxref);

        List<Qualifier> dbxrefs = feature.getQualifiers().get("db_xref");
        assertEquals(1, dbxrefs.size());

        DBReferenceInfo dbInfo = (DBReferenceInfo) dbxrefs.get(0);
        assertEquals("GI", dbInfo.getDatabase());
        assertEquals("12345", dbInfo.getId());
    }

    @Test
    void withDBXrefGI() {
        feature = new TextFeature<>(SOURCE, "", "", "");
        feature.addQualifier("db_xref", new DBReferenceInfo("GI", "GI ID"));
        feature.setLocation(location);

        Source matPeptide = convert.from(feature);
        Map<DbXRef, String> dbxrefs = matPeptide.getDbXref();

        assertTrue(dbxrefs.containsKey(DbXRef.GI));
        assertEquals("GI ID", dbxrefs.get(DbXRef.GI));
    }

    @Test
    void fromSourceWithDbXrefGeneID() {
        Map<DbXRef, String> dbXrefs = new HashMap<>();
        dbXrefs.put(DbXRef.GENE_ID, "12345");
        Source dbxref = Source.builder().withIntervals(defaultIntervals).withDbXref(dbXrefs).build();
        feature = convert.to(dbxref);

        List<Qualifier> dbxrefs = feature.getQualifiers().get("db_xref");
        assertEquals(1, dbxrefs.size());

        DBReferenceInfo dbInfo = (DBReferenceInfo) dbxrefs.get(0);
        assertEquals("GeneID", dbInfo.getDatabase());
        assertEquals("12345", dbInfo.getId());
    }

    @Test
    void withDBXrefGeneID() {
        feature = new TextFeature<>(SOURCE, "", "", "");
        feature.addQualifier("db_xref", new DBReferenceInfo("GeneID", "Gene ID"));
        feature.setLocation(location);

        Source matPeptide = convert.from(feature);
        Map<DbXRef, String> dbxrefs = matPeptide.getDbXref();

        assertTrue(dbxrefs.containsKey(DbXRef.GENE_ID));
        assertEquals("Gene ID", dbxrefs.get(DbXRef.GENE_ID));
    }

    @Test
    void fromSourceWithDbXrefGeneTaxon() {
        Map<DbXRef, String> dbXrefs = new HashMap<>();
        dbXrefs.put(DbXRef.TAXON, "12345");
        Source dbxref = Source.builder().withIntervals(defaultIntervals).withDbXref(dbXrefs).build();
        feature = convert.to(dbxref);

        List<Qualifier> dbxrefs = feature.getQualifiers().get("db_xref");
        assertEquals(1, dbxrefs.size());

        DBReferenceInfo dbInfo = (DBReferenceInfo) dbxrefs.get(0);
        assertEquals("taxon", dbInfo.getDatabase());
        assertEquals("12345", dbInfo.getId());
    }

    @Test
    void withDBXrefTaxon() {
        feature = new TextFeature<>(SOURCE, "", "", "");
        feature.addQualifier("db_xref", new DBReferenceInfo("taxon", "Taxon"));
        feature.setLocation(location);

        Source source = convert.from(feature);
        Map<DbXRef, String> dbxrefs = source.getDbXref();

        assertTrue(dbxrefs.containsKey(DbXRef.TAXON));
        assertEquals("Taxon", dbxrefs.get(DbXRef.TAXON));
    }

    @Test
    void fromSourceWithHost() {
        Source source = Source.builder().withIntervals(defaultIntervals).withHost("Host").build();

        checkSizeAndValue(convert.to(source), "host", 1, "Host");
    }

    @Test
    void withHostFromFeature() {
        feature = new TextFeature<>(SOURCE, "", "", "");
        feature.addQualifier("host", new Qualifier("host", "Host"));
        feature.setLocation(location);

        Source Source = convert.from(feature);

        assertEquals(1, Source.getIntervals().size());
        assertEquals("Host", Source.getHost());
    }

    @Test
    void fromSourceWithIsolate() {
        Source source = Source.builder().withIntervals(defaultIntervals).withIsolate("Isolate").build();

        checkSizeAndValue(convert.to(source), "isolate", 1, "Isolate");
    }

    @Test
    void withIsolateFromFeature() {
        feature = new TextFeature<>(SOURCE, "", "", "");
        feature.addQualifier("isolate", new Qualifier("isolate", "Isolate"));
        feature.setLocation(location);

        Source Source = convert.from(feature);

        assertEquals(1, Source.getIntervals().size());
        assertEquals("Isolate", Source.getIsolate());
    }

    @Test
    void fromSourceWithIsolationSource() {
        Source source = Source.builder()
                              .withIntervals(defaultIntervals)
                              .withIsolationSource("Isolation Source")
                              .build();

        checkSizeAndValue(convert.to(source), "isolation_source", 1, "Isolation Source");
    }

    @Test
    void withIsolationSourceFromFeature() {
        feature = new TextFeature<>(SOURCE, "", "", "");
        feature.addQualifier("isolation_source", new Qualifier("isolation_source", "Isolation Source"));
        feature.setLocation(location);

        Source Source = convert.from(feature);

        assertEquals(1, Source.getIntervals().size());
        assertEquals("Isolation Source", Source.getIsolationSource());
    }

    @Test
    void fromSourceWithLabHostSource() {
        Source source = Source.builder().withIntervals(defaultIntervals).withLabHost("Lab Host").build();

        checkSizeAndValue(convert.to(source), "lab_host", 1, "Lab Host");
    }

    @Test
    void withLabHostFromFeature() {
        feature = new TextFeature<>(SOURCE, "", "", "");
        feature.addQualifier("lab_host", new Qualifier("lab_host", "Lab Host"));
        feature.setLocation(location);

        Source Source = convert.from(feature);

        assertEquals(1, Source.getIntervals().size());
        assertEquals("Lab Host", Source.getLabHost());
    }

    @Test
    void fromSourceWithMetagenomeSource() {
        Source source = Source.builder().withIntervals(defaultIntervals).withMetagenomeSource("Source").build();

        checkSizeAndValue(convert.to(source), "metagenome_source", 1, "Source");
    }

    @Test
    void withMetagenomeSourceFromFeature() {
        feature = new TextFeature<>(SOURCE, "", "", "");
        feature.addQualifier("metagenome_source", new Qualifier("metagenome_source", "Source"));
        feature.setLocation(location);

        Source Source = convert.from(feature);

        assertEquals(1, Source.getIntervals().size());
        assertEquals("Source", Source.getMetagenomeSource());
    }

    @Test
    void fromSourceWithMolType() {
        Source source = Source.builder().withIntervals(defaultIntervals).withMolType("Mol FeatureType").build();

        checkSizeAndValue(convert.to(source), "mol_type", 1, "Mol FeatureType");
    }

    @Test
    void withMolTypeFromFeature() {
        feature = new TextFeature<>(SOURCE, "", "", "");
        feature.addQualifier("mol_type", new Qualifier("mol_type", "Mol FeatureType"));
        feature.setLocation(location);

        Source Source = convert.from(feature);

        assertEquals(1, Source.getIntervals().size());
        assertEquals("Mol FeatureType", Source.getMolType());
    }

    @Test
    void fromSourceWithNote() {
        Source source = Source.builder().withIntervals(defaultIntervals).withNote("Note").build();

        checkSizeAndValue(convert.to(source), "note", 1, "Note");
    }

    @Test
    void withNoteFromFeature() {
        feature = new TextFeature<>(SOURCE, "", "", "");
        feature.addQualifier("note", new Qualifier("note", "Note"));
        feature.setLocation(location);

        Source Source = convert.from(feature);

        assertEquals(1, Source.getIntervals().size());
        assertEquals("Note", Source.getNote());
    }

    @Test
    void fromSourceWithOrganism() {
        Source source = Source.builder().withIntervals(defaultIntervals).withOrganism("Organism").build();

        checkSizeAndValue(convert.to(source), "organism", 1, "Organism");
    }

    @Test
    void withOrganismFromFeature() {
        feature = new TextFeature<>(SOURCE, "", "", "");
        feature.addQualifier("organism", new Qualifier("organism", "Organism"));
        feature.setLocation(location);

        Source Source = convert.from(feature);

        assertEquals(1, Source.getIntervals().size());
        assertEquals("Organism", Source.getOrganism());
    }

    @Test
    void fromSourceWithPCRPrimers() {
        Source source = Source.builder().withIntervals(defaultIntervals).withPCRPrimers("PCR Primers").build();

        checkSizeAndValue(convert.to(source), "PCR_primers", 1, "PCR Primers");
    }

    @Test
    void withPCRPrimersFromFeature() {
        feature = new TextFeature<>(SOURCE, "", "", "");
        feature.addQualifier("PCR_primers", new Qualifier("PCR_primers", "PCR Primers"));
        feature.setLocation(location);

        Source Source = convert.from(feature);

        assertEquals(1, Source.getIntervals().size());
        assertEquals("PCR Primers", Source.getPCRPrimers());
    }

    @Test
    void fromSourceWithSegment() {
        Source source = Source.builder().withIntervals(defaultIntervals).withSegment("Segment").build();

        checkSizeAndValue(convert.to(source), "segment", 1, "Segment");
    }

    @Test
    void withSegmentFromFeature() {
        Source Source = convert.from(createFeature("segment", "Segment", location));

        assertEquals(1, Source.getIntervals().size());
        assertEquals("Segment", Source.getSegment());
    }

    @Test
    void fromSourceWithSerotype() {
        Source source = Source.builder().withIntervals(defaultIntervals).withSerotype("Serotype").build();

        checkSizeAndValue(convert.to(source), "serotype", 1, "Serotype");
    }

    @Test
    void withSerotypeFromFeature() {
        Source Source = convert.from(createFeature("serotype", "Serotype", location));

        assertEquals(1, Source.getIntervals().size());
        assertEquals("Serotype", Source.getSerotype());
    }

    @Test
    void fromSourceWithStrain() {
        Source source = Source.builder().withIntervals(defaultIntervals).withStrain("Strain").build();

        checkSizeAndValue(convert.to(source), "strain", 1, "Strain");
    }

    @Test
    void withStrainFromFeature() {
        Source Source = convert.from(createFeature("strain", "Strain", location));

        assertEquals(1, Source.getIntervals().size());
        assertEquals("Strain", Source.getStrain());
    }

    @Test
    void fromSourceWithSubSpecies() {
        Source source = Source.builder().withIntervals(defaultIntervals).withSubSpecies("Subspecies").build();

        checkSizeAndValue(convert.to(source), "sub_species", 1, "Subspecies");
    }

    @Test
    void withSubSpeciesFromFeature() {
        Source Source = convert.from(createFeature("sub_species", "Subspecies", location));

        assertEquals(1, Source.getIntervals().size());
        assertEquals("Subspecies", Source.getSubSpecies());
    }

    @Test
    void fromSourceWithSubStrain() {
        Source source = Source.builder().withIntervals(defaultIntervals).withSubStrain("Substrain").build();

        checkSizeAndValue(convert.to(source), "sub_strain", 1, "Substrain");
    }

    @Test
    void withSubStrainFromFeature() {
        Source Source = convert.from(createFeature("sub_strain", "Substrain", location));

        assertEquals(1, Source.getIntervals().size());
        assertEquals("Substrain", Source.getSubStrain());
    }

    @Test
    void fromSourceWithTissueType() {
        Source source = Source.builder().withIntervals(defaultIntervals).withTissueType("Tissue FeatureType").build();

        checkSizeAndValue(convert.to(source), "tissue_type", 1, "Tissue FeatureType");
    }

    @Test
    void withTissueTypeFromFeature() {
        Source Source = convert.from(createFeature("tissue_type", "Tissue FeatureType", location));

        assertEquals(1, Source.getIntervals().size());
        assertEquals("Tissue FeatureType", Source.getTissueType());
    }

    private void checkSizeAndValue(FeatureInterface<?, ?> feature, String key, int size, String value) {
        List<Qualifier> qualifiers = feature.getQualifiers().get(key);
        assertEquals(size, qualifiers.size());
        assertEquals(value, qualifiers.get(0).getValue());
    }

    private FeatureInterface<AbstractSequence<NucleotideCompound>, NucleotideCompound> createFeature(String key,
            String value, AbstractLocation location) {
        FeatureInterface<AbstractSequence<NucleotideCompound>, NucleotideCompound> feature;

        feature = new TextFeature<>(SOURCE, "", "", "");
        feature.addQualifier(key, new Qualifier(key, value));
        feature.setLocation(location);

        return feature;
    }

}
