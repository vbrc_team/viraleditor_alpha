package Shared.interchange;

import static org.junit.jupiter.api.Assertions.assertEquals;

import java.io.IOException;
import java.util.List;

import org.junit.jupiter.api.Test;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

public class AminoAcidSequenceTest {
    private static final StringToAminoAcids toAminoAcids = new StringToAminoAcids();

    @Test
    void toJson() throws JsonProcessingException {
        String sequence = "ACDEFGHIKLMNPQRSTVWYX-";
        List<AminoAcid> acids = toAminoAcids.to(sequence);
        String json = new ObjectMapper().writer().writeValueAsString(new AminoAcidSequence(acids));
        assertEquals("{\"type\":\"AminoAcidSequence\",\"sequence\":\"ACDEFGHIKLMNPQRSTVWYX-\"}", json);
    }

    @Test
    void fromJson() throws IOException {
        String json = "{\"type\":\"AminoAcidSequence\",\"sequence\":\"ACDEFGHIKLMNPQRSTVWYX-\"}";
        AminoAcidSequence sequence = new ObjectMapper().reader().readValue(json, AminoAcidSequence.class);
        assertEquals("ACDEFGHIKLMNPQRSTVWYX-", sequence.getSequence());
    }
}
