package Shared.interchange;

import static org.junit.jupiter.api.Assertions.assertEquals;

import java.io.IOException;
import java.util.List;

import org.junit.jupiter.api.Test;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

public class NucleotideSequenceTest {
    private static final StringToNucleotides toNucleotides = new StringToNucleotides();

    @Test
    void toJson() throws JsonProcessingException {
        String sequence = "ACGTN-";
        List<Nucleotide> nucleotides = toNucleotides.to(sequence);
        String json = new ObjectMapper().writer()
                                        .writeValueAsString(new NucleotideSequence(nucleotides));
        assertEquals("{\"type\":\"NucleotideSequence\",\"sequence\":\"ACGTN-\"}", json);
    }

    @Test
    void fromJson() throws IOException {
        String json = "{\"type\":\"NucleotideSequence\",\"sequence\":\"ACGTN-\"}";
        NucleotideSequence sequence = new ObjectMapper().reader().readValue(json, NucleotideSequence.class);
        assertEquals("ACGTN-", sequence.getSequence());
    }
}
