package Shared.interchange;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

import java.util.ArrayList;
import java.util.List;

import org.biojava.nbio.core.sequence.Strand;
import org.biojava.nbio.core.sequence.location.SimpleLocation;
import org.biojava.nbio.core.sequence.location.template.AbstractLocation;
import org.biojava.nbio.core.sequence.location.template.Location;
import org.junit.jupiter.api.Test;

class IntervalsToAbstractLocationTest {

    @Test
    void noncomplementIntervalToLocation() {
        List<Interval> intervals = new ArrayList<>();
        intervals.add(new Interval(false, 1, 3));

        IntervalsToAbstractLocation toLocation = new IntervalsToAbstractLocation();
        AbstractLocation location = toLocation.to(intervals);

        assertEquals(Strand.POSITIVE, location.getStrand());
        assertEquals(1, location.getStart().getPosition());
        assertEquals(3, location.getEnd().getPosition());
    }

    @Test
    void complementIntervalToLocation() {
        List<Interval> intervals = new ArrayList<>();
        intervals.add(new Interval(true, 1, 3));

        IntervalsToAbstractLocation toLocation = new IntervalsToAbstractLocation();
        AbstractLocation location = toLocation.to(intervals);

        assertEquals(Strand.NEGATIVE, location.getStrand());
        assertEquals(1, location.getStart().getPosition());
        assertEquals(3, location.getEnd().getPosition());
    }

    @Test
    void negativeLocationToInterval() {
        AbstractLocation location = new SimpleLocation(1, 3, Strand.NEGATIVE);
        IntervalsToAbstractLocation fromLocation = new IntervalsToAbstractLocation();
        List<Interval> intervals = fromLocation.from(location);

        assertEquals(1, intervals.size());
        Interval interval = intervals.get(0);

        assertEquals(true, interval.isComplement());
        assertEquals(1, interval.getLow());
        assertEquals(3, interval.getHigh());
    }

    @Test
    void positiveLocationToInterval() {
        AbstractLocation location = new SimpleLocation(1, 3, Strand.POSITIVE);
        IntervalsToAbstractLocation fromLocation = new IntervalsToAbstractLocation();
        List<Interval> intervals = fromLocation.from(location);

        assertEquals(1, intervals.size());
        Interval interval = intervals.get(0);

        assertEquals(false, interval.isComplement());
        assertEquals(1, interval.getLow());
        assertEquals(3, interval.getHigh());
    }

    @Test
    void intervalsToLocation() {
        List<Interval> intervals = new ArrayList<>();
        intervals.add(new Interval(false, 1, 3));
        intervals.add(new Interval(true, 5, 10));
        intervals.add(new Interval(false, 17, 33));

        IntervalsToAbstractLocation toLocation = new IntervalsToAbstractLocation();
        AbstractLocation location = toLocation.to(intervals);

        assertEquals(Strand.POSITIVE, location.getStrand());
        assertEquals(1, location.getStart().getPosition());
        assertEquals(33, location.getEnd().getPosition());

        assertEquals(3, location.getSubLocations().size());
        List<Location> subLocations = location.getSubLocations();

        Location loc = subLocations.get(0);
        assertEquals(Strand.POSITIVE, loc.getStrand());
        assertEquals(1, loc.getStart().getPosition());
        assertEquals(3, loc.getEnd().getPosition());

        loc = subLocations.get(1);
        assertEquals(Strand.NEGATIVE, loc.getStrand());
        assertEquals(5, loc.getStart().getPosition());
        assertEquals(10, loc.getEnd().getPosition());

        loc = subLocations.get(2);
        assertEquals(Strand.POSITIVE, loc.getStrand());
        assertEquals(17, loc.getStart().getPosition());
        assertEquals(33, loc.getEnd().getPosition());
    }

    @Test
    void noIntervalsThrowsException() {
        List<Interval> intervals = new ArrayList<>();
        IntervalsToAbstractLocation toLocation = new IntervalsToAbstractLocation();

        assertThrows(IllegalArgumentException.class, () -> toLocation.to(intervals));
    }

}
