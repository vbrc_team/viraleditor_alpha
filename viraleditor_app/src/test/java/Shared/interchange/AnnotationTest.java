package Shared.interchange;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

import java.util.Arrays;
import java.util.List;

import org.junit.jupiter.api.Test;

class AnnotationTest {
    private static final List<Interval> intervals = Arrays.asList(new Interval(false, 1, 2));
    private static final StringToNucleotides toNucleotides = new StringToNucleotides();
    private static final StringToAminoAcids toAminoAcids = new StringToAminoAcids();

    @Test
    void getType() {
        Annotation annotation = Annotation.builder().withIntervals(intervals).build();
        assertEquals(FeatureType.ANNOTATION, annotation.getType());
    }

    @Test
    void getGeneID() {
        Annotation annotation = Annotation.builder().withIntervals(intervals).build();
        assertEquals(0, annotation.getGeneID());

        annotation = Annotation.builder().withIntervals(intervals).withGeneID(2).build();
        assertEquals(2, annotation.getGeneID());

        Annotation.Builder builder = Annotation.builder().withIntervals(intervals);

        assertThrows(IllegalArgumentException.class, () -> builder.withGeneID(-2));
    }

    @Test
    void getFamilyID() {
        Annotation annotation = Annotation.builder().withIntervals(intervals).build();
        assertEquals(0, annotation.getFamilyID());

        annotation = Annotation.builder().withIntervals(intervals).withFamilyID(2).build();
        assertEquals(2, annotation.getFamilyID());

        Annotation.Builder builder = Annotation.builder().withIntervals(intervals);

        assertThrows(IllegalArgumentException.class, () -> builder.withFamilyID(-2));
    }

    @Test
    void getGenomeID() {
        Annotation annotation = Annotation.builder().withIntervals(intervals).build();
        assertEquals(0, annotation.getGenomeID());

        annotation = Annotation.builder().withIntervals(intervals).withGenomeID(2).build();
        assertEquals(2, annotation.getGenomeID());

        Annotation.Builder builder = Annotation.builder();
        assertThrows(IllegalArgumentException.class, () -> builder.withGenomeID(-2));
    }

    @Test
    void getGeneAbbreviation() {
        Annotation annotation = Annotation.builder().withIntervals(intervals).build();
        assertEquals("", annotation.getGeneAbbreviation());

        annotation = Annotation.builder().withIntervals(intervals).withGeneAbbreviation("Abbreviation").build();
        assertEquals("Abbreviation", annotation.getGeneAbbreviation());
    }

    @Test
    void getStrand() {
        Annotation annotation = Annotation.builder().withIntervals(intervals).build();
        assertEquals('+', annotation.getStrand());

        annotation = Annotation.builder().withIntervals(intervals).withStrand('-').build();
        assertEquals('-', annotation.getStrand());

        annotation = Annotation.builder().withIntervals(intervals).withStrand('+').build();
        assertEquals('+', annotation.getStrand());

        Annotation.Builder builder = Annotation.builder();
        assertThrows(IllegalArgumentException.class, () -> builder.withStrand('A'));
    }

    @Test
    void getAnnotator() {
        Annotation annotation = Annotation.builder().withIntervals(intervals).build();
        assertEquals("", annotation.getAnnotator());

        annotation = Annotation.builder().withIntervals(intervals).withAnnotator("Annotator").build();
        assertEquals("Annotator", annotation.getAnnotator());
    }

    @Test
    void getAnnoSource() {
        Annotation annotation = Annotation.builder().withIntervals(intervals).build();
        assertEquals("", annotation.getAnnoSource());

        annotation = Annotation.builder().withIntervals(intervals).withAnnoSource("Source").build();
        assertEquals("Source", annotation.getAnnoSource());
    }

    @Test
    void getMoleculeType() {
        Annotation annotation = Annotation.builder().withIntervals(intervals).build();
        assertEquals("", annotation.getMoleculeType());

        annotation = Annotation.builder().withIntervals(intervals).withMoleculeType("Molecule").build();
        assertEquals("Molecule", annotation.getMoleculeType());
    }

    @Test
    void getLastModified() {
        Annotation annotation = Annotation.builder().withIntervals(intervals).build();
        assertEquals("1970-01-01T00:00", annotation.getLastModified());

        annotation = Annotation.builder().withIntervals(intervals).withLastModified("2020-01-01T00:00").build();
        assertEquals("2020-01-01T00:00", annotation.getLastModified());

        Annotation.Builder builder = Annotation.builder().withIntervals(intervals);
        assertThrows(IllegalArgumentException.class, () -> builder.withLastModified("1970-01-01"));
    }

    @Test
    void getDescription() {
        Annotation annotation = Annotation.builder().withIntervals(intervals).build();
        assertEquals("", annotation.getDescription());

        annotation = Annotation.builder().withIntervals(intervals).withDescription("Description").build();
        assertEquals("Description", annotation.getDescription());
    }

    @Test
    void getNotes() {
        Annotation annotation = Annotation.builder().withIntervals(intervals).build();
        assertEquals("", annotation.getNotes());

        annotation = Annotation.builder().withIntervals(intervals).withNotes("Notes").build();
        assertEquals("Notes", annotation.getNotes());
    }

    @Test
    void getGenBankName() {
        Annotation annotation = Annotation.builder().withIntervals(intervals).build();
        assertEquals("", annotation.getGenBankName());

        annotation = Annotation.builder().withIntervals(intervals).withGenBankName("Name").build();
        assertEquals("Name", annotation.getGenBankName());
    }

    @Test
    void getGenkBankGeneGI() {
        Annotation annotation = Annotation.builder().withIntervals(intervals).build();
        assertEquals(0, annotation.getGenBankGeneGI());

        annotation = Annotation.builder().withIntervals(intervals).withGenBankGeneGI(1).build();
        assertEquals(1, annotation.getGenBankGeneGI());

        Annotation.Builder builder = Annotation.builder().withIntervals(intervals);
        assertThrows(IllegalArgumentException.class, () -> builder.withGenBankGeneGI(-2));
    }

    @Test
    void getGenBankGeneVersion() {
        Annotation annotation = Annotation.builder().withIntervals(intervals).build();
        assertEquals(1, annotation.getGenBankGeneVersion());

        annotation = Annotation.builder().withIntervals(intervals).withGenBankGeneVersion(2).build();
        assertEquals(2, annotation.getGenBankGeneVersion());

        Annotation.Builder builder = Annotation.builder().withIntervals(intervals);
        assertThrows(IllegalArgumentException.class, () -> builder.withGenBankGeneVersion(-2));
    }

    @Test
    void getOrthologGroupID() {
        Annotation annotation = Annotation.builder().withIntervals(intervals).build();
        assertEquals(0, annotation.getOrthologGroupID());

        annotation = Annotation.builder().withIntervals(intervals).withOrthologGroupID(1).build();
        assertEquals(1, annotation.getOrthologGroupID());

        Annotation.Builder builder = Annotation.builder().withIntervals(intervals);
        assertThrows(IllegalArgumentException.class, () -> builder.withOrthologGroupID(-2));
    }

    @Test
    void getOrthologEvidence() {
        Annotation annotation = Annotation.builder().withIntervals(intervals).build();
        assertEquals("", annotation.getOrthologEvidence());

        annotation = Annotation.builder().withIntervals(intervals).withOrthologEvidence("manual").build();
        assertEquals("manual", annotation.getOrthologEvidence());
    }

    @Test
    void getUpstreamSequence() {
        Annotation annotation = Annotation.builder().withIntervals(intervals).build();
        assertEquals("", annotation.getUpstreamSequence().getSequence());

        NucleotideSequence valid = new NucleotideSequence(toNucleotides.to("ACGTN-"));
        annotation = Annotation.builder().withIntervals(intervals).withUpstreamSequence(valid).build();
        assertEquals("ACGTN-", annotation.getUpstreamSequence().getSequence());
    }

    @Test
    void getNTSequence() {
        Annotation annotation = Annotation.builder().withIntervals(intervals).build();
        assertEquals("", annotation.getNTSequence().getSequence());

        NucleotideSequence valid = new NucleotideSequence(toNucleotides.to("ACGTN-"));
        annotation = Annotation.builder().withIntervals(intervals).withNTSequence(valid).build();
        assertEquals("ACGTN-", annotation.getNTSequence().getSequence());
    }

    @Test
    void withAASequence() {
        Annotation annotation = Annotation.builder().withIntervals(intervals).build();
        assertEquals("", annotation.getAASequence().getSequence());

        AminoAcidSequence valid = new AminoAcidSequence(toAminoAcids.to("ACDEFGHIKLMNPQRSTVWYX-"));
        annotation = Annotation.builder().withIntervals(intervals).withAASequence(valid).build();
        assertEquals("ACDEFGHIKLMNPQRSTVWYX-", annotation.getAASequence().getSequence());
    }

    @Test
    void getInsertedOn() {
        Annotation annotation = Annotation.builder().withIntervals(intervals).build();
        assertEquals("1970-01-01T00:00", annotation.getLastModified());

        annotation = Annotation.builder().withIntervals(intervals).withInsertedOn("2020-01-01T00:00").build();
        assertEquals("2020-01-01T00:00", annotation.getInsertedOn());

        Annotation.Builder builder = Annotation.builder().withIntervals(intervals);
        assertThrows(IllegalArgumentException.class, () -> builder.withInsertedOn("1970-01-01"));
    }

}
