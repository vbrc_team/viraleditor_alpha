package Shared.interchange;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

import java.util.Arrays;
import java.util.List;

import org.junit.jupiter.api.Test;

class TaxonomyNodeTest {
    private static final List<Interval> intervals = Arrays.asList(new Interval(false, 1, 2));

    @Test
    void invalidNodeID() {
        TaxonomyNode.Builder builder = TaxonomyNode.builder().withIntervals(intervals);
        assertThrows(IllegalArgumentException.class, () -> builder.withNodeID(-2));
    }

    @Test
    void invalidParentNodeID() {
        TaxonomyNode.Builder builder = TaxonomyNode.builder().withIntervals(intervals);
        assertThrows(IllegalArgumentException.class, () -> builder.withParentNodeID(-2));
    }

    @Test
    void invalidFamilyID() {
        TaxonomyNode.Builder builder = TaxonomyNode.builder().withIntervals(intervals);
        assertThrows(IllegalArgumentException.class, () -> builder.withFamilyID(-2));
    }

    @Test
    void invalidGenusID() {
        TaxonomyNode.Builder builder = TaxonomyNode.builder().withIntervals(intervals);
        assertThrows(IllegalArgumentException.class, () -> builder.withGenusID(-2));
    }

    @Test
    void testInvalidSpeciesID() {
        TaxonomyNode.Builder builder = TaxonomyNode.builder().withIntervals(intervals);
        assertThrows(IllegalArgumentException.class, () -> builder.withSpeciesID(-2));
    }

    @Test
    void getNodeID() {
        TaxonomyNode node = TaxonomyNode.builder().withIntervals(intervals).build();
        assertEquals(0, node.getNodeID());

        node = TaxonomyNode.builder().withIntervals(intervals).withNodeID(2).build();
        assertEquals(2, node.getNodeID());
    }

    @Test
    void getParentNodeID() {
        TaxonomyNode node = TaxonomyNode.builder().withIntervals(intervals).build();
        assertEquals(0, node.getParentNodeID());

        node = TaxonomyNode.builder().withIntervals(intervals).withParentNodeID(2).build();
        assertEquals(2, node.getParentNodeID());
    }

    @Test
    void getFamilyID() {
        TaxonomyNode node = TaxonomyNode.builder().withIntervals(intervals).build();
        assertEquals(0, node.getFamilyID());

        node = TaxonomyNode.builder().withIntervals(intervals).withFamilyID(2).build();
        assertEquals(2, node.getFamilyID());
    }

    @Test
    void getSpeciesID() {
        TaxonomyNode node = TaxonomyNode.builder().withIntervals(intervals).build();
        assertEquals(0, node.getSpeciesID());

        node = TaxonomyNode.builder().withIntervals(intervals).withSpeciesID(2).build();
        assertEquals(2, node.getSpeciesID());
    }

    @Test
    void getGenusID() {
        TaxonomyNode node = TaxonomyNode.builder().withIntervals(intervals).build();
        assertEquals(0, node.getGenusID());

        node = TaxonomyNode.builder().withIntervals(intervals).withGenusID(2).build();
        assertEquals(2, node.getGenusID());
    }

    @Test
    void getName() {
        TaxonomyNode node = TaxonomyNode.builder().withIntervals(intervals).build();
        assertEquals("", node.getName());

        node = TaxonomyNode.builder().withIntervals(intervals).withName("Node").build();
        assertEquals("Node", node.getName());
    }
}
