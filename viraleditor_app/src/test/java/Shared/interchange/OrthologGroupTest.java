package Shared.interchange;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

import java.util.Arrays;
import java.util.List;

import org.junit.jupiter.api.Test;

class OrthologGroupTest {
    private static final List<Interval> intervals = Arrays.asList(new Interval(false, 1, 2));

    @Test
    void getType() {
        assertEquals(FeatureType.ORTHOLOG_GROUP, OrthologGroup.builder().withIntervals(intervals).build().getType());
    }

    @Test
    void getFamilyID() {
        OrthologGroup defaultValue = OrthologGroup.builder().withIntervals(intervals).build();
        assertEquals(0, defaultValue.getFamilyID());

        OrthologGroup setValue = OrthologGroup.builder().withIntervals(intervals).withFamilyID(1).build();
        assertEquals(1, setValue.getFamilyID());

        OrthologGroup.Builder builder = OrthologGroup.builder().withIntervals(intervals);
        assertThrows(IllegalArgumentException.class, () -> builder.withFamilyID(-2));
    }

    @Test
    void getOrthologGroupID() {
        OrthologGroup defaultValue = OrthologGroup.builder().withIntervals(intervals).build();
        assertEquals(0, defaultValue.getOrthologGroupID());

        OrthologGroup setValue = OrthologGroup.builder().withIntervals(intervals).withOrthologGroupID(1).build();
        assertEquals(1, setValue.getOrthologGroupID());

        OrthologGroup.Builder builder = OrthologGroup.builder().withIntervals(intervals);
        assertThrows(IllegalArgumentException.class, () -> builder.withOrthologGroupID(-2));
    }

    @Test
    void getVocFunction() {
        OrthologGroup defaultValue = OrthologGroup.builder().withIntervals(intervals).build();
        assertEquals("", defaultValue.getVocFunction());

        OrthologGroup setValue = OrthologGroup.builder().withIntervals(intervals).withVocFunction("Function").build();
        assertEquals("Function", setValue.getVocFunction());
    }

    @Test
    void getName() {
        OrthologGroup defaultValue = OrthologGroup.builder().withIntervals(intervals).build();
        assertEquals("", defaultValue.getName());

        OrthologGroup setValue = OrthologGroup.builder().withIntervals(intervals).withName("Name").build();
        assertEquals("Name", setValue.getName());
    }
}
