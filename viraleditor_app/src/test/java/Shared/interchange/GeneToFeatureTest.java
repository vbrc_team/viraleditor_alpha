package Shared.interchange;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.biojava.nbio.core.sequence.compound.NucleotideCompound;
import org.biojava.nbio.core.sequence.features.DBReferenceInfo;
import org.biojava.nbio.core.sequence.features.FeatureInterface;
import org.biojava.nbio.core.sequence.features.Qualifier;
import org.biojava.nbio.core.sequence.features.TextFeature;
import org.biojava.nbio.core.sequence.location.SimpleLocation;
import org.biojava.nbio.core.sequence.location.template.AbstractLocation;
import org.biojava.nbio.core.sequence.template.AbstractSequence;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

class GeneToFeatureTest {
    private GeneToFeature convert;
    private List<Interval> defaultIntervals;
    private FeatureInterface<AbstractSequence<NucleotideCompound>, NucleotideCompound> feature;
    private AbstractLocation location;

    private static final String GENE = "gene";

    @BeforeEach
    void before() {
        convert = new GeneToFeature();
        defaultIntervals = new ArrayList<>();
        defaultIntervals.add(new Interval(false, 1, 3));
        location = new SimpleLocation(1, 3);
        feature = null;
    }

    @Test
    void fromSourceWithDbXrefGI() {
        Map<DbXRef, String> dbXrefs = new HashMap<>();
        dbXrefs.put(DbXRef.GI, "12345");
        Gene dbxref = Gene.builder().withIntervals(defaultIntervals).withDbXref(dbXrefs).build();
        feature = convert.to(dbxref);

        List<Qualifier> dbxrefs = feature.getQualifiers().get("db_xref");
        assertEquals(1, dbxrefs.size());

        DBReferenceInfo dbInfo = (DBReferenceInfo) dbxrefs.get(0);
        assertEquals("GI", dbInfo.getDatabase());
        assertEquals("12345", dbInfo.getId());
    }

    @Test
    void withDBXrefGI() {
        feature = new TextFeature<>(GENE, "", "", "");
        feature.addQualifier("db_xref", new DBReferenceInfo("GI", "GI ID"));
        feature.setLocation(location);

        Gene gene = convert.from(feature);
        Map<DbXRef, String> dbxrefs = gene.getDbXref();

        assertTrue(dbxrefs.containsKey(DbXRef.GI));
        assertEquals("GI ID", dbxrefs.get(DbXRef.GI));
    }

    @Test
    void fromSourceWithDbXrefGeneID() {
        Map<DbXRef, String> dbXrefs = new HashMap<>();
        dbXrefs.put(DbXRef.GENE_ID, "12345");
        Gene dbxref = Gene.builder().withIntervals(defaultIntervals).withDbXref(dbXrefs).build();
        feature = convert.to(dbxref);

        List<Qualifier> dbxrefs = feature.getQualifiers().get("db_xref");
        assertEquals(1, dbxrefs.size());

        DBReferenceInfo dbInfo = (DBReferenceInfo) dbxrefs.get(0);
        assertEquals("GeneID", dbInfo.getDatabase());
        assertEquals("12345", dbInfo.getId());
    }

    @Test
    void withDBXrefGeneID() {
        feature = new TextFeature<>(GENE, "", "", "");
        feature.addQualifier("db_xref", new DBReferenceInfo("GeneID", "Gene ID"));
        feature.setLocation(location);

        Gene gene = convert.from(feature);
        Map<DbXRef, String> dbxrefs = gene.getDbXref();

        assertTrue(dbxrefs.containsKey(DbXRef.GENE_ID));
        assertEquals("Gene ID", dbxrefs.get(DbXRef.GENE_ID));
    }

    @Test
    void fromSourceWithDbXrefGeneTaxon() {
        Map<DbXRef, String> dbXrefs = new HashMap<>();
        dbXrefs.put(DbXRef.TAXON, "12345");
        Gene dbxref = Gene.builder().withIntervals(defaultIntervals).withDbXref(dbXrefs).build();
        feature = convert.to(dbxref);

        List<Qualifier> dbxrefs = feature.getQualifiers().get("db_xref");
        assertEquals(1, dbxrefs.size());

        DBReferenceInfo dbInfo = (DBReferenceInfo) dbxrefs.get(0);
        assertEquals("taxon", dbInfo.getDatabase());
        assertEquals("12345", dbInfo.getId());
    }

    @Test
    void fromGeneWithAllele() {
        Gene gene = Gene.builder().withIntervals(defaultIntervals).withAllele("Allele").build();

        checkSizeAndValue(convert.to(gene), "allele", 1, "Allele");
    }

    @Test
    void withAlleleFromFeature() {
        Gene gene = convert.from(createFeature("allele", "Allele", location));

        assertEquals(1, gene.getIntervals().size());
        assertEquals("Allele", gene.getAllele());
    }

    @Test
    void fromGeneWithGene() {
        Gene gene = Gene.builder().withIntervals(defaultIntervals).withGene("Gene").build();

        checkSizeAndValue(convert.to(gene), "gene", 1, "Gene");
    }

    @Test
    void withGeneFromGene() {
        Gene gene = convert.from(createFeature("gene", "Gene", location));

        assertEquals(1, gene.getIntervals().size());
        assertEquals("Gene", gene.getGene());
    }

    @Test
    void fromGeneWithGeneSynonym() {
        Gene gene = Gene.builder().withIntervals(defaultIntervals).withGeneSynonym("Synonym").build();

        checkSizeAndValue(convert.to(gene), "gene_synonym", 1, "Synonym");
    }

    @Test
    void withGeneSynonymFromGene() {
        Gene gene = convert.from(createFeature("gene_synonym", "Gene synonym", location));

        assertEquals(1, gene.getIntervals().size());
        assertEquals("Gene synonym", gene.getGeneSynonym());
    }

    @Test
    void fromGeneWithLocusTag() {
        Gene gene = Gene.builder().withIntervals(defaultIntervals).withLocusTag("Locus Tag").build();

        checkSizeAndValue(convert.to(gene), "locus_tag", 1, "Locus Tag");
    }

    @Test
    void withLocusTagFromGene() {
        Gene gene = convert.from(createFeature("locus_tag", "Locus Tag", location));

        assertEquals(1, gene.getIntervals().size());
        assertEquals("Locus Tag", gene.getLocusTag());
    }

    @Test
    void fromGeneWithNote() {
        Gene gene = Gene.builder().withIntervals(defaultIntervals).withNote("Note").build();

        checkSizeAndValue(convert.to(gene), "note", 1, "Note");
    }

    @Test
    void withNoteFromGene() {
        Gene gene = convert.from(createFeature("note", "Note", location));

        assertEquals(1, gene.getIntervals().size());
        assertEquals("Note", gene.getNote());
    }

    @Test
    void fromGeneWithOldLocusTag() {
        Gene gene = Gene.builder().withIntervals(defaultIntervals).withOldLocusTag("Old Tag").build();

        checkSizeAndValue(convert.to(gene), "old_locus_tag", 1, "Old Tag");
    }

    @Test
    void withOldLocusTagFromGene() {
        Gene gene = convert.from(createFeature("old_locus_tag", "Old locus tag", location));

        assertEquals(1, gene.getIntervals().size());
        assertEquals("Old locus tag", gene.getOldLocusTag());
    }

    @Test
    void fromGeneWithProduct() {
        Gene gene = Gene.builder().withIntervals(defaultIntervals).withProduct("Product").build();

        checkSizeAndValue(convert.to(gene), "product", 1, "Product");
    }

    @Test
    void withProductFromGene() {
        Gene gene = convert.from(createFeature("product", "Product", location));

        assertEquals(1, gene.getIntervals().size());
        assertEquals("Product", gene.getProduct());
    }

    @Test
    void fromGeneWithPseudogene() {
        Gene gene = Gene.builder().withIntervals(defaultIntervals).withPseudogene("Pseudo Gene").build();

        checkSizeAndValue(convert.to(gene), "pseudogene", 1, "Pseudo Gene");
    }

    @Test
    void withPseudogeneFromGene() {
        Gene gene = convert.from(createFeature("pseudogene", "Pseudogene", location));

        assertEquals(1, gene.getIntervals().size());
        assertEquals("Pseudogene", gene.getPseudogene());
    }

    @Test
    void fromGeneWithStandardName() {
        Gene gene = Gene.builder().withIntervals(defaultIntervals).withStandardName("Standard Name").build();

        checkSizeAndValue(convert.to(gene), "standard_name", 1, "Standard Name");
    }

    @Test
    void withStandardNameFromGene() {
        Gene gene = convert.from(createFeature("standard_name", "Standard Name", location));

        assertEquals(1, gene.getIntervals().size());
        assertEquals("Standard Name", gene.getStandardName());
    }

    private void checkSizeAndValue(FeatureInterface<?, ?> feature, String key, int size, String value) {
        List<Qualifier> qualifiers = feature.getQualifiers().get(key);
        assertEquals(size, qualifiers.size());
        assertEquals(value, qualifiers.get(0).getValue());
    }

    private FeatureInterface<AbstractSequence<NucleotideCompound>, NucleotideCompound> createFeature(String key,
            String value, AbstractLocation location) {
        FeatureInterface<AbstractSequence<NucleotideCompound>, NucleotideCompound> feature;

        feature = new TextFeature<>(GENE, "", "", "");
        feature.addQualifier(key, new Qualifier(key, value));
        feature.setLocation(location);

        return feature;
    }

}
