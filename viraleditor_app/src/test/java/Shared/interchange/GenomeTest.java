package Shared.interchange;

import static org.junit.jupiter.api.Assertions.assertDoesNotThrow;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

import java.util.Arrays;
import java.util.List;

import org.junit.jupiter.api.Test;

class GenomeTest {
    private static final List<Interval> intervals = Arrays.asList(new Interval(false, 1, 2));

    @Test
    void defaultValues() {
        Genome genome = Genome.builder().withIntervals(intervals).build();
        String emptyString = "";
        String defaultData = "1970-01-01T00:00";
        assertEquals(0, genome.getFamilyID());
        assertEquals(0, genome.getGenomeID());
        assertEquals(emptyString, genome.getOrganismName());
        assertEquals(emptyString, genome.getOrganismAbbrevation());
        assertEquals(emptyString, genome.getStrainName());
        assertEquals(emptyString, genome.getStrainAbbrevation());
        assertEquals(emptyString, genome.getMoleculeType());
        assertEquals(emptyString, genome.getDescription());
        assertEquals(emptyString, genome.getNotes());
        assertEquals(defaultData, genome.getLastModified());
        assertEquals(defaultData, genome.getInsertedOn());
    }

    @Test
    void getOrganismName() {
        Genome genome = Genome.builder().withIntervals(intervals).withOrganismName("Name").build();
        assertEquals("Name", genome.getOrganismName());
    }

    @Test
    void getOrganismAbbrevation() {
        Genome genome = Genome.builder().withIntervals(intervals).withOrganismAbbrevation("Abbrevation").build();
        assertEquals("Abbrevation", genome.getOrganismAbbrevation());
    }

    @Test
    void getStrainName() {
        Genome genome = Genome.builder().withIntervals(intervals).withStrainName("Name").build();
        assertEquals("Name", genome.getStrainName());
    }

    @Test
    void getStrainAbbrevation() {
        Genome genome = Genome.builder().withIntervals(intervals).withStrainAbbrevation("Abbrevation").build();
        assertEquals("Abbrevation", genome.getStrainAbbrevation());
    }

    @Test
    void getMoleculeType() {
        Genome genome = Genome.builder().withIntervals(intervals).withMoleculeType("FeatureType").build();
        assertEquals("FeatureType", genome.getMoleculeType());
    }

    @Test
    void getDescription() {
        Genome genome = Genome.builder().withIntervals(intervals).withDescription("Description").build();
        assertEquals("Description", genome.getDescription());
    }

    @Test
    void getNotes() {
        Genome genome = Genome.builder().withIntervals(intervals).withNotes("Notes").build();
        assertEquals("Notes", genome.getNotes());
    }

    @Test
    void lastModifiedInvalidFormatYYYYMMDD() {
        Genome.Builder builderTwo = Genome.builder().withIntervals(intervals);
        assertThrows(IllegalArgumentException.class, () -> builderTwo.withLastModified("1970-01-01"));
    }

    @Test
    void lastModifiedInvalidFormatEmptryString() {
        Genome genome = Genome.builder().withIntervals(intervals).withLastModified("1970-01-01T00:00").build();
        assertEquals("1970-01-01T00:00", genome.getLastModified());
    }

    @Test
    void getLastModified() {
        Genome genome = Genome.builder().withIntervals(intervals).withLastModified("2020-01-01T00:00").build();
        assertEquals("2020-01-01T00:00", genome.getLastModified());
    }

    @Test
    void insertedOnInvalidFormatYYYYMMDD() {
        Genome.Builder builder = Genome.builder().withIntervals(intervals);
        assertThrows(IllegalArgumentException.class, () -> builder.withInsertedOn("1970-01-01"));
    }

    @Test
    void insertedOnInvalidFormatEmptryString() {
        Genome.Builder builder = Genome.builder().withIntervals(intervals);
        assertThrows(IllegalArgumentException.class, () -> builder.withInsertedOn(""));
    }

    @Test
    void insertedOnValidFormat() {
        assertDoesNotThrow(() -> Genome.builder().withIntervals(intervals).withInsertedOn("1970-01-01T00:00"));
    }

    @Test
    void getInsertedOn() {
        Genome genome = Genome.builder().withIntervals(intervals).withInsertedOn("1970-01-01T00:00").build();
        assertEquals("1970-01-01T00:00", genome.getInsertedOn());

        genome = Genome.builder().withIntervals(intervals).withInsertedOn("2020-01-01T00:00").build();
        assertEquals("2020-01-01T00:00", genome.getInsertedOn());
    }

    @Test
    void getType() {
        Genome genome = Genome.builder().withIntervals(intervals).build();
        assertEquals(FeatureType.GENOME, genome.getType());
    }

}
