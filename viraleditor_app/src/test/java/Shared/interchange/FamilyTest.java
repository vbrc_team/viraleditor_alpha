package Shared.interchange;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

import java.util.Arrays;
import java.util.List;

import org.junit.jupiter.api.Test;

class FamilyTest {
    private static final List<Interval> intervals = Arrays.asList(new Interval(false, 1, 2));

    @Test
    void familyIDInvalidValue() {
        Family.Builder builder = Family.builder().withIntervals(intervals);
        assertThrows(IllegalArgumentException.class, () -> builder.withFamilyID(-2));
    }

    @Test
    void getFamilyID() {
        Family family = Family.builder().withIntervals(intervals).build();
        assertEquals(0, family.getFamilyID());

        family = Family.builder().withIntervals(intervals).withFamilyID(2).build();
        assertEquals(2, family.getFamilyID());
    }

    @Test
    void getFamilyName() {
        Family family = Family.builder().withIntervals(intervals).build();
        assertEquals("", family.getFamilyName());

        family = Family.builder().withIntervals(intervals).withFamilyName("Family").build();
        assertEquals("Family", family.getFamilyName());
    }

}
