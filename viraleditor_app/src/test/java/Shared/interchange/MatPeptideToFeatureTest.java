package Shared.interchange;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.biojava.nbio.core.sequence.compound.NucleotideCompound;
import org.biojava.nbio.core.sequence.features.DBReferenceInfo;
import org.biojava.nbio.core.sequence.features.FeatureInterface;
import org.biojava.nbio.core.sequence.features.Qualifier;
import org.biojava.nbio.core.sequence.features.TextFeature;
import org.biojava.nbio.core.sequence.location.SimpleLocation;
import org.biojava.nbio.core.sequence.location.template.AbstractLocation;
import org.biojava.nbio.core.sequence.template.AbstractSequence;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

class MatPeptideToFeatureTest {
    private MatPeptideToFeature convert;
    private List<Interval> defaultIntervals;
    private FeatureInterface<AbstractSequence<NucleotideCompound>, NucleotideCompound> feature;
    private AbstractLocation location;

    private static final String MAT_PEPTIDE = "mat_peptide";

    @BeforeEach
    void before() {
        convert = new MatPeptideToFeature();
        defaultIntervals = new ArrayList<>();
        defaultIntervals.add(new Interval(false, 1, 3));
        location = new SimpleLocation(1, 3);
        feature = null;
    }

    @Test
    void fromMatPeptideWithAllele() {
        MatPeptide matPeptide = MatPeptide.builder().withIntervals(defaultIntervals).withAllele("Allele").build();

        checkSizeAndValue(convert.to(matPeptide), "allele", 1, "Allele");
    }

    @Test
    void withAlleleFromFeature() {
        feature = new TextFeature<>(MAT_PEPTIDE, "", "", "");
        feature.addQualifier("allele", new Qualifier("allele", "Allele"));
        feature.setLocation(location);

        MatPeptide matPeptide = convert.from(feature);

        assertEquals(1, matPeptide.getIntervals().size());
        assertEquals("Allele", matPeptide.getAllele());
    }

    @Test
    void fromMatPeptideWithDbXrefGI() {
        Map<DbXRef, String> dbXrefs = new HashMap<>();
        dbXrefs.put(DbXRef.GI, "12345");
        MatPeptide dbxref = MatPeptide.builder().withIntervals(defaultIntervals).withDbXref(dbXrefs).build();
        feature = convert.to(dbxref);

        List<Qualifier> dbxrefs = feature.getQualifiers().get("db_xref");
        assertEquals(1, dbxrefs.size());

        DBReferenceInfo dbInfo = (DBReferenceInfo) dbxrefs.get(0);
        assertEquals("GI", dbInfo.getDatabase());
        assertEquals("12345", dbInfo.getId());
    }

    @Test
    void withDBXrefGI() {
        feature = new TextFeature<>(MAT_PEPTIDE, "", "", "");
        feature.addQualifier("db_xref", new DBReferenceInfo("GI", "GI ID"));
        feature.setLocation(location);

        MatPeptide matPeptide = convert.from(feature);
        Map<DbXRef, String> dbxrefs = matPeptide.getDbXref();

        assertTrue(dbxrefs.containsKey(DbXRef.GI));
        assertEquals("GI ID", dbxrefs.get(DbXRef.GI));
    }

    @Test
    void fromMatPeptideWithDbXrefGeneID() {
        Map<DbXRef, String> dbXrefs = new HashMap<>();
        dbXrefs.put(DbXRef.GENE_ID, "12345");
        MatPeptide dbxref = MatPeptide.builder().withIntervals(defaultIntervals).withDbXref(dbXrefs).build();
        feature = convert.to(dbxref);

        List<Qualifier> dbxrefs = feature.getQualifiers().get("db_xref");
        assertEquals(1, dbxrefs.size());

        DBReferenceInfo dbInfo = (DBReferenceInfo) dbxrefs.get(0);
        assertEquals("GeneID", dbInfo.getDatabase());
        assertEquals("12345", dbInfo.getId());
    }

    @Test
    void withDBXrefGeneID() {
        feature = new TextFeature<>(MAT_PEPTIDE, "", "", "");
        feature.addQualifier("db_xref", new DBReferenceInfo("GeneID", "Gene ID"));
        feature.setLocation(location);

        MatPeptide matPeptide = convert.from(feature);
        Map<DbXRef, String> dbxrefs = matPeptide.getDbXref();

        assertTrue(dbxrefs.containsKey(DbXRef.GENE_ID));
        assertEquals("Gene ID", dbxrefs.get(DbXRef.GENE_ID));
    }

    @Test
    void fromMatPeptideWithDbXrefGeneTaxon() {
        Map<DbXRef, String> dbXrefs = new HashMap<>();
        dbXrefs.put(DbXRef.TAXON, "12345");
        MatPeptide dbxref = MatPeptide.builder().withIntervals(defaultIntervals).withDbXref(dbXrefs).build();
        feature = convert.to(dbxref);

        List<Qualifier> dbxrefs = feature.getQualifiers().get("db_xref");
        assertEquals(1, dbxrefs.size());

        DBReferenceInfo dbInfo = (DBReferenceInfo) dbxrefs.get(0);
        assertEquals("taxon", dbInfo.getDatabase());
        assertEquals("12345", dbInfo.getId());
    }

    @Test
    void withDBXrefTaxon() {
        feature = new TextFeature<>(MAT_PEPTIDE, "", "", "");
        feature.addQualifier("db_xref", new DBReferenceInfo("taxon", "Taxon"));
        feature.setLocation(location);

        MatPeptide matPeptide = convert.from(feature);
        Map<DbXRef, String> dbxrefs = matPeptide.getDbXref();

        assertTrue(dbxrefs.containsKey(DbXRef.TAXON));
        assertEquals("Taxon", dbxrefs.get(DbXRef.TAXON));
    }

    @Test
    void fromMatPeptideWithEcNumber() {
        MatPeptide matPeptide = MatPeptide.builder().withIntervals(defaultIntervals).withEcNumber("Number").build();

        checkSizeAndValue(convert.to(matPeptide), "EC_number", 1, "Number");
    }

    @Test
    void withEcNumberFromMatPeptide() {
        MatPeptide matPeptide = convert.from(createFeature("EC_number", "EC Number", location));

        assertEquals(1, matPeptide.getIntervals().size());
        assertEquals("EC Number", matPeptide.getEcNumber());
    }

    @Test
    void fromMatPeptideWithGene() {
        MatPeptide matPeptide = MatPeptide.builder().withIntervals(defaultIntervals).withGene("Gene").build();

        checkSizeAndValue(convert.to(matPeptide), "gene", 1, "Gene");
    }

    @Test
    void withGeneFromMatPeptide() {
        MatPeptide matPeptide = convert.from(createFeature("gene", "Gene", location));

        assertEquals(1, matPeptide.getIntervals().size());
        assertEquals("Gene", matPeptide.getGene());
    }

    @Test
    void fromMatPeptideWithGeneSynonym() {
        MatPeptide matPeptide = MatPeptide.builder().withIntervals(defaultIntervals).withGeneSynonym("Synonym").build();

        checkSizeAndValue(convert.to(matPeptide), "gene_synonym", 1, "Synonym");
    }

    @Test
    void withGeneSynonymFromMatPeptide() {
        MatPeptide matPeptide = convert.from(createFeature("gene_synonym", "Gene synonym", location));

        assertEquals(1, matPeptide.getIntervals().size());
        assertEquals("Gene synonym", matPeptide.getGeneSynonym());
    }

    @Test
    void fromMatPeptideWithLocusTag() {
        MatPeptide matPeptide = MatPeptide.builder().withIntervals(defaultIntervals).withLocusTag("Locus Tag").build();

        checkSizeAndValue(convert.to(matPeptide), "locus_tag", 1, "Locus Tag");
    }

    @Test
    void withLocusTagFromMatPeptide() {
        MatPeptide matPeptide = convert.from(createFeature("locus_tag", "Locus Tag", location));

        assertEquals(1, matPeptide.getIntervals().size());
        assertEquals("Locus Tag", matPeptide.getLocusTag());
    }

    @Test
    void fromMatPeptideWithNote() {
        MatPeptide matPeptide = MatPeptide.builder().withIntervals(defaultIntervals).withNote("Note").build();

        checkSizeAndValue(convert.to(matPeptide), "note", 1, "Note");
    }

    @Test
    void withNoteFromMatPeptide() {
        MatPeptide matPeptide = convert.from(createFeature("note", "Note", location));

        assertEquals(1, matPeptide.getIntervals().size());
        assertEquals("Note", matPeptide.getNote());
    }

    @Test
    void fromMatPeptideWithOldLocusTag() {
        MatPeptide matPeptide = MatPeptide.builder().withIntervals(defaultIntervals).withOldLocusTag("Old Tag").build();

        checkSizeAndValue(convert.to(matPeptide), "old_locus_tag", 1, "Old Tag");
    }

    @Test
    void withOldLocusTagFromMatPeptide() {
        MatPeptide matPeptide = convert.from(createFeature("old_locus_tag", "Old locus tag", location));

        assertEquals(1, matPeptide.getIntervals().size());
        assertEquals("Old locus tag", matPeptide.getOldLocusTag());
    }

    @Test
    void fromMatPeptideWithProduct() {
        MatPeptide matPeptide = MatPeptide.builder().withIntervals(defaultIntervals).withProduct("Product").build();

        checkSizeAndValue(convert.to(matPeptide), "product", 1, "Product");
    }

    @Test
    void withProductFromMatPeptide() {
        MatPeptide matPeptide = convert.from(createFeature("product", "Product", location));

        assertEquals(1, matPeptide.getIntervals().size());
        assertEquals("Product", matPeptide.getProduct());
    }

    @Test
    void fromMatPeptideWithPseudogene() {
        MatPeptide matPeptide = MatPeptide.builder()
                                          .withIntervals(defaultIntervals)
                                          .withPseudogene("Pseudo Gene")
                                          .build();

        checkSizeAndValue(convert.to(matPeptide), "pseudogene", 1, "Pseudo Gene");
    }

    @Test
    void withPseudogeneFromMatPeptide() {
        MatPeptide matPeptide = convert.from(createFeature("pseudogene", "Pseudogene", location));

        assertEquals(1, matPeptide.getIntervals().size());
        assertEquals("Pseudogene", matPeptide.getPseudogene());
    }

    @Test
    void fromMatPeptideWithStandardName() {
        MatPeptide matPeptide = MatPeptide.builder()
                                          .withIntervals(defaultIntervals)
                                          .withStandardName("Standard Name")
                                          .build();

        checkSizeAndValue(convert.to(matPeptide), "standard_name", 1, "Standard Name");
    }

    @Test
    void withStandardNameFromMatPeptide() {
        MatPeptide matPeptide = convert.from(createFeature("standard_name", "Standard Name", location));

        assertEquals(1, matPeptide.getIntervals().size());
        assertEquals("Standard Name", matPeptide.getStandardName());
    }

    private void checkSizeAndValue(FeatureInterface<?, ?> feature, String key, int size, String value) {
        List<Qualifier> qualifiers = feature.getQualifiers().get(key);
        assertEquals(size, qualifiers.size());
        assertEquals(value, qualifiers.get(0).getValue());
    }

    private FeatureInterface<AbstractSequence<NucleotideCompound>, NucleotideCompound> createFeature(String key,
            String value, AbstractLocation location) {
        FeatureInterface<AbstractSequence<NucleotideCompound>, NucleotideCompound> feature;

        feature = new TextFeature<>(MAT_PEPTIDE, "", "", "");
        feature.addQualifier(key, new Qualifier(key, value));
        feature.setLocation(location);

        return feature;
    }
}
