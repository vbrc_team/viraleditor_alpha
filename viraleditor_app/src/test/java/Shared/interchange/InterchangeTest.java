package Shared.interchange;

import static org.junit.jupiter.api.Assertions.assertEquals;

import java.util.ArrayList;
import java.util.List;

import org.junit.jupiter.api.Test;

public class InterchangeTest {
    private static final StringToNucleotides toNucleotides = new StringToNucleotides();
    @Test
    void defaultInterchange() {
        Interchange interchange = Interchange.builder().build();

        assertEquals(0, interchange.getReferences().size());
    }

    @Test
    void getResources() {
        Reference r0 = Reference.builder().build();
        Reference r1 = Reference.builder().withOrigin(new NucleotideSequence(toNucleotides.to("ACGTN"))).build();
        List<Reference> refs = new ArrayList<Reference>();
        refs.add(r0);
        refs.add(r1);

        Interchange interchange = Interchange.builder().withReferences(refs).build();

        assertEquals(2, interchange.getReferences().size());
        assertEquals(r0, interchange.getReferences().get(0));
        assertEquals(r1, interchange.getReferences().get(1));
    }
}
