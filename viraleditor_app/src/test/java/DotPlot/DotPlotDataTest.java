package DotPlot;

import java.util.*;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import DotPlot.DotPlotData;

import static org.junit.jupiter.api.Assertions.*;

public class DotPlotDataTest {

	DotPlotData dpdata;
	String pathToResource = "src/test/resources";
	
	@BeforeEach
    void init() {
		dpdata = new DotPlotData();
    }
	
	@Test
	void testGetType() {
		assertEquals(0, dpdata.getType());
	}
	
	@Test
	void testGetBuffer() {
		assertEquals(0, (dpdata.getBuffer()).length);
	}
	
	@Test
	void testGetWidth() {
		assertEquals(0, dpdata.getWidth());	
	}
	
	@Test
	void testGetHeight() {
		assertEquals(0, dpdata.getHeight());	
	}
	
	@Test
	void testSetPixelFactor() {
		dpdata.setPixelFactor(1);
		assertEquals(1, dpdata.m_pixelFac);	
	}
	
	@Test
	void testGetPixelFactor() {
		dpdata.setPixelFactor(2);
		assertEquals(2, dpdata.getPixelFactor());	
	}
	
	@Test
	void testSetVertFeatures() {
		List<Integer> l = new ArrayList<>();
		l.add(1);
		dpdata.setVertFeatures(l);
		assertEquals(l, dpdata.m_vFeatures);
	}
	
	@Test
	void testGetVertFeatures() {
		List<Integer> l = new ArrayList<>();
		l.add(1);
		dpdata.setVertFeatures(l);
		assertEquals(l, dpdata.getVertFeatures());
	}
	
	@Test
	void testSetHorizFeatures() {
		List<Integer> l = new ArrayList<>();
		l.add(1);
		dpdata.setHorizFeatures(l);
		assertEquals(l, dpdata.m_hFeatures);
	}
	
	@Test
	void testGetHorizFeatures() {
		List<Integer> l = new ArrayList<>();
		l.add(1);
		dpdata.setHorizFeatures(l);
		assertEquals(l, dpdata.getHorizFeatures());
	}
	
	@Test
	void testAddHorizSequence() {
		dpdata.addHorizSequence("aaa", "bbb");
		assertAll("HorizSeq",
				() -> assertEquals("aaa", dpdata.m_hSeqNames.get(0)),
				() -> assertEquals("BBB", dpdata.m_hSequences.get(0)),
				() -> assertEquals(3, dpdata.m_hLength)
				);	
	}
	
	@Test
	void testAddVerSequence() {
		dpdata.addVertSequence("aaa", "bbb");
		assertAll("VertSeq",
				() -> assertEquals("aaa", dpdata.m_vSeqNames.get(0)),
				() -> assertEquals("BBB", dpdata.m_vSequences.get(0)),
				() -> assertEquals(3, dpdata.m_vLength)
				);	
	}
	
	@Test
	void testGetHorizNames() {
		dpdata.addHorizSequence("aaa", "bbb");
		assertEquals("aaa", dpdata.getHorizNames().get(0));
	}
	
	@Test
	void testGetVertNames() {
		dpdata.addVertSequence("aaa", "bbb");
		assertEquals("aaa", dpdata.getVertNames().get(0));
	}
	
	@Test
	void testGetHorizSequences() {
		dpdata.addHorizSequence("aaa", "bbb");
		assertEquals("BBB", dpdata.getHorizSequences().get(0));
	}
	
	@Test
	void testGetVertSequences() {
		dpdata.addVertSequence("aaa", "bbb");
		assertEquals("BBB", dpdata.getVertSequences().get(0));
	}
	
	@Test
	void testSetVSeqStart() {
		dpdata.setVSeqStart(1);
		assertEquals(1, dpdata.m_vStart);
	}

	@Test
	void testGetVSeqStart() {
		dpdata.setVSeqStart(1);
		assertEquals(1, dpdata.getVSeqStart());
	}
	
	@Test
	void testSetHSeqStart() {
		dpdata.setHSeqStart(1);
		assertEquals(1, dpdata.m_hStart);
	}
	
	@Test
	void testGetHSeqStart() {
		dpdata.setHSeqStart(1);
		assertEquals(1, dpdata.getHSeqStart());
	}
	
	@Test
	void testGetHSeqLength() {
		dpdata.addHorizSequence("aaa", "bbb");		
		assertEquals(3, dpdata.getHSeqLength());
	}
	
	@Test
	void testGetVSeqLength() {
		dpdata.addVertSequence("aaa", "bbb");		
		assertEquals(3, dpdata.getVSeqLength());
	}
	
	@Test
	void testSetWindowLength() {
		dpdata.setWindowLength(3);
		assertEquals(3, dpdata.m_windowLen);
	}
	
	@Test
	void testGetWindowLength() {
		dpdata.setWindowLength(3);
		assertEquals(3, dpdata.getWindowLength());
	}
	
	@Test
	void testSetZoom() {
		dpdata.setZoom(3);
		assertEquals(3, dpdata.m_zoom);
	}
	
	@Test
	void testGetZoom() {
		dpdata.setZoom(3);
		assertEquals(3, dpdata.getZoom());
	}
	
	@Test
	void testSetMatrixKeys() {
		String[] keys = {"a","b"};
		dpdata.setMatrixKeys(keys);
		assertEquals(keys, dpdata.m_matrixKeys);
	}
	
	@Test
	void testGetMatrixKeys() {
		String[] keys = {"a","b"};
		dpdata.setMatrixKeys(keys);
		assertEquals(keys, dpdata.getMatrixKeys());
	}
	
	@Test
	void testSetMatrixName() {
		dpdata.setMatrixName("abc");
		assertEquals("abc", dpdata.m_matrixName);
	}
	
	@Test
	void testGetMatrixName() {
		dpdata.setMatrixName("abc");
		assertEquals("abc", dpdata.getMatrixName());
	}
	
	@Test
	void testSetMatrix() {
		int[] matrix = {1,1,1};
		dpdata.setMatrix(matrix);
		assertEquals(matrix, dpdata.m_matrix);
	}
	
	@Test
	void testGetMatrix() {
		int[] matrix = {1,1,1};
		dpdata.setMatrix(matrix);
		assertEquals(matrix, dpdata.getMatrix());
	}
}
