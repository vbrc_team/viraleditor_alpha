package DotPlot;

import java.util.*;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import DotPlot.DotPlotPanel;

import static org.junit.jupiter.api.Assertions.*;

public class FeatureTest {
	Feature f;
	
	@BeforeEach
	void init() {
		f = new Feature(0,100,1,"abc");
	}
	
	@Test
	void testGetName() {
		assertEquals("abc", f.getName());
	}
	
	@Test
	void testGetStart() {
		assertEquals(0, f.getStart());
	}
	
	@Test
	void testGetStop() {
		assertEquals(100, f.getStop());
	}
	
	@Test
	void testGetStrand() {
		assertEquals(1, f.getStrand());
	}
}
