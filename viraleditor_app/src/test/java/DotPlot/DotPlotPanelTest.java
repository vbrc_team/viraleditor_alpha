package DotPlot;

import java.util.*;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import DotPlot.DotPlotPanel;

import static org.junit.jupiter.api.Assertions.*;

public class DotPlotPanelTest {
	DotPlotPanel dpp;
	DotPlotData data;
	
	@BeforeEach
	void init() {
		dpp = new DotPlotPanel();
		data = new DotPlotData();
	}
	
	@Test
	void testSetDataGetData() {
		dpp.setData(data);
		assertEquals(data, dpp.getData());
	}
	
}
