package ViralEditor;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.BeforeEach;
import static org.junit.jupiter.api.Assertions.*;

import java.util.Arrays;
import java.util.List;
import java.util.ArrayList;

public class ViralEditorComposerTest {

	ViralEditor composer;
	private List<String> databaseList = new ArrayList<String>();
	
	@BeforeEach
	void init() {
		composer = new ViralEditor();
	}
	
//	// TODO: deal with exception properly
//	@Test
//	void setDatabases() throws InterruptedException {
//		
//		// populate databaseList
//		databaseList.add("database1");
//		databaseList.add("database2");
//		
//		composer.setDatabaseList(databaseList);
//		composer.chooseDatabase("database1");
//		assertEquals("database1", composer.databaseChosen());
//		
//		composer.chooseDatabase("database2");
//		assertEquals("database2", composer.databaseChosen());
//	}
//	
//	// TODO: deal with exception properly
//	@Test
//	void chooseTool() throws InterruptedException {
//		composer.chooseTool("test");
//		assertEquals("test", composer.toolChosen());
//	}
	
	
	// Testing of these methods is complicated by the JavaFX stuff, and a lack of observable behavior.
	// Might come back and figure this out later. -Mattias
	
	/* Valid tools: (in ViralEditor.toolNames.java)
	 * ALIGNMENT("Alignment", "Please select the sequences you would like, either from the database or upload from your machine."),
     * DBANALYSIS("Database Analysis", "Please select the viruses you would like to view.\nClick on the buttons to view more detailed information about them."),
     * DOTPLOT("DotPlot", "Please pick a maximum of two genomes.\nFasta files can include more than one sequence to create a 2x2 plot."),
     * ANNOTATION("Annotation", "Please select a reference genome (from database or local file) and a target genome from your local machine."),
     * GENOMEMAP("Genome Map", "Please select the sequences you would like, either from the database or upload from your machine."),
     * GRAPHDNA("Graph DNA", "Please select the sequences you would like, either from the database or upload from your machine."),
     * CODONSTATS("Codon Statistics", "Please select the viruses you would like to view.\n");
	 *	
	 * use toolnames.tool.__________.name to get string
	*/
	
	// TODO: setHelpInfo
	// Expected behavior: 
	// Upon clicking the associated help button on the launch screen of ViralEditor,
	// opens a window with the description of the selected tool.
	// Invalid selection: 
	// prints an error message to the console. Should not normally happen, since there
	// are only buttons for valid tools.
	
	// @Test 
	// void setHelpInfo() {
	// 	composer.setHelpInfo(toolNames.tool.ALIGNMENT.name);
	// }
	
	// TODO: openTool
	// Expected behavior:
	// Clicking the "open" button for a tool launches a new Viral investigator window for the selected tool
	// and virus database.
	// Invalid selection: 
	// prints an error message to the console. Should not normally happen, since there
	// are only buttons for valid tools.
	
	// @Test
	// void openTool() {
	// 	composer.openTool(toolNames.tool.ALIGNMENT.name);
	// }
	
	
}
