import connexion
import six
import uuid
import tempfile
import subprocess
from pathlib import Path
from flask import abort

from openapi_server.models.created import Created
from openapi_server.models.request import Request
from openapi_server.models.sequence import Sequence
from openapi_server.models.status import Status
from openapi_server import util


def create_request():
    """Add a new request



    :param request:
    :type request: dict | bytes

    :rtype: Created
    """
    if connexion.request.is_json:
        request = Request.from_dict(connexion.request.get_json())

    requestID = uuid.uuid4()
    inPath = Path(tempfile.gettempdir(), "requests", str(requestID) + ".in")
    while inPath.exists():
        requestID = uuid.uuid4()
        inPath = Path(tempfile.gettempdir(), "requests", str(requestID) + ".in")

    inPath.touch()
    with inPath.open(mode="wt") as f:
        for seq in request.sequences:
            f.write(">" + str(seq.name) + "\n")
            f.write(str(seq.sequence) + "\n")

    outPath = Path(tempfile.gettempdir(), "requests", str(requestID) + ".out")
    jobPath = Path(tempfile.gettempdir(), "requests", str(requestID) + ".job")
    if request.tool == "mafft":
        alignment_command = mafft_command(inPath, outPath)
    elif request.tool == "clustalo":
        alignment_command = clustalo_command(inPath, outPath)
    elif request.tool == "muscle":
        alignment_command = muscle_command(inPath, outPath)

    parts = [
        "sbatch",
        "--quiet",
        "--job-name",
        str(requestID),
        "--output",
        str(jobPath),
        "runBatch.sh",
        alignment_command,
    ]
    subprocess.run(" ".join(parts), shell=True)

    return Created(requestID)


def get_request_by_id(id_):
    """Get status of request



    :param id:
    :type id: str

    :rtype: Status
    """

    processResult = subprocess.run(
        [
            "squeue --noheader",
            "--format",
            '"%j %.2t"',
            "--noheader",
            "--name",
            str(id_),
        ],
        shell=True,
        capture_output=True,
    )
    out = processResult.stdout
    if len(out) > 0:
        return Status(id_, "Running")
    elif Path(tempfile.gettempdir(), "requests", str(id_) + ".out").exists():
        return Status(id_, "Complete")
    else:
        abort(404)


def get_result_by_id(id_):
    """Get result of request



    :param id:
    :type id: str

    :rtype: List[Sequence]
    """

    path = Path(tempfile.gettempdir(), "requests", id_ + ".out")
    result = subprocess.run(
        ["squeue --noheader", "--format", '"%j %.2t"', "--noheader", "--name", id_],
        shell=True,
        capture_output=True,
    )
    if path.exists() and not len(result.stdout):
        sequences = []
        with path.open(mode="rt") as f:
            line = f.readline().rstrip()
            while not line == "":
                if line.startswith(">"):
                    name = line[1:]
                    line = f.readline().rstrip()
                    sequence_lines = []
                    while not line == "" and not line.startswith(">"):
                        sequence_lines.append(line)
                        line = f.readline().rstrip()
                    sequences.append(Sequence(name, "".join(sequence_lines)))
        return sequences
    else:
        abort(404)


def mafft_command(in_path, out_path):
    return "'mafft --auto " + str(in_path) + " > " + str(out_path) + "'"


def clustalo_command(in_path, out_path):
    return (
        "'clustalo --auto --infile " + str(in_path) + " --outfile " + str(out_path) + "'"
    )


def muscle_command(in_path, out_path):
    return "'muscle -quiet -in " + str(in_path) + " -out " + str(out_path) + "'"
