package ca.virology.veserver;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;

@SpringBootApplication /*(scanBasePackages = {"ca.virology.veserver", "ca.virology.veserver.family", 
											"ca.virology.veserver.gene", "ca.virology.veserver.geneOrf",
											"ca.virology.veserver.genome", "ca.virology.veserver.orthologGroup"}) */
@EnableAutoConfiguration
@EnableJpaRepositories(basePackages = {"ca.virology.veserver.family", 
										"ca.virology.veserver.gene", "ca.virology.veserver.geneOrf",
										"ca.virology.veserver.genome", "ca.virology.veserver.orthologGroup",
										"ca.virology.veserver.taxonomyNode"})

@EntityScan(basePackages = {"ca.virology.veserver", "ca.virology.veserver.family", 
							"ca.virology.veserver.gene", "ca.virology.veserver.geneOrf",
							"ca.virology.veserver.genome", "ca.virology.veserver.orthologGroup",
							"ca.virology.veserver.taxonomyNode"})

//@ComponentScan("ca.virology.veserver")
public class ViralEditorServerApplication {

	public static void main(String[] args) {
		SpringApplication.run(ViralEditorServerApplication.class, args);
	}
	
}
