package ca.virology.veserver.genome;

import java.util.List;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;

import ca.virology.veserver.genome.Genome;

public interface GenomeRepository extends CrudRepository<Genome, Integer> {
	
	Iterable<Genome> findByFamilyIdIn(List<Integer> family_id);
	
	@Query("SELECT g FROM Genome g WHERE genome_id IN :genome_ids")
	Iterable<Genome> epicTestQuery(@Param("genome_ids") List<Integer> genome_ids);
	
	@Query("SELECT g FROM Genome g WHERE family_id IN :family_ids")
	Iterable<Genome> epicFamilyTestQuery(@Param("family_ids") List<Integer> family_ids);
	
	// Get genome with sequence data by ID
	@Query("SELECT g FROM Genome g WHERE genome_id IN :genome_ids")
	Iterable<Genome> getSequenceById(@Param("genome_ids") List<Integer> genome_ids);
	
	/*
	// TODO: add mapping step because this doesn't actually return a Genome object I guess
	// Get genome without sequence data by ID
	@Query("SELECT g.genomeId, g.familyId, g.isolateId, g.centerId, g.taxnodeId, g.genomeName,"
			+ " g.strainName, g.genomeAbbr, g.strainAbbr, g.segmentName, g.description, "
			+ "g.notes, g.moleculeType, g.isComplete, g.isPublished, g.seqVersion,"
			+ "g.insertedOn, g.lastModified, g.gbAccession, g.gbVersion, g.gbGi,"
			+ "g.gbTaxid, g.gbDate "
			+ "FROM Genome g WHERE genome_id IN :genome_ids")
	Iterable<Genome> getMetaById(@Param("genome_ids") List<Integer> genome_ids);
	*/
}
