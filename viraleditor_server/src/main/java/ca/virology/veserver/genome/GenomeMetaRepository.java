package ca.virology.veserver.genome;

import java.util.List;

import org.springframework.data.repository.CrudRepository;

import ca.virology.veserver.genome.GenomeMeta;

public interface GenomeMetaRepository extends CrudRepository<GenomeMeta, Integer> {

	Iterable<GenomeMeta> findByFamilyIdIn(List<Integer> family_id);
	
}
