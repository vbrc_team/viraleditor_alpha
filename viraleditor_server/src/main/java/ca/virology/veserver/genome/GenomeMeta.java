package ca.virology.veserver.genome;

import javax.persistence.Entity;
import javax.persistence.Column;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.TemporalType;
import javax.persistence.Temporal;

import java.time.LocalDateTime;
import java.time.ZoneId;
import java.util.Date;

/**
 * Entity model for genomes without sequence data.
 * Required because I can't read docs.
 * @author Mattias Park
 *
 */
@Entity
@Table(name = "genome")
public class GenomeMeta {

	@Id
	@Column(name = "genome_id")
	private Integer genomeId;
	
	@Column(name = "family_id")
	private Integer familyId;
	
	@Column(name = "isolate_id")
	private Integer isolateId;
	
	@Column(name = "center_id")
	private Integer centerId;
	
	@Column(name = "taxnode_id")
	private Integer taxnodeId;
	
	// Names + textual info
	@Column(name = "genome_name")
	private String genomeName;
	
	@Column(name = "strain_name")
	private String strainName;
	
	@Column(name = "genome_abbr")
	private String genomeAbbr;
	
	@Column(name = "strain_abbr")
	private String strainAbbr;
	
	@Column(name = "segment_name")
	private String segmentName;
	
	@Column(name = "description")
	private String description;
	
	@Column(name = "notes")
	private String notes;
	
	// Genome stuff
	@Column(name = "molecule_type")
	private String moleculeType;
	
	// Entry metadata
	@Column(name = "is_complete")
	private char isComplete;
	
	@Column(name = "is_published")
	private char isPublished;
	
	@Column(name = "seq_version")
	private Integer seqVersion;
	
	// Timestamps
	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "inserted_on")
	private Date insertedOn;
	
	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "last_modified")
	private Date lastModified;
	
	// Genbank stuff
	@Column(name = "gb_accession")
	private String gbAccession;
	
	@Column(name = "gb_version")
	private Integer gbVersion;
	
	@Column(name = "gb_gi")
	private Integer gbGi;
	
	@Column(name = "gb_taxid")
	private Integer gbTaxid;
	
	@Column(name = "gb_date")
	private String gbDate;

	// Getters
	public Integer getGenomeId() {
		return genomeId;
	}

	public Integer getFamilyId() {
		return familyId;
	}

	public Integer getIsolateId() {
		return isolateId;
	}

	public Integer getCenterId() {
		return centerId;
	}

	public Integer getTaxnodeId() {
		return taxnodeId;
	}

	public String getGenomeName() {
		return genomeName;
	}

	public String getStrainName() {
		return strainName;
	}

	public String getGenomeAbbr() {
		return genomeAbbr;
	}

	public String getStrainAbbr() {
		return strainAbbr;
	}

	public String getSegmentName() {
		return segmentName;
	}

	public String getDescription() {
		return description;
	}

	public String getNotes() {
		return notes;
	}

	public String getMoleculeType() {
		return moleculeType;
	}

	public char getIsComplete() {
		return isComplete;
	}

	public char getIsPublished() {
		return isPublished;
	}

	public Integer getSeqVersion() {
		return seqVersion;
	}

	public LocalDateTime getInsertedOn() {
		return insertedOn.toInstant().atZone(ZoneId.of("GMT-8")).toLocalDateTime();
	}

	public LocalDateTime getLastModified() {
		return lastModified.toInstant().atZone(ZoneId.of("GMT-8")).toLocalDateTime();
	}
	
	public String getGbAccession() {
		return gbAccession;
	}

	public Integer getGbVersion() {
		return gbVersion;
	}

	public Integer getGbGi() {
		return gbGi;
	}

	public Integer getGbTaxid() {
		return gbTaxid;
	}

	public String getGbDate() {
		return gbDate;
	}

}
