package ca.virology.veserver;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.server.ResponseStatusException;

import com.fasterxml.jackson.core.JsonProcessingException;

import Shared.interchange.*;
import ca.virology.veserver.family.Family;
import ca.virology.veserver.family.FamilyRepository;
import ca.virology.veserver.gene.Gene;
import ca.virology.veserver.gene.GeneMeta;
import ca.virology.veserver.gene.GeneMetaRepository;
import ca.virology.veserver.gene.GeneRepository;
import ca.virology.veserver.geneOrf.GeneOrf;
import ca.virology.veserver.geneOrf.GeneOrfRepository;
import ca.virology.veserver.genome.Genome;
import ca.virology.veserver.genome.GenomeMeta;
import ca.virology.veserver.genome.GenomeMetaRepository;
import ca.virology.veserver.genome.GenomeRepository;
import ca.virology.veserver.orthologGroup.OrthologGroup;
import ca.virology.veserver.orthologGroup.OrthologGroupRepository;
import ca.virology.veserver.taxonomyNode.TaxonomyNode;
import ca.virology.veserver.taxonomyNode.TaxonomyNodeRepository;


@Controller
@RequestMapping(path="")
public class MainController {
    private static final StringToNucleotides toNucleotides = new StringToNucleotides();
    private static final StringToAminoAcids toAminoAcids = new StringToAminoAcids();
	
	@Autowired
	private FamilyRepository familyRepository;
	
	@Autowired
	private OrthologGroupRepository orthologGroupRepository;
	
	@Autowired
	private GeneOrfRepository geneOrfRepository;
	
	@Autowired
	private TaxonomyNodeRepository taxonomyNodeRepository;
	
	// Genes/genomes with sequences
	@Autowired
	private GenomeRepository genomeRepository;
	@Autowired
	private GeneRepository geneRepository;
	
	// Genes/genomes without sequences
	@Autowired
	private GenomeMetaRepository genomeMetaRepository;
	@Autowired
	private GeneMetaRepository geneMetaRepository;

	// TODO:
	// Add robust exception handling
	// Testing
	
	// FAMILIES
	
	// TODO: Docstring, rename
	@GetMapping(path="/families")
	public @ResponseBody Interchange getAllFamilies(
		   @RequestParam(required = false, value = "ids") String[] family_ids_param) throws JsonProcessingException {
		
		ArrayList<Family> families = new ArrayList<> ();
		
		if (family_ids_param != null) {
			// Get by IDs
			for (Family f : familyRepository.findAllById(parseIntegers(family_ids_param))) {
				families.add(f);
			}
		} else {
			// Get all
			for (Family f : familyRepository.findAll()) {
				families.add(f);
			}
		}
		
		// List of references for building the interchange
		List<Shared.interchange.Reference> references = new ArrayList<> ();
		
		// Interchange conversion
		for (Family f : families) {
			
			// Create interval
			// TODO: find out if this needs actual values
			List<Shared.interchange.Interval> intervals = new ArrayList<>();
			intervals.add(new Interval(false, 1, 2));
			
			// Build interchange family feature
			List<Shared.interchange.Feature> family_features = new ArrayList<> ();

			Shared.interchange.Family.Builder family_builder = Shared.interchange.Family.builder();
			family_builder.withIntervals(intervals);
			if (f.getFamilyId() != null) family_builder.withFamilyID(f.getFamilyId());
			if (f.getFamilyName() != null) family_builder.withFamilyName(f.getFamilyName());
			
			Shared.interchange.Feature family = family_builder.build();
			family_features.add(family);
			
			// Add that feature to a new reference, add to the list
			Shared.interchange.Reference ref = Shared.interchange.Reference.builder()
					.withFeatures(family_features)
					.build();
			references.add(ref);
		}
		
		// Put the references in the interchange object
		Shared.interchange.Interchange return_interchange = Shared.interchange.Interchange.builder()
				.withReferences(references)
				.build();
		
		return return_interchange;
	}
	
	// GENOMES
	/**
	 * Endpoint for fetching genomes. If no parameters are provided, 
	 * will return metadata for all genomes in database (no sequence data).
	 * genome_ids takes precedence over family_ids- i.e. if both genome ids
	 * and family ids are provided in the URL, will fetch by genome ids.
	 * <p>
	 * Takes 3 parameters in URL-
	 * <p> 
	 * genome_ids_param (ids):
	 * The genome_id(s) of the desired genome, seperated by commas. 
	 * Takes precedence over the family id (i.e. family will be 
	 * ignored if genome ids are provided.)
	 * <p>
	 * family_ids_param (family-ids):
	 * The family_id(s) of desired families, seperated by commas.
	 * All genomes with one of these family_ids will be returned.
	 * <p> 
	 * get_sequence_param (sequence):
	 * "true" to get sequences, otherwise assumed false.
	 * Whether or not sequence data should be returned. Should only be used 
	 * with genomes fetched by ID to prevent extremely large responses.
	 * 
	 * @param genome_ids_param
	 * @param family_ids_param
	 * @param get_sequence_param
	 * @return JSON string representation of an Interchange object.
	 * @throws JsonProcessingException 
	 */
	@GetMapping(path="/genomes")
	public @ResponseBody Interchange getGenomes(
			@RequestParam (value = "ids", required = false) String[] genome_ids_param,
			@RequestParam(required = false, value = "family-ids") String[] family_ids_param,
			@RequestParam (value = "sequence", required = false, defaultValue = "false") String get_sequence_param) throws JsonProcessingException {
		
		List<Integer> genome_ids = new ArrayList<> ();
		List<Integer> family_ids = new ArrayList<> ();
		
		List<Genome> genomes = new ArrayList<> ();
		List<GenomeMeta> genomes_meta = new ArrayList<> ();
		
		// Get genomes by genome_id
		if (genome_ids_param != null) {
			genome_ids = parseIntegers(genome_ids_param);
			
			if (get_sequence_param.equals("true")) {
				// Get sequences
				for (Genome g : genomeRepository.findAllById(genome_ids)) {
					genomes.add(g);
				}
				
				return genomeToInterchange(genomes);
				
			} else {
				// No sequences
				for (GenomeMeta g: genomeMetaRepository.findAllById(genome_ids)) {
					genomes_meta.add(g);
				}
				
				return genomeMetaToInterchange(genomes_meta);
			}
			
		} else if (family_ids_param != null) {
			// Get genomes by family_id
			
			family_ids = parseIntegers(family_ids_param);
			for (GenomeMeta g: genomeMetaRepository.findByFamilyIdIn(family_ids)) {
				genomes_meta.add(g);
			}
			
			return genomeMetaToInterchange(genomes_meta);
			
		} else {
			// get all genomes (without sequences)
			for (GenomeMeta g: genomeMetaRepository.findAll()) {
				genomes_meta.add(g);
			}
			
			return genomeMetaToInterchange(genomes_meta);
		}
		
	}
	
	/**
	 * Helper method to convert a list of genomes (including sequence data) 
	 * to a JSON interchange string.
	 * @param genomes
	 * @return
	 * @throws JsonProcessingException 
	 */
	private Interchange genomeToInterchange(List<Genome> genomes) throws JsonProcessingException {
		// List of references for building the interchange
		List<Shared.interchange.Reference> references = new ArrayList<> ();
		
		// Do interchange conversion
		for (Genome g : genomes) {
			
			// Create interval
			// TODO: figure out complement situation
			List<Shared.interchange.Interval> intervals = new ArrayList<>();
				
			// Get sequence length and create interval
			Integer low = 1;
			Integer high = 2;
			if (g.getGenomeSeq() != null) high = g.getGenomeSeq().length(); // The null check is a failsafe, but this should always happen!
			intervals.add(new Interval(false, low, high));
			
			// Create genome feature
			List<Shared.interchange.Feature> genome_features = new ArrayList<> ();
			
			Shared.interchange.Genome.Builder genome_builder = Shared.interchange.Genome.builder();
			genome_builder.withIntervals(intervals);
			if (g.getGenomeId() != null) genome_builder.withGenomeID(g.getGenomeId());
			if (g.getFamilyId() != null) genome_builder.withFamilyID(g.getFamilyId());
			if (g.getTaxnodeId() != null) genome_builder.withTaxnodeID(g.getTaxnodeId());
			if (g.getGenomeName() != null) genome_builder.withOrganismName(g.getGenomeName());
			if (g.getGenomeAbbr() != null) genome_builder.withOrganismAbbrevation(g.getGenomeAbbr());
			if (g.getNotes() != null) genome_builder.withNotes(g.getNotes());
			if (g.getDescription() != null) genome_builder.withDescription(g.getDescription());
			if (g.getMoleculeType() != null) genome_builder.withMoleculeType(g.getMoleculeType());
			if (g.getLastModified() != null) genome_builder.withLastModified(g.getLastModified().toString());
			if (g.getInsertedOn() != null) genome_builder.withInsertedOn(g.getInsertedOn().toString());
			
			Shared.interchange.Feature genome = genome_builder.build();
			genome_features.add(genome);

			// Build reference
			Shared.interchange.Reference.Builder ref_builder = Shared.interchange.Reference.builder();
			ref_builder.withFeatures(genome_features);
            if (g.getGenomeSeq() != null)
                ref_builder.withOrigin(new Shared.interchange.NucleotideSequence(toNucleotides.to(g.getGenomeSeq())));
			
			Shared.interchange.Reference ref = ref_builder.build();
			references.add(ref);
		}
		
		// Put the references in the interchange object
		Shared.interchange.Interchange return_interchange = Shared.interchange.Interchange.builder()
				.withReferences(references)
				.build();
		
		return return_interchange;
	}
	
	/**
	 * Helper method to convert a list of genomes (without sequence data)
	 * to a JSON interchange string.
	 * @param genomes
	 * @return
	 * @throws JsonProcessingException 
	 */
	private Interchange genomeMetaToInterchange(List<GenomeMeta> genomes) throws JsonProcessingException {
		// List of references for building the interchange
		List<Shared.interchange.Reference> references = new ArrayList<> ();
		
		// Do interchange conversion
		for (GenomeMeta g : genomes) {
			
			// Create interval
			// We have no sequence data, so cannot create placeholder interval
			List<Shared.interchange.Interval> intervals = new ArrayList<>();
			intervals.add(new Interval(false, 1, 2));
				
			// Create genome feature
			List<Shared.interchange.Feature> genome_features = new ArrayList<> ();

			Shared.interchange.Genome.Builder genome_builder = Shared.interchange.Genome.builder();
			genome_builder.withIntervals(intervals);
			if (g.getGenomeId() != null) genome_builder.withGenomeID(g.getGenomeId());
			if (g.getFamilyId() != null) genome_builder.withFamilyID(g.getFamilyId());
			if (g.getTaxnodeId() != null) genome_builder.withTaxnodeID(g.getTaxnodeId());
			if (g.getGenomeName() != null) genome_builder.withOrganismName(g.getGenomeName());
			if (g.getGenomeAbbr() != null) genome_builder.withOrganismAbbrevation(g.getGenomeAbbr());
			if (g.getNotes() != null) genome_builder.withNotes(g.getNotes());
			if (g.getDescription() != null) genome_builder.withDescription(g.getDescription());
			if (g.getMoleculeType() != null) genome_builder.withMoleculeType(g.getMoleculeType());
			if (g.getLastModified() != null) genome_builder.withLastModified(g.getLastModified().toString());
			if (g.getInsertedOn() != null) genome_builder.withInsertedOn(g.getInsertedOn().toString());
			
			Shared.interchange.Feature genome = genome_builder.build();
			genome_features.add(genome);
			
			// Create reference
			Shared.interchange.Reference ref = Shared.interchange.Reference.builder()
					.withFeatures(genome_features)
					.build();
			references.add(ref);

		}
		
		// Put the references in the interchange object
		Shared.interchange.Interchange return_interchange = Shared.interchange.Interchange.builder()
				.withReferences(references)
				.build();
		
		return return_interchange;
	}
	
	
	// GENES
	/**
	 * Endpoint for fetching genes. If no parameters are provided, will return
	 * an empty interchange. Gene ids take precedence over genome ids- i.e. if both 
	 * are provided in the URL, will fetch by gene ids.
	 * <p>
	 * Returns a JSON string representation of an Interchange object. Fetching by 
	 * gene ids will return an Interchange with one reference per gene, 
	 * while fetching by genome id will return an Interchange with one reference
	 * containing multiple gene/annotation features.
	 * <p>
	 * Takes 3 parameters in URL- 
	 * <p>
	 * gene_ids_param (ids): 
	 * The gene_ids of the desired genes.
	 * <p>
	 * genome_ids_param (genome-ids):
	 * the genome_id of the desired genes.
	 * <p>
	 * get_sequence_param (sequence):
	 * Whether or not to return sequence data.
	 * 
	 * @param gene_ids_param
	 * @param genome_ids_param
	 * @param get_sequence_param
	 * @return JSON string representation of an Interchange object.
	 * @throws JsonProcessingException
	 */
	// TODO: better handling of no-parameter endpoint
	@GetMapping(path="/genes")
	public @ResponseBody Interchange getGenes(@RequestParam (value = "ids", required = false) String[] gene_ids_param,
			@RequestParam(required = false, value = "genome-ids") String[] genome_ids_param,
			@RequestParam (value = "sequence", required = false, defaultValue = "false") String get_sequence_param) throws JsonProcessingException {
				
				List<Integer> gene_ids = new ArrayList<> ();
				List<Integer> genome_ids = new ArrayList<> ();
				
				List<Gene> genes = new ArrayList<> ();
				List<GeneMeta> genes_meta = new ArrayList<> ();
				
				List<Shared.interchange.Feature> features = new ArrayList<>();
				List<Shared.interchange.Reference> references;
				
				if (gene_ids_param != null) {
					// Get genes by gene_id.
					// Puts each in its own reference and stores in references list.
					
					gene_ids = parseIntegers(gene_ids_param);
			
					if (get_sequence_param.equals("true")) {
						for (Gene g : geneRepository.findAllById(gene_ids)) {
							genes.add(g);
						}
						features = genesToFeatures(genes);
					} else {
						for (GeneMeta g : geneMetaRepository.findAllById(gene_ids)) {
							genes_meta.add(g);
						}						
						features = genesMetaToFeatures(genes_meta);
					}
					
					references = new ArrayList<> ();
					
					// Now, make a reference for each feature (gene)
					for (Shared.interchange.Feature f : features) {
						
						// Reference builder takes a list of features, so must do this
						List<Shared.interchange.Feature> single_feature = new ArrayList<> ();
						single_feature.add(f);
						
						Shared.interchange.Reference ref = Shared.interchange.Reference.builder()
								.withFeatures(single_feature)
								.build();
						references.add(ref);
					}
					
				} else if (genome_ids_param != null) {
					// Get genes by genome_id.
					// Puts them all in one reference and stores in references list.
					// TODO: handling multiple genomes?
					
					genome_ids = parseIntegers(genome_ids_param);
					
					if (get_sequence_param.equals("true")) {
						
						references = new ArrayList<> ();
						
						for (Integer i : genome_ids) {
							// Create a reference w/ n features for each genome
							// Clear genes list for each iteration
							genes.clear();	
							
							for (Gene g : geneRepository.findByGenomeId(i)) {
								genes.add(g);
							}
							features = genesToFeatures(genes);
							
							Shared.interchange.Reference ref = Shared.interchange.Reference.builder()
									.withFeatures(features)
									.build();
							references.add(ref);
						}
						
					} else { // No sequence data
						
						references = new ArrayList<> ();
						
						for (Integer i : genome_ids) {
							// Create a reference w/ n features for each genome
							// Clear genes list for each iteration
							
							genes_meta.clear();
							for (GeneMeta g : geneMetaRepository.findByGenomeId(i)) {
								genes_meta.add(g);
							}
							features = genesMetaToFeatures(genes_meta);
						
							Shared.interchange.Reference ref = Shared.interchange.Reference.builder()
									.withFeatures(features)
									.build();
							references.add(ref);
						}
					
					}
					
				} else {
					// No query data in params- return empty string
					// TODO: Make this return a 404 or something
					
					references = new ArrayList<> ();
				}
				
				// If an actual query was made (by gene_id or genome_id) then 
				// return an interchange using the references created above
				Shared.interchange.Interchange return_interchange = Shared.interchange.Interchange.builder()
						.withReferences(references)
						.build();
				
				return return_interchange;
			}

	/**
	 * Helper method for getGenes.
	 * Takes a list of Gene objects, and returns a list of annotation Feature objects.
	 * @param genes
	 * @return List of interchange Feature objects
	 */
	private List<Shared.interchange.Feature> genesToFeatures(List<Gene> genes) {
		
		List<GeneOrf> gene_orfs = new ArrayList<>();
		List<Integer> gene_ids = new ArrayList<>();
		List<Shared.interchange.Feature> features = new ArrayList<>();
		
		// Get gene ids (In the case of querying by Genome, we have a list 
		// of genes but must fetch their IDs instead of getting them as param)
		for (Gene g : genes) {
			gene_ids.add(g.getGeneId());
		}
		
		// Query ORFs (In a single query so it's not slow)
		for (GeneOrf o: geneOrfRepository.findAllById(gene_ids)) {
			gene_orfs.add(o);
		}

		// Now, we can make the features
		for (Gene g : genes) {
			
			// Another list for orfs, since we need them in the correct order for each gene
			List<GeneOrf> curr_gene_orfs = new ArrayList<> ();
			
			// Get the ORF(s) with the corresponding gene ID and put them in that list
			for (GeneOrf o : gene_orfs) {
				if (o.getGeneId().equals(g.getGeneId())) {
					curr_gene_orfs.add(o);
				}
			}
			
			// Sort the current gene orfs list to be in ascending order
			Collections.sort(curr_gene_orfs);
			
			// Intervals for current gene
			List<Shared.interchange.Interval> intervals = new ArrayList<> ();
			
			// Make the interval(s)
			for (GeneOrf o : curr_gene_orfs) {
				
				Integer start = 1;
				Integer stop = 2;
				
				if (o.getStart() != null) start = o.getStart();
				if (o.getStop() != null) stop = o.getStop();
				
				if (g.getStrand() == '+') {
					intervals.add(new Shared.interchange.Interval(false, start, stop));
				} else if (g.getStrand()  == '-') {
					intervals.add(new Shared.interchange.Interval(true, stop, start));
				} else {
					// Badly formatted strand data
					// I don't think there are currently any in the db
					// TODO: Handle this
				}
			}
			
			if (intervals.size() < 1) {
				// No intervals! should probably throw something
				intervals.add(new Shared.interchange.Interval(false, 1, 2));
			}
			
			// Now we can build the feature
			Shared.interchange.Annotation.Builder gene_builder = Shared.interchange.Annotation.builder();
			gene_builder.withIntervals(intervals);
			if (g.getGeneId() != null) gene_builder.withGeneID(g.getGeneId());
			if (g.getFamilyId() != null) gene_builder.withFamilyID(g.getFamilyId());
			if (g.getGenomeId() != null) gene_builder.withGenomeID(g.getGenomeId());
			if (g.getGene_abbr() != null) gene_builder.withGeneAbbreviation(g.getGene_abbr());
			gene_builder.withStrand(g.getStrand()); // This one is a char
			if (g.getAnnotator() != null) gene_builder.withAnnotator(g.getAnnotator());
			if (g.getAnnoSource() != null) gene_builder.withAnnoSource(g.getAnnoSource());
			if (g.getMoleculeType() != null) gene_builder.withMoleculeType(g.getMoleculeType());
			if (g.getFeatureName() != null) gene_builder.withFeatureName(g.getFeatureName());
			if (g.getLastModified() != null) gene_builder.withLastModified(g.getLastModified().toString());
			if (g.getInsertedOn() != null) gene_builder.withInsertedOn(g.getInsertedOn().toString());
			if (g.getDescription() != null) gene_builder.withDescription(g.getDescription());
			if (g.getNotes() != null) gene_builder.withNotes(g.getNotes());
			if (g.getGbName() != null) gene_builder.withGenBankName(g.getGbName());
			if (g.getGbGeneGi() != null) gene_builder.withGenBankGeneGI(g.getGbGeneGi());
			if (g.getGbGeneVersion() != null) gene_builder.withGenBankGeneVersion(g.getGbGeneVersion());
			if (g.getOrthologGroupId() != null) gene_builder.withOrthologGroupID(g.getOrthologGroupId());
			if (g.getOrthologEvid() != null) gene_builder.withOrthologEvidence(g.getOrthologEvid());

			if (g.getUpstreamSeq() != null) gene_builder.withUpstreamSequence
                (new NucleotideSequence(toNucleotides.to(g.getUpstreamSeq())));
			if (g.getNtSeq() != null) gene_builder.withNTSequence
                (new NucleotideSequence(toNucleotides.to(g.getNtSeq())));
			if (g.getAaSeq() != null) gene_builder.withAASequence
                (new AminoAcidSequence(toAminoAcids.to(g.getAaSeq())));
					
			Shared.interchange.Annotation gene = gene_builder.build();
			features.add(gene);
		}
		return features;
	}
	
	/**
	 * Helper method for getGenes.
	 * Takes a list of GeneMeta objects, and returns a list of annotation Feature objects.
	 * Identical to genesToFeatures, but without sequence data.
	 * @param genes
	 * @return List of interchange Feature objects
	 */
	private List<Shared.interchange.Feature> genesMetaToFeatures(List<GeneMeta> genes) {
		List<GeneOrf> gene_orfs = new ArrayList<>();
		List<Integer> gene_ids = new ArrayList<>();
		List<Shared.interchange.Feature> features = new ArrayList<>();
		
		// Get gene ids (In the case of querying by Genome, we have a list 
		// of genes but must fetch their IDs instead of getting them as param)
		for (GeneMeta g : genes) {
			gene_ids.add(g.getGeneId());
		}
		
		// Query ORFs (In a single query so it's not slow)
		for (GeneOrf o: geneOrfRepository.findAllById(gene_ids)) {
			gene_orfs.add(o);
		}

		// Now, we can make the features
		for (GeneMeta g : genes) {
			
			// Another list for orfs, since we need them in the correct order for each gene
			List<GeneOrf> curr_gene_orfs = new ArrayList<> ();
			
			// Get the ORF(s) with the corresponding gene ID and put them in that list
			for (GeneOrf o : gene_orfs) {
				if (o.getGeneId().equals(g.getGeneId())) {
					curr_gene_orfs.add(o);
				}
			}
			
			// Sort the current gene orfs list to be in ascending order
			Collections.sort(curr_gene_orfs);
			
			// Intervals for current gene
			List<Shared.interchange.Interval> intervals = new ArrayList<> ();
			
			// Make the interval(s)
			for (GeneOrf o : curr_gene_orfs) {
				
				Integer start = 1;
				Integer stop = 2;
				
				if (o.getStart() != null) start = o.getStart();
				if (o.getStop() != null) stop = o.getStop();
				
				if (g.getStrand() == '+') {
					intervals.add(new Shared.interchange.Interval(false, start, stop));
				} else if (g.getStrand()  == '-') {
					intervals.add(new Shared.interchange.Interval(true, stop, start));
				} else {
					// Badly formatted strand data- i dunno what to do
				}
			}
			
			if (intervals.size() < 1) {
				// No intervals! should probably throw something
				intervals.add(new Shared.interchange.Interval(false, 1, 2));
			}
			
			// Now we can build the feature
			Shared.interchange.Annotation.Builder gene_builder = Shared.interchange.Annotation.builder();
			gene_builder.withIntervals(intervals);
			if (g.getGeneId() != null) gene_builder.withGeneID(g.getGeneId());
			if (g.getFamilyId() != null) gene_builder.withFamilyID(g.getFamilyId());
			if (g.getGenomeId() != null) gene_builder.withGenomeID(g.getGenomeId());
			if (g.getGene_abbr() != null) gene_builder.withGeneAbbreviation(g.getGene_abbr());
			gene_builder.withStrand(g.getStrand()); // This one is a char
			if (g.getAnnotator() != null) gene_builder.withAnnotator(g.getAnnotator());
			if (g.getAnnoSource() != null) gene_builder.withAnnoSource(g.getAnnoSource());
			if (g.getMoleculeType() != null) gene_builder.withMoleculeType(g.getMoleculeType());
			if (g.getFeatureName() != null) gene_builder.withFeatureName(g.getFeatureName());
			if (g.getLastModified() != null) gene_builder.withLastModified(g.getLastModified().toString());
			if (g.getInsertedOn() != null) gene_builder.withInsertedOn(g.getInsertedOn().toString());
			if (g.getDescription() != null) gene_builder.withDescription(g.getDescription());
			if (g.getNotes() != null) gene_builder.withNotes(g.getNotes());
			if (g.getGbName() != null) gene_builder.withGenBankName(g.getGbName());
			if (g.getGbGeneGi() != null) gene_builder.withGenBankGeneGI(g.getGbGeneGi());
			if (g.getGbGeneVersion() != null) gene_builder.withGenBankGeneVersion(g.getGbGeneVersion());
			if (g.getOrthologGroupId() != null) gene_builder.withOrthologGroupID(g.getOrthologGroupId());
			if (g.getOrthologEvid() != null) gene_builder.withOrthologEvidence(g.getOrthologEvid());
			
			Shared.interchange.Annotation gene = gene_builder.build();
			features.add(gene);
		}
		return features;
	}
	
	/**
	 * 
	 * @param ortholog_group_ids_param
	 * @return
	 * @throws JsonProcessingException
	 */
	@GetMapping(path="/ortholog-groups")
	public @ResponseBody Interchange getOrthologGroups(
		   @RequestParam(required = false, value = "ids") String[] ortholog_group_ids_param ) throws JsonProcessingException {
	
		ArrayList<OrthologGroup> ortholog_groups = new ArrayList<> ();
		
		if (ortholog_group_ids_param != null) {
			// Get by IDs
			for (OrthologGroup g : orthologGroupRepository.findAllById(parseIntegers(ortholog_group_ids_param))) {
				ortholog_groups.add(g);
			}
		} else {
			// Get all
			for (OrthologGroup g : orthologGroupRepository.findAll()) {
				ortholog_groups.add(g);
			}
		}
		
		// List of references for building the interchange
		List<Shared.interchange.Reference> references = new ArrayList<> ();
		
		// Interchange conversion
		for (OrthologGroup og : ortholog_groups) {
			
			// Create interval (placeholder)
			List<Shared.interchange.Interval> intervals = new ArrayList<>();
			intervals.add(new Interval(false, 1, 2));
			
			// Build interchange ortholog group feature
			List<Shared.interchange.Feature> ortholog_features = new ArrayList<> ();
			
			Shared.interchange.OrthologGroup.Builder ortholog_builder = Shared.interchange.OrthologGroup.builder();	
			ortholog_builder.withIntervals(intervals);
			if (og.getFamilyId() != null) ortholog_builder.withFamilyID(og.getFamilyId());
			if (og.getOrthologGroupId() != null) ortholog_builder.withOrthologGroupID(og.getOrthologGroupId());
			if (og.getName() != null) ortholog_builder.withName(og.getName());
			if (og.getRefAbbrs() != null) ortholog_builder.withRefAbbrs(og.getRefAbbrs());
			if (og.getVocAbbr() != null) ortholog_builder.withVocAbbr(og.getVocAbbr());
			if (og.getVocFunction() != null) ortholog_builder.withVocFunction(og.getVocFunction());
					
			Shared.interchange.Feature ortholog = ortholog_builder.build();
			ortholog_features.add(ortholog);
			
			// Add that feature to a new reference, add to the list
			Shared.interchange.Reference ref = Shared.interchange.Reference.builder()
					.withFeatures(ortholog_features)
					.build();
			references.add(ref);
		}
		
		// Put the references in the interchange object
		Shared.interchange.Interchange return_interchange = Shared.interchange.Interchange.builder()
				.withReferences(references)
				.build();
		
		// Make and return JSON string
		return return_interchange;
		
	}
	
	/**
	 * 
	 * @return
	 */
	@GetMapping(path="/taxnodes")
	public @ResponseBody Interchange getTaxnodes() {
		
		List<TaxonomyNode> taxnodes = new ArrayList<> ();
		
		for (TaxonomyNode tn: taxonomyNodeRepository.findAll()) {
			taxnodes.add(tn);
		}
		
		// List of references for building the interchange
		List<Shared.interchange.Reference> references = new ArrayList<> ();
		
		for (TaxonomyNode tn : taxnodes) {
			// Create intervals (placeholders, not actually used in taxnodes)
			List<Shared.interchange.Interval> intervals = new ArrayList<>();
			intervals.add(new Interval(false, 1, 2));
			
			// Build interchange taxnode feature
			List<Shared.interchange.Feature> taxnode_features = new ArrayList<> ();

			Shared.interchange.TaxonomyNode.Builder taxnode_builder = Shared.interchange.TaxonomyNode.builder();
			taxnode_builder.withIntervals(intervals);
			if (tn.getTaxnodeId() != null) taxnode_builder.withNodeID(tn.getTaxnodeId());
			if (tn.getParentTaxnodeId() != null) taxnode_builder.withParentNodeID(tn.getParentTaxnodeId());
			if (tn.getFamilyId() != null) taxnode_builder.withFamilyID(tn.getFamilyId());
			if (tn.getGenusId() != null) taxnode_builder.withGenusID(tn.getGenusId());
			if (tn.getSpeciesId() != null) taxnode_builder.withSpeciesID(tn.getSpeciesId());
			if (!(tn.getName().equals(""))) taxnode_builder.withName(tn.getName());

			Shared.interchange.Feature taxnode = taxnode_builder.build();
			taxnode_features.add(taxnode);
			
			Shared.interchange.Reference ref = Shared.interchange.Reference.builder()
					.withFeatures(taxnode_features)
					.build();
			references.add(ref);
		}
		Shared.interchange.Interchange return_interchange = Shared.interchange.Interchange.builder()
				.withReferences(references)
				.build();
		
		return return_interchange;
		
	}
	
	// Helper method to parse string arr > integer arraylist
	// TODO: handle this good
	private ArrayList<Integer> parseIntegers(String[] string_array) {
		
		ArrayList<Integer> integer_list = new ArrayList<Integer> ();	
		try {
			for (String s : string_array) integer_list.add(Integer.parseInt(s));
		} catch (NumberFormatException e) {
			// TODO: Handle this
		}
		
		return integer_list;
	}

	/**
	 * An endpoint to override Spring's auto-generated entity model endpoints,
	 * which may cause confusion for devs and additional load on the server if
	 * someone uses them.
	 * @return
	 */
	@GetMapping(path= {"genomeMetas", "geneMetas", "geneOrfs", "orthologGroups", "taxonomyNodes"} )
	public @ResponseBody void endpointOverride() {
		throw new ResponseStatusException(HttpStatus.NOT_FOUND, "resource not found");
	}
}

