package ca.virology.veserver.family;

import org.springframework.data.repository.CrudRepository;

import ca.virology.veserver.family.Family;

public interface FamilyRepository extends CrudRepository<Family, Integer> {
	
}
