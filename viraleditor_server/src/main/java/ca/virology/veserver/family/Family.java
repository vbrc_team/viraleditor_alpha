package ca.virology.veserver.family;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Column;
import javax.persistence.Id;

@Entity
public class Family {

	@Id
	private Integer family_id;

	private String family_name;
	
	private char is_single_stranded;
	
	private char is_circular;
	
	private char has_itr;
	
	private Integer seq_window_length;
	
	public Integer getFamilyId() {
		return family_id;
	}
	
	public String getFamilyName() {
		return family_name;
	}
	
	public char getSingleStranded() {
		return is_single_stranded;
	}
	
	public char getIsCircular() {
		return is_circular;
	}
	
	public Integer getSeqWindowLength() {
		return seq_window_length;
	}
}
