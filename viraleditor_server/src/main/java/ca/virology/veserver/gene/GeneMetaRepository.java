package ca.virology.veserver.gene;

import java.util.List;

import org.springframework.data.repository.CrudRepository;
import org.springframework.transaction.annotation.Transactional;

import ca.virology.veserver.gene.GeneMeta;

public interface GeneMetaRepository extends CrudRepository<GeneMeta, Integer> {
	
	@Transactional
	Iterable<GeneMeta> findByGenomeIdIn(List<Integer> genome_id);
	
	Iterable<GeneMeta> findByGenomeId(Integer genome_id);
	
}
