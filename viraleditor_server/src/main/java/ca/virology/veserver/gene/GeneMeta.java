package ca.virology.veserver.gene;

import javax.persistence.Entity;
import javax.persistence.Column;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import java.time.LocalDateTime;
import java.time.ZoneId;
import java.util.Date;

/**
 * Entity representing gene entries, without sequence data.
 * To be used when sequence data is not required, since sequence data is large.
 * @author me929
 *
 */
@Entity
@Table(name = "gene")
public class GeneMeta {

	// IDs
	@Id
	@Column(name = "gene_id")
	private Integer geneId;
	
	@Column(name = "family_id")
	private Integer familyId;
	
	@Column(name = "genome_id")
	private Integer genomeId;

	@Column(name = "parent_gene_id")
	private Integer parentGeneId;
	
	
	// Names + textual info
	@Column(name = "gene_abbr")
	private String gene_abbr;
	
	@Column(name = "annotator")
	private String annotator;
	
	@Column(name = "anno_source")
	private String annoSource;
	
	@Column(name = "feature_name")
	private String featureName;
	
	@Column(name = "function_name")
	private String functionName;
	
	@Column(name = "regulation")
	private String regulation;
	
	@Column(name = "description")
	private String description;
	
	@Column(name = "notes")
	private String notes;
	
	
	// Timestamps
	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "inserted_on")
	private Date insertedOn;
	
	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "last_modified")
	private Date lastModified;
	
	// Gene metadata
	@Column(name = "molecule_type")
	private String moleculeType;
	
	@Column(name = "strand")
	private char strand;
	
	@Column(name = "ec_num")
	private String ecNum;
	
	@Column(name = "seq_version")
	private Integer seqVersion;
	
	
	// Genbank stuff
	@Column(name = "gb_name")
	private String gbName;
	
	@Column(name = "gb_gene_gi")
	private Integer gbGeneGi;
	
	@Column(name = "gb_gene_accession")
	private String gbGeneAccession;
	
	@Column(name = "gb_gene_version")
	private Integer gbGeneVersion;

	// Ortholog data
	@Column(name = "ortholog_group_id")
	private Integer orthologGroupId;
	
	@Column(name = "ortholog_evid")
	private String orthologEvid;
	
	// Getters
	public Integer getGeneId() {
		return geneId;
	}

	public Integer getFamilyId() {
		return familyId;
	}

	public Integer getGenomeId() {
		return genomeId;
	}

	public Integer getParentGeneId() {
		return parentGeneId;
	}

	public String getGene_abbr() {
		return gene_abbr;
	}

	public String getAnnotator() {
		return annotator;
	}

	public String getAnnoSource() {
		return annoSource;
	}

	public String getFeatureName() {
		return featureName;
	}

	public String getFunctionName() {
		return functionName;
	}

	public String getRegulation() {
		return regulation;
	}

	public String getDescription() {
		return description;
	}

	public String getNotes() {
		return notes;
	}

	public LocalDateTime getInsertedOn() {
		return insertedOn.toInstant().atZone(ZoneId.of("GMT-8")).toLocalDateTime();
	}

	public LocalDateTime getLastModified() {
		return lastModified.toInstant().atZone(ZoneId.of("GMT-8")).toLocalDateTime();
	}

	public String getMoleculeType() {
		return moleculeType;
	}

	public char getStrand() {
		return strand;
	}

	public String getEcNum() {
		return ecNum;
	}

	public Integer getSeqVersion() {
		return seqVersion;
	}

	public String getGbName() {
		return gbName;
	}

	public Integer getGbGeneGi() {
		if (gbGeneGi == null) {
			return 0;
		}
		return gbGeneGi;
	}

	public String getGbGeneAccession() {
		return gbGeneAccession;
	}

	public Integer getGbGeneVersion() {
		if (gbGeneVersion == null) {
			return 1;
		}
		return gbGeneVersion;
	}

	public Integer getOrthologGroupId() {
		return orthologGroupId;
	}

	public String getOrthologEvid() {
		return orthologEvid;
	}
	
}
