package ca.virology.veserver.gene;

import java.util.ArrayList;
import java.util.List;

import org.springframework.data.repository.CrudRepository;
import org.springframework.transaction.annotation.Transactional;

import ca.virology.veserver.gene.Gene;
import ca.virology.veserver.genome.Genome;

public interface GeneRepository extends CrudRepository<Gene, Integer> {
	
	@Transactional
	Iterable<Gene> findByGenomeIdIn(List<Integer> genome_id);
	
	Iterable<Gene> findByGenomeId(Integer genome_id);
	
}
