package ca.virology.veserver.taxonomyNode;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;

import ca.virology.veserver.taxonomyNode.TaxonomyNode;

public interface TaxonomyNodeRepository extends CrudRepository<TaxonomyNode, Integer> {

}
