package ca.virology.veserver.taxonomyNode;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "taxonomy_node")
public class TaxonomyNode {

	@Id
	@Column(name = "taxnode_id")
	Integer taxnodeId;

	@Column(name = "parent_taxnode_id")
	Integer parentTaxnodeId;
	
	@Column(name = "family_id")
	Integer familyId;
	
	@Column(name = "genus_id")
	Integer genusId;
	
	@Column(name = "species_id")
	Integer speciesId;
	
	@Column(name = "name")
	String name;

	public Integer getTaxnodeId() {
		return taxnodeId;
	}

	public Integer getParentTaxnodeId() {
		return parentTaxnodeId;
	}

	public Integer getFamilyId() {
		return familyId;
	}

	public Integer getGenusId() {
		return genusId;
	}

	public Integer getSpeciesId() {
		return speciesId;
	}

	public String getName() {
		return name;
	}	
	
}
