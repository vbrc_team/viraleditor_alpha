package ca.virology.veserver.orthologGroup;

import org.springframework.data.repository.CrudRepository;

import ca.virology.veserver.orthologGroup.OrthologGroup;

public interface OrthologGroupRepository extends CrudRepository<OrthologGroup, Integer> {

}
