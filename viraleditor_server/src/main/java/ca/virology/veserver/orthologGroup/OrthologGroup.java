package ca.virology.veserver.orthologGroup;

import javax.persistence.Entity;
import javax.persistence.Column;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table (name = "ortholog_group")
public class OrthologGroup {
	
	@Id
	@Column(name = "ortholog_group_id")
	Integer orthologGroupId;
	
	@Column(name = "family_id")
	Integer familyId;
	
	@Column(name = "voc_abbr")
	String vocAbbr;
	
	@Column(name = "voc_function")
	String vocFunction;
	
	@Column(name = "name")
	String name;
	
	@Column(name = "ref_abbrs")
	String refAbbrs;
	
	@Column(name = "notes")
	String notes;

	// Getters
	
	public Integer getOrthologGroupId() {
		return orthologGroupId;
	}

	public Integer getFamilyId() {
		return familyId;
	}

	public String getVocAbbr() {
		return vocAbbr;
	}

	public String getVocFunction() {
		return vocFunction;
	}

	public String getName() {
		return name;
	}

	public String getRefAbbrs() {
		return refAbbrs;
	}

	public String getNotes() {
		return notes;
	}
	
}
