package ca.virology.veserver.geneOrf;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;

public interface GeneOrfRepository extends CrudRepository<GeneOrf, Integer> {

	/*
	@Query("SELECT g FROM GeneOrf WHERE gene_id = ?1")
	Iterable<GeneOrf> getGeneOrfsByGeneId(Integer gene_id);
	*/
}
