package ca.virology.veserver.geneOrf;

import javax.persistence.Entity;
import javax.persistence.Column;
import javax.persistence.Id;
import javax.persistence.IdClass;
import javax.persistence.Table;

@Entity
@Table (name = "gene_orf")
//@IdClass(GeneOrfId.class)
public class GeneOrf implements java.lang.Comparable<GeneOrf>{

	@Id
	@Column(name = "gene_id")
	private Integer geneId;
	
	@Column(name = "position")
	private Integer position;
	
	@Column(name = "family_id")
	private Integer familyId;
	
	@Column(name = "start")
	private Integer start;
	
	@Column(name = "stop")
	private Integer stop;

	// Getters

	public Integer getGeneId() {
		return geneId;
	}

	public Integer getPosition() {
		return position;
	}

	public Integer getFamilyId() {
		return familyId;
	}

	public Integer getStart() {
		return start;
	}

	public Integer getStop() {
		return stop;
	}
	
	// Comparisons
	public int compareTo(GeneOrf o) {
		return position - o.getPosition();
	}

	
}
