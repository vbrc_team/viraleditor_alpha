package ca.virology.veserver.geneOrf;

import java.io.Serializable;

// May want to use @EmbeddedID over @IdClass, i dunno

public class GeneOrfId implements Serializable {

	private String gene_id;
	
	private String position;
	
	public GeneOrfId() {
		super();
	}
	
	public GeneOrfId(String gene_id, String position) {
		this.gene_id = gene_id;
		this.position = position;
	}
	
	public boolean equals(Object o) {
		
		if (o == null) {
			return false;
		}
		
		if (getClass() != o.getClass()) {
			return false;
		}
		
		final GeneOrfId other = (GeneOrfId) o;
		
		// TODO: dealing with nulls, maybe- they should be required, 
		// but might not be a bad idea anyways
		
		if ( (this.getGene_id().equals(other.getGene_id()) ) && 
				( this.getPosition().equals(other.getPosition())) ) {
			return true;
		} else {
			return false;
		}
	}
	
	// Getters / setters required for hibernate bean creation

	public String getGene_id() {
		return gene_id;
	}

	public void setGene_id(String gene_id) {
		this.gene_id = gene_id;
	}

	public String getPosition() {
		return position;
	}

	public void setPosition(String position) {
		this.position = position;
	}


	
	
}
