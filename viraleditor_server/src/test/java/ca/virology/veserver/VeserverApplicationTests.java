package ca.virology.veserver;

import static org.junit.jupiter.api.Assertions.assertNotNull;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

@SpringBootTest
class VeserverApplicationTests {

	@Autowired
	private MainController controller;
	
	@Test
	void contextLoads() throws Exception {
		assertNotNull(controller);
	}

}
